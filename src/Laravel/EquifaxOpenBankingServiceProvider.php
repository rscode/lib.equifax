<?php

namespace Ratespecial\Equifax\Laravel;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\RequestOptions;
use Illuminate\Contracts\Support\DeferrableProvider;
use Ratespecial\Equifax\Common\EquifaxApiProduct;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Exceptions\UnsupportedProductEnvironmentException;
use Ratespecial\Equifax\OpenBankingConsumer\OpenBankingService;

class EquifaxOpenBankingServiceProvider extends AbstractEquifaxProductServiceProvider implements DeferrableProvider
{
    public const PRODUCTION_SERVICE = 'ef-openbanking-consumer-live';
    public const UAT_SERVICE = 'ef-openbanking-consumer-test';

    public function register(): void
    {
        $this->registerOpenBankingConsumerService();
    }

    protected function registerOpenBankingConsumerService(): void
    {
        $this->app->bind(self::UAT_SERVICE, function () {
            $stack = HandlerStack::create();

            if ($retryMiddleware = $this->buildRetryMiddleware()) {
                $stack->push($retryMiddleware, 'retry');
            }

            $client = new Client([
                'base_uri'              => 'https://api.uat.equifax.co.uk',
                'handler'               => $stack,
                RequestOptions::HEADERS => [
                    'User-Agent' => $this->getUserAgent(),
                ],
            ]);

            $cachedToken = $this->getCachedToken(EquifaxApiProduct::OPEN_BANKING_CONSUMER, EquifaxEnvironment::UAT);

            return new OpenBankingService($client, EquifaxEnvironment::UAT, $cachedToken);
        });

        $this->app->bind(self::PRODUCTION_SERVICE, function () {
            $stack = HandlerStack::create();

            if ($retryMiddleware = $this->buildRetryMiddleware()) {
                $stack->push($retryMiddleware, 'retry');
            }

            $client = new Client([
                'base_uri'              => 'https://api.equifax.co.uk',
                'handler'               => $stack,
                RequestOptions::HEADERS => [
                    'User-Agent' => $this->getUserAgent(),
                ],
            ]);

            $cachedToken = $this->getCachedToken(EquifaxApiProduct::OPEN_BANKING_CONSUMER, EquifaxEnvironment::PRODUCTION);

            return new OpenBankingService($client, EquifaxEnvironment::PRODUCTION, $cachedToken);
        });

        $this->app->bind(OpenBankingService::class, $this->getServiceTag());
    }

    /**
     * Gets the label of the service as it's stored in the Laravel Container
     *
     * @param EquifaxEnvironment|null $env
     * @return string
     * @throws Exception
     */
    protected function getServiceTag(?EquifaxEnvironment $env = null): string
    {
        // Read the default environment from the config file if necessary
        if (is_null($env)) {
            $env = EquifaxEnvironment::from($this->app['config']['equifax.default-env']);
        }

        return match ($env) {
            EquifaxEnvironment::PRODUCTION => self::PRODUCTION_SERVICE,
            EquifaxEnvironment::UAT => self::UAT_SERVICE,
            default => throw new UnsupportedProductEnvironmentException("Unsupported Open Banking environment [{$env->name}]"),
        };
    }

    public function provides(): array
    {
        return [
            OpenBankingService::class,
            self::PRODUCTION_SERVICE,
            self::UAT_SERVICE,
        ];
    }
}
