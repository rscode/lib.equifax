<?php

namespace Ratespecial\Equifax\Laravel;

use Closure;
use Exception;
use GuzzleHttp\Utils;
use GuzzleRetry\GuzzleRetryMiddleware;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;
use Psr\Http\Message\RequestInterface;
use Psr\Log\LoggerInterface;
use Ratespecial\Equifax\Common\AccessToken;
use Ratespecial\Equifax\Common\AccessTokenCacheInterface;
use Ratespecial\Equifax\Common\EquifaxApiProduct;
use Ratespecial\Equifax\Common\EquifaxEnvironment;

abstract class AbstractEquifaxProductServiceProvider extends ServiceProvider
{
    protected function getUserAgent(): string
    {
        $appName = (string)$this->app['config']['app.name'];

        if ($appName) {
            $ua = Utils::defaultUserAgent() . " ratspecial/equifax ($appName)";
        } else {
            $ua = Utils::defaultUserAgent() . " ratspecial/equifax";
        }

        return $ua;
    }

    protected function buildRetryMiddleware(): ?Closure
    {
        $config          = $this->app['config']['equifax'];
        $retryMiddleware = null;

        if (Arr::get($config, 'retry.lib-config.retry_enabled')) {
            // If the required library isn't installed, let's be loud about it.
            if (!class_exists('\GuzzleRetry\GuzzleRetryMiddleware')) {
                throw new Exception('Retry configured but required library missing');
            }

            // Setup optional logging.  Only do this if they haven't overridden the retry callback.
            if (Arr::get($config, 'retry.log-retries') && !Arr::get($config, 'retry.lib-config.on_retry_callback')) {
                Arr::set($config, 'retry.lib-config.on_retry_callback', $this->buildRetryLogger());
            }

            $retryMiddleware = GuzzleRetryMiddleware::factory(Arr::get($config, 'retry.lib-config'));
        }

        return $retryMiddleware;
    }

    /**
     * Builds a callback for use with GuzzleRetryMiddleware's on_retry_callback feature.
     *
     * @return callable
     * @throws BindingResolutionException
     */
    protected function buildRetryLogger(): callable
    {
        /** @var LoggerInterface $logger */
        $logger = $this->app->make(LoggerInterface::class);

        return function (int $retryCount, int|float $delayTimeout, RequestInterface $request) use ($logger) {
            $logger->debug('Retrying Equifax call', [
                'retry-count' => $retryCount,
                'uri'         => (string)$request->getUri(),
            ]);
        };
    }

    protected function getCachedToken(EquifaxApiProduct $service, EquifaxEnvironment $env): ?AccessToken
    {
        /** @var AccessTokenCacheInterface $tokenCache */
        $tokenCache = $this->app->make(AccessTokenCacheInterface::class);

        return $tokenCache->load($service, $env);
    }
}
