<?php

namespace Ratespecial\Equifax\Laravel\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Config\Repository as ConfigRepository;
use Ratespecial\Equifax\Common\EquifaxApiProduct;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Common\RefreshAccessTokenFactory;
use Ratespecial\Equifax\Luminate\LuminateService;

class LuminatePasswordExpirationCommand extends Command
{
    protected $signature = 'equifax:luminate-password-expiration 
        {--eq-environment= : uat or production}';
    protected $description = 'Queries the Equifax security service for our current password\'s expiration date';

    public function __construct(private readonly ConfigRepository $configRepository)
    {
        parent::__construct();
    }

    public function handle(RefreshAccessTokenFactory $accessTokenFactory): int
    {
        $env     = $this->determineEnvironment();
        $service = $accessTokenFactory->get(EquifaxApiProduct::LUMINATE, $env);

        /** @var LuminateService $tokenService */
        $tokenService = $service->getTokenService();

        $expirationDate = $tokenService->getPasswordExpirationDate($service->getUsernameOrClientId());

        $this->line($expirationDate->toDateString());

        return self::SUCCESS;
    }

    private function determineEnvironment(): EquifaxEnvironment
    {
        $envStr = $this->option('eq-environment');

        if (empty($envStr)) {
            $envStr = $this->configRepository->get('equifax.default-env');
        }

        return EquifaxEnvironment::from($envStr);
    }
}
