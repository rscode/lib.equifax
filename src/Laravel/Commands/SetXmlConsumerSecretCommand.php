<?php

namespace Ratespecial\Equifax\Laravel\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Contracts\Config\Repository as ConfigRepository;
use Ratespecial\Equifax\Common\EquifaxApiProduct;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Luminate\Credentials\LuminateCredentials;
use Ratespecial\Equifax\Luminate\Credentials\PasswordStoreInterface;
use Ratespecial\Equifax\XMLConsumer\Security\Credentials\Credentials;
use Ratespecial\Equifax\XMLConsumer\Security\Credentials\CredentialsStoreInterface;

class SetXmlConsumerSecretCommand extends Command
{
    protected $signature = 'equifax:set-secret 
        {service : luminate or xml-consumer}
        {secret}
        {eq-environment : uat or production}
        {--at= : Date secret was generated at.  YYYY-MM-DD}';
    protected $description = 'Sets client secret for XML consumer service using configured credentials store';

    private CredentialsStoreInterface $xmlStore;
    private PasswordStoreInterface $luminateStore;

    public function __construct(private readonly ConfigRepository $config)
    {
        parent::__construct();
    }

    public function handle(CredentialsStoreInterface $xmlStore, PasswordStoreInterface $luminateStore): int
    {
        $this->xmlStore      = $xmlStore;
        $this->luminateStore = $luminateStore;

        if ($at = $this->option('at')) {
            $at = new Carbon($at);
        } else {
            $at = Carbon::now();
        }

        $service = EquifaxApiProduct::from($this->argument('service'));
        $env          = $this->determineEnvironment();
        $secret = $this->argument('secret');

        if ($service == EquifaxApiProduct::LUMINATE) {
            $this->luminate($secret, $at, $env);
        } elseif ($service == EquifaxApiProduct::XML_CONSUMER) {
            $this->xmlConsumer($secret, $at, $env);
        } else {
            $this->error('Invalid service provided');
            return self::FAILURE;
        }

        $this->line('Done');

        return self::SUCCESS;
    }

    private function luminate(string $secret, Carbon $at, EquifaxEnvironment $env): void
    {
        $configString = sprintf('equifax.%s.%s.username', EquifaxApiProduct::LUMINATE->value, $env->value);

        $cred = new LuminateCredentials(
            $this->config->get($configString),
            $secret,
            $at,
        );

        $this->luminateStore->save($cred, $env);
    }

    private function xmlConsumer(string $secret, Carbon $at, EquifaxEnvironment $env): void
    {
        $configString = sprintf('equifax.%s.%s.client-id', EquifaxApiProduct::XML_CONSUMER->value, $env->value);

        $cred = new Credentials(
            $this->config->get($configString),
            $secret,
            $at,
        );

        $this->xmlStore->save($cred, $env);
    }

    private function determineEnvironment(): EquifaxEnvironment
    {
        $envStr = $this->argument('eq-environment');

        if (empty($envStr)) {
            $envStr = $this->config->get('equifax.default-env');
        }

        return EquifaxEnvironment::from($envStr);
    }
}
