<?php

namespace Ratespecial\Equifax\Laravel\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Config\Repository as ConfigRepository;
use Illuminate\Log\Logger;
use Ratespecial\Equifax\Common\EquifaxApiProduct;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Luminate\Credentials\PasswordCyclerFactory;
use Ratespecial\Equifax\XMLConsumer\Security\Credentials\ClientSecretCyclerFactory;

class MaintainSecretCommand extends Command
{
    protected $signature = 'equifax:maintain-secret 
        {services? : CSV of luminate,xml-consumer} 
        {maxAgeInDays?} 
        {--eq-environment= : uat or production}';
    protected $description = 'Cycles client secret or password for security services as needed';

    public function __construct(private readonly ConfigRepository $configRepository, private readonly Logger $logger)
    {
        parent::__construct();
    }

    public function handle(ClientSecretCyclerFactory $xmlCyclerFactory, PasswordCyclerFactory $passwordCyclerFactory): int
    {
        $env           = $this->determineEnvironment();
        $services      = $this->determineServices();

        foreach ($services as $service) {
            if ($service == EquifaxApiProduct::XML_CONSUMER) {
                $cyclerService = $xmlCyclerFactory->get($env, $this->argument('maxAgeInDays'));
            } else {
                $cyclerService = $passwordCyclerFactory->get($env, $this->argument('maxAgeInDays'));
            }

            $this->line("Refreshing secret for {$service->name}@{$env->name} if necessary (max age {$cyclerService->maxAgeInDays} days)");

            $wasRotated = $cyclerService->refreshIfNecessary($this->argument('maxAgeInDays'));
            $daysOld    = $cyclerService->getLastCheckedCredentials()?->generatedAt->diffInDays();

            if ($wasRotated) {
                $log = "{$service->name}@{$env->name} secret rotated ($daysOld days old)";
            } else {
                $log = "{$service->name}@{$env->name} secret rotation skipped ($daysOld days old)";
            }

            $this->line($log);
            $this->logger->info($log);
        }

        return self::SUCCESS;
    }

    /**
     * @return EquifaxApiProduct[]
     */
    private function determineServices(): array
    {
        if (!$this->argument('services')) {
            $services = [EquifaxApiProduct::LUMINATE, EquifaxApiProduct::XML_CONSUMER];
        } else {
            $services = explode(',', $this->argument('services'));
            $services = array_map(fn($v) => EquifaxApiProduct::from($v), $services);
        }

        return $services;
    }

    private function determineEnvironment(): EquifaxEnvironment
    {
        $envStr = $this->option('eq-environment');

        if (empty($envStr)) {
            $envStr = $this->configRepository->get('equifax.default-env');
        }

        return EquifaxEnvironment::from($envStr);
    }
}
