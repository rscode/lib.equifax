<?php

namespace Ratespecial\Equifax\Laravel\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Cache\Repository as CacheRepository;
use Illuminate\Contracts\Config\Repository as ConfigRepository;
use Illuminate\Log\Logger;
use Ratespecial\Equifax\Common\EquifaxApiProduct;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Common\RefreshAccessTokenFactory;
use Ratespecial\Equifax\Laravel\ManagesFailureDelays;
use Ratespecial\Equifax\Luminate\Exceptions\InvalidCredentialsException;

class MaintainAccessTokensCommand extends Command
{
    use ManagesFailureDelays;

    protected $signature = 'equifax:maintain-tokens 
        {services? : CSV of luminate,open-banking,xml-consumer} 
        {--f|force : Force token refresh regardless of expiration time}
        {--eq-environment= : sandbox, uat, or production}';
    protected $description = 'Ensures we have working AccessTokens for services';

    public function __construct(
        private readonly ConfigRepository $configRepository,
        private readonly Logger $logger,
        private readonly CacheRepository $cache,
    ) {
        parent::__construct();
    }

    public function handle(RefreshAccessTokenFactory $serviceFactory): int
    {
        $services = $this->determineServices();
        $force    = (bool)$this->option('force');
        $env      = $this->determineEnvironment();

        foreach ($services as $service) {
            $failureCacheKey = $this->getFailureDelayKey($service, $env);

            // If we recently failed this service, don't try again.
            if ($this->cache->get($failureCacheKey)) {
                $this->logger->warning('Access token refresh skipped due to delay cache');
                continue;
            }

            $this->logger->info("Refreshing AccessToken for {$service->name}@{$env->name}", ['force' => $force]);

            $refresher = $serviceFactory->get($service, $env);

            try {
                if ($force) {
                    $refresher->refreshAndSave();
                    $result = true;
                } else {
                    $result = $refresher->refreshIfNecessary();
                }

                $this->logger->info("Refreshed AccessToken for {$service->name}@{$env->name}", ['result' => $result]);
            } catch (InvalidCredentialsException $ex) {
                $this->logger->error($ex);

                // Invalid credentials need manual intervention to correct.  Delay the next retry by a configured amount.
                $failureDelayMinutes = $this->configRepository->get('equifax.refresh-failure-delay');

                if ($failureDelayMinutes > 0) {
                    $this->cache->put($failureCacheKey, now(), now()->addMinutes($failureDelayMinutes));
                }
            }
        }

        return self::SUCCESS;
    }

    /**
     * @return EquifaxApiProduct[]
     */
    protected function determineServices(): array
    {
        if (!$this->argument('services')) {
            $services = EquifaxApiProduct::all();
        } else {
            $services = explode(',', $this->argument('services'));
            $services = array_map(fn($v) => EquifaxApiProduct::from($v), $services);
        }

        return $services;
    }

    protected function determineEnvironment(): EquifaxEnvironment
    {
        $envStr = $this->option('eq-environment');

        if (empty($envStr)) {
            $envStr = $this->configRepository->get('equifax.default-env');
        }

        return EquifaxEnvironment::from($envStr);
    }
}
