<?php

namespace Ratespecial\Equifax\Laravel;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\RequestOptions;
use Illuminate\Contracts\Support\DeferrableProvider;
use Ratespecial\Equifax\Common\EquifaxApiProduct;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Luminate\LuminateService;

class EquifaxLuminateServiceProvider extends AbstractEquifaxProductServiceProvider implements DeferrableProvider
{
    public const PRODUCTION_SERVICE = 'ef-luminate-live';
    public const UAT_SERVICE = 'ef-luminate-uat';
    public const SANDBOX_SERVICE = 'ef-luminate-sandbox';

    public function register(): void
    {
        $this->registerLuminateService();
    }

    protected function registerLuminateService(): void
    {
        /*
         * PRODUCTION
         */
        $this->app->bind(self::PRODUCTION_SERVICE, function () {
            $stack = HandlerStack::create();

            if ($retryMiddleware = $this->buildRetryMiddleware()) {
                $stack->push($retryMiddleware, 'retry');
            }

            $client = new Client([
                'base_uri'              => 'https://api.equifax.co.uk',
                'handler'               => $stack,
                RequestOptions::HEADERS => [
                    'User-Agent' => $this->getUserAgent(),
                ],
            ]);

            $cachedToken = $this->getCachedToken(EquifaxApiProduct::LUMINATE, EquifaxEnvironment::PRODUCTION);

            return new LuminateService($client, EquifaxEnvironment::PRODUCTION, $cachedToken);
        });

        /*
         * UAT
         */
        $this->app->bind(self::UAT_SERVICE, function () {
            $stack = HandlerStack::create();

            if ($retryMiddleware = $this->buildRetryMiddleware()) {
                $stack->push($retryMiddleware, 'retry');
            }

            $client = new Client([
                'base_uri'              => 'https://api.uat.equifax.co.uk',
                'handler'               => $stack,
                RequestOptions::HEADERS => [
                    'User-Agent' => $this->getUserAgent(),
                ],
            ]);

            $cachedToken = $this->getCachedToken(EquifaxApiProduct::LUMINATE, EquifaxEnvironment::UAT);

            return new LuminateService($client, EquifaxEnvironment::UAT, $cachedToken);
        });

        /*
         * SANDBOX
         */
        $this->app->bind(self::SANDBOX_SERVICE, function () {
            $client = new Client([
                'base_uri' => 'https://api.sandbox.equifax.co.uk',
            ]);

            $cachedToken = $this->getCachedToken(EquifaxApiProduct::LUMINATE, EquifaxEnvironment::SANDBOX);

            return new LuminateService($client, EquifaxEnvironment::SANDBOX, $cachedToken);
        });

        // Setup default binding per default environment in config
        $this->app->bind(LuminateService::class, $this->getServiceTag());
    }

    /**
     * Gets the label of the service as it's stored in the Laravel Container
     *
     * @param EquifaxEnvironment|null $env
     * @return string
     */
    protected function getServiceTag(?EquifaxEnvironment $env = null): string
    {
        // Read the default environment from the config file if necessary
        if (is_null($env)) {
            $env = EquifaxEnvironment::from($this->app['config']['equifax.default-env']);
        }

        return match ($env) {
            EquifaxEnvironment::PRODUCTION => self::PRODUCTION_SERVICE,
            EquifaxEnvironment::UAT => self::UAT_SERVICE,
            EquifaxEnvironment::SANDBOX => self::SANDBOX_SERVICE,
        };
    }

    public function provides(): array
    {
        return [
            LuminateService::class,
            self::UAT_SERVICE,
            self::SANDBOX_SERVICE,
            self::PRODUCTION_SERVICE,
        ];
    }
}
