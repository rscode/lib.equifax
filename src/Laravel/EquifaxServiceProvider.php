<?php

namespace Ratespecial\Equifax\Laravel;

use Illuminate\Support\ServiceProvider;
use Ratespecial\Equifax\Common\AccessTokenCacheInterface;
use Ratespecial\Equifax\Common\SimpleTokenCache;
use Ratespecial\Equifax\Laravel\Commands\LuminatePasswordExpirationCommand;
use Ratespecial\Equifax\Laravel\Commands\MaintainAccessTokensCommand;
use Ratespecial\Equifax\Laravel\Commands\MaintainSecretCommand;
use Ratespecial\Equifax\Laravel\Commands\SetXmlConsumerSecretCommand;

/**
 * Laravel service provider.  Reads the config file and makes it available to the product specific service providers.  The product providers
 * are deferred until needed.
 */
class EquifaxServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        // Check for existence of function.  Lumen doesn't have it.
        if (function_exists('config_path')) {
            $this->publishes([
                __DIR__ . '/../../config/equifax.php' => config_path('equifax.php'),
            ]);
        }

        if ($this->app->runningInConsole()) {
            $this->commands([
                MaintainAccessTokensCommand::class,
                SetXmlConsumerSecretCommand::class,
                MaintainSecretCommand::class,
                LuminatePasswordExpirationCommand::class,
            ]);
        }
    }

    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/equifax.php', 'equifax');

        $this->app->bind(AccessTokenCacheInterface::class, SimpleTokenCache::class);
    }
}
