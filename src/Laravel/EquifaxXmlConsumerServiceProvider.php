<?php

namespace Ratespecial\Equifax\Laravel;

use Exception;
use Illuminate\Contracts\Support\DeferrableProvider;
use Ratespecial\Equifax\Common\EquifaxApiProduct;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Exceptions\MissingWsdlException;
use Ratespecial\Equifax\Exceptions\UnsupportedProductEnvironmentException;
use Ratespecial\Equifax\XMLConsumer\Address\AddressService;
use Ratespecial\Equifax\XMLConsumer\Consumer\ConsumerService;
use Ratespecial\Equifax\XMLConsumer\Security\ClassMap;
use Ratespecial\Equifax\XMLConsumer\Security\SecurityService;
use WsdlToPhp\PackageBase\SoapClientInterface;

class EquifaxXmlConsumerServiceProvider extends AbstractEquifaxProductServiceProvider implements DeferrableProvider
{
    public const SECURITY_LIVE = 'ef-securityservice-live';
    public const SECURITY_UAT = 'ef-securityservice-test';
    public const XML_LIVE = 'ef-consumerservice-live';
    public const XML_UAT = 'ef-consumerservice-test';
    public const ADDRESS_LIVE = 'ef-addressservice-live';
    public const ADDRESS_UAT = 'ef-addressservice-test';

    public function register(): void
    {
        $this->registerXmlConsumerSecurity();
        $this->registerXmlConsumerConsumer();
        $this->registerXmlConsumerAddress();
    }

    protected function registerXmlConsumerSecurity(): void
    {
        /*
         * Production SecurityService
         */
        $this->app->bind(self::SECURITY_LIVE, function () {
            $options = [
                SoapClientInterface::WSDL_URL      => $this->getWsdlPath('security', EquifaxEnvironment::PRODUCTION),
                SoapClientInterface::WSDL_CLASSMAP => ClassMap::get(),
                SoapClientInterface::WSDL_LOCATION => 'https://api.equifax.co.uk/efx-uk-xmlii-security/v1',
            ];

            $cachedToken = $this->getCachedToken(EquifaxApiProduct::XML_CONSUMER, EquifaxEnvironment::PRODUCTION);

            if (is_null($cachedToken)) {
                $cachedToken = '';
            }

            $service = new SecurityService($options, $cachedToken);
            $service->setEnvironment(EquifaxEnvironment::PRODUCTION);

            return $service;
        });

        /*
         * UAT SecurityService
         */
        $this->app->bind(self::SECURITY_UAT, function () {
            $options = [
                SoapClientInterface::WSDL_URL      => $this->getWsdlPath('security', EquifaxEnvironment::UAT),
                SoapClientInterface::WSDL_CLASSMAP => ClassMap::get(),
                SoapClientInterface::WSDL_LOCATION => 'https://api.uat.equifax.co.uk/efx-uk-xmlii-security/v1',
            ];

            $cachedToken = $this->getCachedToken(EquifaxApiProduct::XML_CONSUMER, EquifaxEnvironment::UAT);

            if (is_null($cachedToken)) {
                $cachedToken = '';
            }

            $service = new SecurityService($options, $cachedToken);
            $service->setEnvironment(EquifaxEnvironment::UAT);

            return $service;
        });

        /*
         * SecurityService has no sandbox
         */

        // Setup default binding per default environment in config
        $this->app->bind(SecurityService::class, $this->getServiceTag('security'));
    }

    protected function registerXmlConsumerConsumer(): void
    {
        /*
         * Production ConsumerService
         */
        $this->app->bind(self::XML_LIVE, function () {
            $options = [
                SoapClientInterface::WSDL_URL      => $this->getWsdlPath('xml', EquifaxEnvironment::PRODUCTION),
                SoapClientInterface::WSDL_CLASSMAP => \Ratespecial\Equifax\XMLConsumer\Consumer\ClassMap::get(),
                SoapClientInterface::WSDL_LOCATION => 'https://api.equifax.co.uk/efx-uk-xmlii-consumer/v1',
            ];

            $cachedToken = $this->getCachedToken(EquifaxApiProduct::XML_CONSUMER, EquifaxEnvironment::PRODUCTION);

            if (is_null($cachedToken)) {
                $cachedToken = '';
            }

            $service = new ConsumerService($options, $cachedToken);
            $service->setEnvironment(EquifaxEnvironment::PRODUCTION);

            return $service;
        });

        /*
         * UAT ConsumerService
         */
        $this->app->bind(self::XML_UAT, function () {
            $options = [
                SoapClientInterface::WSDL_URL      => $this->getWsdlPath('xml', EquifaxEnvironment::UAT),
                SoapClientInterface::WSDL_CLASSMAP => \Ratespecial\Equifax\XMLConsumer\Consumer\ClassMap::get(),
                SoapClientInterface::WSDL_LOCATION => 'https://api.uat.equifax.co.uk/efx-uk-xmlii-consumer/v1',
            ];

            $cachedToken = $this->getCachedToken(EquifaxApiProduct::XML_CONSUMER, EquifaxEnvironment::UAT);

            if (is_null($cachedToken)) {
                $cachedToken = '';
            }

            $service = new ConsumerService($options, $cachedToken);
            $service->setEnvironment(EquifaxEnvironment::UAT);

            return $service;
        });

        /*
         * ConsumerService has no sandbox
         */

        // Setup default binding per default environment in config
        $this->app->bind(ConsumerService::class, $this->getServiceTag('xml'));
    }

    protected function registerXmlConsumerAddress(): void
    {
        /*
         * Production AddressService
         */
        $this->app->bind(self::ADDRESS_LIVE, function () {
            $options = [
                SoapClientInterface::WSDL_URL      => $this->getWsdlPath('address', EquifaxEnvironment::PRODUCTION),
                SoapClientInterface::WSDL_CLASSMAP => \Ratespecial\Equifax\XMLConsumer\Address\ClassMap::get(),
                SoapClientInterface::WSDL_LOCATION => 'https://api.equifax.co.uk/efx-uk-xmlii-address/v1',
            ];

            $cachedToken = $this->getCachedToken(EquifaxApiProduct::XML_CONSUMER, EquifaxEnvironment::PRODUCTION);

            if (is_null($cachedToken)) {
                $cachedToken = '';
            }

            $service = new AddressService($options, $cachedToken);
            $service->setEnvironment(EquifaxEnvironment::PRODUCTION);

            return $service;
        });

        /*
         * UAT AddressService
         */
        $this->app->bind(self::ADDRESS_UAT, function () {
            $options = [
                SoapClientInterface::WSDL_URL      => $this->getWsdlPath('address', EquifaxEnvironment::UAT),
                SoapClientInterface::WSDL_CLASSMAP => \Ratespecial\Equifax\XMLConsumer\Address\ClassMap::get(),
                SoapClientInterface::WSDL_LOCATION => 'https://api.uat.equifax.co.uk/efx-uk-xmlii-address/v1',
            ];

            $cachedToken = $this->getCachedToken(EquifaxApiProduct::XML_CONSUMER, EquifaxEnvironment::UAT);

            if (is_null($cachedToken)) {
                $cachedToken = '';
            }

            $service = new AddressService($options, $cachedToken);
            $service->setEnvironment(EquifaxEnvironment::UAT);

            return $service;
        });

        /*
         * AddressService has no sandbox
         */

        // By default, if a service is needed, use live.
        $this->app->bind(AddressService::class, $this->getServiceTag('address'));
    }

    /**
     * Gets the label of the service as it's stored in the Laravel Container
     *
     * @param string                  $service
     * @param EquifaxEnvironment|null $env
     * @return string
     * @throws Exception
     */
    protected function getServiceTag(string $service, ?EquifaxEnvironment $env = null): string
    {
        // Read the default environment from the config file if necessary
        if (is_null($env)) {
            $env = EquifaxEnvironment::from($this->app['config']['equifax.default-env']);
        }

        if ($service == 'xml') {
            return match ($env) {
                EquifaxEnvironment::PRODUCTION => self::XML_LIVE,
                EquifaxEnvironment::UAT => self::XML_UAT,
                default => throw new UnsupportedProductEnvironmentException("Unsupported XMLConsumer service [$service] and environment [{$env->name}]")
            };
        } elseif ($service == 'address') {
            return match ($env) {
                EquifaxEnvironment::PRODUCTION => self::ADDRESS_LIVE,
                EquifaxEnvironment::UAT => self::ADDRESS_UAT,
                default => throw new UnsupportedProductEnvironmentException("Unsupported XMLConsumer service [$service] and environment [{$env->name}]")
            };
        } elseif ($service == 'security') {
            return match ($env) {
                EquifaxEnvironment::PRODUCTION => self::SECURITY_LIVE,
                EquifaxEnvironment::UAT => self::SECURITY_UAT,
                default => throw new UnsupportedProductEnvironmentException("Unsupported XMLConsumer service [$service] and environment [{$env->name}]")
            };
        }

        throw new UnsupportedProductEnvironmentException("Unsupported XMLConsumer service [$service] and environment [{$env->name}]");
    }

    protected function getWsdlPath(string $service, EquifaxEnvironment $env): string
    {
        $serviceConfig = match ($service) {
            'security' => 'security-service-wsdl',
            'xml' => 'consumer-service-wsdl',
            'address' => 'address-service-wsdl',
        };

        $config = sprintf('equifax.%s.%s.%s', EquifaxApiProduct::XML_CONSUMER->value, $env->value, $serviceConfig);
        $wsdl   = $this->app['config'][$config];

        if (!file_exists($wsdl)) {
            throw new MissingWsdlException("XML Consumer WSDL file doesn't exist");
        }

        return $wsdl;
    }

    public function provides(): array
    {
        return [
            SecurityService::class,
            self::SECURITY_LIVE,
            self::SECURITY_UAT,

            ConsumerService::class,
            self::XML_LIVE,
            self::XML_UAT,

            AddressService::class,
            self::ADDRESS_LIVE,
            self::ADDRESS_UAT,
        ];
    }
}
