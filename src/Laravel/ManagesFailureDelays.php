<?php

namespace Ratespecial\Equifax\Laravel;

use Ratespecial\Equifax\Common\EquifaxApiProduct;
use Ratespecial\Equifax\Common\EquifaxEnvironment;

/**
 * Some failures will cache that a failure happened so future attempts won't be repeated.  This helps create the cache key.
 */
trait ManagesFailureDelays
{
    protected function getFailureDelayKey(EquifaxApiProduct $service, EquifaxEnvironment $env): string
    {
        return "failure-delay-{$service->name}@{$env->name}";
    }
}
