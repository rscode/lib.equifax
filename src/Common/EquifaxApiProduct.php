<?php

namespace Ratespecial\Equifax\Common;

/**
 * Enumeration of Equifax API products
 */
enum EquifaxApiProduct: string
{
    case LUMINATE = 'luminate';
    case OPEN_BANKING_CONSUMER = 'open-banking-consumer';
    case XML_CONSUMER = 'xml-consumer';

    public static function all(): array
    {
        return [self::LUMINATE, self::OPEN_BANKING_CONSUMER, self::XML_CONSUMER];
    }
}
