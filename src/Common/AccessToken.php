<?php

namespace Ratespecial\Equifax\Common;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Namshi\JOSE\JWS;

class AccessToken
{
    public string $access_token = '';
    public string $refresh_token = '';
    public string $token_type = '';
    /**
     * Number of seconds from token issue to expiration.  To know how long from the current time to expiration, see lifeLeft()
     */
    public int $ttl = 0;

    public Carbon $issuedAt;

    public function __construct(?Carbon $issuedAt = null)
    {
        if (is_null($issuedAt)) {
            $this->issuedAt = Carbon::now('UTC');
        } else {
            $this->issuedAt = $issuedAt;
        }
    }

    public static function createFromJWT(string $jwt): self
    {
        $jws = JWS::load($jwt);

        $issuedAt  = $jws->getPayload()['iat'];
        $expiresAt = $jws->getPayload()['exp'];

        $issuedAtDt = Carbon::createFromTimestampUTC($issuedAt);

        $accessToken               = new self($issuedAtDt);
        $accessToken->access_token = $jwt;
        $accessToken->ttl          = $expiresAt - $issuedAt;

        return $accessToken;
    }

    public function isExpired(): bool
    {
        return $this->expiresAt()->isPast();
    }

    /**
     * Time the token will expire
     */
    public function expiresAt(): Carbon
    {
        return $this->issuedAt->clone()->addSeconds($this->ttl);
    }

    /**
     * How long from the current time to token expiration
     */
    public function lifeLeft(): CarbonInterval
    {
        return Carbon::now()
            ->diffAsCarbonInterval(
                $this->expiresAt(),
                absolute: false,
            );
    }

    public function __toString(): string
    {
        return $this->access_token;
    }
}
