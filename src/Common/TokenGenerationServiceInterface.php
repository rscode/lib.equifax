<?php

namespace Ratespecial\Equifax\Common;

/**
 * Equifax service responsible for turning credentials into access tokens.  This will be internal services like LuminateService or
 * SecurityService.
 */
interface TokenGenerationServiceInterface
{
    public function generateAccessToken(string $usernameOrClientId, string $passwordOrSecret): AccessToken;

    public function getServiceName(): EquifaxApiProduct;

    public function getEnvironment(): EquifaxEnvironment;
}
