<?php

namespace Ratespecial\Equifax\Common;

/**
 * A service that can save and load access tokens to Equifax' services, so they don't need to be retrieved each use.  This will need to be
 * implemented at the application level.
 *
 * @see RefreshAccessTokenService
 * @see TokenGenerationServiceInterface
 */
interface AccessTokenCacheInterface
{
    public function save(EquifaxApiProduct $service, EquifaxEnvironment $env, AccessToken $token): void;

    public function load(EquifaxApiProduct $service, EquifaxEnvironment $env): ?AccessToken;
}
