<?php

namespace Ratespecial\Equifax\Common;

use Exception;
use InvalidArgumentException;

/**
 * Aids in generating a random password for use in changing the Luminate password.  This
 * was gleaned from https://stackoverflow.com/questions/6101956/generating-a-random-password-in-php/31284266#31284266
 */
class RandomPasswordGenerator
{
    private const PASSWORD_LENGTH = 30;
    private const KEYSPACE = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-+_!@#$^*.,?';

    /**
     * Regex pattern to match a valid password against.  Courtesy of https://stackoverflow.com/questions/1559751/regex-to-make-sure-that-the-string-contains-at-least-one-lower-case-char-upper
     */
    private const REGEX_TEST = '/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[-+_!@#$^*.,?])/m';
    /**
     * Test for two consecutive identical characters
     */
    private const REGEX_TEST2 = '/(\w)\1/m';

    /**
     * @throws Exception
     */
    public static function generate(): string
    {
        do {
            $pw = self::randomString();
            preg_match_all(self::REGEX_TEST, $pw, $policy1, PREG_SET_ORDER, 0);
            preg_match_all(self::REGEX_TEST2, $pw, $policy2, PREG_SET_ORDER, 0);
        } while (count($policy1) == 0 && count($policy2) == 0);

        return $pw;
    }

    /**
     * Generate a random string, using a cryptographically secure
     * pseudorandom number generator (random_int)
     *
     * For PHP 7, random_int is a PHP core function
     * For PHP 5.x, depends on https://github.com/paragonie/random_compat
     *
     * @throws Exception
     */
    private static function randomString(): string
    {
        $str = '';
        $max = mb_strlen(self::KEYSPACE, '8bit') - 1;
        if ($max < 1) {
            throw new InvalidArgumentException('$keyspace must be at least two characters long');
        }
        for ($i = 0; $i < self::PASSWORD_LENGTH; ++$i) {
            $str .= self::KEYSPACE[random_int(0, $max)];
        }

        return $str;
    }
}
