<?php

namespace Ratespecial\Equifax\Common;

/**
 * Manages AccessTokens to make sure there is always a fresh one for use.
 */
class RefreshAccessTokenService
{
    /**
     * @param TokenGenerationServiceInterface $tokenService
     * @param AccessTokenCacheInterface       $cache
     * @param string                          $usernameOrClientId
     * @param string                          $passwordOrSecret
     * @param int                             $minuteThreshold How long before token expiration should we consider a token stale and in
     *                                                         need of refreshing
     */
    public function __construct(
        protected readonly TokenGenerationServiceInterface $tokenService,
        protected readonly AccessTokenCacheInterface $cache,
        protected string $usernameOrClientId = '',
        protected string $passwordOrSecret = '',
        protected int $minuteThreshold = 60,
    ) {
    }

    /**
     * Load the cached token and check to see if it's about to expire.  If so, this will call refreshTokenAndSave()
     *
     * @param int|null $minuteThreshold
     * @return bool     True if a refresh occurred, false if not
     */
    public function refreshIfNecessary(?int $minuteThreshold = null): bool
    {
        if (is_null($minuteThreshold)) {
            $minuteThreshold = $this->minuteThreshold;
        }

        $token = $this->cache->load($this->tokenService->getServiceName(), $this->tokenService->getEnvironment());

        if ($token && $token->expiresAt()->subMinutes($minuteThreshold)->isFuture()) {
            return false;
        }

        $this->refreshAndSave($this->usernameOrClientId, $this->passwordOrSecret);

        return true;
    }

    /**
     * Get a new token and store it in the cache.  Uses member variables for credentials if blanks are supplied here.
     */
    public function refreshAndSave(string $usernameOrClientId = '', string $passwordOrSecret = ''): AccessToken
    {
        if (empty($usernameOrClientId)) {
            $usernameOrClientId = $this->usernameOrClientId;
        }
        if (empty($passwordOrSecret)) {
            $passwordOrSecret = $this->passwordOrSecret;
        }

        $token = $this->tokenService->generateAccessToken($usernameOrClientId, $passwordOrSecret);

        $this->cache->save($this->tokenService->getServiceName(), $this->tokenService->getEnvironment(), $token);

        return $token;
    }

    public function getUsernameOrClientId(): string
    {
        return $this->usernameOrClientId;
    }

    public function getMinuteThreshold(): int
    {
        return $this->minuteThreshold;
    }

    public function setMinuteThreshold(int $minuteThreshold): self
    {
        $this->minuteThreshold = $minuteThreshold;

        return $this;
    }

    public function getTokenService(): TokenGenerationServiceInterface
    {
        return $this->tokenService;
    }
}
