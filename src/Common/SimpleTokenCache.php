<?php

namespace Ratespecial\Equifax\Common;

use DateInterval;
use Psr\SimpleCache\CacheInterface;

class SimpleTokenCache implements AccessTokenCacheInterface
{
    public function __construct(protected readonly CacheInterface $cache)
    {
    }

    public function save(EquifaxApiProduct $service, EquifaxEnvironment $env, AccessToken $token): void
    {
        $cacheKey = $this->getCacheKey($service, $env);

        /*
         * Equifax sent notice in October 2024 that some of their services will re-use access tokens for a 1 hour period instead of
         * generating a new one each request.  This means even though we know new tokens expire in say 5 hours, we can't assume the token
         * we just requested will expire in 5 hours.  It's important to use the expiration timestamp in the JWT.
         */

        $this->cache->set(
            $cacheKey,
            serialize($token),
            (int) $token->lifeLeft()->totalSeconds,
        );
    }

    public function load(EquifaxApiProduct $service, EquifaxEnvironment $env): ?AccessToken
    {
        $cacheKey    = $this->getCacheKey($service, $env);
        $tokenString = $this->cache->get($cacheKey);

        $token = null;
        if ($tokenString) {
            $token = unserialize($tokenString);
        }

        return $token ?: null;
    }

    protected function getCacheKey(EquifaxApiProduct $service, EquifaxEnvironment $env): string
    {
        return "equifax-{$service->name}-{$env->name}-token";
    }
}
