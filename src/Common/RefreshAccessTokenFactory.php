<?php

namespace Ratespecial\Equifax\Common;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Container\Container;
use Ratespecial\Equifax\Exceptions\MissingClientSecretException;
use Ratespecial\Equifax\Exceptions\UnsupportedProductEnvironmentException;
use Ratespecial\Equifax\Laravel\EquifaxLuminateServiceProvider;
use Ratespecial\Equifax\Laravel\EquifaxOpenBankingServiceProvider;
use Ratespecial\Equifax\Laravel\EquifaxXmlConsumerServiceProvider;
use Ratespecial\Equifax\Luminate\Credentials\PasswordStoreInterface;
use Ratespecial\Equifax\Luminate\LuminateService;
use Ratespecial\Equifax\XMLConsumer\Common\AbstractXmlConsumerService;
use Ratespecial\Equifax\XMLConsumer\Security\Credentials\CredentialsStoreInterface;

/**
 * Produces RefreshAccessTokenService for the myriad of different product and environment combinations.
 * Laravel only.
 */
class RefreshAccessTokenFactory
{
    public function __construct(protected readonly Container $app)
    {
    }

    /**
     * @throws BindingResolutionException|UnsupportedProductEnvironmentException
     * @throws MissingClientSecretException
     */
    public function get(EquifaxApiProduct $product, EquifaxEnvironment $env): RefreshAccessTokenService
    {
        $minutes      = $this->app['config']['equifax.refresh-token-threshold'];
        $cacheService = $this->app->make(AccessTokenCacheInterface::class);

        [$usernameField, $passwordField, $serviceTag] = $this->determineFieldsAndService($product, $env);

        // Grab the field values from the config file.
        $username = $this->app['config']["equifax.{$product->value}.{$env->value}.$usernameField"];
        $password = $this->app['config']["equifax.{$product->value}.{$env->value}.$passwordField"];

        // Create the token service
        $tokenService = $this->app->make($serviceTag);

        if ($tokenService instanceof AbstractXmlConsumerService) {
            $tokenService->setEnvironment($env);

            // XML Consumer secret rotates regularly and therefore isn't stored in the config.
            /** @var CredentialsStoreInterface $secretStore */
            $secretStore = $this->app->make(CredentialsStoreInterface::class);
            $credentials = $secretStore->load($env);

            if (empty($credentials->clientSecret)) {
                throw new MissingClientSecretException('XML Consumer client secret not found');
            }

            $password = $credentials->clientSecret;
        } elseif ($tokenService instanceof LuminateService) {
            /** @var PasswordStoreInterface $passwordStore */
            $passwordStore = $this->app->make(PasswordStoreInterface::class);
            $credentials   = $passwordStore->load($env);

            if (empty($credentials->password)) {
                throw new MissingClientSecretException('Luminate password not found');
            }

            $password = $credentials->password;
        }

        return new RefreshAccessTokenService($tokenService, $cacheService, $username, $password, $minutes);
    }

    /**
     * @param EquifaxApiProduct  $product
     * @param EquifaxEnvironment $env
     * @return array{string, string, string}
     * @throws UnsupportedProductEnvironmentException
     */
    protected function determineFieldsAndService(EquifaxApiProduct $product, EquifaxEnvironment $env): array
    {
        $serviceTag = '';

        // Based on the Equifax service, different field names and token services are used.
        if ($product == EquifaxApiProduct::LUMINATE) {
            $usernameField = 'username';
            $passwordField = 'this-will-come-from-store';

            if ($env == EquifaxEnvironment::PRODUCTION) {
                $serviceTag = EquifaxLuminateServiceProvider::PRODUCTION_SERVICE;
            } elseif ($env == EquifaxEnvironment::UAT) {
                $serviceTag = EquifaxLuminateServiceProvider::UAT_SERVICE;
            } elseif ($env == EquifaxEnvironment::SANDBOX) {
                $serviceTag = EquifaxLuminateServiceProvider::SANDBOX_SERVICE;
            }
        } elseif ($product == EquifaxApiProduct::XML_CONSUMER) {
            $usernameField = 'client-id';
            $passwordField = 'this-will-come-from-store';

            if ($env == EquifaxEnvironment::PRODUCTION) {
                $serviceTag = EquifaxXmlConsumerServiceProvider::SECURITY_LIVE;
            } elseif ($env == EquifaxEnvironment::UAT) {
                $serviceTag = EquifaxXmlConsumerServiceProvider::SECURITY_UAT;
            }
        } elseif ($product == EquifaxApiProduct::OPEN_BANKING_CONSUMER) {
            $usernameField = 'client-id';
            $passwordField = 'client-secret';

            if ($env == EquifaxEnvironment::PRODUCTION) {
                $serviceTag = EquifaxOpenBankingServiceProvider::PRODUCTION_SERVICE;
            } elseif ($env == EquifaxEnvironment::UAT) {
                $serviceTag = EquifaxOpenBankingServiceProvider::UAT_SERVICE;
            }
        } else {
            throw new UnsupportedProductEnvironmentException("Unsupported environment [{$env->name}]");
        }

        if (!$serviceTag) {
            throw new UnsupportedProductEnvironmentException("Unsupported product [$product->name] and environment [$env->name]");
        }

        return [$usernameField, $passwordField, $serviceTag];
    }
}
