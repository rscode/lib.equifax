<?php

namespace Ratespecial\Equifax\Common;

enum EquifaxEnvironment: string
{
    case SANDBOX = 'sandbox';
    case UAT = 'uat';
    case PRODUCTION = 'production';
}
