<?php

namespace Ratespecial\Equifax\Luminate;

use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use JsonMapper;
use JsonMapper_Exception;
use Ratespecial\Equifax\Common\AccessToken;
use Ratespecial\Equifax\Common\EquifaxApiProduct;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Common\TokenGenerationServiceInterface;
use Ratespecial\Equifax\Exceptions\MissingAccessTokenException;
use Ratespecial\Equifax\Luminate\Exceptions\InvalidCredentialsException;
use Ratespecial\Equifax\Luminate\Exceptions\LuminateException;
use Ratespecial\Equifax\Luminate\Requests\AbstractLuminateRequest;
use Ratespecial\Equifax\Luminate\Requests\AbstractLuminateResponse;

class LuminateService implements TokenGenerationServiceInterface
{
    private const INVALID_CREDENTIALS_MESSAGE = 'The credentials provided were invalid.';

    protected JsonMapper $mapper;

    public function __construct(
        protected readonly Client $client,
        public readonly EquifaxEnvironment $environment,
        public ?AccessToken $accessToken = null,
    ) {
        $this->mapper = new JsonMapper();

        // If this function exists on the mapped object, it will be called at the end.  Good for re-organizing an object once
        // all data is present.
        $this->mapper->postMappingMethod = 'afterMapping';

        // If there are some new fields in the record that our class doesn't define, set them in the object.
        $this->mapper->undefinedPropertyHandler = function ($object, $propName, $jsonValue) {
            $object->$propName = $jsonValue;
        };
    }

    public function getServiceName(): EquifaxApiProduct
    {
        return EquifaxApiProduct::LUMINATE;
    }

    public function getEnvironment(): EquifaxEnvironment
    {
        return $this->environment;
    }

    /**
     * @param string $username
     * @param string $password
     * @return AccessToken
     * @throws GuzzleException
     * @throws LuminateException
     */
    public function generateAccessToken(string $username, string $password): AccessToken
    {
        $postData = [
            'username'   => $username,
            'password'   => $password,
            'grant_type' => 'password',
            'scope'      => 'sa.readprofile sa.editprofile',
        ];

        try {
            $resp = $this->client->post('/icsaas-security-service/v1/token', [
                RequestOptions::HEADERS     => [
                    'Accept' => 'application/json',
                ],
                RequestOptions::FORM_PARAMS => $postData,
            ]);
        } catch (BadResponseException $ex) {
            $responseBody   = (string)$ex->getResponse()->getBody();
            $framedException = new LuminateException($responseBody, $ex->getCode(), $ex);

            if ($framedException->getMessage() == self::INVALID_CREDENTIALS_MESSAGE) {
                $framedException = new InvalidCredentialsException($responseBody, $ex->getCode(), $framedException);
            }

            throw $framedException;
        }

        $responseBody = (string)$resp->getBody();

        /** @var object{token_type: string, expires_in: int, access_token: string, refresh_token: string} $data */
        $data = json_decode($responseBody);

        if (is_array($data) && str_contains($responseBody, 'logref')) {
            throw new LuminateException($responseBody);
        }

        $token                = AccessToken::createFromJWT($data->access_token);
        $token->refresh_token = $data->refresh_token;
        $token->token_type    = $data->token_type;

        $this->accessToken = $token;

        return $token;
    }

    /**
     * Retrieves password expiration date for a given user
     *
     * @throws GuzzleException
     * @throws LuminateException
     */
    public function getPasswordExpirationDate(string $username): Carbon
    {
        try {
            $resp = $this->client->get("/icsaas-security-service/v1/api/users/$username/pwdexpirationdate", [
                RequestOptions::HEADERS => [
                    'Content-Type'  => 'application/vnd.com.equifax.clientConfig.v1+json',
                    'Authorization' => "Bearer $this->accessToken",
                ],
            ]);
        } catch (BadResponseException $ex) {
            throw new LuminateException((string)$ex->getResponse()->getBody(), $ex->getCode(), $ex);
        }

        $responseBody = (string)$resp->getBody();
        $data         = json_decode($responseBody);

        if (is_array($data) && str_contains($responseBody, 'logref')) {
            throw new LuminateException($responseBody);
        }

        return Carbon::createFromFormat('d-m-Y', $data->ExpirationDate)->startOfDay();
    }

    /**
     * @throws GuzzleException
     * @throws LuminateException
     */
    public function updatePassword(string $username, string $currentPassword, string $newPassword): void
    {
        try {
            $resp = $this->client->put("/icsaas-security-service/v1/api/users/$username", [
                RequestOptions::HEADERS => [
                    'Content-Type'  => 'application/vnd.com.equifax.clientConfig.v1+json',
                    'Authorization' => "Bearer $this->accessToken",
                ],
                RequestOptions::JSON => [
                    'userId'     => $username,
                    'currentPwd' => $currentPassword,
                    'newPwd'     => $newPassword,
                ],
            ]);
        } catch (BadResponseException $ex) {
            throw new LuminateException((string)$ex->getResponse()->getBody(), $ex->getCode(), $ex);
        }

        $responseBody = (string)$resp->getBody();
        $data         = json_decode($responseBody);

        if (is_array($data) && str_contains($responseBody, 'logref')) {
            throw new LuminateException($responseBody);
        }
    }

    /**
     * Executes Identity Verification, Identity Assurance, or Digital Trust calls
     *
     * @throws MissingAccessTokenException
     * @throws JsonMapper_Exception
     * @throws GuzzleException|LuminateException
     * @throws Exception
     */
    public function execute(AbstractLuminateRequest $request): AbstractLuminateResponse
    {
        if (!$this->accessToken) {
            throw new MissingAccessTokenException('Missing access token during request');
        }

        $url = "/luminate/v1/clientimplementation-service/api/transaction/{$request->productId->value}/{$request->getProductConfiguration()}/InitialBaseline/{$request->getOrchestration()->value}/{$request->getClientOrganization()}/{$request->getClientImplementation()}";

        try {
            $resp = $this->client->post($url, [
                RequestOptions::HEADERS => [
                    // Setting this causes an HTML "request rejected"
                    //  'Accept'               => 'application/json',

                    // Setting this to anything else, such as application/json, causes "request rejected"
                    'Content-Type' => 'application/vnd.com.equifax.clientConfig.v1+json',

                    'Authorization' => "Bearer $this->accessToken",
                ],
                RequestOptions::JSON    => $request,
            ]);
        } catch (BadResponseException $ex) {
            throw new LuminateException((string)$ex->getResponse()->getBody(), $ex->getCode(), $ex);
        }

        $responseBody = (string)$resp->getBody();
        $data         = json_decode($responseBody);

        if (is_null($data)) {
            throw new Exception("Invalid JSON response: $responseBody");
        } elseif (is_array($data) && str_contains($responseBody, 'logref')) {
            throw new LuminateException($responseBody);
        }

        $responseClass = $request->getResponseClass();

        return $this->mapper->map($data, new $responseClass());
    }
}
