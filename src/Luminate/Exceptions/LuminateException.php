<?php

namespace Ratespecial\Equifax\Luminate\Exceptions;

use Exception;
use Throwable;

/**
 * Errors come back in the form of
 *
 * [
 *   {
 *     "logref": "89364e4d35e3f7f24344b6ba78139b76",
 *     "message": "Request is unauthorized."
 *   }
 * ]
 *
 * This will parse out the first error message and use that for our exception
 */
class LuminateException extends Exception
{
    public readonly array $allErrors;

    public function __construct(string $responseBody, int $code = 0, ?Throwable $previous = null)
    {
        $errMsg = 'Luminate Exception';
        $data = json_decode($responseBody);

        if (is_array($data)) {
            $this->allErrors = $data;

            $errMsg = $data[0]->message;
        } else {
            $this->allErrors = [];
        }

        parent::__construct($errMsg, $code, $previous);
    }


    /*public function __construct(BadResponseException $ex)
    {
        $data = json_decode($ex->getResponse()->getBody());

        $code    = $ex->getCode();
        $message = 'Luminate Exception';

        if (is_array($data)) {
            $this->allErrors = $data;

            $message = $data[0]->message;
        } else {
            $this->allErrors = [];
        }

        parent::__construct($message, $code, $ex);
    }*/
}
