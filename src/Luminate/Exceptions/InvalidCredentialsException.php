<?php

namespace Ratespecial\Equifax\Luminate\Exceptions;

/**
 * Getting the access token failed due to invalid credentials
 */
class InvalidCredentialsException extends LuminateException
{
}
