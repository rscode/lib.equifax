<?php

namespace Ratespecial\Equifax\Luminate\Requests;

use Ratespecial\Equifax\Luminate\Enums\Orchestration;
use Ratespecial\Equifax\Luminate\Enums\Product;
use Ratespecial\Equifax\Luminate\Models\ProductConfiguration;

/**
 * URL breakdown:
 * https://api.equifax.co.uk/luminate/v1/clientimplementation-service/api/transaction/{{productId}}/{{productConfiguration}}/InitialBaseline/{{orchestration}}/{{clientOrganization}}/{{clientImplementation}}
 */
abstract class AbstractLuminateRequest
{
    /**
     * Used as a URL segment in requests.  Get this from Equifax handover documents.
     *
     * @var string
     */
    protected string $productConfiguration = '';
    /**
     * Used as a URL segment in requests.  Initial requests are IDENTITY requests.  KBA responses are ASSURANCE requests.
     *
     * @var Orchestration
     */
    protected Orchestration $orchestration = Orchestration::IDENTITY;
    /**
     * Used as a URL segment in requests.  Get this from Equifax handover documents.
     *
     * @var string
     */
    protected string $clientOrganization = '';
    /**
     * Differs based on the action being performed (identity request, KBA assurance, digital trust).  Used as a URL segment in requests.
     * Get this from Equifax handover documents.
     *
     * @var string
     */
    protected string $clientImplementation = '';

    /**
     * Identifies the customer interacting with the platform.  Is a field passed in the request body.
     *
     * @var string
     */
    public string $tenantId = '';
    /**
     * Identifies the customer's application interacting with the platform.  Is a field passed in the request body.
     *
     * @var string
     */
    public string $applicationId = '';
    /**
     * Is a field passed in the request body.  Also used as a URL segment.
     *
     * @var Product
     */
    public Product $productId;
    /**
     * Specifies the orchestration of a certain product that is executing the capability. A product might have several orchestrations that
     * will determine product execution.  Is a field passed in the request body.
     *
     * @var string
     */
    public string $configId = '';

    /**
     * A pass-through identifier used by the customer's application to identify a specific request.
     *
     * @var string
     */
    public string $referenceId = '';
    /**
     * Input the STS User ID. This allows our customers to identify which requests have been made via API.
     *
     * @var string
     */
    public string $consumerId = '';

    public static function fromConfig(ProductConfiguration $config): self
    {
        $obj = new static();

        $obj->configure($config);

        return $obj;
    }

    public function configure(ProductConfiguration $config): self
    {
        $this->tenantId           = $config->tenantId;
        $this->applicationId      = $config->applicationId;
        $this->configId           = $config->configId;
        $this->clientOrganization = $config->clientOrganization;

        return $this;
    }

    /**
     * Name of the class that will be mapped to the response of this request.
     *
     * @return string
     */
    abstract public function getResponseClass(): string;

    public function getProductConfiguration(): string
    {
        return $this->productConfiguration;
    }

    public function setProductConfiguration(string $productConfiguration): AbstractLuminateRequest
    {
        $this->productConfiguration = $productConfiguration;

        return $this;
    }

    public function getOrchestration(): Orchestration
    {
        return $this->orchestration;
    }

    public function setOrchestration(Orchestration $orchestration): AbstractLuminateRequest
    {
        $this->orchestration = $orchestration;

        return $this;
    }

    public function getClientOrganization(): string
    {
        return $this->clientOrganization;
    }

    public function setClientOrganization(string $clientOrganization): AbstractLuminateRequest
    {
        $this->clientOrganization = $clientOrganization;

        return $this;
    }

    public function getClientImplementation(): string
    {
        return $this->clientImplementation;
    }

    public function setClientImplementation(string $clientImplementation): AbstractLuminateRequest
    {
        $this->clientImplementation = $clientImplementation;

        return $this;
    }
}
