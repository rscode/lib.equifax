<?php

namespace Ratespecial\Equifax\Luminate\Requests;

use JsonMapper;
use Ratespecial\Equifax\Luminate\Enums\IdentityDecisionAssessment;
use Ratespecial\Equifax\Luminate\Models\Error;
use Ratespecial\Equifax\Luminate\Models\KbaAuthentication;
use Ratespecial\Equifax\Luminate\Models\KbaQuestions;

class IdentityAssuranceResponse extends AbstractLuminateResponse
{
    public string $originalTransactionId = '';

    public function setAssessment(array $data): self
    {
        foreach ($data as $datum) {
            $this->assessment[$datum->key] = IdentityDecisionAssessment::from($datum->value);
        }

        return $this;
    }

    public function getDecision(): ?IdentityDecisionAssessment
    {
        return $this->assessment['decision'] ?? null;
    }

    public function setResults(array $results): void
    {
        $mapper = new JsonMapper();

        foreach ($results as &$result) {
            // Detect KBA questions in results and map a proper object for it.
            if (is_object($result) && isset($result->questionnaire)) {
                $kbaQuestions = $mapper->map($result, new KbaQuestions());
                $result       = $kbaQuestions;
            }

            if (is_object($result) && isset($result->errors) && is_array($result->errors)) {
                $objs           = array_map(fn($e) => $mapper->map($e, new Error()), $result->errors);
                $result->errors = $objs;
            }

            if (is_object($result) && isset($result->kbaAuthentication)) {
                $obj    = $mapper->map($result, new KbaAuthentication());
                $result = $obj;
            }
        }

        $this->results = $results;
    }

    /**
     * If one of the $result objects is the KBA questions
     *
     * @return bool
     */
    public function hasKbaQuestions(): bool
    {
        return (bool)array_filter($this->results, fn($result) => $result instanceof KbaQuestions);
    }

    public function getKbaQuestions(): ?KbaQuestions
    {
        $resultObj = array_filter($this->results, fn($result) => $result instanceof KbaQuestions);

        if (empty($resultObj)) {
            return null;
        }

        return current($resultObj);
    }

    public function getKbaDecisionBlock(): ?KbaAuthentication
    {
        $resultObj = array_filter($this->results, fn($result) => $result instanceof KbaAuthentication);

        if (empty($resultObj)) {
            return null;
        }

        return current($resultObj);
    }
}
