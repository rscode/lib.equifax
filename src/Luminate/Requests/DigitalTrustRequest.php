<?php

namespace Ratespecial\Equifax\Luminate\Requests;

use Ratespecial\Equifax\Luminate\Enums\IovationType;
use Ratespecial\Equifax\Luminate\Enums\Orchestration;
use Ratespecial\Equifax\Luminate\Enums\Product;
use Ratespecial\Equifax\Luminate\Models\DigitalTrustRequestData;

class DigitalTrustRequest extends AbstractLuminateRequest
{
    public Product $productId = Product::DIGITAL_TRUST;
    public string $productConfiguration = 'DigitalTrustConfig';
    public Orchestration $orchestration = Orchestration::DIGITAL_TRUST;
    public DigitalTrustRequestData $data;

    public function __construct()
    {
        $this->data = new DigitalTrustRequestData();
    }

    public function setIpAddress(string $ip): self
    {
        $this->data->ipAddress = $ip;

        return $this;
    }

    public function setBlackbox(string $val): self
    {
        $this->data->device->blackBox = $val;

        return $this;
    }

    public function setIovationType(IovationType $type): self
    {
        $this->data->additionalFields[] = [
            'key'   => 'iovation.type',
            'value' => $type->value,
        ];

        return $this;
    }

    public function setIovationAccountCode(string $code): self
    {
        $this->data->additionalFields[] = [
            'key'   => 'iovation.accountcode',
            'value' => $code,
        ];

        return $this;
    }

    public function getResponseClass(): string
    {
        return DigitalTrustResponse::class;
    }
}
