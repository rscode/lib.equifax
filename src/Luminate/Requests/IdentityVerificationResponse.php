<?php

namespace Ratespecial\Equifax\Luminate\Requests;

use Ratespecial\Equifax\Luminate\Enums\IdentityDecisionAssessment;
use Ratespecial\Equifax\Luminate\Models\IdentityRequestData;

class IdentityVerificationResponse extends AbstractLuminateResponse
{
    /**
     * Input data from request that produced this response
     *
     * @var IdentityRequestData|null
     */
    public ?IdentityRequestData $data = null;

    /** @var \Ratespecial\Equifax\Luminate\Models\Address[]|null */
    public ?array $outputAddress = null;

    public string $originalTransactionId;

    public function setAssessment(array $data): self
    {
        foreach ($data as $datum) {
            $this->assessment[$datum->key] = IdentityDecisionAssessment::from($datum->value);
        }

        return $this;
    }

    public function getDecision(): ?IdentityDecisionAssessment
    {
        return $this->assessment['decision'] ?? null;
    }
}
