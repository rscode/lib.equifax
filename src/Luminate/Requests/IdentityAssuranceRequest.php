<?php

namespace Ratespecial\Equifax\Luminate\Requests;

use Ratespecial\Equifax\Luminate\Enums\Product;
use Ratespecial\Equifax\Luminate\Models\Identity;
use Ratespecial\Equifax\Luminate\Models\IdentityRequestData;
use Ratespecial\Equifax\Luminate\Models\KbaAnswers;

class IdentityAssuranceRequest extends AbstractLuminateRequest
{
    public Product $productId = Product::IDENTITY_ASSURANCE;

    /**
     * A system generated identifier that is returned in the response with the question set. This value ties together
     * the initial request and the KBA
     *
     * @var string
     */
    public string $correlationId = '';

    public IdentityRequestData $data;

    public function __construct()
    {
        $this->data           = new IdentityRequestData();
        $this->data->identity = new Identity();
    }

    public function setIdentity(Identity $identity): self
    {
        $this->data->identity = $identity;

        return $this;
    }

    public function setKbaAnswers(KbaAnswers $answers): self
    {
        $this->data->kba = $answers;

        return $this;
    }

    public function getResponseClass(): string
    {
        return IdentityAssuranceResponse::class;
    }
}
