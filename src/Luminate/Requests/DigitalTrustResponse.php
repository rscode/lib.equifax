<?php

namespace Ratespecial\Equifax\Luminate\Requests;

use Ratespecial\Equifax\Luminate\Enums\DigitalTrustAssessment;

class DigitalTrustResponse extends AbstractLuminateResponse
{
    /**
     * Final answer.  See getDecision() to extract the useful value.
     *
     * @var array
     */
    public array $assessment;
    public string $originalTransactionId;

    public function setAssessment(array $data)
    {
        foreach ($data as $datum) {
            $this->assessment[$datum->key] = DigitalTrustAssessment::from($datum->value);
        }
    }

    public function getDecision(): ?DigitalTrustAssessment
    {
        return $this->assessment['decision'] ?? null;
    }
}
