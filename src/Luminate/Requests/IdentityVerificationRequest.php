<?php

namespace Ratespecial\Equifax\Luminate\Requests;

use Ratespecial\Equifax\Luminate\Enums\Product;
use Ratespecial\Equifax\Luminate\Models\Identity;
use Ratespecial\Equifax\Luminate\Models\IdentityRequestData;

class IdentityVerificationRequest extends AbstractLuminateRequest
{
    public Product $productId = Product::IDENTITY_VERIFICATION;
    public IdentityRequestData $data;

    public function __construct()
    {
        $this->data           = new IdentityRequestData();
        $this->data->identity = new Identity();
    }

    public function setIdentity(Identity $identity): self
    {
        $this->data->identity = $identity;

        return $this;
    }

    public function getResponseClass(): string
    {
        return IdentityAssuranceResponse::class;
    }
}
