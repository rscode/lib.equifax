<?php

namespace Ratespecial\Equifax\Luminate\Requests;

use Ratespecial\Equifax\Luminate\Enums\Product;
use Ratespecial\Equifax\Luminate\Models\Error;
use Ratespecial\Equifax\Luminate\Models\IdentityRequestData;

abstract class AbstractLuminateResponse
{
    public string $tenantId = '';
    public string $applicationId = '';
    /**
     * A pass-through identifier used by the customer's application to identify a specific request.
     *
     * @var string
     */
    public string $referenceId = '';
    public string $consumerId = '';
    public string $transactionId = '';
    public string $correlationId = '';
    public Product $productId;
    public string $configId = '';
    /**
     * ID for identity key and linking purposes. ID obtained on the data fabric entity master. (future use)
     *
     * @var string
     */
    public string $entityId = '';

    /**
     * Collection of data objects that varies based on the request performed.
     *
     * @var array
     */
    public array $results = [];
    public ?IdentityRequestData $data = null;

    /**
     * Stored in a keyed array, as $assessment['decision'] => [Enum instance].
     * For Identity Assurance and Verification, it will be IdentityDecisionAssessment::*
     * For Digital Trust, it will be DigitalTrustAssessment::*
     *
     * @var array
     */
    public array $assessment = [];

    /** @var \Ratespecial\Equifax\Luminate\Models\Address[]|null */
    public ?array $outputAddress = null;

    public string $originalTransactionId = '';

    public function setProductId(string $val): self
    {
        $this->productId = Product::from($val);

        return $this;
    }

    /**
     * @return Error[]
     */
    public function getErrors(): array
    {
        $return       = [];
        $errorResults = array_filter($this->results, fn($result) => isset($result->errors));

        /** @var object{'errors': array} $result */
        foreach ($errorResults as $result) {
            $return = array_merge($return, $result->errors);
        }

        return $return;
    }
}
