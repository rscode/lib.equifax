<?php

namespace Ratespecial\Equifax\Luminate\Credentials;

use Carbon\Carbon;

class LuminateCredentials
{
    public function __construct(
        public readonly string $username,
        public readonly string $password,
        public readonly Carbon $generatedAt,
    ) {
    }
}
