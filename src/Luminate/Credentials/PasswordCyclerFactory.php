<?php

namespace Ratespecial\Equifax\Luminate\Credentials;

use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Exceptions\UnsupportedProductEnvironmentException;
use Ratespecial\Equifax\Laravel\EquifaxLuminateServiceProvider;

class PasswordCyclerFactory
{
    public function __construct(protected readonly Container $app)
    {
    }

    /**
     * @throws UnsupportedProductEnvironmentException
     * @throws BindingResolutionException
     */
    public function get(EquifaxEnvironment $env, ?int $maxAgeInDays = null): PasswordCyclerService
    {
        $serviceTag = match ($env) {
            EquifaxEnvironment::PRODUCTION => EquifaxLuminateServiceProvider::PRODUCTION_SERVICE,
            EquifaxEnvironment::UAT => EquifaxLuminateServiceProvider::UAT_SERVICE,
            default => throw new UnsupportedProductEnvironmentException("Unsupported environment [{$env->name}]"),
        };

        $luminateService = $this->app->make($serviceTag);

        $with = [
            'luminateService' => $luminateService,
        ];

        // If null is supplied, we'll let PasswordCyclerService use its default value.
        if (!is_null($maxAgeInDays)) {
            $with['maxAgeInDays'] = $maxAgeInDays;
        }

        return $this->app->makeWith(PasswordCyclerService::class, $with);
    }
}
