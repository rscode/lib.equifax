<?php

namespace Ratespecial\Equifax\Luminate\Credentials;

use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use Ratespecial\Equifax\Common\RandomPasswordGenerator;
use Ratespecial\Equifax\Luminate\Exceptions\LuminateException;
use Ratespecial\Equifax\Luminate\LuminateService;
use Ratespecial\Equifax\XMLConsumer\Exceptions\MissingCredentialsException;

/**
 * Luminate passwords expire and must be rotated regularly.  This service will accomplish that.
 */
class PasswordCyclerService
{
    protected ?LuminateCredentials $lastCheckedCredentials = null;

    public function __construct(
        protected readonly LuminateService $luminateService,
        protected readonly PasswordStoreInterface $store,
        public int $maxAgeInDays = 340,     // In my testing, passwords are good for one year.
    ) {
    }

    /**
     * @param int|null $maxAgeInDays
     * @return bool  True if the password was refreshed, false if not.
     * @throws LuminateException
     * @throws MissingCredentialsException
     * @throws GuzzleException
     */
    public function refreshIfNecessary(?int $maxAgeInDays = null): bool
    {
        if (is_null($maxAgeInDays)) {
            $maxAgeInDays = $this->maxAgeInDays;
        }

        $secret = $this->store->load($this->luminateService->getEnvironment());

        if (!$secret) {
            throw new MissingCredentialsException("No credentials found when refreshing password.");
        }

        $this->lastCheckedCredentials = $secret;

        if ($secret->generatedAt->clone()->addDays($maxAgeInDays)->isFuture()) {
            return false;
        }

        $newPw = RandomPasswordGenerator::generate();

        $this->luminateService->updatePassword($secret->username, $secret->password, $newPw);
        $newCreds = new LuminateCredentials($secret->username, $newPw, Carbon::now('UTC'));
        $this->store->save($newCreds, $this->luminateService->getEnvironment());

        return true;
    }

    /**
     * After refreshIfNecessary() has run, this will be what credential was examined.
     */
    public function getLastCheckedCredentials(): ?LuminateCredentials
    {
        return $this->lastCheckedCredentials;
    }
}
