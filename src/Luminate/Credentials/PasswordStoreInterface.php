<?php

namespace Ratespecial\Equifax\Luminate\Credentials;

use Ratespecial\Equifax\Common\EquifaxEnvironment;

interface PasswordStoreInterface
{
    public function save(LuminateCredentials $credentials, EquifaxEnvironment $environment): void;

    public function load(EquifaxEnvironment $environment): ?LuminateCredentials;
}
