<?php

namespace Ratespecial\Equifax\Luminate\Models;

use Ratespecial\Equifax\Luminate\Enums\AddressType;

class Address
{
    public AddressType $addressType = AddressType::CURRENT;
    public string $countryCode = '';
    public string $addressLine1 = '';
    public ?string $addressLine2 = '';
    public string $city = '';
    public ?string $county = '';
    public string $region = '';
    public string $postalCode = '';
    public string $postalIdentifier = '';
    public string $isPrimary = 'true';

    public function setAddressType(string $val): self
    {
        $this->addressType = AddressType::from($val);

        return $this;
    }
}
