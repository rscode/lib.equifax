<?php

namespace Ratespecial\Equifax\Luminate\Models;

class ProductConfiguration
{
    /**
     * Identifies the customer interacting with the platform.
     *
     * @var string
     */
    public string $tenantId = '';
    /**
     * Identifies the customer's application interacting with the platform.
     *
     * @var string
     */
    public string $applicationId = '';
    /**
     * Specifies the orchestration of a certain product that is executing the capability. A product might have several orchestrations
     * that will determine product execution.
     *
     * @var string
     */
    public string $configId = '';
    /**
     * Used as part of the URL when making requests.  Seems to be static per Equifax client.
     *
     * @var string
     */
    public string $clientOrganization = '';

    public function __construct(string $tenantId = '', string $applicationId = '', string $configId = '', string $clientOrganization = '')
    {
        $this->tenantId           = $tenantId;
        $this->applicationId      = $applicationId;
        $this->configId           = $configId;
        $this->clientOrganization = $clientOrganization;
    }
}
