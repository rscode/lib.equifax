<?php

namespace Ratespecial\Equifax\Luminate\Models;

use Ratespecial\Equifax\Luminate\Requests\IdentityAssuranceRequest;
use Ratespecial\Equifax\Luminate\Requests\IdentityVerificationRequest;

/**
 * "data" property of a IdentityAssuranceRequest and IdentityVerificationRequest
 * The request data can differ based on request
 *
 * @see IdentityAssuranceRequest
 * @see IdentityVerificationRequest
 */
class IdentityRequestData
{
    public ?Identity $identity = null;
    public ?KbaAnswers $kba = null;
}
