<?php

namespace Ratespecial\Equifax\Luminate\Models;

class KbaQuestionChoice
{
    public int $choiceId;
    public string $choiceText;
    public bool $isCorrect;
}
