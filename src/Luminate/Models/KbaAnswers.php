<?php

namespace Ratespecial\Equifax\Luminate\Models;

/**
 * When responding to KBA questions, this object will be included in the IdentityAssuranceRequest
 */
class KbaAnswers
{
    /**
     * A system generated identifier that is returned in the response with the
     * question set. This value ties together the KBA question sets so is
     * required to be copied across from the response to the initial request.
     *
     * @var string
     */
    public string $authenticationId = '';
    /**
     * A sequential number returned from the initial request identifying the question set being responded to
     *
     * @var int
     */
    public int $questionnaireId = 0;

    public array $questionAnswers = [];

    public function __construct(string $authenticationId = '', int $questionnaireId = 0)
    {
        $this->authenticationId = $authenticationId;
        $this->questionnaireId  = $questionnaireId;
    }

    public function addAnswer(int $questionId, int $answer): self
    {
        // Answer always seems to be an array.
//        if (!is_array($answer)) {
            $answer = [$answer];
//        }

        $this->questionAnswers[] = [
            'questionId' => $questionId,
            'answer'     => $answer,
        ];

        return $this;
    }
}
