<?php

namespace Ratespecial\Equifax\Luminate\Models;

class Name
{
    public string $firstName;
    public ?string $middleName = null;
    public string $lastName;
    public ?string $secondLastName = null;
    public ?string $namePrefix = null;
    public ?string $nameSuffix = null;
}
