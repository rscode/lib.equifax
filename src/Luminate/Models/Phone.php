<?php

namespace Ratespecial\Equifax\Luminate\Models;

class Phone
{
    /**
     * @var string Home | Mobile | Work
     */
    public string $type = '';
    public string $number = '';
}
