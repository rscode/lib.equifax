<?php

namespace Ratespecial\Equifax\Luminate\Models;

class KbaQuestion
{
    public int $questionId;
    public string $questionText;
    /**
     * According to Equifax, this will always be SINGLE_CHOICE
     *
     * @var string
     */
    public string $choiceType;
    /** @var KbaQuestionChoice[] */
    public array $choices;

    public function setChoiceType(object $val): self
    {
        /*
         * "choiceType": {
         *   "type": "SINGLE_CHOICE"
         * }
         */

        $this->choiceType = $val->type;

        return $this;
    }
}
