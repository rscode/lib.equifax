<?php

namespace Ratespecial\Equifax\Luminate\Models;

use Ratespecial\Equifax\Luminate\Requests\DigitalTrustRequest;
use stdClass;

/**
 * "data" property of a DigitalTrustRequest
 *
 * @see DigitalTrustRequest
 */
class DigitalTrustRequestData
{
    public string $ipAddress = '';
    /**
     * @var stdClass{'blackBox': string}
     */
    public stdClass $device;
    public array $additionalFields = [];

    public function __construct()
    {
        $this->device = new stdClass();
    }
}
