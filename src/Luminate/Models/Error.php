<?php

namespace Ratespecial\Equifax\Luminate\Models;

/**
 * If something went wrong, such as a missing required field, these will show up under results->errors[]
 */
class Error
{
    public string $code = '';
    public string $msg = '';
    public string $detailedMsg = '';
}
