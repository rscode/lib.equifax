<?php

namespace Ratespecial\Equifax\Luminate\Models;

/**
 * In the "results" block of a response from IdentityAssurance there can be a KBA decision result.  This represents that piece.
 */
class KbaAuthentication
{
    public string $authenticationId = '';
    public array $kbaAuthentication = [
        'characteristics' => [],
    ];

    /**
     * Changes structure from
     * "kbaAuthentication": {
     *   "characteristics": [
     *     {"key": "decision","value": "pass","text": "N/A"}
     *   ]
     * }
     * to
     * "kbaAuthentication": {
     *   "characteristics": [
     *     "decision": {"key": "decision","value": "pass","text": "N/A"}
     *   ]
     *  }
     *
     * @param array $data
     * @return self
     */
    public function setKbaAuthentication(array $data): self
    {
        foreach ($data['characteristics'] as $characteristic) {
            $this->kbaAuthentication['characteristics'][$characteristic->key] = $characteristic;
        }

        return $this;
    }

    /**
     * @param string $key
     * @return object{'key': string, 'value': string, 'text': string}|null
     */
    public function getCharacteristic(string $key): ?object
    {
        return $this->kbaAuthentication['characteristics'][$key] ?? null;
    }

    /**
     * So far this has always been present.
     *
     * @return object{'key': string, 'value': string, 'text': string}
     */
    public function getDecision(): object
    {
        return $this->kbaAuthentication['characteristics']['decision'];
    }
}
