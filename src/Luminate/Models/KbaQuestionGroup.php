<?php

namespace Ratespecial\Equifax\Luminate\Models;

class KbaQuestionGroup
{
    public string $questionGroup;
    public string $questionHeader;
    /** @var int[] */
    public array $appliesToQuestionId;

//    /**
//     * Copy of objects from the KbaQuestionnaire, for ease of use.
//     *
//     * @var KbaQuestion[]
//     */
//    public array $questions = [];
}
