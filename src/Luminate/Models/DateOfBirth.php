<?php

namespace Ratespecial\Equifax\Luminate\Models;

use Carbon\Carbon;

class DateOfBirth
{
    public string $day;
    public string $month;
    public string $year;

    public function __construct(string $day = '', string $month = '', string $year = '')
    {
        $this->day   = $day;
        $this->month = $month;
        $this->year  = $year;
    }

    public function readString(string $date): self
    {
        $dt          = strtotime($date);
        $this->year  = date('Y', $dt);
        $this->month = date('m', $dt);
        $this->day   = date('d', $dt);

        return $this;
    }

    public function readCarbon(Carbon $date): self
    {
        $this->year  = $date->year;
        $this->month = $date->month;
        $this->day   = $date->day;

        return $this;
    }
}
