<?php

namespace Ratespecial\Equifax\Luminate\Models;

class KbaQuestionnaire
{
    public int $questionnaireId;
    /**
     * @var KbaQuestionGroup[]
     */
    public array $questionGroups;
    /** @var KbaQuestion[] */
    public array $questions;

    public function afterMapping(): void
    {
        // TODO.  The way the sandbox data is structured is there is only one question group.  So why bother saying what
        // questions that group applies to, if all questions will be in that group?
//        foreach ($this->questionGroups as $group) {
//            $group->
//        }
    }

    public function getQuestion(int $questionId): ?KbaQuestion
    {
        foreach ($this->questions as $question) {
            if ($question->questionId == $questionId) {
                return $question;
            }
        }

        return null;
    }
}
