<?php

namespace Ratespecial\Equifax\Luminate\Models;

/**
 * Questions from an Identity Assurance challenge
 */
class KbaQuestions
{
    /**
     * A system generated identifier that is returned in the response with the
     * question set. This value ties together the KBA question sets so is
     * required to be copied across from the response to the initial request.
     *
     * @var string
     */
    public string $authenticationId = '';

    public KbaQuestionnaire $questionnaire;
}
