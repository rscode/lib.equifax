<?php

namespace Ratespecial\Equifax\Luminate\Models;

class Identity
{
    public Name $name;
    /** @var \Ratespecial\Equifax\Luminate\Models\Address[] */
    public array $address = [];
    public DateOfBirth $dateOfBirth;
    public string $countryOfResidence = '';
    public ?Phone $phone = null;

    public function __construct()
    {
        $this->name        = new Name();
        $this->dateOfBirth = new DateOfBirth();
    }

    public function addAddress(Address $address): self
    {
        $this->address[] = $address;

        return $this;
    }
}
