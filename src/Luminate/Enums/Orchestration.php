<?php

namespace Ratespecial\Equifax\Luminate\Enums;

/**
 * Product orchestration, used as a segment of the URL in requests.
 */
enum Orchestration: string
{
    /**
     * Finds record of individual based on PII info
     */
    case IDENTITY = 'IdentityOrch';

    /**
     * Responds to KBA questions
     */
    case ASSURANCE = 'AssuranceOrch';

    case DIGITAL_TRUST = 'DigitalTrustOrch';
}
