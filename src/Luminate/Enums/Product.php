<?php

namespace Ratespecial\Equifax\Luminate\Enums;

enum Product: string
{
    /**
     * Finds record of individual based on PII info
     */
    case IDENTITY_VERIFICATION = 'IdentityVerification';

    /**
     * Responds to KBA questions
     */
    case IDENTITY_ASSURANCE = 'IdentityAssurance';

    /**
     * Sends back Iovation black box build from device properties
     */
    case DIGITAL_TRUST = 'DigitalTrust';
}
