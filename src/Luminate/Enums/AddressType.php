<?php

namespace Ratespecial\Equifax\Luminate\Enums;

enum AddressType: string
{
    case CURRENT = 'current';
    case FORMER = 'former';
}
