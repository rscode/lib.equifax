<?php

namespace Ratespecial\Equifax\Luminate\Enums;

/**
 * For use with "additional fields" in Device Trust requests.
 */
enum IovationType: string
{
    case ENROLLMENT = 'enrollment';
    case LOGIN = 'login';
}
