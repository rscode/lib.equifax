<?php

namespace Ratespecial\Equifax\Luminate\Enums;

/**
 * Values for Identity Verification / Assurance response field assessment.decision
 */
enum IdentityDecisionAssessment: string
{
    /**
     * Failed overall authentication
     */
    case FAIL = 'FAIL';

    /**
     * Passed overall authentication
     */
    case PASS = 'PASS';

    /**
     * This is a temporary status meaning that the KBA questions have been presented and that we are still awaiting a response
     */
    case CHALLENGE = 'CHALLENGE';

    /**
     * We have been unable to complete the full authentication due to a technical issue
     */
    case UNDECISIONED = 'UNDECISIONED';
}
