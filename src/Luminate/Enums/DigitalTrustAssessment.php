<?php

namespace Ratespecial\Equifax\Luminate\Enums;

/**
 * Values for Identity Verification / Assurance response field assessment.decision
 */
enum DigitalTrustAssessment: string
{
    /**
     * The risk of this device is above tolerance levels and should be rejected.
     */
    case HIGH = 'High';

    /**
     * The risk level of this device falls into the 'refer' threshold so we'd recommend carrying out further manual due diligence on this
     * individual before continuing any further with their application.
     */
    case MEDIUM = 'Medium';

    /**
     * There is little or no risk associated with this device so we're happy for this application to continue.
     */
    case LOW = 'Low';
}
