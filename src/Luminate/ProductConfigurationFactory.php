<?php

namespace Ratespecial\Equifax\Luminate;

use Illuminate\Contracts\Config\Repository as ConfigRepository;
use Ratespecial\Equifax\Common\EquifaxApiProduct;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Luminate\Models\ProductConfiguration;

/**
 * Reads Laravel config file for product configuration.
 */
class ProductConfigurationFactory
{
    public function __construct(private readonly ConfigRepository $config)
    {
    }

    public function get(EquifaxEnvironment $env): ProductConfiguration
    {
        $configKey = sprintf('equifax.%s.%s', EquifaxApiProduct::LUMINATE->value, $env->value);
        $config    = $this->config->get($configKey);

        return new ProductConfiguration(
            $config['tenant-id'],
            $config['application-id'],
            $config['config-id'],
            $config['client-organization'],
        );
    }
}
