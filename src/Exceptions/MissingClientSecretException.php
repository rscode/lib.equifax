<?php

namespace Ratespecial\Equifax\Exceptions;

use Exception;

/**
 * Client secret can't be pulled from CredentialsStoreInterface
 */
class MissingClientSecretException extends Exception
{
}
