<?php

namespace Ratespecial\Equifax\Exceptions;

use Exception;

class UnsupportedProductEnvironmentException extends Exception
{
}
