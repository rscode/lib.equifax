<?php

namespace Ratespecial\Equifax\Exceptions;

use Exception;

/**
 * Calls are made to services that require an AccessToken be present in the service.
 */
class MissingAccessTokenException extends Exception
{
}
