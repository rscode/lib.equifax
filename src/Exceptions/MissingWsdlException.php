<?php

namespace Ratespecial\Equifax\Exceptions;

use Exception;

class MissingWsdlException extends Exception
{
}
