<?php

namespace Ratespecial\Equifax\XMLConsumer\Security;

use Ratespecial\Equifax\Common\AccessToken;
use Ratespecial\Equifax\Common\EquifaxApiProduct;
use Ratespecial\Equifax\Common\TokenGenerationServiceInterface;
use Ratespecial\Equifax\XMLConsumer\Common\AbstractXmlConsumerService;
use Ratespecial\Equifax\XMLConsumer\Exceptions\XMLConsumerException;
use Ratespecial\Equifax\XMLConsumer\Security\StructType\ChangeclientsecretRequest;
use Ratespecial\Equifax\XMLConsumer\Security\StructType\ChangeclientsecretResponse;
use Ratespecial\Equifax\XMLConsumer\Security\StructType\LogonRequest;
use Ratespecial\Equifax\XMLConsumer\Security\StructType\LogonResponse;
use SoapFault;

class SecurityService extends AbstractXmlConsumerService implements TokenGenerationServiceInterface
{
    public function getServiceName(): EquifaxApiProduct
    {
        return EquifaxApiProduct::XML_CONSUMER;
    }

    /**
     * Generate an access token for use with other services, such as Consumer credit search or Address service.
     *
     * @param string $clientId
     * @param string $clientSecret
     * @return LogonResponse
     * @throws XMLConsumerException
     * @throws SoapFault
     */
    public function logon(string $clientId, string $clientSecret): LogonResponse
    {
        $request = new LogonRequest($clientId, $clientSecret);

        return $this->execute('logon', $request);
    }

    public function generateAccessToken(string $clientId, string $clientSecret): AccessToken
    {
        $resp = $this->logon($clientId, $clientSecret);

        return $resp->getAccessTokenObject();
    }

    /**
     * Generates a new client secret and returns it in the response object.  Be careful to record the value, otherwise you will have
     * to fetch it through the idAMS service.
     *
     * @param string $clientId     Current client ID
     * @param string $clientSecret Current client secret
     * @return ChangeclientsecretResponse
     * @throws XMLConsumerException
     * @throws SoapFault
     */
    public function changeClientSecret(string $clientId, string $clientSecret): ChangeclientsecretResponse
    {
        $request = new ChangeclientsecretRequest($clientId, $clientSecret);

        return $this->execute('changeclientsecret', $request);
    }
}
