<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Security;

/**
 * Class which returns the class map definition
 */
class ClassMap
{
    /**
     * Returns the mapping between the WSDL Structs and generated Structs' classes
     * This array is sent to the \SoapClient when calling the WS
     *
     * @return string[]
     */
    final public static function get(): array
    {
        return [
            'logonRequest'               => '\\Ratespecial\\Equifax\\XMLConsumer\\Security\\StructType\\LogonRequest',
            'logonResponse'              => '\\Ratespecial\\Equifax\\XMLConsumer\\Security\\StructType\\LogonResponse',
            'changeclientsecretRequest'  => '\\Ratespecial\\Equifax\\XMLConsumer\\Security\\StructType\\ChangeclientsecretRequest',
            'changeclientsecretResponse' => '\\Ratespecial\\Equifax\\XMLConsumer\\Security\\StructType\\ChangeclientsecretResponse',
            'pingRequest'                => '\\Ratespecial\\Equifax\\XMLConsumer\\Common\\PingRequest',
            'pingResponse'               => '\\Ratespecial\\Equifax\\XMLConsumer\\Common\\PingResponse',
            'serviceDetail'              => '\\Ratespecial\\Equifax\\XMLConsumer\\Common\\ServiceDetail',
            'EWSFault'                   => '\\Ratespecial\\Equifax\\XMLConsumer\\Common\\EWSFault',
        ];
    }
}
