<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Security\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for logonRequest StructType
 * Meta information extracted from the WSDL
 * - documentation: Logon request with supplied client id and client secret to get access token | Logon request.
 *
 * @subpackage Structs
 */
class LogonRequest extends AbstractStructBase
{
    /**
     * The clientId
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 64
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $clientId;
    /**
     * The clientSecret
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 512
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $clientSecret;

    /**
     * Constructor method for logonRequest
     *
     * @param string $clientId
     * @param string $clientSecret
     * @uses LogonRequest::setClientId()
     * @uses LogonRequest::setClientSecret()
     */
    public function __construct(string $clientId, string $clientSecret)
    {
        $this
            ->setClientId($clientId)
            ->setClientSecret($clientSecret);
    }

    /**
     * Get clientId value
     *
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * Set clientId value
     *
     * @param string $clientId
     * @return LogonRequest
     */
    public function setClientId(string $clientId): self
    {
        // validation for constraint: string
        if (!is_null($clientId) && !is_string($clientId)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($clientId, true),
                gettype($clientId)
            ), __LINE__);
        }
        // validation for constraint: maxLength(64)
        if (!is_null($clientId) && mb_strlen((string)$clientId) > 64) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 64',
                mb_strlen((string)$clientId)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($clientId) && mb_strlen((string)$clientId) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$clientId)
            ), __LINE__);
        }
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * Get clientSecret value
     *
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    /**
     * Set clientSecret value
     *
     * @param string $clientSecret
     * @return LogonRequest
     */
    public function setClientSecret(string $clientSecret): self
    {
        // validation for constraint: string
        if (!is_null($clientSecret) && !is_string($clientSecret)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($clientSecret, true),
                gettype($clientSecret)
            ), __LINE__);
        }
        // validation for constraint: maxLength(512)
        if (!is_null($clientSecret) && mb_strlen((string)$clientSecret) > 512) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 512',
                mb_strlen((string)$clientSecret)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($clientSecret) && mb_strlen((string)$clientSecret) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$clientSecret)
            ), __LINE__);
        }
        $this->clientSecret = $clientSecret;

        return $this;
    }
}
