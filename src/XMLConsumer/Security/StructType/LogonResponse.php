<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Security\StructType;

use Carbon\Carbon;
use InvalidArgumentException;
use Namshi\JOSE\JWS;
use Ratespecial\Equifax\Common\AccessToken;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for logonResponse StructType
 * Meta information extracted from the WSDL
 * - documentation: Logon Response return access token.
 *
 * @subpackage Structs
 */
class LogonResponse extends AbstractStructBase
{
    /**
     * The accesstoken
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 1024
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $accesstoken;

    public function __construct(string $accesstoken)
    {
        $this
            ->setAccesstoken($accesstoken);
    }

    /**
     * Get accesstoken value
     *
     * @return string
     */
    public function getAccesstoken(): string
    {
        return $this->accesstoken;
    }

    public function getAccessTokenObject(): AccessToken
    {
        return AccessToken::createFromJWT($this->accesstoken);
    }

    /**
     * Set accesstoken value
     *
     * @param string $accesstoken
     * @return LogonResponse
     */
    public function setAccesstoken(string $accesstoken): self
    {
        // validation for constraint: string
        if (!is_null($accesstoken) && !is_string($accesstoken)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($accesstoken, true),
                gettype($accesstoken)
            ), __LINE__);
        }
        // validation for constraint: maxLength(1024)
        if (!is_null($accesstoken) && mb_strlen((string)$accesstoken) > 1024) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 1024',
                mb_strlen((string)$accesstoken)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($accesstoken) && mb_strlen((string)$accesstoken) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$accesstoken)
            ), __LINE__);
        }
        $this->accesstoken = $accesstoken;

        return $this;
    }
}
