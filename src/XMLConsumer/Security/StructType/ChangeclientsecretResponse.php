<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Security\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for changeclientsecretResponse StructType
 * Meta information extracted from the WSDL
 * - documentation: Change Client secret return new secret.
 *
 * @subpackage Structs
 */
class ChangeclientsecretResponse extends AbstractStructBase
{
    /**
     * The clientSecret
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 512
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $clientSecret;

    /**
     * Constructor method for changeclientsecretResponse
     *
     * @param string $clientSecret
     * @uses ChangeclientsecretResponse::setClientSecret()
     */
    public function __construct(string $clientSecret)
    {
        $this
            ->setClientSecret($clientSecret);
    }

    /**
     * Get clientSecret value
     *
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    /**
     * Set clientSecret value
     *
     * @param string $clientSecret
     * @return ChangeclientsecretResponse
     */
    public function setClientSecret(string $clientSecret): self
    {
        // validation for constraint: string
        if (!is_null($clientSecret) && !is_string($clientSecret)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($clientSecret, true),
                gettype($clientSecret)
            ), __LINE__);
        }
        // validation for constraint: maxLength(512)
        if (!is_null($clientSecret) && mb_strlen((string)$clientSecret) > 512) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 512',
                mb_strlen((string)$clientSecret)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($clientSecret) && mb_strlen((string)$clientSecret) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$clientSecret)
            ), __LINE__);
        }
        $this->clientSecret = $clientSecret;

        return $this;
    }
}
