<?php

namespace Ratespecial\Equifax\XMLConsumer\Security\Credentials;

use Carbon\Carbon;
use Ratespecial\Equifax\XMLConsumer\Exceptions\MissingCredentialsException;
use Ratespecial\Equifax\XMLConsumer\Exceptions\XMLConsumerException;
use Ratespecial\Equifax\XMLConsumer\Security\SecurityService;
use SoapFault;

/**
 * Equifax requires that the client secret be rotated regularly.  This service will accomplish that.
 */
class ClientSecretCyclerService
{
    protected ?Credentials $lastCheckedCredentials = null;

    public function __construct(
        protected readonly SecurityService $securityService,
        protected readonly CredentialsStoreInterface $store,
        public int $maxAgeInDays = 27,
    ) {
    }

    /**
     * @return bool  True if the client secret was refreshed, false if not.
     * @throws XMLConsumerException
     * @throws MissingCredentialsException
     * @throws SoapFault
     */
    public function refreshIfNecessary(?int $maxAgeInDays = null): bool
    {
        if (is_null($maxAgeInDays)) {
            $maxAgeInDays = $this->maxAgeInDays;
        }

        $secret = $this->store->load($this->securityService->getEnvironment());

        if (!$secret) {
            throw new MissingCredentialsException("No credentials found when refreshing client secret.");
        }

        $this->lastCheckedCredentials = $secret;

        if ($secret->generatedAt->clone()->addDays($maxAgeInDays)->isFuture()) {
            return false;
        }

        $changeResponse = $this->securityService->changeClientSecret($secret->clientId, $secret->clientSecret);
        $newCreds       = new Credentials($secret->clientId, $changeResponse->getClientSecret(), Carbon::now('UTC'));
        $this->store->save($newCreds, $this->securityService->getEnvironment());

        return true;
    }

    /**
     * After refreshIfNecessary() has run, this will be what credential was examined.
     */
    public function getLastCheckedCredentials(): ?Credentials
    {
        return $this->lastCheckedCredentials;
    }
}
