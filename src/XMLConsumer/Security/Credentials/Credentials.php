<?php

namespace Ratespecial\Equifax\XMLConsumer\Security\Credentials;

use Carbon\Carbon;

class Credentials
{
    public function __construct(public readonly string $clientId, public readonly string $clientSecret, public readonly Carbon $generatedAt)
    {
    }
}
