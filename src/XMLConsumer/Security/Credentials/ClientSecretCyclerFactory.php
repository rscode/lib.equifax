<?php

namespace Ratespecial\Equifax\XMLConsumer\Security\Credentials;

use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Exceptions\UnsupportedProductEnvironmentException;
use Ratespecial\Equifax\Laravel\EquifaxXmlConsumerServiceProvider;

class ClientSecretCyclerFactory
{
    public function __construct(protected readonly Container $app)
    {
    }

    /**
     * @throws UnsupportedProductEnvironmentException
     * @throws BindingResolutionException
     */
    public function get(EquifaxEnvironment $env, ?int $maxAgeInDays = null): ClientSecretCyclerService
    {
        $serviceTag = match ($env) {
            EquifaxEnvironment::PRODUCTION => EquifaxXmlConsumerServiceProvider::SECURITY_LIVE,
            EquifaxEnvironment::UAT => EquifaxXmlConsumerServiceProvider::SECURITY_UAT,
            default => throw new UnsupportedProductEnvironmentException("Unsupported environment [{$env->name}]"),
        };

        $securityService = $this->app->make($serviceTag);

        $with = [
            'securityService' => $securityService,
        ];

        // If null is supplied, we'll let ClientSecretCyclerService use its default value.
        if (!is_null($maxAgeInDays)) {
            $with['maxAgeInDays'] = $maxAgeInDays;
        }

        return $this->app->makeWith(ClientSecretCyclerService::class, $with);
    }
}
