<?php

namespace Ratespecial\Equifax\XMLConsumer\Security\Credentials;

use Ratespecial\Equifax\Common\EquifaxEnvironment;

interface CredentialsStoreInterface
{
    public function save(Credentials $credentials, EquifaxEnvironment $environment): void;

    public function load(EquifaxEnvironment $environment): ?Credentials;
}
