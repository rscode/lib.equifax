<?php

namespace Ratespecial\Equifax\XMLConsumer\Exceptions;

use Exception;

/**
 * Trying to use services that expect Credentials (client id & client secret) to be available.
 */
class MissingCredentialsException extends Exception
{
}
