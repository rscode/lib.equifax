<?php

namespace Ratespecial\Equifax\XMLConsumer\Exceptions;

use Exception;
use Ratespecial\Equifax\XMLConsumer\Common\EWSFault;

class XMLConsumerException extends Exception
{
    public ?EWSFault $fault = null;
}
