<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for Sex EnumType
 *
 * @subpackage Enumerations
 */
class Sex extends AbstractStructEnumBase
{
    /**
     * Constant for value 'female'
     *
     * @return string 'female'
     */
    public const VALUE_FEMALE = 'female';
    /**
     * Constant for value 'male'
     *
     * @return string 'male'
     */
    public const VALUE_MALE = 'male';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_MALE
     * @uses self::VALUE_FEMALE
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_FEMALE,
            self::VALUE_MALE,
        ];
    }
}
