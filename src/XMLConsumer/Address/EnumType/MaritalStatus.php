<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for MaritalStatus EnumType
 *
 * @subpackage Enumerations
 */
class MaritalStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'single'
     *
     * @return string 'single'
     */
    public const VALUE_SINGLE = 'single';
    /**
     * Constant for value 'married'
     *
     * @return string 'married'
     */
    public const VALUE_MARRIED = 'married';
    /**
     * Constant for value 'cohabiting'
     *
     * @return string 'cohabiting'
     */
    public const VALUE_COHABITING = 'cohabiting';
    /**
     * Constant for value 'divorced'
     *
     * @return string 'divorced'
     */
    public const VALUE_DIVORCED = 'divorced';
    /**
     * Constant for value 'separated'
     *
     * @return string 'separated'
     */
    public const VALUE_SEPARATED = 'separated';
    /**
     * Constant for value 'widowed'
     *
     * @return string 'widowed'
     */
    public const VALUE_WIDOWED = 'widowed';
    /**
     * Constant for value 'other'
     *
     * @return string 'other'
     */
    public const VALUE_OTHER = 'other';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_MARRIED
     * @uses self::VALUE_COHABITING
     * @uses self::VALUE_DIVORCED
     * @uses self::VALUE_SEPARATED
     * @uses self::VALUE_WIDOWED
     * @uses self::VALUE_OTHER
     * @uses self::VALUE_SINGLE
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_SINGLE,
            self::VALUE_MARRIED,
            self::VALUE_COHABITING,
            self::VALUE_DIVORCED,
            self::VALUE_SEPARATED,
            self::VALUE_WIDOWED,
            self::VALUE_OTHER,
        ];
    }
}
