<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for CarOwnedStatus EnumType
 *
 * @subpackage Enumerations
 */
class CarOwnedStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'No vehicle owned'
     *
     * @return string 'No vehicle owned'
     */
    public const VALUE_NO_VEHICLE_OWNED = 'No vehicle owned';
    /**
     * Constant for value 'Owned but under finance'
     *
     * @return string 'Owned but under finance'
     */
    public const VALUE_OWNED_BUT_UNDER_FINANCE = 'Owned but under finance';
    /**
     * Constant for value 'Wholly owned'
     *
     * @return string 'Wholly owned'
     */
    public const VALUE_WHOLLY_OWNED = 'Wholly owned';
    /**
     * Constant for value 'Other'
     *
     * @return string 'Other'
     */
    public const VALUE_OTHER = 'Other';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_OWNED_BUT_UNDER_FINANCE
     * @uses self::VALUE_WHOLLY_OWNED
     * @uses self::VALUE_OTHER
     * @uses self::VALUE_NO_VEHICLE_OWNED
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_NO_VEHICLE_OWNED,
            self::VALUE_OWNED_BUT_UNDER_FINANCE,
            self::VALUE_WHOLLY_OWNED,
            self::VALUE_OTHER,
        ];
    }
}
