<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for ResidenceType EnumType
 *
 * @subpackage Enumerations
 */
class ResidenceType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'detached'
     *
     * @return string 'detached'
     */
    public const VALUE_DETACHED = 'detached';
    /**
     * Constant for value 'bungalow'
     *
     * @return string 'bungalow'
     */
    public const VALUE_BUNGALOW = 'bungalow';
    /**
     * Constant for value 'flat'
     *
     * @return string 'flat'
     */
    public const VALUE_FLAT = 'flat';
    /**
     * Constant for value 'semi-detached'
     *
     * @return string 'semi-detached'
     */
    public const VALUE_SEMI_DETACHED = 'semi-detached';
    /**
     * Constant for value 'terraced'
     *
     * @return string 'terraced'
     */
    public const VALUE_TERRACED = 'terraced';
    /**
     * Constant for value 'other'
     *
     * @return string 'other'
     */
    public const VALUE_OTHER = 'other';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_BUNGALOW
     * @uses self::VALUE_FLAT
     * @uses self::VALUE_SEMI_DETACHED
     * @uses self::VALUE_TERRACED
     * @uses self::VALUE_OTHER
     * @uses self::VALUE_DETACHED
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_DETACHED,
            self::VALUE_BUNGALOW,
            self::VALUE_FLAT,
            self::VALUE_SEMI_DETACHED,
            self::VALUE_TERRACED,
            self::VALUE_OTHER,
        ];
    }
}
