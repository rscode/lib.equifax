<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for TelephoneNumberUsageType EnumType
 *
 * @subpackage Enumerations
 */
class TelephoneNumberUsageType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'personal'
     *
     * @return string 'personal'
     */
    public const VALUE_PERSONAL = 'personal';
    /**
     * Constant for value 'business'
     *
     * @return string 'business'
     */
    public const VALUE_BUSINESS = 'business';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_BUSINESS
     * @uses self::VALUE_PERSONAL
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_PERSONAL,
            self::VALUE_BUSINESS,
        ];
    }
}
