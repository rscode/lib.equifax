<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CurrentEmployment StructType
 *
 * @subpackage Structs
 */
class CurrentEmployment extends AbstractStructBase
{
    /**
     * The EmploymentStatus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $EmploymentStatus = null;
    /**
     * The TimeInCurrentEmployment
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $TimeInCurrentEmployment = null;
    /**
     * The SupplementaryEmploymentDetail
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $SupplementaryEmploymentDetail = null;
    /**
     * The EmployerIndustryCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $EmployerIndustryCode = null;
    /**
     * The EmployerIndustryDescription
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $EmployerIndustryDescription = null;

    /**
     * Constructor method for CurrentEmployment
     *
     * @param string $employmentStatus
     * @param string $timeInCurrentEmployment
     * @param string $supplementaryEmploymentDetail
     * @param string $employerIndustryCode
     * @param string $employerIndustryDescription
     * @uses CurrentEmployment::setEmploymentStatus()
     * @uses CurrentEmployment::setTimeInCurrentEmployment()
     * @uses CurrentEmployment::setSupplementaryEmploymentDetail()
     * @uses CurrentEmployment::setEmployerIndustryCode()
     * @uses CurrentEmployment::setEmployerIndustryDescription()
     */
    public function __construct(
        ?string $employmentStatus = null,
        ?string $timeInCurrentEmployment = null,
        ?string $supplementaryEmploymentDetail = null,
        ?string $employerIndustryCode = null,
        ?string $employerIndustryDescription = null
    ) {
        $this
            ->setEmploymentStatus($employmentStatus)
            ->setTimeInCurrentEmployment($timeInCurrentEmployment)
            ->setSupplementaryEmploymentDetail($supplementaryEmploymentDetail)
            ->setEmployerIndustryCode($employerIndustryCode)
            ->setEmployerIndustryDescription($employerIndustryDescription);
    }

    /**
     * Get EmploymentStatus value
     *
     * @return string|null
     */
    public function getEmploymentStatus(): ?string
    {
        return $this->EmploymentStatus;
    }

    /**
     * Set EmploymentStatus value
     *
     * @param string $employmentStatus
     * @return CurrentEmployment
     */
    public function setEmploymentStatus(?string $employmentStatus = null): self
    {
        // validation for constraint: string
        if (!is_null($employmentStatus) && !is_string($employmentStatus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($employmentStatus, true),
                gettype($employmentStatus)
            ), __LINE__);
        }
        $this->EmploymentStatus = $employmentStatus;

        return $this;
    }

    /**
     * Get TimeInCurrentEmployment value
     *
     * @return string|null
     */
    public function getTimeInCurrentEmployment(): ?string
    {
        return $this->TimeInCurrentEmployment;
    }

    /**
     * Set TimeInCurrentEmployment value
     *
     * @param string $timeInCurrentEmployment
     * @return CurrentEmployment
     */
    public function setTimeInCurrentEmployment(?string $timeInCurrentEmployment = null): self
    {
        // validation for constraint: string
        if (!is_null($timeInCurrentEmployment) && !is_string($timeInCurrentEmployment)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($timeInCurrentEmployment, true),
                gettype($timeInCurrentEmployment)
            ), __LINE__);
        }
        $this->TimeInCurrentEmployment = $timeInCurrentEmployment;

        return $this;
    }

    /**
     * Get SupplementaryEmploymentDetail value
     *
     * @return string|null
     */
    public function getSupplementaryEmploymentDetail(): ?string
    {
        return $this->SupplementaryEmploymentDetail;
    }

    /**
     * Set SupplementaryEmploymentDetail value
     *
     * @param string $supplementaryEmploymentDetail
     * @return CurrentEmployment
     */
    public function setSupplementaryEmploymentDetail(?string $supplementaryEmploymentDetail = null): self
    {
        // validation for constraint: string
        if (!is_null($supplementaryEmploymentDetail) && !is_string($supplementaryEmploymentDetail)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($supplementaryEmploymentDetail, true),
                gettype($supplementaryEmploymentDetail)
            ), __LINE__);
        }
        $this->SupplementaryEmploymentDetail = $supplementaryEmploymentDetail;

        return $this;
    }

    /**
     * Get EmployerIndustryCode value
     *
     * @return string|null
     */
    public function getEmployerIndustryCode(): ?string
    {
        return $this->EmployerIndustryCode;
    }

    /**
     * Set EmployerIndustryCode value
     *
     * @param string $employerIndustryCode
     * @return CurrentEmployment
     */
    public function setEmployerIndustryCode(?string $employerIndustryCode = null): self
    {
        // validation for constraint: string
        if (!is_null($employerIndustryCode) && !is_string($employerIndustryCode)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($employerIndustryCode, true),
                gettype($employerIndustryCode)
            ), __LINE__);
        }
        $this->EmployerIndustryCode = $employerIndustryCode;

        return $this;
    }

    /**
     * Get EmployerIndustryDescription value
     *
     * @return string|null
     */
    public function getEmployerIndustryDescription(): ?string
    {
        return $this->EmployerIndustryDescription;
    }

    /**
     * Set EmployerIndustryDescription value
     *
     * @param string $employerIndustryDescription
     * @return CurrentEmployment
     */
    public function setEmployerIndustryDescription(?string $employerIndustryDescription = null): self
    {
        // validation for constraint: string
        if (!is_null($employerIndustryDescription) && !is_string($employerIndustryDescription)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($employerIndustryDescription, true),
                gettype($employerIndustryDescription)
            ), __LINE__);
        }
        $this->EmployerIndustryDescription = $employerIndustryDescription;

        return $this;
    }
}
