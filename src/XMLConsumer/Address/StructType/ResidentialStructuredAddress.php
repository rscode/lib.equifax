<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Address\EnumType\ResidenceType;

/**
 * This class stands for ResidentialStructuredAddress StructType
 *
 * @subpackage Structs
 */
class ResidentialStructuredAddress extends StructuredAddress
{
    /**
     * The residenceType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $residenceType = null;

    /**
     * Constructor method for ResidentialStructuredAddress
     *
     * @param string $residenceType
     * @uses ResidentialStructuredAddress::setResidenceType()
     */
    public function __construct(?string $residenceType = null)
    {
        $this
            ->setResidenceType($residenceType);
    }

    /**
     * Get residenceType value
     *
     * @return string|null
     */
    public function getResidenceType(): ?string
    {
        return $this->residenceType;
    }

    /**
     * Set residenceType value
     *
     * @param string $residenceType
     * @return ResidentialStructuredAddress
     * @throws InvalidArgumentException
     * @uses ResidenceType::getValidValues
     * @uses ResidenceType::valueIsValid
     */
    public function setResidenceType(?string $residenceType = null): self
    {
        // validation for constraint: enumeration
        if (!ResidenceType::valueIsValid($residenceType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Address\EnumType\ResidenceType',
                is_array($residenceType) ? implode(', ', $residenceType) : var_export($residenceType, true),
                implode(', ', ResidenceType::getValidValues())
            ), __LINE__);
        }
        $this->residenceType = $residenceType;

        return $this;
    }
}
