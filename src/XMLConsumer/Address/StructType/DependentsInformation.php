<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DependentsInformation StructType
 *
 * @subpackage Structs
 */
class DependentsInformation extends AbstractStructBase
{
    /**
     * The NumberOfDependents
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $NumberOfDependents = null;
    /**
     * The NumberOfDependents01
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $NumberOfDependents01 = null;
    /**
     * The NumberOfDependents24
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $NumberOfDependents24 = null;
    /**
     * The NumberOfDependents510
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $NumberOfDependents510 = null;
    /**
     * The NumberOfDependents1116
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $NumberOfDependents1116 = null;
    /**
     * The NumberOfDependents17
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $NumberOfDependents17 = null;

    /**
     * Constructor method for DependentsInformation
     *
     * @param string $numberOfDependents
     * @param string $numberOfDependents01
     * @param string $numberOfDependents24
     * @param string $numberOfDependents510
     * @param string $numberOfDependents1116
     * @param string $numberOfDependents17
     * @uses DependentsInformation::setNumberOfDependents()
     * @uses DependentsInformation::setNumberOfDependents01()
     * @uses DependentsInformation::setNumberOfDependents24()
     * @uses DependentsInformation::setNumberOfDependents510()
     * @uses DependentsInformation::setNumberOfDependents1116()
     * @uses DependentsInformation::setNumberOfDependents17()
     */
    public function __construct(
        ?string $numberOfDependents = null,
        ?string $numberOfDependents01 = null,
        ?string $numberOfDependents24 = null,
        ?string $numberOfDependents510 = null,
        ?string $numberOfDependents1116 = null,
        ?string $numberOfDependents17 = null
    ) {
        $this
            ->setNumberOfDependents($numberOfDependents)
            ->setNumberOfDependents01($numberOfDependents01)
            ->setNumberOfDependents24($numberOfDependents24)
            ->setNumberOfDependents510($numberOfDependents510)
            ->setNumberOfDependents1116($numberOfDependents1116)
            ->setNumberOfDependents17($numberOfDependents17);
    }

    /**
     * Get NumberOfDependents value
     *
     * @return string|null
     */
    public function getNumberOfDependents(): ?string
    {
        return $this->NumberOfDependents;
    }

    /**
     * Set NumberOfDependents value
     *
     * @param string $numberOfDependents
     * @return DependentsInformation
     */
    public function setNumberOfDependents(?string $numberOfDependents = null): self
    {
        // validation for constraint: string
        if (!is_null($numberOfDependents) && !is_string($numberOfDependents)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($numberOfDependents, true),
                gettype($numberOfDependents)
            ), __LINE__);
        }
        $this->NumberOfDependents = $numberOfDependents;

        return $this;
    }

    /**
     * Get NumberOfDependents01 value
     *
     * @return string|null
     */
    public function getNumberOfDependents01(): ?string
    {
        return $this->NumberOfDependents01;
    }

    /**
     * Set NumberOfDependents01 value
     *
     * @param string $numberOfDependents01
     * @return DependentsInformation
     */
    public function setNumberOfDependents01(?string $numberOfDependents01 = null): self
    {
        // validation for constraint: string
        if (!is_null($numberOfDependents01) && !is_string($numberOfDependents01)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($numberOfDependents01, true),
                gettype($numberOfDependents01)
            ), __LINE__);
        }
        $this->NumberOfDependents01 = $numberOfDependents01;

        return $this;
    }

    /**
     * Get NumberOfDependents24 value
     *
     * @return string|null
     */
    public function getNumberOfDependents24(): ?string
    {
        return $this->NumberOfDependents24;
    }

    /**
     * Set NumberOfDependents24 value
     *
     * @param string $numberOfDependents24
     * @return DependentsInformation
     */
    public function setNumberOfDependents24(?string $numberOfDependents24 = null): self
    {
        // validation for constraint: string
        if (!is_null($numberOfDependents24) && !is_string($numberOfDependents24)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($numberOfDependents24, true),
                gettype($numberOfDependents24)
            ), __LINE__);
        }
        $this->NumberOfDependents24 = $numberOfDependents24;

        return $this;
    }

    /**
     * Get NumberOfDependents510 value
     *
     * @return string|null
     */
    public function getNumberOfDependents510(): ?string
    {
        return $this->NumberOfDependents510;
    }

    /**
     * Set NumberOfDependents510 value
     *
     * @param string $numberOfDependents510
     * @return DependentsInformation
     */
    public function setNumberOfDependents510(?string $numberOfDependents510 = null): self
    {
        // validation for constraint: string
        if (!is_null($numberOfDependents510) && !is_string($numberOfDependents510)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($numberOfDependents510, true),
                gettype($numberOfDependents510)
            ), __LINE__);
        }
        $this->NumberOfDependents510 = $numberOfDependents510;

        return $this;
    }

    /**
     * Get NumberOfDependents1116 value
     *
     * @return string|null
     */
    public function getNumberOfDependents1116(): ?string
    {
        return $this->NumberOfDependents1116;
    }

    /**
     * Set NumberOfDependents1116 value
     *
     * @param string $numberOfDependents1116
     * @return DependentsInformation
     */
    public function setNumberOfDependents1116(?string $numberOfDependents1116 = null): self
    {
        // validation for constraint: string
        if (!is_null($numberOfDependents1116) && !is_string($numberOfDependents1116)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($numberOfDependents1116, true),
                gettype($numberOfDependents1116)
            ), __LINE__);
        }
        $this->NumberOfDependents1116 = $numberOfDependents1116;

        return $this;
    }

    /**
     * Get NumberOfDependents17 value
     *
     * @return string|null
     */
    public function getNumberOfDependents17(): ?string
    {
        return $this->NumberOfDependents17;
    }

    /**
     * Set NumberOfDependents17 value
     *
     * @param string $numberOfDependents17
     * @return DependentsInformation
     */
    public function setNumberOfDependents17(?string $numberOfDependents17 = null): self
    {
        // validation for constraint: string
        if (!is_null($numberOfDependents17) && !is_string($numberOfDependents17)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($numberOfDependents17, true),
                gettype($numberOfDependents17)
            ), __LINE__);
        }
        $this->NumberOfDependents17 = $numberOfDependents17;

        return $this;
    }
}
