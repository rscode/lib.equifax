<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;

/**
 * This class stands for MultipleMatch StructType
 * Meta information extracted from the WSDL
 * - documentation: Several matching addresses were found. In this case several 'return address' elements will be returned, subject to the maximum. Each address will contain a full address with PTCAbs code.
 *
 * @subpackage Structs
 */
class MultipleMatch extends MatchBase
{
    /**
     * The matchDetail
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 1
     *
     * @var MatchDetail[]
     */
    protected array $matchDetail;

    /**
     * Constructor method for MultipleMatch
     *
     * @param MatchDetail[] $matchDetail
     * @uses MultipleMatch::setMatchDetail()
     */
    public function __construct(array $matchDetail)
    {
        $this
            ->setMatchDetail($matchDetail);
    }

    /**
     * This method is responsible for validating the values passed to the setMatchDetail method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMatchDetail method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMatchDetailForArrayConstraintsFromSetMatchDetail(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $multipleMatchMatchDetailItem) {
            // validation for constraint: itemType
            if (!$multipleMatchMatchDetailItem instanceof MatchDetail) {
                $invalidValues[] = is_object($multipleMatchMatchDetailItem) ? get_class($multipleMatchMatchDetailItem) : sprintf(
                    '%s(%s)',
                    gettype($multipleMatchMatchDetailItem),
                    var_export($multipleMatchMatchDetailItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The matchDetail property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Address\StructType\MatchDetail, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get matchDetail value
     *
     * @return MatchDetail[]
     */
    public function getMatchDetail(): array
    {
        return $this->matchDetail;
    }

    /**
     * Set matchDetail value
     *
     * @param MatchDetail[] $matchDetail
     * @return MultipleMatch
     * @throws InvalidArgumentException
     */
    public function setMatchDetail(array $matchDetail): self
    {
        // validation for constraint: array
        if ('' !== ($matchDetailArrayErrorMessage = self::validateMatchDetailForArrayConstraintsFromSetMatchDetail($matchDetail))) {
            throw new InvalidArgumentException($matchDetailArrayErrorMessage, __LINE__);
        }
        $this->matchDetail = $matchDetail;

        return $this;
    }

    /**
     * Add item to matchDetail value
     *
     * @param MatchDetail $item
     * @return MultipleMatch
     * @throws InvalidArgumentException
     */
    public function addToMatchDetail(MatchDetail $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof MatchDetail) {
            throw new InvalidArgumentException(sprintf(
                'The matchDetail property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Address\StructType\MatchDetail, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        $this->matchDetail[] = $item;

        return $this;
    }
}
