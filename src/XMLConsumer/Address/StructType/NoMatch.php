<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

/**
 * This class stands for NoMatch StructType
 * Meta information extracted from the WSDL
 * - documentation: No matching addresses were found. In this case there will only be one 'return address' element.
 *
 * @subpackage Structs
 */
class NoMatch extends MatchBase
{
    /**
     * The name
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var Name|null
     */
    protected ?Name $name = null;
    /**
     * The freeFormatAddress
     *
     * @var FreeFormatAddress|null
     */
    protected ?FreeFormatAddress $freeFormatAddress = null;
    /**
     * The structuredAddress
     *
     * @var StructuredAddress|null
     */
    protected ?StructuredAddress $structuredAddress = null;

    /**
     * Constructor method for NoMatch
     *
     * @param Name              $name
     * @param FreeFormatAddress $freeFormatAddress
     * @param StructuredAddress $structuredAddress
     * @uses NoMatch::setName()
     * @uses NoMatch::setFreeFormatAddress()
     * @uses NoMatch::setStructuredAddress()
     */
    public function __construct(
        ?Name $name = null,
        ?FreeFormatAddress $freeFormatAddress = null,
        ?StructuredAddress $structuredAddress = null
    ) {
        $this
            ->setName($name)
            ->setFreeFormatAddress($freeFormatAddress)
            ->setStructuredAddress($structuredAddress);
    }

    /**
     * Get name value
     *
     * @return Name|null
     */
    public function getName(): ?Name
    {
        return $this->name;
    }

    /**
     * Set name value
     *
     * @param Name $name
     * @return NoMatch
     */
    public function setName(?Name $name = null): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get freeFormatAddress value
     *
     * @return FreeFormatAddress|null
     */
    public function getFreeFormatAddress(): ?FreeFormatAddress
    {
        return $this->freeFormatAddress;
    }

    /**
     * Set freeFormatAddress value
     *
     * @param FreeFormatAddress $freeFormatAddress
     * @return NoMatch
     */
    public function setFreeFormatAddress(?FreeFormatAddress $freeFormatAddress = null): self
    {
        $this->freeFormatAddress = $freeFormatAddress;

        return $this;
    }

    /**
     * Get structuredAddress value
     *
     * @return StructuredAddress|null
     */
    public function getStructuredAddress(): ?StructuredAddress
    {
        return $this->structuredAddress;
    }

    /**
     * Set structuredAddress value
     *
     * @param StructuredAddress $structuredAddress
     * @return NoMatch
     */
    public function setStructuredAddress(?StructuredAddress $structuredAddress = null): self
    {
        $this->structuredAddress = $structuredAddress;

        return $this;
    }
}
