<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PosttownHelp StructType
 * Meta information extracted from the WSDL
 * - documentation: Town help data returned. No matching addresses were found but close matches were found for the post town. The return addresses will contain values for post town and county.
 *
 * @subpackage Structs
 */
class PosttownHelp extends AbstractStructBase
{
    /**
     * The county
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 18
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $county;
    /**
     * The posttown
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 28
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $posttown;

    /**
     * Constructor method for PosttownHelp
     *
     * @param string $county
     * @param string $posttown
     * @uses PosttownHelp::setCounty()
     * @uses PosttownHelp::setPosttown()
     */
    public function __construct(string $county, string $posttown)
    {
        $this
            ->setCounty($county)
            ->setPosttown($posttown);
    }

    /**
     * Get county value
     *
     * @return string
     */
    public function getCounty(): string
    {
        return $this->county;
    }

    /**
     * Set county value
     *
     * @param string $county
     * @return PosttownHelp
     */
    public function setCounty(string $county): self
    {
        // validation for constraint: string
        if (!is_null($county) && !is_string($county)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($county, true),
                gettype($county)
            ), __LINE__);
        }
        // validation for constraint: maxLength(18)
        if (!is_null($county) && mb_strlen((string)$county) > 18) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 18',
                mb_strlen((string)$county)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($county) && mb_strlen((string)$county) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$county)
            ), __LINE__);
        }
        $this->county = $county;

        return $this;
    }

    /**
     * Get posttown value
     *
     * @return string
     */
    public function getPosttown(): string
    {
        return $this->posttown;
    }

    /**
     * Set posttown value
     *
     * @param string $posttown
     * @return PosttownHelp
     */
    public function setPosttown(string $posttown): self
    {
        // validation for constraint: string
        if (!is_null($posttown) && !is_string($posttown)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($posttown, true),
                gettype($posttown)
            ), __LINE__);
        }
        // validation for constraint: maxLength(28)
        if (!is_null($posttown) && mb_strlen((string)$posttown) > 28) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 28',
                mb_strlen((string)$posttown)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($posttown) && mb_strlen((string)$posttown) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$posttown)
            ), __LINE__);
        }
        $this->posttown = $posttown;

        return $this;
    }
}
