<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for listAddressByPostcodeRequest StructType
 *
 * @subpackage Structs
 */
class ListAddressByPostcodeRequest extends AbstractStructBase
{
    /**
     * The clientRef
     * Meta information extracted from the WSDL
     * - documentation: Used to supply an enquiry reference identifier.
     * - base: xs:string
     * - maxLength: 40
     * - maxOccurs: 1
     * - minLength: 1
     * - minOccurs: 1
     * - pattern: .*[^\s].*
     *
     * @var string
     */
    protected string $clientRef;
    /**
     * The postcode
     * Meta information extracted from the WSDL
     * - documentation: Short form regular expression to validate postcodes according to BS7666
     * - base: xs:string
     * - maxLength: 9
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $postcode;

    /**
     * Constructor method for listAddressByPostcodeRequest
     *
     * @param string $clientRef
     * @param string $postcode
     * @uses ListAddressByPostcodeRequest::setClientRef()
     * @uses ListAddressByPostcodeRequest::setPostcode()
     */
    public function __construct(string $clientRef, string $postcode)
    {
        $this
            ->setClientRef($clientRef)
            ->setPostcode($postcode);
    }

    /**
     * Get clientRef value
     *
     * @return string
     */
    public function getClientRef(): string
    {
        return $this->clientRef;
    }

    /**
     * Set clientRef value
     *
     * @param string $clientRef
     * @return ListAddressByPostcodeRequest
     */
    public function setClientRef(string $clientRef): self
    {
        // validation for constraint: string
        if (!is_null($clientRef) && !is_string($clientRef)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($clientRef, true),
                gettype($clientRef)
            ), __LINE__);
        }
        // validation for constraint: maxLength(40)
        if (!is_null($clientRef) && mb_strlen((string)$clientRef) > 40) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 40',
                mb_strlen((string)$clientRef)
            ), __LINE__);
        }
        // validation for constraint: minLength(1)
        if (!is_null($clientRef) && mb_strlen((string)$clientRef) < 1) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 1',
                mb_strlen((string)$clientRef)
            ), __LINE__);
        }
        // validation for constraint: pattern(.*[^\s].*)
        if (!is_null($clientRef) && !preg_match('/.*[^\\s].*/', $clientRef)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /.*[^\\s].*/',
                var_export($clientRef, true)
            ), __LINE__);
        }
        $this->clientRef = $clientRef;

        return $this;
    }

    /**
     * Get postcode value
     *
     * @return string
     */
    public function getPostcode(): string
    {
        return $this->postcode;
    }

    /**
     * Set postcode value
     *
     * @param string $postcode
     * @return ListAddressByPostcodeRequest
     */
    public function setPostcode(string $postcode): self
    {
        // validation for constraint: string
        if (!is_null($postcode) && !is_string($postcode)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($postcode, true),
                gettype($postcode)
            ), __LINE__);
        }
        // validation for constraint: maxLength(9)
        if (!is_null($postcode) && mb_strlen((string)$postcode) > 9) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 9',
                mb_strlen((string)$postcode)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($postcode) && mb_strlen((string)$postcode) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$postcode)
            ), __LINE__);
        }
        $this->postcode = $postcode;

        return $this;
    }
}
