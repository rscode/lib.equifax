<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;

/**
 * This class stands for UniqueMatch StructType
 * Meta information extracted from the WSDL
 * - documentation: A single matching address was found. In this case there will only be one 'return address' element, containing a full address with PTC-Abs code.
 *
 * @subpackage Structs
 */
class UniqueMatch extends MatchBase
{
    /**
     * The matchDetail
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var MatchDetail
     */
    protected MatchDetail $matchDetail;
    /**
     * The index
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 999
     * - maxOccurs: unbounded
     * - minInclusive: 0
     * - minOccurs: 0
     *
     * @var int[]
     */
    protected ?array $index = null;

    /**
     * Constructor method for UniqueMatch
     *
     * @param MatchDetail $matchDetail
     * @param int[]       $index
     * @uses UniqueMatch::setMatchDetail()
     * @uses UniqueMatch::setIndex()
     */
    public function __construct(MatchDetail $matchDetail, ?array $index = null)
    {
        $this
            ->setMatchDetail($matchDetail)
            ->setIndex($index);
    }

    /**
     * This method is responsible for validating the values passed to the setIndex method
     * This method is willingly generated in order to preserve the one-line inline validation within the setIndex method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateIndexForArrayConstraintsFromSetIndex(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $uniqueMatchIndexItem) {
            // validation for constraint: itemType
            if (!(is_int($uniqueMatchIndexItem) || ctype_digit($uniqueMatchIndexItem))) {
                $invalidValues[] = is_object($uniqueMatchIndexItem) ? get_class($uniqueMatchIndexItem) : sprintf(
                    '%s(%s)',
                    gettype($uniqueMatchIndexItem),
                    var_export($uniqueMatchIndexItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The index property can only contain items of type int, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get matchDetail value
     *
     * @return MatchDetail
     */
    public function getMatchDetail(): MatchDetail
    {
        return $this->matchDetail;
    }

    /**
     * Set matchDetail value
     *
     * @param MatchDetail $matchDetail
     * @return UniqueMatch
     */
    public function setMatchDetail(MatchDetail $matchDetail): self
    {
        $this->matchDetail = $matchDetail;

        return $this;
    }

    /**
     * Get index value
     *
     * @return int[]
     */
    public function getIndex(): ?array
    {
        return $this->index;
    }

    /**
     * Set index value
     *
     * @param int[] $index
     * @return UniqueMatch
     * @throws InvalidArgumentException
     */
    public function setIndex(?array $index = null): self
    {
        // validation for constraint: array
        if ('' !== ($indexArrayErrorMessage = self::validateIndexForArrayConstraintsFromSetIndex($index))) {
            throw new InvalidArgumentException($indexArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxInclusive(999)
        if (!is_null($index) && $index > 999) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 999',
                var_export($index, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($index) && $index < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($index, true)
            ), __LINE__);
        }
        $this->index = $index;

        return $this;
    }

    /**
     * Add item to index value
     *
     * @param int $item
     * @return UniqueMatch
     * @throws InvalidArgumentException
     */
    public function addToIndex(int $item): self
    {
        // validation for constraint: itemType
        if (!(is_int($item) || ctype_digit($item))) {
            throw new InvalidArgumentException(sprintf(
                'The index property can only contain items of type int, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(999)
        if ($item > 999) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 999',
                var_export($item, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if ($item < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($item, true)
            ), __LINE__);
        }
        $this->index[] = $item;

        return $this;
    }
}
