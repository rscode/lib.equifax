<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;

/**
 * This class stands for Person StructType
 *
 * @subpackage Structs
 */
class Person extends PersonBase
{
    /**
     * The currentAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var ResidenceInstance|null
     */
    protected ?ResidenceInstance $currentAddress = null;
    /**
     * The previousAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     *
     * @var ResidenceInstance[]
     */
    protected ?array $previousAddress = null;

    /**
     * Constructor method for Person
     *
     * @param ResidenceInstance   $currentAddress
     * @param ResidenceInstance[] $previousAddress
     * @uses Person::setCurrentAddress()
     * @uses Person::setPreviousAddress()
     */
    public function __construct(?ResidenceInstance $currentAddress = null, ?array $previousAddress = null)
    {
        $this
            ->setCurrentAddress($currentAddress)
            ->setPreviousAddress($previousAddress);
    }

    /**
     * This method is responsible for validating the values passed to the setPreviousAddress method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPreviousAddress method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePreviousAddressForArrayConstraintsFromSetPreviousAddress(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $personPreviousAddressItem) {
            // validation for constraint: itemType
            if (!$personPreviousAddressItem instanceof ResidenceInstance) {
                $invalidValues[] = is_object($personPreviousAddressItem) ? get_class($personPreviousAddressItem) : sprintf(
                    '%s(%s)',
                    gettype($personPreviousAddressItem),
                    var_export($personPreviousAddressItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The previousAddress property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Address\StructType\ResidenceInstance, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get currentAddress value
     *
     * @return ResidenceInstance|null
     */
    public function getCurrentAddress(): ?ResidenceInstance
    {
        return $this->currentAddress;
    }

    /**
     * Set currentAddress value
     *
     * @param ResidenceInstance $currentAddress
     * @return Person
     */
    public function setCurrentAddress(?ResidenceInstance $currentAddress = null): self
    {
        $this->currentAddress = $currentAddress;

        return $this;
    }

    /**
     * Get previousAddress value
     *
     * @return ResidenceInstance[]
     */
    public function getPreviousAddress(): ?array
    {
        return $this->previousAddress;
    }

    /**
     * Set previousAddress value
     *
     * @param ResidenceInstance[] $previousAddress
     * @return Person
     * @throws InvalidArgumentException
     */
    public function setPreviousAddress(?array $previousAddress = null): self
    {
        // validation for constraint: array
        if ('' !== ($previousAddressArrayErrorMessage = self::validatePreviousAddressForArrayConstraintsFromSetPreviousAddress($previousAddress))) {
            throw new InvalidArgumentException($previousAddressArrayErrorMessage, __LINE__);
        }
        $this->previousAddress = $previousAddress;

        return $this;
    }

    /**
     * Add item to previousAddress value
     *
     * @param ResidenceInstance $item
     * @return Person
     * @throws InvalidArgumentException
     */
    public function addToPreviousAddress(ResidenceInstance $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof ResidenceInstance) {
            throw new InvalidArgumentException(sprintf(
                'The previousAddress property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Address\StructType\ResidenceInstance, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        $this->previousAddress[] = $item;

        return $this;
    }
}
