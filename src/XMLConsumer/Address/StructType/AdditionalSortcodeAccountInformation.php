<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AdditionalSortcodeAccountInformation StructType
 *
 * @subpackage Structs
 */
class AdditionalSortcodeAccountInformation extends AbstractStructBase
{
    /**
     * The BankDetails2
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var BankAccount|null
     */
    protected ?BankAccount $BankDetails2 = null;
    /**
     * The BankDetails3
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var BankAccount|null
     */
    protected ?BankAccount $BankDetails3 = null;
    /**
     * The BankDetails4
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var BankAccount|null
     */
    protected ?BankAccount $BankDetails4 = null;

    /**
     * Constructor method for AdditionalSortcodeAccountInformation
     *
     * @param BankAccount $bankDetails2
     * @param BankAccount $bankDetails3
     * @param BankAccount $bankDetails4
     * @uses AdditionalSortcodeAccountInformation::setBankDetails2()
     * @uses AdditionalSortcodeAccountInformation::setBankDetails3()
     * @uses AdditionalSortcodeAccountInformation::setBankDetails4()
     */
    public function __construct(?BankAccount $bankDetails2 = null, ?BankAccount $bankDetails3 = null, ?BankAccount $bankDetails4 = null)
    {
        $this
            ->setBankDetails2($bankDetails2)
            ->setBankDetails3($bankDetails3)
            ->setBankDetails4($bankDetails4);
    }

    /**
     * Get BankDetails2 value
     *
     * @return BankAccount|null
     */
    public function getBankDetails2(): ?BankAccount
    {
        return $this->BankDetails2;
    }

    /**
     * Set BankDetails2 value
     *
     * @param BankAccount $bankDetails2
     * @return AdditionalSortcodeAccountInformation
     */
    public function setBankDetails2(?BankAccount $bankDetails2 = null): self
    {
        $this->BankDetails2 = $bankDetails2;

        return $this;
    }

    /**
     * Get BankDetails3 value
     *
     * @return BankAccount|null
     */
    public function getBankDetails3(): ?BankAccount
    {
        return $this->BankDetails3;
    }

    /**
     * Set BankDetails3 value
     *
     * @param BankAccount $bankDetails3
     * @return AdditionalSortcodeAccountInformation
     */
    public function setBankDetails3(?BankAccount $bankDetails3 = null): self
    {
        $this->BankDetails3 = $bankDetails3;

        return $this;
    }

    /**
     * Get BankDetails4 value
     *
     * @return BankAccount|null
     */
    public function getBankDetails4(): ?BankAccount
    {
        return $this->BankDetails4;
    }

    /**
     * Set BankDetails4 value
     *
     * @param BankAccount $bankDetails4
     * @return AdditionalSortcodeAccountInformation
     */
    public function setBankDetails4(?BankAccount $bankDetails4 = null): self
    {
        $this->BankDetails4 = $bankDetails4;

        return $this;
    }
}
