<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ResidencyInformation StructType
 *
 * @subpackage Structs
 */
class ResidencyInformation extends AbstractStructBase
{
    /**
     * The CurrentlyUKResident
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $CurrentlyUKResident = null;
    /**
     * The OnlyEverUKResident
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $OnlyEverUKResident = null;

    /**
     * Constructor method for ResidencyInformation
     *
     * @param string $currentlyUKResident
     * @param string $onlyEverUKResident
     * @uses ResidencyInformation::setCurrentlyUKResident()
     * @uses ResidencyInformation::setOnlyEverUKResident()
     */
    public function __construct(?string $currentlyUKResident = null, ?string $onlyEverUKResident = null)
    {
        $this
            ->setCurrentlyUKResident($currentlyUKResident)
            ->setOnlyEverUKResident($onlyEverUKResident);
    }

    /**
     * Get CurrentlyUKResident value
     *
     * @return string|null
     */
    public function getCurrentlyUKResident(): ?string
    {
        return $this->CurrentlyUKResident;
    }

    /**
     * Set CurrentlyUKResident value
     *
     * @param string $currentlyUKResident
     * @return ResidencyInformation
     */
    public function setCurrentlyUKResident(?string $currentlyUKResident = null): self
    {
        // validation for constraint: string
        if (!is_null($currentlyUKResident) && !is_string($currentlyUKResident)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($currentlyUKResident, true),
                gettype($currentlyUKResident)
            ), __LINE__);
        }
        $this->CurrentlyUKResident = $currentlyUKResident;

        return $this;
    }

    /**
     * Get OnlyEverUKResident value
     *
     * @return string|null
     */
    public function getOnlyEverUKResident(): ?string
    {
        return $this->OnlyEverUKResident;
    }

    /**
     * Set OnlyEverUKResident value
     *
     * @param string $onlyEverUKResident
     * @return ResidencyInformation
     */
    public function setOnlyEverUKResident(?string $onlyEverUKResident = null): self
    {
        // validation for constraint: string
        if (!is_null($onlyEverUKResident) && !is_string($onlyEverUKResident)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($onlyEverUKResident, true),
                gettype($onlyEverUKResident)
            ), __LINE__);
        }
        $this->OnlyEverUKResident = $onlyEverUKResident;

        return $this;
    }
}
