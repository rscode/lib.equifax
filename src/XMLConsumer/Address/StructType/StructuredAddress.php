<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;

/**
 * This class stands for StructuredAddress StructType
 *
 * @subpackage Structs
 */
class StructuredAddress extends Address
{
    /**
     * The country
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $country = null;
    /**
     * The county
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 18
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $county = null;
    /**
     * The district
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $district = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The number
     * Meta information extracted from the WSDL
     * - documentation: Short form regular expression to validate house number
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $number = null;
    /**
     * The poBox
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $poBox = null;
    /**
     * The postcode
     * Meta information extracted from the WSDL
     * - documentation: Short form regular expression to validate postcodes according to BS7666
     * - base: xs:string
     * - maxLength: 9
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $postcode = null;
    /**
     * The postTown
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 28
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $postTown = null;
    /**
     * The street1
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $street1 = null;
    /**
     * The street2
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $street2 = null;
    /**
     * The subBuilding
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $subBuilding = null;

    public function __construct(
        ?string $country = null,
        ?string $county = null,
        ?string $district = null,
        ?string $name = null,
        ?string $number = null,
        ?string $poBox = null,
        ?string $postcode = null,
        ?string $postTown = null,
        ?string $street1 = null,
        ?string $street2 = null,
        ?string $subBuilding = null
    ) {
        $this
            ->setCountry($country)
            ->setCounty($county)
            ->setDistrict($district)
            ->setName($name)
            ->setNumber($number)
            ->setPoBox($poBox)
            ->setPostcode($postcode)
            ->setPostTown($postTown)
            ->setStreet1($street1)
            ->setStreet2($street2)
            ->setSubBuilding($subBuilding);
    }

    /**
     * Get country value
     *
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * Set country value
     *
     * @param string $country
     * @return StructuredAddress
     */
    public function setCountry(?string $country = null): self
    {
        // validation for constraint: string
        if (!is_null($country) && !is_string($country)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($country, true),
                gettype($country)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($country) && mb_strlen((string)$country) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$country)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($country) && mb_strlen((string)$country) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$country)
            ), __LINE__);
        }
        $this->country = $country;

        return $this;
    }

    /**
     * Get county value
     *
     * @return string|null
     */
    public function getCounty(): ?string
    {
        return $this->county;
    }

    /**
     * Set county value
     *
     * @param string $county
     * @return StructuredAddress
     */
    public function setCounty(?string $county = null): self
    {
        // validation for constraint: string
        if (!is_null($county) && !is_string($county)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($county, true),
                gettype($county)
            ), __LINE__);
        }
        // validation for constraint: maxLength(18)
        if (!is_null($county) && mb_strlen((string)$county) > 18) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 18',
                mb_strlen((string)$county)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($county) && mb_strlen((string)$county) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$county)
            ), __LINE__);
        }
        $this->county = $county;

        return $this;
    }

    /**
     * Get district value
     *
     * @return string|null
     */
    public function getDistrict(): ?string
    {
        return $this->district;
    }

    /**
     * Set district value
     *
     * @param string $district
     * @return StructuredAddress
     */
    public function setDistrict(?string $district = null): self
    {
        // validation for constraint: string
        if (!is_null($district) && !is_string($district)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($district, true),
                gettype($district)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($district) && mb_strlen((string)$district) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$district)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($district) && mb_strlen((string)$district) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$district)
            ), __LINE__);
        }
        $this->district = $district;

        return $this;
    }

    /**
     * Get name value
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set name value
     *
     * @param string $name
     * @return StructuredAddress
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($name, true),
                gettype($name)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($name) && mb_strlen((string)$name) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$name)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($name) && mb_strlen((string)$name) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$name)
            ), __LINE__);
        }
        $this->name = $name;

        return $this;
    }

    /**
     * Get number value
     *
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * Set number value
     *
     * @param string $number
     * @return StructuredAddress
     */
    public function setNumber(?string $number = null): self
    {
        // validation for constraint: string
        if (!is_null($number) && !is_string($number)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($number, true),
                gettype($number)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($number) && mb_strlen((string)$number) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$number)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($number) && mb_strlen((string)$number) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$number)
            ), __LINE__);
        }
        $this->number = $number;

        return $this;
    }

    /**
     * Get poBox value
     *
     * @return string|null
     */
    public function getPoBox(): ?string
    {
        return $this->poBox;
    }

    /**
     * Set poBox value
     *
     * @param string $poBox
     * @return StructuredAddress
     */
    public function setPoBox(?string $poBox = null): self
    {
        // validation for constraint: string
        if (!is_null($poBox) && !is_string($poBox)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($poBox, true),
                gettype($poBox)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($poBox) && mb_strlen((string)$poBox) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$poBox)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($poBox) && mb_strlen((string)$poBox) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$poBox)
            ), __LINE__);
        }
        $this->poBox = $poBox;

        return $this;
    }

    /**
     * Get postcode value
     *
     * @return string|null
     */
    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    /**
     * Set postcode value
     *
     * @param string $postcode
     * @return StructuredAddress
     */
    public function setPostcode(?string $postcode = null): self
    {
        // validation for constraint: string
        if (!is_null($postcode) && !is_string($postcode)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($postcode, true),
                gettype($postcode)
            ), __LINE__);
        }
        // validation for constraint: maxLength(9)
        if (!is_null($postcode) && mb_strlen((string)$postcode) > 9) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 9',
                mb_strlen((string)$postcode)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($postcode) && mb_strlen((string)$postcode) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$postcode)
            ), __LINE__);
        }
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postTown value
     *
     * @return string|null
     */
    public function getPostTown(): ?string
    {
        return $this->postTown;
    }

    /**
     * Set postTown value
     *
     * @param string $postTown
     * @return StructuredAddress
     */
    public function setPostTown(?string $postTown = null): self
    {
        // validation for constraint: string
        if (!is_null($postTown) && !is_string($postTown)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($postTown, true),
                gettype($postTown)
            ), __LINE__);
        }
        // validation for constraint: maxLength(28)
        if (!is_null($postTown) && mb_strlen((string)$postTown) > 28) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 28',
                mb_strlen((string)$postTown)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($postTown) && mb_strlen((string)$postTown) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$postTown)
            ), __LINE__);
        }
        $this->postTown = $postTown;

        return $this;
    }

    /**
     * Get street1 value
     *
     * @return string|null
     */
    public function getStreet1(): ?string
    {
        return $this->street1;
    }

    /**
     * Set street1 value
     *
     * @param string $street1
     * @return StructuredAddress
     */
    public function setStreet1(?string $street1 = null): self
    {
        // validation for constraint: string
        if (!is_null($street1) && !is_string($street1)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($street1, true),
                gettype($street1)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($street1) && mb_strlen((string)$street1) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$street1)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($street1) && mb_strlen((string)$street1) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$street1)
            ), __LINE__);
        }
        $this->street1 = $street1;

        return $this;
    }

    /**
     * Get street2 value
     *
     * @return string|null
     */
    public function getStreet2(): ?string
    {
        return $this->street2;
    }

    /**
     * Set street2 value
     *
     * @param string $street2
     * @return StructuredAddress
     */
    public function setStreet2(?string $street2 = null): self
    {
        // validation for constraint: string
        if (!is_null($street2) && !is_string($street2)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($street2, true),
                gettype($street2)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($street2) && mb_strlen((string)$street2) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$street2)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($street2) && mb_strlen((string)$street2) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$street2)
            ), __LINE__);
        }
        $this->street2 = $street2;

        return $this;
    }

    /**
     * Get subBuilding value
     *
     * @return string|null
     */
    public function getSubBuilding(): ?string
    {
        return $this->subBuilding;
    }

    /**
     * Set subBuilding value
     *
     * @param string $subBuilding
     * @return StructuredAddress
     */
    public function setSubBuilding(?string $subBuilding = null): self
    {
        // validation for constraint: string
        if (!is_null($subBuilding) && !is_string($subBuilding)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($subBuilding, true),
                gettype($subBuilding)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($subBuilding) && mb_strlen((string)$subBuilding) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$subBuilding)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($subBuilding) && mb_strlen((string)$subBuilding) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$subBuilding)
            ), __LINE__);
        }
        $this->subBuilding = $subBuilding;

        return $this;
    }
}
