<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for HouseHelp StructType
 * Meta information extracted from the WSDL
 * - documentation: House help data returned. No matching addresses were found but relevant addresses within the specified street or postcode are returned. The returned addresses will either contain complete addresses with PTC-Abs code, or almost
 * complete addresses with a range value in the house number attribute (separate odd and even number ranges) and no PTC-Abs codes.
 *
 * @subpackage Structs
 */
class HouseHelp extends AbstractStructBase
{
    /**
     * The addressID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 11
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $addressID = null;
    /**
     * The county
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 18
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $county = null;
    /**
     * The district
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $district = null;
    /**
     * The houseNumber
     * Meta information extracted from the WSDL
     * - documentation: Short form regular expression to validate house number
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $houseNumber = null;
    /**
     * The postcode
     * Meta information extracted from the WSDL
     * - documentation: Short form regular expression to validate postcodes according to BS7666
     * - base: xs:string
     * - maxLength: 9
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $postcode = null;
    /**
     * The posttown
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 28
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $posttown = null;
    /**
     * The street1
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $street1 = null;
    /**
     * The street2
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $street2 = null;

    /**
     * Constructor method for HouseHelp
     *
     * @param string $addressID
     * @param string $county
     * @param string $district
     * @param string $houseNumber
     * @param string $postcode
     * @param string $posttown
     * @param string $street1
     * @param string $street2
     * @uses HouseHelp::setAddressID()
     * @uses HouseHelp::setCounty()
     * @uses HouseHelp::setDistrict()
     * @uses HouseHelp::setHouseNumber()
     * @uses HouseHelp::setPostcode()
     * @uses HouseHelp::setPosttown()
     * @uses HouseHelp::setStreet1()
     * @uses HouseHelp::setStreet2()
     */
    public function __construct(
        ?string $addressID = null,
        ?string $county = null,
        ?string $district = null,
        ?string $houseNumber = null,
        ?string $postcode = null,
        ?string $posttown = null,
        ?string $street1 = null,
        ?string $street2 = null
    ) {
        $this
            ->setAddressID($addressID)
            ->setCounty($county)
            ->setDistrict($district)
            ->setHouseNumber($houseNumber)
            ->setPostcode($postcode)
            ->setPosttown($posttown)
            ->setStreet1($street1)
            ->setStreet2($street2);
    }

    /**
     * Get addressID value
     *
     * @return string|null
     */
    public function getAddressID(): ?string
    {
        return $this->addressID;
    }

    /**
     * Set addressID value
     *
     * @param string $addressID
     * @return HouseHelp
     */
    public function setAddressID(?string $addressID = null): self
    {
        // validation for constraint: string
        if (!is_null($addressID) && !is_string($addressID)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($addressID, true),
                gettype($addressID)
            ), __LINE__);
        }
        // validation for constraint: maxLength(11)
        if (!is_null($addressID) && mb_strlen((string)$addressID) > 11) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 11',
                mb_strlen((string)$addressID)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($addressID) && mb_strlen((string)$addressID) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$addressID)
            ), __LINE__);
        }
        $this->addressID = $addressID;

        return $this;
    }

    /**
     * Get county value
     *
     * @return string|null
     */
    public function getCounty(): ?string
    {
        return $this->county;
    }

    /**
     * Set county value
     *
     * @param string $county
     * @return HouseHelp
     */
    public function setCounty(?string $county = null): self
    {
        // validation for constraint: string
        if (!is_null($county) && !is_string($county)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($county, true),
                gettype($county)
            ), __LINE__);
        }
        // validation for constraint: maxLength(18)
        if (!is_null($county) && mb_strlen((string)$county) > 18) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 18',
                mb_strlen((string)$county)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($county) && mb_strlen((string)$county) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$county)
            ), __LINE__);
        }
        $this->county = $county;

        return $this;
    }

    /**
     * Get district value
     *
     * @return string|null
     */
    public function getDistrict(): ?string
    {
        return $this->district;
    }

    /**
     * Set district value
     *
     * @param string $district
     * @return HouseHelp
     */
    public function setDistrict(?string $district = null): self
    {
        // validation for constraint: string
        if (!is_null($district) && !is_string($district)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($district, true),
                gettype($district)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($district) && mb_strlen((string)$district) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$district)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($district) && mb_strlen((string)$district) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$district)
            ), __LINE__);
        }
        $this->district = $district;

        return $this;
    }

    /**
     * Get houseNumber value
     *
     * @return string|null
     */
    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    /**
     * Set houseNumber value
     *
     * @param string $houseNumber
     * @return HouseHelp
     */
    public function setHouseNumber(?string $houseNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($houseNumber) && !is_string($houseNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($houseNumber, true),
                gettype($houseNumber)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($houseNumber) && mb_strlen((string)$houseNumber) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$houseNumber)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($houseNumber) && mb_strlen((string)$houseNumber) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$houseNumber)
            ), __LINE__);
        }
        $this->houseNumber = $houseNumber;

        return $this;
    }

    /**
     * Get postcode value
     *
     * @return string|null
     */
    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    /**
     * Set postcode value
     *
     * @param string $postcode
     * @return HouseHelp
     */
    public function setPostcode(?string $postcode = null): self
    {
        // validation for constraint: string
        if (!is_null($postcode) && !is_string($postcode)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($postcode, true),
                gettype($postcode)
            ), __LINE__);
        }
        // validation for constraint: maxLength(9)
        if (!is_null($postcode) && mb_strlen((string)$postcode) > 9) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 9',
                mb_strlen((string)$postcode)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($postcode) && mb_strlen((string)$postcode) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$postcode)
            ), __LINE__);
        }
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get posttown value
     *
     * @return string|null
     */
    public function getPosttown(): ?string
    {
        return $this->posttown;
    }

    /**
     * Set posttown value
     *
     * @param string $posttown
     * @return HouseHelp
     */
    public function setPosttown(?string $posttown = null): self
    {
        // validation for constraint: string
        if (!is_null($posttown) && !is_string($posttown)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($posttown, true),
                gettype($posttown)
            ), __LINE__);
        }
        // validation for constraint: maxLength(28)
        if (!is_null($posttown) && mb_strlen((string)$posttown) > 28) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 28',
                mb_strlen((string)$posttown)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($posttown) && mb_strlen((string)$posttown) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$posttown)
            ), __LINE__);
        }
        $this->posttown = $posttown;

        return $this;
    }

    /**
     * Get street1 value
     *
     * @return string|null
     */
    public function getStreet1(): ?string
    {
        return $this->street1;
    }

    /**
     * Set street1 value
     *
     * @param string $street1
     * @return HouseHelp
     */
    public function setStreet1(?string $street1 = null): self
    {
        // validation for constraint: string
        if (!is_null($street1) && !is_string($street1)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($street1, true),
                gettype($street1)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($street1) && mb_strlen((string)$street1) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$street1)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($street1) && mb_strlen((string)$street1) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$street1)
            ), __LINE__);
        }
        $this->street1 = $street1;

        return $this;
    }

    /**
     * Get street2 value
     *
     * @return string|null
     */
    public function getStreet2(): ?string
    {
        return $this->street2;
    }

    /**
     * Set street2 value
     *
     * @param string $street2
     * @return HouseHelp
     */
    public function setStreet2(?string $street2 = null): self
    {
        // validation for constraint: string
        if (!is_null($street2) && !is_string($street2)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($street2, true),
                gettype($street2)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($street2) && mb_strlen((string)$street2) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$street2)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($street2) && mb_strlen((string)$street2) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$street2)
            ), __LINE__);
        }
        $this->street2 = $street2;

        return $this;
    }
}
