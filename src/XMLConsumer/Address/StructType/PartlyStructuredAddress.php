<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;

/**
 * This class stands for PartlyStructuredAddress StructType
 *
 * @subpackage Structs
 */
class PartlyStructuredAddress extends FreeFormatAddress
{
    /**
     * The postcode
     * Meta information extracted from the WSDL
     * - documentation: Short form regular expression to validate postcodes according to BS7666
     * - base: xs:string
     * - maxLength: 9
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $postcode;

    /**
     * Constructor method for PartlyStructuredAddress
     *
     * @param string $postcode
     * @uses PartlyStructuredAddress::setPostcode()
     */
    public function __construct(string $postcode)
    {
        $this
            ->setPostcode($postcode);
    }

    /**
     * Get postcode value
     *
     * @return string
     */
    public function getPostcode(): string
    {
        return $this->postcode;
    }

    /**
     * Set postcode value
     *
     * @param string $postcode
     * @return PartlyStructuredAddress
     */
    public function setPostcode(string $postcode): self
    {
        // validation for constraint: string
        if (!is_null($postcode) && !is_string($postcode)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($postcode, true),
                gettype($postcode)
            ), __LINE__);
        }
        // validation for constraint: maxLength(9)
        if (!is_null($postcode) && mb_strlen((string)$postcode) > 9) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 9',
                mb_strlen((string)$postcode)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($postcode) && mb_strlen((string)$postcode) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$postcode)
            ), __LINE__);
        }
        $this->postcode = $postcode;

        return $this;
    }
}
