<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;

/**
 * This class stands for PartialName StructType
 *
 * @subpackage Structs
 */
class PartialName extends NameBase
{
    /**
     * The forename
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $forename = null;

    /**
     * Constructor method for PartialName
     *
     * @param string $forename
     * @uses PartialName::setForename()
     */
    public function __construct(?string $forename = null)
    {
        $this
            ->setForename($forename);
    }

    /**
     * Get forename value
     *
     * @return string|null
     */
    public function getForename(): ?string
    {
        return $this->forename;
    }

    /**
     * Set forename value
     *
     * @param string $forename
     * @return PartialName
     */
    public function setForename(?string $forename = null): self
    {
        // validation for constraint: string
        if (!is_null($forename) && !is_string($forename)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($forename, true),
                gettype($forename)
            ), __LINE__);
        }
        $this->forename = $forename;

        return $this;
    }
}
