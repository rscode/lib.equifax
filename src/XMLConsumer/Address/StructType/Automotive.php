<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Address\EnumType\CarOwnedStatus;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Automotive StructType
 *
 * @subpackage Structs
 */
class Automotive extends AbstractStructBase
{
    /**
     * The status
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $status = null;
    /**
     * The VRNNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $VRNNumber = null;
    /**
     * The VINNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $VINNumber = null;
    /**
     * The ValueOfVehicle
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ValueOfVehicle = null;

    /**
     * Constructor method for Automotive
     *
     * @param string $status
     * @param string $vRNNumber
     * @param string $vINNumber
     * @param string $valueOfVehicle
     * @uses Automotive::setStatus()
     * @uses Automotive::setVRNNumber()
     * @uses Automotive::setVINNumber()
     * @uses Automotive::setValueOfVehicle()
     */
    public function __construct(
        ?string $status = null,
        ?string $vRNNumber = null,
        ?string $vINNumber = null,
        ?string $valueOfVehicle = null
    ) {
        $this
            ->setStatus($status)
            ->setVRNNumber($vRNNumber)
            ->setVINNumber($vINNumber)
            ->setValueOfVehicle($valueOfVehicle);
    }

    /**
     * Get status value
     *
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * Set status value
     *
     * @param string $status
     * @return Automotive
     * @throws InvalidArgumentException
     * @uses CarOwnedStatus::getValidValues
     * @uses CarOwnedStatus::valueIsValid
     */
    public function setStatus(?string $status = null): self
    {
        // validation for constraint: enumeration
        if (!CarOwnedStatus::valueIsValid($status)) {
            throw new InvalidArgumentException(
                sprintf(
                    'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Address\EnumType\CarOwnedStatus',
                    is_array($status) ? implode(', ', $status) : var_export($status, true),
                    implode(', ', CarOwnedStatus::getValidValues())
                ),
                __LINE__
            );
        }
        $this->status = $status;

        return $this;
    }

    /**
     * Get VRNNumber value
     *
     * @return string|null
     */
    public function getVRNNumber(): ?string
    {
        return $this->VRNNumber;
    }

    /**
     * Set VRNNumber value
     *
     * @param string $vRNNumber
     * @return Automotive
     */
    public function setVRNNumber(?string $vRNNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($vRNNumber) && !is_string($vRNNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($vRNNumber, true),
                gettype($vRNNumber)
            ), __LINE__);
        }
        $this->VRNNumber = $vRNNumber;

        return $this;
    }

    /**
     * Get VINNumber value
     *
     * @return string|null
     */
    public function getVINNumber(): ?string
    {
        return $this->VINNumber;
    }

    /**
     * Set VINNumber value
     *
     * @param string $vINNumber
     * @return Automotive
     */
    public function setVINNumber(?string $vINNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($vINNumber) && !is_string($vINNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($vINNumber, true),
                gettype($vINNumber)
            ), __LINE__);
        }
        $this->VINNumber = $vINNumber;

        return $this;
    }

    /**
     * Get ValueOfVehicle value
     *
     * @return string|null
     */
    public function getValueOfVehicle(): ?string
    {
        return $this->ValueOfVehicle;
    }

    /**
     * Set ValueOfVehicle value
     *
     * @param string $valueOfVehicle
     * @return Automotive
     */
    public function setValueOfVehicle(?string $valueOfVehicle = null): self
    {
        // validation for constraint: string
        if (!is_null($valueOfVehicle) && !is_string($valueOfVehicle)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($valueOfVehicle, true),
                gettype($valueOfVehicle)
            ), __LINE__);
        }
        $this->ValueOfVehicle = $valueOfVehicle;

        return $this;
    }
}
