<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ProductInformation StructType
 *
 * @subpackage Structs
 */
class ProductInformation extends AbstractStructBase
{
    /**
     * The ProductType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ProductType = null;
    /**
     * The ProductPurpose
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ProductPurpose = null;
    /**
     * The BalanceTransfer
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $BalanceTransfer = null;
    /**
     * The RequestedProductDuration
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $RequestedProductDuration = null;
    /**
     * The RequestedCreditLimit
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $RequestedCreditLimit = null;
    /**
     * The PaymentMode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $PaymentMode = null;

    /**
     * Constructor method for ProductInformation
     *
     * @param string $productType
     * @param string $productPurpose
     * @param string $balanceTransfer
     * @param string $requestedProductDuration
     * @param string $requestedCreditLimit
     * @param string $paymentMode
     * @uses ProductInformation::setProductType()
     * @uses ProductInformation::setProductPurpose()
     * @uses ProductInformation::setBalanceTransfer()
     * @uses ProductInformation::setRequestedProductDuration()
     * @uses ProductInformation::setRequestedCreditLimit()
     * @uses ProductInformation::setPaymentMode()
     */
    public function __construct(
        ?string $productType = null,
        ?string $productPurpose = null,
        ?string $balanceTransfer = null,
        ?string $requestedProductDuration = null,
        ?string $requestedCreditLimit = null,
        ?string $paymentMode = null
    ) {
        $this
            ->setProductType($productType)
            ->setProductPurpose($productPurpose)
            ->setBalanceTransfer($balanceTransfer)
            ->setRequestedProductDuration($requestedProductDuration)
            ->setRequestedCreditLimit($requestedCreditLimit)
            ->setPaymentMode($paymentMode);
    }

    /**
     * Get ProductType value
     *
     * @return string|null
     */
    public function getProductType(): ?string
    {
        return $this->ProductType;
    }

    /**
     * Set ProductType value
     *
     * @param string $productType
     * @return ProductInformation
     */
    public function setProductType(?string $productType = null): self
    {
        // validation for constraint: string
        if (!is_null($productType) && !is_string($productType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($productType, true),
                gettype($productType)
            ), __LINE__);
        }
        $this->ProductType = $productType;

        return $this;
    }

    /**
     * Get ProductPurpose value
     *
     * @return string|null
     */
    public function getProductPurpose(): ?string
    {
        return $this->ProductPurpose;
    }

    /**
     * Set ProductPurpose value
     *
     * @param string $productPurpose
     * @return ProductInformation
     */
    public function setProductPurpose(?string $productPurpose = null): self
    {
        // validation for constraint: string
        if (!is_null($productPurpose) && !is_string($productPurpose)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($productPurpose, true),
                gettype($productPurpose)
            ), __LINE__);
        }
        $this->ProductPurpose = $productPurpose;

        return $this;
    }

    /**
     * Get BalanceTransfer value
     *
     * @return string|null
     */
    public function getBalanceTransfer(): ?string
    {
        return $this->BalanceTransfer;
    }

    /**
     * Set BalanceTransfer value
     *
     * @param string $balanceTransfer
     * @return ProductInformation
     */
    public function setBalanceTransfer(?string $balanceTransfer = null): self
    {
        // validation for constraint: string
        if (!is_null($balanceTransfer) && !is_string($balanceTransfer)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($balanceTransfer, true),
                gettype($balanceTransfer)
            ), __LINE__);
        }
        $this->BalanceTransfer = $balanceTransfer;

        return $this;
    }

    /**
     * Get RequestedProductDuration value
     *
     * @return string|null
     */
    public function getRequestedProductDuration(): ?string
    {
        return $this->RequestedProductDuration;
    }

    /**
     * Set RequestedProductDuration value
     *
     * @param string $requestedProductDuration
     * @return ProductInformation
     */
    public function setRequestedProductDuration(?string $requestedProductDuration = null): self
    {
        // validation for constraint: string
        if (!is_null($requestedProductDuration) && !is_string($requestedProductDuration)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($requestedProductDuration, true),
                gettype($requestedProductDuration)
            ), __LINE__);
        }
        $this->RequestedProductDuration = $requestedProductDuration;

        return $this;
    }

    /**
     * Get RequestedCreditLimit value
     *
     * @return string|null
     */
    public function getRequestedCreditLimit(): ?string
    {
        return $this->RequestedCreditLimit;
    }

    /**
     * Set RequestedCreditLimit value
     *
     * @param string $requestedCreditLimit
     * @return ProductInformation
     */
    public function setRequestedCreditLimit(?string $requestedCreditLimit = null): self
    {
        // validation for constraint: string
        if (!is_null($requestedCreditLimit) && !is_string($requestedCreditLimit)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($requestedCreditLimit, true),
                gettype($requestedCreditLimit)
            ), __LINE__);
        }
        $this->RequestedCreditLimit = $requestedCreditLimit;

        return $this;
    }

    /**
     * Get PaymentMode value
     *
     * @return string|null
     */
    public function getPaymentMode(): ?string
    {
        return $this->PaymentMode;
    }

    /**
     * Set PaymentMode value
     *
     * @param string $paymentMode
     * @return ProductInformation
     */
    public function setPaymentMode(?string $paymentMode = null): self
    {
        // validation for constraint: string
        if (!is_null($paymentMode) && !is_string($paymentMode)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($paymentMode, true),
                gettype($paymentMode)
            ), __LINE__);
        }
        $this->PaymentMode = $paymentMode;

        return $this;
    }
}
