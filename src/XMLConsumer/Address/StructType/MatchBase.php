<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MatchBase StructType
 *
 * @subpackage Structs
 */
class MatchBase extends AbstractStructBase
{
    /**
     * The inputIDRef
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 32
     * - minLength: 0
     * - use: optional
     *
     * @var string|null
     */
    protected ?string $inputIDRef = null;

    /**
     * Constructor method for MatchBase
     *
     * @param string $inputIDRef
     * @uses MatchBase::setInputIDRef()
     */
    public function __construct(?string $inputIDRef = null)
    {
        $this
            ->setInputIDRef($inputIDRef);
    }

    /**
     * Get inputIDRef value
     *
     * @return string|null
     */
    public function getInputIDRef(): ?string
    {
        return $this->inputIDRef;
    }

    /**
     * Set inputIDRef value
     *
     * @param string $inputIDRef
     * @return MatchBase
     */
    public function setInputIDRef(?string $inputIDRef = null): self
    {
        // validation for constraint: string
        if (!is_null($inputIDRef) && !is_string($inputIDRef)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($inputIDRef, true),
                gettype($inputIDRef)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($inputIDRef) && mb_strlen((string)$inputIDRef) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$inputIDRef)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($inputIDRef) && mb_strlen((string)$inputIDRef) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$inputIDRef)
            ), __LINE__);
        }
        $this->inputIDRef = $inputIDRef;

        return $this;
    }
}
