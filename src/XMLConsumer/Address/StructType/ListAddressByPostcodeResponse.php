<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for listAddressByPostcodeResponse StructType
 *
 * @subpackage Structs
 */
class ListAddressByPostcodeResponse extends AbstractStructBase
{
    /**
     * The clientRef
     * Meta information extracted from the WSDL
     * - documentation: Used to supply an enquiry reference identifier.
     * - base: xs:string
     * - maxLength: 40
     * - maxOccurs: 1
     * - minLength: 1
     * - minOccurs: 1
     * - pattern: .*[^\s].*
     *
     * @var string
     */
    protected string $clientRef;
    /**
     * The totalResults
     * Meta information extracted from the WSDL
     * - base: xs:nonNegativeInteger
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $totalResults;
    /**
     * The matchedStructuredAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: 150
     * - minOccurs: 0
     *
     * @var MatchedStructuredAddress[]
     */
    protected ?array $matchedStructuredAddress = null;

    /**
     * Constructor method for listAddressByPostcodeResponse
     *
     * @param string                     $clientRef
     * @param int                        $totalResults
     * @param MatchedStructuredAddress[] $matchedStructuredAddress
     * @uses ListAddressByPostcodeResponse::setClientRef()
     * @uses ListAddressByPostcodeResponse::setTotalResults()
     * @uses ListAddressByPostcodeResponse::setMatchedStructuredAddress()
     */
    public function __construct(string $clientRef, int $totalResults, ?array $matchedStructuredAddress = null)
    {
        $this
            ->setClientRef($clientRef)
            ->setTotalResults($totalResults)
            ->setMatchedStructuredAddress($matchedStructuredAddress);
    }

    /**
     * This method is responsible for validating the values passed to the setMatchedStructuredAddress method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMatchedStructuredAddress method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMatchedStructuredAddressForArrayConstraintsFromSetMatchedStructuredAddress(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $listAddressByPostcodeResponseMatchedStructuredAddressItem) {
            // validation for constraint: itemType
            if (!$listAddressByPostcodeResponseMatchedStructuredAddressItem instanceof MatchedStructuredAddress) {
                $invalidValues[] = is_object($listAddressByPostcodeResponseMatchedStructuredAddressItem) ? get_class($listAddressByPostcodeResponseMatchedStructuredAddressItem) : sprintf(
                    '%s(%s)',
                    gettype($listAddressByPostcodeResponseMatchedStructuredAddressItem),
                    var_export($listAddressByPostcodeResponseMatchedStructuredAddressItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The matchedStructuredAddress property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Address\StructType\MatchedStructuredAddress, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get clientRef value
     *
     * @return string
     */
    public function getClientRef(): string
    {
        return $this->clientRef;
    }

    /**
     * Set clientRef value
     *
     * @param string $clientRef
     * @return ListAddressByPostcodeResponse
     */
    public function setClientRef(string $clientRef): self
    {
        // validation for constraint: string
        if (!is_null($clientRef) && !is_string($clientRef)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($clientRef, true),
                gettype($clientRef)
            ), __LINE__);
        }
        // validation for constraint: maxLength(40)
        if (!is_null($clientRef) && mb_strlen((string)$clientRef) > 40) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 40',
                mb_strlen((string)$clientRef)
            ), __LINE__);
        }
        // validation for constraint: minLength(1)
        if (!is_null($clientRef) && mb_strlen((string)$clientRef) < 1) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 1',
                mb_strlen((string)$clientRef)
            ), __LINE__);
        }
        // validation for constraint: pattern(.*[^\s].*)
        if (!is_null($clientRef) && !preg_match('/.*[^\\s].*/', $clientRef)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /.*[^\\s].*/',
                var_export($clientRef, true)
            ), __LINE__);
        }
        $this->clientRef = $clientRef;

        return $this;
    }

    /**
     * Get totalResults value
     *
     * @return int
     */
    public function getTotalResults(): int
    {
        return $this->totalResults;
    }

    /**
     * Set totalResults value
     *
     * @param int $totalResults
     * @return ListAddressByPostcodeResponse
     */
    public function setTotalResults(int $totalResults): self
    {
        // validation for constraint: int
        if (!is_null($totalResults) && !(is_int($totalResults) || ctype_digit($totalResults))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($totalResults, true),
                gettype($totalResults)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($totalResults) && $totalResults < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($totalResults, true)
            ), __LINE__);
        }
        $this->totalResults = $totalResults;

        return $this;
    }

    /**
     * Get matchedStructuredAddress value
     *
     * @return MatchedStructuredAddress[]
     */
    public function getMatchedStructuredAddress(): ?array
    {
        return $this->matchedStructuredAddress;
    }

    /**
     * Set matchedStructuredAddress value
     *
     * @param MatchedStructuredAddress[] $matchedStructuredAddress
     * @return ListAddressByPostcodeResponse
     * @throws InvalidArgumentException
     */
    public function setMatchedStructuredAddress(?array $matchedStructuredAddress = null): self
    {
        // validation for constraint: array
        if ('' !== ($matchedStructuredAddressArrayErrorMessage = self::validateMatchedStructuredAddressForArrayConstraintsFromSetMatchedStructuredAddress($matchedStructuredAddress))) {
            throw new InvalidArgumentException($matchedStructuredAddressArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(150)
        if (is_array($matchedStructuredAddress) && count($matchedStructuredAddress) > 150) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 150',
                count($matchedStructuredAddress)
            ), __LINE__);
        }
        $this->matchedStructuredAddress = $matchedStructuredAddress;

        return $this;
    }

    /**
     * Add item to matchedStructuredAddress value
     *
     * @param MatchedStructuredAddress $item
     * @return ListAddressByPostcodeResponse
     * @throws InvalidArgumentException
     */
    public function addToMatchedStructuredAddress(MatchedStructuredAddress $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof MatchedStructuredAddress) {
            throw new InvalidArgumentException(sprintf(
                'The matchedStructuredAddress property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Address\StructType\MatchedStructuredAddress, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(150)
        if (is_array($this->matchedStructuredAddress) && count($this->matchedStructuredAddress) >= 150) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 150',
                count($this->matchedStructuredAddress)
            ), __LINE__);
        }
        $this->matchedStructuredAddress[] = $item;

        return $this;
    }
}
