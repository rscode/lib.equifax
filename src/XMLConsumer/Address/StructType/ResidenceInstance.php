<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Address\EnumType\ResidentialStatus;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ResidenceInstance StructType
 *
 * @subpackage Structs
 */
class ResidenceInstance extends AbstractStructBase
{
    /**
     * The residentialStatus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $residentialStatus = null;
    /**
     * The timeAtAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $timeAtAddress = null;
    /**
     * The address
     *
     * @var ResidentialStructuredAddress|null
     */
    protected ?ResidentialStructuredAddress $address = null;
    /**
     * The freeFormatAddress
     *
     * @var ResidentialFreeFormatAddress|null
     */
    protected ?ResidentialFreeFormatAddress $freeFormatAddress = null;
    /**
     * The matchedAddress
     *
     * @var MatchedResidentialStructuredAddress|null
     */
    protected ?MatchedResidentialStructuredAddress $matchedAddress = null;

    /**
     * Constructor method for ResidenceInstance
     *
     * @param string                              $residentialStatus
     * @param string                              $timeAtAddress
     * @param ResidentialStructuredAddress        $address
     * @param ResidentialFreeFormatAddress        $freeFormatAddress
     * @param MatchedResidentialStructuredAddress $matchedAddress
     * @uses ResidenceInstance::setResidentialStatus()
     * @uses ResidenceInstance::setTimeAtAddress()
     * @uses ResidenceInstance::setAddress()
     * @uses ResidenceInstance::setFreeFormatAddress()
     * @uses ResidenceInstance::setMatchedAddress()
     */
    public function __construct(
        ?string $residentialStatus = null,
        ?string $timeAtAddress = null,
        ?ResidentialStructuredAddress $address = null,
        ?ResidentialFreeFormatAddress $freeFormatAddress = null,
        ?MatchedResidentialStructuredAddress $matchedAddress = null
    ) {
        $this
            ->setResidentialStatus($residentialStatus)
            ->setTimeAtAddress($timeAtAddress)
            ->setAddress($address)
            ->setFreeFormatAddress($freeFormatAddress)
            ->setMatchedAddress($matchedAddress);
    }

    /**
     * Get residentialStatus value
     *
     * @return string|null
     */
    public function getResidentialStatus(): ?string
    {
        return $this->residentialStatus;
    }

    /**
     * Set residentialStatus value
     *
     * @param string $residentialStatus
     * @return ResidenceInstance
     * @throws InvalidArgumentException
     * @uses ResidentialStatus::getValidValues
     * @uses ResidentialStatus::valueIsValid
     */
    public function setResidentialStatus(?string $residentialStatus = null): self
    {
        // validation for constraint: enumeration
        if (!ResidentialStatus::valueIsValid($residentialStatus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Address\EnumType\ResidentialStatus',
                is_array($residentialStatus) ? implode(', ', $residentialStatus) : var_export($residentialStatus, true),
                implode(', ', ResidentialStatus::getValidValues())
            ), __LINE__);
        }
        $this->residentialStatus = $residentialStatus;

        return $this;
    }

    /**
     * Get timeAtAddress value
     *
     * @return string|null
     */
    public function getTimeAtAddress(): ?string
    {
        return $this->timeAtAddress;
    }

    /**
     * Set timeAtAddress value
     *
     * @param string $timeAtAddress
     * @return ResidenceInstance
     */
    public function setTimeAtAddress(?string $timeAtAddress = null): self
    {
        // validation for constraint: string
        if (!is_null($timeAtAddress) && !is_string($timeAtAddress)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($timeAtAddress, true),
                gettype($timeAtAddress)
            ), __LINE__);
        }
        $this->timeAtAddress = $timeAtAddress;

        return $this;
    }

    /**
     * Get address value
     *
     * @return ResidentialStructuredAddress|null
     */
    public function getAddress(): ?ResidentialStructuredAddress
    {
        return $this->address;
    }

    /**
     * Set address value
     *
     * @param ResidentialStructuredAddress $address
     * @return ResidenceInstance
     */
    public function setAddress(?ResidentialStructuredAddress $address = null): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get freeFormatAddress value
     *
     * @return ResidentialFreeFormatAddress|null
     */
    public function getFreeFormatAddress(): ?ResidentialFreeFormatAddress
    {
        return $this->freeFormatAddress;
    }

    /**
     * Set freeFormatAddress value
     *
     * @param ResidentialFreeFormatAddress $freeFormatAddress
     * @return ResidenceInstance
     */
    public function setFreeFormatAddress(?ResidentialFreeFormatAddress $freeFormatAddress = null): self
    {
        $this->freeFormatAddress = $freeFormatAddress;

        return $this;
    }

    /**
     * Get matchedAddress value
     *
     * @return MatchedResidentialStructuredAddress|null
     */
    public function getMatchedAddress(): ?MatchedResidentialStructuredAddress
    {
        return $this->matchedAddress;
    }

    /**
     * Set matchedAddress value
     *
     * @param MatchedResidentialStructuredAddress $matchedAddress
     * @return ResidenceInstance
     */
    public function setMatchedAddress(?MatchedResidentialStructuredAddress $matchedAddress = null): self
    {
        $this->matchedAddress = $matchedAddress;

        return $this;
    }
}
