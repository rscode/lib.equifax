<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for matchAddressResponse StructType
 *
 * @subpackage Structs
 */
class MatchAddressResponse extends AbstractStructBase
{
    /**
     * The clientRef
     * Meta information extracted from the WSDL
     * - documentation: Used to supply an enquiry reference identifier.
     * - base: xs:string
     * - maxLength: 40
     * - maxOccurs: 1
     * - minLength: 1
     * - minOccurs: 1
     * - pattern: .*[^\s].*
     *
     * @var string
     */
    protected string $clientRef;
    /**
     * The matchHelp
     *
     * @var MatchHelp[]|null
     */
    protected ?array $matchHelp = null;
    /**
     * The multipleMatch
     *
     * @var MultipleMatch[]|null
     */
    protected ?array $multipleMatch = null;
    /**
     * The noMatch
     *
     * @var NoMatch[]|null
     */
    protected ?array $noMatch = null;
    /**
     * The uniqueMatch
     *
     * @var UniqueMatch[]|null
     */
    protected ?array $uniqueMatch = null;

    public function __construct(
        string $clientRef,
        ?array $matchHelp = null,
        ?array $multipleMatch = null,
        ?array $noMatch = null,
        ?array $uniqueMatch = null
    ) {
        $this
            ->setClientRef($clientRef)
            ->setMatchHelp($matchHelp)
            ->setMultipleMatch($multipleMatch)
            ->setNoMatch($noMatch)
            ->setUniqueMatch($uniqueMatch);
    }

    /**
     * Get clientRef value
     *
     * @return string
     */
    public function getClientRef(): string
    {
        return $this->clientRef;
    }

    /**
     * Set clientRef value
     *
     * @param string $clientRef
     * @return MatchAddressResponse
     */
    public function setClientRef(string $clientRef): self
    {
        // validation for constraint: string
        if (!is_null($clientRef) && !is_string($clientRef)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($clientRef, true),
                gettype($clientRef)
            ), __LINE__);
        }
        // validation for constraint: maxLength(40)
        if (!is_null($clientRef) && mb_strlen((string)$clientRef) > 40) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 40',
                mb_strlen((string)$clientRef)
            ), __LINE__);
        }
        // validation for constraint: minLength(1)
        if (!is_null($clientRef) && mb_strlen((string)$clientRef) < 1) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 1',
                mb_strlen((string)$clientRef)
            ), __LINE__);
        }
        // validation for constraint: pattern(.*[^\s].*)
        if (!is_null($clientRef) && !preg_match('/.*[^\\s].*/', $clientRef)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /.*[^\\s].*/',
                var_export($clientRef, true)
            ), __LINE__);
        }
        $this->clientRef = $clientRef;

        return $this;
    }

    /**
     * @return MatchHelp[]|null
     */
    public function getMatchHelp(): ?array
    {
        return $this->matchHelp;
    }

    /**
     * @param MatchHelp[]|null $matchHelp
     * @return MatchAddressResponse
     */
    public function setMatchHelp(?array $matchHelp): MatchAddressResponse
    {
        $this->matchHelp = $matchHelp;

        return $this;
    }

    /**
     * @return MultipleMatch[]|null
     */
    public function getMultipleMatch(): ?array
    {
        return $this->multipleMatch;
    }

    /**
     * @param MultipleMatch[]|null $multipleMatch
     * @return MatchAddressResponse
     */
    public function setMultipleMatch(?array $multipleMatch): MatchAddressResponse
    {
        $this->multipleMatch = $multipleMatch;

        return $this;
    }

    /**
     * @return NoMatch[]|null
     */
    public function getNoMatch(): ?array
    {
        return $this->noMatch;
    }

    /**
     * @param NoMatch[]|null $noMatch
     * @return MatchAddressResponse
     */
    public function setNoMatch(?array $noMatch): MatchAddressResponse
    {
        $this->noMatch = $noMatch;

        return $this;
    }

    /**
     * @return UniqueMatch[]|null
     */
    public function getUniqueMatch(): ?array
    {
        return $this->uniqueMatch;
    }

    /**
     * @param UniqueMatch[]|null $uniqueMatch
     * @return MatchAddressResponse
     */
    public function setUniqueMatch(?array $uniqueMatch): MatchAddressResponse
    {
        $this->uniqueMatch = $uniqueMatch;

        return $this;
    }
}
