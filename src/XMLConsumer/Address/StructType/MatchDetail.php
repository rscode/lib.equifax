<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MatchDetail StructType
 * Meta information extracted from the WSDL
 * - documentation: Store the returned address details (including PTC-Abs code) and proceed
 *
 * @subpackage Structs
 */
class MatchDetail extends AbstractStructBase
{
    /**
     * The matchedAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var MatchedStructuredAddress
     */
    protected MatchedStructuredAddress $matchedAddress;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var Name|null
     */
    protected ?Name $name = null;

    /**
     * Constructor method for MatchDetail
     *
     * @param MatchedStructuredAddress $matchedAddress
     * @param Name                     $name
     * @uses MatchDetail::setMatchedAddress()
     * @uses MatchDetail::setName()
     */
    public function __construct(MatchedStructuredAddress $matchedAddress, ?Name $name = null)
    {
        $this
            ->setMatchedAddress($matchedAddress)
            ->setName($name);
    }

    /**
     * Get matchedAddress value
     *
     * @return MatchedStructuredAddress
     */
    public function getMatchedAddress(): MatchedStructuredAddress
    {
        return $this->matchedAddress;
    }

    /**
     * Set matchedAddress value
     *
     * @param MatchedStructuredAddress $matchedAddress
     * @return MatchDetail
     */
    public function setMatchedAddress(MatchedStructuredAddress $matchedAddress): self
    {
        $this->matchedAddress = $matchedAddress;

        return $this;
    }

    /**
     * Get name value
     *
     * @return Name|null
     */
    public function getName(): ?Name
    {
        return $this->name;
    }

    /**
     * Set name value
     *
     * @param Name $name
     * @return MatchDetail
     */
    public function setName(?Name $name = null): self
    {
        $this->name = $name;

        return $this;
    }
}
