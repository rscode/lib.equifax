<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for nameAndAddress StructType
 *
 * @subpackage Structs
 */
class NameAndAddress extends AbstractStructBase
{
    /**
     * The name
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var Name|null
     */
    protected ?Name $name = null;
    /**
     * The freeFormatAddress
     *
     * @var FreeFormatAddress|null
     */
    protected ?FreeFormatAddress $freeFormatAddress = null;
    /**
     * The structuredAddress
     *
     * @var StructuredAddress|null
     */
    protected ?StructuredAddress $structuredAddress = null;
    /**
     * The id
     * Meta information extracted from the WSDL
     * - use: optional
     *
     * @var string|null
     */
    protected ?string $id = null;

    /**
     * Constructor method for nameAndAddress
     *
     * @param Name              $name
     * @param FreeFormatAddress $freeFormatAddress
     * @param StructuredAddress $structuredAddress
     * @param string            $id
     * @uses NameAndAddress::setName()
     * @uses NameAndAddress::setFreeFormatAddress()
     * @uses NameAndAddress::setStructuredAddress()
     * @uses NameAndAddress::setId()
     */
    public function __construct(
        ?Name $name = null,
        ?FreeFormatAddress $freeFormatAddress = null,
        ?StructuredAddress $structuredAddress = null,
        ?string $id = null
    ) {
        $this
            ->setName($name)
            ->setFreeFormatAddress($freeFormatAddress)
            ->setStructuredAddress($structuredAddress)
            ->setId($id);
    }

    /**
     * Get name value
     *
     * @return Name|null
     */
    public function getName(): ?Name
    {
        return $this->name;
    }

    /**
     * Set name value
     *
     * @param Name $name
     * @return NameAndAddress
     */
    public function setName(?Name $name = null): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get freeFormatAddress value
     *
     * @return FreeFormatAddress|null
     */
    public function getFreeFormatAddress(): ?FreeFormatAddress
    {
        return $this->freeFormatAddress;
    }

    /**
     * Set freeFormatAddress value
     *
     * @param FreeFormatAddress $freeFormatAddress
     * @return NameAndAddress
     */
    public function setFreeFormatAddress(?FreeFormatAddress $freeFormatAddress = null): self
    {
        $this->freeFormatAddress = $freeFormatAddress;

        return $this;
    }

    /**
     * Get structuredAddress value
     *
     * @return StructuredAddress|null
     */
    public function getStructuredAddress(): ?StructuredAddress
    {
        return $this->structuredAddress;
    }

    /**
     * Set structuredAddress value
     *
     * @param StructuredAddress $structuredAddress
     * @return NameAndAddress
     */
    public function setStructuredAddress(?StructuredAddress $structuredAddress = null): self
    {
        $this->structuredAddress = $structuredAddress;

        return $this;
    }

    /**
     * Get id value
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * Set id value
     *
     * @param string $id
     * @return NameAndAddress
     */
    public function setId(?string $id = null): self
    {
        // validation for constraint: string
        if (!is_null($id) && !is_string($id)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($id, true),
                gettype($id)
            ), __LINE__);
        }
        $this->id = $id;

        return $this;
    }
}
