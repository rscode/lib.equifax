<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for NetMonthlyIncomeFields StructType
 *
 * @subpackage Structs
 */
class NetMonthlyIncomeFields extends AbstractStructBase
{
    /**
     * The ApplicantTotalNetMonthlyIncome
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ApplicantTotalNetMonthlyIncome = null;
    /**
     * The ApplicantNetMonthlySalary
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ApplicantNetMonthlySalary = null;
    /**
     * The ApplicantNetMonthlyBonus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ApplicantNetMonthlyBonus = null;
    /**
     * The ApplicantSpousalIncome
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ApplicantSpousalIncome = null;
    /**
     * The ApplicantNetMonthlyOtherIncome
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ApplicantNetMonthlyOtherIncome = null;
    /**
     * The HouseholdTotalNetMonthlyIncome
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $HouseholdTotalNetMonthlyIncome = null;

    /**
     * Constructor method for NetMonthlyIncomeFields
     *
     * @param string $applicantTotalNetMonthlyIncome
     * @param string $applicantNetMonthlySalary
     * @param string $applicantNetMonthlyBonus
     * @param string $applicantSpousalIncome
     * @param string $applicantNetMonthlyOtherIncome
     * @param string $householdTotalNetMonthlyIncome
     * @uses NetMonthlyIncomeFields::setApplicantTotalNetMonthlyIncome()
     * @uses NetMonthlyIncomeFields::setApplicantNetMonthlySalary()
     * @uses NetMonthlyIncomeFields::setApplicantNetMonthlyBonus()
     * @uses NetMonthlyIncomeFields::setApplicantSpousalIncome()
     * @uses NetMonthlyIncomeFields::setApplicantNetMonthlyOtherIncome()
     * @uses NetMonthlyIncomeFields::setHouseholdTotalNetMonthlyIncome()
     */
    public function __construct(
        ?string $applicantTotalNetMonthlyIncome = null,
        ?string $applicantNetMonthlySalary = null,
        ?string $applicantNetMonthlyBonus = null,
        ?string $applicantSpousalIncome = null,
        ?string $applicantNetMonthlyOtherIncome = null,
        ?string $householdTotalNetMonthlyIncome = null
    ) {
        $this
            ->setApplicantTotalNetMonthlyIncome($applicantTotalNetMonthlyIncome)
            ->setApplicantNetMonthlySalary($applicantNetMonthlySalary)
            ->setApplicantNetMonthlyBonus($applicantNetMonthlyBonus)
            ->setApplicantSpousalIncome($applicantSpousalIncome)
            ->setApplicantNetMonthlyOtherIncome($applicantNetMonthlyOtherIncome)
            ->setHouseholdTotalNetMonthlyIncome($householdTotalNetMonthlyIncome);
    }

    /**
     * Get ApplicantTotalNetMonthlyIncome value
     *
     * @return string|null
     */
    public function getApplicantTotalNetMonthlyIncome(): ?string
    {
        return $this->ApplicantTotalNetMonthlyIncome;
    }

    /**
     * Set ApplicantTotalNetMonthlyIncome value
     *
     * @param string $applicantTotalNetMonthlyIncome
     * @return NetMonthlyIncomeFields
     */
    public function setApplicantTotalNetMonthlyIncome(?string $applicantTotalNetMonthlyIncome = null): self
    {
        // validation for constraint: string
        if (!is_null($applicantTotalNetMonthlyIncome) && !is_string($applicantTotalNetMonthlyIncome)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($applicantTotalNetMonthlyIncome, true),
                gettype($applicantTotalNetMonthlyIncome)
            ), __LINE__);
        }
        $this->ApplicantTotalNetMonthlyIncome = $applicantTotalNetMonthlyIncome;

        return $this;
    }

    /**
     * Get ApplicantNetMonthlySalary value
     *
     * @return string|null
     */
    public function getApplicantNetMonthlySalary(): ?string
    {
        return $this->ApplicantNetMonthlySalary;
    }

    /**
     * Set ApplicantNetMonthlySalary value
     *
     * @param string $applicantNetMonthlySalary
     * @return NetMonthlyIncomeFields
     */
    public function setApplicantNetMonthlySalary(?string $applicantNetMonthlySalary = null): self
    {
        // validation for constraint: string
        if (!is_null($applicantNetMonthlySalary) && !is_string($applicantNetMonthlySalary)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($applicantNetMonthlySalary, true),
                gettype($applicantNetMonthlySalary)
            ), __LINE__);
        }
        $this->ApplicantNetMonthlySalary = $applicantNetMonthlySalary;

        return $this;
    }

    /**
     * Get ApplicantNetMonthlyBonus value
     *
     * @return string|null
     */
    public function getApplicantNetMonthlyBonus(): ?string
    {
        return $this->ApplicantNetMonthlyBonus;
    }

    /**
     * Set ApplicantNetMonthlyBonus value
     *
     * @param string $applicantNetMonthlyBonus
     * @return NetMonthlyIncomeFields
     */
    public function setApplicantNetMonthlyBonus(?string $applicantNetMonthlyBonus = null): self
    {
        // validation for constraint: string
        if (!is_null($applicantNetMonthlyBonus) && !is_string($applicantNetMonthlyBonus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($applicantNetMonthlyBonus, true),
                gettype($applicantNetMonthlyBonus)
            ), __LINE__);
        }
        $this->ApplicantNetMonthlyBonus = $applicantNetMonthlyBonus;

        return $this;
    }

    /**
     * Get ApplicantSpousalIncome value
     *
     * @return string|null
     */
    public function getApplicantSpousalIncome(): ?string
    {
        return $this->ApplicantSpousalIncome;
    }

    /**
     * Set ApplicantSpousalIncome value
     *
     * @param string $applicantSpousalIncome
     * @return NetMonthlyIncomeFields
     */
    public function setApplicantSpousalIncome(?string $applicantSpousalIncome = null): self
    {
        // validation for constraint: string
        if (!is_null($applicantSpousalIncome) && !is_string($applicantSpousalIncome)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($applicantSpousalIncome, true),
                gettype($applicantSpousalIncome)
            ), __LINE__);
        }
        $this->ApplicantSpousalIncome = $applicantSpousalIncome;

        return $this;
    }

    /**
     * Get ApplicantNetMonthlyOtherIncome value
     *
     * @return string|null
     */
    public function getApplicantNetMonthlyOtherIncome(): ?string
    {
        return $this->ApplicantNetMonthlyOtherIncome;
    }

    /**
     * Set ApplicantNetMonthlyOtherIncome value
     *
     * @param string $applicantNetMonthlyOtherIncome
     * @return NetMonthlyIncomeFields
     */
    public function setApplicantNetMonthlyOtherIncome(?string $applicantNetMonthlyOtherIncome = null): self
    {
        // validation for constraint: string
        if (!is_null($applicantNetMonthlyOtherIncome) && !is_string($applicantNetMonthlyOtherIncome)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($applicantNetMonthlyOtherIncome, true),
                gettype($applicantNetMonthlyOtherIncome)
            ), __LINE__);
        }
        $this->ApplicantNetMonthlyOtherIncome = $applicantNetMonthlyOtherIncome;

        return $this;
    }

    /**
     * Get HouseholdTotalNetMonthlyIncome value
     *
     * @return string|null
     */
    public function getHouseholdTotalNetMonthlyIncome(): ?string
    {
        return $this->HouseholdTotalNetMonthlyIncome;
    }

    /**
     * Set HouseholdTotalNetMonthlyIncome value
     *
     * @param string $householdTotalNetMonthlyIncome
     * @return NetMonthlyIncomeFields
     */
    public function setHouseholdTotalNetMonthlyIncome(?string $householdTotalNetMonthlyIncome = null): self
    {
        // validation for constraint: string
        if (!is_null($householdTotalNetMonthlyIncome) && !is_string($householdTotalNetMonthlyIncome)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($householdTotalNetMonthlyIncome, true),
                gettype($householdTotalNetMonthlyIncome)
            ), __LINE__);
        }
        $this->HouseholdTotalNetMonthlyIncome = $householdTotalNetMonthlyIncome;

        return $this;
    }
}
