<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for matchAddressRequest StructType
 *
 * @subpackage Structs
 */
class MatchAddressRequest extends AbstractStructBase
{
    /**
     * The clientRef
     * Meta information extracted from the WSDL
     * - documentation: Used to supply an enquiry reference identifier.
     * - base: xs:string
     * - maxLength: 40
     * - maxOccurs: 1
     * - minLength: 1
     * - minOccurs: 1
     * - pattern: .*[^\s].*
     *
     * @var string
     */
    protected string $clientRef;
    /**
     * The nameAndAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 1
     *
     * @var NameAndAddress[]
     */
    protected array $nameAndAddress;
    /**
     * The suppressMultipleMatchesAndMatchHelp
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $suppressMultipleMatchesAndMatchHelp;

    /**
     * Constructor method for matchAddressRequest
     *
     * @param string           $clientRef
     * @param NameAndAddress[] $nameAndAddress
     * @param bool             $suppressMultipleMatchesAndMatchHelp
     * @uses MatchAddressRequest::setClientRef()
     * @uses MatchAddressRequest::setNameAndAddress()
     * @uses MatchAddressRequest::setSuppressMultipleMatchesAndMatchHelp()
     */
    public function __construct(string $clientRef, array $nameAndAddress = [], bool $suppressMultipleMatchesAndMatchHelp = false)
    {
        $this
            ->setClientRef($clientRef)
            ->setNameAndAddress($nameAndAddress)
            ->setSuppressMultipleMatchesAndMatchHelp($suppressMultipleMatchesAndMatchHelp);
    }

    /**
     * This method is responsible for validating the values passed to the setNameAndAddress method
     * This method is willingly generated in order to preserve the one-line inline validation within the setNameAndAddress method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateNameAndAddressForArrayConstraintsFromSetNameAndAddress(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $matchAddressRequestNameAndAddressItem) {
            // validation for constraint: itemType
            if (!$matchAddressRequestNameAndAddressItem instanceof NameAndAddress) {
                $invalidValues[] = is_object($matchAddressRequestNameAndAddressItem) ? get_class($matchAddressRequestNameAndAddressItem) : sprintf(
                    '%s(%s)',
                    gettype($matchAddressRequestNameAndAddressItem),
                    var_export($matchAddressRequestNameAndAddressItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The nameAndAddress property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Address\StructType\NameAndAddress, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get clientRef value
     *
     * @return string
     */
    public function getClientRef(): string
    {
        return $this->clientRef;
    }

    /**
     * Set clientRef value
     *
     * @param string $clientRef
     * @return MatchAddressRequest
     */
    public function setClientRef(string $clientRef): self
    {
        // validation for constraint: string
        if (!is_null($clientRef) && !is_string($clientRef)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($clientRef, true),
                gettype($clientRef)
            ), __LINE__);
        }
        // validation for constraint: maxLength(40)
        if (!is_null($clientRef) && mb_strlen((string)$clientRef) > 40) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 40',
                mb_strlen((string)$clientRef)
            ), __LINE__);
        }
        // validation for constraint: minLength(1)
        if (!is_null($clientRef) && mb_strlen((string)$clientRef) < 1) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 1',
                mb_strlen((string)$clientRef)
            ), __LINE__);
        }
        // validation for constraint: pattern(.*[^\s].*)
        if (!is_null($clientRef) && !preg_match('/.*[^\\s].*/', $clientRef)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /.*[^\\s].*/',
                var_export($clientRef, true)
            ), __LINE__);
        }
        $this->clientRef = $clientRef;

        return $this;
    }

    /**
     * Get nameAndAddress value
     *
     * @return NameAndAddress[]
     */
    public function getNameAndAddress(): array
    {
        return $this->nameAndAddress;
    }

    /**
     * Set nameAndAddress value
     *
     * @param NameAndAddress[] $nameAndAddress
     * @return MatchAddressRequest
     * @throws InvalidArgumentException
     */
    public function setNameAndAddress(array $nameAndAddress): self
    {
        // validation for constraint: array
        if ('' !== ($nameAndAddressArrayErrorMessage = self::validateNameAndAddressForArrayConstraintsFromSetNameAndAddress($nameAndAddress))) {
            throw new InvalidArgumentException($nameAndAddressArrayErrorMessage, __LINE__);
        }
        $this->nameAndAddress = $nameAndAddress;

        return $this;
    }

    /**
     * Add item to nameAndAddress value
     *
     * @param NameAndAddress $item
     * @return MatchAddressRequest
     * @throws InvalidArgumentException
     */
    public function addToNameAndAddress(NameAndAddress $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof NameAndAddress) {
            throw new InvalidArgumentException(sprintf(
                'The nameAndAddress property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Address\StructType\NameAndAddress, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        $this->nameAndAddress[] = $item;

        return $this;
    }

    /**
     * Get suppressMultipleMatchesAndMatchHelp value
     *
     * @return bool
     */
    public function getSuppressMultipleMatchesAndMatchHelp(): bool
    {
        return $this->suppressMultipleMatchesAndMatchHelp;
    }

    /**
     * Set suppressMultipleMatchesAndMatchHelp value
     *
     * @param bool $suppressMultipleMatchesAndMatchHelp
     * @return MatchAddressRequest
     */
    public function setSuppressMultipleMatchesAndMatchHelp(bool $suppressMultipleMatchesAndMatchHelp): self
    {
        // validation for constraint: boolean
        if (!is_null($suppressMultipleMatchesAndMatchHelp) && !is_bool($suppressMultipleMatchesAndMatchHelp)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($suppressMultipleMatchesAndMatchHelp, true),
                gettype($suppressMultipleMatchesAndMatchHelp)
            ), __LINE__);
        }
        $this->suppressMultipleMatchesAndMatchHelp = $suppressMultipleMatchesAndMatchHelp;

        return $this;
    }
}
