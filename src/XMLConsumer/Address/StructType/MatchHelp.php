<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;

/**
 * This class stands for MatchHelp StructType
 *
 * @subpackage Structs
 */
class MatchHelp extends MatchBase
{
    /**
     * The houseHelp
     * Meta information extracted from the WSDL
     * - choice: houseHelp | posttownHelp | streetHelp
     * - choiceMaxOccurs: 1
     * - choiceMinOccurs: 1
     * - maxOccurs: unbounded
     * - minOccurs: 1
     *
     * @var HouseHelp[]
     */
    protected array $houseHelp;
    /**
     * The posttownHelp
     * Meta information extracted from the WSDL
     * - choice: houseHelp | posttownHelp | streetHelp
     * - choiceMaxOccurs: 1
     * - choiceMinOccurs: 1
     * - maxOccurs: unbounded
     * - minOccurs: 1
     *
     * @var PosttownHelp[]
     */
    protected array $posttownHelp;
    /**
     * The streetHelp
     * Meta information extracted from the WSDL
     * - choice: houseHelp | posttownHelp | streetHelp
     * - choiceMaxOccurs: 1
     * - choiceMinOccurs: 1
     * - maxOccurs: unbounded
     * - minOccurs: 1
     *
     * @var StreetHelp[]
     */
    protected array $streetHelp;

    /**
     * Constructor method for MatchHelp
     *
     * @param HouseHelp[]    $houseHelp
     * @param PosttownHelp[] $posttownHelp
     * @param StreetHelp[]   $streetHelp
     * @uses MatchHelp::setHouseHelp()
     * @uses MatchHelp::setPosttownHelp()
     * @uses MatchHelp::setStreetHelp()
     */
    public function __construct(array $houseHelp, array $posttownHelp, array $streetHelp)
    {
        $this
            ->setHouseHelp($houseHelp)
            ->setPosttownHelp($posttownHelp)
            ->setStreetHelp($streetHelp);
    }

    /**
     * This method is responsible for validating the values passed to the setHouseHelp method
     * This method is willingly generated in order to preserve the one-line inline validation within the setHouseHelp method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateHouseHelpForArrayConstraintsFromSetHouseHelp(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $matchHelpHouseHelpItem) {
            // validation for constraint: itemType
            if (!$matchHelpHouseHelpItem instanceof HouseHelp) {
                $invalidValues[] = is_object($matchHelpHouseHelpItem) ? get_class($matchHelpHouseHelpItem) : sprintf(
                    '%s(%s)',
                    gettype($matchHelpHouseHelpItem),
                    var_export($matchHelpHouseHelpItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The houseHelp property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Address\StructType\HouseHelp, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * This method is responsible for validating the values passed to the setPosttownHelp method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPosttownHelp method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePosttownHelpForArrayConstraintsFromSetPosttownHelp(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $matchHelpPosttownHelpItem) {
            // validation for constraint: itemType
            if (!$matchHelpPosttownHelpItem instanceof PosttownHelp) {
                $invalidValues[] = is_object($matchHelpPosttownHelpItem) ? get_class($matchHelpPosttownHelpItem) : sprintf(
                    '%s(%s)',
                    gettype($matchHelpPosttownHelpItem),
                    var_export($matchHelpPosttownHelpItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The posttownHelp property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Address\StructType\PosttownHelp, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * This method is responsible for validating the values passed to the setStreetHelp method
     * This method is willingly generated in order to preserve the one-line inline validation within the setStreetHelp method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateStreetHelpForArrayConstraintsFromSetStreetHelp(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $matchHelpStreetHelpItem) {
            // validation for constraint: itemType
            if (!$matchHelpStreetHelpItem instanceof StreetHelp) {
                $invalidValues[] = is_object($matchHelpStreetHelpItem) ? get_class($matchHelpStreetHelpItem) : sprintf(
                    '%s(%s)',
                    gettype($matchHelpStreetHelpItem),
                    var_export($matchHelpStreetHelpItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The streetHelp property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Address\StructType\StreetHelp, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get houseHelp value
     *
     * @return HouseHelp[]
     */
    public function getHouseHelp(): array
    {
        return isset($this->houseHelp) ? $this->houseHelp : null;
    }

    /**
     * Set houseHelp value
     * This property belongs to a choice that allows only one property to exist. It is
     * therefore removable from the request, consequently if the value assigned to this
     * property is null, the property is removed from this object
     *
     * @param HouseHelp[] $houseHelp
     * @return MatchHelp
     * @throws InvalidArgumentException
     * @throws InvalidArgumentException
     */
    public function setHouseHelp(array $houseHelp): self
    {
        // validation for constraint: array
        if ('' !== ($houseHelpArrayErrorMessage = self::validateHouseHelpForArrayConstraintsFromSetHouseHelp($houseHelp))) {
            throw new InvalidArgumentException($houseHelpArrayErrorMessage, __LINE__);
        }
        // validation for constraint: choice(houseHelp, posttownHelp, streetHelp)
        if ('' !== ($houseHelpChoiceErrorMessage = self::validateHouseHelpForChoiceConstraintsFromSetHouseHelp($houseHelp))) {
            throw new InvalidArgumentException($houseHelpChoiceErrorMessage, __LINE__);
        }
        // validation for constraint: choiceMaxOccurs(1)
        if (is_array($houseHelp) && count($houseHelp) > 1) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 1',
                count($houseHelp)
            ), __LINE__);
        }
        if (is_null($houseHelp) || (is_array($houseHelp) && empty($houseHelp))) {
            unset($this->houseHelp);
        } else {
            $this->houseHelp = $houseHelp;
        }

        return $this;
    }

    /**
     * This method is responsible for validating the value passed to the setHouseHelp method
     * This method is willingly generated in order to preserve the one-line inline validation within the setHouseHelp method
     * This has to validate that the property which is being set is the only one among the given choices
     *
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public function validateHouseHelpForChoiceConstraintsFromSetHouseHelp($value): string
    {
        $message = '';
        if (is_null($value)) {
            return $message;
        }
        $properties = [
            'posttownHelp',
            'streetHelp',
        ];
        try {
            foreach ($properties as $property) {
                if (isset($this->{$property})) {
                    throw new InvalidArgumentException(sprintf(
                        'The property houseHelp can\'t be set as the property %s is already set. Only one property must be set among these properties: houseHelp, %s.',
                        $property,
                        implode(', ', $properties)
                    ), __LINE__);
                }
            }
        } catch (InvalidArgumentException $e) {
            $message = $e->getMessage();
        }

        return $message;
    }

    /**
     * Add item to houseHelp value
     *
     * @param HouseHelp $item
     * @return MatchHelp
     * @throws InvalidArgumentException
     */
    public function addToHouseHelp(HouseHelp $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof HouseHelp) {
            throw new InvalidArgumentException(sprintf(
                'The houseHelp property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Address\StructType\HouseHelp, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: choice(houseHelp, posttownHelp, streetHelp)
        if ('' !== ($itemChoiceErrorMessage = self::validateItemForChoiceConstraintsFromAddToHouseHelp($item))) {
            throw new InvalidArgumentException($itemChoiceErrorMessage, __LINE__);
        }
        // validation for constraint: choiceMaxOccurs(1)
        if (is_array($this->houseHelp) && count($this->houseHelp) >= 1) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 1',
                count($this->houseHelp)
            ), __LINE__);
        }
        $this->houseHelp[] = $item;

        return $this;
    }

    /**
     * This method is responsible for validating the value passed to the addToHouseHelp method
     * This method is willingly generated in order to preserve the one-line inline validation within the addToHouseHelp method
     * This has to validate that the property which is being set is the only one among the given choices
     *
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public function validateItemForChoiceConstraintsFromAddToHouseHelp($value): string
    {
        $message = '';
        if (is_null($value)) {
            return $message;
        }
        $properties = [
            'posttownHelp',
            'streetHelp',
        ];
        try {
            foreach ($properties as $property) {
                if (isset($this->{$property})) {
                    throw new InvalidArgumentException(sprintf(
                        'The property houseHelp can\'t be set as the property %s is already set. Only one property must be set among these properties: houseHelp, %s.',
                        $property,
                        implode(', ', $properties)
                    ), __LINE__);
                }
            }
        } catch (InvalidArgumentException $e) {
            $message = $e->getMessage();
        }

        return $message;
    }

    /**
     * Get posttownHelp value
     *
     * @return PosttownHelp[]
     */
    public function getPosttownHelp(): array
    {
        return isset($this->posttownHelp) ? $this->posttownHelp : null;
    }

    /**
     * Set posttownHelp value
     * This property belongs to a choice that allows only one property to exist. It is
     * therefore removable from the request, consequently if the value assigned to this
     * property is null, the property is removed from this object
     *
     * @param PosttownHelp[] $posttownHelp
     * @return MatchHelp
     * @throws InvalidArgumentException
     * @throws InvalidArgumentException
     */
    public function setPosttownHelp(array $posttownHelp): self
    {
        // validation for constraint: array
        if ('' !== ($posttownHelpArrayErrorMessage = self::validatePosttownHelpForArrayConstraintsFromSetPosttownHelp($posttownHelp))) {
            throw new InvalidArgumentException($posttownHelpArrayErrorMessage, __LINE__);
        }
        // validation for constraint: choice(houseHelp, posttownHelp, streetHelp)
        if ('' !== ($posttownHelpChoiceErrorMessage = self::validatePosttownHelpForChoiceConstraintsFromSetPosttownHelp($posttownHelp))) {
            throw new InvalidArgumentException($posttownHelpChoiceErrorMessage, __LINE__);
        }
        // validation for constraint: choiceMaxOccurs(1)
        if (is_array($posttownHelp) && count($posttownHelp) > 1) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 1',
                count($posttownHelp)
            ), __LINE__);
        }
        if (is_null($posttownHelp) || (is_array($posttownHelp) && empty($posttownHelp))) {
            unset($this->posttownHelp);
        } else {
            $this->posttownHelp = $posttownHelp;
        }

        return $this;
    }

    /**
     * This method is responsible for validating the value passed to the setPosttownHelp method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPosttownHelp method
     * This has to validate that the property which is being set is the only one among the given choices
     *
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public function validatePosttownHelpForChoiceConstraintsFromSetPosttownHelp($value): string
    {
        $message = '';
        if (is_null($value)) {
            return $message;
        }
        $properties = [
            'houseHelp',
            'streetHelp',
        ];
        try {
            foreach ($properties as $property) {
                if (isset($this->{$property})) {
                    throw new InvalidArgumentException(sprintf(
                        'The property posttownHelp can\'t be set as the property %s is already set. Only one property must be set among these properties: posttownHelp, %s.',
                        $property,
                        implode(', ', $properties)
                    ), __LINE__);
                }
            }
        } catch (InvalidArgumentException $e) {
            $message = $e->getMessage();
        }

        return $message;
    }

    /**
     * Add item to posttownHelp value
     *
     * @param PosttownHelp $item
     * @return MatchHelp
     * @throws InvalidArgumentException
     */
    public function addToPosttownHelp(PosttownHelp $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof PosttownHelp) {
            throw new InvalidArgumentException(sprintf(
                'The posttownHelp property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Address\StructType\PosttownHelp, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: choice(houseHelp, posttownHelp, streetHelp)
        if ('' !== ($itemChoiceErrorMessage = self::validateItemForChoiceConstraintsFromAddToPosttownHelp($item))) {
            throw new InvalidArgumentException($itemChoiceErrorMessage, __LINE__);
        }
        // validation for constraint: choiceMaxOccurs(1)
        if (is_array($this->posttownHelp) && count($this->posttownHelp) >= 1) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 1',
                count($this->posttownHelp)
            ), __LINE__);
        }
        $this->posttownHelp[] = $item;

        return $this;
    }

    /**
     * This method is responsible for validating the value passed to the addToPosttownHelp method
     * This method is willingly generated in order to preserve the one-line inline validation within the addToPosttownHelp method
     * This has to validate that the property which is being set is the only one among the given choices
     *
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public function validateItemForChoiceConstraintsFromAddToPosttownHelp($value): string
    {
        $message = '';
        if (is_null($value)) {
            return $message;
        }
        $properties = [
            'houseHelp',
            'streetHelp',
        ];
        try {
            foreach ($properties as $property) {
                if (isset($this->{$property})) {
                    throw new InvalidArgumentException(sprintf(
                        'The property posttownHelp can\'t be set as the property %s is already set. Only one property must be set among these properties: posttownHelp, %s.',
                        $property,
                        implode(', ', $properties)
                    ), __LINE__);
                }
            }
        } catch (InvalidArgumentException $e) {
            $message = $e->getMessage();
        }

        return $message;
    }

    /**
     * Get streetHelp value
     *
     * @return StreetHelp[]
     */
    public function getStreetHelp(): array
    {
        return isset($this->streetHelp) ? $this->streetHelp : null;
    }

    /**
     * Set streetHelp value
     * This property belongs to a choice that allows only one property to exist. It is
     * therefore removable from the request, consequently if the value assigned to this
     * property is null, the property is removed from this object
     *
     * @param StreetHelp[] $streetHelp
     * @return MatchHelp
     * @throws InvalidArgumentException
     * @throws InvalidArgumentException
     */
    public function setStreetHelp(array $streetHelp): self
    {
        // validation for constraint: array
        if ('' !== ($streetHelpArrayErrorMessage = self::validateStreetHelpForArrayConstraintsFromSetStreetHelp($streetHelp))) {
            throw new InvalidArgumentException($streetHelpArrayErrorMessage, __LINE__);
        }
        // validation for constraint: choice(houseHelp, posttownHelp, streetHelp)
        if ('' !== ($streetHelpChoiceErrorMessage = self::validateStreetHelpForChoiceConstraintsFromSetStreetHelp($streetHelp))) {
            throw new InvalidArgumentException($streetHelpChoiceErrorMessage, __LINE__);
        }
        // validation for constraint: choiceMaxOccurs(1)
        if (is_array($streetHelp) && count($streetHelp) > 1) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 1',
                count($streetHelp)
            ), __LINE__);
        }
        if (is_null($streetHelp) || (is_array($streetHelp) && empty($streetHelp))) {
            unset($this->streetHelp);
        } else {
            $this->streetHelp = $streetHelp;
        }

        return $this;
    }

    /**
     * This method is responsible for validating the value passed to the setStreetHelp method
     * This method is willingly generated in order to preserve the one-line inline validation within the setStreetHelp method
     * This has to validate that the property which is being set is the only one among the given choices
     *
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public function validateStreetHelpForChoiceConstraintsFromSetStreetHelp($value): string
    {
        $message = '';
        if (is_null($value)) {
            return $message;
        }
        $properties = [
            'houseHelp',
            'posttownHelp',
        ];
        try {
            foreach ($properties as $property) {
                if (isset($this->{$property})) {
                    throw new InvalidArgumentException(sprintf(
                        'The property streetHelp can\'t be set as the property %s is already set. Only one property must be set among these properties: streetHelp, %s.',
                        $property,
                        implode(', ', $properties)
                    ), __LINE__);
                }
            }
        } catch (InvalidArgumentException $e) {
            $message = $e->getMessage();
        }

        return $message;
    }

    /**
     * Add item to streetHelp value
     *
     * @param StreetHelp $item
     * @return MatchHelp
     * @throws InvalidArgumentException
     */
    public function addToStreetHelp(StreetHelp $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof StreetHelp) {
            throw new InvalidArgumentException(sprintf(
                'The streetHelp property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Address\StructType\StreetHelp, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: choice(houseHelp, posttownHelp, streetHelp)
        if ('' !== ($itemChoiceErrorMessage = self::validateItemForChoiceConstraintsFromAddToStreetHelp($item))) {
            throw new InvalidArgumentException($itemChoiceErrorMessage, __LINE__);
        }
        // validation for constraint: choiceMaxOccurs(1)
        if (is_array($this->streetHelp) && count($this->streetHelp) >= 1) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 1',
                count($this->streetHelp)
            ), __LINE__);
        }
        $this->streetHelp[] = $item;

        return $this;
    }

    /**
     * This method is responsible for validating the value passed to the addToStreetHelp method
     * This method is willingly generated in order to preserve the one-line inline validation within the addToStreetHelp method
     * This has to validate that the property which is being set is the only one among the given choices
     *
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public function validateItemForChoiceConstraintsFromAddToStreetHelp($value): string
    {
        $message = '';
        if (is_null($value)) {
            return $message;
        }
        $properties = [
            'houseHelp',
            'posttownHelp',
        ];
        try {
            foreach ($properties as $property) {
                if (isset($this->{$property})) {
                    throw new InvalidArgumentException(sprintf(
                        'The property streetHelp can\'t be set as the property %s is already set. Only one property must be set among these properties: streetHelp, %s.',
                        $property,
                        implode(', ', $properties)
                    ), __LINE__);
                }
            }
        } catch (InvalidArgumentException $e) {
            $message = $e->getMessage();
        }

        return $message;
    }
}
