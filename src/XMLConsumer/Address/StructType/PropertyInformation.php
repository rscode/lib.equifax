<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PropertyInformation StructType
 *
 * @subpackage Structs
 */
class PropertyInformation extends AbstractStructBase
{
    /**
     * The NumberOfBedrooms
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $NumberOfBedrooms = null;
    /**
     * The HouseType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $HouseType = null;
    /**
     * The AffordabilityResidentialStatus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $AffordabilityResidentialStatus = null;
    /**
     * The ValueOfProperty
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ValueOfProperty = null;

    /**
     * Constructor method for PropertyInformation
     *
     * @param string $numberOfBedrooms
     * @param string $houseType
     * @param string $affordabilityResidentialStatus
     * @param string $valueOfProperty
     * @uses PropertyInformation::setNumberOfBedrooms()
     * @uses PropertyInformation::setHouseType()
     * @uses PropertyInformation::setAffordabilityResidentialStatus()
     * @uses PropertyInformation::setValueOfProperty()
     */
    public function __construct(
        ?string $numberOfBedrooms = null,
        ?string $houseType = null,
        ?string $affordabilityResidentialStatus = null,
        ?string $valueOfProperty = null
    ) {
        $this
            ->setNumberOfBedrooms($numberOfBedrooms)
            ->setHouseType($houseType)
            ->setAffordabilityResidentialStatus($affordabilityResidentialStatus)
            ->setValueOfProperty($valueOfProperty);
    }

    /**
     * Get NumberOfBedrooms value
     *
     * @return string|null
     */
    public function getNumberOfBedrooms(): ?string
    {
        return $this->NumberOfBedrooms;
    }

    /**
     * Set NumberOfBedrooms value
     *
     * @param string $numberOfBedrooms
     * @return PropertyInformation
     */
    public function setNumberOfBedrooms(?string $numberOfBedrooms = null): self
    {
        // validation for constraint: string
        if (!is_null($numberOfBedrooms) && !is_string($numberOfBedrooms)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($numberOfBedrooms, true),
                gettype($numberOfBedrooms)
            ), __LINE__);
        }
        $this->NumberOfBedrooms = $numberOfBedrooms;

        return $this;
    }

    /**
     * Get HouseType value
     *
     * @return string|null
     */
    public function getHouseType(): ?string
    {
        return $this->HouseType;
    }

    /**
     * Set HouseType value
     *
     * @param string $houseType
     * @return PropertyInformation
     */
    public function setHouseType(?string $houseType = null): self
    {
        // validation for constraint: string
        if (!is_null($houseType) && !is_string($houseType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($houseType, true),
                gettype($houseType)
            ), __LINE__);
        }
        $this->HouseType = $houseType;

        return $this;
    }

    /**
     * Get AffordabilityResidentialStatus value
     *
     * @return string|null
     */
    public function getAffordabilityResidentialStatus(): ?string
    {
        return $this->AffordabilityResidentialStatus;
    }

    /**
     * Set AffordabilityResidentialStatus value
     *
     * @param string $affordabilityResidentialStatus
     * @return PropertyInformation
     */
    public function setAffordabilityResidentialStatus(?string $affordabilityResidentialStatus = null): self
    {
        // validation for constraint: string
        if (!is_null($affordabilityResidentialStatus) && !is_string($affordabilityResidentialStatus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($affordabilityResidentialStatus, true),
                gettype($affordabilityResidentialStatus)
            ), __LINE__);
        }
        $this->AffordabilityResidentialStatus = $affordabilityResidentialStatus;

        return $this;
    }

    /**
     * Get ValueOfProperty value
     *
     * @return string|null
     */
    public function getValueOfProperty(): ?string
    {
        return $this->ValueOfProperty;
    }

    /**
     * Set ValueOfProperty value
     *
     * @param string $valueOfProperty
     * @return PropertyInformation
     */
    public function setValueOfProperty(?string $valueOfProperty = null): self
    {
        // validation for constraint: string
        if (!is_null($valueOfProperty) && !is_string($valueOfProperty)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($valueOfProperty, true),
                gettype($valueOfProperty)
            ), __LINE__);
        }
        $this->ValueOfProperty = $valueOfProperty;

        return $this;
    }
}
