<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for StreetHelp StructType
 * Meta information extracted from the WSDL
 * - documentation: Street help data returned. No matching addresses were found but close matches were found for the street name. The return addresses will contain values for street 1, street 2 (where applicable), district, post town and county.
 *
 * @subpackage Structs
 */
class StreetHelp extends AbstractStructBase
{
    /**
     * The county
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 18
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $county;
    /**
     * The district
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $district;
    /**
     * The posttown
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 28
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $posttown;
    /**
     * The street1
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $street1;
    /**
     * The street2
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $street2;

    /**
     * Constructor method for StreetHelp
     *
     * @param string $county
     * @param string $district
     * @param string $posttown
     * @param string $street1
     * @param string $street2
     * @uses StreetHelp::setCounty()
     * @uses StreetHelp::setDistrict()
     * @uses StreetHelp::setPosttown()
     * @uses StreetHelp::setStreet1()
     * @uses StreetHelp::setStreet2()
     */
    public function __construct(string $county, string $district, string $posttown, string $street1, string $street2)
    {
        $this
            ->setCounty($county)
            ->setDistrict($district)
            ->setPosttown($posttown)
            ->setStreet1($street1)
            ->setStreet2($street2);
    }

    /**
     * Get county value
     *
     * @return string
     */
    public function getCounty(): string
    {
        return $this->county;
    }

    /**
     * Set county value
     *
     * @param string $county
     * @return StreetHelp
     */
    public function setCounty(string $county): self
    {
        // validation for constraint: string
        if (!is_null($county) && !is_string($county)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($county, true),
                gettype($county)
            ), __LINE__);
        }
        // validation for constraint: maxLength(18)
        if (!is_null($county) && mb_strlen((string)$county) > 18) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 18',
                mb_strlen((string)$county)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($county) && mb_strlen((string)$county) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$county)
            ), __LINE__);
        }
        $this->county = $county;

        return $this;
    }

    /**
     * Get district value
     *
     * @return string
     */
    public function getDistrict(): string
    {
        return $this->district;
    }

    /**
     * Set district value
     *
     * @param string $district
     * @return StreetHelp
     */
    public function setDistrict(string $district): self
    {
        // validation for constraint: string
        if (!is_null($district) && !is_string($district)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($district, true),
                gettype($district)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($district) && mb_strlen((string)$district) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$district)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($district) && mb_strlen((string)$district) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$district)
            ), __LINE__);
        }
        $this->district = $district;

        return $this;
    }

    /**
     * Get posttown value
     *
     * @return string
     */
    public function getPosttown(): string
    {
        return $this->posttown;
    }

    /**
     * Set posttown value
     *
     * @param string $posttown
     * @return StreetHelp
     */
    public function setPosttown(string $posttown): self
    {
        // validation for constraint: string
        if (!is_null($posttown) && !is_string($posttown)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($posttown, true),
                gettype($posttown)
            ), __LINE__);
        }
        // validation for constraint: maxLength(28)
        if (!is_null($posttown) && mb_strlen((string)$posttown) > 28) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 28',
                mb_strlen((string)$posttown)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($posttown) && mb_strlen((string)$posttown) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$posttown)
            ), __LINE__);
        }
        $this->posttown = $posttown;

        return $this;
    }

    /**
     * Get street1 value
     *
     * @return string
     */
    public function getStreet1(): string
    {
        return $this->street1;
    }

    /**
     * Set street1 value
     *
     * @param string $street1
     * @return StreetHelp
     */
    public function setStreet1(string $street1): self
    {
        // validation for constraint: string
        if (!is_null($street1) && !is_string($street1)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($street1, true),
                gettype($street1)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($street1) && mb_strlen((string)$street1) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$street1)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($street1) && mb_strlen((string)$street1) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$street1)
            ), __LINE__);
        }
        $this->street1 = $street1;

        return $this;
    }

    /**
     * Get street2 value
     *
     * @return string
     */
    public function getStreet2(): string
    {
        return $this->street2;
    }

    /**
     * Set street2 value
     *
     * @param string $street2
     * @return StreetHelp
     */
    public function setStreet2(string $street2): self
    {
        // validation for constraint: string
        if (!is_null($street2) && !is_string($street2)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($street2, true),
                gettype($street2)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($street2) && mb_strlen((string)$street2) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$street2)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($street2) && mb_strlen((string)$street2) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$street2)
            ), __LINE__);
        }
        $this->street2 = $street2;

        return $this;
    }
}
