<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Address\EnumType\Sex;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PersonBase StructType
 *
 * @subpackage Structs
 */
abstract class PersonBase extends AbstractStructBase
{
    /**
     * The annualExpenditure
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var int|null
     */
    protected ?int $annualExpenditure = null;
    /**
     * The bankingInfo
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var BankingInfo|null
     */
    protected ?BankingInfo $bankingInfo = null;
    /**
     * The contactInfo
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var ContactInfo|null
     */
    protected ?ContactInfo $contactInfo = null;
    /**
     * The countryOfResidence
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $countryOfResidence = null;
    /**
     * The currentEmployment
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var EmploymentInfo|null
     */
    protected ?EmploymentInfo $currentEmployment = null;
    /**
     * The dob
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $dob = null;
    /**
     * The drivingLicenceNumber
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 25
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $drivingLicenceNumber = null;
    /**
     * The maritalInfo
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var MaritalInfo|null
     */
    protected ?MaritalInfo $maritalInfo = null;
    /**
     * The mothersMaidenName
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 20
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $mothersMaidenName = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var Name|null
     */
    protected ?Name $name = null;
    /**
     * The nationalInsuranceNumber
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 25
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $nationalInsuranceNumber = null;
    /**
     * The nationality
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $nationality = null;
    /**
     * The passportNumber
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 25
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $passportNumber = null;
    /**
     * The placeOfBirth
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 20
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $placeOfBirth = null;
    /**
     * The previousEmployment
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var EmploymentInfo|null
     */
    protected ?EmploymentInfo $previousEmployment = null;
    /**
     * The previousName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var Name|null
     */
    protected ?Name $previousName = null;
    /**
     * The sex
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $sex = null;

    /**
     * Constructor method for PersonBase
     *
     * @param int            $annualExpenditure
     * @param BankingInfo    $bankingInfo
     * @param ContactInfo    $contactInfo
     * @param string         $countryOfResidence
     * @param EmploymentInfo $currentEmployment
     * @param string         $dob
     * @param string         $drivingLicenceNumber
     * @param MaritalInfo    $maritalInfo
     * @param string         $mothersMaidenName
     * @param Name           $name
     * @param string         $nationalInsuranceNumber
     * @param string         $nationality
     * @param string         $passportNumber
     * @param string         $placeOfBirth
     * @param EmploymentInfo $previousEmployment
     * @param Name           $previousName
     * @param string         $sex
     * @uses PersonBase::setAnnualExpenditure()
     * @uses PersonBase::setBankingInfo()
     * @uses PersonBase::setContactInfo()
     * @uses PersonBase::setCountryOfResidence()
     * @uses PersonBase::setCurrentEmployment()
     * @uses PersonBase::setDob()
     * @uses PersonBase::setDrivingLicenceNumber()
     * @uses PersonBase::setMaritalInfo()
     * @uses PersonBase::setMothersMaidenName()
     * @uses PersonBase::setName()
     * @uses PersonBase::setNationalInsuranceNumber()
     * @uses PersonBase::setNationality()
     * @uses PersonBase::setPassportNumber()
     * @uses PersonBase::setPlaceOfBirth()
     * @uses PersonBase::setPreviousEmployment()
     * @uses PersonBase::setPreviousName()
     * @uses PersonBase::setSex()
     */
    public function __construct(
        ?int $annualExpenditure = null,
        ?BankingInfo $bankingInfo = null,
        ?ContactInfo $contactInfo = null,
        ?string $countryOfResidence = null,
        ?EmploymentInfo $currentEmployment = null,
        ?string $dob = null,
        ?string $drivingLicenceNumber = null,
        ?MaritalInfo $maritalInfo = null,
        ?string $mothersMaidenName = null,
        ?Name $name = null,
        ?string $nationalInsuranceNumber = null,
        ?string $nationality = null,
        ?string $passportNumber = null,
        ?string $placeOfBirth = null,
        ?EmploymentInfo $previousEmployment = null,
        ?Name $previousName = null,
        ?string $sex = null
    ) {
        $this
            ->setAnnualExpenditure($annualExpenditure)
            ->setBankingInfo($bankingInfo)
            ->setContactInfo($contactInfo)
            ->setCountryOfResidence($countryOfResidence)
            ->setCurrentEmployment($currentEmployment)
            ->setDob($dob)
            ->setDrivingLicenceNumber($drivingLicenceNumber)
            ->setMaritalInfo($maritalInfo)
            ->setMothersMaidenName($mothersMaidenName)
            ->setName($name)
            ->setNationalInsuranceNumber($nationalInsuranceNumber)
            ->setNationality($nationality)
            ->setPassportNumber($passportNumber)
            ->setPlaceOfBirth($placeOfBirth)
            ->setPreviousEmployment($previousEmployment)
            ->setPreviousName($previousName)
            ->setSex($sex);
    }

    /**
     * Get annualExpenditure value
     *
     * @return int|null
     */
    public function getAnnualExpenditure(): ?int
    {
        return $this->annualExpenditure;
    }

    /**
     * Set annualExpenditure value
     *
     * @param int $annualExpenditure
     * @return PersonBase
     */
    public function setAnnualExpenditure(?int $annualExpenditure = null): self
    {
        // validation for constraint: int
        if (!is_null($annualExpenditure) && !(is_int($annualExpenditure) || ctype_digit($annualExpenditure))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($annualExpenditure, true),
                gettype($annualExpenditure)
            ), __LINE__);
        }
        $this->annualExpenditure = $annualExpenditure;

        return $this;
    }

    /**
     * Get bankingInfo value
     *
     * @return BankingInfo|null
     */
    public function getBankingInfo(): ?BankingInfo
    {
        return $this->bankingInfo;
    }

    /**
     * Set bankingInfo value
     *
     * @param BankingInfo $bankingInfo
     * @return PersonBase
     */
    public function setBankingInfo(?BankingInfo $bankingInfo = null): self
    {
        $this->bankingInfo = $bankingInfo;

        return $this;
    }

    /**
     * Get contactInfo value
     *
     * @return ContactInfo|null
     */
    public function getContactInfo(): ?ContactInfo
    {
        return $this->contactInfo;
    }

    /**
     * Set contactInfo value
     *
     * @param ContactInfo $contactInfo
     * @return PersonBase
     */
    public function setContactInfo(?ContactInfo $contactInfo = null): self
    {
        $this->contactInfo = $contactInfo;

        return $this;
    }

    /**
     * Get countryOfResidence value
     *
     * @return string|null
     */
    public function getCountryOfResidence(): ?string
    {
        return $this->countryOfResidence;
    }

    /**
     * Set countryOfResidence value
     *
     * @param string $countryOfResidence
     * @return PersonBase
     */
    public function setCountryOfResidence(?string $countryOfResidence = null): self
    {
        // validation for constraint: string
        if (!is_null($countryOfResidence) && !is_string($countryOfResidence)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($countryOfResidence, true),
                gettype($countryOfResidence)
            ), __LINE__);
        }
        $this->countryOfResidence = $countryOfResidence;

        return $this;
    }

    /**
     * Get currentEmployment value
     *
     * @return EmploymentInfo|null
     */
    public function getCurrentEmployment(): ?EmploymentInfo
    {
        return $this->currentEmployment;
    }

    /**
     * Set currentEmployment value
     *
     * @param EmploymentInfo $currentEmployment
     * @return PersonBase
     */
    public function setCurrentEmployment(?EmploymentInfo $currentEmployment = null): self
    {
        $this->currentEmployment = $currentEmployment;

        return $this;
    }

    /**
     * Get dob value
     *
     * @return string|null
     */
    public function getDob(): ?string
    {
        return $this->dob;
    }

    /**
     * Set dob value
     *
     * @param string $dob
     * @return PersonBase
     */
    public function setDob(?string $dob = null): self
    {
        // validation for constraint: string
        if (!is_null($dob) && !is_string($dob)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($dob, true),
                gettype($dob)
            ), __LINE__);
        }
        $this->dob = $dob;

        return $this;
    }

    /**
     * Get drivingLicenceNumber value
     *
     * @return string|null
     */
    public function getDrivingLicenceNumber(): ?string
    {
        return $this->drivingLicenceNumber;
    }

    /**
     * Set drivingLicenceNumber value
     *
     * @param string $drivingLicenceNumber
     * @return PersonBase
     */
    public function setDrivingLicenceNumber(?string $drivingLicenceNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($drivingLicenceNumber) && !is_string($drivingLicenceNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($drivingLicenceNumber, true),
                gettype($drivingLicenceNumber)
            ), __LINE__);
        }
        // validation for constraint: maxLength(25)
        if (!is_null($drivingLicenceNumber) && mb_strlen((string)$drivingLicenceNumber) > 25) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 25',
                mb_strlen((string)$drivingLicenceNumber)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($drivingLicenceNumber) && mb_strlen((string)$drivingLicenceNumber) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$drivingLicenceNumber)
            ), __LINE__);
        }
        $this->drivingLicenceNumber = $drivingLicenceNumber;

        return $this;
    }

    /**
     * Get maritalInfo value
     *
     * @return MaritalInfo|null
     */
    public function getMaritalInfo(): ?MaritalInfo
    {
        return $this->maritalInfo;
    }

    /**
     * Set maritalInfo value
     *
     * @param MaritalInfo $maritalInfo
     * @return PersonBase
     */
    public function setMaritalInfo(?MaritalInfo $maritalInfo = null): self
    {
        $this->maritalInfo = $maritalInfo;

        return $this;
    }

    /**
     * Get mothersMaidenName value
     *
     * @return string|null
     */
    public function getMothersMaidenName(): ?string
    {
        return $this->mothersMaidenName;
    }

    /**
     * Set mothersMaidenName value
     *
     * @param string $mothersMaidenName
     * @return PersonBase
     */
    public function setMothersMaidenName(?string $mothersMaidenName = null): self
    {
        // validation for constraint: string
        if (!is_null($mothersMaidenName) && !is_string($mothersMaidenName)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($mothersMaidenName, true),
                gettype($mothersMaidenName)
            ), __LINE__);
        }
        // validation for constraint: maxLength(20)
        if (!is_null($mothersMaidenName) && mb_strlen((string)$mothersMaidenName) > 20) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 20',
                mb_strlen((string)$mothersMaidenName)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($mothersMaidenName) && mb_strlen((string)$mothersMaidenName) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$mothersMaidenName)
            ), __LINE__);
        }
        $this->mothersMaidenName = $mothersMaidenName;

        return $this;
    }

    /**
     * Get name value
     *
     * @return Name|null
     */
    public function getName(): ?Name
    {
        return $this->name;
    }

    /**
     * Set name value
     *
     * @param Name $name
     * @return PersonBase
     */
    public function setName(?Name $name = null): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get nationalInsuranceNumber value
     *
     * @return string|null
     */
    public function getNationalInsuranceNumber(): ?string
    {
        return $this->nationalInsuranceNumber;
    }

    /**
     * Set nationalInsuranceNumber value
     *
     * @param string $nationalInsuranceNumber
     * @return PersonBase
     */
    public function setNationalInsuranceNumber(?string $nationalInsuranceNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($nationalInsuranceNumber) && !is_string($nationalInsuranceNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($nationalInsuranceNumber, true),
                gettype($nationalInsuranceNumber)
            ), __LINE__);
        }
        // validation for constraint: maxLength(25)
        if (!is_null($nationalInsuranceNumber) && mb_strlen((string)$nationalInsuranceNumber) > 25) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 25',
                mb_strlen((string)$nationalInsuranceNumber)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($nationalInsuranceNumber) && mb_strlen((string)$nationalInsuranceNumber) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$nationalInsuranceNumber)
            ), __LINE__);
        }
        $this->nationalInsuranceNumber = $nationalInsuranceNumber;

        return $this;
    }

    /**
     * Get nationality value
     *
     * @return string|null
     */
    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    /**
     * Set nationality value
     *
     * @param string $nationality
     * @return PersonBase
     */
    public function setNationality(?string $nationality = null): self
    {
        // validation for constraint: string
        if (!is_null($nationality) && !is_string($nationality)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($nationality, true),
                gettype($nationality)
            ), __LINE__);
        }
        $this->nationality = $nationality;

        return $this;
    }

    /**
     * Get passportNumber value
     *
     * @return string|null
     */
    public function getPassportNumber(): ?string
    {
        return $this->passportNumber;
    }

    /**
     * Set passportNumber value
     *
     * @param string $passportNumber
     * @return PersonBase
     */
    public function setPassportNumber(?string $passportNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($passportNumber) && !is_string($passportNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($passportNumber, true),
                gettype($passportNumber)
            ), __LINE__);
        }
        // validation for constraint: maxLength(25)
        if (!is_null($passportNumber) && mb_strlen((string)$passportNumber) > 25) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 25',
                mb_strlen((string)$passportNumber)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($passportNumber) && mb_strlen((string)$passportNumber) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$passportNumber)
            ), __LINE__);
        }
        $this->passportNumber = $passportNumber;

        return $this;
    }

    /**
     * Get placeOfBirth value
     *
     * @return string|null
     */
    public function getPlaceOfBirth(): ?string
    {
        return $this->placeOfBirth;
    }

    /**
     * Set placeOfBirth value
     *
     * @param string $placeOfBirth
     * @return PersonBase
     */
    public function setPlaceOfBirth(?string $placeOfBirth = null): self
    {
        // validation for constraint: string
        if (!is_null($placeOfBirth) && !is_string($placeOfBirth)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($placeOfBirth, true),
                gettype($placeOfBirth)
            ), __LINE__);
        }
        // validation for constraint: maxLength(20)
        if (!is_null($placeOfBirth) && mb_strlen((string)$placeOfBirth) > 20) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 20',
                mb_strlen((string)$placeOfBirth)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($placeOfBirth) && mb_strlen((string)$placeOfBirth) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$placeOfBirth)
            ), __LINE__);
        }
        $this->placeOfBirth = $placeOfBirth;

        return $this;
    }

    /**
     * Get previousEmployment value
     *
     * @return EmploymentInfo|null
     */
    public function getPreviousEmployment(): ?EmploymentInfo
    {
        return $this->previousEmployment;
    }

    /**
     * Set previousEmployment value
     *
     * @param EmploymentInfo $previousEmployment
     * @return PersonBase
     */
    public function setPreviousEmployment(?EmploymentInfo $previousEmployment = null): self
    {
        $this->previousEmployment = $previousEmployment;

        return $this;
    }

    /**
     * Get previousName value
     *
     * @return Name|null
     */
    public function getPreviousName(): ?Name
    {
        return $this->previousName;
    }

    /**
     * Set previousName value
     *
     * @param Name $previousName
     * @return PersonBase
     */
    public function setPreviousName(?Name $previousName = null): self
    {
        $this->previousName = $previousName;

        return $this;
    }

    /**
     * Get sex value
     *
     * @return string|null
     */
    public function getSex(): ?string
    {
        return $this->sex;
    }

    /**
     * Set sex value
     *
     * @param string $sex
     * @return PersonBase
     * @throws InvalidArgumentException
     * @uses Sex::getValidValues
     * @uses Sex::valueIsValid
     */
    public function setSex(?string $sex = null): self
    {
        // validation for constraint: enumeration
        if (!Sex::valueIsValid($sex)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Address\EnumType\Sex',
                is_array($sex) ? implode(', ', $sex) : var_export($sex, true),
                implode(', ', Sex::getValidValues())
            ), __LINE__);
        }
        $this->sex = $sex;

        return $this;
    }
}
