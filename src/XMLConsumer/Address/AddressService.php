<?php

namespace Ratespecial\Equifax\XMLConsumer\Address;

use Ratespecial\Equifax\XMLConsumer\Address\StructType\ListAddressByPostcodeRequest;
use Ratespecial\Equifax\XMLConsumer\Address\StructType\ListAddressByPostcodeResponse;
use Ratespecial\Equifax\XMLConsumer\Address\StructType\MatchAddressRequest;
use Ratespecial\Equifax\XMLConsumer\Address\StructType\MatchAddressResponse;
use Ratespecial\Equifax\XMLConsumer\Common\AbstractXmlConsumerService;
use Ratespecial\Equifax\XMLConsumer\Exceptions\XMLConsumerException;
use SoapFault;

/**
 * Credit reference data (bureau data) is mainly related to an individual's current and previous addresses. Equifax bureau data is managed
 * using a proprietary key value that uniquely identifies each known address (known as the AddressID). Address matching is an important
 * stage in the credit search process as it facilitates the resolution of real world address details to unique address identifier values.
 * This ensures that any bureau data extracted is relevant only to the individual concerned. While other address verification methods are
 * available from the numerous PAF licensees, they cannot provide the required unique address identifier values.
 *
 * It should be noted, the service described within this section relates to the provision and matching of residential address data only and
 * cannot be used to resolve the addresses of commercial entities. The Address Matching Service is designed to be used in an interactive
 * manner and if possible the results returned to the end-user to allow the correct address to be identified. The match status indicates how
 * successful the address matching was and should be used to determine how the results are presented back. This is important where both
 * multiple matches and help data are encountered.
 *
 * It is therefore important if the address matching service is being utilised that your own system is designed to allow this level of
 * interactivity within these situations. Without this interactivity, the ability to resolve an address to a single match will be limited
 * and whilst this may not prevent any application process from continuing it will necessitate additional effort on the part of the operator.
 *
 * @see https://developer.equifax.co.uk/sites/uk/files/xml_ref_guide/guide/addressService.html
 */
class AddressService extends AbstractXmlConsumerService
{
    /**
     * This request is used to return a list of addresses for the supplied postcode. The details returned should contain the full address
     * text plus postcode and the Equifax PTCAbs key for each result. The service expects a full UK postcode to be supplied as the input and
     * will return all addresses held against that postcode. Results will by default be returned in a single response message but may be
     * returned as a subset of the total results if the size of the result set is greater than a defined service threshold1 .
     *
     * The Royal Mail work to a maximum of 150 addresses (delivery points) within a single postcode, however in the Equifax environment
     * there are a relatively small number of postcodes which contain a large number of addresses; these are usually halls of residence,
     * nursing homes etc. and these may contain up to 1500 addresses. From our experience the average number of addresses within a postcode
     * is approximately 15.
     *
     * @param string $postCode
     * @param string $clientRef Value is simply echoed back in the response.  Required.
     * @return ListAddressByPostcodeResponse
     * @throws XMLConsumerException
     * @throws SoapFault
     */
    public function listAddressByPostcode(string $postCode, string $clientRef): ListAddressByPostcodeResponse
    {
        $request = new ListAddressByPostcodeRequest($clientRef, $postCode);

        return $this->execute('listAddressByPostcode', $request);
    }

    /**
     * This request is used to validate a supplied address or addresses and return a PTC-Abs code(s).
     *
     * @param MatchAddressRequest $request
     * @return MatchAddressResponse
     * @throws XMLConsumerException
     * @throws SoapFault
     */
    public function matchAddress(MatchAddressRequest $request): MatchAddressResponse
    {
        return $this->execute('matchAddress', $request);
    }
}
