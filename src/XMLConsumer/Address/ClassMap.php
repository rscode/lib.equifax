<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Address;

/**
 * Class which returns the class map definition
 */
class ClassMap
{
    /**
     * Returns the mapping between the WSDL Structs and generated Structs' classes
     * This array is sent to the \SoapClient when calling the WS
     *
     * @return string[]
     */
    final public static function get(): array
    {
        return [
            'StructuredAddress'                    => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\StructuredAddress',
            'ResidentialStructuredAddress'         => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\ResidentialStructuredAddress',
            'MatchedResidentialStructuredAddress'  => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\MatchedResidentialStructuredAddress',
            'MatchedStructuredAddress'             => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\MatchedStructuredAddress',
            'Address'                              => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\Address',
            'FreeFormatAddress'                    => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\FreeFormatAddress',
            'PartlyStructuredAddress'              => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\PartlyStructuredAddress',
            'ResidentialFreeFormatAddress'         => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\ResidentialFreeFormatAddress',
            'ResidentialPartlyStructuredAddress'   => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\ResidentialPartlyStructuredAddress',
            'MatchedPartlyStructuredAddress'       => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\MatchedPartlyStructuredAddress',
            'BankAccount'                          => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\BankAccount',
            'Bank'                                 => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\Bank',
            'PersonBase'                           => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\PersonBase',
            'EmploymentInfo'                       => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\EmploymentInfo',
            'Employer'                             => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\Employer',
            'TelephoneNumber'                      => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\TelephoneNumber',
            'ContactInfo'                          => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\ContactInfo',
            'BankingInfo'                          => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\BankingInfo',
            'EmailAddress'                         => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\EmailAddress',
            'MaritalInfo'                          => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\MaritalInfo',
            'Person'                               => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\Person',
            'Name'                                 => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\Name',
            'ResidenceInstance'                    => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\ResidenceInstance',
            'NameBase'                             => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\NameBase',
            'PartialName'                          => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\PartialName',
            'Affordability'                        => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\Affordability',
            'ProductInformation'                   => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\ProductInformation',
            'DependentsInformation'                => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\DependentsInformation',
            'AdditionalExpenditure'                => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\AdditionalExpenditure',
            'ExpenditureSupplyMethod1'             => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\ExpenditureSupplyMethod1',
            'CurrentEmployment'                    => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\CurrentEmployment',
            'OtherIncomeFields'                    => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\OtherIncomeFields',
            'NetMonthlyIncomeFields'               => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\NetMonthlyIncomeFields',
            'GrossAnnualIncomeFields'              => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\GrossAnnualIncomeFields',
            'PropertyInformation'                  => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\PropertyInformation',
            'AdditionalSortcodeAccountInformation' => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\AdditionalSortcodeAccountInformation',
            'ResidencyInformation'                 => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\ResidencyInformation',
            'Automotive'                           => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\Automotive',
            'listAddressByPostcodeRequest'         => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\ListAddressByPostcodeRequest',
            'listAddressByPostcodeResponse'        => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\ListAddressByPostcodeResponse',
            'matchAddressRequest'                  => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\MatchAddressRequest',
            'nameAndAddress'                       => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\NameAndAddress',
            'UniqueMatch'                          => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\UniqueMatch',
            'NoMatch'                              => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\NoMatch',
            'MultipleMatch'                        => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\MultipleMatch',
            'PosttownHelp'                         => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\PosttownHelp',
            'StreetHelp'                           => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\StreetHelp',
            'HouseHelp'                            => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\HouseHelp',
            'MatchHelp'                            => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\MatchHelp',
            'MatchBase'                            => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\MatchBase',
            'MatchDetail'                          => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\MatchDetail',
            'matchAddressResponse'                 => '\\Ratespecial\\Equifax\\XMLConsumer\\Address\\StructType\\MatchAddressResponse',
            'pingRequest'                          => '\\Ratespecial\\Equifax\\XMLConsumer\\Common\\PingRequest',
            'pingResponse'                         => '\\Ratespecial\\Equifax\\XMLConsumer\\Common\\PingResponse',
            'serviceDetail'                        => '\\Ratespecial\\Equifax\\XMLConsumer\\Common\\ServiceDetail',
            'EWSFault'                             => '\\Ratespecial\\Equifax\\XMLConsumer\\Common\\EWSFault',
        ];
    }
}
