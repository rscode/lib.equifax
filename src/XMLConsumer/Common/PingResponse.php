<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Common;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for pingResponse StructType
 *
 * @subpackage Structs
 */
class PingResponse extends AbstractStructBase
{
    /**
     * The timestamp
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $timestamp;
    /**
     * The host
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $host;
    /**
     * The pid
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 10
     * - maxOccurs: 1
     * - minLength: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $pid;
    /**
     * The tid
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $tid;
    /**
     * The serviceDetail
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var ServiceDetail
     */
    protected ServiceDetail $serviceDetail;
    /**
     * The clientRef
     * Meta information extracted from the WSDL
     * - documentation: The value supplied in the request is simply echoed back in the corresponding response.
     * - base: xs:string
     * - maxLength: 40
     * - maxOccurs: 1
     * - minLength: 1
     * - minOccurs: 0
     * - pattern: .*[^\s].*
     *
     * @var string|null
     */
    protected ?string $clientRef = null;

    /**
     * Constructor method for pingResponse
     *
     * @param string $timestamp
     * @param string $host
     * @param string $pid
     * @param string $tid
     * @param ServiceDetail $serviceDetail
     * @param string $clientRef
     * @uses PingResponse::setTimestamp()
     * @uses PingResponse::setHost()
     * @uses PingResponse::setPid()
     * @uses PingResponse::setTid()
     * @uses PingResponse::setServiceDetail()
     * @uses PingResponse::setClientRef()
     */
    public function __construct(
        string $timestamp,
        string $host,
        string $pid,
        string $tid,
        ServiceDetail $serviceDetail,
        ?string $clientRef = null
    ) {
        $this
            ->setTimestamp($timestamp)
            ->setHost($host)
            ->setPid($pid)
            ->setTid($tid)
            ->setServiceDetail($serviceDetail)
            ->setClientRef($clientRef);
    }

    /**
     * Get timestamp value
     *
     * @return string
     */
    public function getTimestamp(): string
    {
        return $this->timestamp;
    }

    public function setTimestamp(string $timestamp): self
    {
        // validation for constraint: string
        if (!is_null($timestamp) && !is_string($timestamp)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($timestamp, true),
                gettype($timestamp)
            ), __LINE__);
        }
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get host value
     *
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    public function setHost(string $host): self
    {
        // validation for constraint: string
        if (!is_null($host) && !is_string($host)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($host, true),
                gettype($host)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($host) && mb_strlen((string)$host) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$host)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($host) && mb_strlen((string)$host) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$host)
            ), __LINE__);
        }
        $this->host = $host;

        return $this;
    }

    public function getPid(): string
    {
        return $this->pid;
    }

    public function setPid(string $pid): self
    {
        // validation for constraint: string
        if (!is_null($pid) && !is_string($pid)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($pid, true),
                gettype($pid)
            ), __LINE__);
        }
        // validation for constraint: maxLength(10)
        if (!is_null($pid) && mb_strlen((string)$pid) > 10) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 10',
                mb_strlen((string)$pid)
            ), __LINE__);
        }
        // validation for constraint: minLength(1)
        if (!is_null($pid) && mb_strlen((string)$pid) < 1) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 1',
                mb_strlen((string)$pid)
            ), __LINE__);
        }
        $this->pid = $pid;

        return $this;
    }

    public function getTid(): string
    {
        return $this->tid;
    }

    public function setTid(string $tid): self
    {
        // validation for constraint: string
        if (!is_null($tid) && !is_string($tid)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($tid, true),
                gettype($tid)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($tid) && mb_strlen((string)$tid) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$tid)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($tid) && mb_strlen((string)$tid) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$tid)
            ), __LINE__);
        }
        $this->tid = $tid;

        return $this;
    }

    public function getServiceDetail(): ServiceDetail
    {
        return $this->serviceDetail;
    }

    public function setServiceDetail(ServiceDetail $serviceDetail): self
    {
        $this->serviceDetail = $serviceDetail;

        return $this;
    }

    public function getClientRef(): ?string
    {
        return $this->clientRef;
    }

    public function setClientRef(?string $clientRef = null): self
    {
        // validation for constraint: string
        if (!is_null($clientRef) && !is_string($clientRef)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($clientRef, true),
                gettype($clientRef)
            ), __LINE__);
        }
        // validation for constraint: maxLength(40)
        if (!is_null($clientRef) && mb_strlen((string)$clientRef) > 40) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 40',
                mb_strlen((string)$clientRef)
            ), __LINE__);
        }
        // validation for constraint: minLength(1)
        if (!is_null($clientRef) && mb_strlen((string)$clientRef) < 1) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 1',
                mb_strlen((string)$clientRef)
            ), __LINE__);
        }
        // validation for constraint: pattern(.*[^\s].*)
        if (!is_null($clientRef) && !preg_match('/.*[^\\s].*/', $clientRef)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /.*[^\\s].*/',
                var_export($clientRef, true)
            ), __LINE__);
        }
        $this->clientRef = $clientRef;

        return $this;
    }
}
