<?php

namespace Ratespecial\Equifax\XMLConsumer\Common;

/**
 * Seems to be a generic error structure that shows up during different calls
 */
class EWSFault
{
    protected string $code;
    protected string $message;

    public function __construct(string $code = '', string $message = '')
    {
        $this
            ->setCode($code)
            ->setMessage($message);
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }
}
