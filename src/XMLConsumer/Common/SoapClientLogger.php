<?php

namespace Ratespecial\Equifax\XMLConsumer\Common;

use SoapClient;

/**
 * Logger for debugging.  Overload getSoapClientClassName() to return this in any SoapClientBase.
 *
 * class ConsumerService extends AbstractXmlConsumerService
 * {
 *      public function getSoapClientClassName(?string $soapClientClassName = null): string
 *      {
 *          return SoapClientLogger::class;
 *      }
 *      ...
 * }
 */
class SoapClientLogger extends SoapClient
{
    public function __construct(string $wsdl = null, ?array $options = null)
    {
        /**
         * Set trace option to enable logging.
         */
        $options = array_merge($options, [
            'trace' => 1,
        ]);

        parent::__construct($wsdl, $options);
    }

    /**
     * Overloading __doRequest method.
     *
     * @param string $request
     * @param string $location
     * @param string $action
     * @param int    $version
     * @param bool   $oneWay
     * @return string|null
     */
    public function __doRequest(string $request, string $location, string $action, int $version, bool $oneWay = false): ?string
    {
        $startTime = microtime(true);
        $response  = parent::__doRequest($request, $location, $action, $version, $oneWay);

        $this->log(
            $location,
            $action,
            $version,
            number_format(microtime(true) - $startTime, 4),
            parent::__getLastRequestHeaders(),
            parent::__getLastRequest(),
            parent::__getLastResponseHeaders(),
            // Sometimes get last response is null but $response has the body.
            parent::__getLastResponse() ?? $response
        );

        return $response;
    }


    protected function log(
        string $location,
        string $action,
        string $version,
        float $timing,
        string $requestHeaders = null,
        string $requestBody = null,
        string $responseHeaders = null,
        string $responseBody = null
    ): void {
        print_r($requestBody);
    }
}
