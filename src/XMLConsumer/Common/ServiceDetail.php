<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Common;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for serviceDetail StructType
 * Meta information extracted from the WSDL
 * - documentation: General Service Information.
 *
 * @subpackage Structs
 */
class ServiceDetail extends AbstractStructBase
{
    /**
     * The serviceName
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $serviceName;
    /**
     * The serviceVersion
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $serviceVersion;

    public function __construct(string $serviceName, string $serviceVersion)
    {
        $this
            ->setServiceName($serviceName)
            ->setServiceVersion($serviceVersion);
    }

    public function getServiceName(): string
    {
        return $this->serviceName;
    }

    public function setServiceName(string $serviceName): self
    {
        // validation for constraint: string
        if (!is_null($serviceName) && !is_string($serviceName)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($serviceName, true),
                gettype($serviceName)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($serviceName) && mb_strlen((string)$serviceName) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$serviceName)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($serviceName) && mb_strlen((string)$serviceName) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$serviceName)
            ), __LINE__);
        }
        $this->serviceName = $serviceName;

        return $this;
    }

    public function getServiceVersion(): string
    {
        return $this->serviceVersion;
    }

    public function setServiceVersion(string $serviceVersion): self
    {
        // validation for constraint: string
        if (!is_null($serviceVersion) && !is_string($serviceVersion)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($serviceVersion, true),
                gettype($serviceVersion)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($serviceVersion) && mb_strlen((string)$serviceVersion) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$serviceVersion)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($serviceVersion) && mb_strlen((string)$serviceVersion) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$serviceVersion)
            ), __LINE__);
        }
        $this->serviceVersion = $serviceVersion;

        return $this;
    }
}
