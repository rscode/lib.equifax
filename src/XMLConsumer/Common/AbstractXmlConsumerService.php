<?php

namespace Ratespecial\Equifax\XMLConsumer\Common;

use Ratespecial\Equifax\Common\AccessToken;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\XMLConsumer\Exceptions\XMLConsumerException;
use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

abstract class AbstractXmlConsumerService extends AbstractSoapClientBase
{
    protected EquifaxEnvironment $environment = EquifaxEnvironment::UAT;

    /**
     * @param array              $wsdlOptions See WsdlToPhp\PackageBase\SoapClientInterface.  Required values are WSDL_URL, WSDL_CLASSMAP,
     *                                        WSDL_LOCATION
     * @param string|AccessToken $accessToken Token generated from SecurityService::logon()
     */
    public function __construct(array $wsdlOptions = [], string|AccessToken $accessToken = '')
    {
        // Add the Authorization: Bearer header to the request.  This Soap extension is weird.
        if ($accessToken !== '') {
            $httpHeaders = [
                'http' => [
                    'header' => "Authorization: Bearer $accessToken \r\n",
                ]
            ];
            $context     = stream_context_create($httpHeaders);

            $wsdlOptions[self::WSDL_STREAM_CONTEXT] = $context;
        }

        parent::__construct($wsdlOptions);
    }

    public function getEnvironment(): EquifaxEnvironment
    {
        return $this->environment;
    }

    public function setEnvironment(EquifaxEnvironment $environment): self
    {
        $this->environment = $environment;

        return $this;
    }

    /**
     * Return basic info on the underlying service.
     *
     * @param string $clientRef Value is simply echoed back in the response.  Required.
     * @return PingResponse
     * @throws SoapFault
     * @throws XMLConsumerException
     */
    public function ping(string $clientRef): PingResponse
    {
        $request = new PingRequest($clientRef);

        return $this->execute('ping', $request);
    }

    protected function execute(string $method, $request)
    {
        try {
            $result = $this->getSoapClient()->__soapCall($method, [$request], outputHeaders: $this->outputHeaders);

            $this->setResult($result);

            return $result;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);

            /*
             * Structure will be something like
             * {
             *   "detail": {
             *     "logonFault": {code, message}
             *   }
             * }
             *
             * Depending on the operation, it might not be "logonFault".  So we iterate over the object and grab the value of the first
             * element to get our error.
             */
            $err = null;

            if ($soapFault->detail) {
                foreach ($soapFault->detail as $err) {
                    break;
                }
            }

            if ($err) {
                $e        = new XMLConsumerException($err->message, 0, $soapFault);
                $e->fault = new EWSFault($err->code, $err->message);
                throw $e;
            }

            throw $soapFault;
        }
    }
}
