<?php

namespace Ratespecial\Equifax\XMLConsumer\Consumer\Dictionary;

use InvalidArgumentException;

/**
 * Translate Court and Insolvency type to their descriptions.
 *
 * @see https://developer.equifax.co.uk/sites/uk/files/xml_ref_guide/ref/Data%20Dictionary/CCJ%20Data%20Types.pdf
 */
final class CCJTypes
{
    public static function describe(string $code): string
    {
        return match ($code) {
            'ADJ' => 'Adjudication (Bankruptcy)',
            'ADO' => 'Administration Order',
            'AOP' => 'Administration Order Suspended',
            'AOR' => 'Administration Order Rescinded/Revoked',
            'AOS' => 'Administration Order Superseded',
            'BKY' => 'Bankruptcy Order',
            'BOS' => 'Bill of Sale',
            'BRI' => 'Interim Bankruptcy Restriction Order',
            'BRO' => 'Bankruptcy Restriction Order',
            'BRU' => 'Bankruptcy Restriction Undertaking',
            'BSS' => 'Bill of Sale Satisfied',
            'CCL' => 'Consumer Credit Licence',
            'CD	' => 'Court Decree (Scotland)',
            'CDP' => 'Court Decree Paid',
            'CDU' => 'Unenforced Court Decree',
            'CJ	' => 'Court Judgement (England, Wales and NI)',
            'CJP' => 'Court Judgement Paid',
            'CJU' => 'Unenforced Court Judgement',
            'CJW' => 'C.C.J. Application Withdrawn',
            'CUE' => 'Certificate of Unenforceability',
            'DAR' => 'Deed of Arrangement',
            'DAS' => 'Debt Arrangement Scheme',
            'DPA' => 'Dissolution of Partnership',
            'DQD' => 'Disqualified Director',
            'DRI' => 'Interim Debt Relief Restriction Order',
            'DRO' => 'Debt Relief Order',
            'DRR' => 'Debt Relief Restriction Order',
            'DRU' => 'Debt Relief Restriction Undertaking',
            'IVA' => 'Individual Voluntary Arrangement',
            'IVF' => 'Fast Track Voluntary Arrangement',
            'LAP' => 'Liquidator Appointed',
            'LIL' => 'Low Income Low Asset/Minimal Asset Process',
            'MOC' => 'Meeting of Creditors',
            'NBO' => 'Bankruptcy Order (NI)',
            'NID' => 'Debt Relief Order (NI)',
            'NIV' => 'Individual Voluntary Arrangement (NI)',
            'ODI' => 'Order of Discharge',
            'PBL' => 'Protested Bill',
            'PDS' => 'Petition For Discharge of Sequestration',
            'PTD' => 'Protected Trust Deed',
            'ROR' => 'Receiving Order',
            'RRR' => 'Receiving Order Rescinded',
            'SBO' => 'Bankruptcy Restrict. Order (Scotland)',
            'SBU' => 'Bankruptcy Restrict. Undertaking (Scotland)',
            'SQA' => 'Sequestration Actioned',
            'SQD' => 'Discharge of Sequestration',
            'SQP' => 'Petition for Sequestration',
            'TCM' => 'Trustee Commissioner',
            'TDD' => 'Trust Deed',
            'VAR' => 'Voluntary Arrangement',
            'VWU' => 'Voluntary Winding Up',
            'WOR' => 'Winding Up Order Rescinded',
            'WUO' => 'Winding Up Order',
            default => throw new InvalidArgumentException("Unknown code: $code")
        };
    }
}
