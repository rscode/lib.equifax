<?php

namespace Ratespecial\Equifax\XMLConsumer\Consumer\Dictionary;

use InvalidArgumentException;

/**
 * Translate Company types to their descriptions.
 *
 * @see https://developer.equifax.co.uk/sites/uk/files/xml_ref_guide/ref/Data%20Dictionary/Company%20Types.pdf
 */
final class CompanyTypes
{
    public static function describe(string $code): string
    {
        return match ($code) {
            'AC' => 'Accountancy',
            'BF' => 'Bank (Non-CLSB and Finance Divisions)',
            'BK' => 'Bank',
            'BS' => 'Building Society',
            'CB' => 'Credit Broker',
            'CC' => 'Credit Card',
            'CE' => 'Customs and Excise',
            'CG' => 'Cheque Guarantor',
            'CH' => 'Charge Card',
            'CI' => 'Credit Insurer',
            'CL' => 'Consumer Letter',
            'CR' => 'Commercial Reporting',
            'CS' => 'Communication Supplier',
            'DC' => 'Debt Collector',
            'DI' => 'Distribution and Wholesalers',
            'EA' => 'Employment Agency',
            'EN' => 'Enquiry Agent',
            'ES' => 'Energy Supplier',
            'FN' => 'Finance House',
            'FS' => 'Financial Services',
            'GI' => 'General Insurance',
            'GV' => 'Government',
            'HC' => 'Hire Car Rental',
            'HF' => 'Home Furnisher',
            'HI' => 'Home Improvement',
            'HO' => 'House Builder',
            'HS' => 'Health Services',
            'HT' => 'Hotel and Travel',
            'IB' => 'Insurance Broker',
            'IN' => 'Insurance',
            'IR' => 'Inland Revenue',
            'LA' => 'Loss Adjuster',
            'LG' => 'Leasing',
            'MD' => 'Motor Dealer',
            'ME' => 'Media',
            'MF' => 'Manufacturing / Industrial',
            'MK' => 'Marketing',
            'MN' => 'Miscellaneous',
            'MO' => 'Mail Order',
            'MS' => 'Mortgage Supplier',
            'OS' => 'Overseas',
            'PM' => 'Property Management',
            'PO' => 'Police',
            'PU' => 'Public Utility',
            'RN' => 'Rental',
            'RT' => 'Retailer',
            'SB' => 'Stock Broker',
            'SO' => 'Solicitor',
            'SR' => 'Slot Rental',
            'SS' => 'Security Services',
            'TC' => 'TV Programme Supplier',
            'TI' => 'Travel Insurer',
            'TP' => 'Third party',
            'TV' => 'TV Rental',
            'XX' => 'Training',
            default => throw new InvalidArgumentException("Unknown code: $code")
        };
    }
}
