<?php

namespace Ratespecial\Equifax\XMLConsumer\Consumer\Dictionary;

use InvalidArgumentException;

/**
 * Translate Court and Insolvency type to their descriptions.
 *
 * @see https://developer.equifax.co.uk/sites/uk/files/xml_ref_guide/ref/Data%20Dictionary/Name%20Match%20Levels.pdf
 */
final class NameMatchLevel
{
    public static function describe(string $code): string
    {
        return match ($code) {
            'A' => 'Subject 1 Details (including aliases)',
            'B' => 'Subject 2 Details (including aliases)',
            'C' => 'Associate 1 of Subject 1 Details (including aliases)',
            'D' => 'Associate 2 of Subject 1 details',
            'E' => 'Associate 3 of Subject 1 details',
            'F' => 'Associate 1 of Subject 2 Details (including aliases)',
            'G' => 'Associate 2 of Subject 2 details',
            'H' => 'Associate 3 of Subject 2 details',
            'I' => 'Potential Associate 1 of Subject 1 details and family',
            'J' => 'Potential Associate 2 of Subject 1 details and family',
            'K' => 'Potential Associate 3 of Subject 1 details and family',
            'L' => 'Potential Associate 1 of Subject 2 details and family',
            'M' => 'Potential Associate 2 of Subject 2 details and family',
            'N' => 'Potential Associate 3 of Subject 2 details and family',
            'P' => 'Potential Associate 1 of subject 1 details',
            'Q' => 'Potential Associate 2 of subject 1 details',
            'R' => 'Potential Associate 3 of subject 1 details',
            'S' => 'Potential Associate 1 of subject 2 details',
            'T' => 'Potential Associate 2 of subject 2 details',
            'U' => 'Potential Associate 3 of subject 2 details',
            'V' => 'Family Member of Subject 1',
            'W' => 'Family Member of Subject 2',
            'X' => 'Attributable Data Subject 1 and family',
            'Y' => 'Attributable Data Subject 2 and family',
            'Z' => 'Other Person',
            default => throw new InvalidArgumentException("Unknown code: $code")
        };
    }
}
