<?php

namespace Ratespecial\Equifax\XMLConsumer\Consumer\Dictionary;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\PaymentStatus;

/**
 * Translate insight payment statuses to their descriptions.
 *
 * @see https://developer.equifax.co.uk/sites/uk/files/xml_ref_guide/ref/Data%20Dictionary/Insight%20Payment%20Statuses.pdf
 */
final class InsightPaymentStatuses
{
    public static function describe(string $code): string
    {
        return match ($code) {
            PaymentStatus::VALUE_DOT => 'No update received',
            PaymentStatus::VALUE_ZERO => 'Up to date with payments',
            PaymentStatus::VALUE_ONE => '1 payment in arrears',
            PaymentStatus::VALUE_TWO => '2 payments in arrears',
            PaymentStatus::VALUE_THREE => '3 payments in arrears',
            PaymentStatus::VALUE_FOUR => '4 payments in arrears',
            PaymentStatus::VALUE_FIVE => '5 payments in arrears',
            PaymentStatus::VALUE_SIX => '6 or more payments in arrears',
            PaymentStatus::VALUE_A => 'Moderate arrears',
            PaymentStatus::VALUE_B => 'Bad arrears',
            PaymentStatus::VALUE_I => 'Arrangement to pay',
            PaymentStatus::VALUE_S => 'Settled or Satisfied',
            PaymentStatus::VALUE_U => 'No payment due yet or unclassified',
            PaymentStatus::VALUE_R => 'Repossession',
            PaymentStatus::VALUE_D => 'Default',
            PaymentStatus::VALUE_Q => 'Query (account under review)',
            PaymentStatus::VALUE_G => 'Gone away',
            PaymentStatus::VALUE_N => 'Inactive',
            PaymentStatus::VALUE_Z => 'Never taken up',
            PaymentStatus::VALUE_V => 'Goods voluntarily surrendered',
            PaymentStatus::VALUE_W => 'Written off',
            PaymentStatus::VALUE_X => 'Transfer',
            default => throw new InvalidArgumentException("Unknown payment status code: $code")
        };
    }
}
