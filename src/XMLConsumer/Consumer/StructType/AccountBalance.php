<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AccountBalance StructType
 *
 * @subpackage Structs
 */
class AccountBalance extends AbstractStructBase
{
    /**
     * The balanceAmount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var MonetaryAmount|null
     */
    protected ?MonetaryAmount $balanceAmount = null;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;
    /**
     * The suppressed
     * Meta information extracted from the WSDL
     * - use: optional
     *
     * @var bool|null
     */
    protected ?bool $suppressed = null;

    /**
     * Constructor method for AccountBalance
     *
     * @param MonetaryAmount          $balanceAmount
     * @param DOMDocument|string|null $any
     * @param bool                    $suppressed
     * @uses AccountBalance::setBalanceAmount()
     * @uses AccountBalance::setAny()
     * @uses AccountBalance::setSuppressed()
     */
    public function __construct(?MonetaryAmount $balanceAmount = null, $any = null, ?bool $suppressed = null)
    {
        $this
            ->setBalanceAmount($balanceAmount)
            ->setAny($any)
            ->setSuppressed($suppressed);
    }

    /**
     * Get balanceAmount value
     *
     * @return MonetaryAmount|null
     */
    public function getBalanceAmount(): ?MonetaryAmount
    {
        return $this->balanceAmount;
    }

    /**
     * Set balanceAmount value
     *
     * @param MonetaryAmount $balanceAmount
     * @return AccountBalance
     */
    public function setBalanceAmount(?MonetaryAmount $balanceAmount = null): self
    {
        $this->balanceAmount = $balanceAmount;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return AccountBalance
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }

    /**
     * Get suppressed value
     *
     * @return bool|null
     */
    public function getSuppressed(): ?bool
    {
        return $this->suppressed;
    }

    /**
     * Set suppressed value
     *
     * @param bool $suppressed
     * @return AccountBalance
     */
    public function setSuppressed(?bool $suppressed = null): self
    {
        // validation for constraint: boolean
        if (!is_null($suppressed) && !is_bool($suppressed)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($suppressed, true),
                gettype($suppressed)
            ), __LINE__);
        }
        $this->suppressed = $suppressed;

        return $this;
    }
}
