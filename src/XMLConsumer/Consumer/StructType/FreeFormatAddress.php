<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for FreeFormatAddress StructType
 *
 * @subpackage Structs
 */
class FreeFormatAddress extends Address
{
    /**
     * The line
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 192
     * - maxOccurs: 6
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string[]
     */
    protected ?array $line = null;

    /**
     * Constructor method for FreeFormatAddress
     *
     * @param string[] $line
     * @uses FreeFormatAddress::setLine()
     */
    public function __construct(?array $line = null)
    {
        $this
            ->setLine($line);
    }

    /**
     * This method is responsible for validating the values passed to the setLine method
     * This method is willingly generated in order to preserve the one-line inline validation within the setLine method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateLineForArrayConstraintsFromSetLine(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $freeFormatAddressLineItem) {
            // validation for constraint: itemType
            if (!is_string($freeFormatAddressLineItem)) {
                $invalidValues[] = is_object($freeFormatAddressLineItem) ? get_class($freeFormatAddressLineItem) : sprintf(
                    '%s(%s)',
                    gettype($freeFormatAddressLineItem),
                    var_export($freeFormatAddressLineItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The line property can only contain items of type string, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * This method is responsible for validating the value passed to the setLine method
     * This method is willingly generated in order to preserve the one-line inline validation within the setLine method
     * This has to validate that the items contained by the array match the length constraint
     *
     * @param mixed $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateLineForMaxLengthConstraintFromSetLine($values): string
    {
        $message       = '';
        $invalidValues = [];
        foreach ($values as $freeFormatAddressLineItem) {
            // validation for constraint: maxLength(192)
            if (mb_strlen((string)$freeFormatAddressLineItem) > 192) {
                $invalidValues[] = var_export($freeFormatAddressLineItem, true);
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'Invalid length for value(s) %s, the number of characters/octets contained by the literal must be less than or equal to 192',
                implode(', ', $invalidValues)
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * This method is responsible for validating the value passed to the setLine method
     * This method is willingly generated in order to preserve the one-line inline validation within the setLine method
     * This has to validate that the items contained by the array match the length constraint
     *
     * @param mixed $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateLineForMinLengthConstraintFromSetLine($values): string
    {
        $message       = '';
        $invalidValues = [];
        foreach ($values as $freeFormatAddressLineItem) {
            // validation for constraint: minLength
            if (mb_strlen((string)$freeFormatAddressLineItem) < 0) {
                $invalidValues[] = var_export($freeFormatAddressLineItem, true);
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'Invalid length for value(s) %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                implode(', ', $invalidValues)
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get line value
     *
     * @return string[]
     */
    public function getLine(): ?array
    {
        return $this->line;
    }

    /**
     * Set line value
     *
     * @param string[] $line
     * @return FreeFormatAddress
     * @throws InvalidArgumentException
     */
    public function setLine(?array $line = null): self
    {
        // validation for constraint: array
        if ('' !== ($lineArrayErrorMessage = self::validateLineForArrayConstraintsFromSetLine($line))) {
            throw new InvalidArgumentException($lineArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxLength(192)
        if ('' !== ($lineMaxLengthErrorMessage = self::validateLineForMaxLengthConstraintFromSetLine($line))) {
            throw new InvalidArgumentException($lineMaxLengthErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(6)
        if (is_array($line) && count($line) > 6) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 6',
                count($line)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if ('' !== ($lineMinLengthErrorMessage = self::validateLineForMinLengthConstraintFromSetLine($line))) {
            throw new InvalidArgumentException($lineMinLengthErrorMessage, __LINE__);
        }
        $this->line = $line;

        return $this;
    }

    /**
     * Add item to line value
     *
     * @param string $item
     * @return FreeFormatAddress
     * @throws InvalidArgumentException
     */
    public function addToLine(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf(
                'The line property can only contain items of type string, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxLength(192)
        if (mb_strlen((string)$item) > 192) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 192',
                mb_strlen((string)$item)
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(6)
        if (is_array($this->line) && count($this->line) >= 6) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 6',
                count($this->line)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (mb_strlen((string)$item) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$item)
            ), __LINE__);
        }
        $this->line[] = $item;

        return $this;
    }
}
