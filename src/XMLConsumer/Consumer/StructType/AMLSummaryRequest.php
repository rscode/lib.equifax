<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for AMLSummaryRequest StructType
 *
 * @subpackage Structs
 */
class AMLSummaryRequest extends CodedDataRequest
{
}
