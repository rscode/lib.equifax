<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for PreviousSearchContainer StructType
 *
 * @subpackage Structs
 */
class PreviousSearchContainer extends DataItemContainer
{
    /**
     * The previousCreditSearch
     *
     * @var PreviousCreditSearch[]|null
     */
    protected ?array $previousCreditSearch = null;
    /**
     * The previousNonCreditSearch
     *
     * @var PreviousNonCreditSearch[]|null
     */
    protected ?array $previousNonCreditSearch = null;

    public function __construct(
        ?array $previousCreditSearch = null,
        ?array $previousNonCreditSearch = null,
    ) {
        $this
            ->setPreviousCreditSearch($previousCreditSearch)
            ->setPreviousNonCreditSearch($previousNonCreditSearch);
    }

    public function getPreviousCreditSearch(): ?array
    {
        return $this->previousCreditSearch;
    }

    public function setPreviousCreditSearch(?array $previousCreditSearch): PreviousSearchContainer
    {
        $this->previousCreditSearch = $previousCreditSearch;

        return $this;
    }

    public function getPreviousNonCreditSearch(): ?array
    {
        return $this->previousNonCreditSearch;
    }

    public function setPreviousNonCreditSearch(?array $previousNonCreditSearch): PreviousSearchContainer
    {
        $this->previousNonCreditSearch = $previousNonCreditSearch;

        return $this;
    }
}
