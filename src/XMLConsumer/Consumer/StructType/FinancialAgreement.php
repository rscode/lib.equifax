<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\CompanyClass;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\InsightSupplementaryInformation;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\PaymentFrequency;

/**
 * @see https://developer.equifax.co.uk/sites/uk/files/xml_ref_guide/ref/Data%20Groups/Insight.pdf
 */
abstract class FinancialAgreement extends NameMatchedData
{
    /**
     * The companyClass
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $companyClass;
    /**
     * The insightQuality
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var InsightQuality
     */
    protected InsightQuality $insightQuality;
    /**
     * Record identifier, only populated if the record belongs to the enquiring company
     *
     * @var string|null
     */
    protected ?string $accountNumber = null;
    /**
     * Up to 48 months of payment status history, first entry is the current status. Age in months indicates where the status
     * occurred; see Insight Payment Statuses for status values. For the first entry (ageInMonths of 0), several other properties
     * are present: account balance (current balance), credit limit and statement (for credit card accounts). The statement
     * property includes: cash advance count, cash advance value, credit limit change description, whether the minimum
     * payment is being paid, the payment amount, whether a promotional rate is being applied and the statement balance
     *
     * @var AgreementPreviousState[]
     */
    protected ?array $paymentHistory = null;
    /**
     * Equifax client identifier, only populated if the record belongs to the enquiring company
     *
     * @var string|null
     */
    protected ?string $clientNumber = null;
    /**
     * The creditLimit
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var CreditLimit|null
     */
    protected ?CreditLimit $creditLimit = null;
    /**
     * Amount of debt outstanding (in pounds) and currency for active accounts (can be zero), or, zero for satisfied accounts
     *
     * @var AccountBalance|null
     */
    protected ?AccountBalance $currentBalance = null;
    /**
     * Default or delinquent (depending on current status) balance (in pounds) and currency and point where the status was
     * recorded
     *
     * @var AccountBalance|null
     */
    protected ?AccountBalance $defaultBalance = null;
    /**
     * Date of final payment for satisfied or settled accounts or date of default (CCYY-MM-DD format)
     *
     * @var string|null
     */
    protected ?string $endDate = null;
    /**
     * The fixedPaymentTerms
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var FixedPaymentTerms|null
     */
    protected ?FixedPaymentTerms $fixedPaymentTerms = null;
    /**
     * The supplementaryInformation
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $supplementaryInformation = null;
    /**
     * The lastDelinquentDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * - union: date | gYear | gYearMonth | MMYY | MMYYYY
     *
     * @var string|null
     */
    protected ?string $lastDelinquentDate = null;
    /**
     * The lastUpdateDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $lastUpdateDate = null;
    /**
     * The paymentFrequency
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $paymentFrequency = null;
    /**
     * The startBalance
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AccountBalance|null
     */
    protected ?AccountBalance $startBalance = null;
    /**
     * Either agreement date, date of loan or date of credit card issue (CCYY-MM-DD format)
     *
     * @var string|null
     */
    protected ?string $startDate = null;
    /**
     * Name to be included is same as Company name
     *
     * @var string|null
     */
    protected ?string $companyName = null;
    /**
     * 3 byte data group sequencing in line with other insight
     *
     * @var int|null
     */
    protected ?int $insightSequenceNumber = null;
    /**
     * The durationOfAccount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $durationOfAccount = null;
    /**
     * Joint (T) or Sole Account (E) indication.
     *
     * @var string|null
     */
    protected ?string $additionalSupplementaryInformation = null;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    public function __construct(
        string $companyClass,
        InsightQuality $insightQuality,
        ?string $accountNumber = null,
        ?array $paymentHistory = null,
        ?string $clientNumber = null,
        ?CreditLimit $creditLimit = null,
        ?AccountBalance $currentBalance = null,
        ?AccountBalance $defaultBalance = null,
        ?string $endDate = null,
        ?FixedPaymentTerms $fixedPaymentTerms = null,
        ?string $supplementaryInformation = null,
        ?string $lastDelinquentDate = null,
        ?string $lastUpdateDate = null,
        ?string $paymentFrequency = null,
        ?AccountBalance $startBalance = null,
        ?string $startDate = null,
        ?string $companyName = null,
        ?int $insightSequenceNumber = null,
        ?string $durationOfAccount = null,
        ?string $additionalSupplementaryInformation = null,
        $any = null
    ) {
        $this
            ->setCompanyClass($companyClass)
            ->setInsightQuality($insightQuality)
            ->setAccountNumber($accountNumber)
            ->setPaymentHistory($paymentHistory)
            ->setClientNumber($clientNumber)
            ->setCreditLimit($creditLimit)
            ->setCurrentBalance($currentBalance)
            ->setDefaultBalance($defaultBalance)
            ->setEndDate($endDate)
            ->setFixedPaymentTerms($fixedPaymentTerms)
            ->setSupplementaryInformation($supplementaryInformation)
            ->setLastDelinquentDate($lastDelinquentDate)
            ->setLastUpdateDate($lastUpdateDate)
            ->setPaymentFrequency($paymentFrequency)
            ->setStartBalance($startBalance)
            ->setStartDate($startDate)
            ->setCompanyName($companyName)
            ->setInsightSequenceNumber($insightSequenceNumber)
            ->setDurationOfAccount($durationOfAccount)
            ->setAdditionalSupplementaryInformation($additionalSupplementaryInformation)
            ->setAny($any);
    }

    /**
     * This method is responsible for validating the values passed to the setPaymentHistory method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPaymentHistory method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePaymentHistoryForArrayConstraintsFromSetPaymentHistory(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $financialAgreementPaymentHistoryItem) {
            // validation for constraint: itemType
            if (!$financialAgreementPaymentHistoryItem instanceof AgreementPreviousState) {
                $invalidValues[] = is_object($financialAgreementPaymentHistoryItem) ? get_class($financialAgreementPaymentHistoryItem) : sprintf(
                    '%s(%s)',
                    gettype($financialAgreementPaymentHistoryItem),
                    var_export($financialAgreementPaymentHistoryItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The paymentHistory property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AgreementPreviousState, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * This method is responsible for validating the value passed to the setLastDelinquentDate method
     * This method is willingly generated in order to preserve the one-line inline validation within the setLastDelinquentDate method
     * This is a set of validation rules based on the union types associated to the property being set by the setLastDelinquentDate method
     *
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateLastDelinquentDateForUnionConstraintsFromSetLastDelinquentDate($value): string
    {
        $message = '';
        // validation for constraint: int
        if (!is_null($value) && !(is_int($value) || ctype_digit($value))) {
            $exception0 = new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($value, true),
                gettype($value)
            ), __LINE__);
        }
        // validation for constraint: pattern((0[123456789]|1[012]){1}\d{2})
        if (!is_null($value) && !preg_match('/(0[123456789]|1[012]){1}\\d{2}/', $value)) {
            $exception1 = new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /(0[123456789]|1[012]){1}\\d{2}/',
                var_export($value, true)
            ), __LINE__);
        }
        // validation for constraint: pattern((0[123456789]|1[012]){1}\d{4})
        if (!is_null($value) && !preg_match('/(0[123456789]|1[012]){1}\\d{4}/', $value)) {
            $exception2 = new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /(0[123456789]|1[012]){1}\\d{4}/',
                var_export($value, true)
            ), __LINE__);
        }
        if (isset($exception0) && isset($exception1) && isset($exception2)) {
            $message = sprintf(
                "The value %s does not match any of the union rules: date, gYear, gYearMonth, MMYY, MMYYYY. See following errors:\n%s",
                var_export($value, true),
                implode("\n", array_map(function (InvalidArgumentException $e) {
                    return sprintf(' - %s', $e->getMessage());
                },
                [$exception0, $exception1, $exception2]))
            );
        }
        unset($exception0, $exception1, $exception2);

        return $message;
    }

    /**
     * Get companyClass value
     *
     * @return string
     */
    public function getCompanyClass(): string
    {
        return $this->companyClass;
    }

    /**
     * Set companyClass value
     *
     * @param string $companyClass
     * @return FinancialAgreement
     * @throws InvalidArgumentException
     * @uses CompanyClass::getValidValues
     * @uses CompanyClass::valueIsValid
     */
    public function setCompanyClass(string $companyClass): self
    {
        // validation for constraint: enumeration
        if (!CompanyClass::valueIsValid($companyClass)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\CompanyClass',
                is_array($companyClass) ? implode(', ', $companyClass) : var_export($companyClass, true),
                implode(', ', CompanyClass::getValidValues())
            ), __LINE__);
        }
        $this->companyClass = $companyClass;

        return $this;
    }

    /**
     * Get insightQuality value
     *
     * @return InsightQuality
     */
    public function getInsightQuality(): InsightQuality
    {
        return $this->insightQuality;
    }

    /**
     * Set insightQuality value
     *
     * @param InsightQuality $insightQuality
     * @return FinancialAgreement
     */
    public function setInsightQuality(InsightQuality $insightQuality): self
    {
        $this->insightQuality = $insightQuality;

        return $this;
    }

    /**
     * Get accountNumber value
     *
     * @return string|null
     */
    public function getAccountNumber(): ?string
    {
        return $this->accountNumber;
    }

    /**
     * Set accountNumber value
     *
     * @param string $accountNumber
     * @return FinancialAgreement
     */
    public function setAccountNumber(?string $accountNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($accountNumber) && !is_string($accountNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($accountNumber, true),
                gettype($accountNumber)
            ), __LINE__);
        }
        // validation for constraint: maxLength(20)
        if (!is_null($accountNumber) && mb_strlen((string)$accountNumber) > 20) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 20',
                mb_strlen((string)$accountNumber)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($accountNumber) && mb_strlen((string)$accountNumber) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$accountNumber)
            ), __LINE__);
        }
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Get paymentHistory value
     *
     * @return AgreementPreviousState[]
     */
    public function getPaymentHistory(): ?array
    {
        return $this->paymentHistory;
    }

    /**
     * Set paymentHistory value
     *
     * @param AgreementPreviousState[] $paymentHistory
     * @return FinancialAgreement
     * @throws InvalidArgumentException
     */
    public function setPaymentHistory(?array $paymentHistory = null): self
    {
        // validation for constraint: array
        if ('' !== ($paymentHistoryArrayErrorMessage = self::validatePaymentHistoryForArrayConstraintsFromSetPaymentHistory($paymentHistory))) {
            throw new InvalidArgumentException($paymentHistoryArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(48)
        if (is_array($paymentHistory) && count($paymentHistory) > 48) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 48',
                count($paymentHistory)
            ), __LINE__);
        }
        $this->paymentHistory = $paymentHistory;

        return $this;
    }

    /**
     * Add item to paymentHistory value
     *
     * @param AgreementPreviousState $item
     * @return FinancialAgreement
     * @throws InvalidArgumentException
     */
    public function addToPaymentHistory(AgreementPreviousState $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof AgreementPreviousState) {
            throw new InvalidArgumentException(sprintf(
                'The paymentHistory property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AgreementPreviousState, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(48)
        if (is_array($this->paymentHistory) && count($this->paymentHistory) >= 48) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 48',
                count($this->paymentHistory)
            ), __LINE__);
        }
        $this->paymentHistory[] = $item;

        return $this;
    }

    /**
     * Get clientNumber value
     *
     * @return string|null
     */
    public function getClientNumber(): ?string
    {
        return $this->clientNumber;
    }

    /**
     * Set clientNumber value
     *
     * @param string $clientNumber
     * @return FinancialAgreement
     */
    public function setClientNumber(?string $clientNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($clientNumber) && !is_string($clientNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($clientNumber, true),
                gettype($clientNumber)
            ), __LINE__);
        }
        // validation for constraint: maxLength(8)
        if (!is_null($clientNumber) && mb_strlen((string)$clientNumber) > 8) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 8',
                mb_strlen((string)$clientNumber)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($clientNumber) && mb_strlen((string)$clientNumber) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$clientNumber)
            ), __LINE__);
        }
        $this->clientNumber = $clientNumber;

        return $this;
    }

    /**
     * Get creditLimit value
     *
     * @return CreditLimit|null
     */
    public function getCreditLimit(): ?CreditLimit
    {
        return $this->creditLimit;
    }

    /**
     * Set creditLimit value
     *
     * @param CreditLimit $creditLimit
     * @return FinancialAgreement
     */
    public function setCreditLimit(?CreditLimit $creditLimit = null): self
    {
        $this->creditLimit = $creditLimit;

        return $this;
    }

    /**
     * Get currentBalance value
     *
     * @return AccountBalance|null
     */
    public function getCurrentBalance(): ?AccountBalance
    {
        return $this->currentBalance;
    }

    /**
     * Set currentBalance value
     *
     * @param AccountBalance $currentBalance
     * @return FinancialAgreement
     */
    public function setCurrentBalance(?AccountBalance $currentBalance = null): self
    {
        $this->currentBalance = $currentBalance;

        return $this;
    }

    /**
     * Get defaultBalance value
     *
     * @return AccountBalance|null
     */
    public function getDefaultBalance(): ?AccountBalance
    {
        return $this->defaultBalance;
    }

    /**
     * Set defaultBalance value
     *
     * @param AccountBalance $defaultBalance
     * @return FinancialAgreement
     */
    public function setDefaultBalance(?AccountBalance $defaultBalance = null): self
    {
        $this->defaultBalance = $defaultBalance;

        return $this;
    }

    /**
     * Get endDate value
     *
     * @return string|null
     */
    public function getEndDate(): ?string
    {
        return $this->endDate;
    }

    /**
     * Set endDate value
     *
     * @param string $endDate
     * @return FinancialAgreement
     */
    public function setEndDate(?string $endDate = null): self
    {
        // validation for constraint: string
        if (!is_null($endDate) && !is_string($endDate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($endDate, true),
                gettype($endDate)
            ), __LINE__);
        }
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get fixedPaymentTerms value
     *
     * @return FixedPaymentTerms|null
     */
    public function getFixedPaymentTerms(): ?FixedPaymentTerms
    {
        return $this->fixedPaymentTerms;
    }

    /**
     * Set fixedPaymentTerms value
     *
     * @param FixedPaymentTerms $fixedPaymentTerms
     * @return FinancialAgreement
     */
    public function setFixedPaymentTerms(?FixedPaymentTerms $fixedPaymentTerms = null): self
    {
        $this->fixedPaymentTerms = $fixedPaymentTerms;

        return $this;
    }

    /**
     * Get supplementaryInformation value
     *
     * @return string|null
     */
    public function getSupplementaryInformation(): ?string
    {
        return $this->supplementaryInformation;
    }

    /**
     * Set supplementaryInformation value
     *
     * @param string $supplementaryInformation
     * @return FinancialAgreement
     * @throws InvalidArgumentException
     * @uses InsightSupplementaryInformation::getValidValues
     * @uses InsightSupplementaryInformation::valueIsValid
     */
    public function setSupplementaryInformation(?string $supplementaryInformation = null): self
    {
        // validation for constraint: enumeration
        if (!InsightSupplementaryInformation::valueIsValid($supplementaryInformation)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\InsightSupplementaryInformation',
                is_array($supplementaryInformation) ? implode(', ', $supplementaryInformation) : var_export(
                    $supplementaryInformation,
                    true
                ),
                implode(', ', InsightSupplementaryInformation::getValidValues())
            ), __LINE__);
        }
        $this->supplementaryInformation = $supplementaryInformation;

        return $this;
    }

    /**
     * Get lastDelinquentDate value
     *
     * @return string|null
     */
    public function getLastDelinquentDate(): ?string
    {
        return $this->lastDelinquentDate;
    }

    /**
     * Set lastDelinquentDate value
     *
     * @param string $lastDelinquentDate
     * @return FinancialAgreement
     */
    public function setLastDelinquentDate(?string $lastDelinquentDate = null): self
    {
        // validation for constraint: string
        if (!is_null($lastDelinquentDate) && !is_string($lastDelinquentDate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($lastDelinquentDate, true),
                gettype($lastDelinquentDate)
            ), __LINE__);
        }
        // validation for constraint: union(date, gYear, gYearMonth, MMYY, MMYYYY)
        if ('' !== ($lastDelinquentDateUnionErrorMessage = self::validateLastDelinquentDateForUnionConstraintsFromSetLastDelinquentDate($lastDelinquentDate))) {
            throw new InvalidArgumentException($lastDelinquentDateUnionErrorMessage, __LINE__);
        }
        $this->lastDelinquentDate = $lastDelinquentDate;

        return $this;
    }

    /**
     * Get lastUpdateDate value
     *
     * @return string|null
     */
    public function getLastUpdateDate(): ?string
    {
        return $this->lastUpdateDate;
    }

    /**
     * Set lastUpdateDate value
     *
     * @param string $lastUpdateDate
     * @return FinancialAgreement
     */
    public function setLastUpdateDate(?string $lastUpdateDate = null): self
    {
        // validation for constraint: string
        if (!is_null($lastUpdateDate) && !is_string($lastUpdateDate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($lastUpdateDate, true),
                gettype($lastUpdateDate)
            ), __LINE__);
        }
        $this->lastUpdateDate = $lastUpdateDate;

        return $this;
    }

    /**
     * Get paymentFrequency value
     *
     * @return string|null
     */
    public function getPaymentFrequency(): ?string
    {
        return $this->paymentFrequency;
    }

    /**
     * Set paymentFrequency value
     *
     * @param string $paymentFrequency
     * @return FinancialAgreement
     * @throws InvalidArgumentException
     * @uses PaymentFrequency::getValidValues
     * @uses PaymentFrequency::valueIsValid
     */
    public function setPaymentFrequency(?string $paymentFrequency = null): self
    {
        // validation for constraint: enumeration
        if (!PaymentFrequency::valueIsValid($paymentFrequency)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\PaymentFrequency',
                is_array($paymentFrequency) ? implode(', ', $paymentFrequency) : var_export($paymentFrequency, true),
                implode(', ', PaymentFrequency::getValidValues())
            ), __LINE__);
        }
        $this->paymentFrequency = $paymentFrequency;

        return $this;
    }

    /**
     * Get startBalance value
     *
     * @return AccountBalance|null
     */
    public function getStartBalance(): ?AccountBalance
    {
        return $this->startBalance;
    }

    /**
     * Set startBalance value
     *
     * @param AccountBalance $startBalance
     * @return FinancialAgreement
     */
    public function setStartBalance(?AccountBalance $startBalance = null): self
    {
        $this->startBalance = $startBalance;

        return $this;
    }

    /**
     * Get startDate value
     *
     * @return string|null
     */
    public function getStartDate(): ?string
    {
        return $this->startDate;
    }

    /**
     * Set startDate value
     *
     * @param string $startDate
     * @return FinancialAgreement
     */
    public function setStartDate(?string $startDate = null): self
    {
        // validation for constraint: string
        if (!is_null($startDate) && !is_string($startDate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($startDate, true),
                gettype($startDate)
            ), __LINE__);
        }
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get companyName value
     *
     * @return string|null
     */
    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    /**
     * Set companyName value
     *
     * @param string $companyName
     * @return FinancialAgreement
     */
    public function setCompanyName(?string $companyName = null): self
    {
        // validation for constraint: string
        if (!is_null($companyName) && !is_string($companyName)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($companyName, true),
                gettype($companyName)
            ), __LINE__);
        }
        // validation for constraint: maxLength(40)
        if (!is_null($companyName) && mb_strlen((string)$companyName) > 40) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 40',
                mb_strlen((string)$companyName)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($companyName) && mb_strlen((string)$companyName) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$companyName)
            ), __LINE__);
        }
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get insightSequenceNumber value
     *
     * @return int|null
     */
    public function getInsightSequenceNumber(): ?int
    {
        return $this->insightSequenceNumber;
    }

    /**
     * Set insightSequenceNumber value
     *
     * @param int $insightSequenceNumber
     * @return FinancialAgreement
     */
    public function setInsightSequenceNumber(?int $insightSequenceNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($insightSequenceNumber) && !(is_int($insightSequenceNumber) || ctype_digit($insightSequenceNumber))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($insightSequenceNumber, true),
                gettype($insightSequenceNumber)
            ), __LINE__);
        }
        $this->insightSequenceNumber = $insightSequenceNumber;

        return $this;
    }

    /**
     * Get durationOfAccount value
     *
     * @return string|null
     */
    public function getDurationOfAccount(): ?string
    {
        return $this->durationOfAccount;
    }

    /**
     * Set durationOfAccount value
     *
     * @param string $durationOfAccount
     * @return FinancialAgreement
     */
    public function setDurationOfAccount(?string $durationOfAccount = null): self
    {
        // validation for constraint: string
        if (!is_null($durationOfAccount) && !is_string($durationOfAccount)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($durationOfAccount, true),
                gettype($durationOfAccount)
            ), __LINE__);
        }
        $this->durationOfAccount = $durationOfAccount;

        return $this;
    }

    /**
     * Get additionalSupplementaryInformation value
     *
     * @return string|null
     */
    public function getAdditionalSupplementaryInformation(): ?string
    {
        return $this->additionalSupplementaryInformation;
    }

    /**
     * Set additionalSupplementaryInformation value
     *
     * @param string $additionalSupplementaryInformation
     * @return FinancialAgreement
     */
    public function setAdditionalSupplementaryInformation(?string $additionalSupplementaryInformation = null): self
    {
        // validation for constraint: string
        if (!is_null($additionalSupplementaryInformation) && !is_string($additionalSupplementaryInformation)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($additionalSupplementaryInformation, true),
                gettype($additionalSupplementaryInformation)
            ), __LINE__);
        }
        $this->additionalSupplementaryInformation = $additionalSupplementaryInformation;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return FinancialAgreement
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
