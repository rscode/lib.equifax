<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for HirePurchase StructType
 *
 * @subpackage Structs
 */
class HirePurchase extends FinancialAgreement
{
    /**
     * The variableRate
     * Meta information extracted from the WSDL
     * - use: optional
     *
     * @var bool|null
     */
    protected ?bool $variableRate = null;

    /**
     * Constructor method for HirePurchase
     *
     * @param bool $variableRate
     * @uses HirePurchase::setVariableRate()
     */
    public function __construct(?bool $variableRate = null)
    {
        $this
            ->setVariableRate($variableRate);
    }

    /**
     * Get variableRate value
     *
     * @return bool|null
     */
    public function getVariableRate(): ?bool
    {
        return $this->variableRate;
    }

    /**
     * Set variableRate value
     *
     * @param bool $variableRate
     * @return HirePurchase
     */
    public function setVariableRate(?bool $variableRate = null): self
    {
        // validation for constraint: boolean
        if (!is_null($variableRate) && !is_bool($variableRate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($variableRate, true),
                gettype($variableRate)
            ), __LINE__);
        }
        $this->variableRate = $variableRate;

        return $this;
    }
}
