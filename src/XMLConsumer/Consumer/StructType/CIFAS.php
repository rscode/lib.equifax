<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;

/**
 * This class stands for CIFAS StructType
 * Meta information extracted from the WSDL
 * - documentation: Credit Industry Fraud Avoidance Scheme (CIFAS) is a method of preventing fraud by allowing credit grantors to exchange details of fraud when it's discovered. Only CIFAS members are eligible to view CIFAS records.
 *
 * @subpackage Structs
 */
class CIFAS extends NameMatchedData
{
    /**
     * The productCode
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 4
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $productCode;
    /**
     * The referenceText
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 40
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $referenceText;
    /**
     * The supplyingMemberId
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 3
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $supplyingMemberId;
    /**
     * The supplyingMemberName
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 40
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $supplyingMemberName;
    /**
     * The additionalInformationIndicator
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 1
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $additionalInformationIndicator = null;
    /**
     * The applicationDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $applicationDate = null;
    /**
     * The caseId
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 9
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $caseId = null;
    /**
     * The caseNumber
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 6
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $caseNumber = null;
    /**
     * The caseType
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 3
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $caseType = null;
    /**
     * The companyNumber
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 8
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $companyNumber = null;
    /**
     * The emailAddress
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 60
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $emailAddress = null;
    /**
     * The facility
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 3
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $facility = null;
    /**
     * The filingReason
     * Meta information extracted from the WSDL
     * - maxOccurs: 25
     * - minOccurs: 0
     *
     * @var FilingReason[]
     */
    protected ?array $filingReason = null;
    /**
     * The fraudAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var PartlyStructuredAddress|null
     */
    protected ?PartlyStructuredAddress $fraudAddress = null;
    /**
     * The fraudAddressType
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 2
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $fraudAddressType = null;
    /**
     * The fraudDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $fraudDate = null;
    /**
     * The homeTelephone
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 20
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $homeTelephone = null;
    /**
     * The managingMemberName
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 40
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $managingMemberName = null;
    /**
     * The managingMemberNumber
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 3
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $managingMemberNumber = null;
    /**
     * The matchRuleResult
     * Meta information extracted from the WSDL
     * - maxOccurs: 8
     * - minOccurs: 0
     *
     * @var MatchRuleResult[]
     */
    protected ?array $matchRuleResult = null;
    /**
     * The mobileTelephone
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 20
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $mobileTelephone = null;
    /**
     * The searchReference
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 20
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $searchReference = null;
    /**
     * The subjectRole
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 3
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $subjectRole = null;
    /**
     * The subjectRoleQualifier
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 3
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $subjectRoleQualifier = null;
    /**
     * The timeAtAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var TimeAtAddressChoice|null
     */
    protected ?TimeAtAddressChoice $timeAtAddress = null;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for CIFAS
     *
     * @param string                  $productCode
     * @param string                  $referenceText
     * @param string                  $supplyingMemberId
     * @param string                  $supplyingMemberName
     * @param string                  $additionalInformationIndicator
     * @param string                  $applicationDate
     * @param string                  $caseId
     * @param string                  $caseNumber
     * @param string                  $caseType
     * @param string                  $companyNumber
     * @param string                  $emailAddress
     * @param string                  $facility
     * @param FilingReason[]          $filingReason
     * @param PartlyStructuredAddress $fraudAddress
     * @param string                  $fraudAddressType
     * @param string                  $fraudDate
     * @param string                  $homeTelephone
     * @param string                  $managingMemberName
     * @param string                  $managingMemberNumber
     * @param MatchRuleResult[]       $matchRuleResult
     * @param string                  $mobileTelephone
     * @param string                  $searchReference
     * @param string                  $subjectRole
     * @param string                  $subjectRoleQualifier
     * @param TimeAtAddressChoice     $timeAtAddress
     * @param DOMDocument|string|null $any
     * @uses CIFAS::setProductCode()
     * @uses CIFAS::setReferenceText()
     * @uses CIFAS::setSupplyingMemberId()
     * @uses CIFAS::setSupplyingMemberName()
     * @uses CIFAS::setAdditionalInformationIndicator()
     * @uses CIFAS::setApplicationDate()
     * @uses CIFAS::setCaseId()
     * @uses CIFAS::setCaseNumber()
     * @uses CIFAS::setCaseType()
     * @uses CIFAS::setCompanyNumber()
     * @uses CIFAS::setEmailAddress()
     * @uses CIFAS::setFacility()
     * @uses CIFAS::setFilingReason()
     * @uses CIFAS::setFraudAddress()
     * @uses CIFAS::setFraudAddressType()
     * @uses CIFAS::setFraudDate()
     * @uses CIFAS::setHomeTelephone()
     * @uses CIFAS::setManagingMemberName()
     * @uses CIFAS::setManagingMemberNumber()
     * @uses CIFAS::setMatchRuleResult()
     * @uses CIFAS::setMobileTelephone()
     * @uses CIFAS::setSearchReference()
     * @uses CIFAS::setSubjectRole()
     * @uses CIFAS::setSubjectRoleQualifier()
     * @uses CIFAS::setTimeAtAddress()
     * @uses CIFAS::setAny()
     */
    public function __construct(
        string $productCode,
        string $referenceText,
        string $supplyingMemberId,
        string $supplyingMemberName,
        ?string $additionalInformationIndicator = null,
        ?string $applicationDate = null,
        ?string $caseId = null,
        ?string $caseNumber = null,
        ?string $caseType = null,
        ?string $companyNumber = null,
        ?string $emailAddress = null,
        ?string $facility = null,
        ?array $filingReason = null,
        ?PartlyStructuredAddress $fraudAddress = null,
        ?string $fraudAddressType = null,
        ?string $fraudDate = null,
        ?string $homeTelephone = null,
        ?string $managingMemberName = null,
        ?string $managingMemberNumber = null,
        ?array $matchRuleResult = null,
        ?string $mobileTelephone = null,
        ?string $searchReference = null,
        ?string $subjectRole = null,
        ?string $subjectRoleQualifier = null,
        ?TimeAtAddressChoice $timeAtAddress = null,
        $any = null
    ) {
        $this
            ->setProductCode($productCode)
            ->setReferenceText($referenceText)
            ->setSupplyingMemberId($supplyingMemberId)
            ->setSupplyingMemberName($supplyingMemberName)
            ->setAdditionalInformationIndicator($additionalInformationIndicator)
            ->setApplicationDate($applicationDate)
            ->setCaseId($caseId)
            ->setCaseNumber($caseNumber)
            ->setCaseType($caseType)
            ->setCompanyNumber($companyNumber)
            ->setEmailAddress($emailAddress)
            ->setFacility($facility)
            ->setFilingReason($filingReason)
            ->setFraudAddress($fraudAddress)
            ->setFraudAddressType($fraudAddressType)
            ->setFraudDate($fraudDate)
            ->setHomeTelephone($homeTelephone)
            ->setManagingMemberName($managingMemberName)
            ->setManagingMemberNumber($managingMemberNumber)
            ->setMatchRuleResult($matchRuleResult)
            ->setMobileTelephone($mobileTelephone)
            ->setSearchReference($searchReference)
            ->setSubjectRole($subjectRole)
            ->setSubjectRoleQualifier($subjectRoleQualifier)
            ->setTimeAtAddress($timeAtAddress)
            ->setAny($any);
    }

    /**
     * This method is responsible for validating the values passed to the setFilingReason method
     * This method is willingly generated in order to preserve the one-line inline validation within the setFilingReason method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateFilingReasonForArrayConstraintsFromSetFilingReason(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $cIFASFilingReasonItem) {
            // validation for constraint: itemType
            if (!$cIFASFilingReasonItem instanceof FilingReason) {
                $invalidValues[] = is_object($cIFASFilingReasonItem) ? get_class($cIFASFilingReasonItem) : sprintf(
                    '%s(%s)',
                    gettype($cIFASFilingReasonItem),
                    var_export($cIFASFilingReasonItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The filingReason property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\FilingReason, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * This method is responsible for validating the values passed to the setMatchRuleResult method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMatchRuleResult method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMatchRuleResultForArrayConstraintsFromSetMatchRuleResult(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $cIFASMatchRuleResultItem) {
            // validation for constraint: itemType
            if (!$cIFASMatchRuleResultItem instanceof MatchRuleResult) {
                $invalidValues[] = is_object($cIFASMatchRuleResultItem) ? get_class($cIFASMatchRuleResultItem) : sprintf(
                    '%s(%s)',
                    gettype($cIFASMatchRuleResultItem),
                    var_export($cIFASMatchRuleResultItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The matchRuleResult property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\MatchRuleResult, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get productCode value
     *
     * @return string
     */
    public function getProductCode(): string
    {
        return $this->productCode;
    }

    /**
     * Set productCode value
     *
     * @param string $productCode
     * @return CIFAS
     */
    public function setProductCode(string $productCode): self
    {
        // validation for constraint: string
        if (!is_null($productCode) && !is_string($productCode)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($productCode, true),
                gettype($productCode)
            ), __LINE__);
        }
        // validation for constraint: maxLength(4)
        if (!is_null($productCode) && mb_strlen((string)$productCode) > 4) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 4',
                mb_strlen((string)$productCode)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($productCode) && mb_strlen((string)$productCode) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$productCode)
            ), __LINE__);
        }
        $this->productCode = $productCode;

        return $this;
    }

    /**
     * Get referenceText value
     *
     * @return string
     */
    public function getReferenceText(): string
    {
        return $this->referenceText;
    }

    /**
     * Set referenceText value
     *
     * @param string $referenceText
     * @return CIFAS
     */
    public function setReferenceText(string $referenceText): self
    {
        // validation for constraint: string
        if (!is_null($referenceText) && !is_string($referenceText)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($referenceText, true),
                gettype($referenceText)
            ), __LINE__);
        }
        // validation for constraint: maxLength(40)
        if (!is_null($referenceText) && mb_strlen((string)$referenceText) > 40) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 40',
                mb_strlen((string)$referenceText)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($referenceText) && mb_strlen((string)$referenceText) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$referenceText)
            ), __LINE__);
        }
        $this->referenceText = $referenceText;

        return $this;
    }

    /**
     * Get supplyingMemberId value
     *
     * @return string
     */
    public function getSupplyingMemberId(): string
    {
        return $this->supplyingMemberId;
    }

    /**
     * Set supplyingMemberId value
     *
     * @param string $supplyingMemberId
     * @return CIFAS
     */
    public function setSupplyingMemberId(string $supplyingMemberId): self
    {
        // validation for constraint: string
        if (!is_null($supplyingMemberId) && !is_string($supplyingMemberId)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($supplyingMemberId, true),
                gettype($supplyingMemberId)
            ), __LINE__);
        }
        // validation for constraint: maxLength(3)
        if (!is_null($supplyingMemberId) && mb_strlen((string)$supplyingMemberId) > 3) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 3',
                mb_strlen((string)$supplyingMemberId)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($supplyingMemberId) && mb_strlen((string)$supplyingMemberId) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$supplyingMemberId)
            ), __LINE__);
        }
        $this->supplyingMemberId = $supplyingMemberId;

        return $this;
    }

    /**
     * Get supplyingMemberName value
     *
     * @return string
     */
    public function getSupplyingMemberName(): string
    {
        return $this->supplyingMemberName;
    }

    /**
     * Set supplyingMemberName value
     *
     * @param string $supplyingMemberName
     * @return CIFAS
     */
    public function setSupplyingMemberName(string $supplyingMemberName): self
    {
        // validation for constraint: string
        if (!is_null($supplyingMemberName) && !is_string($supplyingMemberName)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($supplyingMemberName, true),
                gettype($supplyingMemberName)
            ), __LINE__);
        }
        // validation for constraint: maxLength(40)
        if (!is_null($supplyingMemberName) && mb_strlen((string)$supplyingMemberName) > 40) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 40',
                mb_strlen((string)$supplyingMemberName)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($supplyingMemberName) && mb_strlen((string)$supplyingMemberName) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$supplyingMemberName)
            ), __LINE__);
        }
        $this->supplyingMemberName = $supplyingMemberName;

        return $this;
    }

    /**
     * Get additionalInformationIndicator value
     *
     * @return string|null
     */
    public function getAdditionalInformationIndicator(): ?string
    {
        return $this->additionalInformationIndicator;
    }

    /**
     * Set additionalInformationIndicator value
     *
     * @param string $additionalInformationIndicator
     * @return CIFAS
     */
    public function setAdditionalInformationIndicator(?string $additionalInformationIndicator = null): self
    {
        // validation for constraint: string
        if (!is_null($additionalInformationIndicator) && !is_string($additionalInformationIndicator)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($additionalInformationIndicator, true),
                gettype($additionalInformationIndicator)
            ), __LINE__);
        }
        // validation for constraint: maxLength(1)
        if (!is_null($additionalInformationIndicator) && mb_strlen((string)$additionalInformationIndicator) > 1) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 1',
                mb_strlen((string)$additionalInformationIndicator)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($additionalInformationIndicator) && mb_strlen((string)$additionalInformationIndicator) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$additionalInformationIndicator)
            ), __LINE__);
        }
        $this->additionalInformationIndicator = $additionalInformationIndicator;

        return $this;
    }

    /**
     * Get applicationDate value
     *
     * @return string|null
     */
    public function getApplicationDate(): ?string
    {
        return $this->applicationDate;
    }

    /**
     * Set applicationDate value
     *
     * @param string $applicationDate
     * @return CIFAS
     */
    public function setApplicationDate(?string $applicationDate = null): self
    {
        // validation for constraint: string
        if (!is_null($applicationDate) && !is_string($applicationDate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($applicationDate, true),
                gettype($applicationDate)
            ), __LINE__);
        }
        $this->applicationDate = $applicationDate;

        return $this;
    }

    /**
     * Get caseId value
     *
     * @return string|null
     */
    public function getCaseId(): ?string
    {
        return $this->caseId;
    }

    /**
     * Set caseId value
     *
     * @param string $caseId
     * @return CIFAS
     */
    public function setCaseId(?string $caseId = null): self
    {
        // validation for constraint: string
        if (!is_null($caseId) && !is_string($caseId)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($caseId, true),
                gettype($caseId)
            ), __LINE__);
        }
        // validation for constraint: maxLength(9)
        if (!is_null($caseId) && mb_strlen((string)$caseId) > 9) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 9',
                mb_strlen((string)$caseId)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($caseId) && mb_strlen((string)$caseId) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$caseId)
            ), __LINE__);
        }
        $this->caseId = $caseId;

        return $this;
    }

    /**
     * Get caseNumber value
     *
     * @return string|null
     */
    public function getCaseNumber(): ?string
    {
        return $this->caseNumber;
    }

    /**
     * Set caseNumber value
     *
     * @param string $caseNumber
     * @return CIFAS
     */
    public function setCaseNumber(?string $caseNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($caseNumber) && !is_string($caseNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($caseNumber, true),
                gettype($caseNumber)
            ), __LINE__);
        }
        // validation for constraint: maxLength(6)
        if (!is_null($caseNumber) && mb_strlen((string)$caseNumber) > 6) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 6',
                mb_strlen((string)$caseNumber)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($caseNumber) && mb_strlen((string)$caseNumber) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$caseNumber)
            ), __LINE__);
        }
        $this->caseNumber = $caseNumber;

        return $this;
    }

    /**
     * Get caseType value
     *
     * @return string|null
     */
    public function getCaseType(): ?string
    {
        return $this->caseType;
    }

    /**
     * Set caseType value
     *
     * @param string $caseType
     * @return CIFAS
     */
    public function setCaseType(?string $caseType = null): self
    {
        // validation for constraint: string
        if (!is_null($caseType) && !is_string($caseType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($caseType, true),
                gettype($caseType)
            ), __LINE__);
        }
        // validation for constraint: maxLength(3)
        if (!is_null($caseType) && mb_strlen((string)$caseType) > 3) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 3',
                mb_strlen((string)$caseType)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($caseType) && mb_strlen((string)$caseType) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$caseType)
            ), __LINE__);
        }
        $this->caseType = $caseType;

        return $this;
    }

    /**
     * Get companyNumber value
     *
     * @return string|null
     */
    public function getCompanyNumber(): ?string
    {
        return $this->companyNumber;
    }

    /**
     * Set companyNumber value
     *
     * @param string $companyNumber
     * @return CIFAS
     */
    public function setCompanyNumber(?string $companyNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($companyNumber) && !is_string($companyNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($companyNumber, true),
                gettype($companyNumber)
            ), __LINE__);
        }
        // validation for constraint: maxLength(8)
        if (!is_null($companyNumber) && mb_strlen((string)$companyNumber) > 8) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 8',
                mb_strlen((string)$companyNumber)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($companyNumber) && mb_strlen((string)$companyNumber) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$companyNumber)
            ), __LINE__);
        }
        $this->companyNumber = $companyNumber;

        return $this;
    }

    /**
     * Get emailAddress value
     *
     * @return string|null
     */
    public function getEmailAddress(): ?string
    {
        return $this->emailAddress;
    }

    /**
     * Set emailAddress value
     *
     * @param string $emailAddress
     * @return CIFAS
     */
    public function setEmailAddress(?string $emailAddress = null): self
    {
        // validation for constraint: string
        if (!is_null($emailAddress) && !is_string($emailAddress)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($emailAddress, true),
                gettype($emailAddress)
            ), __LINE__);
        }
        // validation for constraint: maxLength(60)
        if (!is_null($emailAddress) && mb_strlen((string)$emailAddress) > 60) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 60',
                mb_strlen((string)$emailAddress)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($emailAddress) && mb_strlen((string)$emailAddress) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$emailAddress)
            ), __LINE__);
        }
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * Get facility value
     *
     * @return string|null
     */
    public function getFacility(): ?string
    {
        return $this->facility;
    }

    /**
     * Set facility value
     *
     * @param string $facility
     * @return CIFAS
     */
    public function setFacility(?string $facility = null): self
    {
        // validation for constraint: string
        if (!is_null($facility) && !is_string($facility)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($facility, true),
                gettype($facility)
            ), __LINE__);
        }
        // validation for constraint: maxLength(3)
        if (!is_null($facility) && mb_strlen((string)$facility) > 3) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 3',
                mb_strlen((string)$facility)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($facility) && mb_strlen((string)$facility) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$facility)
            ), __LINE__);
        }
        $this->facility = $facility;

        return $this;
    }

    /**
     * Get filingReason value
     *
     * @return FilingReason[]
     */
    public function getFilingReason(): ?array
    {
        return $this->filingReason;
    }

    /**
     * Set filingReason value
     *
     * @param FilingReason[] $filingReason
     * @return CIFAS
     * @throws InvalidArgumentException
     */
    public function setFilingReason(?array $filingReason = null): self
    {
        // validation for constraint: array
        if ('' !== ($filingReasonArrayErrorMessage = self::validateFilingReasonForArrayConstraintsFromSetFilingReason($filingReason))) {
            throw new InvalidArgumentException($filingReasonArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(25)
        if (is_array($filingReason) && count($filingReason) > 25) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 25',
                count($filingReason)
            ), __LINE__);
        }
        $this->filingReason = $filingReason;

        return $this;
    }

    /**
     * Add item to filingReason value
     *
     * @param FilingReason $item
     * @return CIFAS
     * @throws InvalidArgumentException
     */
    public function addToFilingReason(FilingReason $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof FilingReason) {
            throw new InvalidArgumentException(sprintf(
                'The filingReason property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\FilingReason, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(25)
        if (is_array($this->filingReason) && count($this->filingReason) >= 25) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 25',
                count($this->filingReason)
            ), __LINE__);
        }
        $this->filingReason[] = $item;

        return $this;
    }

    /**
     * Get fraudAddress value
     *
     * @return PartlyStructuredAddress|null
     */
    public function getFraudAddress(): ?PartlyStructuredAddress
    {
        return $this->fraudAddress;
    }

    /**
     * Set fraudAddress value
     *
     * @param PartlyStructuredAddress $fraudAddress
     * @return CIFAS
     */
    public function setFraudAddress(?PartlyStructuredAddress $fraudAddress = null): self
    {
        $this->fraudAddress = $fraudAddress;

        return $this;
    }

    /**
     * Get fraudAddressType value
     *
     * @return string|null
     */
    public function getFraudAddressType(): ?string
    {
        return $this->fraudAddressType;
    }

    /**
     * Set fraudAddressType value
     *
     * @param string $fraudAddressType
     * @return CIFAS
     */
    public function setFraudAddressType(?string $fraudAddressType = null): self
    {
        // validation for constraint: string
        if (!is_null($fraudAddressType) && !is_string($fraudAddressType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($fraudAddressType, true),
                gettype($fraudAddressType)
            ), __LINE__);
        }
        // validation for constraint: maxLength(2)
        if (!is_null($fraudAddressType) && mb_strlen((string)$fraudAddressType) > 2) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 2',
                mb_strlen((string)$fraudAddressType)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($fraudAddressType) && mb_strlen((string)$fraudAddressType) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$fraudAddressType)
            ), __LINE__);
        }
        $this->fraudAddressType = $fraudAddressType;

        return $this;
    }

    /**
     * Get fraudDate value
     *
     * @return string|null
     */
    public function getFraudDate(): ?string
    {
        return $this->fraudDate;
    }

    /**
     * Set fraudDate value
     *
     * @param string $fraudDate
     * @return CIFAS
     */
    public function setFraudDate(?string $fraudDate = null): self
    {
        // validation for constraint: string
        if (!is_null($fraudDate) && !is_string($fraudDate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($fraudDate, true),
                gettype($fraudDate)
            ), __LINE__);
        }
        $this->fraudDate = $fraudDate;

        return $this;
    }

    /**
     * Get homeTelephone value
     *
     * @return string|null
     */
    public function getHomeTelephone(): ?string
    {
        return $this->homeTelephone;
    }

    /**
     * Set homeTelephone value
     *
     * @param string $homeTelephone
     * @return CIFAS
     */
    public function setHomeTelephone(?string $homeTelephone = null): self
    {
        // validation for constraint: string
        if (!is_null($homeTelephone) && !is_string($homeTelephone)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($homeTelephone, true),
                gettype($homeTelephone)
            ), __LINE__);
        }
        // validation for constraint: maxLength(20)
        if (!is_null($homeTelephone) && mb_strlen((string)$homeTelephone) > 20) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 20',
                mb_strlen((string)$homeTelephone)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($homeTelephone) && mb_strlen((string)$homeTelephone) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$homeTelephone)
            ), __LINE__);
        }
        $this->homeTelephone = $homeTelephone;

        return $this;
    }

    /**
     * Get managingMemberName value
     *
     * @return string|null
     */
    public function getManagingMemberName(): ?string
    {
        return $this->managingMemberName;
    }

    /**
     * Set managingMemberName value
     *
     * @param string $managingMemberName
     * @return CIFAS
     */
    public function setManagingMemberName(?string $managingMemberName = null): self
    {
        // validation for constraint: string
        if (!is_null($managingMemberName) && !is_string($managingMemberName)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($managingMemberName, true),
                gettype($managingMemberName)
            ), __LINE__);
        }
        // validation for constraint: maxLength(40)
        if (!is_null($managingMemberName) && mb_strlen((string)$managingMemberName) > 40) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 40',
                mb_strlen((string)$managingMemberName)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($managingMemberName) && mb_strlen((string)$managingMemberName) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$managingMemberName)
            ), __LINE__);
        }
        $this->managingMemberName = $managingMemberName;

        return $this;
    }

    /**
     * Get managingMemberNumber value
     *
     * @return string|null
     */
    public function getManagingMemberNumber(): ?string
    {
        return $this->managingMemberNumber;
    }

    /**
     * Set managingMemberNumber value
     *
     * @param string $managingMemberNumber
     * @return CIFAS
     */
    public function setManagingMemberNumber(?string $managingMemberNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($managingMemberNumber) && !is_string($managingMemberNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($managingMemberNumber, true),
                gettype($managingMemberNumber)
            ), __LINE__);
        }
        // validation for constraint: maxLength(3)
        if (!is_null($managingMemberNumber) && mb_strlen((string)$managingMemberNumber) > 3) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 3',
                mb_strlen((string)$managingMemberNumber)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($managingMemberNumber) && mb_strlen((string)$managingMemberNumber) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$managingMemberNumber)
            ), __LINE__);
        }
        $this->managingMemberNumber = $managingMemberNumber;

        return $this;
    }

    /**
     * Get matchRuleResult value
     *
     * @return MatchRuleResult[]
     */
    public function getMatchRuleResult(): ?array
    {
        return $this->matchRuleResult;
    }

    /**
     * Set matchRuleResult value
     *
     * @param MatchRuleResult[] $matchRuleResult
     * @return CIFAS
     * @throws InvalidArgumentException
     */
    public function setMatchRuleResult(?array $matchRuleResult = null): self
    {
        // validation for constraint: array
        if ('' !== ($matchRuleResultArrayErrorMessage = self::validateMatchRuleResultForArrayConstraintsFromSetMatchRuleResult($matchRuleResult))) {
            throw new InvalidArgumentException($matchRuleResultArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(8)
        if (is_array($matchRuleResult) && count($matchRuleResult) > 8) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 8',
                count($matchRuleResult)
            ), __LINE__);
        }
        $this->matchRuleResult = $matchRuleResult;

        return $this;
    }

    /**
     * Add item to matchRuleResult value
     *
     * @param MatchRuleResult $item
     * @return CIFAS
     * @throws InvalidArgumentException
     */
    public function addToMatchRuleResult(MatchRuleResult $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof MatchRuleResult) {
            throw new InvalidArgumentException(sprintf(
                'The matchRuleResult property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\MatchRuleResult, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(8)
        if (is_array($this->matchRuleResult) && count($this->matchRuleResult) >= 8) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 8',
                count($this->matchRuleResult)
            ), __LINE__);
        }
        $this->matchRuleResult[] = $item;

        return $this;
    }

    /**
     * Get mobileTelephone value
     *
     * @return string|null
     */
    public function getMobileTelephone(): ?string
    {
        return $this->mobileTelephone;
    }

    /**
     * Set mobileTelephone value
     *
     * @param string $mobileTelephone
     * @return CIFAS
     */
    public function setMobileTelephone(?string $mobileTelephone = null): self
    {
        // validation for constraint: string
        if (!is_null($mobileTelephone) && !is_string($mobileTelephone)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($mobileTelephone, true),
                gettype($mobileTelephone)
            ), __LINE__);
        }
        // validation for constraint: maxLength(20)
        if (!is_null($mobileTelephone) && mb_strlen((string)$mobileTelephone) > 20) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 20',
                mb_strlen((string)$mobileTelephone)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($mobileTelephone) && mb_strlen((string)$mobileTelephone) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$mobileTelephone)
            ), __LINE__);
        }
        $this->mobileTelephone = $mobileTelephone;

        return $this;
    }

    /**
     * Get searchReference value
     *
     * @return string|null
     */
    public function getSearchReference(): ?string
    {
        return $this->searchReference;
    }

    /**
     * Set searchReference value
     *
     * @param string $searchReference
     * @return CIFAS
     */
    public function setSearchReference(?string $searchReference = null): self
    {
        // validation for constraint: string
        if (!is_null($searchReference) && !is_string($searchReference)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($searchReference, true),
                gettype($searchReference)
            ), __LINE__);
        }
        // validation for constraint: maxLength(20)
        if (!is_null($searchReference) && mb_strlen((string)$searchReference) > 20) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 20',
                mb_strlen((string)$searchReference)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($searchReference) && mb_strlen((string)$searchReference) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$searchReference)
            ), __LINE__);
        }
        $this->searchReference = $searchReference;

        return $this;
    }

    /**
     * Get subjectRole value
     *
     * @return string|null
     */
    public function getSubjectRole(): ?string
    {
        return $this->subjectRole;
    }

    /**
     * Set subjectRole value
     *
     * @param string $subjectRole
     * @return CIFAS
     */
    public function setSubjectRole(?string $subjectRole = null): self
    {
        // validation for constraint: string
        if (!is_null($subjectRole) && !is_string($subjectRole)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($subjectRole, true),
                gettype($subjectRole)
            ), __LINE__);
        }
        // validation for constraint: maxLength(3)
        if (!is_null($subjectRole) && mb_strlen((string)$subjectRole) > 3) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 3',
                mb_strlen((string)$subjectRole)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($subjectRole) && mb_strlen((string)$subjectRole) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$subjectRole)
            ), __LINE__);
        }
        $this->subjectRole = $subjectRole;

        return $this;
    }

    /**
     * Get subjectRoleQualifier value
     *
     * @return string|null
     */
    public function getSubjectRoleQualifier(): ?string
    {
        return $this->subjectRoleQualifier;
    }

    /**
     * Set subjectRoleQualifier value
     *
     * @param string $subjectRoleQualifier
     * @return CIFAS
     */
    public function setSubjectRoleQualifier(?string $subjectRoleQualifier = null): self
    {
        // validation for constraint: string
        if (!is_null($subjectRoleQualifier) && !is_string($subjectRoleQualifier)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($subjectRoleQualifier, true),
                gettype($subjectRoleQualifier)
            ), __LINE__);
        }
        // validation for constraint: maxLength(3)
        if (!is_null($subjectRoleQualifier) && mb_strlen((string)$subjectRoleQualifier) > 3) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 3',
                mb_strlen((string)$subjectRoleQualifier)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($subjectRoleQualifier) && mb_strlen((string)$subjectRoleQualifier) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$subjectRoleQualifier)
            ), __LINE__);
        }
        $this->subjectRoleQualifier = $subjectRoleQualifier;

        return $this;
    }

    /**
     * Get timeAtAddress value
     *
     * @return TimeAtAddressChoice|null
     */
    public function getTimeAtAddress(): ?TimeAtAddressChoice
    {
        return $this->timeAtAddress;
    }

    /**
     * Set timeAtAddress value
     *
     * @param TimeAtAddressChoice $timeAtAddress
     * @return CIFAS
     */
    public function setTimeAtAddress(?TimeAtAddressChoice $timeAtAddress = null): self
    {
        $this->timeAtAddress = $timeAtAddress;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return CIFAS
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
