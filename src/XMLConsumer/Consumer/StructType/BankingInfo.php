<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for BankingInfo StructType
 *
 * @subpackage Structs
 */
class BankingInfo extends AbstractStructBase
{
    /**
     * The account
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var BankAccount|null
     */
    protected ?BankAccount $account = null;
    /**
     * The bank
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var Bank|null
     */
    protected ?Bank $bank = null;
    /**
     * The timeAtBank
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $timeAtBank = null;

    /**
     * Constructor method for BankingInfo
     *
     * @param BankAccount $account
     * @param Bank        $bank
     * @param string      $timeAtBank
     * @uses BankingInfo::setAccount()
     * @uses BankingInfo::setBank()
     * @uses BankingInfo::setTimeAtBank()
     */
    public function __construct(?BankAccount $account = null, ?Bank $bank = null, ?string $timeAtBank = null)
    {
        $this
            ->setAccount($account)
            ->setBank($bank)
            ->setTimeAtBank($timeAtBank);
    }

    /**
     * Get account value
     *
     * @return BankAccount|null
     */
    public function getAccount(): ?BankAccount
    {
        return $this->account;
    }

    /**
     * Set account value
     *
     * @param BankAccount $account
     * @return BankingInfo
     */
    public function setAccount(?BankAccount $account = null): self
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get bank value
     *
     * @return Bank|null
     */
    public function getBank(): ?Bank
    {
        return $this->bank;
    }

    /**
     * Set bank value
     *
     * @param Bank $bank
     * @return BankingInfo
     */
    public function setBank(?Bank $bank = null): self
    {
        $this->bank = $bank;

        return $this;
    }

    /**
     * Get timeAtBank value
     *
     * @return string|null
     */
    public function getTimeAtBank(): ?string
    {
        return $this->timeAtBank;
    }

    /**
     * Set timeAtBank value
     *
     * @param string $timeAtBank
     * @return BankingInfo
     */
    public function setTimeAtBank(?string $timeAtBank = null): self
    {
        // validation for constraint: string
        if (!is_null($timeAtBank) && !is_string($timeAtBank)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($timeAtBank, true),
                gettype($timeAtBank)
            ), __LINE__);
        }
        $this->timeAtBank = $timeAtBank;

        return $this;
    }
}
