<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for AliasesContainer StructType
 *
 * @subpackage Structs
 */
class AliasesContainer extends DataItemContainer
{
    /**
     * The alias
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var Alias[]
     */
    protected ?array $alias = null;

    /**
     * Constructor method for AliasesContainer
     *
     * @param Alias[] $alias
     * @uses AliasesContainer::setAlias()
     */
    public function __construct(?array $alias = null)
    {
        $this
            ->setAlias($alias);
    }

    /**
     * This method is responsible for validating the values passed to the setAlias method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAlias method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAliasForArrayConstraintsFromSetAlias(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $aliasesContainerAliasItem) {
            // validation for constraint: itemType
            if (!$aliasesContainerAliasItem instanceof Alias) {
                $invalidValues[] = is_object($aliasesContainerAliasItem) ? get_class($aliasesContainerAliasItem) : sprintf(
                    '%s(%s)',
                    gettype($aliasesContainerAliasItem),
                    var_export($aliasesContainerAliasItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The alias property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\Alias, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get alias value
     *
     * @return Alias[]
     */
    public function getAlias(): ?array
    {
        return $this->alias;
    }

    /**
     * Set alias value
     *
     * @param Alias[] $alias
     * @return AliasesContainer
     * @throws InvalidArgumentException
     */
    public function setAlias(?array $alias = null): self
    {
        // validation for constraint: array
        if ('' !== ($aliasArrayErrorMessage = self::validateAliasForArrayConstraintsFromSetAlias($alias))) {
            throw new InvalidArgumentException($aliasArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($alias) && count($alias) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($alias)
            ), __LINE__);
        }
        $this->alias = $alias;

        return $this;
    }

    /**
     * Add item to alias value
     *
     * @param Alias $item
     * @return AliasesContainer
     * @throws InvalidArgumentException
     */
    public function addToAlias(Alias $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof Alias) {
            throw new InvalidArgumentException(sprintf(
                'The alias property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\Alias, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->alias) && count($this->alias) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->alias)
            ), __LINE__);
        }
        $this->alias[] = $item;

        return $this;
    }
}
