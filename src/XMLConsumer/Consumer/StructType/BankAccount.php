<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for BankAccount StructType
 * Meta information extracted from the WSDL
 * - documentation: This type holds details of a bank account. It may refer to a bank element.
 *
 * @subpackage Structs
 */
class BankAccount extends AbstractStructBase
{
    /**
     * The accountNumber
     * Meta information extracted from the WSDL
     * - documentation: Represents a bank account number.
     * - base: xs:string
     * - maxOccurs: 1
     * - minOccurs: 1
     * - pattern: [0-9]{8}
     *
     * @var string
     */
    protected string $accountNumber;
    /**
     * The sortCode
     * Meta information extracted from the WSDL
     * - documentation: Represents a bank sort code.
     * - base: xs:string
     * - maxOccurs: 1
     * - minOccurs: 1
     * - pattern: \d\d-\d\d-\d\d
     *
     * @var string
     */
    protected string $sortCode;
    /**
     * The belongsTo
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var Bank|null
     */
    protected ?Bank $belongsTo = null;

    /**
     * Constructor method for BankAccount
     *
     * @param string $accountNumber
     * @param string $sortCode
     * @param Bank   $belongsTo
     * @uses BankAccount::setAccountNumber()
     * @uses BankAccount::setSortCode()
     * @uses BankAccount::setBelongsTo()
     */
    public function __construct(string $accountNumber, string $sortCode, ?Bank $belongsTo = null)
    {
        $this
            ->setAccountNumber($accountNumber)
            ->setSortCode($sortCode)
            ->setBelongsTo($belongsTo);
    }

    /**
     * Get accountNumber value
     *
     * @return string
     */
    public function getAccountNumber(): string
    {
        return $this->accountNumber;
    }

    /**
     * Set accountNumber value
     *
     * @param string $accountNumber
     * @return BankAccount
     */
    public function setAccountNumber(string $accountNumber): self
    {
        // validation for constraint: string
        if (!is_null($accountNumber) && !is_string($accountNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($accountNumber, true),
                gettype($accountNumber)
            ), __LINE__);
        }
        // validation for constraint: pattern([0-9]{8})
        if (!is_null($accountNumber) && !preg_match('/[0-9]{8}/', $accountNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[0-9]{8}/',
                var_export($accountNumber, true)
            ), __LINE__);
        }
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Get sortCode value
     *
     * @return string
     */
    public function getSortCode(): string
    {
        return $this->sortCode;
    }

    /**
     * Set sortCode value
     *
     * @param string $sortCode
     * @return BankAccount
     */
    public function setSortCode(string $sortCode): self
    {
        // validation for constraint: string
        if (!is_null($sortCode) && !is_string($sortCode)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($sortCode, true),
                gettype($sortCode)
            ), __LINE__);
        }
        // validation for constraint: pattern(\d\d-\d\d-\d\d)
        if (!is_null($sortCode) && !preg_match('/\\d\\d-\\d\\d-\\d\\d/', $sortCode)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /\\d\\d-\\d\\d-\\d\\d/',
                var_export($sortCode, true)
            ), __LINE__);
        }
        $this->sortCode = $sortCode;

        return $this;
    }

    /**
     * Get belongsTo value
     *
     * @return Bank|null
     */
    public function getBelongsTo(): ?Bank
    {
        return $this->belongsTo;
    }

    /**
     * Set belongsTo value
     *
     * @param Bank $belongsTo
     * @return BankAccount
     */
    public function setBelongsTo(?Bank $belongsTo = null): self
    {
        $this->belongsTo = $belongsTo;

        return $this;
    }
}
