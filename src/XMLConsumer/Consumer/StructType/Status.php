<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Status StructType
 *
 * @subpackage Structs
 */
class Status extends AbstractStructBase
{
    /**
     * The categorisation
     * Meta information extracted from the WSDL
     * - maxOccurs: 3
     * - minOccurs: 0
     *
     * @var Categorisation[]
     */
    protected ?array $categorisation = null;

    /**
     * Constructor method for Status
     *
     * @param Categorisation[] $categorisation
     * @uses Status::setCategorisation()
     */
    public function __construct(?array $categorisation = null)
    {
        $this
            ->setCategorisation($categorisation);
    }

    /**
     * This method is responsible for validating the values passed to the setCategorisation method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCategorisation method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCategorisationForArrayConstraintsFromSetCategorisation(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $statusCategorisationItem) {
            // validation for constraint: itemType
            if (!$statusCategorisationItem instanceof Categorisation) {
                $invalidValues[] = is_object($statusCategorisationItem) ? get_class($statusCategorisationItem) : sprintf(
                    '%s(%s)',
                    gettype($statusCategorisationItem),
                    var_export($statusCategorisationItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The categorisation property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\Categorisation, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get categorisation value
     *
     * @return Categorisation[]
     */
    public function getCategorisation(): ?array
    {
        return $this->categorisation;
    }

    /**
     * Set categorisation value
     *
     * @param Categorisation[] $categorisation
     * @return Status
     * @throws InvalidArgumentException
     */
    public function setCategorisation(?array $categorisation = null): self
    {
        // validation for constraint: array
        if ('' !== ($categorisationArrayErrorMessage = self::validateCategorisationForArrayConstraintsFromSetCategorisation($categorisation))) {
            throw new InvalidArgumentException($categorisationArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(3)
        if (is_array($categorisation) && count($categorisation) > 3) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 3',
                count($categorisation)
            ), __LINE__);
        }
        $this->categorisation = $categorisation;

        return $this;
    }

    /**
     * Add item to categorisation value
     *
     * @param Categorisation $item
     * @return Status
     * @throws InvalidArgumentException
     */
    public function addToCategorisation(Categorisation $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof Categorisation) {
            throw new InvalidArgumentException(sprintf(
                'The categorisation property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\Categorisation, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(3)
        if (is_array($this->categorisation) && count($this->categorisation) >= 3) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 3',
                count($this->categorisation)
            ), __LINE__);
        }
        $this->categorisation[] = $item;

        return $this;
    }
}
