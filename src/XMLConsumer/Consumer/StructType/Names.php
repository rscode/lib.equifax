<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Names StructType
 *
 * @subpackage Structs
 */
class Names extends AbstractStructBase
{
    /**
     * The name
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $name;
    /**
     * The gender
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $gender;
    /**
     * The originalScript
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var OriginalScript
     */
    protected OriginalScript $originalScript;
    /**
     * The maidenName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $maidenName;
    /**
     * The birth
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var Birth|null
     */
    protected ?Birth $birth = null;
    /**
     * The alias
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var Alias|null
     */
    protected ?Alias $alias = null;
    /**
     * The alternateSpellings
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AlternateSpellings|null
     */
    protected ?AlternateSpellings $alternateSpellings = null;

    /**
     * Constructor method for Names
     *
     * @param string             $name
     * @param string             $gender
     * @param OriginalScript     $originalScript
     * @param string             $maidenName
     * @param Birth              $birth
     * @param Alias              $alias
     * @param AlternateSpellings $alternateSpellings
     * @uses Names::setName()
     * @uses Names::setGender()
     * @uses Names::setOriginalScript()
     * @uses Names::setMaidenName()
     * @uses Names::setBirth()
     * @uses Names::setAlias()
     * @uses Names::setAlternateSpellings()
     */
    public function __construct(
        string $name,
        string $gender,
        OriginalScript $originalScript,
        string $maidenName,
        ?Birth $birth = null,
        ?Alias $alias = null,
        ?AlternateSpellings $alternateSpellings = null
    ) {
        $this
            ->setName($name)
            ->setGender($gender)
            ->setOriginalScript($originalScript)
            ->setMaidenName($maidenName)
            ->setBirth($birth)
            ->setAlias($alias)
            ->setAlternateSpellings($alternateSpellings);
    }

    /**
     * Get name value
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set name value
     *
     * @param string $name
     * @return Names
     */
    public function setName(string $name): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($name, true),
                gettype($name)
            ), __LINE__);
        }
        $this->name = $name;

        return $this;
    }

    /**
     * Get gender value
     *
     * @return string
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * Set gender value
     *
     * @param string $gender
     * @return Names
     */
    public function setGender(string $gender): self
    {
        // validation for constraint: string
        if (!is_null($gender) && !is_string($gender)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($gender, true),
                gettype($gender)
            ), __LINE__);
        }
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get originalScript value
     *
     * @return OriginalScript
     */
    public function getOriginalScript(): OriginalScript
    {
        return $this->originalScript;
    }

    /**
     * Set originalScript value
     *
     * @param OriginalScript $originalScript
     * @return Names
     */
    public function setOriginalScript(OriginalScript $originalScript): self
    {
        $this->originalScript = $originalScript;

        return $this;
    }

    /**
     * Get maidenName value
     *
     * @return string
     */
    public function getMaidenName(): string
    {
        return $this->maidenName;
    }

    /**
     * Set maidenName value
     *
     * @param string $maidenName
     * @return Names
     */
    public function setMaidenName(string $maidenName): self
    {
        // validation for constraint: string
        if (!is_null($maidenName) && !is_string($maidenName)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($maidenName, true),
                gettype($maidenName)
            ), __LINE__);
        }
        $this->maidenName = $maidenName;

        return $this;
    }

    /**
     * Get birth value
     *
     * @return Birth|null
     */
    public function getBirth(): ?Birth
    {
        return $this->birth;
    }

    /**
     * Set birth value
     *
     * @param Birth $birth
     * @return Names
     */
    public function setBirth(?Birth $birth = null): self
    {
        $this->birth = $birth;

        return $this;
    }

    /**
     * Get alias value
     *
     * @return Alias|null
     */
    public function getAlias(): ?Alias
    {
        return $this->alias;
    }

    /**
     * Set alias value
     *
     * @param Alias $alias
     * @return Names
     */
    public function setAlias(?Alias $alias = null): self
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alternateSpellings value
     *
     * @return AlternateSpellings|null
     */
    public function getAlternateSpellings(): ?AlternateSpellings
    {
        return $this->alternateSpellings;
    }

    /**
     * Set alternateSpellings value
     *
     * @param AlternateSpellings $alternateSpellings
     * @return Names
     */
    public function setAlternateSpellings(?AlternateSpellings $alternateSpellings = null): self
    {
        $this->alternateSpellings = $alternateSpellings;

        return $this;
    }
}
