<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for RelativesAndCloseAssociates StructType
 *
 * @subpackage Structs
 */
class RelativesAndCloseAssociates extends AbstractStructBase
{
    /**
     * The relativeAndCloseAssociate
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var RelativeAndCloseAssociate[]
     */
    protected ?array $relativeAndCloseAssociate = null;

    /**
     * Constructor method for RelativesAndCloseAssociates
     *
     * @param RelativeAndCloseAssociate[] $relativeAndCloseAssociate
     * @uses RelativesAndCloseAssociates::setRelativeAndCloseAssociate()
     */
    public function __construct(?array $relativeAndCloseAssociate = null)
    {
        $this
            ->setRelativeAndCloseAssociate($relativeAndCloseAssociate);
    }

    /**
     * This method is responsible for validating the values passed to the setRelativeAndCloseAssociate method
     * This method is willingly generated in order to preserve the one-line inline validation within the setRelativeAndCloseAssociate method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateRelativeAndCloseAssociateForArrayConstraintsFromSetRelativeAndCloseAssociate(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $relativesAndCloseAssociatesRelativeAndCloseAssociateItem) {
            // validation for constraint: itemType
            if (!$relativesAndCloseAssociatesRelativeAndCloseAssociateItem instanceof RelativeAndCloseAssociate) {
                $invalidValues[] = is_object($relativesAndCloseAssociatesRelativeAndCloseAssociateItem) ? get_class($relativesAndCloseAssociatesRelativeAndCloseAssociateItem) : sprintf(
                    '%s(%s)',
                    gettype($relativesAndCloseAssociatesRelativeAndCloseAssociateItem),
                    var_export($relativesAndCloseAssociatesRelativeAndCloseAssociateItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The relativeAndCloseAssociate property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\RelativeAndCloseAssociate, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get relativeAndCloseAssociate value
     *
     * @return RelativeAndCloseAssociate[]
     */
    public function getRelativeAndCloseAssociate(): ?array
    {
        return $this->relativeAndCloseAssociate;
    }

    /**
     * Set relativeAndCloseAssociate value
     *
     * @param RelativeAndCloseAssociate[] $relativeAndCloseAssociate
     * @return RelativesAndCloseAssociates
     * @throws InvalidArgumentException
     */
    public function setRelativeAndCloseAssociate(?array $relativeAndCloseAssociate = null): self
    {
        // validation for constraint: array
        if ('' !== ($relativeAndCloseAssociateArrayErrorMessage = self::validateRelativeAndCloseAssociateForArrayConstraintsFromSetRelativeAndCloseAssociate($relativeAndCloseAssociate))) {
            throw new InvalidArgumentException($relativeAndCloseAssociateArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($relativeAndCloseAssociate) && count($relativeAndCloseAssociate) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($relativeAndCloseAssociate)
            ), __LINE__);
        }
        $this->relativeAndCloseAssociate = $relativeAndCloseAssociate;

        return $this;
    }

    /**
     * Add item to relativeAndCloseAssociate value
     *
     * @param RelativeAndCloseAssociate $item
     * @return RelativesAndCloseAssociates
     * @throws InvalidArgumentException
     */
    public function addToRelativeAndCloseAssociate(RelativeAndCloseAssociate $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof RelativeAndCloseAssociate) {
            throw new InvalidArgumentException(sprintf(
                'The relativeAndCloseAssociate property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\RelativeAndCloseAssociate, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->relativeAndCloseAssociate) && count($this->relativeAndCloseAssociate) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->relativeAndCloseAssociate)
            ), __LINE__);
        }
        $this->relativeAndCloseAssociate[] = $item;

        return $this;
    }
}
