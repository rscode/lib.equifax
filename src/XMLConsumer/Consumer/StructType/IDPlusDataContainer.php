<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for IDPlusDataContainer StructType
 *
 * @subpackage Structs
 */
class IDPlusDataContainer extends DataItemContainer
{
    /**
     * The idPlusCounts
     * Meta information extracted from the WSDL
     * - maxOccurs: 2
     * - minOccurs: 0
     *
     * @var IDPlusCounts[]
     */
    protected ?array $idPlusCounts = null;

    /**
     * Constructor method for IDPlusDataContainer
     *
     * @param IDPlusCounts[] $idPlusCounts
     * @uses IDPlusDataContainer::setIdPlusCounts()
     */
    public function __construct(?array $idPlusCounts = null)
    {
        $this
            ->setIdPlusCounts($idPlusCounts);
    }

    /**
     * This method is responsible for validating the values passed to the setIdPlusCounts method
     * This method is willingly generated in order to preserve the one-line inline validation within the setIdPlusCounts method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateIdPlusCountsForArrayConstraintsFromSetIdPlusCounts(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $iDPlusDataContainerIdPlusCountsItem) {
            // validation for constraint: itemType
            if (!$iDPlusDataContainerIdPlusCountsItem instanceof IDPlusCounts) {
                $invalidValues[] = is_object($iDPlusDataContainerIdPlusCountsItem) ? get_class($iDPlusDataContainerIdPlusCountsItem) : sprintf(
                    '%s(%s)',
                    gettype($iDPlusDataContainerIdPlusCountsItem),
                    var_export($iDPlusDataContainerIdPlusCountsItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The idPlusCounts property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\IDPlusCounts, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get idPlusCounts value
     *
     * @return IDPlusCounts[]
     */
    public function getIdPlusCounts(): ?array
    {
        return $this->idPlusCounts;
    }

    /**
     * Set idPlusCounts value
     *
     * @param IDPlusCounts[] $idPlusCounts
     * @return IDPlusDataContainer
     * @throws InvalidArgumentException
     */
    public function setIdPlusCounts(?array $idPlusCounts = null): self
    {
        // validation for constraint: array
        if ('' !== ($idPlusCountsArrayErrorMessage = self::validateIdPlusCountsForArrayConstraintsFromSetIdPlusCounts($idPlusCounts))) {
            throw new InvalidArgumentException($idPlusCountsArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(2)
        if (is_array($idPlusCounts) && count($idPlusCounts) > 2) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 2',
                count($idPlusCounts)
            ), __LINE__);
        }
        $this->idPlusCounts = $idPlusCounts;

        return $this;
    }

    /**
     * Add item to idPlusCounts value
     *
     * @param IDPlusCounts $item
     * @return IDPlusDataContainer
     * @throws InvalidArgumentException
     */
    public function addToIdPlusCounts(IDPlusCounts $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof IDPlusCounts) {
            throw new InvalidArgumentException(sprintf(
                'The idPlusCounts property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\IDPlusCounts, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(2)
        if (is_array($this->idPlusCounts) && count($this->idPlusCounts) >= 2) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 2',
                count($this->idPlusCounts)
            ), __LINE__);
        }
        $this->idPlusCounts[] = $item;

        return $this;
    }
}
