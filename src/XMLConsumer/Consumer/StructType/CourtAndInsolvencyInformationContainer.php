<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * @see https://developer.equifax.co.uk/sites/uk/files/xml_ref_guide/ref/Data%20Groups/Court%20And%20Insolvency%20Information.pdf
 */
class CourtAndInsolvencyInformationContainer extends DataItemContainer
{
    /**
     * The courtAndInsolvencyInformation
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var CourtAndInsolvencyInformation[]
     */
    protected ?array $courtAndInsolvencyInformation = null;

    /**
     * Constructor method for CourtAndInsolvencyInformationContainer
     *
     * @param CourtAndInsolvencyInformation[] $courtAndInsolvencyInformation
     * @uses CourtAndInsolvencyInformationContainer::setCourtAndInsolvencyInformation()
     */
    public function __construct(?array $courtAndInsolvencyInformation = null)
    {
        $this
            ->setCourtAndInsolvencyInformation($courtAndInsolvencyInformation);
    }

    /**
     * This method is responsible for validating the values passed to the setCourtAndInsolvencyInformation method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCourtAndInsolvencyInformation method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCourtAndInsolvencyInformationForArrayConstraintsFromSetCourtAndInsolvencyInformation(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $courtAndInsolvencyInformationContainerCourtAndInsolvencyInformationItem) {
            // validation for constraint: itemType
            if (!$courtAndInsolvencyInformationContainerCourtAndInsolvencyInformationItem instanceof CourtAndInsolvencyInformation) {
                $invalidValues[] = is_object($courtAndInsolvencyInformationContainerCourtAndInsolvencyInformationItem) ? get_class($courtAndInsolvencyInformationContainerCourtAndInsolvencyInformationItem) : sprintf(
                    '%s(%s)',
                    gettype($courtAndInsolvencyInformationContainerCourtAndInsolvencyInformationItem),
                    var_export($courtAndInsolvencyInformationContainerCourtAndInsolvencyInformationItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The courtAndInsolvencyInformation property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\CourtAndInsolvencyInformation, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get courtAndInsolvencyInformation value
     *
     * @return CourtAndInsolvencyInformation[]
     */
    public function getCourtAndInsolvencyInformation(): ?array
    {
        return $this->courtAndInsolvencyInformation;
    }

    /**
     * Set courtAndInsolvencyInformation value
     *
     * @param CourtAndInsolvencyInformation[] $courtAndInsolvencyInformation
     * @return CourtAndInsolvencyInformationContainer
     * @throws InvalidArgumentException
     */
    public function setCourtAndInsolvencyInformation(?array $courtAndInsolvencyInformation = null): self
    {
        // validation for constraint: array
        if ('' !== ($courtAndInsolvencyInformationArrayErrorMessage = self::validateCourtAndInsolvencyInformationForArrayConstraintsFromSetCourtAndInsolvencyInformation($courtAndInsolvencyInformation))) {
            throw new InvalidArgumentException($courtAndInsolvencyInformationArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($courtAndInsolvencyInformation) && count($courtAndInsolvencyInformation) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($courtAndInsolvencyInformation)
            ), __LINE__);
        }
        $this->courtAndInsolvencyInformation = $courtAndInsolvencyInformation;

        return $this;
    }

    /**
     * Add item to courtAndInsolvencyInformation value
     *
     * @param CourtAndInsolvencyInformation $item
     * @return CourtAndInsolvencyInformationContainer
     * @throws InvalidArgumentException
     */
    public function addToCourtAndInsolvencyInformation(CourtAndInsolvencyInformation $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof CourtAndInsolvencyInformation) {
            throw new InvalidArgumentException(sprintf(
                'The courtAndInsolvencyInformation property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\CourtAndInsolvencyInformation, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->courtAndInsolvencyInformation) && count($this->courtAndInsolvencyInformation) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->courtAndInsolvencyInformation)
            ), __LINE__);
        }
        $this->courtAndInsolvencyInformation[] = $item;

        return $this;
    }
}
