<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AddressSpecificData StructType
 * Meta information extracted from the WSDL
 * - documentation: Contains requested data groups if found during enquiry processing.
 *
 * @subpackage Structs
 */
class AddressSpecificData extends AbstractStructBase
{
    /**
     * The amlHMTreasurySanctionsCheckData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AMLHMTreasurySanctionsCheckContainer|null
     */
    protected ?AMLHMTreasurySanctionsCheckContainer $amlHMTreasurySanctionsCheckData = null;
    /**
     * The amlHaloDeceasedCheckData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AMLHaloDeceasedCheckContainer|null
     */
    protected ?AMLHaloDeceasedCheckContainer $amlHaloDeceasedCheckData = null;
    /**
     * The amlOFACCheckData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AMLOFACCheckContainer|null
     */
    protected ?AMLOFACCheckContainer $amlOFACCheckData = null;
    /**
     * The amlSeniorPoliticalFiguresSanctionsCheckData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AMLSeniorPoliticalFiguresSanctionsCheckContainer|null
     */
    protected ?AMLSeniorPoliticalFiguresSanctionsCheckContainer $amlSeniorPoliticalFiguresSanctionsCheckData = null;
    /**
     * The amlSummaryData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AMLSummaryContainer|null
     */
    protected ?AMLSummaryContainer $amlSummaryData = null;
    /**
     * The bankCheckData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var BankCheckContainer|null
     */
    protected ?BankCheckContainer $bankCheckData = null;
    /**
     * The bureauVerificationData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var BureauVerificationContainer|null
     */
    protected ?BureauVerificationContainer $bureauVerificationData = null;
    /**
     * The cifasData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var CIFASContainer|null
     */
    protected ?CIFASContainer $cifasData = null;
    /**
     * The courtAndInsolvencyInformationData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var CourtAndInsolvencyInformationContainer|null
     */
    protected ?CourtAndInsolvencyInformationContainer $courtAndInsolvencyInformationData = null;
    /**
     * The electoralRollData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var ElectoralRollContainer|null
     */
    protected ?ElectoralRollContainer $electoralRollData = null;
    /**
     * The employerCheckData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var EmployerCheckContainer|null
     */
    protected ?EmployerCheckContainer $employerCheckData = null;
    /**
     * The extendedHomeTelephoneCheckData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var ExtendedHomeTelephoneCheckContainer|null
     */
    protected ?ExtendedHomeTelephoneCheckContainer $extendedHomeTelephoneCheckData = null;
    /**
     * The fraudProfileSummaryData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var FraudProfileSummaryContainer|null
     */
    protected ?FraudProfileSummaryContainer $fraudProfileSummaryData = null;
    /**
     * The gainData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var GAINContainer|null
     */
    protected ?GAINContainer $gainData = null;
    /**
     * The homeTelephoneCheckData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var HomeTelephoneCheckContainer|null
     */
    protected ?HomeTelephoneCheckContainer $homeTelephoneCheckData = null;
    /**
     * The IDPlusData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var IDPlusDataContainer|null
     */
    protected ?IDPlusDataContainer $IDPlusData = null;
    /**
     * The insightData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var InsightAccountContainer|null
     */
    protected ?InsightAccountContainer $insightData = null;
    /**
     * The linkNamesNotifications
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var LinkNamesNotificationContainer|null
     */
    protected ?LinkNamesNotificationContainer $linkNamesNotifications = null;
    /**
     * The noticeOfCorrectionOrDisputeData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var NoticeOfCorrectionOrDisputeContainer|null
     */
    protected ?NoticeOfCorrectionOrDisputeContainer $noticeOfCorrectionOrDisputeData = null;
    /**
     * The previousSearches
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var PreviousSearchContainer|null
     */
    protected ?PreviousSearchContainer $previousSearches = null;
    /**
     * The propertyValuationData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var PropertyValuationDataContainer|null
     */
    protected ?PropertyValuationDataContainer $propertyValuationData = null;
    /**
     * The rollingRegisterData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var RollingRegisterContainer|null
     */
    protected ?RollingRegisterContainer $rollingRegisterData = null;
    /**
     * The siranChecksData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var SiranChecksContainer|null
     */
    protected ?SiranChecksContainer $siranChecksData = null;
    /**
     * The telephoneData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var TelephoneDataContainer|null
     */
    protected ?TelephoneDataContainer $telephoneData = null;

    /**
     * Constructor method for AddressSpecificData
     *
     * @param AMLHMTreasurySanctionsCheckContainer $amlHMTreasurySanctionsCheckData
     * @param AMLHaloDeceasedCheckContainer $amlHaloDeceasedCheckData
     * @param AMLOFACCheckContainer $amlOFACCheckData
     * @param AMLSeniorPoliticalFiguresSanctionsCheckContainer $amlSeniorPoliticalFiguresSanctionsCheckData
     * @param AMLSummaryContainer $amlSummaryData
     * @param BankCheckContainer $bankCheckData
     * @param BureauVerificationContainer $bureauVerificationData
     * @param CIFASContainer $cifasData
     * @param CourtAndInsolvencyInformationContainer $courtAndInsolvencyInformationData
     * @param ElectoralRollContainer $electoralRollData
     * @param EmployerCheckContainer $employerCheckData
     * @param ExtendedHomeTelephoneCheckContainer $extendedHomeTelephoneCheckData
     * @param FraudProfileSummaryContainer $fraudProfileSummaryData
     * @param GAINContainer $gainData
     * @param HomeTelephoneCheckContainer $homeTelephoneCheckData
     * @param IDPlusDataContainer $iDPlusData
     * @param InsightAccountContainer $insightData
     * @param LinkNamesNotificationContainer $linkNamesNotifications
     * @param NoticeOfCorrectionOrDisputeContainer $noticeOfCorrectionOrDisputeData
     * @param PreviousSearchContainer $previousSearches
     * @param PropertyValuationDataContainer $propertyValuationData
     * @param RollingRegisterContainer $rollingRegisterData
     * @param SiranChecksContainer $siranChecksData
     * @param TelephoneDataContainer $telephoneData
     * @uses AddressSpecificData::setAmlHMTreasurySanctionsCheckData()
     * @uses AddressSpecificData::setAmlHaloDeceasedCheckData()
     * @uses AddressSpecificData::setAmlOFACCheckData()
     * @uses AddressSpecificData::setAmlSeniorPoliticalFiguresSanctionsCheckData()
     * @uses AddressSpecificData::setAmlSummaryData()
     * @uses AddressSpecificData::setBankCheckData()
     * @uses AddressSpecificData::setBureauVerificationData()
     * @uses AddressSpecificData::setCifasData()
     * @uses AddressSpecificData::setCourtAndInsolvencyInformationData()
     * @uses AddressSpecificData::setElectoralRollData()
     * @uses AddressSpecificData::setEmployerCheckData()
     * @uses AddressSpecificData::setExtendedHomeTelephoneCheckData()
     * @uses AddressSpecificData::setFraudProfileSummaryData()
     * @uses AddressSpecificData::setGainData()
     * @uses AddressSpecificData::setHomeTelephoneCheckData()
     * @uses AddressSpecificData::setIDPlusData()
     * @uses AddressSpecificData::setInsightData()
     * @uses AddressSpecificData::setLinkNamesNotifications()
     * @uses AddressSpecificData::setNoticeOfCorrectionOrDisputeData()
     * @uses AddressSpecificData::setPreviousSearches()
     * @uses AddressSpecificData::setPropertyValuationData()
     * @uses AddressSpecificData::setRollingRegisterData()
     * @uses AddressSpecificData::setSiranChecksData()
     * @uses AddressSpecificData::setTelephoneData()
     */
    public function __construct(
        ?AMLHMTreasurySanctionsCheckContainer $amlHMTreasurySanctionsCheckData = null,
        ?AMLHaloDeceasedCheckContainer $amlHaloDeceasedCheckData = null,
        ?AMLOFACCheckContainer $amlOFACCheckData = null,
        ?AMLSeniorPoliticalFiguresSanctionsCheckContainer $amlSeniorPoliticalFiguresSanctionsCheckData = null,
        ?AMLSummaryContainer $amlSummaryData = null,
        ?BankCheckContainer $bankCheckData = null,
        ?BureauVerificationContainer $bureauVerificationData = null,
        ?CIFASContainer $cifasData = null,
        ?CourtAndInsolvencyInformationContainer $courtAndInsolvencyInformationData = null,
        ?ElectoralRollContainer $electoralRollData = null,
        ?EmployerCheckContainer $employerCheckData = null,
        ?ExtendedHomeTelephoneCheckContainer $extendedHomeTelephoneCheckData = null,
        ?FraudProfileSummaryContainer $fraudProfileSummaryData = null,
        ?GAINContainer $gainData = null,
        ?HomeTelephoneCheckContainer $homeTelephoneCheckData = null,
        ?IDPlusDataContainer $iDPlusData = null,
        ?InsightAccountContainer $insightData = null,
        ?LinkNamesNotificationContainer $linkNamesNotifications = null,
        ?NoticeOfCorrectionOrDisputeContainer $noticeOfCorrectionOrDisputeData = null,
        ?PreviousSearchContainer $previousSearches = null,
        ?PropertyValuationDataContainer $propertyValuationData = null,
        ?RollingRegisterContainer $rollingRegisterData = null,
        ?SiranChecksContainer $siranChecksData = null,
        ?TelephoneDataContainer $telephoneData = null
    ) {
        $this
            ->setAmlHMTreasurySanctionsCheckData($amlHMTreasurySanctionsCheckData)
            ->setAmlHaloDeceasedCheckData($amlHaloDeceasedCheckData)
            ->setAmlOFACCheckData($amlOFACCheckData)
            ->setAmlSeniorPoliticalFiguresSanctionsCheckData($amlSeniorPoliticalFiguresSanctionsCheckData)
            ->setAmlSummaryData($amlSummaryData)
            ->setBankCheckData($bankCheckData)
            ->setBureauVerificationData($bureauVerificationData)
            ->setCifasData($cifasData)
            ->setCourtAndInsolvencyInformationData($courtAndInsolvencyInformationData)
            ->setElectoralRollData($electoralRollData)
            ->setEmployerCheckData($employerCheckData)
            ->setExtendedHomeTelephoneCheckData($extendedHomeTelephoneCheckData)
            ->setFraudProfileSummaryData($fraudProfileSummaryData)
            ->setGainData($gainData)
            ->setHomeTelephoneCheckData($homeTelephoneCheckData)
            ->setIDPlusData($iDPlusData)
            ->setInsightData($insightData)
            ->setLinkNamesNotifications($linkNamesNotifications)
            ->setNoticeOfCorrectionOrDisputeData($noticeOfCorrectionOrDisputeData)
            ->setPreviousSearches($previousSearches)
            ->setPropertyValuationData($propertyValuationData)
            ->setRollingRegisterData($rollingRegisterData)
            ->setSiranChecksData($siranChecksData)
            ->setTelephoneData($telephoneData);
    }

    /**
     * Get amlHMTreasurySanctionsCheckData value
     *
     * @return AMLHMTreasurySanctionsCheckContainer|null
     */
    public function getAmlHMTreasurySanctionsCheckData(): ?AMLHMTreasurySanctionsCheckContainer
    {
        return $this->amlHMTreasurySanctionsCheckData;
    }

    /**
     * Set amlHMTreasurySanctionsCheckData value
     *
     * @param AMLHMTreasurySanctionsCheckContainer $amlHMTreasurySanctionsCheckData
     * @return AddressSpecificData
     */
    public function setAmlHMTreasurySanctionsCheckData(?AMLHMTreasurySanctionsCheckContainer $amlHMTreasurySanctionsCheckData = null): self
    {
        $this->amlHMTreasurySanctionsCheckData = $amlHMTreasurySanctionsCheckData;

        return $this;
    }

    /**
     * Get amlHaloDeceasedCheckData value
     *
     * @return AMLHaloDeceasedCheckContainer|null
     */
    public function getAmlHaloDeceasedCheckData(): ?AMLHaloDeceasedCheckContainer
    {
        return $this->amlHaloDeceasedCheckData;
    }

    /**
     * Set amlHaloDeceasedCheckData value
     *
     * @param AMLHaloDeceasedCheckContainer $amlHaloDeceasedCheckData
     * @return AddressSpecificData
     */
    public function setAmlHaloDeceasedCheckData(?AMLHaloDeceasedCheckContainer $amlHaloDeceasedCheckData = null): self
    {
        $this->amlHaloDeceasedCheckData = $amlHaloDeceasedCheckData;

        return $this;
    }

    /**
     * Get amlOFACCheckData value
     *
     * @return AMLOFACCheckContainer|null
     */
    public function getAmlOFACCheckData(): ?AMLOFACCheckContainer
    {
        return $this->amlOFACCheckData;
    }

    /**
     * Set amlOFACCheckData value
     *
     * @param AMLOFACCheckContainer $amlOFACCheckData
     * @return AddressSpecificData
     */
    public function setAmlOFACCheckData(?AMLOFACCheckContainer $amlOFACCheckData = null): self
    {
        $this->amlOFACCheckData = $amlOFACCheckData;

        return $this;
    }

    /**
     * Get amlSeniorPoliticalFiguresSanctionsCheckData value
     *
     * @return AMLSeniorPoliticalFiguresSanctionsCheckContainer|null
     */
    public function getAmlSeniorPoliticalFiguresSanctionsCheckData(): ?AMLSeniorPoliticalFiguresSanctionsCheckContainer
    {
        return $this->amlSeniorPoliticalFiguresSanctionsCheckData;
    }

    /**
     * Set amlSeniorPoliticalFiguresSanctionsCheckData value
     *
     * @param AMLSeniorPoliticalFiguresSanctionsCheckContainer $amlSeniorPoliticalFiguresSanctionsCheckData
     * @return AddressSpecificData
     */
    public function setAmlSeniorPoliticalFiguresSanctionsCheckData(
        ?AMLSeniorPoliticalFiguresSanctionsCheckContainer $amlSeniorPoliticalFiguresSanctionsCheckData = null
    ): self {
        $this->amlSeniorPoliticalFiguresSanctionsCheckData = $amlSeniorPoliticalFiguresSanctionsCheckData;

        return $this;
    }

    /**
     * Get amlSummaryData value
     *
     * @return AMLSummaryContainer|null
     */
    public function getAmlSummaryData(): ?AMLSummaryContainer
    {
        return $this->amlSummaryData;
    }

    /**
     * Set amlSummaryData value
     *
     * @param AMLSummaryContainer $amlSummaryData
     * @return AddressSpecificData
     */
    public function setAmlSummaryData(?AMLSummaryContainer $amlSummaryData = null): self
    {
        $this->amlSummaryData = $amlSummaryData;

        return $this;
    }

    /**
     * Get bankCheckData value
     *
     * @return BankCheckContainer|null
     */
    public function getBankCheckData(): ?BankCheckContainer
    {
        return $this->bankCheckData;
    }

    /**
     * Set bankCheckData value
     *
     * @param BankCheckContainer $bankCheckData
     * @return AddressSpecificData
     */
    public function setBankCheckData(?BankCheckContainer $bankCheckData = null): self
    {
        $this->bankCheckData = $bankCheckData;

        return $this;
    }

    /**
     * Get bureauVerificationData value
     *
     * @return BureauVerificationContainer|null
     */
    public function getBureauVerificationData(): ?BureauVerificationContainer
    {
        return $this->bureauVerificationData;
    }

    /**
     * Set bureauVerificationData value
     *
     * @param BureauVerificationContainer $bureauVerificationData
     * @return AddressSpecificData
     */
    public function setBureauVerificationData(?BureauVerificationContainer $bureauVerificationData = null): self
    {
        $this->bureauVerificationData = $bureauVerificationData;

        return $this;
    }

    /**
     * Get cifasData value
     *
     * @return CIFASContainer|null
     */
    public function getCifasData(): ?CIFASContainer
    {
        return $this->cifasData;
    }

    /**
     * Set cifasData value
     *
     * @param CIFASContainer $cifasData
     * @return AddressSpecificData
     */
    public function setCifasData(?CIFASContainer $cifasData = null): self
    {
        $this->cifasData = $cifasData;

        return $this;
    }

    /**
     * Get courtAndInsolvencyInformationData value
     *
     * @return CourtAndInsolvencyInformationContainer|null
     */
    public function getCourtAndInsolvencyInformationData(): ?CourtAndInsolvencyInformationContainer
    {
        return $this->courtAndInsolvencyInformationData;
    }

    /**
     * Set courtAndInsolvencyInformationData value
     *
     * @param CourtAndInsolvencyInformationContainer $courtAndInsolvencyInformationData
     * @return AddressSpecificData
     */
    public function setCourtAndInsolvencyInformationData(?CourtAndInsolvencyInformationContainer $courtAndInsolvencyInformationData = null): self
    {
        $this->courtAndInsolvencyInformationData = $courtAndInsolvencyInformationData;

        return $this;
    }

    /**
     * Get electoralRollData value
     *
     * @return ElectoralRollContainer|null
     */
    public function getElectoralRollData(): ?ElectoralRollContainer
    {
        return $this->electoralRollData;
    }

    /**
     * Set electoralRollData value
     *
     * @param ElectoralRollContainer $electoralRollData
     * @return AddressSpecificData
     */
    public function setElectoralRollData(?ElectoralRollContainer $electoralRollData = null): self
    {
        $this->electoralRollData = $electoralRollData;

        return $this;
    }

    /**
     * Get employerCheckData value
     *
     * @return EmployerCheckContainer|null
     */
    public function getEmployerCheckData(): ?EmployerCheckContainer
    {
        return $this->employerCheckData;
    }

    /**
     * Set employerCheckData value
     *
     * @param EmployerCheckContainer $employerCheckData
     * @return AddressSpecificData
     */
    public function setEmployerCheckData(?EmployerCheckContainer $employerCheckData = null): self
    {
        $this->employerCheckData = $employerCheckData;

        return $this;
    }

    /**
     * Get extendedHomeTelephoneCheckData value
     *
     * @return ExtendedHomeTelephoneCheckContainer|null
     */
    public function getExtendedHomeTelephoneCheckData(): ?ExtendedHomeTelephoneCheckContainer
    {
        return $this->extendedHomeTelephoneCheckData;
    }

    /**
     * Set extendedHomeTelephoneCheckData value
     *
     * @param ExtendedHomeTelephoneCheckContainer $extendedHomeTelephoneCheckData
     * @return AddressSpecificData
     */
    public function setExtendedHomeTelephoneCheckData(?ExtendedHomeTelephoneCheckContainer $extendedHomeTelephoneCheckData = null): self
    {
        $this->extendedHomeTelephoneCheckData = $extendedHomeTelephoneCheckData;

        return $this;
    }

    /**
     * Get fraudProfileSummaryData value
     *
     * @return FraudProfileSummaryContainer|null
     */
    public function getFraudProfileSummaryData(): ?FraudProfileSummaryContainer
    {
        return $this->fraudProfileSummaryData;
    }

    /**
     * Set fraudProfileSummaryData value
     *
     * @param FraudProfileSummaryContainer $fraudProfileSummaryData
     * @return AddressSpecificData
     */
    public function setFraudProfileSummaryData(?FraudProfileSummaryContainer $fraudProfileSummaryData = null): self
    {
        $this->fraudProfileSummaryData = $fraudProfileSummaryData;

        return $this;
    }

    /**
     * Get gainData value
     *
     * @return GAINContainer|null
     */
    public function getGainData(): ?GAINContainer
    {
        return $this->gainData;
    }

    /**
     * Set gainData value
     *
     * @param GAINContainer $gainData
     * @return AddressSpecificData
     */
    public function setGainData(?GAINContainer $gainData = null): self
    {
        $this->gainData = $gainData;

        return $this;
    }

    /**
     * Get homeTelephoneCheckData value
     *
     * @return HomeTelephoneCheckContainer|null
     */
    public function getHomeTelephoneCheckData(): ?HomeTelephoneCheckContainer
    {
        return $this->homeTelephoneCheckData;
    }

    /**
     * Set homeTelephoneCheckData value
     *
     * @param HomeTelephoneCheckContainer $homeTelephoneCheckData
     * @return AddressSpecificData
     */
    public function setHomeTelephoneCheckData(?HomeTelephoneCheckContainer $homeTelephoneCheckData = null): self
    {
        $this->homeTelephoneCheckData = $homeTelephoneCheckData;

        return $this;
    }

    /**
     * Get IDPlusData value
     *
     * @return IDPlusDataContainer|null
     */
    public function getIDPlusData(): ?IDPlusDataContainer
    {
        return $this->IDPlusData;
    }

    /**
     * Set IDPlusData value
     *
     * @param IDPlusDataContainer $iDPlusData
     * @return AddressSpecificData
     */
    public function setIDPlusData(?IDPlusDataContainer $iDPlusData = null): self
    {
        $this->IDPlusData = $iDPlusData;

        return $this;
    }

    /**
     * Get insightData value
     *
     * @return InsightAccountContainer|null
     */
    public function getInsightData(): ?InsightAccountContainer
    {
        return $this->insightData;
    }

    /**
     * Set insightData value
     *
     * @param InsightAccountContainer $insightData
     * @return AddressSpecificData
     */
    public function setInsightData(?InsightAccountContainer $insightData = null): self
    {
        $this->insightData = $insightData;

        return $this;
    }

    /**
     * Get linkNamesNotifications value
     *
     * @return LinkNamesNotificationContainer|null
     */
    public function getLinkNamesNotifications(): ?LinkNamesNotificationContainer
    {
        return $this->linkNamesNotifications;
    }

    /**
     * Set linkNamesNotifications value
     *
     * @param LinkNamesNotificationContainer $linkNamesNotifications
     * @return AddressSpecificData
     */
    public function setLinkNamesNotifications(?LinkNamesNotificationContainer $linkNamesNotifications = null): self
    {
        $this->linkNamesNotifications = $linkNamesNotifications;

        return $this;
    }

    /**
     * Get noticeOfCorrectionOrDisputeData value
     *
     * @return NoticeOfCorrectionOrDisputeContainer|null
     */
    public function getNoticeOfCorrectionOrDisputeData(): ?NoticeOfCorrectionOrDisputeContainer
    {
        return $this->noticeOfCorrectionOrDisputeData;
    }

    /**
     * Set noticeOfCorrectionOrDisputeData value
     *
     * @param NoticeOfCorrectionOrDisputeContainer $noticeOfCorrectionOrDisputeData
     * @return AddressSpecificData
     */
    public function setNoticeOfCorrectionOrDisputeData(?NoticeOfCorrectionOrDisputeContainer $noticeOfCorrectionOrDisputeData = null): self
    {
        $this->noticeOfCorrectionOrDisputeData = $noticeOfCorrectionOrDisputeData;

        return $this;
    }

    /**
     * Get previousSearches value
     *
     * @return PreviousSearchContainer|null
     */
    public function getPreviousSearches(): ?PreviousSearchContainer
    {
        return $this->previousSearches;
    }

    /**
     * Set previousSearches value
     *
     * @param PreviousSearchContainer $previousSearches
     * @return AddressSpecificData
     */
    public function setPreviousSearches(?PreviousSearchContainer $previousSearches = null): self
    {
        $this->previousSearches = $previousSearches;

        return $this;
    }

    /**
     * Get propertyValuationData value
     *
     * @return PropertyValuationDataContainer|null
     */
    public function getPropertyValuationData(): ?PropertyValuationDataContainer
    {
        return $this->propertyValuationData;
    }

    /**
     * Set propertyValuationData value
     *
     * @param PropertyValuationDataContainer $propertyValuationData
     * @return AddressSpecificData
     */
    public function setPropertyValuationData(?PropertyValuationDataContainer $propertyValuationData = null): self
    {
        $this->propertyValuationData = $propertyValuationData;

        return $this;
    }

    /**
     * Get rollingRegisterData value
     *
     * @return RollingRegisterContainer|null
     */
    public function getRollingRegisterData(): ?RollingRegisterContainer
    {
        return $this->rollingRegisterData;
    }

    /**
     * Set rollingRegisterData value
     *
     * @param RollingRegisterContainer $rollingRegisterData
     * @return AddressSpecificData
     */
    public function setRollingRegisterData(?RollingRegisterContainer $rollingRegisterData = null): self
    {
        $this->rollingRegisterData = $rollingRegisterData;

        return $this;
    }

    /**
     * Get siranChecksData value
     *
     * @return SiranChecksContainer|null
     */
    public function getSiranChecksData(): ?SiranChecksContainer
    {
        return $this->siranChecksData;
    }

    /**
     * Set siranChecksData value
     *
     * @param SiranChecksContainer $siranChecksData
     * @return AddressSpecificData
     */
    public function setSiranChecksData(?SiranChecksContainer $siranChecksData = null): self
    {
        $this->siranChecksData = $siranChecksData;

        return $this;
    }

    /**
     * Get telephoneData value
     *
     * @return TelephoneDataContainer|null
     */
    public function getTelephoneData(): ?TelephoneDataContainer
    {
        return $this->telephoneData;
    }

    /**
     * Set telephoneData value
     *
     * @param TelephoneDataContainer $telephoneData
     * @return AddressSpecificData
     */
    public function setTelephoneData(?TelephoneDataContainer $telephoneData = null): self
    {
        $this->telephoneData = $telephoneData;

        return $this;
    }
}
