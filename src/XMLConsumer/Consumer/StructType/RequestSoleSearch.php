<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for RequestSoleSearch StructType
 * Meta information extracted from the WSDL
 * - documentation: Primary applicant | Nature of the search: sole applicant application
 *
 * @subpackage Structs
 */
class RequestSoleSearch extends Search
{
    /**
     * Primary applicant | Nature of the search: sole applicant application
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var RequestPerson
     */
    protected RequestPerson $primary;

    public function __construct()
    {
        $this->primary = new RequestPerson();

        parent::__construct();
    }

    public function getPrimary(): RequestPerson
    {
        return $this->primary;
    }

    public function setPrimary(RequestPerson $primary): self
    {
        $this->primary = $primary;

        return $this;
    }
}
