<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for InsightRequest StructType
 *
 * @subpackage Structs
 */
class InsightRequest extends RawDataAvailableAtLinkedAddressRequest
{
    /**
     * The lenderName
     *
     * @var bool|null
     */
    protected ?bool $lenderName = null;

    public function __construct(int $maxNumber, ?string $scope = null, ?bool $lenderName = null)
    {
        $this
            ->setLenderName($lenderName);

        parent::__construct($maxNumber, $scope);
    }

    public function getLenderName(): ?bool
    {
        return $this->lenderName;
    }

    public function setLenderName(?bool $lenderName = null): self
    {
        // validation for constraint: boolean
        if (!is_null($lenderName) && !is_bool($lenderName)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($lenderName, true),
                gettype($lenderName)
            ), __LINE__);
        }
        $this->lenderName = $lenderName;

        return $this;
    }
}
