<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for WatchlistCheck StructType
 *
 * @subpackage Structs
 */
class WatchlistCheck extends AbstractStructBase
{
    /**
     * The matchCategory
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $matchCategory;
    /**
     * The checkName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $checkName;
    /**
     * The percentageMatch
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $percentageMatch;
    /**
     * The alertIdentifier
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $alertIdentifier;
    /**
     * The matchIdentifier
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $matchIdentifier;
    /**
     * The relatedName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $relatedName;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for WatchlistCheck
     *
     * @param string $matchCategory
     * @param string $checkName
     * @param string $percentageMatch
     * @param string $alertIdentifier
     * @param string $matchIdentifier
     * @param string $relatedName
     * @param DOMDocument|string|null $any
     * @uses WatchlistCheck::setMatchCategory()
     * @uses WatchlistCheck::setCheckName()
     * @uses WatchlistCheck::setPercentageMatch()
     * @uses WatchlistCheck::setAlertIdentifier()
     * @uses WatchlistCheck::setMatchIdentifier()
     * @uses WatchlistCheck::setRelatedName()
     * @uses WatchlistCheck::setAny()
     */
    public function __construct(
        string $matchCategory,
        string $checkName,
        string $percentageMatch,
        string $alertIdentifier,
        string $matchIdentifier,
        string $relatedName,
        $any = null
    ) {
        $this
            ->setMatchCategory($matchCategory)
            ->setCheckName($checkName)
            ->setPercentageMatch($percentageMatch)
            ->setAlertIdentifier($alertIdentifier)
            ->setMatchIdentifier($matchIdentifier)
            ->setRelatedName($relatedName)
            ->setAny($any);
    }

    /**
     * Get matchCategory value
     *
     * @return string
     */
    public function getMatchCategory(): string
    {
        return $this->matchCategory;
    }

    /**
     * Set matchCategory value
     *
     * @param string $matchCategory
     * @return WatchlistCheck
     */
    public function setMatchCategory(string $matchCategory): self
    {
        // validation for constraint: string
        if (!is_null($matchCategory) && !is_string($matchCategory)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($matchCategory, true),
                gettype($matchCategory)
            ), __LINE__);
        }
        $this->matchCategory = $matchCategory;

        return $this;
    }

    /**
     * Get checkName value
     *
     * @return string
     */
    public function getCheckName(): string
    {
        return $this->checkName;
    }

    /**
     * Set checkName value
     *
     * @param string $checkName
     * @return WatchlistCheck
     */
    public function setCheckName(string $checkName): self
    {
        // validation for constraint: string
        if (!is_null($checkName) && !is_string($checkName)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($checkName, true),
                gettype($checkName)
            ), __LINE__);
        }
        $this->checkName = $checkName;

        return $this;
    }

    /**
     * Get percentageMatch value
     *
     * @return string
     */
    public function getPercentageMatch(): string
    {
        return $this->percentageMatch;
    }

    /**
     * Set percentageMatch value
     *
     * @param string $percentageMatch
     * @return WatchlistCheck
     */
    public function setPercentageMatch(string $percentageMatch): self
    {
        // validation for constraint: string
        if (!is_null($percentageMatch) && !is_string($percentageMatch)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($percentageMatch, true),
                gettype($percentageMatch)
            ), __LINE__);
        }
        $this->percentageMatch = $percentageMatch;

        return $this;
    }

    /**
     * Get alertIdentifier value
     *
     * @return string
     */
    public function getAlertIdentifier(): string
    {
        return $this->alertIdentifier;
    }

    /**
     * Set alertIdentifier value
     *
     * @param string $alertIdentifier
     * @return WatchlistCheck
     */
    public function setAlertIdentifier(string $alertIdentifier): self
    {
        // validation for constraint: string
        if (!is_null($alertIdentifier) && !is_string($alertIdentifier)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($alertIdentifier, true),
                gettype($alertIdentifier)
            ), __LINE__);
        }
        $this->alertIdentifier = $alertIdentifier;

        return $this;
    }

    /**
     * Get matchIdentifier value
     *
     * @return string
     */
    public function getMatchIdentifier(): string
    {
        return $this->matchIdentifier;
    }

    /**
     * Set matchIdentifier value
     *
     * @param string $matchIdentifier
     * @return WatchlistCheck
     */
    public function setMatchIdentifier(string $matchIdentifier): self
    {
        // validation for constraint: string
        if (!is_null($matchIdentifier) && !is_string($matchIdentifier)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($matchIdentifier, true),
                gettype($matchIdentifier)
            ), __LINE__);
        }
        $this->matchIdentifier = $matchIdentifier;

        return $this;
    }

    /**
     * Get relatedName value
     *
     * @return string
     */
    public function getRelatedName(): string
    {
        return $this->relatedName;
    }

    /**
     * Set relatedName value
     *
     * @param string $relatedName
     * @return WatchlistCheck
     */
    public function setRelatedName(string $relatedName): self
    {
        // validation for constraint: string
        if (!is_null($relatedName) && !is_string($relatedName)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($relatedName, true),
                gettype($relatedName)
            ), __LINE__);
        }
        $this->relatedName = $relatedName;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return WatchlistCheck
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
