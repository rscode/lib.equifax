<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\ResidenceType;

/**
 * This class stands for ResidentialStructuredAddress StructType
 *
 * @subpackage Structs
 */
class ResidentialStructuredAddress extends StructuredAddress
{
    /**
     * The residenceType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $residenceType = null;

    public function __construct(
        ?string $country = null,
        ?string $county = null,
        ?string $district = null,
        ?string $name = null,
        ?string $number = null,
        ?string $poBox = null,
        ?string $postcode = null,
        ?string $postTown = null,
        ?string $street1 = null,
        ?string $street2 = null,
        ?string $subBuilding = null,
        ?string $residenceType = null
    ) {
        $this
            ->setResidenceType($residenceType);

        parent::__construct($country, $county, $district, $name, $number, $poBox, $postcode, $postTown, $street1, $street2, $subBuilding);
    }

    /**
     * Get residenceType value
     *
     * @return string|null
     */
    public function getResidenceType(): ?string
    {
        return $this->residenceType;
    }

    /**
     * Set residenceType value
     *
     * @param string $residenceType
     * @return ResidentialStructuredAddress
     * @throws InvalidArgumentException
     * @uses ResidenceType::getValidValues
     * @uses ResidenceType::valueIsValid
     */
    public function setResidenceType(?string $residenceType = null): self
    {
        // validation for constraint: enumeration
        if (!ResidenceType::valueIsValid($residenceType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\ResidenceType',
                is_array($residenceType) ? implode(', ', $residenceType) : var_export($residenceType, true),
                implode(', ', ResidenceType::getValidValues())
            ), __LINE__);
        }
        $this->residenceType = $residenceType;

        return $this;
    }
}
