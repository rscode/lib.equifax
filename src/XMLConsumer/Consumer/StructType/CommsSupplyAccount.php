<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for CommsSupplyAccount StructType
 *
 * @subpackage Structs
 */
class CommsSupplyAccount extends FinancialAgreement
{
}
