<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for XmasClub StructType
 *
 * @subpackage Structs
 */
class XmasClub extends FinancialAgreement
{
}
