<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for InputAddressRequest StructType
 * Meta information extracted from the WSDL
 * - documentation: Free format address as supplied on input or matched address identifier relating to the linked address found request.
 *
 * @subpackage Structs
 */
class InputAddressRequest extends RawDataRequest
{
}
