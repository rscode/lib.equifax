<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for TelephoneDataRequest StructType
 *
 * @subpackage Structs
 */
class TelephoneDataRequest extends RawDataRequest
{
}
