<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for UnpresentableCheque StructType
 *
 * @subpackage Structs
 */
class UnpresentableCheque extends FinancialAgreement
{
}
