<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for UnsecuredLoan StructType
 *
 * @subpackage Structs
 */
class UnsecuredLoan extends FinancialAgreement
{
}
