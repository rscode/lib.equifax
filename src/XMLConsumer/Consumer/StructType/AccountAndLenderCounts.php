<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AccountAndLenderCounts StructType
 *
 * @subpackage Structs
 */
class AccountAndLenderCounts extends AbstractStructBase
{
    /**
     * The insightAccountCounts
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var InsightAccountCounts
     */
    protected InsightAccountCounts $insightAccountCounts;
    /**
     * The lenderCounts
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var LenderCounts
     */
    protected LenderCounts $lenderCounts;
    /**
     * The latestDateAge
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $latestDateAge = null;
    /**
     * The oldestDateAge
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $oldestDateAge = null;


    public function __construct(
        InsightAccountCounts $insightAccountCounts,
        LenderCounts $lenderCounts,
        ?string $latestDateAge = null,
        ?string $oldestDateAge = null,
    ) {
        $this
            ->setInsightAccountCounts($insightAccountCounts)
            ->setLenderCounts($lenderCounts)
            ->setLatestDateAge($latestDateAge)
            ->setOldestDateAge($oldestDateAge);
    }

    /**
     * Get insightAccountCounts value
     *
     * @return InsightAccountCounts
     */
    public function getInsightAccountCounts(): InsightAccountCounts
    {
        return $this->insightAccountCounts;
    }

    /**
     * Set insightAccountCounts value
     *
     * @param InsightAccountCounts $insightAccountCounts
     * @return AccountAndLenderCounts
     */
    public function setInsightAccountCounts(InsightAccountCounts $insightAccountCounts): self
    {
        $this->insightAccountCounts = $insightAccountCounts;

        return $this;
    }

    /**
     * Get lenderCounts value
     *
     * @return LenderCounts
     */
    public function getLenderCounts(): LenderCounts
    {
        return $this->lenderCounts;
    }

    /**
     * Set lenderCounts value
     *
     * @param LenderCounts $lenderCounts
     * @return AccountAndLenderCounts
     */
    public function setLenderCounts(LenderCounts $lenderCounts): self
    {
        $this->lenderCounts = $lenderCounts;

        return $this;
    }

    /**
     * Get latestDateAge value
     *
     * @return string|null
     */
    public function getLatestDateAge(): ?string
    {
        return $this->latestDateAge;
    }

    /**
     * Set latestDateAge value
     *
     * @param string $latestDateAge
     * @return AccountAndLenderCounts
     */
    public function setLatestDateAge(?string $latestDateAge = null): self
    {
        // validation for constraint: string
        if (!is_null($latestDateAge) && !is_string($latestDateAge)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($latestDateAge, true),
                gettype($latestDateAge)
            ), __LINE__);
        }
        $this->latestDateAge = $latestDateAge;

        return $this;
    }

    /**
     * Get oldestDateAge value
     *
     * @return string|null
     */
    public function getOldestDateAge(): ?string
    {
        return $this->oldestDateAge;
    }

    /**
     * Set oldestDateAge value
     *
     * @param string $oldestDateAge
     * @return AccountAndLenderCounts
     */
    public function setOldestDateAge(?string $oldestDateAge = null): self
    {
        // validation for constraint: string
        if (!is_null($oldestDateAge) && !is_string($oldestDateAge)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($oldestDateAge, true),
                gettype($oldestDateAge)
            ), __LINE__);
        }
        $this->oldestDateAge = $oldestDateAge;

        return $this;
    }
}
