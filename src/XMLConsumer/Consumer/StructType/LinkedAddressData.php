<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\AddressMatchingStatus;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\LinkedAddressType;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for LinkedAddressData StructType
 *
 * @subpackage Structs
 */
class LinkedAddressData extends AbstractStructBase
{
    /**
     * The addressMatchStatus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $addressMatchStatus = null;
    /**
     * The addressSpecificData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AddressSpecificData|null
     */
    protected ?AddressSpecificData $addressSpecificData = null;
    /**
     * The index
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 99
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 0
     *
     * @var int|null
     */
    protected ?int $index = null;
    /**
     * The linkedAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var MatchedConsumerAddress|null
     */
    protected ?MatchedConsumerAddress $linkedAddress = null;
    /**
     * The linkType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $linkType = null;
    /**
     * The noticeOfCorrectionOrDisputePresent
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var bool|null
     */
    protected ?bool $noticeOfCorrectionOrDisputePresent = null;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for LinkedAddressData
     *
     * @param string                  $addressMatchStatus
     * @param AddressSpecificData     $addressSpecificData
     * @param int                     $index
     * @param MatchedConsumerAddress  $linkedAddress
     * @param string                  $linkType
     * @param bool                    $noticeOfCorrectionOrDisputePresent
     * @param DOMDocument|string|null $any
     * @uses LinkedAddressData::setAddressMatchStatus()
     * @uses LinkedAddressData::setAddressSpecificData()
     * @uses LinkedAddressData::setIndex()
     * @uses LinkedAddressData::setLinkedAddress()
     * @uses LinkedAddressData::setLinkType()
     * @uses LinkedAddressData::setNoticeOfCorrectionOrDisputePresent()
     * @uses LinkedAddressData::setAny()
     */
    public function __construct(
        ?string $addressMatchStatus = null,
        ?AddressSpecificData $addressSpecificData = null,
        ?int $index = null,
        ?MatchedConsumerAddress $linkedAddress = null,
        ?string $linkType = null,
        ?bool $noticeOfCorrectionOrDisputePresent = null,
        $any = null
    ) {
        $this
            ->setAddressMatchStatus($addressMatchStatus)
            ->setAddressSpecificData($addressSpecificData)
            ->setIndex($index)
            ->setLinkedAddress($linkedAddress)
            ->setLinkType($linkType)
            ->setNoticeOfCorrectionOrDisputePresent($noticeOfCorrectionOrDisputePresent)
            ->setAny($any);
    }

    /**
     * Get addressMatchStatus value
     *
     * @return string|null
     */
    public function getAddressMatchStatus(): ?string
    {
        return $this->addressMatchStatus;
    }

    /**
     * Set addressMatchStatus value
     *
     * @param string $addressMatchStatus
     * @return LinkedAddressData
     * @throws InvalidArgumentException
     * @uses AddressMatchingStatus::getValidValues
     * @uses AddressMatchingStatus::valueIsValid
     */
    public function setAddressMatchStatus(?string $addressMatchStatus = null): self
    {
        // validation for constraint: enumeration
        if (!AddressMatchingStatus::valueIsValid($addressMatchStatus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\AddressMatchingStatus',
                is_array($addressMatchStatus) ? implode(', ', $addressMatchStatus) : var_export($addressMatchStatus, true),
                implode(', ', AddressMatchingStatus::getValidValues())
            ), __LINE__);
        }
        $this->addressMatchStatus = $addressMatchStatus;

        return $this;
    }

    /**
     * Get addressSpecificData value
     *
     * @return AddressSpecificData|null
     */
    public function getAddressSpecificData(): ?AddressSpecificData
    {
        return $this->addressSpecificData;
    }

    /**
     * Set addressSpecificData value
     *
     * @param AddressSpecificData $addressSpecificData
     * @return LinkedAddressData
     */
    public function setAddressSpecificData(?AddressSpecificData $addressSpecificData = null): self
    {
        $this->addressSpecificData = $addressSpecificData;

        return $this;
    }

    /**
     * Get index value
     *
     * @return int|null
     */
    public function getIndex(): ?int
    {
        return $this->index;
    }

    /**
     * Set index value
     *
     * @param int $index
     * @return LinkedAddressData
     */
    public function setIndex(?int $index = null): self
    {
        // validation for constraint: int
        if (!is_null($index) && !(is_int($index) || ctype_digit($index))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($index, true),
                gettype($index)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(99)
        if (!is_null($index) && $index > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 99',
                var_export($index, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($index) && $index < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($index, true)
            ), __LINE__);
        }
        $this->index = $index;

        return $this;
    }

    /**
     * Get linkedAddress value
     *
     * @return MatchedConsumerAddress|null
     */
    public function getLinkedAddress(): ?MatchedConsumerAddress
    {
        return $this->linkedAddress;
    }

    /**
     * Set linkedAddress value
     *
     * @param MatchedConsumerAddress $linkedAddress
     * @return LinkedAddressData
     */
    public function setLinkedAddress(?MatchedConsumerAddress $linkedAddress = null): self
    {
        $this->linkedAddress = $linkedAddress;

        return $this;
    }

    /**
     * Get linkType value
     *
     * @return string|null
     */
    public function getLinkType(): ?string
    {
        return $this->linkType;
    }

    /**
     * Set linkType value
     *
     * @param string $linkType
     * @return LinkedAddressData
     * @throws InvalidArgumentException
     * @uses LinkedAddressType::getValidValues
     * @uses LinkedAddressType::valueIsValid
     */
    public function setLinkType(?string $linkType = null): self
    {
        // validation for constraint: enumeration
        if (!LinkedAddressType::valueIsValid($linkType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\LinkedAddressType',
                is_array($linkType) ? implode(', ', $linkType) : var_export($linkType, true),
                implode(', ', LinkedAddressType::getValidValues())
            ), __LINE__);
        }
        $this->linkType = $linkType;

        return $this;
    }

    /**
     * Get noticeOfCorrectionOrDisputePresent value
     *
     * @return bool|null
     */
    public function getNoticeOfCorrectionOrDisputePresent(): ?bool
    {
        return $this->noticeOfCorrectionOrDisputePresent;
    }

    /**
     * Set noticeOfCorrectionOrDisputePresent value
     *
     * @param bool $noticeOfCorrectionOrDisputePresent
     * @return LinkedAddressData
     */
    public function setNoticeOfCorrectionOrDisputePresent(?bool $noticeOfCorrectionOrDisputePresent = null): self
    {
        // validation for constraint: boolean
        if (!is_null($noticeOfCorrectionOrDisputePresent) && !is_bool($noticeOfCorrectionOrDisputePresent)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($noticeOfCorrectionOrDisputePresent, true),
                gettype($noticeOfCorrectionOrDisputePresent)
            ), __LINE__);
        }
        $this->noticeOfCorrectionOrDisputePresent = $noticeOfCorrectionOrDisputePresent;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return LinkedAddressData
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
