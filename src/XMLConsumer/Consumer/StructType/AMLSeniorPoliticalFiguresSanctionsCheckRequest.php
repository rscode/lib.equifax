<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for AMLSeniorPoliticalFiguresSanctionsCheckRequest StructType
 *
 * @subpackage Structs
 */
class AMLSeniorPoliticalFiguresSanctionsCheckRequest extends CodedDataRequest
{
}
