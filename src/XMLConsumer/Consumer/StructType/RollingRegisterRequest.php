<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for RollingRegisterRequest StructType
 *
 * @subpackage Structs
 */
class RollingRegisterRequest extends RawDataAvailableAtLinkedAddressRequest
{
}
