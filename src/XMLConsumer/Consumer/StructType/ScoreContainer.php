<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for ScoreContainer StructType
 *
 * @subpackage Structs
 */
class ScoreContainer extends DataItemContainer
{
    /**
     * The score
     * Meta information extracted from the WSDL
     * - maxOccurs: 9
     * - minOccurs: 0
     *
     * @var Score[]
     */
    protected ?array $score = null;

    /**
     * Constructor method for ScoreContainer
     *
     * @param Score[] $score
     * @uses ScoreContainer::setScore()
     */
    public function __construct(?array $score = null)
    {
        $this
            ->setScore($score);
    }

    /**
     * This method is responsible for validating the values passed to the setScore method
     * This method is willingly generated in order to preserve the one-line inline validation within the setScore method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateScoreForArrayConstraintsFromSetScore(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $scoreContainerScoreItem) {
            // validation for constraint: itemType
            if (!$scoreContainerScoreItem instanceof Score) {
                $invalidValues[] = is_object($scoreContainerScoreItem) ? get_class($scoreContainerScoreItem) : sprintf(
                    '%s(%s)',
                    gettype($scoreContainerScoreItem),
                    var_export($scoreContainerScoreItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The score property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\Score, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get score value
     *
     * @return Score[]
     */
    public function getScore(): ?array
    {
        return $this->score;
    }

    /**
     * Set score value
     *
     * @param Score[] $score
     * @return ScoreContainer
     * @throws InvalidArgumentException
     */
    public function setScore(?array $score = null): self
    {
        // validation for constraint: array
        if ('' !== ($scoreArrayErrorMessage = self::validateScoreForArrayConstraintsFromSetScore($score))) {
            throw new InvalidArgumentException($scoreArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(9)
        if (is_array($score) && count($score) > 9) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 9',
                count($score)
            ), __LINE__);
        }
        $this->score = $score;

        return $this;
    }

    /**
     * Add item to score value
     *
     * @param Score $item
     * @return ScoreContainer
     * @throws InvalidArgumentException
     */
    public function addToScore(Score $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof Score) {
            throw new InvalidArgumentException(sprintf(
                'The score property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\Score, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(9)
        if (is_array($this->score) && count($this->score) >= 9) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 9',
                count($this->score)
            ), __LINE__);
        }
        $this->score[] = $item;

        return $this;
    }
}
