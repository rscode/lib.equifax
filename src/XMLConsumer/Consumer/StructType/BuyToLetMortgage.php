<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for BuyToLetMortgage StructType
 *
 * @subpackage Structs
 */
class BuyToLetMortgage extends FinancialAgreement
{
}
