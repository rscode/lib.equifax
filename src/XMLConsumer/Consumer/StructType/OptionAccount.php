<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for OptionAccount StructType
 *
 * @subpackage Structs
 */
class OptionAccount extends FinancialAgreement
{
}
