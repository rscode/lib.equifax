<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\PaymentStatus;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AgreementPreviousState StructType
 *
 * @subpackage Structs
 */
class AgreementPreviousState extends AbstractStructBase
{
    /**
     * The ageInMonths
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 99
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $ageInMonths;
    /**
     * The accountBalance
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AccountBalance|null
     */
    protected ?AccountBalance $accountBalance = null;
    /**
     * The creditLimit
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var CreditLimit|null
     */
    protected ?CreditLimit $creditLimit = null;
    /**
     * The paymentStatus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $paymentStatus = null;
    /**
     * The statement
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var FinancialAgreementStatement|null
     */
    protected ?FinancialAgreementStatement $statement = null;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for AgreementPreviousState
     *
     * @param int                         $ageInMonths
     * @param AccountBalance              $accountBalance
     * @param CreditLimit                 $creditLimit
     * @param string                      $paymentStatus
     * @param FinancialAgreementStatement $statement
     * @param DOMDocument|string|null     $any
     * @uses AgreementPreviousState::setAgeInMonths()
     * @uses AgreementPreviousState::setAccountBalance()
     * @uses AgreementPreviousState::setCreditLimit()
     * @uses AgreementPreviousState::setPaymentStatus()
     * @uses AgreementPreviousState::setStatement()
     * @uses AgreementPreviousState::setAny()
     */
    public function __construct(
        int $ageInMonths,
        ?AccountBalance $accountBalance = null,
        ?CreditLimit $creditLimit = null,
        ?string $paymentStatus = null,
        ?FinancialAgreementStatement $statement = null,
        $any = null
    ) {
        $this
            ->setAgeInMonths($ageInMonths)
            ->setAccountBalance($accountBalance)
            ->setCreditLimit($creditLimit)
            ->setPaymentStatus($paymentStatus)
            ->setStatement($statement)
            ->setAny($any);
    }

    /**
     * Get ageInMonths value
     *
     * @return int
     */
    public function getAgeInMonths(): int
    {
        return $this->ageInMonths;
    }

    /**
     * Set ageInMonths value
     *
     * @param int $ageInMonths
     * @return AgreementPreviousState
     */
    public function setAgeInMonths(int $ageInMonths): self
    {
        // validation for constraint: int
        if (!is_null($ageInMonths) && !(is_int($ageInMonths) || ctype_digit($ageInMonths))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($ageInMonths, true),
                gettype($ageInMonths)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(99)
        if (!is_null($ageInMonths) && $ageInMonths > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 99',
                var_export($ageInMonths, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($ageInMonths) && $ageInMonths < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($ageInMonths, true)
            ), __LINE__);
        }
        $this->ageInMonths = $ageInMonths;

        return $this;
    }

    /**
     * Get accountBalance value
     *
     * @return AccountBalance|null
     */
    public function getAccountBalance(): ?AccountBalance
    {
        return $this->accountBalance;
    }

    /**
     * Set accountBalance value
     *
     * @param AccountBalance $accountBalance
     * @return AgreementPreviousState
     */
    public function setAccountBalance(?AccountBalance $accountBalance = null): self
    {
        $this->accountBalance = $accountBalance;

        return $this;
    }

    /**
     * Get creditLimit value
     *
     * @return CreditLimit|null
     */
    public function getCreditLimit(): ?CreditLimit
    {
        return $this->creditLimit;
    }

    /**
     * Set creditLimit value
     *
     * @param CreditLimit $creditLimit
     * @return AgreementPreviousState
     */
    public function setCreditLimit(?CreditLimit $creditLimit = null): self
    {
        $this->creditLimit = $creditLimit;

        return $this;
    }

    /**
     * Get paymentStatus value
     *
     * @return string|null
     */
    public function getPaymentStatus(): ?string
    {
        return $this->paymentStatus;
    }

    /**
     * Set paymentStatus value
     *
     * @param string $paymentStatus
     * @return AgreementPreviousState
     * @throws InvalidArgumentException
     * @uses PaymentStatus::getValidValues
     * @uses PaymentStatus::valueIsValid
     */
    public function setPaymentStatus(?string $paymentStatus = null): self
    {
        // validation for constraint: enumeration
        if (!PaymentStatus::valueIsValid($paymentStatus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\PaymentStatus',
                is_array($paymentStatus) ? implode(', ', $paymentStatus) : var_export($paymentStatus, true),
                implode(', ', PaymentStatus::getValidValues())
            ), __LINE__);
        }
        $this->paymentStatus = $paymentStatus;

        return $this;
    }

    /**
     * Get statement value
     *
     * @return FinancialAgreementStatement|null
     */
    public function getStatement(): ?FinancialAgreementStatement
    {
        return $this->statement;
    }

    /**
     * Set statement value
     *
     * @param FinancialAgreementStatement $statement
     * @return AgreementPreviousState
     */
    public function setStatement(?FinancialAgreementStatement $statement = null): self
    {
        $this->statement = $statement;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return AgreementPreviousState
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
