<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OtherCountry StructType
 *
 * @subpackage Structs
 */
class OtherCountry extends AbstractStructBase
{
    /**
     * The countryType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $countryType = null;
    /**
     * The country
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     *
     * @var string[]
     */
    protected ?array $country = null;

    /**
     * Constructor method for OtherCountry
     *
     * @param string   $countryType
     * @param string[] $country
     * @uses OtherCountry::setCountryType()
     * @uses OtherCountry::setCountry()
     */
    public function __construct(?string $countryType = null, ?array $country = null)
    {
        $this
            ->setCountryType($countryType)
            ->setCountry($country);
    }

    /**
     * This method is responsible for validating the values passed to the setCountry method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCountry method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCountryForArrayConstraintsFromSetCountry(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $otherCountryCountryItem) {
            // validation for constraint: itemType
            if (!is_string($otherCountryCountryItem)) {
                $invalidValues[] = is_object($otherCountryCountryItem) ? get_class($otherCountryCountryItem) : sprintf(
                    '%s(%s)',
                    gettype($otherCountryCountryItem),
                    var_export($otherCountryCountryItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The country property can only contain items of type string, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get countryType value
     *
     * @return string|null
     */
    public function getCountryType(): ?string
    {
        return $this->countryType;
    }

    /**
     * Set countryType value
     *
     * @param string $countryType
     * @return OtherCountry
     */
    public function setCountryType(?string $countryType = null): self
    {
        // validation for constraint: string
        if (!is_null($countryType) && !is_string($countryType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($countryType, true),
                gettype($countryType)
            ), __LINE__);
        }
        $this->countryType = $countryType;

        return $this;
    }

    /**
     * Get country value
     *
     * @return string[]
     */
    public function getCountry(): ?array
    {
        return $this->country;
    }

    /**
     * Set country value
     *
     * @param string[] $country
     * @return OtherCountry
     * @throws InvalidArgumentException
     */
    public function setCountry(?array $country = null): self
    {
        // validation for constraint: array
        if ('' !== ($countryArrayErrorMessage = self::validateCountryForArrayConstraintsFromSetCountry($country))) {
            throw new InvalidArgumentException($countryArrayErrorMessage, __LINE__);
        }
        $this->country = $country;

        return $this;
    }

    /**
     * Add item to country value
     *
     * @param string $item
     * @return OtherCountry
     * @throws InvalidArgumentException
     */
    public function addToCountry(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf(
                'The country property can only contain items of type string, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        $this->country[] = $item;

        return $this;
    }
}
