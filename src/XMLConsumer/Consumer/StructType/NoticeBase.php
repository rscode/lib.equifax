<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for NoticeBase StructType
 *
 * @subpackage Structs
 */
abstract class NoticeBase extends NameMatchedData
{
    /**
     * The text
     * Meta information extracted from the WSDL
     * - documentation: Concatenation of Text field from all 'continued' NCR records.
     * - base: xs:string
     * - maxLength: 40000
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $text;
    /**
     * The typeOfData
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - length: 1
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $typeOfData;
    /**
     * The dateNoticeCreated
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $dateNoticeCreated = null;

    /**
     * Constructor method for NoticeBase
     *
     * @param string $text
     * @param string $typeOfData
     * @param string $dateNoticeCreated
     * @uses NoticeBase::setText()
     * @uses NoticeBase::setTypeOfData()
     * @uses NoticeBase::setDateNoticeCreated()
     */
    public function __construct(string $text, string $typeOfData, ?string $dateNoticeCreated = null)
    {
        $this
            ->setText($text)
            ->setTypeOfData($typeOfData)
            ->setDateNoticeCreated($dateNoticeCreated);
    }

    /**
     * Get text value
     *
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * Set text value
     *
     * @param string $text
     * @return NoticeBase
     */
    public function setText(string $text): self
    {
        // validation for constraint: string
        if (!is_null($text) && !is_string($text)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($text, true),
                gettype($text)
            ), __LINE__);
        }
        // validation for constraint: maxLength(40000)
        if (!is_null($text) && mb_strlen((string)$text) > 40000) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 40000',
                mb_strlen((string)$text)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($text) && mb_strlen((string)$text) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$text)
            ), __LINE__);
        }
        $this->text = $text;

        return $this;
    }

    /**
     * Get typeOfData value
     *
     * @return string
     */
    public function getTypeOfData(): string
    {
        return $this->typeOfData;
    }

    /**
     * Set typeOfData value
     *
     * @param string $typeOfData
     * @return NoticeBase
     */
    public function setTypeOfData(string $typeOfData): self
    {
        // validation for constraint: string
        if (!is_null($typeOfData) && !is_string($typeOfData)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($typeOfData, true),
                gettype($typeOfData)
            ), __LINE__);
        }
        // validation for constraint: length(1)
        if (!is_null($typeOfData) && mb_strlen((string)$typeOfData) !== 1) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be equal to 1',
                mb_strlen((string)$typeOfData)
            ), __LINE__);
        }
        $this->typeOfData = $typeOfData;

        return $this;
    }

    /**
     * Get dateNoticeCreated value
     *
     * @return string|null
     */
    public function getDateNoticeCreated(): ?string
    {
        return $this->dateNoticeCreated;
    }

    /**
     * Set dateNoticeCreated value
     *
     * @param string $dateNoticeCreated
     * @return NoticeBase
     */
    public function setDateNoticeCreated(?string $dateNoticeCreated = null): self
    {
        // validation for constraint: string
        if (!is_null($dateNoticeCreated) && !is_string($dateNoticeCreated)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($dateNoticeCreated, true),
                gettype($dateNoticeCreated)
            ), __LINE__);
        }
        $this->dateNoticeCreated = $dateNoticeCreated;

        return $this;
    }
}
