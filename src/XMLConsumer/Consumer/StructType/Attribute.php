<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Attribute StructType
 *
 * @subpackage Structs
 */
class Attribute extends AbstractStructBase
{
    /**
     * The identifier
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $identifier;
    /**
     * The value
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $value;
    /**
     * The description
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $description = null;
    /**
     * The personLevel
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $personLevel = null;
    /**
     * The addressLevel
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $addressLevel = null;
    /**
     * The numericValue
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var int|null
     */
    protected ?int $numericValue = null;
    /**
     * The reason
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $reason = null;
    /**
     * The source
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $source = null;


    public function __construct(
        string $identifier,
        string $value,
        ?string $description = null,
        ?string $personLevel = null,
        ?string $addressLevel = null,
        ?int $numericValue = null,
        ?string $reason = null,
        ?string $source = null,
        $any = null
    ) {
        $this
            ->setIdentifier($identifier)
            ->setValue($value)
            ->setDescription($description)
            ->setPersonLevel($personLevel)
            ->setAddressLevel($addressLevel)
            ->setNumericValue($numericValue)
            ->setReason($reason)
            ->setSource($source)
            ->setAny($any);
    }

    /**
     * Get identifier value
     *
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * Set identifier value
     *
     * @param string $identifier
     * @return Attribute
     */
    public function setIdentifier(string $identifier): self
    {
        // validation for constraint: string
        if (!is_null($identifier) && !is_string($identifier)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($identifier, true),
                gettype($identifier)
            ), __LINE__);
        }
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get value value
     *
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * Set value value
     *
     * @param string $value
     * @return Attribute
     */
    public function setValue(string $value): self
    {
        // validation for constraint: string
        if (!is_null($value) && !is_string($value)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($value, true),
                gettype($value)
            ), __LINE__);
        }
        $this->value = $value;

        return $this;
    }

    /**
     * Get description value
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set description value
     *
     * @param string $description
     * @return Attribute
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($description, true),
                gettype($description)
            ), __LINE__);
        }
        $this->description = $description;

        return $this;
    }

    /**
     * Get personLevel value
     *
     * @return string|null
     */
    public function getPersonLevel(): ?string
    {
        return $this->personLevel;
    }

    /**
     * Set personLevel value
     *
     * @param string $personLevel
     * @return Attribute
     */
    public function setPersonLevel(?string $personLevel = null): self
    {
        // validation for constraint: string
        if (!is_null($personLevel) && !is_string($personLevel)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($personLevel, true),
                gettype($personLevel)
            ), __LINE__);
        }
        $this->personLevel = $personLevel;

        return $this;
    }

    /**
     * Get addressLevel value
     *
     * @return string|null
     */
    public function getAddressLevel(): ?string
    {
        return $this->addressLevel;
    }

    /**
     * Set addressLevel value
     *
     * @param string $addressLevel
     * @return Attribute
     */
    public function setAddressLevel(?string $addressLevel = null): self
    {
        // validation for constraint: string
        if (!is_null($addressLevel) && !is_string($addressLevel)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($addressLevel, true),
                gettype($addressLevel)
            ), __LINE__);
        }
        $this->addressLevel = $addressLevel;

        return $this;
    }

    /**
     * Get numericValue value
     *
     * @return int|null
     */
    public function getNumericValue(): ?int
    {
        return $this->numericValue;
    }

    /**
     * Set numericValue value
     *
     * @param int $numericValue
     * @return Attribute
     */
    public function setNumericValue(?int $numericValue = null): self
    {
        // validation for constraint: int
        if (!is_null($numericValue) && !(is_int($numericValue) || ctype_digit($numericValue))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($numericValue, true),
                gettype($numericValue)
            ), __LINE__);
        }
        $this->numericValue = $numericValue;

        return $this;
    }

    /**
     * Get reason value
     *
     * @return string|null
     */
    public function getReason(): ?string
    {
        return $this->reason;
    }

    /**
     * Set reason value
     *
     * @param string $reason
     * @return Attribute
     */
    public function setReason(?string $reason = null): self
    {
        // validation for constraint: string
        if (!is_null($reason) && !is_string($reason)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($reason, true),
                gettype($reason)
            ), __LINE__);
        }
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get source value
     *
     * @return string|null
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * Set source value
     *
     * @param string $source
     * @return Attribute
     */
    public function setSource(?string $source = null): self
    {
        // validation for constraint: string
        if (!is_null($source) && !is_string($source)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($source, true),
                gettype($source)
            ), __LINE__);
        }
        $this->source = $source;

        return $this;
    }
}
