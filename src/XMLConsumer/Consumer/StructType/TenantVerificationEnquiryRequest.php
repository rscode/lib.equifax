<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for TenantVerificationEnquiryRequest StructType
 * Meta information extracted from the WSDL
 * - documentation: Tenant verification
 *
 * @subpackage Structs
 */
class TenantVerificationEnquiryRequest extends CommonRequest
{
}
