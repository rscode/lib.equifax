<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for AMLHMTreasurySanctionsCheckRequest StructType
 *
 * @subpackage Structs
 */
class AMLHMTreasurySanctionsCheckRequest extends CodedDataRequest
{
}
