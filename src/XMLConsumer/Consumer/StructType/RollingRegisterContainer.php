<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for RollingRegisterContainer StructType
 *
 * @subpackage Structs
 */
class RollingRegisterContainer extends DataItemContainer
{
    /**
     * The rollingRegister
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var RollingRegister[]
     */
    protected ?array $rollingRegister = null;

    /**
     * Constructor method for RollingRegisterContainer
     *
     * @param RollingRegister[] $rollingRegister
     * @uses RollingRegisterContainer::setRollingRegister()
     */
    public function __construct(?array $rollingRegister = null)
    {
        $this
            ->setRollingRegister($rollingRegister);
    }

    /**
     * This method is responsible for validating the values passed to the setRollingRegister method
     * This method is willingly generated in order to preserve the one-line inline validation within the setRollingRegister method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateRollingRegisterForArrayConstraintsFromSetRollingRegister(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $rollingRegisterContainerRollingRegisterItem) {
            // validation for constraint: itemType
            if (!$rollingRegisterContainerRollingRegisterItem instanceof RollingRegister) {
                $invalidValues[] = is_object($rollingRegisterContainerRollingRegisterItem) ? get_class($rollingRegisterContainerRollingRegisterItem) : sprintf(
                    '%s(%s)',
                    gettype($rollingRegisterContainerRollingRegisterItem),
                    var_export($rollingRegisterContainerRollingRegisterItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The rollingRegister property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\RollingRegister, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get rollingRegister value
     *
     * @return RollingRegister[]
     */
    public function getRollingRegister(): ?array
    {
        return $this->rollingRegister;
    }

    /**
     * Set rollingRegister value
     *
     * @param RollingRegister[] $rollingRegister
     * @return RollingRegisterContainer
     * @throws InvalidArgumentException
     */
    public function setRollingRegister(?array $rollingRegister = null): self
    {
        // validation for constraint: array
        if ('' !== ($rollingRegisterArrayErrorMessage = self::validateRollingRegisterForArrayConstraintsFromSetRollingRegister($rollingRegister))) {
            throw new InvalidArgumentException($rollingRegisterArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($rollingRegister) && count($rollingRegister) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($rollingRegister)
            ), __LINE__);
        }
        $this->rollingRegister = $rollingRegister;

        return $this;
    }

    /**
     * Add item to rollingRegister value
     *
     * @param RollingRegister $item
     * @return RollingRegisterContainer
     * @throws InvalidArgumentException
     */
    public function addToRollingRegister(RollingRegister $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof RollingRegister) {
            throw new InvalidArgumentException(sprintf(
                'The rollingRegister property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\RollingRegister, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->rollingRegister) && count($this->rollingRegister) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->rollingRegister)
            ), __LINE__);
        }
        $this->rollingRegister[] = $item;

        return $this;
    }
}
