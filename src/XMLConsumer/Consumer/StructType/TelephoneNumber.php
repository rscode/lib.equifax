<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\TelephoneNumberType;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\TelephoneNumberUsageType;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TelephoneNumber StructType
 *
 * @subpackage Structs
 */
class TelephoneNumber extends AbstractStructBase
{
    /**
     * The number
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $number;
    /**
     * The prefix
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $prefix;
    /**
     * The directoryListed
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var bool|null
     */
    protected ?bool $directoryListed = null;
    /**
     * The serviceProvider
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $serviceProvider = null;
    /**
     * The type
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * The usageType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $usageType = null;

    /**
     * Constructor method for TelephoneNumber
     *
     * @param string $number
     * @param string $prefix
     * @param bool   $directoryListed
     * @param string $serviceProvider
     * @param string $type
     * @param string $usageType
     * @uses TelephoneNumber::setNumber()
     * @uses TelephoneNumber::setPrefix()
     * @uses TelephoneNumber::setDirectoryListed()
     * @uses TelephoneNumber::setServiceProvider()
     * @uses TelephoneNumber::setType()
     * @uses TelephoneNumber::setUsageType()
     */
    public function __construct(
        string $number,
        string $prefix,
        ?bool $directoryListed = null,
        ?string $serviceProvider = null,
        ?string $type = null,
        ?string $usageType = null
    ) {
        $this
            ->setNumber($number)
            ->setPrefix($prefix)
            ->setDirectoryListed($directoryListed)
            ->setServiceProvider($serviceProvider)
            ->setType($type)
            ->setUsageType($usageType);
    }

    /**
     * Get number value
     *
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * Set number value
     *
     * @param string $number
     * @return TelephoneNumber
     */
    public function setNumber(string $number): self
    {
        // validation for constraint: string
        if (!is_null($number) && !is_string($number)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($number, true),
                gettype($number)
            ), __LINE__);
        }
        $this->number = $number;

        return $this;
    }

    /**
     * Get prefix value
     *
     * @return string
     */
    public function getPrefix(): string
    {
        return $this->prefix;
    }

    /**
     * Set prefix value
     *
     * @param string $prefix
     * @return TelephoneNumber
     */
    public function setPrefix(string $prefix): self
    {
        // validation for constraint: string
        if (!is_null($prefix) && !is_string($prefix)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($prefix, true),
                gettype($prefix)
            ), __LINE__);
        }
        $this->prefix = $prefix;

        return $this;
    }

    /**
     * Get directoryListed value
     *
     * @return bool|null
     */
    public function getDirectoryListed(): ?bool
    {
        return $this->directoryListed;
    }

    /**
     * Set directoryListed value
     *
     * @param bool $directoryListed
     * @return TelephoneNumber
     */
    public function setDirectoryListed(?bool $directoryListed = null): self
    {
        // validation for constraint: boolean
        if (!is_null($directoryListed) && !is_bool($directoryListed)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($directoryListed, true),
                gettype($directoryListed)
            ), __LINE__);
        }
        $this->directoryListed = $directoryListed;

        return $this;
    }

    /**
     * Get serviceProvider value
     *
     * @return string|null
     */
    public function getServiceProvider(): ?string
    {
        return $this->serviceProvider;
    }

    /**
     * Set serviceProvider value
     *
     * @param string $serviceProvider
     * @return TelephoneNumber
     */
    public function setServiceProvider(?string $serviceProvider = null): self
    {
        // validation for constraint: string
        if (!is_null($serviceProvider) && !is_string($serviceProvider)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($serviceProvider, true),
                gettype($serviceProvider)
            ), __LINE__);
        }
        $this->serviceProvider = $serviceProvider;

        return $this;
    }

    /**
     * Get type value
     *
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * Set type value
     *
     * @param string $type
     * @return TelephoneNumber
     * @throws InvalidArgumentException
     * @uses TelephoneNumberType::getValidValues
     * @uses TelephoneNumberType::valueIsValid
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: enumeration
        if (!TelephoneNumberType::valueIsValid($type)) {
            throw new InvalidArgumentException(
                sprintf(
                    'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\TelephoneNumberType',
                    is_array($type) ? implode(', ', $type) : var_export($type, true),
                    implode(', ', TelephoneNumberType::getValidValues())
                ),
                __LINE__
            );
        }
        $this->type = $type;

        return $this;
    }

    /**
     * Get usageType value
     *
     * @return string|null
     */
    public function getUsageType(): ?string
    {
        return $this->usageType;
    }

    /**
     * Set usageType value
     *
     * @param string $usageType
     * @return TelephoneNumber
     * @throws InvalidArgumentException
     * @uses TelephoneNumberUsageType::getValidValues
     * @uses TelephoneNumberUsageType::valueIsValid
     */
    public function setUsageType(?string $usageType = null): self
    {
        // validation for constraint: enumeration
        if (!TelephoneNumberUsageType::valueIsValid($usageType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\TelephoneNumberUsageType',
                is_array($usageType) ? implode(', ', $usageType) : var_export($usageType, true),
                implode(', ', TelephoneNumberUsageType::getValidValues())
            ), __LINE__);
        }
        $this->usageType = $usageType;

        return $this;
    }
}
