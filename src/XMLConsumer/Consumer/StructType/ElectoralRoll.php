<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * List of adults over 18, or attainers (those who will become 18 during the electoral year) that are eligible to vote in UK local and
 * general elections.
 *
 * @see https://developer.equifax.co.uk/sites/uk/files/xml_ref_guide/ref/Data%20Groups/Electoral%20Roll.pdf
 */
class ElectoralRoll extends ElectoralRollBase
{
    /**
     * The annualRegisterPeriod
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var DatePeriod
     */
    protected DatePeriod $annualRegisterPeriod;

    /**
     * Constructor method for ElectoralRoll
     *
     * @param DatePeriod              $annualRegisterPeriod
     * @param DOMDocument|string|null $any
     * @uses ElectoralRoll::setAnnualRegisterPeriod()
     * @uses ElectoralRoll::setAny()
     */
    public function __construct(DatePeriod $annualRegisterPeriod, $any = null)
    {
        $this
            ->setAnnualRegisterPeriod($annualRegisterPeriod)
            ->setAny($any);
    }

    /**
     * Get annualRegisterPeriod value
     *
     * @return DatePeriod
     */
    public function getAnnualRegisterPeriod(): DatePeriod
    {
        return $this->annualRegisterPeriod;
    }

    /**
     * Set annualRegisterPeriod value
     *
     * @param DatePeriod $annualRegisterPeriod
     * @return ElectoralRoll
     */
    public function setAnnualRegisterPeriod(DatePeriod $annualRegisterPeriod): self
    {
        $this->annualRegisterPeriod = $annualRegisterPeriod;

        return $this;
    }
}
