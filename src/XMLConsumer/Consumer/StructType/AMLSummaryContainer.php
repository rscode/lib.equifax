<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for AMLSummaryContainer StructType
 *
 * @subpackage Structs
 */
class AMLSummaryContainer extends DataItemContainer
{
    /**
     * The amlSummary
     * Meta information extracted from the WSDL
     * - maxOccurs: 2
     * - minOccurs: 0
     *
     * @var AMLSummary[]
     */
    protected ?array $amlSummary = null;

    /**
     * Constructor method for AMLSummaryContainer
     *
     * @param AMLSummary[] $amlSummary
     * @uses AMLSummaryContainer::setAmlSummary()
     */
    public function __construct(?array $amlSummary = null)
    {
        $this
            ->setAmlSummary($amlSummary);
    }

    /**
     * This method is responsible for validating the values passed to the setAmlSummary method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAmlSummary method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAmlSummaryForArrayConstraintsFromSetAmlSummary(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $aMLSummaryContainerAmlSummaryItem) {
            // validation for constraint: itemType
            if (!$aMLSummaryContainerAmlSummaryItem instanceof AMLSummary) {
                $invalidValues[] = is_object($aMLSummaryContainerAmlSummaryItem) ? get_class($aMLSummaryContainerAmlSummaryItem) : sprintf(
                    '%s(%s)',
                    gettype($aMLSummaryContainerAmlSummaryItem),
                    var_export($aMLSummaryContainerAmlSummaryItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The amlSummary property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AMLSummary, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get amlSummary value
     *
     * @return AMLSummary[]
     */
    public function getAmlSummary(): ?array
    {
        return $this->amlSummary;
    }

    /**
     * Set amlSummary value
     *
     * @param AMLSummary[] $amlSummary
     * @return AMLSummaryContainer
     * @throws InvalidArgumentException
     */
    public function setAmlSummary(?array $amlSummary = null): self
    {
        // validation for constraint: array
        if ('' !== ($amlSummaryArrayErrorMessage = self::validateAmlSummaryForArrayConstraintsFromSetAmlSummary($amlSummary))) {
            throw new InvalidArgumentException($amlSummaryArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(2)
        if (is_array($amlSummary) && count($amlSummary) > 2) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 2',
                count($amlSummary)
            ), __LINE__);
        }
        $this->amlSummary = $amlSummary;

        return $this;
    }

    /**
     * Add item to amlSummary value
     *
     * @param AMLSummary $item
     * @return AMLSummaryContainer
     * @throws InvalidArgumentException
     */
    public function addToAmlSummary(AMLSummary $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof AMLSummary) {
            throw new InvalidArgumentException(sprintf(
                'The amlSummary property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AMLSummary, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(2)
        if (is_array($this->amlSummary) && count($this->amlSummary) >= 2) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 2',
                count($this->amlSummary)
            ), __LINE__);
        }
        $this->amlSummary[] = $item;

        return $this;
    }
}
