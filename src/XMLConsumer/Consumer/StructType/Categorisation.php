<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Categorisation StructType
 *
 * @subpackage Structs
 */
class Categorisation extends AbstractStructBase
{
    /**
     * The categoryType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $categoryType = null;
    /**
     * The category
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     *
     * @var string[]
     */
    protected ?array $category = null;

    /**
     * Constructor method for Categorisation
     *
     * @param string   $categoryType
     * @param string[] $category
     * @uses Categorisation::setCategoryType()
     * @uses Categorisation::setCategory()
     */
    public function __construct(?string $categoryType = null, ?array $category = null)
    {
        $this
            ->setCategoryType($categoryType)
            ->setCategory($category);
    }

    /**
     * This method is responsible for validating the values passed to the setCategory method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCategory method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCategoryForArrayConstraintsFromSetCategory(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $categorisationCategoryItem) {
            // validation for constraint: itemType
            if (!is_string($categorisationCategoryItem)) {
                $invalidValues[] = is_object($categorisationCategoryItem) ? get_class($categorisationCategoryItem) : sprintf(
                    '%s(%s)',
                    gettype($categorisationCategoryItem),
                    var_export($categorisationCategoryItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The category property can only contain items of type string, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get categoryType value
     *
     * @return string|null
     */
    public function getCategoryType(): ?string
    {
        return $this->categoryType;
    }

    /**
     * Set categoryType value
     *
     * @param string $categoryType
     * @return Categorisation
     */
    public function setCategoryType(?string $categoryType = null): self
    {
        // validation for constraint: string
        if (!is_null($categoryType) && !is_string($categoryType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($categoryType, true),
                gettype($categoryType)
            ), __LINE__);
        }
        $this->categoryType = $categoryType;

        return $this;
    }

    /**
     * Get category value
     *
     * @return string[]
     */
    public function getCategory(): ?array
    {
        return $this->category;
    }

    /**
     * Set category value
     *
     * @param string[] $category
     * @return Categorisation
     * @throws InvalidArgumentException
     */
    public function setCategory(?array $category = null): self
    {
        // validation for constraint: array
        if ('' !== ($categoryArrayErrorMessage = self::validateCategoryForArrayConstraintsFromSetCategory($category))) {
            throw new InvalidArgumentException($categoryArrayErrorMessage, __LINE__);
        }
        $this->category = $category;

        return $this;
    }

    /**
     * Add item to category value
     *
     * @param string $item
     * @return Categorisation
     * @throws InvalidArgumentException
     */
    public function addToCategory(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf(
                'The category property can only contain items of type string, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        $this->category[] = $item;

        return $this;
    }
}
