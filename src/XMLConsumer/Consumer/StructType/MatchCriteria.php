<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\MatchCriteriaStatus;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * Data matching criteria for a number of name match 'levels': applicant, associate, potential associate, attributable or family. Each name
 * match level flag can be set to include/exclude or include/exclude depending upon whether a score has been calculated using data in that
 * name match level.
 *
 * @subpackage Structs
 */
class MatchCriteria extends AbstractStructBase
{
    protected string $associate = MatchCriteriaStatus::VALUE_NOT_REQUIRED;
    protected string $attributable = MatchCriteriaStatus::VALUE_NOT_REQUIRED;
    protected string $family = MatchCriteriaStatus::VALUE_NOT_REQUIRED;
    protected string $potentialAssociate = MatchCriteriaStatus::VALUE_NOT_REQUIRED;
    protected string $subject = MatchCriteriaStatus::VALUE_NOT_REQUIRED;

    public function getAssociate(): string
    {
        return $this->associate;
    }

    public function setAssociate(string $associate): self
    {
        // validation for constraint: enumeration
        if (!MatchCriteriaStatus::valueIsValid($associate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\MatchCriteriaStatus',
                is_array($associate) ? implode(', ', $associate) : var_export($associate, true),
                implode(', ', MatchCriteriaStatus::getValidValues())
            ), __LINE__);
        }
        $this->associate = $associate;

        return $this;
    }

    public function getAttributable(): string
    {
        return $this->attributable;
    }

    public function setAttributable(string $attributable): self
    {
        // validation for constraint: enumeration
        if (!MatchCriteriaStatus::valueIsValid($attributable)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\MatchCriteriaStatus',
                is_array($attributable) ? implode(', ', $attributable) : var_export($attributable, true),
                implode(', ', MatchCriteriaStatus::getValidValues())
            ), __LINE__);
        }
        $this->attributable = $attributable;

        return $this;
    }

    public function getFamily(): string
    {
        return $this->family;
    }

    public function setFamily(string $family): self
    {
        // validation for constraint: enumeration
        if (!MatchCriteriaStatus::valueIsValid($family)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\MatchCriteriaStatus',
                is_array($family) ? implode(', ', $family) : var_export($family, true),
                implode(', ', MatchCriteriaStatus::getValidValues())
            ), __LINE__);
        }
        $this->family = $family;

        return $this;
    }

    public function getPotentialAssociate(): string
    {
        return $this->potentialAssociate;
    }

    public function setPotentialAssociate(string $potentialAssociate): self
    {
        // validation for constraint: enumeration
        if (!MatchCriteriaStatus::valueIsValid($potentialAssociate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\MatchCriteriaStatus',
                is_array($potentialAssociate) ? implode(', ', $potentialAssociate) : var_export($potentialAssociate, true),
                implode(', ', MatchCriteriaStatus::getValidValues())
            ), __LINE__);
        }
        $this->potentialAssociate = $potentialAssociate;

        return $this;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        // validation for constraint: enumeration
        if (!MatchCriteriaStatus::valueIsValid($subject)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\MatchCriteriaStatus',
                is_array($subject) ? implode(', ', $subject) : var_export($subject, true),
                implode(', ', MatchCriteriaStatus::getValidValues())
            ), __LINE__);
        }
        $this->subject = $subject;

        return $this;
    }
}
