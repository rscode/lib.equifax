<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OtherCountryDetails StructType
 *
 * @subpackage Structs
 */
class OtherCountryDetails extends AbstractStructBase
{
    /**
     * The otherCountry
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var OtherCountry[]
     */
    protected ?array $otherCountry = null;

    /**
     * Constructor method for OtherCountryDetails
     *
     * @param OtherCountry[] $otherCountry
     * @uses OtherCountryDetails::setOtherCountry()
     */
    public function __construct(?array $otherCountry = null)
    {
        $this
            ->setOtherCountry($otherCountry);
    }

    /**
     * This method is responsible for validating the values passed to the setOtherCountry method
     * This method is willingly generated in order to preserve the one-line inline validation within the setOtherCountry method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateOtherCountryForArrayConstraintsFromSetOtherCountry(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $otherCountryDetailsOtherCountryItem) {
            // validation for constraint: itemType
            if (!$otherCountryDetailsOtherCountryItem instanceof OtherCountry) {
                $invalidValues[] = is_object($otherCountryDetailsOtherCountryItem) ? get_class($otherCountryDetailsOtherCountryItem) : sprintf(
                    '%s(%s)',
                    gettype($otherCountryDetailsOtherCountryItem),
                    var_export($otherCountryDetailsOtherCountryItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The otherCountry property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\OtherCountry, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get otherCountry value
     *
     * @return OtherCountry[]
     */
    public function getOtherCountry(): ?array
    {
        return $this->otherCountry;
    }

    /**
     * Set otherCountry value
     *
     * @param OtherCountry[] $otherCountry
     * @return OtherCountryDetails
     * @throws InvalidArgumentException
     */
    public function setOtherCountry(?array $otherCountry = null): self
    {
        // validation for constraint: array
        if ('' !== ($otherCountryArrayErrorMessage = self::validateOtherCountryForArrayConstraintsFromSetOtherCountry($otherCountry))) {
            throw new InvalidArgumentException($otherCountryArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($otherCountry) && count($otherCountry) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($otherCountry)
            ), __LINE__);
        }
        $this->otherCountry = $otherCountry;

        return $this;
    }

    /**
     * Add item to otherCountry value
     *
     * @param OtherCountry $item
     * @return OtherCountryDetails
     * @throws InvalidArgumentException
     */
    public function addToOtherCountry(OtherCountry $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof OtherCountry) {
            throw new InvalidArgumentException(sprintf(
                'The otherCountry property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\OtherCountry, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->otherCountry) && count($this->otherCountry) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->otherCountry)
            ), __LINE__);
        }
        $this->otherCountry[] = $item;

        return $this;
    }
}
