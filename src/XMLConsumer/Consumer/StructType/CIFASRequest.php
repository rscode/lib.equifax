<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for CIFASRequest StructType
 *
 * @subpackage Structs
 */
class CIFASRequest extends RawDataAvailableAtLinkedAddressRequest
{
}
