<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for WatchlistCheckFullReportResponse StructType
 * Meta information extracted from the WSDL
 * - documentation: This operation should be used if the identity verification search is being performed to comply with anti-money laundering regulations - use the Identity Verification operation for other ID purposes.
 *
 * @subpackage Structs
 */
class WatchlistCheckFullReportResponse extends CommonResponse
{
    /**
     * The watchlistCheckFull
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var WatchlistCheckFull
     */
    protected WatchlistCheckFull $watchlistCheckFull;

    /**
     * Constructor method for WatchlistCheckFullReportResponse
     *
     * @param WatchlistCheckFull $watchlistCheckFull
     * @uses WatchlistCheckFullReportResponse::setWatchlistCheckFull()
     */
    public function __construct(WatchlistCheckFull $watchlistCheckFull)
    {
        $this
            ->setWatchlistCheckFull($watchlistCheckFull);
    }

    /**
     * Get watchlistCheckFull value
     *
     * @return WatchlistCheckFull
     */
    public function getWatchlistCheckFull(): WatchlistCheckFull
    {
        return $this->watchlistCheckFull;
    }

    /**
     * Set watchlistCheckFull value
     *
     * @param WatchlistCheckFull $watchlistCheckFull
     * @return WatchlistCheckFullReportResponse
     */
    public function setWatchlistCheckFull(WatchlistCheckFull $watchlistCheckFull): self
    {
        $this->watchlistCheckFull = $watchlistCheckFull;

        return $this;
    }
}
