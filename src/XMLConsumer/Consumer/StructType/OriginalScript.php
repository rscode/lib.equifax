<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OriginalScript StructType
 *
 * @subpackage Structs
 */
class OriginalScript extends AbstractStructBase
{
    /**
     * The OriginalScriptName
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var string[]
     */
    protected ?array $OriginalScriptName = null;

    /**
     * Constructor method for OriginalScript
     *
     * @param string[] $originalScriptName
     * @uses OriginalScript::setOriginalScriptName()
     */
    public function __construct(?array $originalScriptName = null)
    {
        $this
            ->setOriginalScriptName($originalScriptName);
    }

    /**
     * This method is responsible for validating the values passed to the setOriginalScriptName method
     * This method is willingly generated in order to preserve the one-line inline validation within the setOriginalScriptName method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateOriginalScriptNameForArrayConstraintsFromSetOriginalScriptName(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $originalScriptOriginalScriptNameItem) {
            // validation for constraint: itemType
            if (!is_string($originalScriptOriginalScriptNameItem)) {
                $invalidValues[] = is_object($originalScriptOriginalScriptNameItem) ? get_class($originalScriptOriginalScriptNameItem) : sprintf(
                    '%s(%s)',
                    gettype($originalScriptOriginalScriptNameItem),
                    var_export($originalScriptOriginalScriptNameItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The OriginalScriptName property can only contain items of type string, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get OriginalScriptName value
     *
     * @return string[]
     */
    public function getOriginalScriptName(): ?array
    {
        return $this->OriginalScriptName;
    }

    /**
     * Set OriginalScriptName value
     *
     * @param string[] $originalScriptName
     * @return OriginalScript
     * @throws InvalidArgumentException
     */
    public function setOriginalScriptName(?array $originalScriptName = null): self
    {
        // validation for constraint: array
        if ('' !== ($originalScriptNameArrayErrorMessage = self::validateOriginalScriptNameForArrayConstraintsFromSetOriginalScriptName($originalScriptName))) {
            throw new InvalidArgumentException($originalScriptNameArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($originalScriptName) && count($originalScriptName) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($originalScriptName)
            ), __LINE__);
        }
        $this->OriginalScriptName = $originalScriptName;

        return $this;
    }

    /**
     * Add item to OriginalScriptName value
     *
     * @param string $item
     * @return OriginalScript
     * @throws InvalidArgumentException
     */
    public function addToOriginalScriptName(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf(
                'The OriginalScriptName property can only contain items of type string, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->OriginalScriptName) && count($this->OriginalScriptName) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->OriginalScriptName)
            ), __LINE__);
        }
        $this->OriginalScriptName[] = $item;

        return $this;
    }
}
