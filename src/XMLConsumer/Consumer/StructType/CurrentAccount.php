<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for CurrentAccount StructType
 *
 * @subpackage Structs
 */
class CurrentAccount extends FinancialAgreement
{
    /**
     * The overdraft
     * Meta information extracted from the WSDL
     * - use: optional
     *
     * @var bool|null
     */
    protected ?bool $overdraft = null;

    /**
     * Constructor method for CurrentAccount
     *
     * @param bool $overdraft
     * @uses CurrentAccount::setOverdraft()
     */
    public function __construct(?bool $overdraft = null)
    {
        $this
            ->setOverdraft($overdraft);
    }

    /**
     * Get overdraft value
     *
     * @return bool|null
     */
    public function getOverdraft(): ?bool
    {
        return $this->overdraft;
    }

    /**
     * Set overdraft value
     *
     * @param bool $overdraft
     * @return CurrentAccount
     */
    public function setOverdraft(?bool $overdraft = null): self
    {
        // validation for constraint: boolean
        if (!is_null($overdraft) && !is_bool($overdraft)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($overdraft, true),
                gettype($overdraft)
            ), __LINE__);
        }
        $this->overdraft = $overdraft;

        return $this;
    }
}
