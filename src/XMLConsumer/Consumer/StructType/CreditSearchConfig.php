<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreditSearchConfig StructType
 * Meta information extracted from the WSDL
 * - documentation: Indicates whether third party data (family or financial associates) should be used on an enquiry.
 *
 * @subpackage Structs
 */
class CreditSearchConfig extends AbstractStructBase
{
    /**
     * The optIn
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $optIn;

    /**
     * Constructor method for CreditSearchConfig
     *
     * @param bool $optIn
     * @uses CreditSearchConfig::setOptIn()
     */
    public function __construct(bool $optIn)
    {
        $this
            ->setOptIn($optIn);
    }

    /**
     * Get optIn value
     *
     * @return bool
     */
    public function getOptIn(): bool
    {
        return $this->optIn;
    }

    /**
     * Set optIn value
     *
     * @param bool $optIn
     * @return CreditSearchConfig
     */
    public function setOptIn(bool $optIn): self
    {
        // validation for constraint: boolean
        if (!is_null($optIn) && !is_bool($optIn)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($optIn, true),
                gettype($optIn)
            ), __LINE__);
        }
        $this->optIn = $optIn;

        return $this;
    }
}
