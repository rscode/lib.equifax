<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for AliasRequest StructType
 *
 * @subpackage Structs
 */
class AliasRequest extends RawDataRequest
{
}
