<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for AssociatesContainer StructType
 *
 * @subpackage Structs
 */
class AssociatesContainer extends DataItemContainer
{
    /**
     * The associate
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var Associate[]
     */
    protected ?array $associate = null;

    /**
     * Constructor method for AssociatesContainer
     *
     * @param Associate[] $associate
     * @uses AssociatesContainer::setAssociate()
     */
    public function __construct(?array $associate = null)
    {
        $this
            ->setAssociate($associate);
    }

    /**
     * This method is responsible for validating the values passed to the setAssociate method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAssociate method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAssociateForArrayConstraintsFromSetAssociate(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $associatesContainerAssociateItem) {
            // validation for constraint: itemType
            if (!$associatesContainerAssociateItem instanceof Associate) {
                $invalidValues[] = is_object($associatesContainerAssociateItem) ? get_class($associatesContainerAssociateItem) : sprintf(
                    '%s(%s)',
                    gettype($associatesContainerAssociateItem),
                    var_export($associatesContainerAssociateItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The associate property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\Associate, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get associate value
     *
     * @return Associate[]
     */
    public function getAssociate(): ?array
    {
        return $this->associate;
    }

    /**
     * Set associate value
     *
     * @param Associate[] $associate
     * @return AssociatesContainer
     * @throws InvalidArgumentException
     */
    public function setAssociate(?array $associate = null): self
    {
        // validation for constraint: array
        if ('' !== ($associateArrayErrorMessage = self::validateAssociateForArrayConstraintsFromSetAssociate($associate))) {
            throw new InvalidArgumentException($associateArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($associate) && count($associate) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($associate)
            ), __LINE__);
        }
        $this->associate = $associate;

        return $this;
    }

    /**
     * Add item to associate value
     *
     * @param Associate $item
     * @return AssociatesContainer
     * @throws InvalidArgumentException
     */
    public function addToAssociate(Associate $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof Associate) {
            throw new InvalidArgumentException(sprintf(
                'The associate property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\Associate, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->associate) && count($this->associate) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->associate)
            ), __LINE__);
        }
        $this->associate[] = $item;

        return $this;
    }
}
