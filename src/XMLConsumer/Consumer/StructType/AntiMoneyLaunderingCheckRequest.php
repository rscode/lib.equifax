<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for AntiMoneyLaunderingCheckRequest StructType
 * Meta information extracted from the WSDL
 * - documentation: This operation should be used if the identity verification search is being performed to comply with anti-money laundering regulations - use the Identity Verification operation for other ID purposes.
 *
 * @subpackage Structs
 */
class AntiMoneyLaunderingCheckRequest extends CommonRequest
{
}
