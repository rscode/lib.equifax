<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\CurrencyType;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MonetaryAmount StructType
 *
 * @subpackage Structs
 */
class MonetaryAmount extends AbstractStructBase
{
    /**
     * The amount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var float
     */
    protected float $amount;
    /**
     * The currency
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $currency;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for MonetaryAmount
     *
     * @param float                   $amount
     * @param string                  $currency
     * @param DOMDocument|string|null $any
     * @uses MonetaryAmount::setAmount()
     * @uses MonetaryAmount::setCurrency()
     * @uses MonetaryAmount::setAny()
     */
    public function __construct(float $amount, string $currency, $any = null)
    {
        $this
            ->setAmount($amount)
            ->setCurrency($currency)
            ->setAny($any);
    }

    /**
     * Get amount value
     *
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * Set amount value
     *
     * @param float $amount
     * @return MonetaryAmount
     */
    public function setAmount(float $amount): self
    {
        // validation for constraint: float
        if (!is_null($amount) && !(is_float($amount) || is_numeric($amount))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a float value, %s given',
                var_export($amount, true),
                gettype($amount)
            ), __LINE__);
        }
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get currency value
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * Set currency value
     *
     * @param string $currency
     * @return MonetaryAmount
     * @throws InvalidArgumentException
     * @uses CurrencyType::getValidValues
     * @uses CurrencyType::valueIsValid
     */
    public function setCurrency(string $currency): self
    {
        // validation for constraint: enumeration
        if (!CurrencyType::valueIsValid($currency)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\CurrencyType',
                is_array($currency) ? implode(', ', $currency) : var_export($currency, true),
                implode(', ', CurrencyType::getValidValues())
            ), __LINE__);
        }
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return MonetaryAmount
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
