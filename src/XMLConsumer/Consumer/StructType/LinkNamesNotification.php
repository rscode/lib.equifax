<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;

/**
 * This class stands for LinkNamesNotification StructType
 *
 * @subpackage Structs
 */
class LinkNamesNotification extends DataItem
{
    /**
     * The errorCode
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $errorCode;
    /**
     * The message
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $message;
    /**
     * The reference
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 40
     * - maxOccurs: 1
     * - minLength: 1
     * - minOccurs: 1
     * - pattern: .*[^\s].*
     *
     * @var string
     */
    protected string $reference;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for LinkNamesNotification
     *
     * @param string                  $errorCode
     * @param string                  $message
     * @param string                  $reference
     * @param DOMDocument|string|null $any
     * @uses LinkNamesNotification::setErrorCode()
     * @uses LinkNamesNotification::setMessage()
     * @uses LinkNamesNotification::setReference()
     * @uses LinkNamesNotification::setAny()
     */
    public function __construct(string $errorCode, string $message, string $reference, $any = null)
    {
        $this
            ->setErrorCode($errorCode)
            ->setMessage($message)
            ->setReference($reference)
            ->setAny($any);
    }

    /**
     * Get errorCode value
     *
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    /**
     * Set errorCode value
     *
     * @param string $errorCode
     * @return LinkNamesNotification
     */
    public function setErrorCode(string $errorCode): self
    {
        // validation for constraint: string
        if (!is_null($errorCode) && !is_string($errorCode)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($errorCode, true),
                gettype($errorCode)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($errorCode) && mb_strlen((string)$errorCode) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$errorCode)
            ), __LINE__);
        }
        $this->errorCode = $errorCode;

        return $this;
    }

    /**
     * Get message value
     *
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * Set message value
     *
     * @param string $message
     * @return LinkNamesNotification
     */
    public function setMessage(string $message): self
    {
        // validation for constraint: string
        if (!is_null($message) && !is_string($message)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($message, true),
                gettype($message)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($message) && mb_strlen((string)$message) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$message)
            ), __LINE__);
        }
        $this->message = $message;

        return $this;
    }

    /**
     * Get reference value
     *
     * @return string
     */
    public function getReference(): string
    {
        return $this->reference;
    }

    /**
     * Set reference value
     *
     * @param string $reference
     * @return LinkNamesNotification
     */
    public function setReference(string $reference): self
    {
        // validation for constraint: string
        if (!is_null($reference) && !is_string($reference)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($reference, true),
                gettype($reference)
            ), __LINE__);
        }
        // validation for constraint: maxLength(40)
        if (!is_null($reference) && mb_strlen((string)$reference) > 40) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 40',
                mb_strlen((string)$reference)
            ), __LINE__);
        }
        // validation for constraint: minLength(1)
        if (!is_null($reference) && mb_strlen((string)$reference) < 1) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 1',
                mb_strlen((string)$reference)
            ), __LINE__);
        }
        // validation for constraint: pattern(.*[^\s].*)
        if (!is_null($reference) && !preg_match('/.*[^\\s].*/', $reference)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /.*[^\\s].*/',
                var_export($reference, true)
            ), __LINE__);
        }
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return LinkNamesNotification
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
