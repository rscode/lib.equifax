<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ContactInfo StructType
 *
 * @subpackage Structs
 */
class ContactInfo extends AbstractStructBase
{
    /**
     * The emailAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: 2
     * - minOccurs: 0
     *
     * @var EmailAddress[]
     */
    protected ?array $emailAddress = null;
    /**
     * The telephoneNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 3
     * - minOccurs: 0
     *
     * @var TelephoneNumber[]
     */
    protected ?array $telephoneNumber = null;

    /**
     * Constructor method for ContactInfo
     *
     * @param EmailAddress[]    $emailAddress
     * @param TelephoneNumber[] $telephoneNumber
     * @uses ContactInfo::setEmailAddress()
     * @uses ContactInfo::setTelephoneNumber()
     */
    public function __construct(?array $emailAddress = null, ?array $telephoneNumber = null)
    {
        $this
            ->setEmailAddress($emailAddress)
            ->setTelephoneNumber($telephoneNumber);
    }

    /**
     * This method is responsible for validating the values passed to the setEmailAddress method
     * This method is willingly generated in order to preserve the one-line inline validation within the setEmailAddress method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateEmailAddressForArrayConstraintsFromSetEmailAddress(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $contactInfoEmailAddressItem) {
            // validation for constraint: itemType
            if (!$contactInfoEmailAddressItem instanceof EmailAddress) {
                $invalidValues[] = is_object($contactInfoEmailAddressItem) ? get_class($contactInfoEmailAddressItem) : sprintf(
                    '%s(%s)',
                    gettype($contactInfoEmailAddressItem),
                    var_export($contactInfoEmailAddressItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The emailAddress property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\EmailAddress, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * This method is responsible for validating the values passed to the setTelephoneNumber method
     * This method is willingly generated in order to preserve the one-line inline validation within the setTelephoneNumber method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateTelephoneNumberForArrayConstraintsFromSetTelephoneNumber(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $contactInfoTelephoneNumberItem) {
            // validation for constraint: itemType
            if (!$contactInfoTelephoneNumberItem instanceof TelephoneNumber) {
                $invalidValues[] = is_object($contactInfoTelephoneNumberItem) ? get_class($contactInfoTelephoneNumberItem) : sprintf(
                    '%s(%s)',
                    gettype($contactInfoTelephoneNumberItem),
                    var_export($contactInfoTelephoneNumberItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The telephoneNumber property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\TelephoneNumber, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get emailAddress value
     *
     * @return EmailAddress[]
     */
    public function getEmailAddress(): ?array
    {
        return $this->emailAddress;
    }

    /**
     * Set emailAddress value
     *
     * @param EmailAddress[] $emailAddress
     * @return ContactInfo
     * @throws InvalidArgumentException
     */
    public function setEmailAddress(?array $emailAddress = null): self
    {
        // validation for constraint: array
        if ('' !== ($emailAddressArrayErrorMessage = self::validateEmailAddressForArrayConstraintsFromSetEmailAddress($emailAddress))) {
            throw new InvalidArgumentException($emailAddressArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(2)
        if (is_array($emailAddress) && count($emailAddress) > 2) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 2',
                count($emailAddress)
            ), __LINE__);
        }
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * Add item to emailAddress value
     *
     * @param EmailAddress $item
     * @return ContactInfo
     * @throws InvalidArgumentException
     */
    public function addToEmailAddress(EmailAddress $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof EmailAddress) {
            throw new InvalidArgumentException(sprintf(
                'The emailAddress property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\EmailAddress, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(2)
        if (is_array($this->emailAddress) && count($this->emailAddress) >= 2) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 2',
                count($this->emailAddress)
            ), __LINE__);
        }
        $this->emailAddress[] = $item;

        return $this;
    }

    /**
     * Get telephoneNumber value
     *
     * @return TelephoneNumber[]
     */
    public function getTelephoneNumber(): ?array
    {
        return $this->telephoneNumber;
    }

    /**
     * Set telephoneNumber value
     *
     * @param TelephoneNumber[] $telephoneNumber
     * @return ContactInfo
     * @throws InvalidArgumentException
     */
    public function setTelephoneNumber(?array $telephoneNumber = null): self
    {
        // validation for constraint: array
        if ('' !== ($telephoneNumberArrayErrorMessage = self::validateTelephoneNumberForArrayConstraintsFromSetTelephoneNumber($telephoneNumber))) {
            throw new InvalidArgumentException($telephoneNumberArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(3)
        if (is_array($telephoneNumber) && count($telephoneNumber) > 3) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 3',
                count($telephoneNumber)
            ), __LINE__);
        }
        $this->telephoneNumber = $telephoneNumber;

        return $this;
    }

    /**
     * Add item to telephoneNumber value
     *
     * @param TelephoneNumber $item
     * @return ContactInfo
     * @throws InvalidArgumentException
     */
    public function addToTelephoneNumber(TelephoneNumber $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof TelephoneNumber) {
            throw new InvalidArgumentException(sprintf(
                'The telephoneNumber property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\TelephoneNumber, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(3)
        if (is_array($this->telephoneNumber) && count($this->telephoneNumber) >= 3) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 3',
                count($this->telephoneNumber)
            ), __LINE__);
        }
        $this->telephoneNumber[] = $item;

        return $this;
    }
}
