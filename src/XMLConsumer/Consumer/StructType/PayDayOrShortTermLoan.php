<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for PayDayOrShortTermLoan StructType
 *
 * @subpackage Structs
 */
class PayDayOrShortTermLoan extends FinancialAgreement
{
}
