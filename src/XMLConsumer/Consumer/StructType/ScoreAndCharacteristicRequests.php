<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ScoreAndCharacteristicRequests StructType
 *
 * @subpackage Structs
 */
class ScoreAndCharacteristicRequests extends AbstractStructBase
{
    /**
     * The employSameCompanyInsight
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $employSameCompanyInsight;
    /**
     * The scoreRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var ScoreRequest|null
     */
    protected ?ScoreRequest $scoreRequest = null;
    /**
     * The attributeRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AttributeRequest|null
     */
    protected ?AttributeRequest $attributeRequest = null;

    public function __construct(
        bool $employSameCompanyInsight,
        ?ScoreRequest $scoreRequest = null,
        ?AttributeRequest $attributeRequest = null
    ) {
        $this
            ->setEmploySameCompanyInsight($employSameCompanyInsight)
            ->setScoreRequest($scoreRequest)
            ->setAttributeRequest($attributeRequest);
    }

    public function getEmploySameCompanyInsight(): bool
    {
        return $this->employSameCompanyInsight;
    }

    public function setEmploySameCompanyInsight(bool $employSameCompanyInsight): self
    {
        $this->employSameCompanyInsight = $employSameCompanyInsight;

        return $this;
    }

    public function getScoreRequest(): ?ScoreRequest
    {
        return $this->scoreRequest;
    }

    public function setScoreRequest(?ScoreRequest $scoreRequest = null): self
    {
        $this->scoreRequest = $scoreRequest;

        return $this;
    }

    public function getAttributeRequest(): ?AttributeRequest
    {
        return $this->attributeRequest;
    }

    public function setAttributeRequest(?AttributeRequest $attributeRequest = null): self
    {
        $this->attributeRequest = $attributeRequest;

        return $this;
    }
}
