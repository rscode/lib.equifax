<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\MaritalStatus;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MaritalInfo StructType
 *
 * @subpackage Structs
 */
class MaritalInfo extends AbstractStructBase
{
    /**
     * The status
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $status;
    /**
     * The timeMarried
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $timeMarried = null;

    /**
     * Constructor method for MaritalInfo
     *
     * @param string $status
     * @param string $timeMarried
     * @uses MaritalInfo::setStatus()
     * @uses MaritalInfo::setTimeMarried()
     */
    public function __construct(string $status, ?string $timeMarried = null)
    {
        $this
            ->setStatus($status)
            ->setTimeMarried($timeMarried);
    }

    /**
     * Get status value
     *
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * Set status value
     *
     * @param string $status
     * @return MaritalInfo
     * @throws InvalidArgumentException
     * @uses MaritalStatus::getValidValues
     * @uses MaritalStatus::valueIsValid
     */
    public function setStatus(string $status): self
    {
        // validation for constraint: enumeration
        if (!MaritalStatus::valueIsValid($status)) {
            throw new InvalidArgumentException(
                sprintf(
                    'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\MaritalStatus',
                    is_array($status) ? implode(', ', $status) : var_export($status, true),
                    implode(', ', MaritalStatus::getValidValues())
                ),
                __LINE__
            );
        }
        $this->status = $status;

        return $this;
    }

    /**
     * Get timeMarried value
     *
     * @return string|null
     */
    public function getTimeMarried(): ?string
    {
        return $this->timeMarried;
    }

    /**
     * Set timeMarried value
     *
     * @param string $timeMarried
     * @return MaritalInfo
     */
    public function setTimeMarried(?string $timeMarried = null): self
    {
        // validation for constraint: string
        if (!is_null($timeMarried) && !is_string($timeMarried)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($timeMarried, true),
                gettype($timeMarried)
            ), __LINE__);
        }
        $this->timeMarried = $timeMarried;

        return $this;
    }
}
