<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * The Credit Search operation is the most common type of enquiry and models the standard credit enquiry and allows the return of
 * characteristic values, scores, raw and coded data items as required, that either results in a credit search footprint, or search type
 * of SR, for initial enquiries, or SE for subsequent enquiries.
 *
 * @see https://developer.equifax.co.uk/sites/uk/files/xml_ref_guide/ref/Consumer%20Service/Consumer%20Credit%20Search.pdf
 */
class ConsumerCreditSearchResponse extends CommonResponse
{
    public function toJson(bool $removeNulls = true): string
    {
        $json = json_encode($this);

        if ($removeNulls) {
            $json = preg_replace('/,\s*"[^"]+":null|"[^"]+":null,?/', '', $json);
        }

        return $json;
    }

    /**
     * Will be true if the credit search didn't match up to any customer.
     *
     * @return bool
     */
    public function isNoMatch(): bool
    {
        return is_null($this->nonAddressSpecificData);
    }
}
