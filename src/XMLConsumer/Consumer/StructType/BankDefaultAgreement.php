<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for BankDefaultAgreement StructType
 *
 * @subpackage Structs
 */
class BankDefaultAgreement extends FinancialAgreement
{
}
