<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for NoticeOfCorrectionOrDisputeContainer StructType
 *
 * @subpackage Structs
 */
class NoticeOfCorrectionOrDisputeContainer extends DataItemContainer
{
    /**
     * The noticeOfCorrectionOrDispute
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var NoticeOfCorrectionOrDispute[]
     */
    protected ?array $noticeOfCorrectionOrDispute = null;

    /**
     * Constructor method for NoticeOfCorrectionOrDisputeContainer
     *
     * @param NoticeOfCorrectionOrDispute[] $noticeOfCorrectionOrDispute
     * @uses NoticeOfCorrectionOrDisputeContainer::setNoticeOfCorrectionOrDispute()
     */
    public function __construct(?array $noticeOfCorrectionOrDispute = null)
    {
        $this
            ->setNoticeOfCorrectionOrDispute($noticeOfCorrectionOrDispute);
    }

    /**
     * This method is responsible for validating the values passed to the setNoticeOfCorrectionOrDispute method
     * This method is willingly generated in order to preserve the one-line inline validation within the setNoticeOfCorrectionOrDispute method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateNoticeOfCorrectionOrDisputeForArrayConstraintsFromSetNoticeOfCorrectionOrDispute(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $noticeOfCorrectionOrDisputeContainerNoticeOfCorrectionOrDisputeItem) {
            // validation for constraint: itemType
            if (!$noticeOfCorrectionOrDisputeContainerNoticeOfCorrectionOrDisputeItem instanceof NoticeOfCorrectionOrDispute) {
                $invalidValues[] = is_object($noticeOfCorrectionOrDisputeContainerNoticeOfCorrectionOrDisputeItem) ? get_class($noticeOfCorrectionOrDisputeContainerNoticeOfCorrectionOrDisputeItem) : sprintf(
                    '%s(%s)',
                    gettype($noticeOfCorrectionOrDisputeContainerNoticeOfCorrectionOrDisputeItem),
                    var_export($noticeOfCorrectionOrDisputeContainerNoticeOfCorrectionOrDisputeItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The noticeOfCorrectionOrDispute property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\NoticeOfCorrectionOrDispute, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get noticeOfCorrectionOrDispute value
     *
     * @return NoticeOfCorrectionOrDispute[]
     */
    public function getNoticeOfCorrectionOrDispute(): ?array
    {
        return $this->noticeOfCorrectionOrDispute;
    }

    /**
     * Set noticeOfCorrectionOrDispute value
     *
     * @param NoticeOfCorrectionOrDispute[] $noticeOfCorrectionOrDispute
     * @return NoticeOfCorrectionOrDisputeContainer
     * @throws InvalidArgumentException
     */
    public function setNoticeOfCorrectionOrDispute(?array $noticeOfCorrectionOrDispute = null): self
    {
        // validation for constraint: array
        if ('' !== ($noticeOfCorrectionOrDisputeArrayErrorMessage = self::validateNoticeOfCorrectionOrDisputeForArrayConstraintsFromSetNoticeOfCorrectionOrDispute($noticeOfCorrectionOrDispute))) {
            throw new InvalidArgumentException($noticeOfCorrectionOrDisputeArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($noticeOfCorrectionOrDispute) && count($noticeOfCorrectionOrDispute) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($noticeOfCorrectionOrDispute)
            ), __LINE__);
        }
        $this->noticeOfCorrectionOrDispute = $noticeOfCorrectionOrDispute;

        return $this;
    }

    /**
     * Add item to noticeOfCorrectionOrDispute value
     *
     * @param NoticeOfCorrectionOrDispute $item
     * @return NoticeOfCorrectionOrDisputeContainer
     * @throws InvalidArgumentException
     */
    public function addToNoticeOfCorrectionOrDispute(NoticeOfCorrectionOrDispute $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof NoticeOfCorrectionOrDispute) {
            throw new InvalidArgumentException(sprintf(
                'The noticeOfCorrectionOrDispute property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\NoticeOfCorrectionOrDispute, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->noticeOfCorrectionOrDispute) && count($this->noticeOfCorrectionOrDispute) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->noticeOfCorrectionOrDispute)
            ), __LINE__);
        }
        $this->noticeOfCorrectionOrDispute[] = $item;

        return $this;
    }
}
