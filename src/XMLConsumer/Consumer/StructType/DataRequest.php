<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DataRequest StructType
 *
 * @subpackage Structs
 */
abstract class DataRequest extends AbstractStructBase
{
}
