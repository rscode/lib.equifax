<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\CreditLimitChange;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FinancialAgreementStatement StructType
 *
 * @subpackage Structs
 */
class FinancialAgreementStatement extends AbstractStructBase
{
    /**
     * The cashAdvanceCount
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 99
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 0
     *
     * @var int|null
     */
    protected ?int $cashAdvanceCount = null;
    /**
     * The cashAdvanceValue
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var MonetaryAmount|null
     */
    protected ?MonetaryAmount $cashAdvanceValue = null;
    /**
     * The creditLimitChange
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $creditLimitChange = null;
    /**
     * The minimumPayment
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var bool|null
     */
    protected ?bool $minimumPayment = null;
    /**
     * The paymentAmount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var MonetaryAmount|null
     */
    protected ?MonetaryAmount $paymentAmount = null;
    /**
     * The promotionalRate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var bool|null
     */
    protected ?bool $promotionalRate = null;
    /**
     * The statementBalance
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AccountBalance|null
     */
    protected ?AccountBalance $statementBalance = null;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for FinancialAgreementStatement
     *
     * @param int                     $cashAdvanceCount
     * @param MonetaryAmount          $cashAdvanceValue
     * @param string                  $creditLimitChange
     * @param bool                    $minimumPayment
     * @param MonetaryAmount          $paymentAmount
     * @param bool                    $promotionalRate
     * @param AccountBalance          $statementBalance
     * @param DOMDocument|string|null $any
     * @uses FinancialAgreementStatement::setCashAdvanceCount()
     * @uses FinancialAgreementStatement::setCashAdvanceValue()
     * @uses FinancialAgreementStatement::setCreditLimitChange()
     * @uses FinancialAgreementStatement::setMinimumPayment()
     * @uses FinancialAgreementStatement::setPaymentAmount()
     * @uses FinancialAgreementStatement::setPromotionalRate()
     * @uses FinancialAgreementStatement::setStatementBalance()
     * @uses FinancialAgreementStatement::setAny()
     */
    public function __construct(
        ?int $cashAdvanceCount = null,
        ?MonetaryAmount $cashAdvanceValue = null,
        ?string $creditLimitChange = null,
        ?bool $minimumPayment = null,
        ?MonetaryAmount $paymentAmount = null,
        ?bool $promotionalRate = null,
        ?AccountBalance $statementBalance = null,
        $any = null
    ) {
        $this
            ->setCashAdvanceCount($cashAdvanceCount)
            ->setCashAdvanceValue($cashAdvanceValue)
            ->setCreditLimitChange($creditLimitChange)
            ->setMinimumPayment($minimumPayment)
            ->setPaymentAmount($paymentAmount)
            ->setPromotionalRate($promotionalRate)
            ->setStatementBalance($statementBalance)
            ->setAny($any);
    }

    /**
     * Get cashAdvanceCount value
     *
     * @return int|null
     */
    public function getCashAdvanceCount(): ?int
    {
        return $this->cashAdvanceCount;
    }

    /**
     * Set cashAdvanceCount value
     *
     * @param int $cashAdvanceCount
     * @return FinancialAgreementStatement
     */
    public function setCashAdvanceCount(?int $cashAdvanceCount = null): self
    {
        // validation for constraint: int
        if (!is_null($cashAdvanceCount) && !(is_int($cashAdvanceCount) || ctype_digit($cashAdvanceCount))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($cashAdvanceCount, true),
                gettype($cashAdvanceCount)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(99)
        if (!is_null($cashAdvanceCount) && $cashAdvanceCount > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 99',
                var_export($cashAdvanceCount, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($cashAdvanceCount) && $cashAdvanceCount < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($cashAdvanceCount, true)
            ), __LINE__);
        }
        $this->cashAdvanceCount = $cashAdvanceCount;

        return $this;
    }

    /**
     * Get cashAdvanceValue value
     *
     * @return MonetaryAmount|null
     */
    public function getCashAdvanceValue(): ?MonetaryAmount
    {
        return $this->cashAdvanceValue;
    }

    /**
     * Set cashAdvanceValue value
     *
     * @param MonetaryAmount $cashAdvanceValue
     * @return FinancialAgreementStatement
     */
    public function setCashAdvanceValue(?MonetaryAmount $cashAdvanceValue = null): self
    {
        $this->cashAdvanceValue = $cashAdvanceValue;

        return $this;
    }

    /**
     * Get creditLimitChange value
     *
     * @return string|null
     */
    public function getCreditLimitChange(): ?string
    {
        return $this->creditLimitChange;
    }

    /**
     * Set creditLimitChange value
     *
     * @param string $creditLimitChange
     * @return FinancialAgreementStatement
     * @throws InvalidArgumentException
     * @uses CreditLimitChange::getValidValues
     * @uses CreditLimitChange::valueIsValid
     */
    public function setCreditLimitChange(?string $creditLimitChange = null): self
    {
        // validation for constraint: enumeration
        if (!CreditLimitChange::valueIsValid($creditLimitChange)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\CreditLimitChange',
                is_array($creditLimitChange) ? implode(', ', $creditLimitChange) : var_export($creditLimitChange, true),
                implode(', ', CreditLimitChange::getValidValues())
            ), __LINE__);
        }
        $this->creditLimitChange = $creditLimitChange;

        return $this;
    }

    /**
     * Get minimumPayment value
     *
     * @return bool|null
     */
    public function getMinimumPayment(): ?bool
    {
        return $this->minimumPayment;
    }

    /**
     * Set minimumPayment value
     *
     * @param bool $minimumPayment
     * @return FinancialAgreementStatement
     */
    public function setMinimumPayment(?bool $minimumPayment = null): self
    {
        // validation for constraint: boolean
        if (!is_null($minimumPayment) && !is_bool($minimumPayment)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($minimumPayment, true),
                gettype($minimumPayment)
            ), __LINE__);
        }
        $this->minimumPayment = $minimumPayment;

        return $this;
    }

    /**
     * Get paymentAmount value
     *
     * @return MonetaryAmount|null
     */
    public function getPaymentAmount(): ?MonetaryAmount
    {
        return $this->paymentAmount;
    }

    /**
     * Set paymentAmount value
     *
     * @param MonetaryAmount $paymentAmount
     * @return FinancialAgreementStatement
     */
    public function setPaymentAmount(?MonetaryAmount $paymentAmount = null): self
    {
        $this->paymentAmount = $paymentAmount;

        return $this;
    }

    /**
     * Get promotionalRate value
     *
     * @return bool|null
     */
    public function getPromotionalRate(): ?bool
    {
        return $this->promotionalRate;
    }

    /**
     * Set promotionalRate value
     *
     * @param bool $promotionalRate
     * @return FinancialAgreementStatement
     */
    public function setPromotionalRate(?bool $promotionalRate = null): self
    {
        // validation for constraint: boolean
        if (!is_null($promotionalRate) && !is_bool($promotionalRate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($promotionalRate, true),
                gettype($promotionalRate)
            ), __LINE__);
        }
        $this->promotionalRate = $promotionalRate;

        return $this;
    }

    /**
     * Get statementBalance value
     *
     * @return AccountBalance|null
     */
    public function getStatementBalance(): ?AccountBalance
    {
        return $this->statementBalance;
    }

    /**
     * Set statementBalance value
     *
     * @param AccountBalance $statementBalance
     * @return FinancialAgreementStatement
     */
    public function setStatementBalance(?AccountBalance $statementBalance = null): self
    {
        $this->statementBalance = $statementBalance;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return FinancialAgreementStatement
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
