<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;

/**
 * This class stands for IDPlusCounts StructType
 *
 * @subpackage Structs
 */
class IDPlusCounts extends DataItem
{
    /**
     * The closedCounts
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var AccountAndLenderCounts
     */
    protected AccountAndLenderCounts $closedCounts;
    /**
     * The openCounts
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var AccountAndLenderCounts
     */
    protected AccountAndLenderCounts $openCounts;
    /**
     * The totalInsightAccounts
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 99
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $totalInsightAccounts;
    /**
     * The totalLenders
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 99
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $totalLenders;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for IDPlusCounts
     *
     * @param AccountAndLenderCounts $closedCounts
     * @param AccountAndLenderCounts $openCounts
     * @param int $totalInsightAccounts
     * @param int $totalLenders
     * @param DOMDocument|string|null $any
     * @uses IDPlusCounts::setClosedCounts()
     * @uses IDPlusCounts::setOpenCounts()
     * @uses IDPlusCounts::setTotalInsightAccounts()
     * @uses IDPlusCounts::setTotalLenders()
     * @uses IDPlusCounts::setAny()
     */
    public function __construct(
        AccountAndLenderCounts $closedCounts,
        AccountAndLenderCounts $openCounts,
        int $totalInsightAccounts,
        int $totalLenders,
        $any = null
    ) {
        $this
            ->setClosedCounts($closedCounts)
            ->setOpenCounts($openCounts)
            ->setTotalInsightAccounts($totalInsightAccounts)
            ->setTotalLenders($totalLenders)
            ->setAny($any);
    }

    /**
     * Get closedCounts value
     *
     * @return AccountAndLenderCounts
     */
    public function getClosedCounts(): AccountAndLenderCounts
    {
        return $this->closedCounts;
    }

    /**
     * Set closedCounts value
     *
     * @param AccountAndLenderCounts $closedCounts
     * @return IDPlusCounts
     */
    public function setClosedCounts(AccountAndLenderCounts $closedCounts): self
    {
        $this->closedCounts = $closedCounts;

        return $this;
    }

    /**
     * Get openCounts value
     *
     * @return AccountAndLenderCounts
     */
    public function getOpenCounts(): AccountAndLenderCounts
    {
        return $this->openCounts;
    }

    /**
     * Set openCounts value
     *
     * @param AccountAndLenderCounts $openCounts
     * @return IDPlusCounts
     */
    public function setOpenCounts(AccountAndLenderCounts $openCounts): self
    {
        $this->openCounts = $openCounts;

        return $this;
    }

    /**
     * Get totalInsightAccounts value
     *
     * @return int
     */
    public function getTotalInsightAccounts(): int
    {
        return $this->totalInsightAccounts;
    }

    /**
     * Set totalInsightAccounts value
     *
     * @param int $totalInsightAccounts
     * @return IDPlusCounts
     */
    public function setTotalInsightAccounts(int $totalInsightAccounts): self
    {
        // validation for constraint: int
        if (!is_null($totalInsightAccounts) && !(is_int($totalInsightAccounts) || ctype_digit($totalInsightAccounts))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($totalInsightAccounts, true),
                gettype($totalInsightAccounts)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(99)
        if (!is_null($totalInsightAccounts) && $totalInsightAccounts > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 99',
                var_export($totalInsightAccounts, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($totalInsightAccounts) && $totalInsightAccounts < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($totalInsightAccounts, true)
            ), __LINE__);
        }
        $this->totalInsightAccounts = $totalInsightAccounts;

        return $this;
    }

    /**
     * Get totalLenders value
     *
     * @return int
     */
    public function getTotalLenders(): int
    {
        return $this->totalLenders;
    }

    /**
     * Set totalLenders value
     *
     * @param int $totalLenders
     * @return IDPlusCounts
     */
    public function setTotalLenders(int $totalLenders): self
    {
        // validation for constraint: int
        if (!is_null($totalLenders) && !(is_int($totalLenders) || ctype_digit($totalLenders))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($totalLenders, true),
                gettype($totalLenders)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(99)
        if (!is_null($totalLenders) && $totalLenders > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 99',
                var_export($totalLenders, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($totalLenders) && $totalLenders < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($totalLenders, true)
            ), __LINE__);
        }
        $this->totalLenders = $totalLenders;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return IDPlusCounts
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
