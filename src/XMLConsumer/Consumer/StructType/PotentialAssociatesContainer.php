<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for PotentialAssociatesContainer StructType
 *
 * @subpackage Structs
 */
class PotentialAssociatesContainer extends DataItemContainer
{
    /**
     * The potentialAssociate
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var PotentialAssociate[]
     */
    protected ?array $potentialAssociate = null;

    /**
     * Constructor method for PotentialAssociatesContainer
     *
     * @param PotentialAssociate[] $potentialAssociate
     * @uses PotentialAssociatesContainer::setPotentialAssociate()
     */
    public function __construct(?array $potentialAssociate = null)
    {
        $this
            ->setPotentialAssociate($potentialAssociate);
    }

    /**
     * This method is responsible for validating the values passed to the setPotentialAssociate method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPotentialAssociate method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePotentialAssociateForArrayConstraintsFromSetPotentialAssociate(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $potentialAssociatesContainerPotentialAssociateItem) {
            // validation for constraint: itemType
            if (!$potentialAssociatesContainerPotentialAssociateItem instanceof PotentialAssociate) {
                $invalidValues[] = is_object($potentialAssociatesContainerPotentialAssociateItem) ? get_class($potentialAssociatesContainerPotentialAssociateItem) : sprintf(
                    '%s(%s)',
                    gettype($potentialAssociatesContainerPotentialAssociateItem),
                    var_export($potentialAssociatesContainerPotentialAssociateItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The potentialAssociate property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\PotentialAssociate, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get potentialAssociate value
     *
     * @return PotentialAssociate[]
     */
    public function getPotentialAssociate(): ?array
    {
        return $this->potentialAssociate;
    }

    /**
     * Set potentialAssociate value
     *
     * @param PotentialAssociate[] $potentialAssociate
     * @return PotentialAssociatesContainer
     * @throws InvalidArgumentException
     */
    public function setPotentialAssociate(?array $potentialAssociate = null): self
    {
        // validation for constraint: array
        if ('' !== ($potentialAssociateArrayErrorMessage = self::validatePotentialAssociateForArrayConstraintsFromSetPotentialAssociate($potentialAssociate))) {
            throw new InvalidArgumentException($potentialAssociateArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($potentialAssociate) && count($potentialAssociate) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($potentialAssociate)
            ), __LINE__);
        }
        $this->potentialAssociate = $potentialAssociate;

        return $this;
    }

    /**
     * Add item to potentialAssociate value
     *
     * @param PotentialAssociate $item
     * @return PotentialAssociatesContainer
     * @throws InvalidArgumentException
     */
    public function addToPotentialAssociate(PotentialAssociate $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof PotentialAssociate) {
            throw new InvalidArgumentException(sprintf(
                'The potentialAssociate property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\PotentialAssociate, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->potentialAssociate) && count($this->potentialAssociate) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->potentialAssociate)
            ), __LINE__);
        }
        $this->potentialAssociate[] = $item;

        return $this;
    }
}
