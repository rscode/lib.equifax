<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for MatchedConsumerAddress StructType
 * Meta information extracted from the WSDL
 * - documentation: Free format address as supplied on input or matched address identifier relating to the linked address found.
 *
 * @subpackage Structs
 */
class MatchedConsumerAddress extends ConsumerAddress
{
    /**
     * The address
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var MatchedResidentialStructuredAddress
     */
    protected MatchedResidentialStructuredAddress $address;

    /**
     * Constructor method for MatchedConsumerAddress
     *
     * @param MatchedResidentialStructuredAddress $address
     * @uses MatchedConsumerAddress::setAddress()
     */
    public function __construct(MatchedResidentialStructuredAddress $address)
    {
        $this
            ->setAddress($address);
    }

    /**
     * Get address value
     *
     * @return MatchedResidentialStructuredAddress
     */
    public function getAddress(): MatchedResidentialStructuredAddress
    {
        return $this->address;
    }

    /**
     * Set address value
     *
     * @param MatchedResidentialStructuredAddress $address
     * @return MatchedConsumerAddress
     */
    public function setAddress(MatchedResidentialStructuredAddress $address): self
    {
        $this->address = $address;

        return $this;
    }
}
