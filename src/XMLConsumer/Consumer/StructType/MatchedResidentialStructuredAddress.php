<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for MatchedResidentialStructuredAddress StructType
 *
 * @subpackage Structs
 */
class MatchedResidentialStructuredAddress extends ResidentialStructuredAddress
{
    /**
     * The addressID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 11
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $addressID;

    /**
     * Constructor method for MatchedResidentialStructuredAddress
     *
     * @param string $addressID
     * @uses MatchedResidentialStructuredAddress::setAddressID()
     */
    public function __construct(string $addressID)
    {
        $this
            ->setAddressID($addressID);
    }

    /**
     * Get addressID value
     *
     * @return string
     */
    public function getAddressID(): string
    {
        return $this->addressID;
    }

    /**
     * Set addressID value
     *
     * @param string $addressID
     * @return MatchedResidentialStructuredAddress
     */
    public function setAddressID(string $addressID): self
    {
        // validation for constraint: string
        if (!is_null($addressID) && !is_string($addressID)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($addressID, true),
                gettype($addressID)
            ), __LINE__);
        }
        // validation for constraint: maxLength(11)
        if (!is_null($addressID) && mb_strlen((string)$addressID) > 11) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 11',
                mb_strlen((string)$addressID)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($addressID) && mb_strlen((string)$addressID) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$addressID)
            ), __LINE__);
        }
        $this->addressID = $addressID;

        return $this;
    }
}
