<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for BureauVerificationContainer StructType
 *
 * @subpackage Structs
 */
class BureauVerificationContainer extends DataItemContainer
{
    /**
     * The bureauVerification
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var BureauVerification|null
     */
    protected ?BureauVerification $bureauVerification = null;

    /**
     * Constructor method for BureauVerificationContainer
     *
     * @param BureauVerification $bureauVerification
     * @uses BureauVerificationContainer::setBureauVerification()
     */
    public function __construct(?BureauVerification $bureauVerification = null)
    {
        $this
            ->setBureauVerification($bureauVerification);
    }

    /**
     * Get bureauVerification value
     *
     * @return BureauVerification|null
     */
    public function getBureauVerification(): ?BureauVerification
    {
        return $this->bureauVerification;
    }

    /**
     * Set bureauVerification value
     *
     * @param BureauVerification $bureauVerification
     * @return BureauVerificationContainer
     */
    public function setBureauVerification(?BureauVerification $bureauVerification = null): self
    {
        $this->bureauVerification = $bureauVerification;

        return $this;
    }
}
