<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\RequestScope;

/**
 * This class stands for CodedDataAvailableAtLinkedAddressRequest StructType
 *
 * @subpackage Structs
 */
abstract class CodedDataAvailableAtLinkedAddressRequest extends CodedDataRequest
{
    /**
     * The scope
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $scope = null;

    /**
     * Constructor method for CodedDataAvailableAtLinkedAddressRequest
     *
     * @param string $scope
     * @uses CodedDataAvailableAtLinkedAddressRequest::setScope()
     */
    public function __construct(?string $scope = null)
    {
        $this
            ->setScope($scope);
    }

    /**
     * Get scope value
     *
     * @return string|null
     */
    public function getScope(): ?string
    {
        return $this->scope;
    }

    /**
     * Set scope value
     *
     * @param string $scope
     * @return CodedDataAvailableAtLinkedAddressRequest
     * @throws InvalidArgumentException
     * @uses RequestScope::getValidValues
     * @uses RequestScope::valueIsValid
     */
    public function setScope(?string $scope = null): self
    {
        // validation for constraint: enumeration
        if (!RequestScope::valueIsValid($scope)) {
            throw new InvalidArgumentException(
                sprintf(
                    'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\RequestScope',
                    is_array($scope) ? implode(', ', $scope) : var_export($scope, true),
                    implode(', ', RequestScope::getValidValues())
                ),
                __LINE__
            );
        }
        $this->scope = $scope;

        return $this;
    }
}
