<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for CharacteristicsRequest StructType
 *
 * @subpackage Structs
 */
class CharacteristicsRequest extends CodedDataRequest
{
    /**
     * The index
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 999
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $index;

    /**
     * Constructor method for CharacteristicsRequest
     *
     * @param int $index
     * @uses CharacteristicsRequest::setIndex()
     */
    public function __construct(int $index)
    {
        $this
            ->setIndex($index);
    }

    /**
     * Get index value
     *
     * @return int
     */
    public function getIndex(): int
    {
        return $this->index;
    }

    /**
     * Set index value
     *
     * @param int $index
     * @return CharacteristicsRequest
     */
    public function setIndex(int $index): self
    {
        // validation for constraint: int
        if (!is_null($index) && !(is_int($index) || ctype_digit($index))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($index, true),
                gettype($index)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(999)
        if (!is_null($index) && $index > 999) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 999',
                var_export($index, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($index) && $index < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($index, true)
            ), __LINE__);
        }
        $this->index = $index;

        return $this;
    }
}
