<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for PropertyValuationDataRequest StructType
 *
 * @subpackage Structs
 */
class PropertyValuationDataRequest extends RawDataAvailableAtLinkedAddressRequest
{
}
