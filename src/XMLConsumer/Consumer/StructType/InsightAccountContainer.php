<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;

/**
 * This class stands for InsightAccountContainer StructType
 *
 * @subpackage Structs
 */
class InsightAccountContainer extends DataItemContainer
{
    /**
     * The bankDefaultAgreement
     *
     * @var BankDefaultAgreement[]|null
     */
    protected ?array $bankDefaultAgreement = null;
    /**
     * The budgetAccount
     *
     * @var BudgetAccount[]|null
     */
    protected ?array $budgetAccount = null;
    /**
     * The chargeCard
     *
     * @var ChargeCard[]|null
     */
    protected ?array $chargeCard = null;
    /**
     * The commsSupplyAccount
     *
     * @var CommsSupplyAccount[]|null
     */
    protected ?array $commsSupplyAccount = null;
    /**
     * The consolidatedDebt
     *
     * @var ConsolidatedDebt[]|null
     */
    protected ?array $consolidatedDebt = null;
    /**
     * The councilArrears
     *
     * @var CouncilArrears[]|null
     */
    protected ?array $councilArrears = null;
    /**
     * The creditCard
     *
     * @var CreditCard[]|null
     */
    protected ?array $creditCard = null;
    /**
     * The currentAccount
     *
     * @var CurrentAccount[]|null
     */
    protected ?array $currentAccount = null;
    /**
     * The fixedTermAgreement
     *
     * @var FixedTermAgreement[]|null
     */
    protected ?array $fixedTermAgreement = null;
    /**
     * The hirePurchase
     *
     * @var HirePurchase[]|null
     */
    protected ?array $hirePurchase = null;
    /**
     * The homeLendingAgreement
     *
     * @var HomeLendingAgreement[]|null
     */
    protected ?array $homeLendingAgreement = null;
    /**
     * The insuranceAgreement
     *
     * @var InsuranceAgreement[]|null
     */
    protected ?array $insuranceAgreement = null;
    /**
     * The mailOrderAccount
     *
     * @var MailOrderAccount[]|null
     */
    protected ?array $mailOrderAccount = null;
    /**
     * The optionAccount
     *
     * @var OptionAccount[]|null
     */
    protected ?array $optionAccount = null;
    /**
     * The publicUtilityAccount
     *
     * @var PublicUtilityAccount[]|null
     */
    protected ?array $publicUtilityAccount = null;
    /**
     * The rentalAgreement
     *
     * @var RentalAgreement[]|null
     */
    protected ?array $rentalAgreement = null;
    /**
     * The securedLoan
     *
     * @var SecuredLoan[]|null
     */
    protected ?array $securedLoan = null;
    /**
     * The studentLoan
     *
     * @var StudentLoan[]|null
     */
    protected ?array $studentLoan = null;
    /**
     * The uncategorisedAgreement
     *
     * @var UncategorisedFinancialAgreement[]|null
     */
    protected ?array $uncategorisedAgreement = null;
    /**
     * The unpresentableCheque
     *
     * @var UnpresentableCheque[]|null
     */
    protected ?array $unpresentableCheque = null;
    /**
     * The unsecuredLoan
     *
     * @var UnsecuredLoan[]|null
     */
    protected ?array $unsecuredLoan = null;
    /**
     * The xmasClub
     *
     * @var XmasClub[]|null
     */
    protected ?array $xmasClub = null;
    /**
     * The payDayOrShortTermLoan
     *
     * @var PayDayOrShortTermLoan[]|null
     */
    protected ?array $payDayOrShortTermLoan = null;
    /**
     * The buyToLetMortgage
     *
     * @var BuyToLetMortgage[]|null
     */
    protected ?array $buyToLetMortgage = null;
    /**
     * The propertyRental
     *
     * @var PropertyRental[]|null
     */
    protected ?array $propertyRental = null;
    /**
     * The basicBankAccount
     *
     * @var BasicBankAccount[]|null
     */
    protected ?array $basicBankAccount = null;
    /**
     * The bridgingFinance
     *
     * @var BridgingFinance[]|null
     */
    protected ?array $bridgingFinance = null;
    /**
     * The greenDeals
     *
     * @var GreenDeals[]|null
     */
    protected ?array $greenDeals = null;
    /**
     * The socialHousingRental
     *
     * @var SocialHousingRental[]|null
     */
    protected ?array $socialHousingRental = null;
    /**
     * The localAuthorityHousing
     *
     * @var LocalAuthorityHousing[]|null
     */
    protected ?array $localAuthorityHousing = null;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    public function __construct(
        ?BankDefaultAgreement $bankDefaultAgreement = null,
        ?BudgetAccount $budgetAccount = null,
        ?ChargeCard $chargeCard = null,
        ?CommsSupplyAccount $commsSupplyAccount = null,
        ?ConsolidatedDebt $consolidatedDebt = null,
        ?CouncilArrears $councilArrears = null,
        ?CreditCard $creditCard = null,
        ?CurrentAccount $currentAccount = null,
        ?FixedTermAgreement $fixedTermAgreement = null,
        ?HirePurchase $hirePurchase = null,
        ?HomeLendingAgreement $homeLendingAgreement = null,
        ?InsuranceAgreement $insuranceAgreement = null,
        ?MailOrderAccount $mailOrderAccount = null,
        ?OptionAccount $optionAccount = null,
        ?PublicUtilityAccount $publicUtilityAccount = null,
        ?RentalAgreement $rentalAgreement = null,
        ?SecuredLoan $securedLoan = null,
        ?StudentLoan $studentLoan = null,
        ?UncategorisedFinancialAgreement $uncategorisedAgreement = null,
        ?UnpresentableCheque $unpresentableCheque = null,
        ?UnsecuredLoan $unsecuredLoan = null,
        ?XmasClub $xmasClub = null,
        ?PayDayOrShortTermLoan $payDayOrShortTermLoan = null,
        ?BuyToLetMortgage $buyToLetMortgage = null,
        ?PropertyRental $propertyRental = null,
        ?BasicBankAccount $basicBankAccount = null,
        ?BridgingFinance $bridgingFinance = null,
        ?GreenDeals $greenDeals = null,
        ?SocialHousingRental $socialHousingRental = null,
        ?LocalAuthorityHousing $localAuthorityHousing = null,
        $any = null
    ) {
        $this
            ->setBankDefaultAgreement($bankDefaultAgreement)
            ->setBudgetAccount($budgetAccount)
            ->setChargeCard($chargeCard)
            ->setCommsSupplyAccount($commsSupplyAccount)
            ->setConsolidatedDebt($consolidatedDebt)
            ->setCouncilArrears($councilArrears)
            ->setCreditCard($creditCard)
            ->setCurrentAccount($currentAccount)
            ->setFixedTermAgreement($fixedTermAgreement)
            ->setHirePurchase($hirePurchase)
            ->setHomeLendingAgreement($homeLendingAgreement)
            ->setInsuranceAgreement($insuranceAgreement)
            ->setMailOrderAccount($mailOrderAccount)
            ->setOptionAccount($optionAccount)
            ->setPublicUtilityAccount($publicUtilityAccount)
            ->setRentalAgreement($rentalAgreement)
            ->setSecuredLoan($securedLoan)
            ->setStudentLoan($studentLoan)
            ->setUncategorisedAgreement($uncategorisedAgreement)
            ->setUnpresentableCheque($unpresentableCheque)
            ->setUnsecuredLoan($unsecuredLoan)
            ->setXmasClub($xmasClub)
            ->setPayDayOrShortTermLoan($payDayOrShortTermLoan)
            ->setBuyToLetMortgage($buyToLetMortgage)
            ->setPropertyRental($propertyRental)
            ->setBasicBankAccount($basicBankAccount)
            ->setBridgingFinance($bridgingFinance)
            ->setGreenDeals($greenDeals)
            ->setSocialHousingRental($socialHousingRental)
            ->setLocalAuthorityHousing($localAuthorityHousing)
            ->setAny($any);
    }

    /**
     * Get bankDefaultAgreement value
     *
     * @return BankDefaultAgreement|null
     */
    public function getBankDefaultAgreement(): ?BankDefaultAgreement
    {
        return $this->bankDefaultAgreement;
    }

    /**
     * Set bankDefaultAgreement value
     *
     * @param BankDefaultAgreement $bankDefaultAgreement
     * @return InsightAccountContainer
     */
    public function setBankDefaultAgreement(?BankDefaultAgreement $bankDefaultAgreement = null): self
    {
        $this->bankDefaultAgreement = $bankDefaultAgreement;

        return $this;
    }

    /**
     * Get budgetAccount value
     *
     * @return BudgetAccount|null
     */
    public function getBudgetAccount(): ?BudgetAccount
    {
        return $this->budgetAccount;
    }

    /**
     * Set budgetAccount value
     *
     * @param BudgetAccount $budgetAccount
     * @return InsightAccountContainer
     */
    public function setBudgetAccount(?BudgetAccount $budgetAccount = null): self
    {
        $this->budgetAccount = $budgetAccount;

        return $this;
    }

    /**
     * Get chargeCard value
     *
     * @return ChargeCard|null
     */
    public function getChargeCard(): ?ChargeCard
    {
        return $this->chargeCard;
    }

    /**
     * Set chargeCard value
     *
     * @param ChargeCard $chargeCard
     * @return InsightAccountContainer
     */
    public function setChargeCard(?ChargeCard $chargeCard = null): self
    {
        $this->chargeCard = $chargeCard;

        return $this;
    }

    /**
     * Get commsSupplyAccount value
     *
     * @return CommsSupplyAccount|null
     */
    public function getCommsSupplyAccount(): ?CommsSupplyAccount
    {
        return $this->commsSupplyAccount;
    }

    /**
     * Set commsSupplyAccount value
     *
     * @param CommsSupplyAccount $commsSupplyAccount
     * @return InsightAccountContainer
     */
    public function setCommsSupplyAccount(?CommsSupplyAccount $commsSupplyAccount = null): self
    {
        $this->commsSupplyAccount = $commsSupplyAccount;

        return $this;
    }

    /**
     * Get consolidatedDebt value
     *
     * @return ConsolidatedDebt|null
     */
    public function getConsolidatedDebt(): ?ConsolidatedDebt
    {
        return $this->consolidatedDebt;
    }

    /**
     * Set consolidatedDebt value
     *
     * @param ConsolidatedDebt $consolidatedDebt
     * @return InsightAccountContainer
     */
    public function setConsolidatedDebt(?ConsolidatedDebt $consolidatedDebt = null): self
    {
        $this->consolidatedDebt = $consolidatedDebt;

        return $this;
    }

    /**
     * Get councilArrears value
     *
     * @return CouncilArrears|null
     */
    public function getCouncilArrears(): ?CouncilArrears
    {
        return $this->councilArrears;
    }

    /**
     * Set councilArrears value
     *
     * @param CouncilArrears $councilArrears
     * @return InsightAccountContainer
     */
    public function setCouncilArrears(?CouncilArrears $councilArrears = null): self
    {
        $this->councilArrears = $councilArrears;

        return $this;
    }

    /**
     * Get creditCard value
     *
     * @return CreditCard|null
     */
    public function getCreditCard(): ?CreditCard
    {
        return $this->creditCard;
    }

    /**
     * Set creditCard value
     *
     * @param CreditCard $creditCard
     * @return InsightAccountContainer
     */
    public function setCreditCard(?CreditCard $creditCard = null): self
    {
        $this->creditCard = $creditCard;

        return $this;
    }

    /**
     * Get currentAccount value
     *
     * @return CurrentAccount|null
     */
    public function getCurrentAccount(): ?CurrentAccount
    {
        return $this->currentAccount;
    }

    /**
     * Set currentAccount value
     *
     * @param CurrentAccount $currentAccount
     * @return InsightAccountContainer
     */
    public function setCurrentAccount(?CurrentAccount $currentAccount = null): self
    {
        $this->currentAccount = $currentAccount;

        return $this;
    }

    /**
     * Get fixedTermAgreement value
     *
     * @return FixedTermAgreement|null
     */
    public function getFixedTermAgreement(): ?FixedTermAgreement
    {
        return $this->fixedTermAgreement;
    }

    /**
     * Set fixedTermAgreement value
     *
     * @param FixedTermAgreement $fixedTermAgreement
     * @return InsightAccountContainer
     */
    public function setFixedTermAgreement(?FixedTermAgreement $fixedTermAgreement = null): self
    {
        $this->fixedTermAgreement = $fixedTermAgreement;

        return $this;
    }

    /**
     * Get hirePurchase value
     *
     * @return HirePurchase|null
     */
    public function getHirePurchase(): ?HirePurchase
    {
        return $this->hirePurchase;
    }

    /**
     * Set hirePurchase value
     *
     * @param HirePurchase $hirePurchase
     * @return InsightAccountContainer
     */
    public function setHirePurchase(?HirePurchase $hirePurchase = null): self
    {
        $this->hirePurchase = $hirePurchase;

        return $this;
    }

    /**
     * Get homeLendingAgreement value
     *
     * @return HomeLendingAgreement|null
     */
    public function getHomeLendingAgreement(): ?HomeLendingAgreement
    {
        return $this->homeLendingAgreement;
    }

    /**
     * Set homeLendingAgreement value
     *
     * @param HomeLendingAgreement $homeLendingAgreement
     * @return InsightAccountContainer
     */
    public function setHomeLendingAgreement(?HomeLendingAgreement $homeLendingAgreement = null): self
    {
        $this->homeLendingAgreement = $homeLendingAgreement;

        return $this;
    }

    /**
     * Get insuranceAgreement value
     *
     * @return InsuranceAgreement|null
     */
    public function getInsuranceAgreement(): ?InsuranceAgreement
    {
        return $this->insuranceAgreement;
    }

    /**
     * Set insuranceAgreement value
     *
     * @param InsuranceAgreement $insuranceAgreement
     * @return InsightAccountContainer
     */
    public function setInsuranceAgreement(?InsuranceAgreement $insuranceAgreement = null): self
    {
        $this->insuranceAgreement = $insuranceAgreement;

        return $this;
    }

    /**
     * Get mailOrderAccount value
     *
     * @return MailOrderAccount|null
     */
    public function getMailOrderAccount(): ?MailOrderAccount
    {
        return $this->mailOrderAccount;
    }

    /**
     * Set mailOrderAccount value
     *
     * @param MailOrderAccount $mailOrderAccount
     * @return InsightAccountContainer
     */
    public function setMailOrderAccount(?MailOrderAccount $mailOrderAccount = null): self
    {
        $this->mailOrderAccount = $mailOrderAccount;

        return $this;
    }

    /**
     * Get optionAccount value
     *
     * @return OptionAccount|null
     */
    public function getOptionAccount(): ?OptionAccount
    {
        return $this->optionAccount;
    }

    /**
     * Set optionAccount value
     *
     * @param OptionAccount $optionAccount
     * @return InsightAccountContainer
     */
    public function setOptionAccount(?OptionAccount $optionAccount = null): self
    {
        $this->optionAccount = $optionAccount;

        return $this;
    }

    /**
     * Get publicUtilityAccount value
     *
     * @return PublicUtilityAccount|null
     */
    public function getPublicUtilityAccount(): ?PublicUtilityAccount
    {
        return $this->publicUtilityAccount;
    }

    /**
     * Set publicUtilityAccount value
     *
     * @param PublicUtilityAccount $publicUtilityAccount
     * @return InsightAccountContainer
     */
    public function setPublicUtilityAccount(?PublicUtilityAccount $publicUtilityAccount = null): self
    {
        $this->publicUtilityAccount = $publicUtilityAccount;

        return $this;
    }

    /**
     * Get rentalAgreement value
     *
     * @return RentalAgreement|null
     */
    public function getRentalAgreement(): ?RentalAgreement
    {
        return $this->rentalAgreement;
    }

    /**
     * Set rentalAgreement value
     *
     * @param RentalAgreement $rentalAgreement
     * @return InsightAccountContainer
     */
    public function setRentalAgreement(?RentalAgreement $rentalAgreement = null): self
    {
        $this->rentalAgreement = $rentalAgreement;

        return $this;
    }

    /**
     * Get securedLoan value
     *
     * @return SecuredLoan|null
     */
    public function getSecuredLoan(): ?SecuredLoan
    {
        return $this->securedLoan;
    }

    /**
     * Set securedLoan value
     *
     * @param SecuredLoan $securedLoan
     * @return InsightAccountContainer
     */
    public function setSecuredLoan(?SecuredLoan $securedLoan = null): self
    {
        $this->securedLoan = $securedLoan;

        return $this;
    }

    /**
     * Get studentLoan value
     *
     * @return StudentLoan|null
     */
    public function getStudentLoan(): ?StudentLoan
    {
        return $this->studentLoan;
    }

    /**
     * Set studentLoan value
     *
     * @param StudentLoan $studentLoan
     * @return InsightAccountContainer
     */
    public function setStudentLoan(?StudentLoan $studentLoan = null): self
    {
        $this->studentLoan = $studentLoan;

        return $this;
    }

    /**
     * Get uncategorisedAgreement value
     *
     * @return UncategorisedFinancialAgreement|null
     */
    public function getUncategorisedAgreement(): ?UncategorisedFinancialAgreement
    {
        return $this->uncategorisedAgreement;
    }

    /**
     * Set uncategorisedAgreement value
     *
     * @param UncategorisedFinancialAgreement $uncategorisedAgreement
     * @return InsightAccountContainer
     */
    public function setUncategorisedAgreement(?UncategorisedFinancialAgreement $uncategorisedAgreement = null): self
    {
        $this->uncategorisedAgreement = $uncategorisedAgreement;

        return $this;
    }

    /**
     * Get unpresentableCheque value
     *
     * @return UnpresentableCheque|null
     */
    public function getUnpresentableCheque(): ?UnpresentableCheque
    {
        return $this->unpresentableCheque;
    }

    /**
     * Set unpresentableCheque value
     *
     * @param UnpresentableCheque $unpresentableCheque
     * @return InsightAccountContainer
     */
    public function setUnpresentableCheque(?UnpresentableCheque $unpresentableCheque = null): self
    {
        $this->unpresentableCheque = $unpresentableCheque;

        return $this;
    }

    /**
     * Get unsecuredLoan value
     *
     * @return UnsecuredLoan|null
     */
    public function getUnsecuredLoan(): ?UnsecuredLoan
    {
        return $this->unsecuredLoan;
    }

    /**
     * Set unsecuredLoan value
     *
     * @param UnsecuredLoan $unsecuredLoan
     * @return InsightAccountContainer
     */
    public function setUnsecuredLoan(?UnsecuredLoan $unsecuredLoan = null): self
    {
        $this->unsecuredLoan = $unsecuredLoan;

        return $this;
    }

    /**
     * Get xmasClub value
     *
     * @return XmasClub|null
     */
    public function getXmasClub(): ?XmasClub
    {
        return $this->xmasClub;
    }

    /**
     * Set xmasClub value
     *
     * @param XmasClub $xmasClub
     * @return InsightAccountContainer
     */
    public function setXmasClub(?XmasClub $xmasClub = null): self
    {
        $this->xmasClub = $xmasClub;

        return $this;
    }

    /**
     * Get payDayOrShortTermLoan value
     *
     * @return PayDayOrShortTermLoan|null
     */
    public function getPayDayOrShortTermLoan(): ?PayDayOrShortTermLoan
    {
        return $this->payDayOrShortTermLoan;
    }

    /**
     * Set payDayOrShortTermLoan value
     *
     * @param PayDayOrShortTermLoan $payDayOrShortTermLoan
     * @return InsightAccountContainer
     */
    public function setPayDayOrShortTermLoan(?PayDayOrShortTermLoan $payDayOrShortTermLoan = null): self
    {
        $this->payDayOrShortTermLoan = $payDayOrShortTermLoan;

        return $this;
    }

    /**
     * Get buyToLetMortgage value
     *
     * @return BuyToLetMortgage|null
     */
    public function getBuyToLetMortgage(): ?BuyToLetMortgage
    {
        return $this->buyToLetMortgage;
    }

    /**
     * Set buyToLetMortgage value
     *
     * @param BuyToLetMortgage $buyToLetMortgage
     * @return InsightAccountContainer
     */
    public function setBuyToLetMortgage(?BuyToLetMortgage $buyToLetMortgage = null): self
    {
        $this->buyToLetMortgage = $buyToLetMortgage;

        return $this;
    }

    /**
     * Get propertyRental value
     *
     * @return PropertyRental|null
     */
    public function getPropertyRental(): ?PropertyRental
    {
        return $this->propertyRental;
    }

    /**
     * Set propertyRental value
     *
     * @param PropertyRental $propertyRental
     * @return InsightAccountContainer
     */
    public function setPropertyRental(?PropertyRental $propertyRental = null): self
    {
        $this->propertyRental = $propertyRental;

        return $this;
    }

    /**
     * Get basicBankAccount value
     *
     * @return BasicBankAccount|null
     */
    public function getBasicBankAccount(): ?BasicBankAccount
    {
        return $this->basicBankAccount;
    }

    /**
     * Set basicBankAccount value
     *
     * @param BasicBankAccount $basicBankAccount
     * @return InsightAccountContainer
     */
    public function setBasicBankAccount(?BasicBankAccount $basicBankAccount = null): self
    {
        $this->basicBankAccount = $basicBankAccount;

        return $this;
    }

    /**
     * Get bridgingFinance value
     *
     * @return BridgingFinance|null
     */
    public function getBridgingFinance(): ?BridgingFinance
    {
        return $this->bridgingFinance;
    }

    /**
     * Set bridgingFinance value
     *
     * @param BridgingFinance $bridgingFinance
     * @return InsightAccountContainer
     */
    public function setBridgingFinance(?BridgingFinance $bridgingFinance = null): self
    {
        $this->bridgingFinance = $bridgingFinance;

        return $this;
    }

    /**
     * Get greenDeals value
     *
     * @return GreenDeals|null
     */
    public function getGreenDeals(): ?GreenDeals
    {
        return $this->greenDeals;
    }

    /**
     * Set greenDeals value
     *
     * @param GreenDeals $greenDeals
     * @return InsightAccountContainer
     */
    public function setGreenDeals(?GreenDeals $greenDeals = null): self
    {
        $this->greenDeals = $greenDeals;

        return $this;
    }

    /**
     * Get socialHousingRental value
     *
     * @return SocialHousingRental|null
     */
    public function getSocialHousingRental(): ?SocialHousingRental
    {
        return $this->socialHousingRental;
    }

    /**
     * Set socialHousingRental value
     *
     * @param SocialHousingRental $socialHousingRental
     * @return InsightAccountContainer
     */
    public function setSocialHousingRental(?SocialHousingRental $socialHousingRental = null): self
    {
        $this->socialHousingRental = $socialHousingRental;

        return $this;
    }

    /**
     * Get localAuthorityHousing value
     *
     * @return LocalAuthorityHousing|null
     */
    public function getLocalAuthorityHousing(): ?LocalAuthorityHousing
    {
        return $this->localAuthorityHousing;
    }

    /**
     * Set localAuthorityHousing value
     *
     * @param LocalAuthorityHousing $localAuthorityHousing
     * @return InsightAccountContainer
     */
    public function setLocalAuthorityHousing(?LocalAuthorityHousing $localAuthorityHousing = null): self
    {
        $this->localAuthorityHousing = $localAuthorityHousing;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return InsightAccountContainer
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
