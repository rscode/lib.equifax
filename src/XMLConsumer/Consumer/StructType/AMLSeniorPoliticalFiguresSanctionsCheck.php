<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;

/**
 * This class stands for AMLSeniorPoliticalFiguresSanctionsCheck StructType
 * Meta information extracted from the WSDL
 * - documentation: The AML Senior Political Figures Sanctions check is simply a lookup of the Global Sanctions list
 *
 * @subpackage Structs
 */
class AMLSeniorPoliticalFiguresSanctionsCheck extends DataItem
{
    /**
     * The onSeniorPoliticalFiguresSanctions
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $onSeniorPoliticalFiguresSanctions;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for AMLSeniorPoliticalFiguresSanctionsCheck
     *
     * @param string                  $onSeniorPoliticalFiguresSanctions
     * @param DOMDocument|string|null $any
     * @uses AMLSeniorPoliticalFiguresSanctionsCheck::setOnSeniorPoliticalFiguresSanctions()
     * @uses AMLSeniorPoliticalFiguresSanctionsCheck::setAny()
     */
    public function __construct(string $onSeniorPoliticalFiguresSanctions, $any = null)
    {
        $this
            ->setOnSeniorPoliticalFiguresSanctions($onSeniorPoliticalFiguresSanctions)
            ->setAny($any);
    }

    /**
     * Get onSeniorPoliticalFiguresSanctions value
     *
     * @return string
     */
    public function getOnSeniorPoliticalFiguresSanctions(): string
    {
        return $this->onSeniorPoliticalFiguresSanctions;
    }

    /**
     * Set onSeniorPoliticalFiguresSanctions value
     *
     * @param string $onSeniorPoliticalFiguresSanctions
     * @return AMLSeniorPoliticalFiguresSanctionsCheck
     */
    public function setOnSeniorPoliticalFiguresSanctions(string $onSeniorPoliticalFiguresSanctions): self
    {
        // validation for constraint: string
        if (!is_null($onSeniorPoliticalFiguresSanctions) && !is_string($onSeniorPoliticalFiguresSanctions)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($onSeniorPoliticalFiguresSanctions, true),
                gettype($onSeniorPoliticalFiguresSanctions)
            ), __LINE__);
        }
        $this->onSeniorPoliticalFiguresSanctions = $onSeniorPoliticalFiguresSanctions;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return AMLSeniorPoliticalFiguresSanctionsCheck
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
