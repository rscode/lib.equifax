<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for StudentLoan StructType
 *
 * @subpackage Structs
 */
class StudentLoan extends FinancialAgreement
{
}
