<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;

/**
 * This class stands for AMLHMTreasurySanctionsCheck StructType
 *
 * @subpackage Structs
 */
class AMLHMTreasurySanctionsCheck extends DataItem
{
    /**
     * The onHMTreasurySanctions
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $onHMTreasurySanctions;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for AMLHMTreasurySanctionsCheck
     *
     * @param string                  $onHMTreasurySanctions
     * @param DOMDocument|string|null $any
     * @uses AMLHMTreasurySanctionsCheck::setOnHMTreasurySanctions()
     * @uses AMLHMTreasurySanctionsCheck::setAny()
     */
    public function __construct(string $onHMTreasurySanctions, $any = null)
    {
        $this
            ->setOnHMTreasurySanctions($onHMTreasurySanctions)
            ->setAny($any);
    }

    /**
     * Get onHMTreasurySanctions value
     *
     * @return string
     */
    public function getOnHMTreasurySanctions(): string
    {
        return $this->onHMTreasurySanctions;
    }

    /**
     * Set onHMTreasurySanctions value
     *
     * @param string $onHMTreasurySanctions
     * @return AMLHMTreasurySanctionsCheck
     */
    public function setOnHMTreasurySanctions(string $onHMTreasurySanctions): self
    {
        // validation for constraint: string
        if (!is_null($onHMTreasurySanctions) && !is_string($onHMTreasurySanctions)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($onHMTreasurySanctions, true),
                gettype($onHMTreasurySanctions)
            ), __LINE__);
        }
        $this->onHMTreasurySanctions = $onHMTreasurySanctions;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return AMLHMTreasurySanctionsCheck
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
