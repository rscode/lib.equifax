<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for ConsolidatedDebt StructType
 *
 * @subpackage Structs
 */
class ConsolidatedDebt extends FinancialAgreement
{
}
