<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for CreditSearchResponse StructType
 * Meta information extracted from the WSDL
 * - documentation: The Credit Search operation is the most common type of enquiry and models the standard credit enquiry and allows the return of characteristic values, scores, raw and coded data items as required, that either results in a credit
 * search footprint, or search type of SR, for initial enquiries, or SE for subsequent enquiries.
 *
 * @subpackage Structs
 */
class CreditSearchResponse extends CommonResponse
{
}
