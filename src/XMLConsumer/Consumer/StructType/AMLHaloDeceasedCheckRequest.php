<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for AMLHaloDeceasedCheckRequest StructType
 *
 * @subpackage Structs
 */
class AMLHaloDeceasedCheckRequest extends CodedDataRequest
{
}
