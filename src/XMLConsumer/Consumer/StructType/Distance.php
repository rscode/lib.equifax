<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\DistanceUnit;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Distance StructType
 *
 * @subpackage Structs
 */
class Distance extends AbstractStructBase
{
    /**
     * The distance
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 9999999
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $distance;
    /**
     * The unit
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $unit;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for Distance
     *
     * @param int                     $distance
     * @param string                  $unit
     * @param DOMDocument|string|null $any
     * @uses Distance::setDistance()
     * @uses Distance::setUnit()
     * @uses Distance::setAny()
     */
    public function __construct(int $distance, string $unit, $any = null)
    {
        $this
            ->setDistance($distance)
            ->setUnit($unit)
            ->setAny($any);
    }

    /**
     * Get distance value
     *
     * @return int
     */
    public function getDistance(): int
    {
        return $this->distance;
    }

    /**
     * Set distance value
     *
     * @param int $distance
     * @return Distance
     */
    public function setDistance(int $distance): self
    {
        // validation for constraint: int
        if (!is_null($distance) && !(is_int($distance) || ctype_digit($distance))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($distance, true),
                gettype($distance)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(9999999)
        if (!is_null($distance) && $distance > 9999999) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 9999999',
                var_export($distance, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($distance) && $distance < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($distance, true)
            ), __LINE__);
        }
        $this->distance = $distance;

        return $this;
    }

    /**
     * Get unit value
     *
     * @return string
     */
    public function getUnit(): string
    {
        return $this->unit;
    }

    /**
     * Set unit value
     *
     * @param string $unit
     * @return Distance
     * @throws InvalidArgumentException
     * @uses DistanceUnit::getValidValues
     * @uses DistanceUnit::valueIsValid
     */
    public function setUnit(string $unit): self
    {
        // validation for constraint: enumeration
        if (!DistanceUnit::valueIsValid($unit)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\DistanceUnit',
                is_array($unit) ? implode(', ', $unit) : var_export($unit, true),
                implode(', ', DistanceUnit::getValidValues())
            ), __LINE__);
        }
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return Distance
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
