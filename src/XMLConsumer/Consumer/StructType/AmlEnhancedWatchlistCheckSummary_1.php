<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AmlEnhancedWatchlistCheckSummary StructType
 *
 * @subpackage Structs
 */
class AmlEnhancedWatchlistCheckSummary_1 extends AbstractStructBase
{
    /**
     * The firstApplicant
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var AmlEnhancedWatchlistCheckSummaryData
     */
    protected AmlEnhancedWatchlistCheckSummaryData $firstApplicant;
    /**
     * The secondApplicant
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var AmlEnhancedWatchlistCheckSummaryData
     */
    protected AmlEnhancedWatchlistCheckSummaryData $secondApplicant;

    /**
     * Constructor method for AmlEnhancedWatchlistCheckSummary
     *
     * @param AmlEnhancedWatchlistCheckSummaryData $firstApplicant
     * @param AmlEnhancedWatchlistCheckSummaryData $secondApplicant
     * @uses AmlEnhancedWatchlistCheckSummary_1::setFirstApplicant()
     * @uses AmlEnhancedWatchlistCheckSummary_1::setSecondApplicant()
     */
    public function __construct(AmlEnhancedWatchlistCheckSummaryData $firstApplicant, AmlEnhancedWatchlistCheckSummaryData $secondApplicant)
    {
        $this
            ->setFirstApplicant($firstApplicant)
            ->setSecondApplicant($secondApplicant);
    }

    /**
     * Get firstApplicant value
     *
     * @return AmlEnhancedWatchlistCheckSummaryData
     */
    public function getFirstApplicant(): AmlEnhancedWatchlistCheckSummaryData
    {
        return $this->firstApplicant;
    }

    /**
     * Set firstApplicant value
     *
     * @param AmlEnhancedWatchlistCheckSummaryData $firstApplicant
     * @return AmlEnhancedWatchlistCheckSummary_1
     */
    public function setFirstApplicant(AmlEnhancedWatchlistCheckSummaryData $firstApplicant): self
    {
        $this->firstApplicant = $firstApplicant;

        return $this;
    }

    /**
     * Get secondApplicant value
     *
     * @return AmlEnhancedWatchlistCheckSummaryData
     */
    public function getSecondApplicant(): AmlEnhancedWatchlistCheckSummaryData
    {
        return $this->secondApplicant;
    }

    /**
     * Set secondApplicant value
     *
     * @param AmlEnhancedWatchlistCheckSummaryData $secondApplicant
     * @return AmlEnhancedWatchlistCheckSummary_1
     */
    public function setSecondApplicant(AmlEnhancedWatchlistCheckSummaryData $secondApplicant): self
    {
        $this->secondApplicant = $secondApplicant;

        return $this;
    }
}
