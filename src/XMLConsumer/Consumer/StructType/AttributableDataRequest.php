<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for AttributableDataRequest StructType
 *
 * @subpackage Structs
 */
class AttributableDataRequest extends RawDataAvailableAtLinkedAddressRequest
{
}
