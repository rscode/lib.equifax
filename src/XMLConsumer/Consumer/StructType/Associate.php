<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for Associate StructType
 * Meta information extracted from the WSDL
 * - documentation: Details of financial associates identified through the supply of bureau data (e.g. Insight joint accounts) and joint credit enquiries as well as those associations supplied directly by Equifax clients.
 *
 * @subpackage Structs
 */
class Associate extends NamedData
{
}
