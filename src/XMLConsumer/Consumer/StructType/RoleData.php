<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for RoleData StructType
 *
 * @subpackage Structs
 */
class RoleData extends AbstractStructBase
{
    /**
     * The roleType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $roleType = null;
    /**
     * The role
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     *
     * @var string[]
     */
    protected ?array $role = null;

    /**
     * Constructor method for RoleData
     *
     * @param string   $roleType
     * @param string[] $role
     * @uses RoleData::setRoleType()
     * @uses RoleData::setRole()
     */
    public function __construct(?string $roleType = null, ?array $role = null)
    {
        $this
            ->setRoleType($roleType)
            ->setRole($role);
    }

    /**
     * This method is responsible for validating the values passed to the setRole method
     * This method is willingly generated in order to preserve the one-line inline validation within the setRole method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateRoleForArrayConstraintsFromSetRole(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $roleDataRoleItem) {
            // validation for constraint: itemType
            if (!is_string($roleDataRoleItem)) {
                $invalidValues[] = is_object($roleDataRoleItem) ? get_class($roleDataRoleItem) : sprintf(
                    '%s(%s)',
                    gettype($roleDataRoleItem),
                    var_export($roleDataRoleItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The role property can only contain items of type string, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get roleType value
     *
     * @return string|null
     */
    public function getRoleType(): ?string
    {
        return $this->roleType;
    }

    /**
     * Set roleType value
     *
     * @param string $roleType
     * @return RoleData
     */
    public function setRoleType(?string $roleType = null): self
    {
        // validation for constraint: string
        if (!is_null($roleType) && !is_string($roleType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($roleType, true),
                gettype($roleType)
            ), __LINE__);
        }
        $this->roleType = $roleType;

        return $this;
    }

    /**
     * Get role value
     *
     * @return string[]
     */
    public function getRole(): ?array
    {
        return $this->role;
    }

    /**
     * Set role value
     *
     * @param string[] $role
     * @return RoleData
     * @throws InvalidArgumentException
     */
    public function setRole(?array $role = null): self
    {
        // validation for constraint: array
        if ('' !== ($roleArrayErrorMessage = self::validateRoleForArrayConstraintsFromSetRole($role))) {
            throw new InvalidArgumentException($roleArrayErrorMessage, __LINE__);
        }
        $this->role = $role;

        return $this;
    }

    /**
     * Add item to role value
     *
     * @param string $item
     * @return RoleData
     * @throws InvalidArgumentException
     */
    public function addToRole(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf(
                'The role property can only contain items of type string, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        $this->role[] = $item;

        return $this;
    }
}
