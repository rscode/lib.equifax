<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;

/**
 * This class stands for AssociateLenderName StructType
 * Meta information extracted from the WSDL
 * - documentation: Details of financial associates identified through the supply of bureau data (e.g. Insight joint accounts) and joint credit enquiries as well as those associations supplied directly by Equifax clients for lender names.
 *
 * @subpackage Structs
 */
class AssociateLenderName extends NamedData
{
    /**
     * The companyName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $companyName = null;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for AssociateLenderName
     *
     * @param string                  $companyName
     * @param DOMDocument|string|null $any
     * @uses AssociateLenderName::setCompanyName()
     * @uses AssociateLenderName::setAny()
     */
    public function __construct(?string $companyName = null, $any = null)
    {
        $this
            ->setCompanyName($companyName)
            ->setAny($any);
    }

    /**
     * Get companyName value
     *
     * @return string|null
     */
    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    /**
     * Set companyName value
     *
     * @param string $companyName
     * @return AssociateLenderName
     */
    public function setCompanyName(?string $companyName = null): self
    {
        // validation for constraint: string
        if (!is_null($companyName) && !is_string($companyName)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($companyName, true),
                gettype($companyName)
            ), __LINE__);
        }
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return AssociateLenderName
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
