<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for RawDataRequest StructType
 *
 * @subpackage Structs
 */
abstract class RawDataRequest extends DataRequest
{
    /**
     * The maxNumber
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 99
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $maxNumber;

    public function __construct(int $maxNumber)
    {
        $this
            ->setMaxNumber($maxNumber);
    }

    public function getMaxNumber(): int
    {
        return $this->maxNumber;
    }

    public function setMaxNumber(int $maxNumber): self
    {
        // validation for constraint: int
        if (!is_null($maxNumber) && !(is_int($maxNumber) || ctype_digit($maxNumber))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($maxNumber, true),
                gettype($maxNumber)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(99)
        if (!is_null($maxNumber) && $maxNumber > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 99',
                var_export($maxNumber, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($maxNumber) && $maxNumber < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($maxNumber, true)
            ), __LINE__);
        }
        $this->maxNumber = $maxNumber;

        return $this;
    }
}
