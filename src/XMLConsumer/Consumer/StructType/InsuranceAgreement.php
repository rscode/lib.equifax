<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for InsuranceAgreement StructType
 *
 * @subpackage Structs
 */
class InsuranceAgreement extends FinancialAgreement
{
    /**
     * The subType
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 2
     * - minLength: 0
     * - use: optional
     *
     * @var string|null
     */
    protected ?string $subType = null;

    /**
     * Constructor method for InsuranceAgreement
     *
     * @param string $subType
     * @uses InsuranceAgreement::setSubType()
     */
    public function __construct(?string $subType = null)
    {
        $this
            ->setSubType($subType);
    }

    /**
     * Get subType value
     *
     * @return string|null
     */
    public function getSubType(): ?string
    {
        return $this->subType;
    }

    /**
     * Set subType value
     *
     * @param string $subType
     * @return InsuranceAgreement
     */
    public function setSubType(?string $subType = null): self
    {
        // validation for constraint: string
        if (!is_null($subType) && !is_string($subType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($subType, true),
                gettype($subType)
            ), __LINE__);
        }
        // validation for constraint: maxLength(2)
        if (!is_null($subType) && mb_strlen((string)$subType) > 2) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 2',
                mb_strlen((string)$subType)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($subType) && mb_strlen((string)$subType) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$subType)
            ), __LINE__);
        }
        $this->subType = $subType;

        return $this;
    }
}
