<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for WatchlistDate StructType
 *
 * @subpackage Structs
 */
class WatchlistDate extends AbstractStructBase
{
    /**
     * The dateType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $dateType = null;
    /**
     * The date
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $date = null;

    /**
     * Constructor method for WatchlistDate
     *
     * @param string $dateType
     * @param string $date
     * @uses WatchlistDate::setDateType()
     * @uses WatchlistDate::setDate()
     */
    public function __construct(?string $dateType = null, ?string $date = null)
    {
        $this
            ->setDateType($dateType)
            ->setDate($date);
    }

    /**
     * Get dateType value
     *
     * @return string|null
     */
    public function getDateType(): ?string
    {
        return $this->dateType;
    }

    /**
     * Set dateType value
     *
     * @param string $dateType
     * @return WatchlistDate
     */
    public function setDateType(?string $dateType = null): self
    {
        // validation for constraint: string
        if (!is_null($dateType) && !is_string($dateType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($dateType, true),
                gettype($dateType)
            ), __LINE__);
        }
        $this->dateType = $dateType;

        return $this;
    }

    /**
     * Get date value
     *
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }

    /**
     * Set date value
     *
     * @param string $date
     * @return WatchlistDate
     */
    public function setDate(?string $date = null): self
    {
        // validation for constraint: string
        if (!is_null($date) && !is_string($date)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($date, true),
                gettype($date)
            ), __LINE__);
        }
        $this->date = $date;

        return $this;
    }
}
