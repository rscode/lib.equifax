<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;

/**
 * This class stands for SiranChecks StructType
 * Meta information extracted from the WSDL
 * - documentation: SIRAN is designed to help identify application fraud.
 *
 * @subpackage Structs
 */
class SiranChecks extends DataItem
{
    /**
     * The appsReferencedCount
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 9999
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $appsReferencedCount;
    /**
     * The rulesHitCount
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 999
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $rulesHitCount;
    /**
     * The siranRefNum
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 10
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $siranRefNum;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for SiranChecks
     *
     * @param int                     $appsReferencedCount
     * @param int                     $rulesHitCount
     * @param string                  $siranRefNum
     * @param DOMDocument|string|null $any
     * @uses SiranChecks::setAppsReferencedCount()
     * @uses SiranChecks::setRulesHitCount()
     * @uses SiranChecks::setSiranRefNum()
     * @uses SiranChecks::setAny()
     */
    public function __construct(int $appsReferencedCount, int $rulesHitCount, string $siranRefNum, $any = null)
    {
        $this
            ->setAppsReferencedCount($appsReferencedCount)
            ->setRulesHitCount($rulesHitCount)
            ->setSiranRefNum($siranRefNum)
            ->setAny($any);
    }

    /**
     * Get appsReferencedCount value
     *
     * @return int
     */
    public function getAppsReferencedCount(): int
    {
        return $this->appsReferencedCount;
    }

    /**
     * Set appsReferencedCount value
     *
     * @param int $appsReferencedCount
     * @return SiranChecks
     */
    public function setAppsReferencedCount(int $appsReferencedCount): self
    {
        // validation for constraint: int
        if (!is_null($appsReferencedCount) && !(is_int($appsReferencedCount) || ctype_digit($appsReferencedCount))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($appsReferencedCount, true),
                gettype($appsReferencedCount)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(9999)
        if (!is_null($appsReferencedCount) && $appsReferencedCount > 9999) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 9999',
                var_export($appsReferencedCount, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($appsReferencedCount) && $appsReferencedCount < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($appsReferencedCount, true)
            ), __LINE__);
        }
        $this->appsReferencedCount = $appsReferencedCount;

        return $this;
    }

    /**
     * Get rulesHitCount value
     *
     * @return int
     */
    public function getRulesHitCount(): int
    {
        return $this->rulesHitCount;
    }

    /**
     * Set rulesHitCount value
     *
     * @param int $rulesHitCount
     * @return SiranChecks
     */
    public function setRulesHitCount(int $rulesHitCount): self
    {
        // validation for constraint: int
        if (!is_null($rulesHitCount) && !(is_int($rulesHitCount) || ctype_digit($rulesHitCount))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($rulesHitCount, true),
                gettype($rulesHitCount)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(999)
        if (!is_null($rulesHitCount) && $rulesHitCount > 999) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 999',
                var_export($rulesHitCount, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($rulesHitCount) && $rulesHitCount < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($rulesHitCount, true)
            ), __LINE__);
        }
        $this->rulesHitCount = $rulesHitCount;

        return $this;
    }

    /**
     * Get siranRefNum value
     *
     * @return string
     */
    public function getSiranRefNum(): string
    {
        return $this->siranRefNum;
    }

    /**
     * Set siranRefNum value
     *
     * @param string $siranRefNum
     * @return SiranChecks
     */
    public function setSiranRefNum(string $siranRefNum): self
    {
        // validation for constraint: string
        if (!is_null($siranRefNum) && !is_string($siranRefNum)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($siranRefNum, true),
                gettype($siranRefNum)
            ), __LINE__);
        }
        // validation for constraint: maxLength(10)
        if (!is_null($siranRefNum) && mb_strlen((string)$siranRefNum) > 10) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 10',
                mb_strlen((string)$siranRefNum)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($siranRefNum) && mb_strlen((string)$siranRefNum) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$siranRefNum)
            ), __LINE__);
        }
        $this->siranRefNum = $siranRefNum;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return SiranChecks
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
