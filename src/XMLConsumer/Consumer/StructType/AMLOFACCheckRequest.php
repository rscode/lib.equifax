<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for AMLOFACCheckRequest StructType
 *
 * @subpackage Structs
 */
class AMLOFACCheckRequest extends CodedDataRequest
{
}
