<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FixedPaymentTerms StructType
 *
 * @subpackage Structs
 */
class FixedPaymentTerms extends AbstractStructBase
{
    /**
     * The numberOfPayments
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 999
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $numberOfPayments;
    /**
     * The paymentAmount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var MonetaryAmount
     */
    protected MonetaryAmount $paymentAmount;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for FixedPaymentTerms
     *
     * @param int                     $numberOfPayments
     * @param MonetaryAmount          $paymentAmount
     * @param DOMDocument|string|null $any
     * @uses FixedPaymentTerms::setNumberOfPayments()
     * @uses FixedPaymentTerms::setPaymentAmount()
     * @uses FixedPaymentTerms::setAny()
     */
    public function __construct(int $numberOfPayments, MonetaryAmount $paymentAmount, $any = null)
    {
        $this
            ->setNumberOfPayments($numberOfPayments)
            ->setPaymentAmount($paymentAmount)
            ->setAny($any);
    }

    /**
     * Get numberOfPayments value
     *
     * @return int
     */
    public function getNumberOfPayments(): int
    {
        return $this->numberOfPayments;
    }

    /**
     * Set numberOfPayments value
     *
     * @param int $numberOfPayments
     * @return FixedPaymentTerms
     */
    public function setNumberOfPayments(int $numberOfPayments): self
    {
        // validation for constraint: int
        if (!is_null($numberOfPayments) && !(is_int($numberOfPayments) || ctype_digit($numberOfPayments))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($numberOfPayments, true),
                gettype($numberOfPayments)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(999)
        if (!is_null($numberOfPayments) && $numberOfPayments > 999) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 999',
                var_export($numberOfPayments, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($numberOfPayments) && $numberOfPayments < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($numberOfPayments, true)
            ), __LINE__);
        }
        $this->numberOfPayments = $numberOfPayments;

        return $this;
    }

    /**
     * Get paymentAmount value
     *
     * @return MonetaryAmount
     */
    public function getPaymentAmount(): MonetaryAmount
    {
        return $this->paymentAmount;
    }

    /**
     * Set paymentAmount value
     *
     * @param MonetaryAmount $paymentAmount
     * @return FixedPaymentTerms
     */
    public function setPaymentAmount(MonetaryAmount $paymentAmount): self
    {
        $this->paymentAmount = $paymentAmount;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return FixedPaymentTerms
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
