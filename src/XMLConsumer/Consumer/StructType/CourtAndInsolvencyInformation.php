<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * Includes satisfied County and High Court Judgements, Bankruptcies, Voluntary Arrangements and other types of court action supplied by
 * Registry Trust and is updated on a daily basis.
 *
 * @see https://developer.equifax.co.uk/sites/uk/files/xml_ref_guide/ref/Data%20Groups/Court%20And%20Insolvency%20Information.pdf
 */
class CourtAndInsolvencyInformation extends NameMatchedData
{
    /**
     * The ccjType
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 3
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $ccjType;
    /**
     * The courtCode
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 3
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $courtCode;
    /**
     * The value
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var MonetaryAmount
     */
    protected MonetaryAmount $value;
    /**
     * The caseNumber
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 20
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $caseNumber = null;
    /**
     * The courtDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $courtDate = null;
    /**
     * The courtName
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $courtName = null;
    /**
     * The oldCaseNumber
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 20
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $oldCaseNumber = null;
    /**
     * The satisfiedDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $satisfiedDate = null;


    public function __construct(
        string $ccjType,
        string $courtCode,
        MonetaryAmount $value,
        ?string $caseNumber = null,
        ?string $courtDate = null,
        ?string $courtName = null,
        ?string $oldCaseNumber = null,
        ?string $satisfiedDate = null
    ) {
        $this
            ->setCcjType($ccjType)
            ->setCourtCode($courtCode)
            ->setValue($value)
            ->setCaseNumber($caseNumber)
            ->setCourtDate($courtDate)
            ->setCourtName($courtName)
            ->setOldCaseNumber($oldCaseNumber)
            ->setSatisfiedDate($satisfiedDate);
    }

    /**
     * Get ccjType value
     *
     * @return string
     */
    public function getCcjType(): string
    {
        return $this->ccjType;
    }

    /**
     * Set ccjType value
     *
     * @param string $ccjType
     * @return CourtAndInsolvencyInformation
     */
    public function setCcjType(string $ccjType): self
    {
        // validation for constraint: string
        if (!is_null($ccjType) && !is_string($ccjType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($ccjType, true),
                gettype($ccjType)
            ), __LINE__);
        }
        // validation for constraint: maxLength(3)
        if (!is_null($ccjType) && mb_strlen((string)$ccjType) > 3) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 3',
                mb_strlen((string)$ccjType)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($ccjType) && mb_strlen((string)$ccjType) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$ccjType)
            ), __LINE__);
        }
        $this->ccjType = $ccjType;

        return $this;
    }

    /**
     * Get courtCode value
     *
     * @return string
     */
    public function getCourtCode(): string
    {
        return $this->courtCode;
    }

    /**
     * Set courtCode value
     *
     * @param string $courtCode
     * @return CourtAndInsolvencyInformation
     */
    public function setCourtCode(string $courtCode): self
    {
        // validation for constraint: string
        if (!is_null($courtCode) && !is_string($courtCode)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($courtCode, true),
                gettype($courtCode)
            ), __LINE__);
        }
        // validation for constraint: maxLength(3)
        if (!is_null($courtCode) && mb_strlen((string)$courtCode) > 3) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 3',
                mb_strlen((string)$courtCode)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($courtCode) && mb_strlen((string)$courtCode) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$courtCode)
            ), __LINE__);
        }
        $this->courtCode = $courtCode;

        return $this;
    }

    /**
     * Get value value
     *
     * @return MonetaryAmount
     */
    public function getValue(): MonetaryAmount
    {
        return $this->value;
    }

    /**
     * Set value value
     *
     * @param MonetaryAmount $value
     * @return CourtAndInsolvencyInformation
     */
    public function setValue(MonetaryAmount $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get caseNumber value
     *
     * @return string|null
     */
    public function getCaseNumber(): ?string
    {
        return $this->caseNumber;
    }

    /**
     * Set caseNumber value
     *
     * @param string $caseNumber
     * @return CourtAndInsolvencyInformation
     */
    public function setCaseNumber(?string $caseNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($caseNumber) && !is_string($caseNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($caseNumber, true),
                gettype($caseNumber)
            ), __LINE__);
        }
        // validation for constraint: maxLength(20)
        if (!is_null($caseNumber) && mb_strlen((string)$caseNumber) > 20) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 20',
                mb_strlen((string)$caseNumber)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($caseNumber) && mb_strlen((string)$caseNumber) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$caseNumber)
            ), __LINE__);
        }
        $this->caseNumber = $caseNumber;

        return $this;
    }

    /**
     * Get courtDate value
     *
     * @return string|null
     */
    public function getCourtDate(): ?string
    {
        return $this->courtDate;
    }

    /**
     * Set courtDate value
     *
     * @param string $courtDate
     * @return CourtAndInsolvencyInformation
     */
    public function setCourtDate(?string $courtDate = null): self
    {
        // validation for constraint: string
        if (!is_null($courtDate) && !is_string($courtDate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($courtDate, true),
                gettype($courtDate)
            ), __LINE__);
        }
        $this->courtDate = $courtDate;

        return $this;
    }

    /**
     * Get courtName value
     *
     * @return string|null
     */
    public function getCourtName(): ?string
    {
        return $this->courtName;
    }

    /**
     * Set courtName value
     *
     * @param string $courtName
     * @return CourtAndInsolvencyInformation
     */
    public function setCourtName(?string $courtName = null): self
    {
        // validation for constraint: string
        if (!is_null($courtName) && !is_string($courtName)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($courtName, true),
                gettype($courtName)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($courtName) && mb_strlen((string)$courtName) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$courtName)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($courtName) && mb_strlen((string)$courtName) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$courtName)
            ), __LINE__);
        }
        $this->courtName = $courtName;

        return $this;
    }

    /**
     * Get oldCaseNumber value
     *
     * @return string|null
     */
    public function getOldCaseNumber(): ?string
    {
        return $this->oldCaseNumber;
    }

    /**
     * Set oldCaseNumber value
     *
     * @param string $oldCaseNumber
     * @return CourtAndInsolvencyInformation
     */
    public function setOldCaseNumber(?string $oldCaseNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($oldCaseNumber) && !is_string($oldCaseNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($oldCaseNumber, true),
                gettype($oldCaseNumber)
            ), __LINE__);
        }
        // validation for constraint: maxLength(20)
        if (!is_null($oldCaseNumber) && mb_strlen((string)$oldCaseNumber) > 20) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 20',
                mb_strlen((string)$oldCaseNumber)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($oldCaseNumber) && mb_strlen((string)$oldCaseNumber) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$oldCaseNumber)
            ), __LINE__);
        }
        $this->oldCaseNumber = $oldCaseNumber;

        return $this;
    }

    /**
     * Get satisfiedDate value
     *
     * @return string|null
     */
    public function getSatisfiedDate(): ?string
    {
        return $this->satisfiedDate;
    }

    /**
     * Set satisfiedDate value
     *
     * @param string $satisfiedDate
     * @return CourtAndInsolvencyInformation
     */
    public function setSatisfiedDate(?string $satisfiedDate = null): self
    {
        // validation for constraint: string
        if (!is_null($satisfiedDate) && !is_string($satisfiedDate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($satisfiedDate, true),
                gettype($satisfiedDate)
            ), __LINE__);
        }
        $this->satisfiedDate = $satisfiedDate;

        return $this;
    }
}
