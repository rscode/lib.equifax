<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for AMLOFACCheckContainer StructType
 *
 * @subpackage Structs
 */
class AMLOFACCheckContainer extends DataItemContainer
{
    /**
     * The amlOFACCheck
     * Meta information extracted from the WSDL
     * - maxOccurs: 2
     * - minOccurs: 0
     *
     * @var AMLOFACCheck[]
     */
    protected ?array $amlOFACCheck = null;

    /**
     * Constructor method for AMLOFACCheckContainer
     *
     * @param AMLOFACCheck[] $amlOFACCheck
     * @uses AMLOFACCheckContainer::setAmlOFACCheck()
     */
    public function __construct(?array $amlOFACCheck = null)
    {
        $this
            ->setAmlOFACCheck($amlOFACCheck);
    }

    /**
     * This method is responsible for validating the values passed to the setAmlOFACCheck method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAmlOFACCheck method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAmlOFACCheckForArrayConstraintsFromSetAmlOFACCheck(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $aMLOFACCheckContainerAmlOFACCheckItem) {
            // validation for constraint: itemType
            if (!$aMLOFACCheckContainerAmlOFACCheckItem instanceof AMLOFACCheck) {
                $invalidValues[] = is_object($aMLOFACCheckContainerAmlOFACCheckItem) ? get_class($aMLOFACCheckContainerAmlOFACCheckItem) : sprintf(
                    '%s(%s)',
                    gettype($aMLOFACCheckContainerAmlOFACCheckItem),
                    var_export($aMLOFACCheckContainerAmlOFACCheckItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The amlOFACCheck property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AMLOFACCheck, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get amlOFACCheck value
     *
     * @return AMLOFACCheck[]
     */
    public function getAmlOFACCheck(): ?array
    {
        return $this->amlOFACCheck;
    }

    /**
     * Set amlOFACCheck value
     *
     * @param AMLOFACCheck[] $amlOFACCheck
     * @return AMLOFACCheckContainer
     * @throws InvalidArgumentException
     */
    public function setAmlOFACCheck(?array $amlOFACCheck = null): self
    {
        // validation for constraint: array
        if ('' !== ($amlOFACCheckArrayErrorMessage = self::validateAmlOFACCheckForArrayConstraintsFromSetAmlOFACCheck($amlOFACCheck))) {
            throw new InvalidArgumentException($amlOFACCheckArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(2)
        if (is_array($amlOFACCheck) && count($amlOFACCheck) > 2) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 2',
                count($amlOFACCheck)
            ), __LINE__);
        }
        $this->amlOFACCheck = $amlOFACCheck;

        return $this;
    }

    /**
     * Add item to amlOFACCheck value
     *
     * @param AMLOFACCheck $item
     * @return AMLOFACCheckContainer
     * @throws InvalidArgumentException
     */
    public function addToAmlOFACCheck(AMLOFACCheck $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof AMLOFACCheck) {
            throw new InvalidArgumentException(sprintf(
                'The amlOFACCheck property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AMLOFACCheck, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(2)
        if (is_array($this->amlOFACCheck) && count($this->amlOFACCheck) >= 2) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 2',
                count($this->amlOFACCheck)
            ), __LINE__);
        }
        $this->amlOFACCheck[] = $item;

        return $this;
    }
}
