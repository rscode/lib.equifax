<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for HomeTelephoneCheckExtendedDataRequest StructType
 *
 * @subpackage Structs
 */
class HomeTelephoneCheckExtendedDataRequest extends CodedDataRequest
{
}
