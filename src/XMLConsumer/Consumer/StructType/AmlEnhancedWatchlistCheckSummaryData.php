<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AmlEnhancedWatchlistCheckSummaryData StructType
 *
 * @subpackage Structs
 */
class AmlEnhancedWatchlistCheckSummaryData extends AbstractStructBase
{
    /**
     * The highRiskRca
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $highRiskRca;
    /**
     * The standardRiskRca
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $standardRiskRca;
    /**
     * The sanctions
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $sanctions = null;
    /**
     * The highRiskPep
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $highRiskPep = null;
    /**
     * The standardRiskPep
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $standardRiskPep = null;
    /**
     * The highRiskSip
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $highRiskSip = null;
    /**
     * The standardRiskSip
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $standardRiskSip = null;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for AmlEnhancedWatchlistCheckSummaryData
     *
     * @param string $highRiskRca
     * @param string $standardRiskRca
     * @param string $sanctions
     * @param string $highRiskPep
     * @param string $standardRiskPep
     * @param string $highRiskSip
     * @param string $standardRiskSip
     * @param DOMDocument|string|null $any
     * @uses AmlEnhancedWatchlistCheckSummaryData::setHighRiskRca()
     * @uses AmlEnhancedWatchlistCheckSummaryData::setStandardRiskRca()
     * @uses AmlEnhancedWatchlistCheckSummaryData::setSanctions()
     * @uses AmlEnhancedWatchlistCheckSummaryData::setHighRiskPep()
     * @uses AmlEnhancedWatchlistCheckSummaryData::setStandardRiskPep()
     * @uses AmlEnhancedWatchlistCheckSummaryData::setHighRiskSip()
     * @uses AmlEnhancedWatchlistCheckSummaryData::setStandardRiskSip()
     * @uses AmlEnhancedWatchlistCheckSummaryData::setAny()
     */
    public function __construct(
        string $highRiskRca,
        string $standardRiskRca,
        ?string $sanctions = null,
        ?string $highRiskPep = null,
        ?string $standardRiskPep = null,
        ?string $highRiskSip = null,
        ?string $standardRiskSip = null,
        $any = null
    ) {
        $this
            ->setHighRiskRca($highRiskRca)
            ->setStandardRiskRca($standardRiskRca)
            ->setSanctions($sanctions)
            ->setHighRiskPep($highRiskPep)
            ->setStandardRiskPep($standardRiskPep)
            ->setHighRiskSip($highRiskSip)
            ->setStandardRiskSip($standardRiskSip)
            ->setAny($any);
    }

    /**
     * Get highRiskRca value
     *
     * @return string
     */
    public function getHighRiskRca(): string
    {
        return $this->highRiskRca;
    }

    /**
     * Set highRiskRca value
     *
     * @param string $highRiskRca
     * @return AmlEnhancedWatchlistCheckSummaryData
     */
    public function setHighRiskRca(string $highRiskRca): self
    {
        // validation for constraint: string
        if (!is_null($highRiskRca) && !is_string($highRiskRca)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($highRiskRca, true),
                gettype($highRiskRca)
            ), __LINE__);
        }
        $this->highRiskRca = $highRiskRca;

        return $this;
    }

    /**
     * Get standardRiskRca value
     *
     * @return string
     */
    public function getStandardRiskRca(): string
    {
        return $this->standardRiskRca;
    }

    /**
     * Set standardRiskRca value
     *
     * @param string $standardRiskRca
     * @return AmlEnhancedWatchlistCheckSummaryData
     */
    public function setStandardRiskRca(string $standardRiskRca): self
    {
        // validation for constraint: string
        if (!is_null($standardRiskRca) && !is_string($standardRiskRca)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($standardRiskRca, true),
                gettype($standardRiskRca)
            ), __LINE__);
        }
        $this->standardRiskRca = $standardRiskRca;

        return $this;
    }

    /**
     * Get sanctions value
     *
     * @return string|null
     */
    public function getSanctions(): ?string
    {
        return $this->sanctions;
    }

    /**
     * Set sanctions value
     *
     * @param string $sanctions
     * @return AmlEnhancedWatchlistCheckSummaryData
     */
    public function setSanctions(?string $sanctions = null): self
    {
        // validation for constraint: string
        if (!is_null($sanctions) && !is_string($sanctions)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($sanctions, true),
                gettype($sanctions)
            ), __LINE__);
        }
        $this->sanctions = $sanctions;

        return $this;
    }

    /**
     * Get highRiskPep value
     *
     * @return string|null
     */
    public function getHighRiskPep(): ?string
    {
        return $this->highRiskPep;
    }

    /**
     * Set highRiskPep value
     *
     * @param string $highRiskPep
     * @return AmlEnhancedWatchlistCheckSummaryData
     */
    public function setHighRiskPep(?string $highRiskPep = null): self
    {
        // validation for constraint: string
        if (!is_null($highRiskPep) && !is_string($highRiskPep)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($highRiskPep, true),
                gettype($highRiskPep)
            ), __LINE__);
        }
        $this->highRiskPep = $highRiskPep;

        return $this;
    }

    /**
     * Get standardRiskPep value
     *
     * @return string|null
     */
    public function getStandardRiskPep(): ?string
    {
        return $this->standardRiskPep;
    }

    /**
     * Set standardRiskPep value
     *
     * @param string $standardRiskPep
     * @return AmlEnhancedWatchlistCheckSummaryData
     */
    public function setStandardRiskPep(?string $standardRiskPep = null): self
    {
        // validation for constraint: string
        if (!is_null($standardRiskPep) && !is_string($standardRiskPep)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($standardRiskPep, true),
                gettype($standardRiskPep)
            ), __LINE__);
        }
        $this->standardRiskPep = $standardRiskPep;

        return $this;
    }

    /**
     * Get highRiskSip value
     *
     * @return string|null
     */
    public function getHighRiskSip(): ?string
    {
        return $this->highRiskSip;
    }

    /**
     * Set highRiskSip value
     *
     * @param string $highRiskSip
     * @return AmlEnhancedWatchlistCheckSummaryData
     */
    public function setHighRiskSip(?string $highRiskSip = null): self
    {
        // validation for constraint: string
        if (!is_null($highRiskSip) && !is_string($highRiskSip)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($highRiskSip, true),
                gettype($highRiskSip)
            ), __LINE__);
        }
        $this->highRiskSip = $highRiskSip;

        return $this;
    }

    /**
     * Get standardRiskSip value
     *
     * @return string|null
     */
    public function getStandardRiskSip(): ?string
    {
        return $this->standardRiskSip;
    }

    /**
     * Set standardRiskSip value
     *
     * @param string $standardRiskSip
     * @return AmlEnhancedWatchlistCheckSummaryData
     */
    public function setStandardRiskSip(?string $standardRiskSip = null): self
    {
        // validation for constraint: string
        if (!is_null($standardRiskSip) && !is_string($standardRiskSip)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($standardRiskSip, true),
                gettype($standardRiskSip)
            ), __LINE__);
        }
        $this->standardRiskSip = $standardRiskSip;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return AmlEnhancedWatchlistCheckSummaryData
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
