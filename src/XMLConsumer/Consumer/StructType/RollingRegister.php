<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\RRRecordType;

/**
 * This class stands for RollingRegister StructType
 * Meta information extracted from the WSDL
 * - documentation: Changes to the Electoral Roll on a monthly basis.
 *
 * @subpackage Structs
 */
class RollingRegister extends ElectoralRollBase
{
    /**
     * The recordType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $recordType;
    /**
     * The supplyDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * - union: date | gYear | gYearMonth | MMYY | MMYYYY
     *
     * @var string
     */
    protected string $supplyDate;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for RollingRegister
     *
     * @param string                  $recordType
     * @param string                  $supplyDate
     * @param DOMDocument|string|null $any
     * @uses RollingRegister::setRecordType()
     * @uses RollingRegister::setSupplyDate()
     * @uses RollingRegister::setAny()
     */
    public function __construct(string $recordType, string $supplyDate, $any = null)
    {
        $this
            ->setRecordType($recordType)
            ->setSupplyDate($supplyDate)
            ->setAny($any);
    }

    /**
     * This method is responsible for validating the value passed to the setSupplyDate method
     * This method is willingly generated in order to preserve the one-line inline validation within the setSupplyDate method
     * This is a set of validation rules based on the union types associated to the property being set by the setSupplyDate method
     *
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateSupplyDateForUnionConstraintsFromSetSupplyDate($value): string
    {
        $message = '';
        // validation for constraint: int
        if (!is_null($value) && !(is_int($value) || ctype_digit($value))) {
            $exception0 = new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($value, true),
                gettype($value)
            ), __LINE__);
        }
        // validation for constraint: pattern((0[123456789]|1[012]){1}\d{2})
        if (!is_null($value) && !preg_match('/(0[123456789]|1[012]){1}\\d{2}/', $value)) {
            $exception1 = new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /(0[123456789]|1[012]){1}\\d{2}/',
                var_export($value, true)
            ), __LINE__);
        }
        // validation for constraint: pattern((0[123456789]|1[012]){1}\d{4})
        if (!is_null($value) && !preg_match('/(0[123456789]|1[012]){1}\\d{4}/', $value)) {
            $exception2 = new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /(0[123456789]|1[012]){1}\\d{4}/',
                var_export($value, true)
            ), __LINE__);
        }
        if (isset($exception0) && isset($exception1) && isset($exception2)) {
            $message = sprintf(
                "The value %s does not match any of the union rules: date, gYear, gYearMonth, MMYY, MMYYYY. See following errors:\n%s",
                var_export($value, true),
                implode("\n", array_map(function (InvalidArgumentException $e) {
                    return sprintf(' - %s', $e->getMessage());
                },
                [$exception0, $exception1, $exception2]))
            );
        }
        unset($exception0, $exception1, $exception2);

        return $message;
    }

    /**
     * Get recordType value
     *
     * @return string
     */
    public function getRecordType(): string
    {
        return $this->recordType;
    }

    /**
     * Set recordType value
     *
     * @param string $recordType
     * @return RollingRegister
     * @throws InvalidArgumentException
     * @uses RRRecordType::getValidValues
     * @uses RRRecordType::valueIsValid
     */
    public function setRecordType(string $recordType): self
    {
        // validation for constraint: enumeration
        if (!RRRecordType::valueIsValid($recordType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\RRRecordType',
                is_array($recordType) ? implode(', ', $recordType) : var_export($recordType, true),
                implode(', ', RRRecordType::getValidValues())
            ), __LINE__);
        }
        $this->recordType = $recordType;

        return $this;
    }

    /**
     * Get supplyDate value
     *
     * @return string
     */
    public function getSupplyDate(): string
    {
        return $this->supplyDate;
    }

    /**
     * Set supplyDate value
     *
     * @param string $supplyDate
     * @return RollingRegister
     */
    public function setSupplyDate(string $supplyDate): self
    {
        // validation for constraint: string
        if (!is_null($supplyDate) && !is_string($supplyDate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($supplyDate, true),
                gettype($supplyDate)
            ), __LINE__);
        }
        // validation for constraint: union(date, gYear, gYearMonth, MMYY, MMYYYY)
        if ('' !== ($supplyDateUnionErrorMessage = self::validateSupplyDateForUnionConstraintsFromSetSupplyDate($supplyDate))) {
            throw new InvalidArgumentException($supplyDateUnionErrorMessage, __LINE__);
        }
        $this->supplyDate = $supplyDate;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return RollingRegister
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
