<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for ConsumerAddress StructType
 *
 * @subpackage Structs
 */
class ConsumerAddress extends DataItem
{
}
