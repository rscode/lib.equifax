<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\EmailAddressType;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for EmailAddress StructType
 *
 * @subpackage Structs
 */
class EmailAddress extends AbstractStructBase
{
    /**
     * The emailAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $emailAddress;
    /**
     * The type
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $type;

    /**
     * Constructor method for EmailAddress
     *
     * @param string $emailAddress
     * @param string $type
     * @uses EmailAddress::setEmailAddress()
     * @uses EmailAddress::setType()
     */
    public function __construct(string $emailAddress, string $type)
    {
        $this
            ->setEmailAddress($emailAddress)
            ->setType($type);
    }

    /**
     * Get emailAddress value
     *
     * @return string
     */
    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }

    /**
     * Set emailAddress value
     *
     * @param string $emailAddress
     * @return EmailAddress
     */
    public function setEmailAddress(string $emailAddress): self
    {
        // validation for constraint: string
        if (!is_null($emailAddress) && !is_string($emailAddress)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($emailAddress, true),
                gettype($emailAddress)
            ), __LINE__);
        }
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * Get type value
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Set type value
     *
     * @param string $type
     * @return EmailAddress
     * @throws InvalidArgumentException
     * @uses EmailAddressType::getValidValues
     * @uses EmailAddressType::valueIsValid
     */
    public function setType(string $type): self
    {
        // validation for constraint: enumeration
        if (!EmailAddressType::valueIsValid($type)) {
            throw new InvalidArgumentException(
                sprintf(
                    'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\EmailAddressType',
                    is_array($type) ? implode(', ', $type) : var_export($type, true),
                    implode(', ', EmailAddressType::getValidValues())
                ),
                __LINE__
            );
        }
        $this->type = $type;

        return $this;
    }
}
