<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for FraudProfileSummaryContainer StructType
 *
 * @subpackage Structs
 */
class FraudProfileSummaryContainer extends DataItemContainer
{
    /**
     * The fraudProfileSummary
     * Meta information extracted from the WSDL
     * - maxOccurs: 2
     * - minOccurs: 0
     *
     * @var FraudProfileSummary[]
     */
    protected ?array $fraudProfileSummary = null;

    /**
     * Constructor method for FraudProfileSummaryContainer
     *
     * @param FraudProfileSummary[] $fraudProfileSummary
     * @uses FraudProfileSummaryContainer::setFraudProfileSummary()
     */
    public function __construct(?array $fraudProfileSummary = null)
    {
        $this
            ->setFraudProfileSummary($fraudProfileSummary);
    }

    /**
     * This method is responsible for validating the values passed to the setFraudProfileSummary method
     * This method is willingly generated in order to preserve the one-line inline validation within the setFraudProfileSummary method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateFraudProfileSummaryForArrayConstraintsFromSetFraudProfileSummary(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $fraudProfileSummaryContainerFraudProfileSummaryItem) {
            // validation for constraint: itemType
            if (!$fraudProfileSummaryContainerFraudProfileSummaryItem instanceof FraudProfileSummary) {
                $invalidValues[] = is_object($fraudProfileSummaryContainerFraudProfileSummaryItem) ? get_class($fraudProfileSummaryContainerFraudProfileSummaryItem) : sprintf(
                    '%s(%s)',
                    gettype($fraudProfileSummaryContainerFraudProfileSummaryItem),
                    var_export($fraudProfileSummaryContainerFraudProfileSummaryItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The fraudProfileSummary property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\FraudProfileSummary, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get fraudProfileSummary value
     *
     * @return FraudProfileSummary[]
     */
    public function getFraudProfileSummary(): ?array
    {
        return $this->fraudProfileSummary;
    }

    /**
     * Set fraudProfileSummary value
     *
     * @param FraudProfileSummary[] $fraudProfileSummary
     * @return FraudProfileSummaryContainer
     * @throws InvalidArgumentException
     */
    public function setFraudProfileSummary(?array $fraudProfileSummary = null): self
    {
        // validation for constraint: array
        if ('' !== ($fraudProfileSummaryArrayErrorMessage = self::validateFraudProfileSummaryForArrayConstraintsFromSetFraudProfileSummary($fraudProfileSummary))) {
            throw new InvalidArgumentException($fraudProfileSummaryArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(2)
        if (is_array($fraudProfileSummary) && count($fraudProfileSummary) > 2) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 2',
                count($fraudProfileSummary)
            ), __LINE__);
        }
        $this->fraudProfileSummary = $fraudProfileSummary;

        return $this;
    }

    /**
     * Add item to fraudProfileSummary value
     *
     * @param FraudProfileSummary $item
     * @return FraudProfileSummaryContainer
     * @throws InvalidArgumentException
     */
    public function addToFraudProfileSummary(FraudProfileSummary $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof FraudProfileSummary) {
            throw new InvalidArgumentException(sprintf(
                'The fraudProfileSummary property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\FraudProfileSummary, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(2)
        if (is_array($this->fraudProfileSummary) && count($this->fraudProfileSummary) >= 2) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 2',
                count($this->fraudProfileSummary)
            ), __LINE__);
        }
        $this->fraudProfileSummary[] = $item;

        return $this;
    }
}
