<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for InsightHistoryRequest StructType
 *
 * @subpackage Structs
 */
class InsightHistoryRequest extends RawDataAvailableAtLinkedAddressRequest
{
}
