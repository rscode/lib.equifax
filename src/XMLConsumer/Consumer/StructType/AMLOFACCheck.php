<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;

/**
 * This class stands for AMLOFACCheck StructType
 * Meta information extracted from the WSDL
 * - documentation: A match against an individual listed on Office of Foreign Asset Control (OFAC).
 *
 * @subpackage Structs
 */
class AMLOFACCheck extends DataItem
{
    /**
     * The onOFAC
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $onOFAC;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for AMLOFACCheck
     *
     * @param string                  $onOFAC
     * @param DOMDocument|string|null $any
     * @uses AMLOFACCheck::setOnOFAC()
     * @uses AMLOFACCheck::setAny()
     */
    public function __construct(string $onOFAC, $any = null)
    {
        $this
            ->setOnOFAC($onOFAC)
            ->setAny($any);
    }

    /**
     * Get onOFAC value
     *
     * @return string
     */
    public function getOnOFAC(): string
    {
        return $this->onOFAC;
    }

    /**
     * Set onOFAC value
     *
     * @param string $onOFAC
     * @return AMLOFACCheck
     */
    public function setOnOFAC(string $onOFAC): self
    {
        // validation for constraint: string
        if (!is_null($onOFAC) && !is_string($onOFAC)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($onOFAC, true),
                gettype($onOFAC)
            ), __LINE__);
        }
        $this->onOFAC = $onOFAC;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return AMLOFACCheck
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
