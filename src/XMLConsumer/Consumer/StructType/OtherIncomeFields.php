<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OtherIncomeFields StructType
 *
 * @subpackage Structs
 */
class OtherIncomeFields extends AbstractStructBase
{
    /**
     * The ApplicantFrequencyOfSalary
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ApplicantFrequencyOfSalary = null;
    /**
     * The IncomeLikelyToReduce
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $IncomeLikelyToReduce = null;
    /**
     * The NetMonthlyIncomeFields
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var NetMonthlyIncomeFields|null
     */
    protected ?NetMonthlyIncomeFields $NetMonthlyIncomeFields = null;
    /**
     * The GrossAnnualIncomeFields
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var GrossAnnualIncomeFields|null
     */
    protected ?GrossAnnualIncomeFields $GrossAnnualIncomeFields = null;

    /**
     * Constructor method for OtherIncomeFields
     *
     * @param string                  $applicantFrequencyOfSalary
     * @param string                  $incomeLikelyToReduce
     * @param NetMonthlyIncomeFields  $netMonthlyIncomeFields
     * @param GrossAnnualIncomeFields $grossAnnualIncomeFields
     * @uses OtherIncomeFields::setApplicantFrequencyOfSalary()
     * @uses OtherIncomeFields::setIncomeLikelyToReduce()
     * @uses OtherIncomeFields::setNetMonthlyIncomeFields()
     * @uses OtherIncomeFields::setGrossAnnualIncomeFields()
     */
    public function __construct(
        ?string $applicantFrequencyOfSalary = null,
        ?string $incomeLikelyToReduce = null,
        ?NetMonthlyIncomeFields $netMonthlyIncomeFields = null,
        ?GrossAnnualIncomeFields $grossAnnualIncomeFields = null
    ) {
        $this
            ->setApplicantFrequencyOfSalary($applicantFrequencyOfSalary)
            ->setIncomeLikelyToReduce($incomeLikelyToReduce)
            ->setNetMonthlyIncomeFields($netMonthlyIncomeFields)
            ->setGrossAnnualIncomeFields($grossAnnualIncomeFields);
    }

    /**
     * Get ApplicantFrequencyOfSalary value
     *
     * @return string|null
     */
    public function getApplicantFrequencyOfSalary(): ?string
    {
        return $this->ApplicantFrequencyOfSalary;
    }

    /**
     * Set ApplicantFrequencyOfSalary value
     *
     * @param string $applicantFrequencyOfSalary
     * @return OtherIncomeFields
     */
    public function setApplicantFrequencyOfSalary(?string $applicantFrequencyOfSalary = null): self
    {
        // validation for constraint: string
        if (!is_null($applicantFrequencyOfSalary) && !is_string($applicantFrequencyOfSalary)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($applicantFrequencyOfSalary, true),
                gettype($applicantFrequencyOfSalary)
            ), __LINE__);
        }
        $this->ApplicantFrequencyOfSalary = $applicantFrequencyOfSalary;

        return $this;
    }

    /**
     * Get IncomeLikelyToReduce value
     *
     * @return string|null
     */
    public function getIncomeLikelyToReduce(): ?string
    {
        return $this->IncomeLikelyToReduce;
    }

    /**
     * Set IncomeLikelyToReduce value
     *
     * @param string $incomeLikelyToReduce
     * @return OtherIncomeFields
     */
    public function setIncomeLikelyToReduce(?string $incomeLikelyToReduce = null): self
    {
        // validation for constraint: string
        if (!is_null($incomeLikelyToReduce) && !is_string($incomeLikelyToReduce)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($incomeLikelyToReduce, true),
                gettype($incomeLikelyToReduce)
            ), __LINE__);
        }
        $this->IncomeLikelyToReduce = $incomeLikelyToReduce;

        return $this;
    }

    /**
     * Get NetMonthlyIncomeFields value
     *
     * @return NetMonthlyIncomeFields|null
     */
    public function getNetMonthlyIncomeFields(): ?NetMonthlyIncomeFields
    {
        return $this->NetMonthlyIncomeFields;
    }

    /**
     * Set NetMonthlyIncomeFields value
     *
     * @param NetMonthlyIncomeFields $netMonthlyIncomeFields
     * @return OtherIncomeFields
     */
    public function setNetMonthlyIncomeFields(?NetMonthlyIncomeFields $netMonthlyIncomeFields = null): self
    {
        $this->NetMonthlyIncomeFields = $netMonthlyIncomeFields;

        return $this;
    }

    /**
     * Get GrossAnnualIncomeFields value
     *
     * @return GrossAnnualIncomeFields|null
     */
    public function getGrossAnnualIncomeFields(): ?GrossAnnualIncomeFields
    {
        return $this->GrossAnnualIncomeFields;
    }

    /**
     * Set GrossAnnualIncomeFields value
     *
     * @param GrossAnnualIncomeFields $grossAnnualIncomeFields
     * @return OtherIncomeFields
     */
    public function setGrossAnnualIncomeFields(?GrossAnnualIncomeFields $grossAnnualIncomeFields = null): self
    {
        $this->GrossAnnualIncomeFields = $grossAnnualIncomeFields;

        return $this;
    }
}
