<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for InsightAccountCounts StructType
 *
 * @subpackage Structs
 */
class InsightAccountCounts extends AbstractStructBase
{
    /**
     * The companyGroupCount
     * Meta information extracted from the WSDL
     * - maxOccurs: 6
     * - minOccurs: 6
     *
     * @var CompanyGroupCount[]
     */
    protected array $companyGroupCount;
    /**
     * The numberOfAccounts
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 99
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $numberOfAccounts;
    /**
     * The updatedLast1MonthCount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AccountUpdateCount|null
     */
    protected ?AccountUpdateCount $updatedLast1MonthCount = null;
    /**
     * The updatedLast3MonthsCount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AccountUpdateCount|null
     */
    protected ?AccountUpdateCount $updatedLast3MonthsCount = null;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for InsightAccountCounts
     *
     * @param CompanyGroupCount[]     $companyGroupCount
     * @param int                     $numberOfAccounts
     * @param AccountUpdateCount      $updatedLast1MonthCount
     * @param AccountUpdateCount      $updatedLast3MonthsCount
     * @param DOMDocument|string|null $any
     * @uses InsightAccountCounts::setCompanyGroupCount()
     * @uses InsightAccountCounts::setNumberOfAccounts()
     * @uses InsightAccountCounts::setUpdatedLast1MonthCount()
     * @uses InsightAccountCounts::setUpdatedLast3MonthsCount()
     * @uses InsightAccountCounts::setAny()
     */
    public function __construct(
        array $companyGroupCount,
        int $numberOfAccounts,
        ?AccountUpdateCount $updatedLast1MonthCount = null,
        ?AccountUpdateCount $updatedLast3MonthsCount = null,
        $any = null
    ) {
        $this
            ->setCompanyGroupCount($companyGroupCount)
            ->setNumberOfAccounts($numberOfAccounts)
            ->setUpdatedLast1MonthCount($updatedLast1MonthCount)
            ->setUpdatedLast3MonthsCount($updatedLast3MonthsCount)
            ->setAny($any);
    }

    /**
     * This method is responsible for validating the values passed to the setCompanyGroupCount method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCompanyGroupCount method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCompanyGroupCountForArrayConstraintsFromSetCompanyGroupCount(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $insightAccountCountsCompanyGroupCountItem) {
            // validation for constraint: itemType
            if (!$insightAccountCountsCompanyGroupCountItem instanceof CompanyGroupCount) {
                $invalidValues[] = is_object($insightAccountCountsCompanyGroupCountItem) ? get_class($insightAccountCountsCompanyGroupCountItem) : sprintf(
                    '%s(%s)',
                    gettype($insightAccountCountsCompanyGroupCountItem),
                    var_export($insightAccountCountsCompanyGroupCountItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The companyGroupCount property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\CompanyGroupCount, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get companyGroupCount value
     *
     * @return CompanyGroupCount[]
     */
    public function getCompanyGroupCount(): array
    {
        return $this->companyGroupCount;
    }

    /**
     * Set companyGroupCount value
     *
     * @param CompanyGroupCount[] $companyGroupCount
     * @return InsightAccountCounts
     * @throws InvalidArgumentException
     */
    public function setCompanyGroupCount(array $companyGroupCount): self
    {
        // validation for constraint: array
        if ('' !== ($companyGroupCountArrayErrorMessage = self::validateCompanyGroupCountForArrayConstraintsFromSetCompanyGroupCount($companyGroupCount))) {
            throw new InvalidArgumentException($companyGroupCountArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(6)
        if (is_array($companyGroupCount) && count($companyGroupCount) > 6) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 6',
                count($companyGroupCount)
            ), __LINE__);
        }
        $this->companyGroupCount = $companyGroupCount;

        return $this;
    }

    /**
     * Add item to companyGroupCount value
     *
     * @param CompanyGroupCount $item
     * @return InsightAccountCounts
     * @throws InvalidArgumentException
     */
    public function addToCompanyGroupCount(CompanyGroupCount $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof CompanyGroupCount) {
            throw new InvalidArgumentException(sprintf(
                'The companyGroupCount property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\CompanyGroupCount, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(6)
        if (is_array($this->companyGroupCount) && count($this->companyGroupCount) >= 6) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 6',
                count($this->companyGroupCount)
            ), __LINE__);
        }
        $this->companyGroupCount[] = $item;

        return $this;
    }

    /**
     * Get numberOfAccounts value
     *
     * @return int
     */
    public function getNumberOfAccounts(): int
    {
        return $this->numberOfAccounts;
    }

    /**
     * Set numberOfAccounts value
     *
     * @param int $numberOfAccounts
     * @return InsightAccountCounts
     */
    public function setNumberOfAccounts(int $numberOfAccounts): self
    {
        // validation for constraint: int
        if (!is_null($numberOfAccounts) && !(is_int($numberOfAccounts) || ctype_digit($numberOfAccounts))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($numberOfAccounts, true),
                gettype($numberOfAccounts)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(99)
        if (!is_null($numberOfAccounts) && $numberOfAccounts > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 99',
                var_export($numberOfAccounts, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($numberOfAccounts) && $numberOfAccounts < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($numberOfAccounts, true)
            ), __LINE__);
        }
        $this->numberOfAccounts = $numberOfAccounts;

        return $this;
    }

    /**
     * Get updatedLast1MonthCount value
     *
     * @return AccountUpdateCount|null
     */
    public function getUpdatedLast1MonthCount(): ?AccountUpdateCount
    {
        return $this->updatedLast1MonthCount;
    }

    /**
     * Set updatedLast1MonthCount value
     *
     * @param AccountUpdateCount $updatedLast1MonthCount
     * @return InsightAccountCounts
     */
    public function setUpdatedLast1MonthCount(?AccountUpdateCount $updatedLast1MonthCount = null): self
    {
        $this->updatedLast1MonthCount = $updatedLast1MonthCount;

        return $this;
    }

    /**
     * Get updatedLast3MonthsCount value
     *
     * @return AccountUpdateCount|null
     */
    public function getUpdatedLast3MonthsCount(): ?AccountUpdateCount
    {
        return $this->updatedLast3MonthsCount;
    }

    /**
     * Set updatedLast3MonthsCount value
     *
     * @param AccountUpdateCount $updatedLast3MonthsCount
     * @return InsightAccountCounts
     */
    public function setUpdatedLast3MonthsCount(?AccountUpdateCount $updatedLast3MonthsCount = null): self
    {
        $this->updatedLast3MonthsCount = $updatedLast3MonthsCount;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return InsightAccountCounts
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
