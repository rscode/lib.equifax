<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * Electoral Roll (ER) is a list of adults over 18, or attainers (those who will become 18 during the electoral year) that are eligible to
 * vote in UK local and general elections.
 *
 * @see https://developer.equifax.co.uk/sites/uk/files/xml_ref_guide/ref/Data%20Groups/Electoral%20Roll.pdf
 */
class ElectoralRollRequest extends RawDataAvailableAtLinkedAddressRequest
{
}
