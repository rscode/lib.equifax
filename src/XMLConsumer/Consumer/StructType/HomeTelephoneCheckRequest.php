<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for HomeTelephoneCheckRequest StructType
 *
 * @subpackage Structs
 */
class HomeTelephoneCheckRequest extends CodedDataRequest
{
}
