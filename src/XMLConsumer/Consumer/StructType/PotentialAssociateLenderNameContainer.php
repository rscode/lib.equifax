<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for PotentialAssociateLenderNameContainer StructType
 *
 * @subpackage Structs
 */
class PotentialAssociateLenderNameContainer extends DataItemContainer
{
    /**
     * The potentialAssociateLenderName
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var PotentialAssociateLenderName[]
     */
    protected ?array $potentialAssociateLenderName = null;

    /**
     * Constructor method for PotentialAssociateLenderNameContainer
     *
     * @param PotentialAssociateLenderName[] $potentialAssociateLenderName
     * @uses PotentialAssociateLenderNameContainer::setPotentialAssociateLenderName()
     */
    public function __construct(?array $potentialAssociateLenderName = null)
    {
        $this
            ->setPotentialAssociateLenderName($potentialAssociateLenderName);
    }

    /**
     * This method is responsible for validating the values passed to the setPotentialAssociateLenderName method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPotentialAssociateLenderName method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePotentialAssociateLenderNameForArrayConstraintsFromSetPotentialAssociateLenderName(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $potentialAssociateLenderNameContainerPotentialAssociateLenderNameItem) {
            // validation for constraint: itemType
            if (!$potentialAssociateLenderNameContainerPotentialAssociateLenderNameItem instanceof PotentialAssociateLenderName) {
                $invalidValues[] = is_object($potentialAssociateLenderNameContainerPotentialAssociateLenderNameItem) ? get_class($potentialAssociateLenderNameContainerPotentialAssociateLenderNameItem) : sprintf(
                    '%s(%s)',
                    gettype($potentialAssociateLenderNameContainerPotentialAssociateLenderNameItem),
                    var_export($potentialAssociateLenderNameContainerPotentialAssociateLenderNameItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The potentialAssociateLenderName property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\PotentialAssociateLenderName, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get potentialAssociateLenderName value
     *
     * @return PotentialAssociateLenderName[]
     */
    public function getPotentialAssociateLenderName(): ?array
    {
        return $this->potentialAssociateLenderName;
    }

    /**
     * Set potentialAssociateLenderName value
     *
     * @param PotentialAssociateLenderName[] $potentialAssociateLenderName
     * @return PotentialAssociateLenderNameContainer
     * @throws InvalidArgumentException
     */
    public function setPotentialAssociateLenderName(?array $potentialAssociateLenderName = null): self
    {
        // validation for constraint: array
        if ('' !== ($potentialAssociateLenderNameArrayErrorMessage = self::validatePotentialAssociateLenderNameForArrayConstraintsFromSetPotentialAssociateLenderName($potentialAssociateLenderName))) {
            throw new InvalidArgumentException($potentialAssociateLenderNameArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($potentialAssociateLenderName) && count($potentialAssociateLenderName) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($potentialAssociateLenderName)
            ), __LINE__);
        }
        $this->potentialAssociateLenderName = $potentialAssociateLenderName;

        return $this;
    }

    /**
     * Add item to potentialAssociateLenderName value
     *
     * @param PotentialAssociateLenderName $item
     * @return PotentialAssociateLenderNameContainer
     * @throws InvalidArgumentException
     */
    public function addToPotentialAssociateLenderName(PotentialAssociateLenderName $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof PotentialAssociateLenderName) {
            throw new InvalidArgumentException(sprintf(
                'The potentialAssociateLenderName property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\PotentialAssociateLenderName, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->potentialAssociateLenderName) && count($this->potentialAssociateLenderName) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->potentialAssociateLenderName)
            ), __LINE__);
        }
        $this->potentialAssociateLenderName[] = $item;

        return $this;
    }
}
