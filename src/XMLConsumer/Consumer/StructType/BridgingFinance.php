<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for BridgingFinance StructType
 *
 * @subpackage Structs
 */
class BridgingFinance extends FinancialAgreement
{
}
