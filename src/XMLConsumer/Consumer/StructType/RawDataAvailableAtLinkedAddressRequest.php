<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\RequestScope;

/**
 * This class stands for RawDataAvailableAtLinkedAddressRequest StructType
 *
 * @subpackage Structs
 */
abstract class RawDataAvailableAtLinkedAddressRequest extends RawDataRequest
{
    /**
     * The scope
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $scope = null;

    public function __construct(int $maxNumber, ?string $scope = null)
    {
        $this
            ->setScope($scope);

        parent::__construct($maxNumber);
    }

    public function getScope(): ?string
    {
        return $this->scope;
    }

    public function setScope(?string $scope): self
    {
        // validation for constraint: enumeration
        if (!RequestScope::valueIsValid($scope)) {
            throw new InvalidArgumentException(
                sprintf(
                    'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\RequestScope',
                    is_array($scope) ? implode(', ', $scope) : var_export($scope, true),
                    implode(', ', RequestScope::getValidValues())
                ),
                __LINE__
            );
        }
        $this->scope = $scope;

        return $this;
    }
}
