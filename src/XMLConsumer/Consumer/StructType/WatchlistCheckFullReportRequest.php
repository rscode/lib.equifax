<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for WatchlistCheckFullReportRequest StructType
 * Meta information extracted from the WSDL
 * - documentation: The Credit Search operation is the most common type of enquiry and models the standard credit enquiry and allows the return of characteristic values, scores, raw and coded data items as required, that either results in a credit
 * search footprint, or search type of SR, for initial enquiries, or SE for subsequent enquiries.
 *
 * @subpackage Structs
 */
class WatchlistCheckFullReportRequest extends AbstractStructBase
{
    /**
     * The transactionReference
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $transactionReference;
    /**
     * The alertIdentifier
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $alertIdentifier;
    /**
     * The matchIdentifier
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $matchIdentifier;
    /**
     * The clientRef
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $clientRef = null;

    /**
     * Constructor method for WatchlistCheckFullReportRequest
     *
     * @param string $transactionReference
     * @param string $alertIdentifier
     * @param string $matchIdentifier
     * @param string $clientRef
     * @uses WatchlistCheckFullReportRequest::setTransactionReference()
     * @uses WatchlistCheckFullReportRequest::setAlertIdentifier()
     * @uses WatchlistCheckFullReportRequest::setMatchIdentifier()
     * @uses WatchlistCheckFullReportRequest::setClientRef()
     */
    public function __construct(string $transactionReference, string $alertIdentifier, string $matchIdentifier, ?string $clientRef = null)
    {
        $this
            ->setTransactionReference($transactionReference)
            ->setAlertIdentifier($alertIdentifier)
            ->setMatchIdentifier($matchIdentifier)
            ->setClientRef($clientRef);
    }

    /**
     * Get transactionReference value
     *
     * @return string
     */
    public function getTransactionReference(): string
    {
        return $this->transactionReference;
    }

    /**
     * Set transactionReference value
     *
     * @param string $transactionReference
     * @return WatchlistCheckFullReportRequest
     */
    public function setTransactionReference(string $transactionReference): self
    {
        // validation for constraint: string
        if (!is_null($transactionReference) && !is_string($transactionReference)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($transactionReference, true),
                gettype($transactionReference)
            ), __LINE__);
        }
        $this->transactionReference = $transactionReference;

        return $this;
    }

    /**
     * Get alertIdentifier value
     *
     * @return string
     */
    public function getAlertIdentifier(): string
    {
        return $this->alertIdentifier;
    }

    /**
     * Set alertIdentifier value
     *
     * @param string $alertIdentifier
     * @return WatchlistCheckFullReportRequest
     */
    public function setAlertIdentifier(string $alertIdentifier): self
    {
        // validation for constraint: string
        if (!is_null($alertIdentifier) && !is_string($alertIdentifier)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($alertIdentifier, true),
                gettype($alertIdentifier)
            ), __LINE__);
        }
        $this->alertIdentifier = $alertIdentifier;

        return $this;
    }

    /**
     * Get matchIdentifier value
     *
     * @return string
     */
    public function getMatchIdentifier(): string
    {
        return $this->matchIdentifier;
    }

    /**
     * Set matchIdentifier value
     *
     * @param string $matchIdentifier
     * @return WatchlistCheckFullReportRequest
     */
    public function setMatchIdentifier(string $matchIdentifier): self
    {
        // validation for constraint: string
        if (!is_null($matchIdentifier) && !is_string($matchIdentifier)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($matchIdentifier, true),
                gettype($matchIdentifier)
            ), __LINE__);
        }
        $this->matchIdentifier = $matchIdentifier;

        return $this;
    }

    /**
     * Get clientRef value
     *
     * @return string|null
     */
    public function getClientRef(): ?string
    {
        return $this->clientRef;
    }

    /**
     * Set clientRef value
     *
     * @param string $clientRef
     * @return WatchlistCheckFullReportRequest
     */
    public function setClientRef(?string $clientRef = null): self
    {
        // validation for constraint: string
        if (!is_null($clientRef) && !is_string($clientRef)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($clientRef, true),
                gettype($clientRef)
            ), __LINE__);
        }
        $this->clientRef = $clientRef;

        return $this;
    }
}
