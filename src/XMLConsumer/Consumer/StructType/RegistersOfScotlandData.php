<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\BuyerSellerRole;

/**
 * This class stands for RegistersOfScotlandData StructType
 *
 * @subpackage Structs
 */
class RegistersOfScotlandData extends PropertyValuationBase
{
    /**
     * The buyerSellerRole
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $buyerSellerRole;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for RegistersOfScotlandData
     *
     * @param string                  $buyerSellerRole
     * @param DOMDocument|string|null $any
     * @uses RegistersOfScotlandData::setBuyerSellerRole()
     * @uses RegistersOfScotlandData::setAny()
     */
    public function __construct(string $buyerSellerRole, $any = null)
    {
        $this
            ->setBuyerSellerRole($buyerSellerRole)
            ->setAny($any);
    }

    /**
     * Get buyerSellerRole value
     *
     * @return string
     */
    public function getBuyerSellerRole(): string
    {
        return $this->buyerSellerRole;
    }

    /**
     * Set buyerSellerRole value
     *
     * @param string $buyerSellerRole
     * @return RegistersOfScotlandData
     * @throws InvalidArgumentException
     * @uses BuyerSellerRole::getValidValues
     * @uses BuyerSellerRole::valueIsValid
     */
    public function setBuyerSellerRole(string $buyerSellerRole): self
    {
        // validation for constraint: enumeration
        if (!BuyerSellerRole::valueIsValid($buyerSellerRole)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\BuyerSellerRole',
                is_array($buyerSellerRole) ? implode(', ', $buyerSellerRole) : var_export($buyerSellerRole, true),
                implode(', ', BuyerSellerRole::getValidValues())
            ), __LINE__);
        }
        $this->buyerSellerRole = $buyerSellerRole;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return RegistersOfScotlandData
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
