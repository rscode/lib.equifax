<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for AMLHaloDeceasedCheckContainer StructType
 *
 * @subpackage Structs
 */
class AMLHaloDeceasedCheckContainer extends DataItemContainer
{
    /**
     * The amlHaloDeceasedCheck
     * Meta information extracted from the WSDL
     * - maxOccurs: 2
     * - minOccurs: 0
     *
     * @var AMLHaloDeceasedCheck[]
     */
    protected ?array $amlHaloDeceasedCheck = null;

    /**
     * Constructor method for AMLHaloDeceasedCheckContainer
     *
     * @param AMLHaloDeceasedCheck[] $amlHaloDeceasedCheck
     * @uses AMLHaloDeceasedCheckContainer::setAmlHaloDeceasedCheck()
     */
    public function __construct(?array $amlHaloDeceasedCheck = null)
    {
        $this
            ->setAmlHaloDeceasedCheck($amlHaloDeceasedCheck);
    }

    /**
     * This method is responsible for validating the values passed to the setAmlHaloDeceasedCheck method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAmlHaloDeceasedCheck method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAmlHaloDeceasedCheckForArrayConstraintsFromSetAmlHaloDeceasedCheck(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $aMLHaloDeceasedCheckContainerAmlHaloDeceasedCheckItem) {
            // validation for constraint: itemType
            if (!$aMLHaloDeceasedCheckContainerAmlHaloDeceasedCheckItem instanceof AMLHaloDeceasedCheck) {
                $invalidValues[] = is_object($aMLHaloDeceasedCheckContainerAmlHaloDeceasedCheckItem) ? get_class($aMLHaloDeceasedCheckContainerAmlHaloDeceasedCheckItem) : sprintf(
                    '%s(%s)',
                    gettype($aMLHaloDeceasedCheckContainerAmlHaloDeceasedCheckItem),
                    var_export($aMLHaloDeceasedCheckContainerAmlHaloDeceasedCheckItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The amlHaloDeceasedCheck property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AMLHaloDeceasedCheck, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get amlHaloDeceasedCheck value
     *
     * @return AMLHaloDeceasedCheck[]
     */
    public function getAmlHaloDeceasedCheck(): ?array
    {
        return $this->amlHaloDeceasedCheck;
    }

    /**
     * Set amlHaloDeceasedCheck value
     *
     * @param AMLHaloDeceasedCheck[] $amlHaloDeceasedCheck
     * @return AMLHaloDeceasedCheckContainer
     * @throws InvalidArgumentException
     */
    public function setAmlHaloDeceasedCheck(?array $amlHaloDeceasedCheck = null): self
    {
        // validation for constraint: array
        if ('' !== ($amlHaloDeceasedCheckArrayErrorMessage = self::validateAmlHaloDeceasedCheckForArrayConstraintsFromSetAmlHaloDeceasedCheck($amlHaloDeceasedCheck))) {
            throw new InvalidArgumentException($amlHaloDeceasedCheckArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(2)
        if (is_array($amlHaloDeceasedCheck) && count($amlHaloDeceasedCheck) > 2) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 2',
                count($amlHaloDeceasedCheck)
            ), __LINE__);
        }
        $this->amlHaloDeceasedCheck = $amlHaloDeceasedCheck;

        return $this;
    }

    /**
     * Add item to amlHaloDeceasedCheck value
     *
     * @param AMLHaloDeceasedCheck $item
     * @return AMLHaloDeceasedCheckContainer
     * @throws InvalidArgumentException
     */
    public function addToAmlHaloDeceasedCheck(AMLHaloDeceasedCheck $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof AMLHaloDeceasedCheck) {
            throw new InvalidArgumentException(sprintf(
                'The amlHaloDeceasedCheck property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AMLHaloDeceasedCheck, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(2)
        if (is_array($this->amlHaloDeceasedCheck) && count($this->amlHaloDeceasedCheck) >= 2) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 2',
                count($this->amlHaloDeceasedCheck)
            ), __LINE__);
        }
        $this->amlHaloDeceasedCheck[] = $item;

        return $this;
    }
}
