<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for PreviousSearch StructType
 *
 * @subpackage Structs
 */
abstract class PreviousSearch extends NameMatchedData
{
    /**
     * The companyType
     * Meta information extracted from the WSDL
     * - documentation: Following is the list of possible values along with its description that could be retuned in a Searches record: AC - Accountancy BF - Bank (Non-CLSB and Finance Divisions) BK - Bank BS - Building Society CB - Credit Broker CC -
     * Credit Card CE - Customs and Excise CG - Cheque Guarantor CH - Charge Card CI - Credit Insurer CL - Consumer Letter CR - Commercial Reporting CS - Communication Supplier DC - Debt Collector DI - Distribution and Wholesalers EA - Employment Agency EN
     * - Enquiry Agent ES - Energy Supplier FN - Finance House FS - Financial Services GI - General Insurance GV - Government HC - Hire Car Rental HF - Home Furnisher HI - Home Improvement HO - House Builder HS - Health Services HT - Hotel and Travel IB -
     * Insurance Broker IN - Insurance IR - Inland Revenue LA - Loss Adjuster LG - Leasing MD - Motor Dealer ME - Media MF - Manufacturing / Industrial MK - Marketing MN - Miscellaneous MO - Mail Order MS - Mortgage Supplier OS - Overseas PM - Property
     * Management PO - Police PU - Public Utility RN - Rental RT - Retailer SB - Stock Broker SO - Solicitor SR - Slot Rental SS - Security Services TC - TV Programme Supplier TI - Travel Insurer TP - Third party TV - TV Rental XX - Training
     * - base: xs:string
     * - maxLength: 2
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $companyType;
    /**
     * The jointApplicant
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $jointApplicant;
    /**
     * The searchDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $searchDate;
    /**
     * The accountNumber
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 8
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $accountNumber = null;
    /**
     * The companyName
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 32
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $companyName = null;

    /**
     * Constructor method for PreviousSearch
     *
     * @param string $companyType
     * @param bool   $jointApplicant
     * @param string $searchDate
     * @param string $accountNumber
     * @param string $companyName
     * @uses PreviousSearch::setCompanyType()
     * @uses PreviousSearch::setJointApplicant()
     * @uses PreviousSearch::setSearchDate()
     * @uses PreviousSearch::setAccountNumber()
     * @uses PreviousSearch::setCompanyName()
     */
    public function __construct(
        string $companyType,
        bool $jointApplicant,
        string $searchDate,
        ?string $accountNumber = null,
        ?string $companyName = null
    ) {
        $this
            ->setCompanyType($companyType)
            ->setJointApplicant($jointApplicant)
            ->setSearchDate($searchDate)
            ->setAccountNumber($accountNumber)
            ->setCompanyName($companyName);
    }

    /**
     * Get companyType value
     *
     * @return string
     */
    public function getCompanyType(): string
    {
        return $this->companyType;
    }

    /**
     * Set companyType value
     *
     * @param string $companyType
     * @return PreviousSearch
     */
    public function setCompanyType(string $companyType): self
    {
        // validation for constraint: string
        if (!is_null($companyType) && !is_string($companyType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($companyType, true),
                gettype($companyType)
            ), __LINE__);
        }
        // validation for constraint: maxLength(2)
        if (!is_null($companyType) && mb_strlen((string)$companyType) > 2) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 2',
                mb_strlen((string)$companyType)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($companyType) && mb_strlen((string)$companyType) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$companyType)
            ), __LINE__);
        }
        $this->companyType = $companyType;

        return $this;
    }

    /**
     * Get jointApplicant value
     *
     * @return bool
     */
    public function getJointApplicant(): bool
    {
        return $this->jointApplicant;
    }

    /**
     * Set jointApplicant value
     *
     * @param bool $jointApplicant
     * @return PreviousSearch
     */
    public function setJointApplicant(bool $jointApplicant): self
    {
        // validation for constraint: boolean
        if (!is_null($jointApplicant) && !is_bool($jointApplicant)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($jointApplicant, true),
                gettype($jointApplicant)
            ), __LINE__);
        }
        $this->jointApplicant = $jointApplicant;

        return $this;
    }

    /**
     * Get searchDate value
     *
     * @return string
     */
    public function getSearchDate(): string
    {
        return $this->searchDate;
    }

    /**
     * Set searchDate value
     *
     * @param string $searchDate
     * @return PreviousSearch
     */
    public function setSearchDate(string $searchDate): self
    {
        // validation for constraint: string
        if (!is_null($searchDate) && !is_string($searchDate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($searchDate, true),
                gettype($searchDate)
            ), __LINE__);
        }
        $this->searchDate = $searchDate;

        return $this;
    }

    /**
     * Get accountNumber value
     *
     * @return string|null
     */
    public function getAccountNumber(): ?string
    {
        return $this->accountNumber;
    }

    /**
     * Set accountNumber value
     *
     * @param string $accountNumber
     * @return PreviousSearch
     */
    public function setAccountNumber(?string $accountNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($accountNumber) && !is_string($accountNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($accountNumber, true),
                gettype($accountNumber)
            ), __LINE__);
        }
        // validation for constraint: maxLength(8)
        if (!is_null($accountNumber) && mb_strlen((string)$accountNumber) > 8) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 8',
                mb_strlen((string)$accountNumber)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($accountNumber) && mb_strlen((string)$accountNumber) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$accountNumber)
            ), __LINE__);
        }
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Get companyName value
     *
     * @return string|null
     */
    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    /**
     * Set companyName value
     *
     * @param string $companyName
     * @return PreviousSearch
     */
    public function setCompanyName(?string $companyName = null): self
    {
        // validation for constraint: string
        if (!is_null($companyName) && !is_string($companyName)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($companyName, true),
                gettype($companyName)
            ), __LINE__);
        }
        // validation for constraint: maxLength(32)
        if (!is_null($companyName) && mb_strlen((string)$companyName) > 32) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 32',
                mb_strlen((string)$companyName)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($companyName) && mb_strlen((string)$companyName) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$companyName)
            ), __LINE__);
        }
        $this->companyName = $companyName;

        return $this;
    }
}
