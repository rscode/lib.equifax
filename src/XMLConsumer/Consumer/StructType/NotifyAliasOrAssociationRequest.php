<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for NotifyAliasOrAssociationRequest StructType
 *
 * @subpackage Structs
 */
class NotifyAliasOrAssociationRequest extends CommonRequest
{
}
