<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for Name StructType
 *
 * @subpackage Structs
 */
class Name extends NameBase
{
    /**
     * The forename
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxOccurs: 1
     * - minLength: 1
     * - minOccurs: 1
     * - whiteSpace: collapse
     *
     * @var string
     */
    protected string $forename;

    public function __construct(string $forename = '', string $surname = '')
    {
        $this
            ->setForename($forename);

        parent::__construct($surname);
    }

    /**
     * Get forename value
     *
     * @return string
     */
    public function getForename(): string
    {
        return $this->forename;
    }

    /**
     * Set forename value
     *
     * @param string $forename
     * @return Name
     */
    public function setForename(string $forename): self
    {
        // validation for constraint: string
        if (!is_null($forename) && !is_string($forename)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($forename, true),
                gettype($forename)
            ), __LINE__);
        }
        // validation for constraint: minLength(1)
        if (!is_null($forename) && mb_strlen((string)$forename) < 1) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 1',
                mb_strlen((string)$forename)
            ), __LINE__);
        }
        $this->forename = $forename;

        return $this;
    }
}
