<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for FraudProfileSummaryRequest StructType
 *
 * @subpackage Structs
 */
class FraudProfileSummaryRequest extends CodedDataRequest
{
}
