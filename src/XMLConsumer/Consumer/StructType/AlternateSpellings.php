<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AlternateSpellings StructType
 *
 * @subpackage Structs
 */
class AlternateSpellings extends AbstractStructBase
{
    /**
     * The alternateSpellingName
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var string[]
     */
    protected ?array $alternateSpellingName = null;

    /**
     * Constructor method for AlternateSpellings
     *
     * @param string[] $alternateSpellingName
     * @uses AlternateSpellings::setAlternateSpellingName()
     */
    public function __construct(?array $alternateSpellingName = null)
    {
        $this
            ->setAlternateSpellingName($alternateSpellingName);
    }

    /**
     * This method is responsible for validating the values passed to the setAlternateSpellingName method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAlternateSpellingName method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAlternateSpellingNameForArrayConstraintsFromSetAlternateSpellingName(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $alternateSpellingsAlternateSpellingNameItem) {
            // validation for constraint: itemType
            if (!is_string($alternateSpellingsAlternateSpellingNameItem)) {
                $invalidValues[] = is_object($alternateSpellingsAlternateSpellingNameItem) ? get_class($alternateSpellingsAlternateSpellingNameItem) : sprintf(
                    '%s(%s)',
                    gettype($alternateSpellingsAlternateSpellingNameItem),
                    var_export($alternateSpellingsAlternateSpellingNameItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The alternateSpellingName property can only contain items of type string, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get alternateSpellingName value
     *
     * @return string[]
     */
    public function getAlternateSpellingName(): ?array
    {
        return $this->alternateSpellingName;
    }

    /**
     * Set alternateSpellingName value
     *
     * @param string[] $alternateSpellingName
     * @return AlternateSpellings
     * @throws InvalidArgumentException
     */
    public function setAlternateSpellingName(?array $alternateSpellingName = null): self
    {
        // validation for constraint: array
        if ('' !== ($alternateSpellingNameArrayErrorMessage = self::validateAlternateSpellingNameForArrayConstraintsFromSetAlternateSpellingName($alternateSpellingName))) {
            throw new InvalidArgumentException($alternateSpellingNameArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($alternateSpellingName) && count($alternateSpellingName) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($alternateSpellingName)
            ), __LINE__);
        }
        $this->alternateSpellingName = $alternateSpellingName;

        return $this;
    }

    /**
     * Add item to alternateSpellingName value
     *
     * @param string $item
     * @return AlternateSpellings
     * @throws InvalidArgumentException
     */
    public function addToAlternateSpellingName(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf(
                'The alternateSpellingName property can only contain items of type string, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->alternateSpellingName) && count($this->alternateSpellingName) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->alternateSpellingName)
            ), __LINE__);
        }
        $this->alternateSpellingName[] = $item;

        return $this;
    }
}
