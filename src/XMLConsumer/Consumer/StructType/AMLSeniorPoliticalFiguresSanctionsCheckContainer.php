<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for AMLSeniorPoliticalFiguresSanctionsCheckContainer
 * StructType
 *
 * @subpackage Structs
 */
class AMLSeniorPoliticalFiguresSanctionsCheckContainer extends DataItemContainer
{
    /**
     * The amlSeniorPoliticalFiguresSanctionsCheck
     * Meta information extracted from the WSDL
     * - maxOccurs: 2
     * - minOccurs: 0
     *
     * @var AMLSeniorPoliticalFiguresSanctionsCheck[]
     */
    protected ?array $amlSeniorPoliticalFiguresSanctionsCheck = null;

    /**
     * Constructor method for AMLSeniorPoliticalFiguresSanctionsCheckContainer
     *
     * @param AMLSeniorPoliticalFiguresSanctionsCheck[] $amlSeniorPoliticalFiguresSanctionsCheck
     * @uses AMLSeniorPoliticalFiguresSanctionsCheckContainer::setAmlSeniorPoliticalFiguresSanctionsCheck()
     */
    public function __construct(?array $amlSeniorPoliticalFiguresSanctionsCheck = null)
    {
        $this
            ->setAmlSeniorPoliticalFiguresSanctionsCheck($amlSeniorPoliticalFiguresSanctionsCheck);
    }

    /**
     * This method is responsible for validating the values passed to the setAmlSeniorPoliticalFiguresSanctionsCheck method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAmlSeniorPoliticalFiguresSanctionsCheck method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAmlSeniorPoliticalFiguresSanctionsCheckForArrayConstraintsFromSetAmlSeniorPoliticalFiguresSanctionsCheck(
        ?array $values = []
    ): string {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $aMLSeniorPoliticalFiguresSanctionsCheckContainerAmlSeniorPoliticalFiguresSanctionsCheckItem) {
            // validation for constraint: itemType
            if (!$aMLSeniorPoliticalFiguresSanctionsCheckContainerAmlSeniorPoliticalFiguresSanctionsCheckItem instanceof AMLSeniorPoliticalFiguresSanctionsCheck) {
                $invalidValues[] = is_object($aMLSeniorPoliticalFiguresSanctionsCheckContainerAmlSeniorPoliticalFiguresSanctionsCheckItem) ? get_class($aMLSeniorPoliticalFiguresSanctionsCheckContainerAmlSeniorPoliticalFiguresSanctionsCheckItem) : sprintf(
                    '%s(%s)',
                    gettype($aMLSeniorPoliticalFiguresSanctionsCheckContainerAmlSeniorPoliticalFiguresSanctionsCheckItem),
                    var_export($aMLSeniorPoliticalFiguresSanctionsCheckContainerAmlSeniorPoliticalFiguresSanctionsCheckItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The amlSeniorPoliticalFiguresSanctionsCheck property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AMLSeniorPoliticalFiguresSanctionsCheck, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get amlSeniorPoliticalFiguresSanctionsCheck value
     *
     * @return AMLSeniorPoliticalFiguresSanctionsCheck[]
     */
    public function getAmlSeniorPoliticalFiguresSanctionsCheck(): ?array
    {
        return $this->amlSeniorPoliticalFiguresSanctionsCheck;
    }

    /**
     * Set amlSeniorPoliticalFiguresSanctionsCheck value
     *
     * @param AMLSeniorPoliticalFiguresSanctionsCheck[] $amlSeniorPoliticalFiguresSanctionsCheck
     * @return AMLSeniorPoliticalFiguresSanctionsCheckContainer
     * @throws InvalidArgumentException
     */
    public function setAmlSeniorPoliticalFiguresSanctionsCheck(?array $amlSeniorPoliticalFiguresSanctionsCheck = null): self
    {
        // validation for constraint: array
        if ('' !== ($amlSeniorPoliticalFiguresSanctionsCheckArrayErrorMessage = self::validateAmlSeniorPoliticalFiguresSanctionsCheckForArrayConstraintsFromSetAmlSeniorPoliticalFiguresSanctionsCheck($amlSeniorPoliticalFiguresSanctionsCheck))) {
            throw new InvalidArgumentException($amlSeniorPoliticalFiguresSanctionsCheckArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(2)
        if (is_array($amlSeniorPoliticalFiguresSanctionsCheck) && count($amlSeniorPoliticalFiguresSanctionsCheck) > 2) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 2',
                count($amlSeniorPoliticalFiguresSanctionsCheck)
            ), __LINE__);
        }
        $this->amlSeniorPoliticalFiguresSanctionsCheck = $amlSeniorPoliticalFiguresSanctionsCheck;

        return $this;
    }

    /**
     * Add item to amlSeniorPoliticalFiguresSanctionsCheck value
     *
     * @param AMLSeniorPoliticalFiguresSanctionsCheck $item
     * @return AMLSeniorPoliticalFiguresSanctionsCheckContainer
     * @throws InvalidArgumentException
     */
    public function addToAmlSeniorPoliticalFiguresSanctionsCheck(AMLSeniorPoliticalFiguresSanctionsCheck $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof AMLSeniorPoliticalFiguresSanctionsCheck) {
            throw new InvalidArgumentException(sprintf(
                'The amlSeniorPoliticalFiguresSanctionsCheck property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AMLSeniorPoliticalFiguresSanctionsCheck, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(2)
        if (is_array($this->amlSeniorPoliticalFiguresSanctionsCheck) && count($this->amlSeniorPoliticalFiguresSanctionsCheck) >= 2) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 2',
                count($this->amlSeniorPoliticalFiguresSanctionsCheck)
            ), __LINE__);
        }
        $this->amlSeniorPoliticalFiguresSanctionsCheck[] = $item;

        return $this;
    }
}
