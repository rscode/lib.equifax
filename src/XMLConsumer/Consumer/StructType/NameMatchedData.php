<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\NameMatchStatus;

/**
 * This class stands for NameMatchedData StructType
 *
 * @subpackage Structs
 */
abstract class NameMatchedData extends NamedData
{
    /**
     * The nameMatchStatus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $nameMatchStatus = null;

    /**
     * Constructor method for NameMatchedData
     *
     * @param string $nameMatchStatus
     * @uses NameMatchedData::setNameMatchStatus()
     */
    public function __construct(?string $nameMatchStatus = null)
    {
        $this
            ->setNameMatchStatus($nameMatchStatus);
    }

    /**
     * Get nameMatchStatus value
     *
     * @return string|null
     */
    public function getNameMatchStatus(): ?string
    {
        return $this->nameMatchStatus;
    }

    /**
     * Set nameMatchStatus value
     *
     * @param string $nameMatchStatus
     * @return NameMatchedData
     * @throws InvalidArgumentException
     * @uses NameMatchStatus::getValidValues
     * @uses NameMatchStatus::valueIsValid
     */
    public function setNameMatchStatus(?string $nameMatchStatus = null): self
    {
        // validation for constraint: enumeration
        if (!NameMatchStatus::valueIsValid($nameMatchStatus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\NameMatchStatus',
                is_array($nameMatchStatus) ? implode(', ', $nameMatchStatus) : var_export($nameMatchStatus, true),
                implode(', ', NameMatchStatus::getValidValues())
            ), __LINE__);
        }
        $this->nameMatchStatus = $nameMatchStatus;

        return $this;
    }
}
