<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DataItemContainer StructType
 *
 * @subpackage Structs
 */
class DataItemContainer extends AbstractStructBase
{
    /**
     * The accessDenied
     * Meta information extracted from the WSDL
     * - use: optional
     *
     * @var bool|null
     */
    protected ?bool $accessDenied = null;

    /**
     * Constructor method for DataItemContainer
     *
     * @param bool $accessDenied
     * @uses DataItemContainer::setAccessDenied()
     */
    public function __construct(?bool $accessDenied = null)
    {
        $this
            ->setAccessDenied($accessDenied);
    }

    /**
     * Get accessDenied value
     *
     * @return bool|null
     */
    public function getAccessDenied(): ?bool
    {
        return $this->accessDenied;
    }

    /**
     * Set accessDenied value
     *
     * @param bool $accessDenied
     * @return DataItemContainer
     */
    public function setAccessDenied(?bool $accessDenied = null): self
    {
        // validation for constraint: boolean
        if (!is_null($accessDenied) && !is_bool($accessDenied)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($accessDenied, true),
                gettype($accessDenied)
            ), __LINE__);
        }
        $this->accessDenied = $accessDenied;

        return $this;
    }
}
