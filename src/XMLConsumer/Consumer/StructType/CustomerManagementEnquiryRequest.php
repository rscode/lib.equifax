<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for CustomerManagementEnquiryRequest StructType
 * Meta information extracted from the WSDL
 * - documentation: Credit customer management
 *
 * @subpackage Structs
 */
class CustomerManagementEnquiryRequest extends CommonRequest
{
}
