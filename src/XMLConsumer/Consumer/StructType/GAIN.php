<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\TraceDirection;

/**
 * This class stands for GAIN StructType
 * Meta information extracted from the WSDL
 * - documentation: Individuals who have left an address leaving behind a debt without notifying the credit grantor of their new address.
 *
 * @subpackage Structs
 */
class GAIN extends NameMatchedData
{
    /**
     * The address
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var StructuredAddress
     */
    protected StructuredAddress $address;
    /**
     * The supplyingMemberID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 4
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $supplyingMemberID;
    /**
     * The accountNumber
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 20
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $accountNumber = null;
    /**
     * The addressSourceIndicator
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $addressSourceIndicator = null;
    /**
     * The furtherDetails
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var bool|null
     */
    protected ?bool $furtherDetails = null;
    /**
     * The goneAwayDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $goneAwayDate = null;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for GAIN
     *
     * @param StructuredAddress $address
     * @param string $supplyingMemberID
     * @param string $accountNumber
     * @param string $addressSourceIndicator
     * @param bool $furtherDetails
     * @param string $goneAwayDate
     * @param DOMDocument|string|null $any
     * @uses GAIN::setAddress()
     * @uses GAIN::setSupplyingMemberID()
     * @uses GAIN::setAccountNumber()
     * @uses GAIN::setAddressSourceIndicator()
     * @uses GAIN::setFurtherDetails()
     * @uses GAIN::setGoneAwayDate()
     * @uses GAIN::setAny()
     */
    public function __construct(
        StructuredAddress $address,
        string $supplyingMemberID,
        ?string $accountNumber = null,
        ?string $addressSourceIndicator = null,
        ?bool $furtherDetails = null,
        ?string $goneAwayDate = null,
        $any = null
    ) {
        $this
            ->setAddress($address)
            ->setSupplyingMemberID($supplyingMemberID)
            ->setAccountNumber($accountNumber)
            ->setAddressSourceIndicator($addressSourceIndicator)
            ->setFurtherDetails($furtherDetails)
            ->setGoneAwayDate($goneAwayDate)
            ->setAny($any);
    }

    /**
     * Get address value
     *
     * @return StructuredAddress
     */
    public function getAddress(): StructuredAddress
    {
        return $this->address;
    }

    /**
     * Set address value
     *
     * @param StructuredAddress $address
     * @return GAIN
     */
    public function setAddress(StructuredAddress $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get supplyingMemberID value
     *
     * @return string
     */
    public function getSupplyingMemberID(): string
    {
        return $this->supplyingMemberID;
    }

    /**
     * Set supplyingMemberID value
     *
     * @param string $supplyingMemberID
     * @return GAIN
     */
    public function setSupplyingMemberID(string $supplyingMemberID): self
    {
        // validation for constraint: string
        if (!is_null($supplyingMemberID) && !is_string($supplyingMemberID)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($supplyingMemberID, true),
                gettype($supplyingMemberID)
            ), __LINE__);
        }
        // validation for constraint: maxLength(4)
        if (!is_null($supplyingMemberID) && mb_strlen((string)$supplyingMemberID) > 4) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 4',
                mb_strlen((string)$supplyingMemberID)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($supplyingMemberID) && mb_strlen((string)$supplyingMemberID) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$supplyingMemberID)
            ), __LINE__);
        }
        $this->supplyingMemberID = $supplyingMemberID;

        return $this;
    }

    /**
     * Get accountNumber value
     *
     * @return string|null
     */
    public function getAccountNumber(): ?string
    {
        return $this->accountNumber;
    }

    /**
     * Set accountNumber value
     *
     * @param string $accountNumber
     * @return GAIN
     */
    public function setAccountNumber(?string $accountNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($accountNumber) && !is_string($accountNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($accountNumber, true),
                gettype($accountNumber)
            ), __LINE__);
        }
        // validation for constraint: maxLength(20)
        if (!is_null($accountNumber) && mb_strlen((string)$accountNumber) > 20) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 20',
                mb_strlen((string)$accountNumber)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($accountNumber) && mb_strlen((string)$accountNumber) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$accountNumber)
            ), __LINE__);
        }
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Get addressSourceIndicator value
     *
     * @return string|null
     */
    public function getAddressSourceIndicator(): ?string
    {
        return $this->addressSourceIndicator;
    }

    /**
     * Set addressSourceIndicator value
     *
     * @param string $addressSourceIndicator
     * @return GAIN
     * @throws InvalidArgumentException
     * @uses TraceDirection::getValidValues
     * @uses TraceDirection::valueIsValid
     */
    public function setAddressSourceIndicator(?string $addressSourceIndicator = null): self
    {
        // validation for constraint: enumeration
        if (!TraceDirection::valueIsValid($addressSourceIndicator)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\TraceDirection',
                is_array($addressSourceIndicator) ? implode(', ', $addressSourceIndicator) : var_export($addressSourceIndicator, true),
                implode(', ', TraceDirection::getValidValues())
            ), __LINE__);
        }
        $this->addressSourceIndicator = $addressSourceIndicator;

        return $this;
    }

    /**
     * Get furtherDetails value
     *
     * @return bool|null
     */
    public function getFurtherDetails(): ?bool
    {
        return $this->furtherDetails;
    }

    /**
     * Set furtherDetails value
     *
     * @param bool $furtherDetails
     * @return GAIN
     */
    public function setFurtherDetails(?bool $furtherDetails = null): self
    {
        // validation for constraint: boolean
        if (!is_null($furtherDetails) && !is_bool($furtherDetails)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($furtherDetails, true),
                gettype($furtherDetails)
            ), __LINE__);
        }
        $this->furtherDetails = $furtherDetails;

        return $this;
    }

    /**
     * Get goneAwayDate value
     *
     * @return string|null
     */
    public function getGoneAwayDate(): ?string
    {
        return $this->goneAwayDate;
    }

    /**
     * Set goneAwayDate value
     *
     * @param string $goneAwayDate
     * @return GAIN
     */
    public function setGoneAwayDate(?string $goneAwayDate = null): self
    {
        // validation for constraint: string
        if (!is_null($goneAwayDate) && !is_string($goneAwayDate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($goneAwayDate, true),
                gettype($goneAwayDate)
            ), __LINE__);
        }
        $this->goneAwayDate = $goneAwayDate;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return GAIN
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
