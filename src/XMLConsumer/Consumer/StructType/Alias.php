<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for Alias StructType
 * Meta information extracted from the WSDL
 * - documentation: Names identified as aliases through the supply of bureau data (e.g. Insight) where an individual's change of name has occurred as well as those aliases supplied directly by Equifax clients.
 *
 * @subpackage Structs
 */
class Alias extends NamedData
{
    /**
     * The aliasName
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var string[]
     */
    protected ?array $aliasName = null;

    /**
     * Constructor method for Alias
     *
     * @param string[] $aliasName
     * @uses Alias::setAliasName()
     */
    public function __construct(?array $aliasName = null)
    {
        $this
            ->setAliasName($aliasName);
    }

    /**
     * This method is responsible for validating the values passed to the setAliasName method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAliasName method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAliasNameForArrayConstraintsFromSetAliasName(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $aliasAliasNameItem) {
            // validation for constraint: itemType
            if (!is_string($aliasAliasNameItem)) {
                $invalidValues[] = is_object($aliasAliasNameItem) ? get_class($aliasAliasNameItem) : sprintf(
                    '%s(%s)',
                    gettype($aliasAliasNameItem),
                    var_export($aliasAliasNameItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The aliasName property can only contain items of type string, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get aliasName value
     *
     * @return string[]
     */
    public function getAliasName(): ?array
    {
        return $this->aliasName;
    }

    /**
     * Set aliasName value
     *
     * @param string[] $aliasName
     * @return Alias
     * @throws InvalidArgumentException
     */
    public function setAliasName(?array $aliasName = null): self
    {
        // validation for constraint: array
        if ('' !== ($aliasNameArrayErrorMessage = self::validateAliasNameForArrayConstraintsFromSetAliasName($aliasName))) {
            throw new InvalidArgumentException($aliasNameArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($aliasName) && count($aliasName) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($aliasName)
            ), __LINE__);
        }
        $this->aliasName = $aliasName;

        return $this;
    }

    /**
     * Add item to aliasName value
     *
     * @param string $item
     * @return Alias
     * @throws InvalidArgumentException
     */
    public function addToAliasName(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf(
                'The aliasName property can only contain items of type string, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->aliasName) && count($this->aliasName) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->aliasName)
            ), __LINE__);
        }
        $this->aliasName[] = $item;

        return $this;
    }
}
