<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * The attributes block is a flexible mechanism that enables Equifax to supply a selection of pre-defined, coded characteristics. Each
 * customer can have access to up to six defined attribute blocks, each containing a combination of attributes, although only one attribute
 * group can be requested for a single enquiry. Each enquiry then simply requests the appropriate attribute block number and the
 * corresponding attributes are returned. 'Same company' information can be excluded from the attributes returned via the
 * <employSameCompanyInsight> boolean element. Equifax can provide consultancy to assist in the selection of appropriate attributes. Part
 * of the consultancy process offered by Equifax is a retrospective analysis ('retro'), the process of calculating the
 * attribute/characteristic values that would have been seen for a sample of previous applications. For full details of the many hundreds
 * of characteristics that can be analysed via the Equifax Risk Navigator retro, please contact your Equifax Account Manager.
 *
 * @see https://developer.equifax.co.uk/sites/uk/files/xml_ref_guide/ref/Data%20Groups/Attribute%20Block.pdf
 */
class Attributes extends AbstractStructBase
{
    /**
     * The attribute
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     *
     * @var Attribute[]
     */
    protected ?array $attribute = null;

    /**
     * Constructor method for Attributes
     *
     * @param Attribute[] $attribute
     * @uses Attributes::setAttribute()
     */
    public function __construct(?array $attribute = null)
    {
        $this
            ->setAttribute($attribute);
    }

    /**
     * This method is responsible for validating the values passed to the setAttribute method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAttribute method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAttributeForArrayConstraintsFromSetAttribute(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $attributesAttributeItem) {
            // validation for constraint: itemType
            if (!$attributesAttributeItem instanceof Attribute) {
                $invalidValues[] = is_object($attributesAttributeItem) ? get_class($attributesAttributeItem) : sprintf(
                    '%s(%s)',
                    gettype($attributesAttributeItem),
                    var_export($attributesAttributeItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The attribute property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\Attribute, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get attribute value
     *
     * @return Attribute[]
     */
    public function getAttribute(): ?array
    {
        return $this->attribute;
    }

    /**
     * Set attribute value
     *
     * @param Attribute[] $attribute
     * @return Attributes
     * @throws InvalidArgumentException
     */
    public function setAttribute(?array $attribute = null): self
    {
        // validation for constraint: array
        if ('' !== ($attributeArrayErrorMessage = self::validateAttributeForArrayConstraintsFromSetAttribute($attribute))) {
            throw new InvalidArgumentException($attributeArrayErrorMessage, __LINE__);
        }
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * Add item to attribute value
     *
     * @param Attribute $item
     * @return Attributes
     * @throws InvalidArgumentException
     */
    public function addToAttribute(Attribute $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof Attribute) {
            throw new InvalidArgumentException(sprintf(
                'The attribute property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\Attribute, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        $this->attribute[] = $item;

        return $this;
    }
}
