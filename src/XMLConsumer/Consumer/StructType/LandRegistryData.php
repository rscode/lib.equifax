<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\TenureType;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\YesNoUnknown;

/**
 * This class stands for LandRegistryData StructType
 *
 * @subpackage Structs
 */
class LandRegistryData extends PropertyValuationBase
{
    /**
     * The newBuild
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $newBuild;
    /**
     * The propertyType
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - length: 1
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $propertyType;
    /**
     * The tenure
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $tenure;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for LandRegistryData
     *
     * @param string                  $newBuild
     * @param string                  $propertyType
     * @param string                  $tenure
     * @param DOMDocument|string|null $any
     * @uses LandRegistryData::setNewBuild()
     * @uses LandRegistryData::setPropertyType()
     * @uses LandRegistryData::setTenure()
     * @uses LandRegistryData::setAny()
     */
    public function __construct(string $newBuild, string $propertyType, string $tenure, $any = null)
    {
        $this
            ->setNewBuild($newBuild)
            ->setPropertyType($propertyType)
            ->setTenure($tenure)
            ->setAny($any);
    }

    /**
     * Get newBuild value
     *
     * @return string
     */
    public function getNewBuild(): string
    {
        return $this->newBuild;
    }

    /**
     * Set newBuild value
     *
     * @param string $newBuild
     * @return LandRegistryData
     * @throws InvalidArgumentException
     * @uses YesNoUnknown::getValidValues
     * @uses YesNoUnknown::valueIsValid
     */
    public function setNewBuild(string $newBuild): self
    {
        // validation for constraint: enumeration
        if (!YesNoUnknown::valueIsValid($newBuild)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\YesNoUnknown',
                is_array($newBuild) ? implode(', ', $newBuild) : var_export($newBuild, true),
                implode(', ', YesNoUnknown::getValidValues())
            ), __LINE__);
        }
        $this->newBuild = $newBuild;

        return $this;
    }

    /**
     * Get propertyType value
     *
     * @return string
     */
    public function getPropertyType(): string
    {
        return $this->propertyType;
    }

    /**
     * Set propertyType value
     *
     * @param string $propertyType
     * @return LandRegistryData
     */
    public function setPropertyType(string $propertyType): self
    {
        // validation for constraint: string
        if (!is_null($propertyType) && !is_string($propertyType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($propertyType, true),
                gettype($propertyType)
            ), __LINE__);
        }
        // validation for constraint: length(1)
        if (!is_null($propertyType) && mb_strlen((string)$propertyType) !== 1) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be equal to 1',
                mb_strlen((string)$propertyType)
            ), __LINE__);
        }
        $this->propertyType = $propertyType;

        return $this;
    }

    /**
     * Get tenure value
     *
     * @return string
     */
    public function getTenure(): string
    {
        return $this->tenure;
    }

    /**
     * Set tenure value
     *
     * @param string $tenure
     * @return LandRegistryData
     * @throws InvalidArgumentException
     * @uses TenureType::getValidValues
     * @uses TenureType::valueIsValid
     */
    public function setTenure(string $tenure): self
    {
        // validation for constraint: enumeration
        if (!TenureType::valueIsValid($tenure)) {
            throw new InvalidArgumentException(
                sprintf(
                    'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\TenureType',
                    is_array($tenure) ? implode(', ', $tenure) : var_export($tenure, true),
                    implode(', ', TenureType::getValidValues())
                ),
                __LINE__
            );
        }
        $this->tenure = $tenure;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return LandRegistryData
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
