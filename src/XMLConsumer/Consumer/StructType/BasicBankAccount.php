<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for BasicBankAccount StructType
 *
 * @subpackage Structs
 */
class BasicBankAccount extends FinancialAgreement
{
}
