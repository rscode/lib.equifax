<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for SiranChecksContainer StructType
 *
 * @subpackage Structs
 */
class SiranChecksContainer extends DataItemContainer
{
    /**
     * The siranChecks
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var SiranChecks|null
     */
    protected ?SiranChecks $siranChecks = null;

    /**
     * Constructor method for SiranChecksContainer
     *
     * @param SiranChecks $siranChecks
     * @uses SiranChecksContainer::setSiranChecks()
     */
    public function __construct(?SiranChecks $siranChecks = null)
    {
        $this
            ->setSiranChecks($siranChecks);
    }

    /**
     * Get siranChecks value
     *
     * @return SiranChecks|null
     */
    public function getSiranChecks(): ?SiranChecks
    {
        return $this->siranChecks;
    }

    /**
     * Set siranChecks value
     *
     * @param SiranChecks $siranChecks
     * @return SiranChecksContainer
     */
    public function setSiranChecks(?SiranChecks $siranChecks = null): self
    {
        $this->siranChecks = $siranChecks;

        return $this;
    }
}
