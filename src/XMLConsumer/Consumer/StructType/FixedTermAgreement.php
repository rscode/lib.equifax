<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for FixedTermAgreement StructType
 *
 * @subpackage Structs
 */
class FixedTermAgreement extends FinancialAgreement
{
    /**
     * The deferred
     * Meta information extracted from the WSDL
     * - use: optional
     *
     * @var bool|null
     */
    protected ?bool $deferred = null;

    /**
     * Constructor method for FixedTermAgreement
     *
     * @param bool $deferred
     * @uses FixedTermAgreement::setDeferred()
     */
    public function __construct(?bool $deferred = null)
    {
        $this
            ->setDeferred($deferred);
    }

    /**
     * Get deferred value
     *
     * @return bool|null
     */
    public function getDeferred(): ?bool
    {
        return $this->deferred;
    }

    /**
     * Set deferred value
     *
     * @param bool $deferred
     * @return FixedTermAgreement
     */
    public function setDeferred(?bool $deferred = null): self
    {
        // validation for constraint: boolean
        if (!is_null($deferred) && !is_bool($deferred)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($deferred, true),
                gettype($deferred)
            ), __LINE__);
        }
        $this->deferred = $deferred;

        return $this;
    }
}
