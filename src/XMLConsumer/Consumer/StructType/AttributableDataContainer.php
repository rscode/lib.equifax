<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for AttributableDataContainer StructType
 *
 * @subpackage Structs
 */
class AttributableDataContainer extends DataItemContainer
{
    /**
     * The attributableData
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var AttributableData[]
     */
    protected ?array $attributableData = null;

    /**
     * Constructor method for AttributableDataContainer
     *
     * @param AttributableData[] $attributableData
     * @uses AttributableDataContainer::setAttributableData()
     */
    public function __construct(?array $attributableData = null)
    {
        $this
            ->setAttributableData($attributableData);
    }

    /**
     * This method is responsible for validating the values passed to the setAttributableData method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAttributableData method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAttributableDataForArrayConstraintsFromSetAttributableData(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $attributableDataContainerAttributableDataItem) {
            // validation for constraint: itemType
            if (!$attributableDataContainerAttributableDataItem instanceof AttributableData) {
                $invalidValues[] = is_object($attributableDataContainerAttributableDataItem) ? get_class($attributableDataContainerAttributableDataItem) : sprintf(
                    '%s(%s)',
                    gettype($attributableDataContainerAttributableDataItem),
                    var_export($attributableDataContainerAttributableDataItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The attributableData property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AttributableData, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get attributableData value
     *
     * @return AttributableData[]
     */
    public function getAttributableData(): ?array
    {
        return $this->attributableData;
    }

    /**
     * Set attributableData value
     *
     * @param AttributableData[] $attributableData
     * @return AttributableDataContainer
     * @throws InvalidArgumentException
     */
    public function setAttributableData(?array $attributableData = null): self
    {
        // validation for constraint: array
        if ('' !== ($attributableDataArrayErrorMessage = self::validateAttributableDataForArrayConstraintsFromSetAttributableData($attributableData))) {
            throw new InvalidArgumentException($attributableDataArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($attributableData) && count($attributableData) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($attributableData)
            ), __LINE__);
        }
        $this->attributableData = $attributableData;

        return $this;
    }

    /**
     * Add item to attributableData value
     *
     * @param AttributableData $item
     * @return AttributableDataContainer
     * @throws InvalidArgumentException
     */
    public function addToAttributableData(AttributableData $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof AttributableData) {
            throw new InvalidArgumentException(sprintf(
                'The attributableData property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AttributableData, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->attributableData) && count($this->attributableData) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->attributableData)
            ), __LINE__);
        }
        $this->attributableData[] = $item;

        return $this;
    }
}
