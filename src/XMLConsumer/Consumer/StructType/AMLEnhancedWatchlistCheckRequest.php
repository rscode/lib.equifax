<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for AMLEnhancedWatchlistCheckRequest StructType
 *
 * @subpackage Structs
 */
class AMLEnhancedWatchlistCheckRequest extends CodedDataRequest
{
}
