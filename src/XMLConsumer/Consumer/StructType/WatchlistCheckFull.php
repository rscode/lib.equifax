<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for WatchlistCheckFull StructType
 *
 * @subpackage Structs
 */
class WatchlistCheckFull extends AbstractStructBase
{
    /**
     * The summaryReportReference
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var SummaryReportReference
     */
    protected SummaryReportReference $summaryReportReference;
    /**
     * The names
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var Names|null
     */
    protected ?Names $names = null;
    /**
     * The imageReferences
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var ImageReferences|null
     */
    protected ?ImageReferences $imageReferences = null;
    /**
     * The status
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var Status|null
     */
    protected ?Status $status = null;
    /**
     * The countryDetails
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var CountryDetails|null
     */
    protected ?CountryDetails $countryDetails = null;
    /**
     * The dowJonesWatchlist
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var DowJonesWatchlist|null
     */
    protected ?DowJonesWatchlist $dowJonesWatchlist = null;
    /**
     * The additionalDetails
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AdditionalDetails|null
     */
    protected ?AdditionalDetails $additionalDetails = null;
    /**
     * The relativesAndCloseAssociates
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var RelativesAndCloseAssociates|null
     */
    protected ?RelativesAndCloseAssociates $relativesAndCloseAssociates = null;
    /**
     * The profile
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var Profile|null
     */
    protected ?Profile $profile = null;

    /**
     * Constructor method for WatchlistCheckFull
     *
     * @param SummaryReportReference      $summaryReportReference
     * @param Names                       $names
     * @param ImageReferences             $imageReferences
     * @param Status                      $status
     * @param CountryDetails              $countryDetails
     * @param DowJonesWatchlist           $dowJonesWatchlist
     * @param AdditionalDetails           $additionalDetails
     * @param RelativesAndCloseAssociates $relativesAndCloseAssociates
     * @param Profile                     $profile
     * @uses WatchlistCheckFull::setSummaryReportReference()
     * @uses WatchlistCheckFull::setNames()
     * @uses WatchlistCheckFull::setImageReferences()
     * @uses WatchlistCheckFull::setStatus()
     * @uses WatchlistCheckFull::setCountryDetails()
     * @uses WatchlistCheckFull::setDowJonesWatchlist()
     * @uses WatchlistCheckFull::setAdditionalDetails()
     * @uses WatchlistCheckFull::setRelativesAndCloseAssociates()
     * @uses WatchlistCheckFull::setProfile()
     */
    public function __construct(
        SummaryReportReference $summaryReportReference,
        ?Names $names = null,
        ?ImageReferences $imageReferences = null,
        ?Status $status = null,
        ?CountryDetails $countryDetails = null,
        ?DowJonesWatchlist $dowJonesWatchlist = null,
        ?AdditionalDetails $additionalDetails = null,
        ?RelativesAndCloseAssociates $relativesAndCloseAssociates = null,
        ?Profile $profile = null
    ) {
        $this
            ->setSummaryReportReference($summaryReportReference)
            ->setNames($names)
            ->setImageReferences($imageReferences)
            ->setStatus($status)
            ->setCountryDetails($countryDetails)
            ->setDowJonesWatchlist($dowJonesWatchlist)
            ->setAdditionalDetails($additionalDetails)
            ->setRelativesAndCloseAssociates($relativesAndCloseAssociates)
            ->setProfile($profile);
    }

    /**
     * Get summaryReportReference value
     *
     * @return SummaryReportReference
     */
    public function getSummaryReportReference(): SummaryReportReference
    {
        return $this->summaryReportReference;
    }

    /**
     * Set summaryReportReference value
     *
     * @param SummaryReportReference $summaryReportReference
     * @return WatchlistCheckFull
     */
    public function setSummaryReportReference(SummaryReportReference $summaryReportReference): self
    {
        $this->summaryReportReference = $summaryReportReference;

        return $this;
    }

    /**
     * Get names value
     *
     * @return Names|null
     */
    public function getNames(): ?Names
    {
        return $this->names;
    }

    /**
     * Set names value
     *
     * @param Names $names
     * @return WatchlistCheckFull
     */
    public function setNames(?Names $names = null): self
    {
        $this->names = $names;

        return $this;
    }

    /**
     * Get imageReferences value
     *
     * @return ImageReferences|null
     */
    public function getImageReferences(): ?ImageReferences
    {
        return $this->imageReferences;
    }

    /**
     * Set imageReferences value
     *
     * @param ImageReferences $imageReferences
     * @return WatchlistCheckFull
     */
    public function setImageReferences(?ImageReferences $imageReferences = null): self
    {
        $this->imageReferences = $imageReferences;

        return $this;
    }

    /**
     * Get status value
     *
     * @return Status|null
     */
    public function getStatus(): ?Status
    {
        return $this->status;
    }

    /**
     * Set status value
     *
     * @param Status $status
     * @return WatchlistCheckFull
     */
    public function setStatus(?Status $status = null): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get countryDetails value
     *
     * @return CountryDetails|null
     */
    public function getCountryDetails(): ?CountryDetails
    {
        return $this->countryDetails;
    }

    /**
     * Set countryDetails value
     *
     * @param CountryDetails $countryDetails
     * @return WatchlistCheckFull
     */
    public function setCountryDetails(?CountryDetails $countryDetails = null): self
    {
        $this->countryDetails = $countryDetails;

        return $this;
    }

    /**
     * Get dowJonesWatchlist value
     *
     * @return DowJonesWatchlist|null
     */
    public function getDowJonesWatchlist(): ?DowJonesWatchlist
    {
        return $this->dowJonesWatchlist;
    }

    /**
     * Set dowJonesWatchlist value
     *
     * @param DowJonesWatchlist $dowJonesWatchlist
     * @return WatchlistCheckFull
     */
    public function setDowJonesWatchlist(?DowJonesWatchlist $dowJonesWatchlist = null): self
    {
        $this->dowJonesWatchlist = $dowJonesWatchlist;

        return $this;
    }

    /**
     * Get additionalDetails value
     *
     * @return AdditionalDetails|null
     */
    public function getAdditionalDetails(): ?AdditionalDetails
    {
        return $this->additionalDetails;
    }

    /**
     * Set additionalDetails value
     *
     * @param AdditionalDetails $additionalDetails
     * @return WatchlistCheckFull
     */
    public function setAdditionalDetails(?AdditionalDetails $additionalDetails = null): self
    {
        $this->additionalDetails = $additionalDetails;

        return $this;
    }

    /**
     * Get relativesAndCloseAssociates value
     *
     * @return RelativesAndCloseAssociates|null
     */
    public function getRelativesAndCloseAssociates(): ?RelativesAndCloseAssociates
    {
        return $this->relativesAndCloseAssociates;
    }

    /**
     * Set relativesAndCloseAssociates value
     *
     * @param RelativesAndCloseAssociates $relativesAndCloseAssociates
     * @return WatchlistCheckFull
     */
    public function setRelativesAndCloseAssociates(?RelativesAndCloseAssociates $relativesAndCloseAssociates = null): self
    {
        $this->relativesAndCloseAssociates = $relativesAndCloseAssociates;

        return $this;
    }

    /**
     * Get profile value
     *
     * @return Profile|null
     */
    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    /**
     * Set profile value
     *
     * @param Profile $profile
     * @return WatchlistCheckFull
     */
    public function setProfile(?Profile $profile = null): self
    {
        $this->profile = $profile;

        return $this;
    }
}
