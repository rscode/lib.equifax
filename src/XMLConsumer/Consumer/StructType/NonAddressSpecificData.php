<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for NonAddressSpecificData StructType
 *
 * @subpackage Structs
 */
class NonAddressSpecificData extends AbstractStructBase
{
    /**
     * The aliases
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AliasesContainer|null
     */
    protected ?AliasesContainer $aliases = null;
    /**
     * The associates
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AssociatesContainer|null
     */
    protected ?AssociatesContainer $associates = null;
    /**
     * The attributableData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AttributableDataContainer|null
     */
    protected ?AttributableDataContainer $attributableData = null;
    /**
     * The linkNamesNotifications
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var LinkNamesNotificationContainer|null
     */
    protected ?LinkNamesNotificationContainer $linkNamesNotifications = null;
    /**
     * The potentialAssociates
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var PotentialAssociatesContainer|null
     */
    protected ?PotentialAssociatesContainer $potentialAssociates = null;
    /**
     * The scores
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var ScoreContainer|null
     */
    protected ?ScoreContainer $scores = null;
    /**
     * The amlEnhancedWatchlistCheckOutcomes
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AmlEnhancedWatchlistCheckOutcomes|null
     */
    protected ?AmlEnhancedWatchlistCheckOutcomes $amlEnhancedWatchlistCheckOutcomes = null;
    /**
     * The amlEnhancedWatchlistCheckSummary
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AmlEnhancedWatchlistCheckSummary_1|null
     */
    protected ?AmlEnhancedWatchlistCheckSummary_1 $amlEnhancedWatchlistCheckSummary = null;
    /**
     * The attributes
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var Attributes|null
     */
    protected ?Attributes $attributes = null;
    /**
     * The associateLenderName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AssociateLenderNameContainer|null
     */
    protected ?AssociateLenderNameContainer $associateLenderName = null;
    /**
     * The potentialAssociateLenderName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var PotentialAssociateLenderNameContainer|null
     */
    protected ?PotentialAssociateLenderNameContainer $potentialAssociateLenderName = null;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for NonAddressSpecificData
     *
     * @param AliasesContainer                      $aliases
     * @param AssociatesContainer                   $associates
     * @param AttributableDataContainer             $attributableData
     * @param LinkNamesNotificationContainer        $linkNamesNotifications
     * @param PotentialAssociatesContainer          $potentialAssociates
     * @param ScoreContainer                        $scores
     * @param AmlEnhancedWatchlistCheckOutcomes     $amlEnhancedWatchlistCheckOutcomes
     * @param AmlEnhancedWatchlistCheckSummary_1    $amlEnhancedWatchlistCheckSummary
     * @param Attributes                            $attributes
     * @param AssociateLenderNameContainer          $associateLenderName
     * @param PotentialAssociateLenderNameContainer $potentialAssociateLenderName
     * @param DOMDocument|string|null               $any
     * @uses NonAddressSpecificData::setAliases()
     * @uses NonAddressSpecificData::setAssociates()
     * @uses NonAddressSpecificData::setAttributableData()
     * @uses NonAddressSpecificData::setLinkNamesNotifications()
     * @uses NonAddressSpecificData::setPotentialAssociates()
     * @uses NonAddressSpecificData::setScores()
     * @uses NonAddressSpecificData::setAmlEnhancedWatchlistCheckOutcomes()
     * @uses NonAddressSpecificData::setAmlEnhancedWatchlistCheckSummary()
     * @uses NonAddressSpecificData::setAttributes()
     * @uses NonAddressSpecificData::setAssociateLenderName()
     * @uses NonAddressSpecificData::setPotentialAssociateLenderName()
     * @uses NonAddressSpecificData::setAny()
     */
    public function __construct(
        ?AliasesContainer $aliases = null,
        ?AssociatesContainer $associates = null,
        ?AttributableDataContainer $attributableData = null,
        ?LinkNamesNotificationContainer $linkNamesNotifications = null,
        ?PotentialAssociatesContainer $potentialAssociates = null,
        ?ScoreContainer $scores = null,
        ?AmlEnhancedWatchlistCheckOutcomes $amlEnhancedWatchlistCheckOutcomes = null,
        ?AmlEnhancedWatchlistCheckSummary_1 $amlEnhancedWatchlistCheckSummary = null,
        ?Attributes $attributes = null,
        ?AssociateLenderNameContainer $associateLenderName = null,
        ?PotentialAssociateLenderNameContainer $potentialAssociateLenderName = null,
        $any = null
    ) {
        $this
            ->setAliases($aliases)
            ->setAssociates($associates)
            ->setAttributableData($attributableData)
            ->setLinkNamesNotifications($linkNamesNotifications)
            ->setPotentialAssociates($potentialAssociates)
            ->setScores($scores)
            ->setAmlEnhancedWatchlistCheckOutcomes($amlEnhancedWatchlistCheckOutcomes)
            ->setAmlEnhancedWatchlistCheckSummary($amlEnhancedWatchlistCheckSummary)
            ->setAttributes($attributes)
            ->setAssociateLenderName($associateLenderName)
            ->setPotentialAssociateLenderName($potentialAssociateLenderName)
            ->setAny($any);
    }

    /**
     * Get aliases value
     *
     * @return AliasesContainer|null
     */
    public function getAliases(): ?AliasesContainer
    {
        return $this->aliases;
    }

    /**
     * Set aliases value
     *
     * @param AliasesContainer $aliases
     * @return NonAddressSpecificData
     */
    public function setAliases(?AliasesContainer $aliases = null): self
    {
        $this->aliases = $aliases;

        return $this;
    }

    /**
     * Get associates value
     *
     * @return AssociatesContainer|null
     */
    public function getAssociates(): ?AssociatesContainer
    {
        return $this->associates;
    }

    /**
     * Set associates value
     *
     * @param AssociatesContainer $associates
     * @return NonAddressSpecificData
     */
    public function setAssociates(?AssociatesContainer $associates = null): self
    {
        $this->associates = $associates;

        return $this;
    }

    /**
     * Get attributableData value
     *
     * @return AttributableDataContainer|null
     */
    public function getAttributableData(): ?AttributableDataContainer
    {
        return $this->attributableData;
    }

    /**
     * Set attributableData value
     *
     * @param AttributableDataContainer $attributableData
     * @return NonAddressSpecificData
     */
    public function setAttributableData(?AttributableDataContainer $attributableData = null): self
    {
        $this->attributableData = $attributableData;

        return $this;
    }

    /**
     * Get linkNamesNotifications value
     *
     * @return LinkNamesNotificationContainer|null
     */
    public function getLinkNamesNotifications(): ?LinkNamesNotificationContainer
    {
        return $this->linkNamesNotifications;
    }

    /**
     * Set linkNamesNotifications value
     *
     * @param LinkNamesNotificationContainer $linkNamesNotifications
     * @return NonAddressSpecificData
     */
    public function setLinkNamesNotifications(?LinkNamesNotificationContainer $linkNamesNotifications = null): self
    {
        $this->linkNamesNotifications = $linkNamesNotifications;

        return $this;
    }

    /**
     * Get potentialAssociates value
     *
     * @return PotentialAssociatesContainer|null
     */
    public function getPotentialAssociates(): ?PotentialAssociatesContainer
    {
        return $this->potentialAssociates;
    }

    /**
     * Set potentialAssociates value
     *
     * @param PotentialAssociatesContainer $potentialAssociates
     * @return NonAddressSpecificData
     */
    public function setPotentialAssociates(?PotentialAssociatesContainer $potentialAssociates = null): self
    {
        $this->potentialAssociates = $potentialAssociates;

        return $this;
    }

    /**
     * Get scores value
     *
     * @return ScoreContainer|null
     */
    public function getScores(): ?ScoreContainer
    {
        return $this->scores;
    }

    /**
     * Set scores value
     *
     * @param ScoreContainer $scores
     * @return NonAddressSpecificData
     */
    public function setScores(?ScoreContainer $scores = null): self
    {
        $this->scores = $scores;

        return $this;
    }

    /**
     * Get amlEnhancedWatchlistCheckOutcomes value
     *
     * @return AmlEnhancedWatchlistCheckOutcomes|null
     */
    public function getAmlEnhancedWatchlistCheckOutcomes(): ?AmlEnhancedWatchlistCheckOutcomes
    {
        return $this->amlEnhancedWatchlistCheckOutcomes;
    }

    /**
     * Set amlEnhancedWatchlistCheckOutcomes value
     *
     * @param AmlEnhancedWatchlistCheckOutcomes $amlEnhancedWatchlistCheckOutcomes
     * @return NonAddressSpecificData
     */
    public function setAmlEnhancedWatchlistCheckOutcomes(?AmlEnhancedWatchlistCheckOutcomes $amlEnhancedWatchlistCheckOutcomes = null): self
    {
        $this->amlEnhancedWatchlistCheckOutcomes = $amlEnhancedWatchlistCheckOutcomes;

        return $this;
    }

    /**
     * Get amlEnhancedWatchlistCheckSummary value
     *
     * @return AmlEnhancedWatchlistCheckSummary_1|null
     */
    public function getAmlEnhancedWatchlistCheckSummary(): ?AmlEnhancedWatchlistCheckSummary_1
    {
        return $this->amlEnhancedWatchlistCheckSummary;
    }

    /**
     * Set amlEnhancedWatchlistCheckSummary value
     *
     * @param AmlEnhancedWatchlistCheckSummary_1 $amlEnhancedWatchlistCheckSummary
     * @return NonAddressSpecificData
     */
    public function setAmlEnhancedWatchlistCheckSummary(?AmlEnhancedWatchlistCheckSummary_1 $amlEnhancedWatchlistCheckSummary = null): self
    {
        $this->amlEnhancedWatchlistCheckSummary = $amlEnhancedWatchlistCheckSummary;

        return $this;
    }

    /**
     * Get attributes value
     *
     * @return Attributes|null
     */
    public function getAttributes(): ?Attributes
    {
        return $this->attributes;
    }

    /**
     * Set attributes value
     *
     * @param Attributes $attributes
     * @return NonAddressSpecificData
     */
    public function setAttributes(?Attributes $attributes = null): self
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * Get associateLenderName value
     *
     * @return AssociateLenderNameContainer|null
     */
    public function getAssociateLenderName(): ?AssociateLenderNameContainer
    {
        return $this->associateLenderName;
    }

    /**
     * Set associateLenderName value
     *
     * @param AssociateLenderNameContainer $associateLenderName
     * @return NonAddressSpecificData
     */
    public function setAssociateLenderName(?AssociateLenderNameContainer $associateLenderName = null): self
    {
        $this->associateLenderName = $associateLenderName;

        return $this;
    }

    /**
     * Get potentialAssociateLenderName value
     *
     * @return PotentialAssociateLenderNameContainer|null
     */
    public function getPotentialAssociateLenderName(): ?PotentialAssociateLenderNameContainer
    {
        return $this->potentialAssociateLenderName;
    }

    /**
     * Set potentialAssociateLenderName value
     *
     * @param PotentialAssociateLenderNameContainer $potentialAssociateLenderName
     * @return NonAddressSpecificData
     */
    public function setPotentialAssociateLenderName(?PotentialAssociateLenderNameContainer $potentialAssociateLenderName = null): self
    {
        $this->potentialAssociateLenderName = $potentialAssociateLenderName;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return NonAddressSpecificData
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
