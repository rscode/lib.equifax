<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Employer StructType
 *
 * @subpackage Structs
 */
class Employer extends AbstractStructBase
{
    /**
     * The name
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $name;
    /**
     * The address
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var StructuredAddress|null
     */
    protected ?StructuredAddress $address = null;
    /**
     * The telephoneNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var TelephoneNumber|null
     */
    protected ?TelephoneNumber $telephoneNumber = null;

    /**
     * Constructor method for Employer
     *
     * @param string            $name
     * @param StructuredAddress $address
     * @param TelephoneNumber   $telephoneNumber
     * @uses Employer::setName()
     * @uses Employer::setAddress()
     * @uses Employer::setTelephoneNumber()
     */
    public function __construct(
        string $name,
        ?StructuredAddress $address = null,
        ?TelephoneNumber $telephoneNumber = null
    ) {
        $this
            ->setName($name)
            ->setAddress($address)
            ->setTelephoneNumber($telephoneNumber);
    }

    /**
     * Get name value
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set name value
     *
     * @param string $name
     * @return Employer
     */
    public function setName(string $name): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($name, true),
                gettype($name)
            ), __LINE__);
        }
        $this->name = $name;

        return $this;
    }

    /**
     * Get address value
     *
     * @return StructuredAddress|null
     */
    public function getAddress(): ?StructuredAddress
    {
        return $this->address;
    }

    /**
     * Set address value
     *
     * @param StructuredAddress $address
     * @return Employer
     */
    public function setAddress(?StructuredAddress $address = null): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get telephoneNumber value
     *
     * @return TelephoneNumber|null
     */
    public function getTelephoneNumber(): ?TelephoneNumber
    {
        return $this->telephoneNumber;
    }

    /**
     * Set telephoneNumber value
     *
     * @param TelephoneNumber $telephoneNumber
     * @return Employer
     */
    public function setTelephoneNumber(?TelephoneNumber $telephoneNumber = null): self
    {
        $this->telephoneNumber = $telephoneNumber;

        return $this;
    }
}
