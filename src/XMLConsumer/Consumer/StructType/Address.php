<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Address StructType
 *
 * @subpackage Structs
 */
abstract class Address extends AbstractStructBase
{
    /**
     * The id
     * Meta information extracted from the WSDL
     * - use: optional
     *
     * @var string|null
     */
    protected ?string $id = null;
    /**
     * The ref
     * Meta information extracted from the WSDL
     * - use: optional
     *
     * @var string|null
     */
    protected ?string $ref = null;

    /**
     * Constructor method for Address
     *
     * @param string $id
     * @param string $ref
     * @uses Address::setId()
     * @uses Address::setRef()
     */
    public function __construct(?string $id = null, ?string $ref = null)
    {
        $this
            ->setId($id)
            ->setRef($ref);
    }

    /**
     * Get id value
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * Set id value
     *
     * @param string $id
     * @return Address
     */
    public function setId(?string $id = null): self
    {
        // validation for constraint: string
        if (!is_null($id) && !is_string($id)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($id, true),
                gettype($id)
            ), __LINE__);
        }
        $this->id = $id;

        return $this;
    }

    /**
     * Get ref value
     *
     * @return string|null
     */
    public function getRef(): ?string
    {
        return $this->ref;
    }

    /**
     * Set ref value
     *
     * @param string $ref
     * @return Address
     */
    public function setRef(?string $ref = null): self
    {
        // validation for constraint: string
        if (!is_null($ref) && !is_string($ref)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($ref, true),
                gettype($ref)
            ), __LINE__);
        }
        $this->ref = $ref;

        return $this;
    }
}
