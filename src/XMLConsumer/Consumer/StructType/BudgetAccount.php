<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for BudgetAccount StructType
 *
 * @subpackage Structs
 */
class BudgetAccount extends FinancialAgreement
{
}
