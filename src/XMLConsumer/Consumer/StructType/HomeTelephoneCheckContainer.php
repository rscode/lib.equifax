<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for HomeTelephoneCheckContainer StructType
 *
 * @subpackage Structs
 */
class HomeTelephoneCheckContainer extends DataItemContainer
{
    /**
     * The homeTelephoneCheck
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var HomeTelephoneCheck|null
     */
    protected ?HomeTelephoneCheck $homeTelephoneCheck = null;

    /**
     * Constructor method for HomeTelephoneCheckContainer
     *
     * @param HomeTelephoneCheck $homeTelephoneCheck
     * @uses HomeTelephoneCheckContainer::setHomeTelephoneCheck()
     */
    public function __construct(?HomeTelephoneCheck $homeTelephoneCheck = null)
    {
        $this
            ->setHomeTelephoneCheck($homeTelephoneCheck);
    }

    /**
     * Get homeTelephoneCheck value
     *
     * @return HomeTelephoneCheck|null
     */
    public function getHomeTelephoneCheck(): ?HomeTelephoneCheck
    {
        return $this->homeTelephoneCheck;
    }

    /**
     * Set homeTelephoneCheck value
     *
     * @param HomeTelephoneCheck $homeTelephoneCheck
     * @return HomeTelephoneCheckContainer
     */
    public function setHomeTelephoneCheck(?HomeTelephoneCheck $homeTelephoneCheck = null): self
    {
        $this->homeTelephoneCheck = $homeTelephoneCheck;

        return $this;
    }
}
