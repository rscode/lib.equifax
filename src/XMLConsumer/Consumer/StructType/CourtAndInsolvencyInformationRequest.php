<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for CourtAndInsolvencyInformationRequest StructType
 *
 * @subpackage Structs
 */
class CourtAndInsolvencyInformationRequest extends RawDataAvailableAtLinkedAddressRequest
{
}
