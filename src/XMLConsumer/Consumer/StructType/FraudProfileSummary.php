<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\CIFASPlusMigrationStatus;

/**
 * This class stands for FraudProfileSummary StructType
 * Meta information extracted from the WSDL
 * - documentation: Provides a summary of anti-fraud checks relevant to fraud prevention.
 *
 * @subpackage Structs
 */
class FraudProfileSummary extends DataItem
{
    /**
     * The alertCount
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 999
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $alertCount;
    /**
     * The appsReferencedCount
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 999
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $appsReferencedCount;
    /**
     * The bankMatch
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $bankMatch;
    /**
     * The cifasPlusMigrationStatus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $cifasPlusMigrationStatus;
    /**
     * The dataFailCount
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 999
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $dataFailCount;
    /**
     * The electoralRollDOBMatch
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $electoralRollDOBMatch;
    /**
     * The employerMatch
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $employerMatch;
    /**
     * The equifaxLinkedNext
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $equifaxLinkedNext;
    /**
     * The homePhoneMatch
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $homePhoneMatch;
    /**
     * The insightDOBMatch
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $insightDOBMatch;
    /**
     * The onCIFAS
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $onCIFAS;
    /**
     * The onCIFASPlus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $onCIFASPlus;
    /**
     * The onHaloDeceased
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $onHaloDeceased;
    /**
     * The onONSDeceased
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $onONSDeceased;
    /**
     * The passportInfoVerified
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $passportInfoVerified;
    /**
     * The royalMailForwardingAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $royalMailForwardingAddress;
    /**
     * The rulesHitCount
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 999
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $rulesHitCount;
    /**
     * The referenceNumber
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 0
     * - pattern: [0-9]{1,10}
     *
     * @var int|null
     */
    protected ?int $referenceNumber = null;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for FraudProfileSummary
     *
     * @param int                     $alertCount
     * @param int                     $appsReferencedCount
     * @param bool                    $bankMatch
     * @param string                  $cifasPlusMigrationStatus
     * @param int                     $dataFailCount
     * @param bool                    $electoralRollDOBMatch
     * @param bool                    $employerMatch
     * @param bool                    $equifaxLinkedNext
     * @param bool                    $homePhoneMatch
     * @param bool                    $insightDOBMatch
     * @param bool                    $onCIFAS
     * @param bool                    $onCIFASPlus
     * @param bool                    $onHaloDeceased
     * @param bool                    $onONSDeceased
     * @param bool                    $passportInfoVerified
     * @param bool                    $royalMailForwardingAddress
     * @param int                     $rulesHitCount
     * @param int                     $referenceNumber
     * @param DOMDocument|string|null $any
     * @uses FraudProfileSummary::setAlertCount()
     * @uses FraudProfileSummary::setAppsReferencedCount()
     * @uses FraudProfileSummary::setBankMatch()
     * @uses FraudProfileSummary::setCifasPlusMigrationStatus()
     * @uses FraudProfileSummary::setDataFailCount()
     * @uses FraudProfileSummary::setElectoralRollDOBMatch()
     * @uses FraudProfileSummary::setEmployerMatch()
     * @uses FraudProfileSummary::setEquifaxLinkedNext()
     * @uses FraudProfileSummary::setHomePhoneMatch()
     * @uses FraudProfileSummary::setInsightDOBMatch()
     * @uses FraudProfileSummary::setOnCIFAS()
     * @uses FraudProfileSummary::setOnCIFASPlus()
     * @uses FraudProfileSummary::setOnHaloDeceased()
     * @uses FraudProfileSummary::setOnONSDeceased()
     * @uses FraudProfileSummary::setPassportInfoVerified()
     * @uses FraudProfileSummary::setRoyalMailForwardingAddress()
     * @uses FraudProfileSummary::setRulesHitCount()
     * @uses FraudProfileSummary::setReferenceNumber()
     * @uses FraudProfileSummary::setAny()
     */
    public function __construct(
        int $alertCount,
        int $appsReferencedCount,
        bool $bankMatch,
        string $cifasPlusMigrationStatus,
        int $dataFailCount,
        bool $electoralRollDOBMatch,
        bool $employerMatch,
        bool $equifaxLinkedNext,
        bool $homePhoneMatch,
        bool $insightDOBMatch,
        bool $onCIFAS,
        bool $onCIFASPlus,
        bool $onHaloDeceased,
        bool $onONSDeceased,
        bool $passportInfoVerified,
        bool $royalMailForwardingAddress,
        int $rulesHitCount,
        ?int $referenceNumber = null,
        $any = null
    ) {
        $this
            ->setAlertCount($alertCount)
            ->setAppsReferencedCount($appsReferencedCount)
            ->setBankMatch($bankMatch)
            ->setCifasPlusMigrationStatus($cifasPlusMigrationStatus)
            ->setDataFailCount($dataFailCount)
            ->setElectoralRollDOBMatch($electoralRollDOBMatch)
            ->setEmployerMatch($employerMatch)
            ->setEquifaxLinkedNext($equifaxLinkedNext)
            ->setHomePhoneMatch($homePhoneMatch)
            ->setInsightDOBMatch($insightDOBMatch)
            ->setOnCIFAS($onCIFAS)
            ->setOnCIFASPlus($onCIFASPlus)
            ->setOnHaloDeceased($onHaloDeceased)
            ->setOnONSDeceased($onONSDeceased)
            ->setPassportInfoVerified($passportInfoVerified)
            ->setRoyalMailForwardingAddress($royalMailForwardingAddress)
            ->setRulesHitCount($rulesHitCount)
            ->setReferenceNumber($referenceNumber)
            ->setAny($any);
    }

    /**
     * Get alertCount value
     *
     * @return int
     */
    public function getAlertCount(): int
    {
        return $this->alertCount;
    }

    /**
     * Set alertCount value
     *
     * @param int $alertCount
     * @return FraudProfileSummary
     */
    public function setAlertCount(int $alertCount): self
    {
        // validation for constraint: int
        if (!is_null($alertCount) && !(is_int($alertCount) || ctype_digit($alertCount))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($alertCount, true),
                gettype($alertCount)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(999)
        if (!is_null($alertCount) && $alertCount > 999) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 999',
                var_export($alertCount, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($alertCount) && $alertCount < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($alertCount, true)
            ), __LINE__);
        }
        $this->alertCount = $alertCount;

        return $this;
    }

    /**
     * Get appsReferencedCount value
     *
     * @return int
     */
    public function getAppsReferencedCount(): int
    {
        return $this->appsReferencedCount;
    }

    /**
     * Set appsReferencedCount value
     *
     * @param int $appsReferencedCount
     * @return FraudProfileSummary
     */
    public function setAppsReferencedCount(int $appsReferencedCount): self
    {
        // validation for constraint: int
        if (!is_null($appsReferencedCount) && !(is_int($appsReferencedCount) || ctype_digit($appsReferencedCount))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($appsReferencedCount, true),
                gettype($appsReferencedCount)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(999)
        if (!is_null($appsReferencedCount) && $appsReferencedCount > 999) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 999',
                var_export($appsReferencedCount, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($appsReferencedCount) && $appsReferencedCount < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($appsReferencedCount, true)
            ), __LINE__);
        }
        $this->appsReferencedCount = $appsReferencedCount;

        return $this;
    }

    /**
     * Get bankMatch value
     *
     * @return bool
     */
    public function getBankMatch(): bool
    {
        return $this->bankMatch;
    }

    /**
     * Set bankMatch value
     *
     * @param bool $bankMatch
     * @return FraudProfileSummary
     */
    public function setBankMatch(bool $bankMatch): self
    {
        // validation for constraint: boolean
        if (!is_null($bankMatch) && !is_bool($bankMatch)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($bankMatch, true),
                gettype($bankMatch)
            ), __LINE__);
        }
        $this->bankMatch = $bankMatch;

        return $this;
    }

    /**
     * Get cifasPlusMigrationStatus value
     *
     * @return string
     */
    public function getCifasPlusMigrationStatus(): string
    {
        return $this->cifasPlusMigrationStatus;
    }

    /**
     * Set cifasPlusMigrationStatus value
     *
     * @param string $cifasPlusMigrationStatus
     * @return FraudProfileSummary
     * @throws InvalidArgumentException
     * @uses CIFASPlusMigrationStatus::getValidValues
     * @uses CIFASPlusMigrationStatus::valueIsValid
     */
    public function setCifasPlusMigrationStatus(string $cifasPlusMigrationStatus): self
    {
        // validation for constraint: enumeration
        if (!CIFASPlusMigrationStatus::valueIsValid($cifasPlusMigrationStatus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\CIFASPlusMigrationStatus',
                is_array($cifasPlusMigrationStatus) ? implode(', ', $cifasPlusMigrationStatus) : var_export(
                    $cifasPlusMigrationStatus,
                    true
                ),
                implode(', ', CIFASPlusMigrationStatus::getValidValues())
            ), __LINE__);
        }
        $this->cifasPlusMigrationStatus = $cifasPlusMigrationStatus;

        return $this;
    }

    /**
     * Get dataFailCount value
     *
     * @return int
     */
    public function getDataFailCount(): int
    {
        return $this->dataFailCount;
    }

    /**
     * Set dataFailCount value
     *
     * @param int $dataFailCount
     * @return FraudProfileSummary
     */
    public function setDataFailCount(int $dataFailCount): self
    {
        // validation for constraint: int
        if (!is_null($dataFailCount) && !(is_int($dataFailCount) || ctype_digit($dataFailCount))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($dataFailCount, true),
                gettype($dataFailCount)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(999)
        if (!is_null($dataFailCount) && $dataFailCount > 999) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 999',
                var_export($dataFailCount, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($dataFailCount) && $dataFailCount < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($dataFailCount, true)
            ), __LINE__);
        }
        $this->dataFailCount = $dataFailCount;

        return $this;
    }

    /**
     * Get electoralRollDOBMatch value
     *
     * @return bool
     */
    public function getElectoralRollDOBMatch(): bool
    {
        return $this->electoralRollDOBMatch;
    }

    /**
     * Set electoralRollDOBMatch value
     *
     * @param bool $electoralRollDOBMatch
     * @return FraudProfileSummary
     */
    public function setElectoralRollDOBMatch(bool $electoralRollDOBMatch): self
    {
        // validation for constraint: boolean
        if (!is_null($electoralRollDOBMatch) && !is_bool($electoralRollDOBMatch)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($electoralRollDOBMatch, true),
                gettype($electoralRollDOBMatch)
            ), __LINE__);
        }
        $this->electoralRollDOBMatch = $electoralRollDOBMatch;

        return $this;
    }

    /**
     * Get employerMatch value
     *
     * @return bool
     */
    public function getEmployerMatch(): bool
    {
        return $this->employerMatch;
    }

    /**
     * Set employerMatch value
     *
     * @param bool $employerMatch
     * @return FraudProfileSummary
     */
    public function setEmployerMatch(bool $employerMatch): self
    {
        // validation for constraint: boolean
        if (!is_null($employerMatch) && !is_bool($employerMatch)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($employerMatch, true),
                gettype($employerMatch)
            ), __LINE__);
        }
        $this->employerMatch = $employerMatch;

        return $this;
    }

    /**
     * Get equifaxLinkedNext value
     *
     * @return bool
     */
    public function getEquifaxLinkedNext(): bool
    {
        return $this->equifaxLinkedNext;
    }

    /**
     * Set equifaxLinkedNext value
     *
     * @param bool $equifaxLinkedNext
     * @return FraudProfileSummary
     */
    public function setEquifaxLinkedNext(bool $equifaxLinkedNext): self
    {
        // validation for constraint: boolean
        if (!is_null($equifaxLinkedNext) && !is_bool($equifaxLinkedNext)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($equifaxLinkedNext, true),
                gettype($equifaxLinkedNext)
            ), __LINE__);
        }
        $this->equifaxLinkedNext = $equifaxLinkedNext;

        return $this;
    }

    /**
     * Get homePhoneMatch value
     *
     * @return bool
     */
    public function getHomePhoneMatch(): bool
    {
        return $this->homePhoneMatch;
    }

    /**
     * Set homePhoneMatch value
     *
     * @param bool $homePhoneMatch
     * @return FraudProfileSummary
     */
    public function setHomePhoneMatch(bool $homePhoneMatch): self
    {
        // validation for constraint: boolean
        if (!is_null($homePhoneMatch) && !is_bool($homePhoneMatch)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($homePhoneMatch, true),
                gettype($homePhoneMatch)
            ), __LINE__);
        }
        $this->homePhoneMatch = $homePhoneMatch;

        return $this;
    }

    /**
     * Get insightDOBMatch value
     *
     * @return bool
     */
    public function getInsightDOBMatch(): bool
    {
        return $this->insightDOBMatch;
    }

    /**
     * Set insightDOBMatch value
     *
     * @param bool $insightDOBMatch
     * @return FraudProfileSummary
     */
    public function setInsightDOBMatch(bool $insightDOBMatch): self
    {
        // validation for constraint: boolean
        if (!is_null($insightDOBMatch) && !is_bool($insightDOBMatch)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($insightDOBMatch, true),
                gettype($insightDOBMatch)
            ), __LINE__);
        }
        $this->insightDOBMatch = $insightDOBMatch;

        return $this;
    }

    /**
     * Get onCIFAS value
     *
     * @return bool
     */
    public function getOnCIFAS(): bool
    {
        return $this->onCIFAS;
    }

    /**
     * Set onCIFAS value
     *
     * @param bool $onCIFAS
     * @return FraudProfileSummary
     */
    public function setOnCIFAS(bool $onCIFAS): self
    {
        // validation for constraint: boolean
        if (!is_null($onCIFAS) && !is_bool($onCIFAS)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($onCIFAS, true),
                gettype($onCIFAS)
            ), __LINE__);
        }
        $this->onCIFAS = $onCIFAS;

        return $this;
    }

    /**
     * Get onCIFASPlus value
     *
     * @return bool
     */
    public function getOnCIFASPlus(): bool
    {
        return $this->onCIFASPlus;
    }

    /**
     * Set onCIFASPlus value
     *
     * @param bool $onCIFASPlus
     * @return FraudProfileSummary
     */
    public function setOnCIFASPlus(bool $onCIFASPlus): self
    {
        // validation for constraint: boolean
        if (!is_null($onCIFASPlus) && !is_bool($onCIFASPlus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($onCIFASPlus, true),
                gettype($onCIFASPlus)
            ), __LINE__);
        }
        $this->onCIFASPlus = $onCIFASPlus;

        return $this;
    }

    /**
     * Get onHaloDeceased value
     *
     * @return bool
     */
    public function getOnHaloDeceased(): bool
    {
        return $this->onHaloDeceased;
    }

    /**
     * Set onHaloDeceased value
     *
     * @param bool $onHaloDeceased
     * @return FraudProfileSummary
     */
    public function setOnHaloDeceased(bool $onHaloDeceased): self
    {
        // validation for constraint: boolean
        if (!is_null($onHaloDeceased) && !is_bool($onHaloDeceased)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($onHaloDeceased, true),
                gettype($onHaloDeceased)
            ), __LINE__);
        }
        $this->onHaloDeceased = $onHaloDeceased;

        return $this;
    }

    /**
     * Get onONSDeceased value
     *
     * @return bool
     */
    public function getOnONSDeceased(): bool
    {
        return $this->onONSDeceased;
    }

    /**
     * Set onONSDeceased value
     *
     * @param bool $onONSDeceased
     * @return FraudProfileSummary
     */
    public function setOnONSDeceased(bool $onONSDeceased): self
    {
        // validation for constraint: boolean
        if (!is_null($onONSDeceased) && !is_bool($onONSDeceased)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($onONSDeceased, true),
                gettype($onONSDeceased)
            ), __LINE__);
        }
        $this->onONSDeceased = $onONSDeceased;

        return $this;
    }

    /**
     * Get passportInfoVerified value
     *
     * @return bool
     */
    public function getPassportInfoVerified(): bool
    {
        return $this->passportInfoVerified;
    }

    /**
     * Set passportInfoVerified value
     *
     * @param bool $passportInfoVerified
     * @return FraudProfileSummary
     */
    public function setPassportInfoVerified(bool $passportInfoVerified): self
    {
        // validation for constraint: boolean
        if (!is_null($passportInfoVerified) && !is_bool($passportInfoVerified)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($passportInfoVerified, true),
                gettype($passportInfoVerified)
            ), __LINE__);
        }
        $this->passportInfoVerified = $passportInfoVerified;

        return $this;
    }

    /**
     * Get royalMailForwardingAddress value
     *
     * @return bool
     */
    public function getRoyalMailForwardingAddress(): bool
    {
        return $this->royalMailForwardingAddress;
    }

    /**
     * Set royalMailForwardingAddress value
     *
     * @param bool $royalMailForwardingAddress
     * @return FraudProfileSummary
     */
    public function setRoyalMailForwardingAddress(bool $royalMailForwardingAddress): self
    {
        // validation for constraint: boolean
        if (!is_null($royalMailForwardingAddress) && !is_bool($royalMailForwardingAddress)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($royalMailForwardingAddress, true),
                gettype($royalMailForwardingAddress)
            ), __LINE__);
        }
        $this->royalMailForwardingAddress = $royalMailForwardingAddress;

        return $this;
    }

    /**
     * Get rulesHitCount value
     *
     * @return int
     */
    public function getRulesHitCount(): int
    {
        return $this->rulesHitCount;
    }

    /**
     * Set rulesHitCount value
     *
     * @param int $rulesHitCount
     * @return FraudProfileSummary
     */
    public function setRulesHitCount(int $rulesHitCount): self
    {
        // validation for constraint: int
        if (!is_null($rulesHitCount) && !(is_int($rulesHitCount) || ctype_digit($rulesHitCount))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($rulesHitCount, true),
                gettype($rulesHitCount)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(999)
        if (!is_null($rulesHitCount) && $rulesHitCount > 999) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 999',
                var_export($rulesHitCount, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($rulesHitCount) && $rulesHitCount < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($rulesHitCount, true)
            ), __LINE__);
        }
        $this->rulesHitCount = $rulesHitCount;

        return $this;
    }

    /**
     * Get referenceNumber value
     *
     * @return int|null
     */
    public function getReferenceNumber(): ?int
    {
        return $this->referenceNumber;
    }

    /**
     * Set referenceNumber value
     *
     * @param int $referenceNumber
     * @return FraudProfileSummary
     */
    public function setReferenceNumber(?int $referenceNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($referenceNumber) && !(is_int($referenceNumber) || ctype_digit($referenceNumber))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($referenceNumber, true),
                gettype($referenceNumber)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($referenceNumber) && $referenceNumber < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($referenceNumber, true)
            ), __LINE__);
        }
        // validation for constraint: pattern([0-9]{1,10})
        if (!is_null($referenceNumber) && !preg_match('/[0-9]{1,10}/', $referenceNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[0-9]{1,10}/',
                var_export($referenceNumber, true)
            ), __LINE__);
        }
        $this->referenceNumber = $referenceNumber;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return FraudProfileSummary
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
