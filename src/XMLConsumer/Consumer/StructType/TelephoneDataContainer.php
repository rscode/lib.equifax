<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for TelephoneDataContainer StructType
 *
 * @subpackage Structs
 */
class TelephoneDataContainer extends DataItemContainer
{
    /**
     * The telephoneData
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var TelephoneData[]
     */
    protected ?array $telephoneData = null;

    /**
     * Constructor method for TelephoneDataContainer
     *
     * @param TelephoneData[] $telephoneData
     * @uses TelephoneDataContainer::setTelephoneData()
     */
    public function __construct(?array $telephoneData = null)
    {
        $this
            ->setTelephoneData($telephoneData);
    }

    /**
     * This method is responsible for validating the values passed to the setTelephoneData method
     * This method is willingly generated in order to preserve the one-line inline validation within the setTelephoneData method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateTelephoneDataForArrayConstraintsFromSetTelephoneData(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $telephoneDataContainerTelephoneDataItem) {
            // validation for constraint: itemType
            if (!$telephoneDataContainerTelephoneDataItem instanceof TelephoneData) {
                $invalidValues[] = is_object($telephoneDataContainerTelephoneDataItem) ? get_class($telephoneDataContainerTelephoneDataItem) : sprintf(
                    '%s(%s)',
                    gettype($telephoneDataContainerTelephoneDataItem),
                    var_export($telephoneDataContainerTelephoneDataItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The telephoneData property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\TelephoneData, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get telephoneData value
     *
     * @return TelephoneData[]
     */
    public function getTelephoneData(): ?array
    {
        return $this->telephoneData;
    }

    /**
     * Set telephoneData value
     *
     * @param TelephoneData[] $telephoneData
     * @return TelephoneDataContainer
     * @throws InvalidArgumentException
     */
    public function setTelephoneData(?array $telephoneData = null): self
    {
        // validation for constraint: array
        if ('' !== ($telephoneDataArrayErrorMessage = self::validateTelephoneDataForArrayConstraintsFromSetTelephoneData($telephoneData))) {
            throw new InvalidArgumentException($telephoneDataArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($telephoneData) && count($telephoneData) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($telephoneData)
            ), __LINE__);
        }
        $this->telephoneData = $telephoneData;

        return $this;
    }

    /**
     * Add item to telephoneData value
     *
     * @param TelephoneData $item
     * @return TelephoneDataContainer
     * @throws InvalidArgumentException
     */
    public function addToTelephoneData(TelephoneData $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof TelephoneData) {
            throw new InvalidArgumentException(sprintf(
                'The telephoneData property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\TelephoneData, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->telephoneData) && count($this->telephoneData) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->telephoneData)
            ), __LINE__);
        }
        $this->telephoneData[] = $item;

        return $this;
    }
}
