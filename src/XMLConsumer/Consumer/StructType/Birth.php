<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Birth StructType
 *
 * @subpackage Structs
 */
class Birth extends AbstractStructBase
{
    /**
     * The dob
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var string[]
     */
    protected ?array $dob = null;

    /**
     * Constructor method for Birth
     *
     * @param string[] $dob
     * @uses Birth::setDob()
     */
    public function __construct(?array $dob = null)
    {
        $this
            ->setDob($dob);
    }

    /**
     * This method is responsible for validating the values passed to the setDob method
     * This method is willingly generated in order to preserve the one-line inline validation within the setDob method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateDobForArrayConstraintsFromSetDob(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $birthDobItem) {
            // validation for constraint: itemType
            if (!is_string($birthDobItem)) {
                $invalidValues[] = is_object($birthDobItem) ? get_class($birthDobItem) : sprintf(
                    '%s(%s)',
                    gettype($birthDobItem),
                    var_export($birthDobItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The dob property can only contain items of type string, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get dob value
     *
     * @return string[]
     */
    public function getDob(): ?array
    {
        return $this->dob;
    }

    /**
     * Set dob value
     *
     * @param string[] $dob
     * @return Birth
     * @throws InvalidArgumentException
     */
    public function setDob(?array $dob = null): self
    {
        // validation for constraint: array
        if ('' !== ($dobArrayErrorMessage = self::validateDobForArrayConstraintsFromSetDob($dob))) {
            throw new InvalidArgumentException($dobArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($dob) && count($dob) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($dob)
            ), __LINE__);
        }
        $this->dob = $dob;

        return $this;
    }

    /**
     * Add item to dob value
     *
     * @param string $item
     * @return Birth
     * @throws InvalidArgumentException
     */
    public function addToDob(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf(
                'The dob property can only contain items of type string, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->dob) && count($this->dob) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->dob)
            ), __LINE__);
        }
        $this->dob[] = $item;

        return $this;
    }
}
