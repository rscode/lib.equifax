<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for AssociateRequest StructType
 *
 * @subpackage Structs
 */
class AssociateRequest extends RawDataRequest
{
    /**
     * The lenderName
     *
     * @var bool|null
     */
    protected ?bool $lenderName = null;

    public function __construct(int $maxNumber, ?bool $lenderName = null)
    {
        parent::__construct($maxNumber);

        $this
            ->setLenderName($lenderName);
    }

    /**
     * Get lenderName value
     *
     * @return bool|null
     */
    public function getLenderName(): ?bool
    {
        return $this->lenderName;
    }

    /**
     * Set lenderName value
     *
     * @param bool $lenderName
     * @return AssociateRequest
     */
    public function setLenderName(?bool $lenderName = null): self
    {
        // validation for constraint: boolean
        if (!is_null($lenderName) && !is_bool($lenderName)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($lenderName, true),
                gettype($lenderName)
            ), __LINE__);
        }
        $this->lenderName = $lenderName;

        return $this;
    }
}
