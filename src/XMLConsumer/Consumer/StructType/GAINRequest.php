<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for GAINRequest StructType
 *
 * @subpackage Structs
 */
class GAINRequest extends RawDataAvailableAtLinkedAddressRequest
{
}
