<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for UncategorisedFinancialAgreement StructType
 *
 * @subpackage Structs
 */
class UncategorisedFinancialAgreement extends FinancialAgreement
{
}
