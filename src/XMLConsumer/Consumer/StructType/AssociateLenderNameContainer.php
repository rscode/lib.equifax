<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for AssociateLenderNameContainer StructType
 *
 * @subpackage Structs
 */
class AssociateLenderNameContainer extends DataItemContainer
{
    /**
     * The associateLenderName
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var AssociateLenderName[]
     */
    protected ?array $associateLenderName = null;

    /**
     * Constructor method for AssociateLenderNameContainer
     *
     * @param AssociateLenderName[] $associateLenderName
     * @uses AssociateLenderNameContainer::setAssociateLenderName()
     */
    public function __construct(?array $associateLenderName = null)
    {
        $this
            ->setAssociateLenderName($associateLenderName);
    }

    /**
     * This method is responsible for validating the values passed to the setAssociateLenderName method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAssociateLenderName method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAssociateLenderNameForArrayConstraintsFromSetAssociateLenderName(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $associateLenderNameContainerAssociateLenderNameItem) {
            // validation for constraint: itemType
            if (!$associateLenderNameContainerAssociateLenderNameItem instanceof AssociateLenderName) {
                $invalidValues[] = is_object($associateLenderNameContainerAssociateLenderNameItem) ? get_class($associateLenderNameContainerAssociateLenderNameItem) : sprintf(
                    '%s(%s)',
                    gettype($associateLenderNameContainerAssociateLenderNameItem),
                    var_export($associateLenderNameContainerAssociateLenderNameItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The associateLenderName property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AssociateLenderName, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get associateLenderName value
     *
     * @return AssociateLenderName[]
     */
    public function getAssociateLenderName(): ?array
    {
        return $this->associateLenderName;
    }

    /**
     * Set associateLenderName value
     *
     * @param AssociateLenderName[] $associateLenderName
     * @return AssociateLenderNameContainer
     * @throws InvalidArgumentException
     */
    public function setAssociateLenderName(?array $associateLenderName = null): self
    {
        // validation for constraint: array
        if ('' !== ($associateLenderNameArrayErrorMessage = self::validateAssociateLenderNameForArrayConstraintsFromSetAssociateLenderName($associateLenderName))) {
            throw new InvalidArgumentException($associateLenderNameArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($associateLenderName) && count($associateLenderName) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($associateLenderName)
            ), __LINE__);
        }
        $this->associateLenderName = $associateLenderName;

        return $this;
    }

    /**
     * Add item to associateLenderName value
     *
     * @param AssociateLenderName $item
     * @return AssociateLenderNameContainer
     * @throws InvalidArgumentException
     */
    public function addToAssociateLenderName(AssociateLenderName $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof AssociateLenderName) {
            throw new InvalidArgumentException(sprintf(
                'The associateLenderName property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AssociateLenderName, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->associateLenderName) && count($this->associateLenderName) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->associateLenderName)
            ), __LINE__);
        }
        $this->associateLenderName[] = $item;

        return $this;
    }
}
