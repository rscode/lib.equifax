<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for ElectoralRollContainer StructType
 *
 * @subpackage Structs
 */
class ElectoralRollContainer extends DataItemContainer
{
    /**
     * The electoralRoll
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var ElectoralRoll[]
     */
    protected ?array $electoralRoll = null;

    /**
     * Constructor method for ElectoralRollContainer
     *
     * @param ElectoralRoll[] $electoralRoll
     * @uses ElectoralRollContainer::setElectoralRoll()
     */
    public function __construct(?array $electoralRoll = null)
    {
        $this
            ->setElectoralRoll($electoralRoll);
    }

    /**
     * This method is responsible for validating the values passed to the setElectoralRoll method
     * This method is willingly generated in order to preserve the one-line inline validation within the setElectoralRoll method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateElectoralRollForArrayConstraintsFromSetElectoralRoll(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $electoralRollContainerElectoralRollItem) {
            // validation for constraint: itemType
            if (!$electoralRollContainerElectoralRollItem instanceof ElectoralRoll) {
                $invalidValues[] = is_object($electoralRollContainerElectoralRollItem) ? get_class($electoralRollContainerElectoralRollItem) : sprintf(
                    '%s(%s)',
                    gettype($electoralRollContainerElectoralRollItem),
                    var_export($electoralRollContainerElectoralRollItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The electoralRoll property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\ElectoralRoll, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get electoralRoll value
     *
     * @return ElectoralRoll[]
     */
    public function getElectoralRoll(): ?array
    {
        return $this->electoralRoll;
    }

    /**
     * Set electoralRoll value
     *
     * @param ElectoralRoll[] $electoralRoll
     * @return ElectoralRollContainer
     * @throws InvalidArgumentException
     */
    public function setElectoralRoll(?array $electoralRoll = null): self
    {
        // validation for constraint: array
        if ('' !== ($electoralRollArrayErrorMessage = self::validateElectoralRollForArrayConstraintsFromSetElectoralRoll($electoralRoll))) {
            throw new InvalidArgumentException($electoralRollArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($electoralRoll) && count($electoralRoll) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($electoralRoll)
            ), __LINE__);
        }
        $this->electoralRoll = $electoralRoll;

        return $this;
    }

    /**
     * Add item to electoralRoll value
     *
     * @param ElectoralRoll $item
     * @return ElectoralRollContainer
     * @throws InvalidArgumentException
     */
    public function addToElectoralRoll(ElectoralRoll $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof ElectoralRoll) {
            throw new InvalidArgumentException(sprintf(
                'The electoralRoll property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\ElectoralRoll, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->electoralRoll) && count($this->electoralRoll) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->electoralRoll)
            ), __LINE__);
        }
        $this->electoralRoll[] = $item;

        return $this;
    }
}
