<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AdditionalDetails StructType
 *
 * @subpackage Structs
 */
class AdditionalDetails extends AbstractStructBase
{
    /**
     * The address
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var string[]
     */
    protected ?array $address = null;
    /**
     * The watchlistDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var WatchlistDate[]
     */
    protected ?array $watchlistDate = null;
    /**
     * The idInformation
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var IdInformation[]
     */
    protected ?array $idInformation = null;
    /**
     * The sourceData
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var string[]
     */
    protected ?array $sourceData = null;
    /**
     * The roleData
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var RoleData[]
     */
    protected ?array $roleData = null;

    /**
     * Constructor method for AdditionalDetails
     *
     * @param string[]        $address
     * @param WatchlistDate[] $watchlistDate
     * @param IdInformation[] $idInformation
     * @param string[]        $sourceData
     * @param RoleData[]      $roleData
     * @uses AdditionalDetails::setAddress()
     * @uses AdditionalDetails::setWatchlistDate()
     * @uses AdditionalDetails::setIdInformation()
     * @uses AdditionalDetails::setSourceData()
     * @uses AdditionalDetails::setRoleData()
     */
    public function __construct(
        ?array $address = null,
        ?array $watchlistDate = null,
        ?array $idInformation = null,
        ?array $sourceData = null,
        ?array $roleData = null
    ) {
        $this
            ->setAddress($address)
            ->setWatchlistDate($watchlistDate)
            ->setIdInformation($idInformation)
            ->setSourceData($sourceData)
            ->setRoleData($roleData);
    }

    /**
     * This method is responsible for validating the values passed to the setAddress method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAddress method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAddressForArrayConstraintsFromSetAddress(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $additionalDetailsAddressItem) {
            // validation for constraint: itemType
            if (!is_string($additionalDetailsAddressItem)) {
                $invalidValues[] = is_object($additionalDetailsAddressItem) ? get_class($additionalDetailsAddressItem) : sprintf(
                    '%s(%s)',
                    gettype($additionalDetailsAddressItem),
                    var_export($additionalDetailsAddressItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The address property can only contain items of type string, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * This method is responsible for validating the values passed to the setWatchlistDate method
     * This method is willingly generated in order to preserve the one-line inline validation within the setWatchlistDate method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateWatchlistDateForArrayConstraintsFromSetWatchlistDate(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $additionalDetailsWatchlistDateItem) {
            // validation for constraint: itemType
            if (!$additionalDetailsWatchlistDateItem instanceof WatchlistDate) {
                $invalidValues[] = is_object($additionalDetailsWatchlistDateItem) ? get_class($additionalDetailsWatchlistDateItem) : sprintf(
                    '%s(%s)',
                    gettype($additionalDetailsWatchlistDateItem),
                    var_export($additionalDetailsWatchlistDateItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The watchlistDate property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\WatchlistDate, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * This method is responsible for validating the values passed to the setIdInformation method
     * This method is willingly generated in order to preserve the one-line inline validation within the setIdInformation method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateIdInformationForArrayConstraintsFromSetIdInformation(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $additionalDetailsIdInformationItem) {
            // validation for constraint: itemType
            if (!$additionalDetailsIdInformationItem instanceof IdInformation) {
                $invalidValues[] = is_object($additionalDetailsIdInformationItem) ? get_class($additionalDetailsIdInformationItem) : sprintf(
                    '%s(%s)',
                    gettype($additionalDetailsIdInformationItem),
                    var_export($additionalDetailsIdInformationItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The idInformation property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\IdInformation, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * This method is responsible for validating the values passed to the setSourceData method
     * This method is willingly generated in order to preserve the one-line inline validation within the setSourceData method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateSourceDataForArrayConstraintsFromSetSourceData(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $additionalDetailsSourceDataItem) {
            // validation for constraint: itemType
            if (!is_string($additionalDetailsSourceDataItem)) {
                $invalidValues[] = is_object($additionalDetailsSourceDataItem) ? get_class($additionalDetailsSourceDataItem) : sprintf(
                    '%s(%s)',
                    gettype($additionalDetailsSourceDataItem),
                    var_export($additionalDetailsSourceDataItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The sourceData property can only contain items of type string, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * This method is responsible for validating the values passed to the setRoleData method
     * This method is willingly generated in order to preserve the one-line inline validation within the setRoleData method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateRoleDataForArrayConstraintsFromSetRoleData(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $additionalDetailsRoleDataItem) {
            // validation for constraint: itemType
            if (!$additionalDetailsRoleDataItem instanceof RoleData) {
                $invalidValues[] = is_object($additionalDetailsRoleDataItem) ? get_class($additionalDetailsRoleDataItem) : sprintf(
                    '%s(%s)',
                    gettype($additionalDetailsRoleDataItem),
                    var_export($additionalDetailsRoleDataItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The roleData property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\RoleData, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get address value
     *
     * @return string[]
     */
    public function getAddress(): ?array
    {
        return $this->address;
    }

    /**
     * Set address value
     *
     * @param string[] $address
     * @return AdditionalDetails
     * @throws InvalidArgumentException
     */
    public function setAddress(?array $address = null): self
    {
        // validation for constraint: array
        if ('' !== ($addressArrayErrorMessage = self::validateAddressForArrayConstraintsFromSetAddress($address))) {
            throw new InvalidArgumentException($addressArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($address) && count($address) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($address)
            ), __LINE__);
        }
        $this->address = $address;

        return $this;
    }

    /**
     * Add item to address value
     *
     * @param string $item
     * @return AdditionalDetails
     * @throws InvalidArgumentException
     */
    public function addToAddress(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf(
                'The address property can only contain items of type string, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->address) && count($this->address) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->address)
            ), __LINE__);
        }
        $this->address[] = $item;

        return $this;
    }

    /**
     * Get watchlistDate value
     *
     * @return WatchlistDate[]
     */
    public function getWatchlistDate(): ?array
    {
        return $this->watchlistDate;
    }

    /**
     * Set watchlistDate value
     *
     * @param WatchlistDate[] $watchlistDate
     * @return AdditionalDetails
     * @throws InvalidArgumentException
     */
    public function setWatchlistDate(?array $watchlistDate = null): self
    {
        // validation for constraint: array
        if ('' !== ($watchlistDateArrayErrorMessage = self::validateWatchlistDateForArrayConstraintsFromSetWatchlistDate($watchlistDate))) {
            throw new InvalidArgumentException($watchlistDateArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($watchlistDate) && count($watchlistDate) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($watchlistDate)
            ), __LINE__);
        }
        $this->watchlistDate = $watchlistDate;

        return $this;
    }

    /**
     * Add item to watchlistDate value
     *
     * @param WatchlistDate $item
     * @return AdditionalDetails
     * @throws InvalidArgumentException
     */
    public function addToWatchlistDate(WatchlistDate $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof WatchlistDate) {
            throw new InvalidArgumentException(sprintf(
                'The watchlistDate property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\WatchlistDate, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->watchlistDate) && count($this->watchlistDate) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->watchlistDate)
            ), __LINE__);
        }
        $this->watchlistDate[] = $item;

        return $this;
    }

    /**
     * Get idInformation value
     *
     * @return IdInformation[]
     */
    public function getIdInformation(): ?array
    {
        return $this->idInformation;
    }

    /**
     * Set idInformation value
     *
     * @param IdInformation[] $idInformation
     * @return AdditionalDetails
     * @throws InvalidArgumentException
     */
    public function setIdInformation(?array $idInformation = null): self
    {
        // validation for constraint: array
        if ('' !== ($idInformationArrayErrorMessage = self::validateIdInformationForArrayConstraintsFromSetIdInformation($idInformation))) {
            throw new InvalidArgumentException($idInformationArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($idInformation) && count($idInformation) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($idInformation)
            ), __LINE__);
        }
        $this->idInformation = $idInformation;

        return $this;
    }

    /**
     * Add item to idInformation value
     *
     * @param IdInformation $item
     * @return AdditionalDetails
     * @throws InvalidArgumentException
     */
    public function addToIdInformation(IdInformation $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof IdInformation) {
            throw new InvalidArgumentException(sprintf(
                'The idInformation property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\IdInformation, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->idInformation) && count($this->idInformation) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->idInformation)
            ), __LINE__);
        }
        $this->idInformation[] = $item;

        return $this;
    }

    /**
     * Get sourceData value
     *
     * @return string[]
     */
    public function getSourceData(): ?array
    {
        return $this->sourceData;
    }

    /**
     * Set sourceData value
     *
     * @param string[] $sourceData
     * @return AdditionalDetails
     * @throws InvalidArgumentException
     */
    public function setSourceData(?array $sourceData = null): self
    {
        // validation for constraint: array
        if ('' !== ($sourceDataArrayErrorMessage = self::validateSourceDataForArrayConstraintsFromSetSourceData($sourceData))) {
            throw new InvalidArgumentException($sourceDataArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($sourceData) && count($sourceData) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($sourceData)
            ), __LINE__);
        }
        $this->sourceData = $sourceData;

        return $this;
    }

    /**
     * Add item to sourceData value
     *
     * @param string $item
     * @return AdditionalDetails
     * @throws InvalidArgumentException
     */
    public function addToSourceData(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf(
                'The sourceData property can only contain items of type string, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->sourceData) && count($this->sourceData) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->sourceData)
            ), __LINE__);
        }
        $this->sourceData[] = $item;

        return $this;
    }

    /**
     * Get roleData value
     *
     * @return RoleData[]
     */
    public function getRoleData(): ?array
    {
        return $this->roleData;
    }

    /**
     * Set roleData value
     *
     * @param RoleData[] $roleData
     * @return AdditionalDetails
     * @throws InvalidArgumentException
     */
    public function setRoleData(?array $roleData = null): self
    {
        // validation for constraint: array
        if ('' !== ($roleDataArrayErrorMessage = self::validateRoleDataForArrayConstraintsFromSetRoleData($roleData))) {
            throw new InvalidArgumentException($roleDataArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($roleData) && count($roleData) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($roleData)
            ), __LINE__);
        }
        $this->roleData = $roleData;

        return $this;
    }

    /**
     * Add item to roleData value
     *
     * @param RoleData $item
     * @return AdditionalDetails
     * @throws InvalidArgumentException
     */
    public function addToRoleData(RoleData $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof RoleData) {
            throw new InvalidArgumentException(sprintf(
                'The roleData property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\RoleData, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->roleData) && count($this->roleData) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->roleData)
            ), __LINE__);
        }
        $this->roleData[] = $item;

        return $this;
    }
}
