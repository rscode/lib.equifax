<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * Request elements for each of the available data groups. A maximum number of records to be returned can be specified along with the
 * address scope (supplied address data, linked address data or data from both).
 *
 * @see https://developer.equifax.co.uk/sites/uk/files/xml_ref_guide/guide/single.pdf#dataGroups
 */
class RequestedData extends AbstractStructBase
{
    /**
     * The cifasRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var CIFASRequest|null
     */
    protected ?CIFASRequest $cifasRequest = null;
    /**
     * The telephoneDataRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var TelephoneDataRequest|null
     */
    protected ?TelephoneDataRequest $telephoneDataRequest = null;
    /**
     * The inputAddressRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var InputAddressRequest|null
     */
    protected ?InputAddressRequest $inputAddressRequest = null;
    /**
     * The multipleAddressRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var MultipleAddressRequest|null
     */
    protected ?MultipleAddressRequest $multipleAddressRequest = null;
    /**
     * The aliasRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AliasRequest|null
     */
    protected ?AliasRequest $aliasRequest = null;
    /**
     * The associateRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AssociateRequest|null
     */
    protected ?AssociateRequest $associateRequest = null;
    /**
     * The potentialAssociateRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var PotentialAssociateRequest|null
     */
    protected ?PotentialAssociateRequest $potentialAssociateRequest = null;
    /**
     * The courtAndInsolvencyInformationRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var CourtAndInsolvencyInformationRequest|null
     */
    protected ?CourtAndInsolvencyInformationRequest $courtAndInsolvencyInformationRequest = null;
    /**
     * The noticeofCorrectionOrDisputeRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var NoticeOfCorrectionOrDisputeRequest|null
     */
    protected ?NoticeOfCorrectionOrDisputeRequest $noticeofCorrectionOrDisputeRequest = null;
    /**
     * The attributableDataRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AttributableDataRequest|null
     */
    protected ?AttributableDataRequest $attributableDataRequest = null;
    /**
     * The outputAddressRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var OutputAddressRequest|null
     */
    protected ?OutputAddressRequest $outputAddressRequest = null;
    /**
     * The propertyValuationDataRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var PropertyValuationDataRequest|null
     */
    protected ?PropertyValuationDataRequest $propertyValuationDataRequest = null;
    /**
     * The electoralRollRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var ElectoralRollRequest|null
     */
    protected ?ElectoralRollRequest $electoralRollRequest = null;
    /**
     * The rollingRegisterRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var RollingRegisterRequest|null
     */
    protected ?RollingRegisterRequest $rollingRegisterRequest = null;
    /**
     * The insightRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var InsightRequest|null
     */
    protected ?InsightRequest $insightRequest = null;
    /**
     * The insightHistoryRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var InsightHistoryRequest|null
     */
    protected ?InsightHistoryRequest $insightHistoryRequest = null;
    /**
     * The searchesRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var SearchesRequest|null
     */
    protected ?SearchesRequest $searchesRequest = null;
    /**
     * The gainRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var GAINRequest|null
     */
    protected ?GAINRequest $gainRequest = null;
    /**
     * The amlSummaryRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AMLSummaryRequest|null
     */
    protected ?AMLSummaryRequest $amlSummaryRequest = null;
    /**
     * The amlHMTreasurySanctionsCheckRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AMLHMTreasurySanctionsCheckRequest|null
     */
    protected ?AMLHMTreasurySanctionsCheckRequest $amlHMTreasurySanctionsCheckRequest = null;
    /**
     * The amlSeniorPoliticalFiguresSanctionsCheckRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AMLSeniorPoliticalFiguresSanctionsCheckRequest|null
     */
    protected ?AMLSeniorPoliticalFiguresSanctionsCheckRequest $amlSeniorPoliticalFiguresSanctionsCheckRequest = null;
    /**
     * The amlHaloDeceasedCheckRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AMLHaloDeceasedCheckRequest|null
     */
    protected ?AMLHaloDeceasedCheckRequest $amlHaloDeceasedCheckRequest = null;
    /**
     * The amlOFACCheckRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AMLOFACCheckRequest|null
     */
    protected ?AMLOFACCheckRequest $amlOFACCheckRequest = null;
    /**
     * The amlEnhancedWatchlistCheckRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AMLEnhancedWatchlistCheckRequest|null
     */
    protected ?AMLEnhancedWatchlistCheckRequest $amlEnhancedWatchlistCheckRequest = null;
    /**
     * The amlEnhancedWatchlistCheckSummary
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AMLEnhancedWatchlistCheckSummary|null
     */
    protected ?AMLEnhancedWatchlistCheckSummary $amlEnhancedWatchlistCheckSummary = null;
    /**
     * The fraudProfileSummaryRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var FraudProfileSummaryRequest|null
     */
    protected ?FraudProfileSummaryRequest $fraudProfileSummaryRequest = null;
    /**
     * The siranChecksRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var SIRANChecksRequest|null
     */
    protected ?SIRANChecksRequest $siranChecksRequest = null;
    /**
     * The bureauVerificationCheckRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var BureauVerificationCheckRequest|null
     */
    protected ?BureauVerificationCheckRequest $bureauVerificationCheckRequest = null;
    /**
     * The homeTelephoneCheckRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var HomeTelephoneCheckRequest|null
     */
    protected ?HomeTelephoneCheckRequest $homeTelephoneCheckRequest = null;
    /**
     * The homeTelephoneCheckExtendedDataRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var HomeTelephoneCheckExtendedDataRequest|null
     */
    protected ?HomeTelephoneCheckExtendedDataRequest $homeTelephoneCheckExtendedDataRequest = null;
    /**
     * The bankCheckRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var BankCheckRequest|null
     */
    protected ?BankCheckRequest $bankCheckRequest = null;
    /**
     * The employerCheckRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var EmployerCheckRequest|null
     */
    protected ?EmployerCheckRequest $employerCheckRequest = null;
    /**
     * The idPlusRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var IDPlusRequest|null
     */
    protected ?IDPlusRequest $idPlusRequest = null;
    /**
     * The scoreAndCharacteristicRequests
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var ScoreAndCharacteristicRequests|null
     */
    protected ?ScoreAndCharacteristicRequests $scoreAndCharacteristicRequests = null;
    /**
     * The amlEnhancedWatchlistCheckProfile
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AmlEnhancedWatchlistCheckProfile|null
     */
    protected ?AmlEnhancedWatchlistCheckProfile $amlEnhancedWatchlistCheckProfile = null;

    public function __construct(
        ?CIFASRequest $cifasRequest = null,
        ?TelephoneDataRequest $telephoneDataRequest = null,
        ?InputAddressRequest $inputAddressRequest = null,
        ?MultipleAddressRequest $multipleAddressRequest = null,
        ?AliasRequest $aliasRequest = null,
        ?AssociateRequest $associateRequest = null,
        ?PotentialAssociateRequest $potentialAssociateRequest = null,
        ?CourtAndInsolvencyInformationRequest $courtAndInsolvencyInformationRequest = null,
        ?NoticeOfCorrectionOrDisputeRequest $noticeofCorrectionOrDisputeRequest = null,
        ?AttributableDataRequest $attributableDataRequest = null,
        ?OutputAddressRequest $outputAddressRequest = null,
        ?PropertyValuationDataRequest $propertyValuationDataRequest = null,
        ?ElectoralRollRequest $electoralRollRequest = null,
        ?RollingRegisterRequest $rollingRegisterRequest = null,
        ?InsightRequest $insightRequest = null,
        ?InsightHistoryRequest $insightHistoryRequest = null,
        ?SearchesRequest $searchesRequest = null,
        ?GAINRequest $gainRequest = null,
        ?AMLSummaryRequest $amlSummaryRequest = null,
        ?AMLHMTreasurySanctionsCheckRequest $amlHMTreasurySanctionsCheckRequest = null,
        ?AMLSeniorPoliticalFiguresSanctionsCheckRequest $amlSeniorPoliticalFiguresSanctionsCheckRequest = null,
        ?AMLHaloDeceasedCheckRequest $amlHaloDeceasedCheckRequest = null,
        ?AMLOFACCheckRequest $amlOFACCheckRequest = null,
        ?AMLEnhancedWatchlistCheckRequest $amlEnhancedWatchlistCheckRequest = null,
        ?AMLEnhancedWatchlistCheckSummary $amlEnhancedWatchlistCheckSummary = null,
        ?FraudProfileSummaryRequest $fraudProfileSummaryRequest = null,
        ?SIRANChecksRequest $siranChecksRequest = null,
        ?BureauVerificationCheckRequest $bureauVerificationCheckRequest = null,
        ?HomeTelephoneCheckRequest $homeTelephoneCheckRequest = null,
        ?HomeTelephoneCheckExtendedDataRequest $homeTelephoneCheckExtendedDataRequest = null,
        ?BankCheckRequest $bankCheckRequest = null,
        ?EmployerCheckRequest $employerCheckRequest = null,
        ?IDPlusRequest $idPlusRequest = null,
        ?ScoreAndCharacteristicRequests $scoreAndCharacteristicRequests = null,
        ?AmlEnhancedWatchlistCheckProfile $amlEnhancedWatchlistCheckProfile = null
    ) {
        $this
            ->setCifasRequest($cifasRequest)
            ->setTelephoneDataRequest($telephoneDataRequest)
            ->setInputAddressRequest($inputAddressRequest)
            ->setMultipleAddressRequest($multipleAddressRequest)
            ->setAliasRequest($aliasRequest)
            ->setAssociateRequest($associateRequest)
            ->setPotentialAssociateRequest($potentialAssociateRequest)
            ->setCourtAndInsolvencyInformationRequest($courtAndInsolvencyInformationRequest)
            ->setNoticeofCorrectionOrDisputeRequest($noticeofCorrectionOrDisputeRequest)
            ->setAttributableDataRequest($attributableDataRequest)
            ->setOutputAddressRequest($outputAddressRequest)
            ->setPropertyValuationDataRequest($propertyValuationDataRequest)
            ->setElectoralRollRequest($electoralRollRequest)
            ->setRollingRegisterRequest($rollingRegisterRequest)
            ->setInsightRequest($insightRequest)
            ->setInsightHistoryRequest($insightHistoryRequest)
            ->setSearchesRequest($searchesRequest)
            ->setGainRequest($gainRequest)
            ->setAmlSummaryRequest($amlSummaryRequest)
            ->setAmlHMTreasurySanctionsCheckRequest($amlHMTreasurySanctionsCheckRequest)
            ->setAmlSeniorPoliticalFiguresSanctionsCheckRequest($amlSeniorPoliticalFiguresSanctionsCheckRequest)
            ->setAmlHaloDeceasedCheckRequest($amlHaloDeceasedCheckRequest)
            ->setAmlOFACCheckRequest($amlOFACCheckRequest)
            ->setAmlEnhancedWatchlistCheckRequest($amlEnhancedWatchlistCheckRequest)
            ->setAmlEnhancedWatchlistCheckSummary($amlEnhancedWatchlistCheckSummary)
            ->setFraudProfileSummaryRequest($fraudProfileSummaryRequest)
            ->setSiranChecksRequest($siranChecksRequest)
            ->setBureauVerificationCheckRequest($bureauVerificationCheckRequest)
            ->setHomeTelephoneCheckRequest($homeTelephoneCheckRequest)
            ->setHomeTelephoneCheckExtendedDataRequest($homeTelephoneCheckExtendedDataRequest)
            ->setBankCheckRequest($bankCheckRequest)
            ->setEmployerCheckRequest($employerCheckRequest)
            ->setIdPlusRequest($idPlusRequest)
            ->setScoreAndCharacteristicRequests($scoreAndCharacteristicRequests)
            ->setAmlEnhancedWatchlistCheckProfile($amlEnhancedWatchlistCheckProfile);
    }

    /**
     * Get cifasRequest value
     *
     * @return CIFASRequest|null
     */
    public function getCifasRequest(): ?CIFASRequest
    {
        return $this->cifasRequest;
    }

    /**
     * Set cifasRequest value
     *
     * @param CIFASRequest $cifasRequest
     * @return RequestedData
     */
    public function setCifasRequest(?CIFASRequest $cifasRequest = null): self
    {
        $this->cifasRequest = $cifasRequest;

        return $this;
    }

    /**
     * Get telephoneDataRequest value
     *
     * @return TelephoneDataRequest|null
     */
    public function getTelephoneDataRequest(): ?TelephoneDataRequest
    {
        return $this->telephoneDataRequest;
    }

    /**
     * Set telephoneDataRequest value
     *
     * @param TelephoneDataRequest $telephoneDataRequest
     * @return RequestedData
     */
    public function setTelephoneDataRequest(?TelephoneDataRequest $telephoneDataRequest = null): self
    {
        $this->telephoneDataRequest = $telephoneDataRequest;

        return $this;
    }

    /**
     * Get inputAddressRequest value
     *
     * @return InputAddressRequest|null
     */
    public function getInputAddressRequest(): ?InputAddressRequest
    {
        return $this->inputAddressRequest;
    }

    /**
     * Set inputAddressRequest value
     *
     * @param InputAddressRequest $inputAddressRequest
     * @return RequestedData
     */
    public function setInputAddressRequest(?InputAddressRequest $inputAddressRequest = null): self
    {
        $this->inputAddressRequest = $inputAddressRequest;

        return $this;
    }

    /**
     * Get multipleAddressRequest value
     *
     * @return MultipleAddressRequest|null
     */
    public function getMultipleAddressRequest(): ?MultipleAddressRequest
    {
        return $this->multipleAddressRequest;
    }

    /**
     * Set multipleAddressRequest value
     *
     * @param MultipleAddressRequest $multipleAddressRequest
     * @return RequestedData
     */
    public function setMultipleAddressRequest(?MultipleAddressRequest $multipleAddressRequest = null): self
    {
        $this->multipleAddressRequest = $multipleAddressRequest;

        return $this;
    }

    /**
     * Get aliasRequest value
     *
     * @return AliasRequest|null
     */
    public function getAliasRequest(): ?AliasRequest
    {
        return $this->aliasRequest;
    }

    /**
     * Set aliasRequest value
     *
     * @param AliasRequest $aliasRequest
     * @return RequestedData
     */
    public function setAliasRequest(?AliasRequest $aliasRequest = null): self
    {
        $this->aliasRequest = $aliasRequest;

        return $this;
    }

    /**
     * Get associateRequest value
     *
     * @return AssociateRequest|null
     */
    public function getAssociateRequest(): ?AssociateRequest
    {
        return $this->associateRequest;
    }

    /**
     * Set associateRequest value
     *
     * @param AssociateRequest $associateRequest
     * @return RequestedData
     */
    public function setAssociateRequest(?AssociateRequest $associateRequest = null): self
    {
        $this->associateRequest = $associateRequest;

        return $this;
    }

    /**
     * Get potentialAssociateRequest value
     *
     * @return PotentialAssociateRequest|null
     */
    public function getPotentialAssociateRequest(): ?PotentialAssociateRequest
    {
        return $this->potentialAssociateRequest;
    }

    /**
     * Set potentialAssociateRequest value
     *
     * @param PotentialAssociateRequest $potentialAssociateRequest
     * @return RequestedData
     */
    public function setPotentialAssociateRequest(?PotentialAssociateRequest $potentialAssociateRequest = null): self
    {
        $this->potentialAssociateRequest = $potentialAssociateRequest;

        return $this;
    }

    /**
     * Get courtAndInsolvencyInformationRequest value
     *
     * @return CourtAndInsolvencyInformationRequest|null
     */
    public function getCourtAndInsolvencyInformationRequest(): ?CourtAndInsolvencyInformationRequest
    {
        return $this->courtAndInsolvencyInformationRequest;
    }

    /**
     * Set courtAndInsolvencyInformationRequest value
     *
     * @param CourtAndInsolvencyInformationRequest $courtAndInsolvencyInformationRequest
     * @return RequestedData
     */
    public function setCourtAndInsolvencyInformationRequest(
        ?CourtAndInsolvencyInformationRequest $courtAndInsolvencyInformationRequest = null
    ): self {
        $this->courtAndInsolvencyInformationRequest = $courtAndInsolvencyInformationRequest;

        return $this;
    }

    /**
     * Get noticeofCorrectionOrDisputeRequest value
     *
     * @return NoticeOfCorrectionOrDisputeRequest|null
     */
    public function getNoticeofCorrectionOrDisputeRequest(): ?NoticeOfCorrectionOrDisputeRequest
    {
        return $this->noticeofCorrectionOrDisputeRequest;
    }

    /**
     * Set noticeofCorrectionOrDisputeRequest value
     *
     * @param NoticeOfCorrectionOrDisputeRequest $noticeofCorrectionOrDisputeRequest
     * @return RequestedData
     */
    public function setNoticeofCorrectionOrDisputeRequest(?NoticeOfCorrectionOrDisputeRequest $noticeofCorrectionOrDisputeRequest = null): self
    {
        $this->noticeofCorrectionOrDisputeRequest = $noticeofCorrectionOrDisputeRequest;

        return $this;
    }

    /**
     * Get attributableDataRequest value
     *
     * @return AttributableDataRequest|null
     */
    public function getAttributableDataRequest(): ?AttributableDataRequest
    {
        return $this->attributableDataRequest;
    }

    /**
     * Set attributableDataRequest value
     *
     * @param AttributableDataRequest $attributableDataRequest
     * @return RequestedData
     */
    public function setAttributableDataRequest(?AttributableDataRequest $attributableDataRequest = null): self
    {
        $this->attributableDataRequest = $attributableDataRequest;

        return $this;
    }

    /**
     * Get outputAddressRequest value
     *
     * @return OutputAddressRequest|null
     */
    public function getOutputAddressRequest(): ?OutputAddressRequest
    {
        return $this->outputAddressRequest;
    }

    /**
     * Set outputAddressRequest value
     *
     * @param OutputAddressRequest $outputAddressRequest
     * @return RequestedData
     */
    public function setOutputAddressRequest(?OutputAddressRequest $outputAddressRequest = null): self
    {
        $this->outputAddressRequest = $outputAddressRequest;

        return $this;
    }

    /**
     * Get propertyValuationDataRequest value
     *
     * @return PropertyValuationDataRequest|null
     */
    public function getPropertyValuationDataRequest(): ?PropertyValuationDataRequest
    {
        return $this->propertyValuationDataRequest;
    }

    /**
     * Set propertyValuationDataRequest value
     *
     * @param PropertyValuationDataRequest $propertyValuationDataRequest
     * @return RequestedData
     */
    public function setPropertyValuationDataRequest(?PropertyValuationDataRequest $propertyValuationDataRequest = null): self
    {
        $this->propertyValuationDataRequest = $propertyValuationDataRequest;

        return $this;
    }

    /**
     * Get electoralRollRequest value
     *
     * @return ElectoralRollRequest|null
     */
    public function getElectoralRollRequest(): ?ElectoralRollRequest
    {
        return $this->electoralRollRequest;
    }

    /**
     * Set electoralRollRequest value
     *
     * @param ElectoralRollRequest $electoralRollRequest
     * @return RequestedData
     */
    public function setElectoralRollRequest(?ElectoralRollRequest $electoralRollRequest = null): self
    {
        $this->electoralRollRequest = $electoralRollRequest;

        return $this;
    }

    /**
     * Get rollingRegisterRequest value
     *
     * @return RollingRegisterRequest|null
     */
    public function getRollingRegisterRequest(): ?RollingRegisterRequest
    {
        return $this->rollingRegisterRequest;
    }

    /**
     * Set rollingRegisterRequest value
     *
     * @param RollingRegisterRequest $rollingRegisterRequest
     * @return RequestedData
     */
    public function setRollingRegisterRequest(?RollingRegisterRequest $rollingRegisterRequest = null): self
    {
        $this->rollingRegisterRequest = $rollingRegisterRequest;

        return $this;
    }

    /**
     * Get insightRequest value
     *
     * @return InsightRequest|null
     */
    public function getInsightRequest(): ?InsightRequest
    {
        return $this->insightRequest;
    }

    /**
     * Set insightRequest value
     *
     * @param InsightRequest $insightRequest
     * @return RequestedData
     */
    public function setInsightRequest(?InsightRequest $insightRequest = null): self
    {
        $this->insightRequest = $insightRequest;

        return $this;
    }

    /**
     * Get insightHistoryRequest value
     *
     * @return InsightHistoryRequest|null
     */
    public function getInsightHistoryRequest(): ?InsightHistoryRequest
    {
        return $this->insightHistoryRequest;
    }

    /**
     * Set insightHistoryRequest value
     *
     * @param InsightHistoryRequest $insightHistoryRequest
     * @return RequestedData
     */
    public function setInsightHistoryRequest(?InsightHistoryRequest $insightHistoryRequest = null): self
    {
        $this->insightHistoryRequest = $insightHistoryRequest;

        return $this;
    }

    /**
     * Get searchesRequest value
     *
     * @return SearchesRequest|null
     */
    public function getSearchesRequest(): ?SearchesRequest
    {
        return $this->searchesRequest;
    }

    /**
     * Set searchesRequest value
     *
     * @param SearchesRequest $searchesRequest
     * @return RequestedData
     */
    public function setSearchesRequest(?SearchesRequest $searchesRequest = null): self
    {
        $this->searchesRequest = $searchesRequest;

        return $this;
    }

    /**
     * Get gainRequest value
     *
     * @return GAINRequest|null
     */
    public function getGainRequest(): ?GAINRequest
    {
        return $this->gainRequest;
    }

    /**
     * Set gainRequest value
     *
     * @param GAINRequest $gainRequest
     * @return RequestedData
     */
    public function setGainRequest(?GAINRequest $gainRequest = null): self
    {
        $this->gainRequest = $gainRequest;

        return $this;
    }

    /**
     * Get amlSummaryRequest value
     *
     * @return AMLSummaryRequest|null
     */
    public function getAmlSummaryRequest(): ?AMLSummaryRequest
    {
        return $this->amlSummaryRequest;
    }

    /**
     * Set amlSummaryRequest value
     *
     * @param AMLSummaryRequest $amlSummaryRequest
     * @return RequestedData
     */
    public function setAmlSummaryRequest(?AMLSummaryRequest $amlSummaryRequest = null): self
    {
        $this->amlSummaryRequest = $amlSummaryRequest;

        return $this;
    }

    /**
     * Get amlHMTreasurySanctionsCheckRequest value
     *
     * @return AMLHMTreasurySanctionsCheckRequest|null
     */
    public function getAmlHMTreasurySanctionsCheckRequest(): ?AMLHMTreasurySanctionsCheckRequest
    {
        return $this->amlHMTreasurySanctionsCheckRequest;
    }

    /**
     * Set amlHMTreasurySanctionsCheckRequest value
     *
     * @param AMLHMTreasurySanctionsCheckRequest $amlHMTreasurySanctionsCheckRequest
     * @return RequestedData
     */
    public function setAmlHMTreasurySanctionsCheckRequest(?AMLHMTreasurySanctionsCheckRequest $amlHMTreasurySanctionsCheckRequest = null): self
    {
        $this->amlHMTreasurySanctionsCheckRequest = $amlHMTreasurySanctionsCheckRequest;

        return $this;
    }

    /**
     * Get amlSeniorPoliticalFiguresSanctionsCheckRequest value
     *
     * @return AMLSeniorPoliticalFiguresSanctionsCheckRequest|null
     */
    public function getAmlSeniorPoliticalFiguresSanctionsCheckRequest(): ?AMLSeniorPoliticalFiguresSanctionsCheckRequest
    {
        return $this->amlSeniorPoliticalFiguresSanctionsCheckRequest;
    }

    /**
     * Set amlSeniorPoliticalFiguresSanctionsCheckRequest value
     *
     * @param AMLSeniorPoliticalFiguresSanctionsCheckRequest $amlSeniorPoliticalFiguresSanctionsCheckRequest
     * @return RequestedData
     */
    public function setAmlSeniorPoliticalFiguresSanctionsCheckRequest(
        ?AMLSeniorPoliticalFiguresSanctionsCheckRequest $amlSeniorPoliticalFiguresSanctionsCheckRequest = null
    ): self {
        $this->amlSeniorPoliticalFiguresSanctionsCheckRequest = $amlSeniorPoliticalFiguresSanctionsCheckRequest;

        return $this;
    }

    /**
     * Get amlHaloDeceasedCheckRequest value
     *
     * @return AMLHaloDeceasedCheckRequest|null
     */
    public function getAmlHaloDeceasedCheckRequest(): ?AMLHaloDeceasedCheckRequest
    {
        return $this->amlHaloDeceasedCheckRequest;
    }

    /**
     * Set amlHaloDeceasedCheckRequest value
     *
     * @param AMLHaloDeceasedCheckRequest $amlHaloDeceasedCheckRequest
     * @return RequestedData
     */
    public function setAmlHaloDeceasedCheckRequest(?AMLHaloDeceasedCheckRequest $amlHaloDeceasedCheckRequest = null): self
    {
        $this->amlHaloDeceasedCheckRequest = $amlHaloDeceasedCheckRequest;

        return $this;
    }

    /**
     * Get amlOFACCheckRequest value
     *
     * @return AMLOFACCheckRequest|null
     */
    public function getAmlOFACCheckRequest(): ?AMLOFACCheckRequest
    {
        return $this->amlOFACCheckRequest;
    }

    /**
     * Set amlOFACCheckRequest value
     *
     * @param AMLOFACCheckRequest $amlOFACCheckRequest
     * @return RequestedData
     */
    public function setAmlOFACCheckRequest(?AMLOFACCheckRequest $amlOFACCheckRequest = null): self
    {
        $this->amlOFACCheckRequest = $amlOFACCheckRequest;

        return $this;
    }

    /**
     * Get amlEnhancedWatchlistCheckRequest value
     *
     * @return AMLEnhancedWatchlistCheckRequest|null
     */
    public function getAmlEnhancedWatchlistCheckRequest(): ?AMLEnhancedWatchlistCheckRequest
    {
        return $this->amlEnhancedWatchlistCheckRequest;
    }

    /**
     * Set amlEnhancedWatchlistCheckRequest value
     *
     * @param AMLEnhancedWatchlistCheckRequest $amlEnhancedWatchlistCheckRequest
     * @return RequestedData
     */
    public function setAmlEnhancedWatchlistCheckRequest(?AMLEnhancedWatchlistCheckRequest $amlEnhancedWatchlistCheckRequest = null): self
    {
        $this->amlEnhancedWatchlistCheckRequest = $amlEnhancedWatchlistCheckRequest;

        return $this;
    }

    /**
     * Get amlEnhancedWatchlistCheckSummary value
     *
     * @return AMLEnhancedWatchlistCheckSummary|null
     */
    public function getAmlEnhancedWatchlistCheckSummary(): ?AMLEnhancedWatchlistCheckSummary
    {
        return $this->amlEnhancedWatchlistCheckSummary;
    }

    /**
     * Set amlEnhancedWatchlistCheckSummary value
     *
     * @param AMLEnhancedWatchlistCheckSummary $amlEnhancedWatchlistCheckSummary
     * @return RequestedData
     */
    public function setAmlEnhancedWatchlistCheckSummary(?AMLEnhancedWatchlistCheckSummary $amlEnhancedWatchlistCheckSummary = null): self
    {
        $this->amlEnhancedWatchlistCheckSummary = $amlEnhancedWatchlistCheckSummary;

        return $this;
    }

    /**
     * Get fraudProfileSummaryRequest value
     *
     * @return FraudProfileSummaryRequest|null
     */
    public function getFraudProfileSummaryRequest(): ?FraudProfileSummaryRequest
    {
        return $this->fraudProfileSummaryRequest;
    }

    /**
     * Set fraudProfileSummaryRequest value
     *
     * @param FraudProfileSummaryRequest $fraudProfileSummaryRequest
     * @return RequestedData
     */
    public function setFraudProfileSummaryRequest(?FraudProfileSummaryRequest $fraudProfileSummaryRequest = null): self
    {
        $this->fraudProfileSummaryRequest = $fraudProfileSummaryRequest;

        return $this;
    }

    /**
     * Get siranChecksRequest value
     *
     * @return SIRANChecksRequest|null
     */
    public function getSiranChecksRequest(): ?SIRANChecksRequest
    {
        return $this->siranChecksRequest;
    }

    /**
     * Set siranChecksRequest value
     *
     * @param SIRANChecksRequest $siranChecksRequest
     * @return RequestedData
     */
    public function setSiranChecksRequest(?SIRANChecksRequest $siranChecksRequest = null): self
    {
        $this->siranChecksRequest = $siranChecksRequest;

        return $this;
    }

    /**
     * Get bureauVerificationCheckRequest value
     *
     * @return BureauVerificationCheckRequest|null
     */
    public function getBureauVerificationCheckRequest(): ?BureauVerificationCheckRequest
    {
        return $this->bureauVerificationCheckRequest;
    }

    /**
     * Set bureauVerificationCheckRequest value
     *
     * @param BureauVerificationCheckRequest $bureauVerificationCheckRequest
     * @return RequestedData
     */
    public function setBureauVerificationCheckRequest(?BureauVerificationCheckRequest $bureauVerificationCheckRequest = null): self
    {
        $this->bureauVerificationCheckRequest = $bureauVerificationCheckRequest;

        return $this;
    }

    /**
     * Get homeTelephoneCheckRequest value
     *
     * @return HomeTelephoneCheckRequest|null
     */
    public function getHomeTelephoneCheckRequest(): ?HomeTelephoneCheckRequest
    {
        return $this->homeTelephoneCheckRequest;
    }

    /**
     * Set homeTelephoneCheckRequest value
     *
     * @param HomeTelephoneCheckRequest $homeTelephoneCheckRequest
     * @return RequestedData
     */
    public function setHomeTelephoneCheckRequest(?HomeTelephoneCheckRequest $homeTelephoneCheckRequest = null): self
    {
        $this->homeTelephoneCheckRequest = $homeTelephoneCheckRequest;

        return $this;
    }

    /**
     * Get homeTelephoneCheckExtendedDataRequest value
     *
     * @return HomeTelephoneCheckExtendedDataRequest|null
     */
    public function getHomeTelephoneCheckExtendedDataRequest(): ?HomeTelephoneCheckExtendedDataRequest
    {
        return $this->homeTelephoneCheckExtendedDataRequest;
    }

    /**
     * Set homeTelephoneCheckExtendedDataRequest value
     *
     * @param HomeTelephoneCheckExtendedDataRequest $homeTelephoneCheckExtendedDataRequest
     * @return RequestedData
     */
    public function setHomeTelephoneCheckExtendedDataRequest(
        ?HomeTelephoneCheckExtendedDataRequest $homeTelephoneCheckExtendedDataRequest = null
    ): self {
        $this->homeTelephoneCheckExtendedDataRequest = $homeTelephoneCheckExtendedDataRequest;

        return $this;
    }

    /**
     * Get bankCheckRequest value
     *
     * @return BankCheckRequest|null
     */
    public function getBankCheckRequest(): ?BankCheckRequest
    {
        return $this->bankCheckRequest;
    }

    /**
     * Set bankCheckRequest value
     *
     * @param BankCheckRequest $bankCheckRequest
     * @return RequestedData
     */
    public function setBankCheckRequest(?BankCheckRequest $bankCheckRequest = null): self
    {
        $this->bankCheckRequest = $bankCheckRequest;

        return $this;
    }

    /**
     * Get employerCheckRequest value
     *
     * @return EmployerCheckRequest|null
     */
    public function getEmployerCheckRequest(): ?EmployerCheckRequest
    {
        return $this->employerCheckRequest;
    }

    /**
     * Set employerCheckRequest value
     *
     * @param EmployerCheckRequest $employerCheckRequest
     * @return RequestedData
     */
    public function setEmployerCheckRequest(?EmployerCheckRequest $employerCheckRequest = null): self
    {
        $this->employerCheckRequest = $employerCheckRequest;

        return $this;
    }

    /**
     * Get idPlusRequest value
     *
     * @return IDPlusRequest|null
     */
    public function getIdPlusRequest(): ?IDPlusRequest
    {
        return $this->idPlusRequest;
    }

    /**
     * Set idPlusRequest value
     *
     * @param IDPlusRequest $idPlusRequest
     * @return RequestedData
     */
    public function setIdPlusRequest(?IDPlusRequest $idPlusRequest = null): self
    {
        $this->idPlusRequest = $idPlusRequest;

        return $this;
    }

    /**
     * Get scoreAndCharacteristicRequests value
     *
     * @return ScoreAndCharacteristicRequests|null
     */
    public function getScoreAndCharacteristicRequests(): ?ScoreAndCharacteristicRequests
    {
        return $this->scoreAndCharacteristicRequests;
    }

    /**
     * Set scoreAndCharacteristicRequests value
     *
     * @param ScoreAndCharacteristicRequests $scoreAndCharacteristicRequests
     * @return RequestedData
     */
    public function setScoreAndCharacteristicRequests(?ScoreAndCharacteristicRequests $scoreAndCharacteristicRequests = null): self
    {
        $this->scoreAndCharacteristicRequests = $scoreAndCharacteristicRequests;

        return $this;
    }

    /**
     * Get amlEnhancedWatchlistCheckProfile value
     *
     * @return AmlEnhancedWatchlistCheckProfile|null
     */
    public function getAmlEnhancedWatchlistCheckProfile(): ?AmlEnhancedWatchlistCheckProfile
    {
        return $this->amlEnhancedWatchlistCheckProfile;
    }

    /**
     * Set amlEnhancedWatchlistCheckProfile value
     *
     * @param AmlEnhancedWatchlistCheckProfile $amlEnhancedWatchlistCheckProfile
     * @return RequestedData
     */
    public function setAmlEnhancedWatchlistCheckProfile(?AmlEnhancedWatchlistCheckProfile $amlEnhancedWatchlistCheckProfile = null): self
    {
        $this->amlEnhancedWatchlistCheckProfile = $amlEnhancedWatchlistCheckProfile;

        return $this;
    }
}
