<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for NotifyAliasOrAssociationResponse StructType
 *
 * @subpackage Structs
 */
class NotifyAliasOrAssociationResponse extends CommonResponse
{
}
