<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for EmployerCheckContainer StructType
 *
 * @subpackage Structs
 */
class EmployerCheckContainer extends DataItemContainer
{
    /**
     * The employerCheck
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var EmployerCheck|null
     */
    protected ?EmployerCheck $employerCheck = null;

    /**
     * Constructor method for EmployerCheckContainer
     *
     * @param EmployerCheck $employerCheck
     * @uses EmployerCheckContainer::setEmployerCheck()
     */
    public function __construct(?EmployerCheck $employerCheck = null)
    {
        $this
            ->setEmployerCheck($employerCheck);
    }

    /**
     * Get employerCheck value
     *
     * @return EmployerCheck|null
     */
    public function getEmployerCheck(): ?EmployerCheck
    {
        return $this->employerCheck;
    }

    /**
     * Set employerCheck value
     *
     * @param EmployerCheck $employerCheck
     * @return EmployerCheckContainer
     */
    public function setEmployerCheck(?EmployerCheck $employerCheck = null): self
    {
        $this->employerCheck = $employerCheck;

        return $this;
    }
}
