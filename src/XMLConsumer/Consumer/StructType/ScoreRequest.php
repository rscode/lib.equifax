<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for ScoreRequest StructType
 *
 * @subpackage Structs
 */
class ScoreRequest extends CodedDataRequest
{
    /**
     * The scoreLabel
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - length: 7
     * - maxOccurs: 30
     * - minOccurs: 1
     *
     * @var string[]
     */
    protected array $scoreLabel;

    /**
     * Constructor method for ScoreRequest
     *
     * @param string[] $scoreLabel
     * @uses ScoreRequest::setScoreLabel()
     */
    public function __construct(array $scoreLabel)
    {
        $this
            ->setScoreLabel($scoreLabel);
    }

    /**
     * This method is responsible for validating the values passed to the setScoreLabel method
     * This method is willingly generated in order to preserve the one-line inline validation within the setScoreLabel method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateScoreLabelForArrayConstraintsFromSetScoreLabel(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $scoreRequestScoreLabelItem) {
            // validation for constraint: itemType
            if (!is_string($scoreRequestScoreLabelItem)) {
                $invalidValues[] = is_object($scoreRequestScoreLabelItem) ? get_class($scoreRequestScoreLabelItem) : sprintf(
                    '%s(%s)',
                    gettype($scoreRequestScoreLabelItem),
                    var_export($scoreRequestScoreLabelItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The scoreLabel property can only contain items of type string, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * This method is responsible for validating the value passed to the setScoreLabel method
     * This method is willingly generated in order to preserve the one-line inline validation within the setScoreLabel method
     * This has to validate that the items contained by the array match the length constraint
     *
     * @param mixed $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateScoreLabelForLengthConstraintFromSetScoreLabel($values): string
    {
        $message       = '';
        $invalidValues = [];
        foreach ($values as $scoreRequestScoreLabelItem) {
            // validation for constraint: length(7)
            if (mb_strlen((string)$scoreRequestScoreLabelItem) !== 7) {
                $invalidValues[] = var_export($scoreRequestScoreLabelItem, true);
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'Invalid length for value(s) %s, the number of characters/octets contained by the literal must be equal to 7',
                implode(', ', $invalidValues)
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get scoreLabel value
     *
     * @return string[]
     */
    public function getScoreLabel(): array
    {
        return $this->scoreLabel;
    }

    /**
     * Set scoreLabel value
     *
     * @param string[] $scoreLabel
     * @return ScoreRequest
     * @throws InvalidArgumentException
     */
    public function setScoreLabel(array $scoreLabel): self
    {
        // validation for constraint: array
        if ('' !== ($scoreLabelArrayErrorMessage = self::validateScoreLabelForArrayConstraintsFromSetScoreLabel($scoreLabel))) {
            throw new InvalidArgumentException($scoreLabelArrayErrorMessage, __LINE__);
        }
        // validation for constraint: length(7)
        if ('' !== ($scoreLabelLengthErrorMessage = self::validateScoreLabelForLengthConstraintFromSetScoreLabel($scoreLabel))) {
            throw new InvalidArgumentException($scoreLabelLengthErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(30)
        if (is_array($scoreLabel) && count($scoreLabel) > 30) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 30',
                count($scoreLabel)
            ), __LINE__);
        }
        $this->scoreLabel = $scoreLabel;

        return $this;
    }

    /**
     * Add item to scoreLabel value
     *
     * @param string $item
     * @return ScoreRequest
     * @throws InvalidArgumentException
     */
    public function addToScoreLabel(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf(
                'The scoreLabel property can only contain items of type string, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: length(7)
        if (mb_strlen((string)$item) !== 7) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be equal to 7',
                mb_strlen((string)$item)
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(30)
        if (is_array($this->scoreLabel) && count($this->scoreLabel) >= 30) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 30',
                count($this->scoreLabel)
            ), __LINE__);
        }
        $this->scoreLabel[] = $item;

        return $this;
    }
}
