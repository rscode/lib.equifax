<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for VerifyIdentityCheckResponse StructType
 * Meta information extracted from the WSDL
 * - documentation: This operation should be used if the identity verification search is NOT being performed to comply with anti-money laundering regulations - use the Anti Money Laundering operation for that purpose.
 *
 * @subpackage Structs
 */
class VerifyIdentityCheckResponse extends CommonResponse
{
}
