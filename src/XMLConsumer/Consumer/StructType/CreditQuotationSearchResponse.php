<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for CreditQuotationSearchResponse StructType
 * Meta information extracted from the WSDL
 * - documentation: Quotation for credit search (or combination with ID).
 *
 * @subpackage Structs
 */
class CreditQuotationSearchResponse extends CommonResponse
{
}
