<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\HomeTelephoneCheckStatus;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\TelephoneLineCheckStatus;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\TelephoneLineType;

/**
 * This class stands for HomeTelephoneCheck StructType
 *
 * @subpackage Structs
 */
class HomeTelephoneCheck extends DataItem
{
    /**
     * The businessLineCheckStatus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $businessLineCheckStatus;
    /**
     * The homeTelephoneCheckStatus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $homeTelephoneCheckStatus;
    /**
     * The idNumber
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 40
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $idNumber;
    /**
     * The lineType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $lineType;
    /**
     * The residentialLineCheckStatus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $residentialLineCheckStatus;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for HomeTelephoneCheck
     *
     * @param string $businessLineCheckStatus
     * @param string $homeTelephoneCheckStatus
     * @param string $idNumber
     * @param string $lineType
     * @param string $residentialLineCheckStatus
     * @param DOMDocument|string|null $any
     * @uses HomeTelephoneCheck::setBusinessLineCheckStatus()
     * @uses HomeTelephoneCheck::setHomeTelephoneCheckStatus()
     * @uses HomeTelephoneCheck::setIdNumber()
     * @uses HomeTelephoneCheck::setLineType()
     * @uses HomeTelephoneCheck::setResidentialLineCheckStatus()
     * @uses HomeTelephoneCheck::setAny()
     */
    public function __construct(
        string $businessLineCheckStatus,
        string $homeTelephoneCheckStatus,
        string $idNumber,
        string $lineType,
        string $residentialLineCheckStatus,
        $any = null
    ) {
        $this
            ->setBusinessLineCheckStatus($businessLineCheckStatus)
            ->setHomeTelephoneCheckStatus($homeTelephoneCheckStatus)
            ->setIdNumber($idNumber)
            ->setLineType($lineType)
            ->setResidentialLineCheckStatus($residentialLineCheckStatus)
            ->setAny($any);
    }

    /**
     * Get businessLineCheckStatus value
     *
     * @return string
     */
    public function getBusinessLineCheckStatus(): string
    {
        return $this->businessLineCheckStatus;
    }

    /**
     * Set businessLineCheckStatus value
     *
     * @param string $businessLineCheckStatus
     * @return HomeTelephoneCheck
     * @throws InvalidArgumentException
     * @uses TelephoneLineCheckStatus::getValidValues
     * @uses TelephoneLineCheckStatus::valueIsValid
     */
    public function setBusinessLineCheckStatus(string $businessLineCheckStatus): self
    {
        // validation for constraint: enumeration
        if (!TelephoneLineCheckStatus::valueIsValid($businessLineCheckStatus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\TelephoneLineCheckStatus',
                is_array($businessLineCheckStatus) ? implode(', ', $businessLineCheckStatus) : var_export($businessLineCheckStatus, true),
                implode(', ', TelephoneLineCheckStatus::getValidValues())
            ), __LINE__);
        }
        $this->businessLineCheckStatus = $businessLineCheckStatus;

        return $this;
    }

    /**
     * Get homeTelephoneCheckStatus value
     *
     * @return string
     */
    public function getHomeTelephoneCheckStatus(): string
    {
        return $this->homeTelephoneCheckStatus;
    }

    /**
     * Set homeTelephoneCheckStatus value
     *
     * @param string $homeTelephoneCheckStatus
     * @return HomeTelephoneCheck
     * @throws InvalidArgumentException
     * @uses HomeTelephoneCheckStatus::getValidValues
     * @uses HomeTelephoneCheckStatus::valueIsValid
     */
    public function setHomeTelephoneCheckStatus(string $homeTelephoneCheckStatus): self
    {
        // validation for constraint: enumeration
        if (!HomeTelephoneCheckStatus::valueIsValid($homeTelephoneCheckStatus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\HomeTelephoneCheckStatus',
                is_array($homeTelephoneCheckStatus) ? implode(', ', $homeTelephoneCheckStatus) : var_export(
                    $homeTelephoneCheckStatus,
                    true
                ),
                implode(', ', HomeTelephoneCheckStatus::getValidValues())
            ), __LINE__);
        }
        $this->homeTelephoneCheckStatus = $homeTelephoneCheckStatus;

        return $this;
    }

    /**
     * Get idNumber value
     *
     * @return string
     */
    public function getIdNumber(): string
    {
        return $this->idNumber;
    }

    /**
     * Set idNumber value
     *
     * @param string $idNumber
     * @return HomeTelephoneCheck
     */
    public function setIdNumber(string $idNumber): self
    {
        // validation for constraint: string
        if (!is_null($idNumber) && !is_string($idNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($idNumber, true),
                gettype($idNumber)
            ), __LINE__);
        }
        // validation for constraint: maxLength(40)
        if (!is_null($idNumber) && mb_strlen((string)$idNumber) > 40) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 40',
                mb_strlen((string)$idNumber)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($idNumber) && mb_strlen((string)$idNumber) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$idNumber)
            ), __LINE__);
        }
        $this->idNumber = $idNumber;

        return $this;
    }

    /**
     * Get lineType value
     *
     * @return string
     */
    public function getLineType(): string
    {
        return $this->lineType;
    }

    /**
     * Set lineType value
     *
     * @param string $lineType
     * @return HomeTelephoneCheck
     * @throws InvalidArgumentException
     * @uses TelephoneLineType::getValidValues
     * @uses TelephoneLineType::valueIsValid
     */
    public function setLineType(string $lineType): self
    {
        // validation for constraint: enumeration
        if (!TelephoneLineType::valueIsValid($lineType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\TelephoneLineType',
                is_array($lineType) ? implode(', ', $lineType) : var_export($lineType, true),
                implode(', ', TelephoneLineType::getValidValues())
            ), __LINE__);
        }
        $this->lineType = $lineType;

        return $this;
    }

    /**
     * Get residentialLineCheckStatus value
     *
     * @return string
     */
    public function getResidentialLineCheckStatus(): string
    {
        return $this->residentialLineCheckStatus;
    }

    /**
     * Set residentialLineCheckStatus value
     *
     * @param string $residentialLineCheckStatus
     * @return HomeTelephoneCheck
     * @throws InvalidArgumentException
     * @uses TelephoneLineCheckStatus::getValidValues
     * @uses TelephoneLineCheckStatus::valueIsValid
     */
    public function setResidentialLineCheckStatus(string $residentialLineCheckStatus): self
    {
        // validation for constraint: enumeration
        if (!TelephoneLineCheckStatus::valueIsValid($residentialLineCheckStatus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\TelephoneLineCheckStatus',
                is_array($residentialLineCheckStatus) ? implode(', ', $residentialLineCheckStatus) : var_export(
                    $residentialLineCheckStatus,
                    true
                ),
                implode(', ', TelephoneLineCheckStatus::getValidValues())
            ), __LINE__);
        }
        $this->residentialLineCheckStatus = $residentialLineCheckStatus;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return HomeTelephoneCheck
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
