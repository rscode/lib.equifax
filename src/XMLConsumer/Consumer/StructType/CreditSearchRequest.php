<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for CreditSearchRequest StructType
 * Meta information extracted from the WSDL
 * - documentation: The Credit Search operation is the most common type of enquiry and models the standard credit enquiry and allows the return of characteristic values, scores, raw and coded data items as required, that either results in a credit
 * search footprint, or search type of SR, for initial enquiries, or SE for subsequent enquiries.
 *
 * @subpackage Structs
 */
class CreditSearchRequest extends CommonRequest
{
    /**
     * The isSubsequentRequest
     * Meta information extracted from the WSDL
     * - documentation: Is this an initial credit search request or a subsequent enquiry.
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var bool|null
     */
    protected ?bool $isSubsequentRequest = null;

    /**
     * Constructor method for CreditSearchRequest
     *
     * @param bool $isSubsequentRequest
     * @uses CreditSearchRequest::setIsSubsequentRequest()
     */
    public function __construct(?bool $isSubsequentRequest = null)
    {
        $this
            ->setIsSubsequentRequest($isSubsequentRequest);
    }

    /**
     * Get isSubsequentRequest value
     *
     * @return bool|null
     */
    public function getIsSubsequentRequest(): ?bool
    {
        return $this->isSubsequentRequest;
    }

    /**
     * Set isSubsequentRequest value
     *
     * @param bool $isSubsequentRequest
     * @return CreditSearchRequest
     */
    public function setIsSubsequentRequest(?bool $isSubsequentRequest = null): self
    {
        // validation for constraint: boolean
        if (!is_null($isSubsequentRequest) && !is_bool($isSubsequentRequest)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($isSubsequentRequest, true),
                gettype($isSubsequentRequest)
            ), __LINE__);
        }
        $this->isSubsequentRequest = $isSubsequentRequest;

        return $this;
    }
}
