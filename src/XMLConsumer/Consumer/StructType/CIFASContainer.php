<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for CIFASContainer StructType
 *
 * @subpackage Structs
 */
class CIFASContainer extends DataItemContainer
{
    /**
     * The cifas
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var CIFAS[]
     */
    protected ?array $cifas = null;

    /**
     * Constructor method for CIFASContainer
     *
     * @param CIFAS[] $cifas
     * @uses CIFASContainer::setCifas()
     */
    public function __construct(?array $cifas = null)
    {
        $this
            ->setCifas($cifas);
    }

    /**
     * This method is responsible for validating the values passed to the setCifas method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCifas method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCifasForArrayConstraintsFromSetCifas(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $cIFASContainerCifasItem) {
            // validation for constraint: itemType
            if (!$cIFASContainerCifasItem instanceof CIFAS) {
                $invalidValues[] = is_object($cIFASContainerCifasItem) ? get_class($cIFASContainerCifasItem) : sprintf(
                    '%s(%s)',
                    gettype($cIFASContainerCifasItem),
                    var_export($cIFASContainerCifasItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The cifas property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\CIFAS, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get cifas value
     *
     * @return CIFAS[]
     */
    public function getCifas(): ?array
    {
        return $this->cifas;
    }

    /**
     * Set cifas value
     *
     * @param CIFAS[] $cifas
     * @return CIFASContainer
     * @throws InvalidArgumentException
     */
    public function setCifas(?array $cifas = null): self
    {
        // validation for constraint: array
        if ('' !== ($cifasArrayErrorMessage = self::validateCifasForArrayConstraintsFromSetCifas($cifas))) {
            throw new InvalidArgumentException($cifasArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($cifas) && count($cifas) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($cifas)
            ), __LINE__);
        }
        $this->cifas = $cifas;

        return $this;
    }

    /**
     * Add item to cifas value
     *
     * @param CIFAS $item
     * @return CIFASContainer
     * @throws InvalidArgumentException
     */
    public function addToCifas(CIFAS $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof CIFAS) {
            throw new InvalidArgumentException(sprintf(
                'The cifas property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\CIFAS, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->cifas) && count($this->cifas) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->cifas)
            ), __LINE__);
        }
        $this->cifas[] = $item;

        return $this;
    }
}
