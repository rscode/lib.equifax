<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for NameBase StructType
 *
 * @subpackage Structs
 */
abstract class NameBase extends AbstractStructBase
{
    /**
     * The surname
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxOccurs: 1
     * - minLength: 2
     * - minOccurs: 1
     * - whiteSpace: collapse
     *
     * @var string
     */
    protected string $surname;
    /**
     * The middleName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $middleName = null;
    /**
     * The title
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $title = null;

    /**
     * Constructor method for NameBase
     *
     * @param string $surname
     * @param string $middleName
     * @param string $title
     * @uses NameBase::setSurname()
     * @uses NameBase::setMiddleName()
     * @uses NameBase::setTitle()
     */
    public function __construct(string $surname, ?string $middleName = null, ?string $title = null)
    {
        $this
            ->setSurname($surname)
            ->setMiddleName($middleName)
            ->setTitle($title);
    }

    /**
     * Get surname value
     *
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * Set surname value
     *
     * @param string $surname
     * @return NameBase
     */
    public function setSurname(string $surname): self
    {
        // validation for constraint: string
        if (!is_null($surname) && !is_string($surname)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($surname, true),
                gettype($surname)
            ), __LINE__);
        }
        // validation for constraint: minLength(2)
        if (!is_null($surname) && mb_strlen((string)$surname) < 2) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 2',
                mb_strlen((string)$surname)
            ), __LINE__);
        }
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get middleName value
     *
     * @return string|null
     */
    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    /**
     * Set middleName value
     *
     * @param string $middleName
     * @return NameBase
     */
    public function setMiddleName(?string $middleName = null): self
    {
        // validation for constraint: string
        if (!is_null($middleName) && !is_string($middleName)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($middleName, true),
                gettype($middleName)
            ), __LINE__);
        }
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Get title value
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set title value
     *
     * @param string $title
     * @return NameBase
     */
    public function setTitle(?string $title = null): self
    {
        // validation for constraint: string
        if (!is_null($title) && !is_string($title)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($title, true),
                gettype($title)
            ), __LINE__);
        }
        $this->title = $title;

        return $this;
    }
}
