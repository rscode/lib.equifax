<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MatchRuleResult StructType
 *
 * @subpackage Structs
 */
class MatchRuleResult extends AbstractStructBase
{
    /**
     * The ruleNumber
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 8
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $ruleNumber;
    /**
     * The matchAchieved
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var bool|null
     */
    protected ?bool $matchAchieved = null;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for MatchRuleResult
     *
     * @param int                     $ruleNumber
     * @param bool                    $matchAchieved
     * @param DOMDocument|string|null $any
     * @uses MatchRuleResult::setRuleNumber()
     * @uses MatchRuleResult::setMatchAchieved()
     * @uses MatchRuleResult::setAny()
     */
    public function __construct(int $ruleNumber, ?bool $matchAchieved = null, $any = null)
    {
        $this
            ->setRuleNumber($ruleNumber)
            ->setMatchAchieved($matchAchieved)
            ->setAny($any);
    }

    /**
     * Get ruleNumber value
     *
     * @return int
     */
    public function getRuleNumber(): int
    {
        return $this->ruleNumber;
    }

    /**
     * Set ruleNumber value
     *
     * @param int $ruleNumber
     * @return MatchRuleResult
     */
    public function setRuleNumber(int $ruleNumber): self
    {
        // validation for constraint: int
        if (!is_null($ruleNumber) && !(is_int($ruleNumber) || ctype_digit($ruleNumber))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($ruleNumber, true),
                gettype($ruleNumber)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(8)
        if (!is_null($ruleNumber) && $ruleNumber > 8) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 8',
                var_export($ruleNumber, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($ruleNumber) && $ruleNumber < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($ruleNumber, true)
            ), __LINE__);
        }
        $this->ruleNumber = $ruleNumber;

        return $this;
    }

    /**
     * Get matchAchieved value
     *
     * @return bool|null
     */
    public function getMatchAchieved(): ?bool
    {
        return $this->matchAchieved;
    }

    /**
     * Set matchAchieved value
     *
     * @param bool $matchAchieved
     * @return MatchRuleResult
     */
    public function setMatchAchieved(?bool $matchAchieved = null): self
    {
        // validation for constraint: boolean
        if (!is_null($matchAchieved) && !is_bool($matchAchieved)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($matchAchieved, true),
                gettype($matchAchieved)
            ), __LINE__);
        }
        $this->matchAchieved = $matchAchieved;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return MatchRuleResult
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
