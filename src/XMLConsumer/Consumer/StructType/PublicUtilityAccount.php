<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for PublicUtilityAccount StructType
 *
 * @subpackage Structs
 */
class PublicUtilityAccount extends FinancialAgreement
{
}
