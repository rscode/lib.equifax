<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for ChargeCard StructType
 *
 * @subpackage Structs
 */
class ChargeCard extends FinancialAgreement
{
}
