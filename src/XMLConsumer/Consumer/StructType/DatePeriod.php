<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DatePeriod StructType
 *
 * @subpackage Structs
 */
class DatePeriod extends AbstractStructBase
{
    /**
     * The start
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * - union: date | gYear | gYearMonth | MMYY | MMYYYY
     *
     * @var string
     */
    protected string $start;
    /**
     * The end
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * - union: date | gYear | gYearMonth | MMYY | MMYYYY
     *
     * @var string|null
     */
    protected ?string $end = null;

    /**
     * Constructor method for DatePeriod
     *
     * @param string $start
     * @param string $end
     * @uses DatePeriod::setStart()
     * @uses DatePeriod::setEnd()
     */
    public function __construct(string $start, ?string $end = null)
    {
        $this
            ->setStart($start)
            ->setEnd($end);
    }

    /**
     * This method is responsible for validating the value passed to the setStart method
     * This method is willingly generated in order to preserve the one-line inline validation within the setStart method
     * This is a set of validation rules based on the union types associated to the property being set by the setStart method
     *
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateStartForUnionConstraintsFromSetStart($value): string
    {
        $message = '';
        // validation for constraint: int
        if (!is_null($value) && !(is_int($value) || ctype_digit($value))) {
            $exception0 = new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($value, true),
                gettype($value)
            ), __LINE__);
        }
        // validation for constraint: pattern((0[123456789]|1[012]){1}\d{2})
        if (!is_null($value) && !preg_match('/(0[123456789]|1[012]){1}\\d{2}/', $value)) {
            $exception1 = new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /(0[123456789]|1[012]){1}\\d{2}/',
                var_export($value, true)
            ), __LINE__);
        }
        // validation for constraint: pattern((0[123456789]|1[012]){1}\d{4})
        if (!is_null($value) && !preg_match('/(0[123456789]|1[012]){1}\\d{4}/', $value)) {
            $exception2 = new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /(0[123456789]|1[012]){1}\\d{4}/',
                var_export($value, true)
            ), __LINE__);
        }
        if (isset($exception0) && isset($exception1) && isset($exception2)) {
            $message = sprintf(
                "The value %s does not match any of the union rules: date, gYear, gYearMonth, MMYY, MMYYYY. See following errors:\n%s",
                var_export($value, true),
                implode("\n", array_map(function (InvalidArgumentException $e) {
                    return sprintf(' - %s', $e->getMessage());
                },
                [$exception0, $exception1, $exception2]))
            );
        }
        unset($exception0, $exception1, $exception2);

        return $message;
    }

    /**
     * This method is responsible for validating the value passed to the setEnd method
     * This method is willingly generated in order to preserve the one-line inline validation within the setEnd method
     * This is a set of validation rules based on the union types associated to the property being set by the setEnd method
     *
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateEndForUnionConstraintsFromSetEnd($value): string
    {
        $message = '';
        // validation for constraint: int
        if (!is_null($value) && !(is_int($value) || ctype_digit($value))) {
            $exception0 = new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($value, true),
                gettype($value)
            ), __LINE__);
        }
        // validation for constraint: pattern((0[123456789]|1[012]){1}\d{2})
        if (!is_null($value) && !preg_match('/(0[123456789]|1[012]){1}\\d{2}/', $value)) {
            $exception1 = new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /(0[123456789]|1[012]){1}\\d{2}/',
                var_export($value, true)
            ), __LINE__);
        }
        // validation for constraint: pattern((0[123456789]|1[012]){1}\d{4})
        if (!is_null($value) && !preg_match('/(0[123456789]|1[012]){1}\\d{4}/', $value)) {
            $exception2 = new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /(0[123456789]|1[012]){1}\\d{4}/',
                var_export($value, true)
            ), __LINE__);
        }
        if (isset($exception0) && isset($exception1) && isset($exception2)) {
            $message = sprintf(
                "The value %s does not match any of the union rules: date, gYear, gYearMonth, MMYY, MMYYYY. See following errors:\n%s",
                var_export($value, true),
                implode("\n", array_map(function (InvalidArgumentException $e) {
                    return sprintf(' - %s', $e->getMessage());
                },
                [$exception0, $exception1, $exception2]))
            );
        }
        unset($exception0, $exception1, $exception2);

        return $message;
    }

    /**
     * Get start value
     *
     * @return string
     */
    public function getStart(): string
    {
        return $this->start;
    }

    /**
     * Set start value
     *
     * @param string $start
     * @return DatePeriod
     */
    public function setStart(string $start): self
    {
        // validation for constraint: string
        if (!is_null($start) && !is_string($start)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($start, true),
                gettype($start)
            ), __LINE__);
        }
        // validation for constraint: union(date, gYear, gYearMonth, MMYY, MMYYYY)
        if ('' !== ($startUnionErrorMessage = self::validateStartForUnionConstraintsFromSetStart($start))) {
            throw new InvalidArgumentException($startUnionErrorMessage, __LINE__);
        }
        $this->start = $start;

        return $this;
    }

    /**
     * Get end value
     *
     * @return string|null
     */
    public function getEnd(): ?string
    {
        return $this->end;
    }

    /**
     * Set end value
     *
     * @param string $end
     * @return DatePeriod
     */
    public function setEnd(?string $end = null): self
    {
        // validation for constraint: string
        if (!is_null($end) && !is_string($end)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($end, true),
                gettype($end)
            ), __LINE__);
        }
        // validation for constraint: union(date, gYear, gYearMonth, MMYY, MMYYYY)
        if ('' !== ($endUnionErrorMessage = self::validateEndForUnionConstraintsFromSetEnd($end))) {
            throw new InvalidArgumentException($endUnionErrorMessage, __LINE__);
        }
        $this->end = $end;

        return $this;
    }
}
