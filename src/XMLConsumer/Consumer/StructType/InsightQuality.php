<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for InsightQuality StructType
 *
 * @subpackage Structs
 */
class InsightQuality extends AbstractStructBase
{
    /**
     * The qualityIndicator1
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var bool|null
     */
    protected ?bool $qualityIndicator1 = null;
    /**
     * The qualityIndicator2
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var bool|null
     */
    protected ?bool $qualityIndicator2 = null;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for InsightQuality
     *
     * @param bool                    $qualityIndicator1
     * @param bool                    $qualityIndicator2
     * @param DOMDocument|string|null $any
     * @uses InsightQuality::setQualityIndicator1()
     * @uses InsightQuality::setQualityIndicator2()
     * @uses InsightQuality::setAny()
     */
    public function __construct(?bool $qualityIndicator1 = null, ?bool $qualityIndicator2 = null, $any = null)
    {
        $this
            ->setQualityIndicator1($qualityIndicator1)
            ->setQualityIndicator2($qualityIndicator2)
            ->setAny($any);
    }

    /**
     * Get qualityIndicator1 value
     *
     * @return bool|null
     */
    public function getQualityIndicator1(): ?bool
    {
        return $this->qualityIndicator1;
    }

    /**
     * Set qualityIndicator1 value
     *
     * @param bool $qualityIndicator1
     * @return InsightQuality
     */
    public function setQualityIndicator1(?bool $qualityIndicator1 = null): self
    {
        // validation for constraint: boolean
        if (!is_null($qualityIndicator1) && !is_bool($qualityIndicator1)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($qualityIndicator1, true),
                gettype($qualityIndicator1)
            ), __LINE__);
        }
        $this->qualityIndicator1 = $qualityIndicator1;

        return $this;
    }

    /**
     * Get qualityIndicator2 value
     *
     * @return bool|null
     */
    public function getQualityIndicator2(): ?bool
    {
        return $this->qualityIndicator2;
    }

    /**
     * Set qualityIndicator2 value
     *
     * @param bool $qualityIndicator2
     * @return InsightQuality
     */
    public function setQualityIndicator2(?bool $qualityIndicator2 = null): self
    {
        // validation for constraint: boolean
        if (!is_null($qualityIndicator2) && !is_bool($qualityIndicator2)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($qualityIndicator2, true),
                gettype($qualityIndicator2)
            ), __LINE__);
        }
        $this->qualityIndicator2 = $qualityIndicator2;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return InsightQuality
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
