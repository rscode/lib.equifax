<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for RelativeAndCloseAssociate StructType
 *
 * @subpackage Structs
 */
class RelativeAndCloseAssociate extends AbstractStructBase
{
    /**
     * The associationType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $associationType = null;
    /**
     * The association
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $association = null;
    /**
     * The associationName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $associationName = null;

    /**
     * Constructor method for RelativeAndCloseAssociate
     *
     * @param string $associationType
     * @param string $association
     * @param string $associationName
     * @uses RelativeAndCloseAssociate::setAssociationType()
     * @uses RelativeAndCloseAssociate::setAssociation()
     * @uses RelativeAndCloseAssociate::setAssociationName()
     */
    public function __construct(?string $associationType = null, ?string $association = null, ?string $associationName = null)
    {
        $this
            ->setAssociationType($associationType)
            ->setAssociation($association)
            ->setAssociationName($associationName);
    }

    /**
     * Get associationType value
     *
     * @return string|null
     */
    public function getAssociationType(): ?string
    {
        return $this->associationType;
    }

    /**
     * Set associationType value
     *
     * @param string $associationType
     * @return RelativeAndCloseAssociate
     */
    public function setAssociationType(?string $associationType = null): self
    {
        // validation for constraint: string
        if (!is_null($associationType) && !is_string($associationType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($associationType, true),
                gettype($associationType)
            ), __LINE__);
        }
        $this->associationType = $associationType;

        return $this;
    }

    /**
     * Get association value
     *
     * @return string|null
     */
    public function getAssociation(): ?string
    {
        return $this->association;
    }

    /**
     * Set association value
     *
     * @param string $association
     * @return RelativeAndCloseAssociate
     */
    public function setAssociation(?string $association = null): self
    {
        // validation for constraint: string
        if (!is_null($association) && !is_string($association)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($association, true),
                gettype($association)
            ), __LINE__);
        }
        $this->association = $association;

        return $this;
    }

    /**
     * Get associationName value
     *
     * @return string|null
     */
    public function getAssociationName(): ?string
    {
        return $this->associationName;
    }

    /**
     * Set associationName value
     *
     * @param string $associationName
     * @return RelativeAndCloseAssociate
     */
    public function setAssociationName(?string $associationName = null): self
    {
        // validation for constraint: string
        if (!is_null($associationName) && !is_string($associationName)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($associationName, true),
                gettype($associationName)
            ), __LINE__);
        }
        $this->associationName = $associationName;

        return $this;
    }
}
