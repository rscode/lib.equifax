<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\ERSeniorityType;

/**
 * This class stands for ElectoralRollBase StructType
 *
 * @subpackage Structs
 */
abstract class ElectoralRollBase extends NameMatchedData
{
    /**
     * The seniority
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $seniority;

    /**
     * Constructor method for ElectoralRollBase
     *
     * @param string $seniority
     * @uses ElectoralRollBase::setSeniority()
     */
    public function __construct(string $seniority)
    {
        $this
            ->setSeniority($seniority);
    }

    /**
     * Get seniority value
     *
     * @return string
     */
    public function getSeniority(): string
    {
        return $this->seniority;
    }

    /**
     * Set seniority value
     *
     * @param string $seniority
     * @return ElectoralRollBase
     * @throws InvalidArgumentException
     * @uses ERSeniorityType::getValidValues
     * @uses ERSeniorityType::valueIsValid
     */
    public function setSeniority(string $seniority): self
    {
        // validation for constraint: enumeration
        if (!ERSeniorityType::valueIsValid($seniority)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\ERSeniorityType',
                is_array($seniority) ? implode(', ', $seniority) : var_export($seniority, true),
                implode(', ', ERSeniorityType::getValidValues())
            ), __LINE__);
        }
        $this->seniority = $seniority;

        return $this;
    }
}
