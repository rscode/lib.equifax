<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ImageReferences StructType
 *
 * @subpackage Structs
 */
class ImageReferences extends AbstractStructBase
{
    /**
     * The imageURL
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var string[]
     */
    protected ?array $imageURL = null;

    /**
     * Constructor method for ImageReferences
     *
     * @param string[] $imageURL
     * @uses ImageReferences::setImageURL()
     */
    public function __construct(?array $imageURL = null)
    {
        $this
            ->setImageURL($imageURL);
    }

    /**
     * This method is responsible for validating the values passed to the setImageURL method
     * This method is willingly generated in order to preserve the one-line inline validation within the setImageURL method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateImageURLForArrayConstraintsFromSetImageURL(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $imageReferencesImageURLItem) {
            // validation for constraint: itemType
            if (!is_string($imageReferencesImageURLItem)) {
                $invalidValues[] = is_object($imageReferencesImageURLItem) ? get_class($imageReferencesImageURLItem) : sprintf(
                    '%s(%s)',
                    gettype($imageReferencesImageURLItem),
                    var_export($imageReferencesImageURLItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The imageURL property can only contain items of type string, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get imageURL value
     *
     * @return string[]
     */
    public function getImageURL(): ?array
    {
        return $this->imageURL;
    }

    /**
     * Set imageURL value
     *
     * @param string[] $imageURL
     * @return ImageReferences
     * @throws InvalidArgumentException
     */
    public function setImageURL(?array $imageURL = null): self
    {
        // validation for constraint: array
        if ('' !== ($imageURLArrayErrorMessage = self::validateImageURLForArrayConstraintsFromSetImageURL($imageURL))) {
            throw new InvalidArgumentException($imageURLArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($imageURL) && count($imageURL) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($imageURL)
            ), __LINE__);
        }
        $this->imageURL = $imageURL;

        return $this;
    }

    /**
     * Add item to imageURL value
     *
     * @param string $item
     * @return ImageReferences
     * @throws InvalidArgumentException
     */
    public function addToImageURL(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf(
                'The imageURL property can only contain items of type string, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->imageURL) && count($this->imageURL) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->imageURL)
            ), __LINE__);
        }
        $this->imageURL[] = $item;

        return $this;
    }
}
