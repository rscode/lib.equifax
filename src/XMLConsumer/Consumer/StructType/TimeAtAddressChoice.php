<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TimeAtAddressChoice StructType
 *
 * @subpackage Structs
 */
class TimeAtAddressChoice extends AbstractStructBase
{
    /**
     * The timeAtAddressFrom
     * Meta information extracted from the WSDL
     * - choice: timeAtAddressFrom | timeAtAddressMoreThan | timeAtAddressPeriod
     * - choiceMaxOccurs: 1
     * - choiceMinOccurs: 1
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $timeAtAddressFrom;
    /**
     * The timeAtAddressMoreThan
     * Meta information extracted from the WSDL
     * - choice: timeAtAddressFrom | timeAtAddressMoreThan | timeAtAddressPeriod
     * - choiceMaxOccurs: 1
     * - choiceMinOccurs: 1
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $timeAtAddressMoreThan;
    /**
     * The timeAtAddressPeriod
     * Meta information extracted from the WSDL
     * - choice: timeAtAddressFrom | timeAtAddressMoreThan | timeAtAddressPeriod
     * - choiceMaxOccurs: 1
     * - choiceMinOccurs: 1
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $timeAtAddressPeriod;

    /**
     * Constructor method for TimeAtAddressChoice
     *
     * @param string $timeAtAddressFrom
     * @param string $timeAtAddressMoreThan
     * @param string $timeAtAddressPeriod
     * @uses TimeAtAddressChoice::setTimeAtAddressFrom()
     * @uses TimeAtAddressChoice::setTimeAtAddressMoreThan()
     * @uses TimeAtAddressChoice::setTimeAtAddressPeriod()
     */
    public function __construct(string $timeAtAddressFrom, string $timeAtAddressMoreThan, string $timeAtAddressPeriod)
    {
        $this
            ->setTimeAtAddressFrom($timeAtAddressFrom)
            ->setTimeAtAddressMoreThan($timeAtAddressMoreThan)
            ->setTimeAtAddressPeriod($timeAtAddressPeriod);
    }

    /**
     * Get timeAtAddressFrom value
     *
     * @return string
     */
    public function getTimeAtAddressFrom(): string
    {
        return isset($this->timeAtAddressFrom) ? $this->timeAtAddressFrom : null;
    }

    /**
     * Set timeAtAddressFrom value
     * This property belongs to a choice that allows only one property to exist. It is
     * therefore removable from the request, consequently if the value assigned to this
     * property is null, the property is removed from this object
     *
     * @param string $timeAtAddressFrom
     * @return TimeAtAddressChoice
     * @throws InvalidArgumentException
     */
    public function setTimeAtAddressFrom(string $timeAtAddressFrom): self
    {
        // validation for constraint: string
        if (!is_null($timeAtAddressFrom) && !is_string($timeAtAddressFrom)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($timeAtAddressFrom, true),
                gettype($timeAtAddressFrom)
            ), __LINE__);
        }
        // validation for constraint: choice(timeAtAddressFrom, timeAtAddressMoreThan, timeAtAddressPeriod)
        if ('' !== ($timeAtAddressFromChoiceErrorMessage = self::validateTimeAtAddressFromForChoiceConstraintsFromSetTimeAtAddressFrom($timeAtAddressFrom))) {
            throw new InvalidArgumentException($timeAtAddressFromChoiceErrorMessage, __LINE__);
        }
        if (is_null($timeAtAddressFrom) || (is_array($timeAtAddressFrom) && empty($timeAtAddressFrom))) {
            unset($this->timeAtAddressFrom);
        } else {
            $this->timeAtAddressFrom = $timeAtAddressFrom;
        }

        return $this;
    }

    /**
     * This method is responsible for validating the value passed to the setTimeAtAddressFrom method
     * This method is willingly generated in order to preserve the one-line inline validation within the setTimeAtAddressFrom method
     * This has to validate that the property which is being set is the only one among the given choices
     *
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public function validateTimeAtAddressFromForChoiceConstraintsFromSetTimeAtAddressFrom($value): string
    {
        $message = '';
        if (is_null($value)) {
            return $message;
        }
        $properties = [
            'timeAtAddressMoreThan',
            'timeAtAddressPeriod',
        ];
        try {
            foreach ($properties as $property) {
                if (isset($this->{$property})) {
                    throw new InvalidArgumentException(sprintf(
                        'The property timeAtAddressFrom can\'t be set as the property %s is already set. Only one property must be set among these properties: timeAtAddressFrom, %s.',
                        $property,
                        implode(', ', $properties)
                    ), __LINE__);
                }
            }
        } catch (InvalidArgumentException $e) {
            $message = $e->getMessage();
        }

        return $message;
    }

    /**
     * Get timeAtAddressMoreThan value
     *
     * @return string
     */
    public function getTimeAtAddressMoreThan(): string
    {
        return isset($this->timeAtAddressMoreThan) ? $this->timeAtAddressMoreThan : null;
    }

    /**
     * Set timeAtAddressMoreThan value
     * This property belongs to a choice that allows only one property to exist. It is
     * therefore removable from the request, consequently if the value assigned to this
     * property is null, the property is removed from this object
     *
     * @param string $timeAtAddressMoreThan
     * @return TimeAtAddressChoice
     * @throws InvalidArgumentException
     */
    public function setTimeAtAddressMoreThan(string $timeAtAddressMoreThan): self
    {
        // validation for constraint: string
        if (!is_null($timeAtAddressMoreThan) && !is_string($timeAtAddressMoreThan)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($timeAtAddressMoreThan, true),
                gettype($timeAtAddressMoreThan)
            ), __LINE__);
        }
        // validation for constraint: choice(timeAtAddressFrom, timeAtAddressMoreThan, timeAtAddressPeriod)
        if ('' !== ($timeAtAddressMoreThanChoiceErrorMessage = self::validateTimeAtAddressMoreThanForChoiceConstraintsFromSetTimeAtAddressMoreThan($timeAtAddressMoreThan))) {
            throw new InvalidArgumentException($timeAtAddressMoreThanChoiceErrorMessage, __LINE__);
        }
        if (is_null($timeAtAddressMoreThan) || (is_array($timeAtAddressMoreThan) && empty($timeAtAddressMoreThan))) {
            unset($this->timeAtAddressMoreThan);
        } else {
            $this->timeAtAddressMoreThan = $timeAtAddressMoreThan;
        }

        return $this;
    }

    /**
     * This method is responsible for validating the value passed to the setTimeAtAddressMoreThan method
     * This method is willingly generated in order to preserve the one-line inline validation within the setTimeAtAddressMoreThan method
     * This has to validate that the property which is being set is the only one among the given choices
     *
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public function validateTimeAtAddressMoreThanForChoiceConstraintsFromSetTimeAtAddressMoreThan($value): string
    {
        $message = '';
        if (is_null($value)) {
            return $message;
        }
        $properties = [
            'timeAtAddressFrom',
            'timeAtAddressPeriod',
        ];
        try {
            foreach ($properties as $property) {
                if (isset($this->{$property})) {
                    throw new InvalidArgumentException(sprintf(
                        'The property timeAtAddressMoreThan can\'t be set as the property %s is already set. Only one property must be set among these properties: timeAtAddressMoreThan, %s.',
                        $property,
                        implode(', ', $properties)
                    ), __LINE__);
                }
            }
        } catch (InvalidArgumentException $e) {
            $message = $e->getMessage();
        }

        return $message;
    }

    /**
     * Get timeAtAddressPeriod value
     *
     * @return string
     */
    public function getTimeAtAddressPeriod(): string
    {
        return isset($this->timeAtAddressPeriod) ? $this->timeAtAddressPeriod : null;
    }

    /**
     * Set timeAtAddressPeriod value
     * This property belongs to a choice that allows only one property to exist. It is
     * therefore removable from the request, consequently if the value assigned to this
     * property is null, the property is removed from this object
     *
     * @param string $timeAtAddressPeriod
     * @return TimeAtAddressChoice
     * @throws InvalidArgumentException
     */
    public function setTimeAtAddressPeriod(string $timeAtAddressPeriod): self
    {
        // validation for constraint: string
        if (!is_null($timeAtAddressPeriod) && !is_string($timeAtAddressPeriod)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($timeAtAddressPeriod, true),
                gettype($timeAtAddressPeriod)
            ), __LINE__);
        }
        // validation for constraint: choice(timeAtAddressFrom, timeAtAddressMoreThan, timeAtAddressPeriod)
        if ('' !== ($timeAtAddressPeriodChoiceErrorMessage = self::validateTimeAtAddressPeriodForChoiceConstraintsFromSetTimeAtAddressPeriod($timeAtAddressPeriod))) {
            throw new InvalidArgumentException($timeAtAddressPeriodChoiceErrorMessage, __LINE__);
        }
        if (is_null($timeAtAddressPeriod) || (is_array($timeAtAddressPeriod) && empty($timeAtAddressPeriod))) {
            unset($this->timeAtAddressPeriod);
        } else {
            $this->timeAtAddressPeriod = $timeAtAddressPeriod;
        }

        return $this;
    }

    /**
     * This method is responsible for validating the value passed to the setTimeAtAddressPeriod method
     * This method is willingly generated in order to preserve the one-line inline validation within the setTimeAtAddressPeriod method
     * This has to validate that the property which is being set is the only one among the given choices
     *
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public function validateTimeAtAddressPeriodForChoiceConstraintsFromSetTimeAtAddressPeriod($value): string
    {
        $message = '';
        if (is_null($value)) {
            return $message;
        }
        $properties = [
            'timeAtAddressFrom',
            'timeAtAddressMoreThan',
        ];
        try {
            foreach ($properties as $property) {
                if (isset($this->{$property})) {
                    throw new InvalidArgumentException(sprintf(
                        'The property timeAtAddressPeriod can\'t be set as the property %s is already set. Only one property must be set among these properties: timeAtAddressPeriod, %s.',
                        $property,
                        implode(', ', $properties)
                    ), __LINE__);
                }
            }
        } catch (InvalidArgumentException $e) {
            $message = $e->getMessage();
        }

        return $message;
    }
}
