<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for RequestPreviousResidenceInstance StructType
 *
 * @subpackage Structs
 */
class RequestPreviousResidenceInstance extends AbstractStructBase
{
    /**
     * The continueOnFailedAddressMatch
     * Meta information extracted from the WSDL
     * - documentation: Default value = false
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $continueOnFailedAddressMatch;
    /**
     * The residenceInstance
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var ResidenceInstance
     */
    protected ResidenceInstance $residenceInstance;

    /**
     * Constructor method for RequestPreviousResidenceInstance
     *
     * @param bool              $continueOnFailedAddressMatch
     * @param ResidenceInstance $residenceInstance
     * @uses RequestPreviousResidenceInstance::setContinueOnFailedAddressMatch()
     * @uses RequestPreviousResidenceInstance::setResidenceInstance()
     */
    public function __construct(bool $continueOnFailedAddressMatch, ResidenceInstance $residenceInstance)
    {
        $this
            ->setContinueOnFailedAddressMatch($continueOnFailedAddressMatch)
            ->setResidenceInstance($residenceInstance);
    }

    /**
     * Get continueOnFailedAddressMatch value
     *
     * @return bool
     */
    public function getContinueOnFailedAddressMatch(): bool
    {
        return $this->continueOnFailedAddressMatch;
    }

    /**
     * Set continueOnFailedAddressMatch value
     *
     * @param bool $continueOnFailedAddressMatch
     * @return RequestPreviousResidenceInstance
     */
    public function setContinueOnFailedAddressMatch(bool $continueOnFailedAddressMatch): self
    {
        // validation for constraint: boolean
        if (!is_null($continueOnFailedAddressMatch) && !is_bool($continueOnFailedAddressMatch)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($continueOnFailedAddressMatch, true),
                gettype($continueOnFailedAddressMatch)
            ), __LINE__);
        }
        $this->continueOnFailedAddressMatch = $continueOnFailedAddressMatch;

        return $this;
    }

    /**
     * Get residenceInstance value
     *
     * @return ResidenceInstance
     */
    public function getResidenceInstance(): ResidenceInstance
    {
        return $this->residenceInstance;
    }

    /**
     * Set residenceInstance value
     *
     * @param ResidenceInstance $residenceInstance
     * @return RequestPreviousResidenceInstance
     */
    public function setResidenceInstance(ResidenceInstance $residenceInstance): self
    {
        $this->residenceInstance = $residenceInstance;

        return $this;
    }
}
