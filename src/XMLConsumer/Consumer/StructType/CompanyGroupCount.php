<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CompanyGroupCount StructType
 *
 * @subpackage Structs
 */
class CompanyGroupCount extends AbstractStructBase
{
    /**
     * The companyGroup
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var CompanyGroup
     */
    protected CompanyGroup $companyGroup;
    /**
     * The numberOfAccounts
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 99
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $numberOfAccounts;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for CompanyGroupCount
     *
     * @param CompanyGroup            $companyGroup
     * @param int                     $numberOfAccounts
     * @param DOMDocument|string|null $any
     * @uses CompanyGroupCount::setCompanyGroup()
     * @uses CompanyGroupCount::setNumberOfAccounts()
     * @uses CompanyGroupCount::setAny()
     */
    public function __construct(CompanyGroup $companyGroup, int $numberOfAccounts, $any = null)
    {
        $this
            ->setCompanyGroup($companyGroup)
            ->setNumberOfAccounts($numberOfAccounts)
            ->setAny($any);
    }

    /**
     * Get companyGroup value
     *
     * @return CompanyGroup
     */
    public function getCompanyGroup(): CompanyGroup
    {
        return $this->companyGroup;
    }

    /**
     * Set companyGroup value
     *
     * @param CompanyGroup $companyGroup
     * @return CompanyGroupCount
     */
    public function setCompanyGroup(CompanyGroup $companyGroup): self
    {
        $this->companyGroup = $companyGroup;

        return $this;
    }

    /**
     * Get numberOfAccounts value
     *
     * @return int
     */
    public function getNumberOfAccounts(): int
    {
        return $this->numberOfAccounts;
    }

    /**
     * Set numberOfAccounts value
     *
     * @param int $numberOfAccounts
     * @return CompanyGroupCount
     */
    public function setNumberOfAccounts(int $numberOfAccounts): self
    {
        // validation for constraint: int
        if (!is_null($numberOfAccounts) && !(is_int($numberOfAccounts) || ctype_digit($numberOfAccounts))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($numberOfAccounts, true),
                gettype($numberOfAccounts)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(99)
        if (!is_null($numberOfAccounts) && $numberOfAccounts > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 99',
                var_export($numberOfAccounts, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($numberOfAccounts) && $numberOfAccounts < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($numberOfAccounts, true)
            ), __LINE__);
        }
        $this->numberOfAccounts = $numberOfAccounts;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return CompanyGroupCount
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
