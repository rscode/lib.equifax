<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for IdCheckByOtherPublicSectorResponse StructType
 * Meta information extracted from the WSDL
 * - documentation: This operation is used for identity verification of an individual mainly by public sector other than IPS
 *
 * @subpackage Structs
 */
class IdCheckByOtherPublicSectorResponse extends CommonResponse
{
}
