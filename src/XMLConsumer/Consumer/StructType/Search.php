<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Search StructType
 *
 * @subpackage Structs
 */
abstract class Search extends AbstractStructBase
{
    /**
     * The matchCriteria
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var MatchCriteria
     */
    protected MatchCriteria $matchCriteria;
    /**
     * The requestedData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var RequestedData
     */
    protected RequestedData $requestedData;
    /**
     * The creditSearchConfig
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var CreditSearchConfig|null
     */
    protected ?CreditSearchConfig $creditSearchConfig = null;

    public function __construct()
    {
        $this->matchCriteria = new MatchCriteria();
        $this->requestedData = new RequestedData();
    }

    public function getMatchCriteria(): MatchCriteria
    {
        return $this->matchCriteria;
    }

    public function setMatchCriteria(MatchCriteria $matchCriteria): self
    {
        $this->matchCriteria = $matchCriteria;

        return $this;
    }

    public function getRequestedData(): RequestedData
    {
        return $this->requestedData;
    }

    public function setRequestedData(RequestedData $requestedData): self
    {
        $this->requestedData = $requestedData;

        return $this;
    }

    public function getCreditSearchConfig(): ?CreditSearchConfig
    {
        return $this->creditSearchConfig;
    }

    public function setCreditSearchConfig(?CreditSearchConfig $creditSearchConfig = null): self
    {
        $this->creditSearchConfig = $creditSearchConfig;

        return $this;
    }
}
