<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\AddressMatchingStatus;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SuppliedAddressData StructType
 *
 * @subpackage Structs
 */
class SuppliedAddressData extends AbstractStructBase
{
    /**
     * The addressMatchStatus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $addressMatchStatus = null;
    /**
     * The addressSpecificData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AddressSpecificData|null
     */
    protected ?AddressSpecificData $addressSpecificData = null;
    /**
     * The index
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 99
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 0
     *
     * @var int|null
     */
    protected ?int $index = null;
    /**
     * The inputAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var FreeFormatConsumerAddress|null
     */
    protected ?FreeFormatConsumerAddress $inputAddress = null;
    /**
     * The noticeOfCorrectionOrDisputePresent
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var bool|null
     */
    protected ?bool $noticeOfCorrectionOrDisputePresent = null;
    /**
     * The matchedAddress
     *
     * @var MatchedConsumerAddress|null
     */
    protected ?MatchedConsumerAddress $matchedAddress = null;
    /**
     * The potentialMatchedAddress
     *
     * @var MatchedConsumerAddress|null
     */
    protected ?MatchedConsumerAddress $potentialMatchedAddress = null;
    /**
     * The unmatchedAddress
     *
     * @var ResidentialStructuredAddress|null
     */
    protected ?ResidentialStructuredAddress $unmatchedAddress = null;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;
    /**
     * The id
     * Meta information extracted from the WSDL
     * - use: optional
     *
     * @var string|null
     */
    protected ?string $id = null;
    /**
     * The ref
     * Meta information extracted from the WSDL
     * - use: optional
     *
     * @var string|null
     */
    protected ?string $ref = null;

    /**
     * Constructor method for SuppliedAddressData
     *
     * @param string                       $addressMatchStatus
     * @param AddressSpecificData          $addressSpecificData
     * @param int                          $index
     * @param FreeFormatConsumerAddress    $inputAddress
     * @param bool                         $noticeOfCorrectionOrDisputePresent
     * @param MatchedConsumerAddress       $matchedAddress
     * @param MatchedConsumerAddress       $potentialMatchedAddress
     * @param ResidentialStructuredAddress $unmatchedAddress
     * @param DOMDocument|string|null      $any
     * @param string                       $id
     * @param string                       $ref
     * @uses SuppliedAddressData::setAddressMatchStatus()
     * @uses SuppliedAddressData::setAddressSpecificData()
     * @uses SuppliedAddressData::setIndex()
     * @uses SuppliedAddressData::setInputAddress()
     * @uses SuppliedAddressData::setNoticeOfCorrectionOrDisputePresent()
     * @uses SuppliedAddressData::setMatchedAddress()
     * @uses SuppliedAddressData::setPotentialMatchedAddress()
     * @uses SuppliedAddressData::setUnmatchedAddress()
     * @uses SuppliedAddressData::setAny()
     * @uses SuppliedAddressData::setId()
     * @uses SuppliedAddressData::setRef()
     */
    public function __construct(
        ?string $addressMatchStatus = null,
        ?AddressSpecificData $addressSpecificData = null,
        ?int $index = null,
        ?FreeFormatConsumerAddress $inputAddress = null,
        ?bool $noticeOfCorrectionOrDisputePresent = null,
        ?MatchedConsumerAddress $matchedAddress = null,
        ?MatchedConsumerAddress $potentialMatchedAddress = null,
        ?ResidentialStructuredAddress $unmatchedAddress = null,
        $any = null,
        ?string $id = null,
        ?string $ref = null
    ) {
        $this
            ->setAddressMatchStatus($addressMatchStatus)
            ->setAddressSpecificData($addressSpecificData)
            ->setIndex($index)
            ->setInputAddress($inputAddress)
            ->setNoticeOfCorrectionOrDisputePresent($noticeOfCorrectionOrDisputePresent)
            ->setMatchedAddress($matchedAddress)
            ->setPotentialMatchedAddress($potentialMatchedAddress)
            ->setUnmatchedAddress($unmatchedAddress)
            ->setAny($any)
            ->setId($id)
            ->setRef($ref);
    }

    /**
     * Get addressMatchStatus value
     *
     * @return string|null
     */
    public function getAddressMatchStatus(): ?string
    {
        return $this->addressMatchStatus;
    }

    /**
     * Set addressMatchStatus value
     *
     * @param string $addressMatchStatus
     * @return SuppliedAddressData
     * @throws InvalidArgumentException
     * @uses AddressMatchingStatus::getValidValues
     * @uses AddressMatchingStatus::valueIsValid
     */
    public function setAddressMatchStatus(?string $addressMatchStatus = null): self
    {
        // validation for constraint: enumeration
        if (!AddressMatchingStatus::valueIsValid($addressMatchStatus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\AddressMatchingStatus',
                is_array($addressMatchStatus) ? implode(', ', $addressMatchStatus) : var_export($addressMatchStatus, true),
                implode(', ', AddressMatchingStatus::getValidValues())
            ), __LINE__);
        }
        $this->addressMatchStatus = $addressMatchStatus;

        return $this;
    }

    /**
     * Get addressSpecificData value
     *
     * @return AddressSpecificData|null
     */
    public function getAddressSpecificData(): ?AddressSpecificData
    {
        return $this->addressSpecificData;
    }

    /**
     * Set addressSpecificData value
     *
     * @param AddressSpecificData $addressSpecificData
     * @return SuppliedAddressData
     */
    public function setAddressSpecificData(?AddressSpecificData $addressSpecificData = null): self
    {
        $this->addressSpecificData = $addressSpecificData;

        return $this;
    }

    /**
     * Get index value
     *
     * @return int|null
     */
    public function getIndex(): ?int
    {
        return $this->index;
    }

    /**
     * Set index value
     *
     * @param int $index
     * @return SuppliedAddressData
     */
    public function setIndex(?int $index = null): self
    {
        // validation for constraint: int
        if (!is_null($index) && !(is_int($index) || ctype_digit($index))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($index, true),
                gettype($index)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(99)
        if (!is_null($index) && $index > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 99',
                var_export($index, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($index) && $index < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($index, true)
            ), __LINE__);
        }
        $this->index = $index;

        return $this;
    }

    /**
     * Get inputAddress value
     *
     * @return FreeFormatConsumerAddress|null
     */
    public function getInputAddress(): ?FreeFormatConsumerAddress
    {
        return $this->inputAddress;
    }

    /**
     * Set inputAddress value
     *
     * @param FreeFormatConsumerAddress $inputAddress
     * @return SuppliedAddressData
     */
    public function setInputAddress(?FreeFormatConsumerAddress $inputAddress = null): self
    {
        $this->inputAddress = $inputAddress;

        return $this;
    }

    /**
     * Get noticeOfCorrectionOrDisputePresent value
     *
     * @return bool|null
     */
    public function getNoticeOfCorrectionOrDisputePresent(): ?bool
    {
        return $this->noticeOfCorrectionOrDisputePresent;
    }

    /**
     * Set noticeOfCorrectionOrDisputePresent value
     *
     * @param bool $noticeOfCorrectionOrDisputePresent
     * @return SuppliedAddressData
     */
    public function setNoticeOfCorrectionOrDisputePresent(?bool $noticeOfCorrectionOrDisputePresent = null): self
    {
        // validation for constraint: boolean
        if (!is_null($noticeOfCorrectionOrDisputePresent) && !is_bool($noticeOfCorrectionOrDisputePresent)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($noticeOfCorrectionOrDisputePresent, true),
                gettype($noticeOfCorrectionOrDisputePresent)
            ), __LINE__);
        }
        $this->noticeOfCorrectionOrDisputePresent = $noticeOfCorrectionOrDisputePresent;

        return $this;
    }

    /**
     * Get matchedAddress value
     *
     * @return MatchedConsumerAddress|null
     */
    public function getMatchedAddress(): ?MatchedConsumerAddress
    {
        return $this->matchedAddress;
    }

    /**
     * Set matchedAddress value
     *
     * @param MatchedConsumerAddress $matchedAddress
     * @return SuppliedAddressData
     */
    public function setMatchedAddress(?MatchedConsumerAddress $matchedAddress = null): self
    {
        $this->matchedAddress = $matchedAddress;

        return $this;
    }

    /**
     * Get potentialMatchedAddress value
     *
     * @return MatchedConsumerAddress|null
     */
    public function getPotentialMatchedAddress(): ?MatchedConsumerAddress
    {
        return $this->potentialMatchedAddress;
    }

    /**
     * Set potentialMatchedAddress value
     *
     * @param MatchedConsumerAddress $potentialMatchedAddress
     * @return SuppliedAddressData
     */
    public function setPotentialMatchedAddress(?MatchedConsumerAddress $potentialMatchedAddress = null): self
    {
        $this->potentialMatchedAddress = $potentialMatchedAddress;

        return $this;
    }

    /**
     * Get unmatchedAddress value
     *
     * @return ResidentialStructuredAddress|null
     */
    public function getUnmatchedAddress(): ?ResidentialStructuredAddress
    {
        return $this->unmatchedAddress;
    }

    /**
     * Set unmatchedAddress value
     *
     * @param ResidentialStructuredAddress $unmatchedAddress
     * @return SuppliedAddressData
     */
    public function setUnmatchedAddress(?ResidentialStructuredAddress $unmatchedAddress = null): self
    {
        $this->unmatchedAddress = $unmatchedAddress;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return SuppliedAddressData
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }

    /**
     * Get id value
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * Set id value
     *
     * @param string $id
     * @return SuppliedAddressData
     */
    public function setId(?string $id = null): self
    {
        // validation for constraint: string
        if (!is_null($id) && !is_string($id)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($id, true),
                gettype($id)
            ), __LINE__);
        }
        $this->id = $id;

        return $this;
    }

    /**
     * Get ref value
     *
     * @return string|null
     */
    public function getRef(): ?string
    {
        return $this->ref;
    }

    /**
     * Set ref value
     *
     * @param string $ref
     * @return SuppliedAddressData
     */
    public function setRef(?string $ref = null): self
    {
        // validation for constraint: string
        if (!is_null($ref) && !is_string($ref)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($ref, true),
                gettype($ref)
            ), __LINE__);
        }
        $this->ref = $ref;

        return $this;
    }
}
