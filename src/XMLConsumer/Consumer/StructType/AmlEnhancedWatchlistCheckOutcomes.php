<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AmlEnhancedWatchlistCheckOutcomes StructType
 *
 * @subpackage Structs
 */
class AmlEnhancedWatchlistCheckOutcomes extends AbstractStructBase
{
    /**
     * The firstApplicant
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var FirstApplicant
     */
    protected FirstApplicant $firstApplicant;
    /**
     * The secondApplicant
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var SecondApplicant|null
     */
    protected ?SecondApplicant $secondApplicant = null;

    /**
     * Constructor method for AmlEnhancedWatchlistCheckOutcomes
     *
     * @param FirstApplicant  $firstApplicant
     * @param SecondApplicant $secondApplicant
     * @uses AmlEnhancedWatchlistCheckOutcomes::setFirstApplicant()
     * @uses AmlEnhancedWatchlistCheckOutcomes::setSecondApplicant()
     */
    public function __construct(FirstApplicant $firstApplicant, ?SecondApplicant $secondApplicant = null)
    {
        $this
            ->setFirstApplicant($firstApplicant)
            ->setSecondApplicant($secondApplicant);
    }

    /**
     * Get firstApplicant value
     *
     * @return FirstApplicant
     */
    public function getFirstApplicant(): FirstApplicant
    {
        return $this->firstApplicant;
    }

    /**
     * Set firstApplicant value
     *
     * @param FirstApplicant $firstApplicant
     * @return AmlEnhancedWatchlistCheckOutcomes
     */
    public function setFirstApplicant(FirstApplicant $firstApplicant): self
    {
        $this->firstApplicant = $firstApplicant;

        return $this;
    }

    /**
     * Get secondApplicant value
     *
     * @return SecondApplicant|null
     */
    public function getSecondApplicant(): ?SecondApplicant
    {
        return $this->secondApplicant;
    }

    /**
     * Set secondApplicant value
     *
     * @param SecondApplicant $secondApplicant
     * @return AmlEnhancedWatchlistCheckOutcomes
     */
    public function setSecondApplicant(?SecondApplicant $secondApplicant = null): self
    {
        $this->secondApplicant = $secondApplicant;

        return $this;
    }
}
