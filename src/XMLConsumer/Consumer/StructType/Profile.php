<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Profile StructType
 *
 * @subpackage Structs
 */
class Profile extends AbstractStructBase
{
    /**
     * The profileNotes
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $profileNotes;

    /**
     * Constructor method for Profile
     *
     * @param string $profileNotes
     * @uses Profile::setProfileNotes()
     */
    public function __construct(string $profileNotes)
    {
        $this
            ->setProfileNotes($profileNotes);
    }

    /**
     * Get profileNotes value
     *
     * @return string
     */
    public function getProfileNotes(): string
    {
        return $this->profileNotes;
    }

    /**
     * Set profileNotes value
     *
     * @param string $profileNotes
     * @return Profile
     */
    public function setProfileNotes(string $profileNotes): self
    {
        // validation for constraint: string
        if (!is_null($profileNotes) && !is_string($profileNotes)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($profileNotes, true),
                gettype($profileNotes)
            ), __LINE__);
        }
        $this->profileNotes = $profileNotes;

        return $this;
    }
}
