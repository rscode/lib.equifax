<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for MultipleAddressRequest StructType
 * Meta information extracted from the WSDL
 * - documentation: In situations where the supplied address(es) matches to more than one address on the Equifax database, in order to receive the address text of the multiple addresses that have been matched, a Multiple Address Match request must be
 * included within the list of requested data types.
 *
 * @subpackage Structs
 */
class MultipleAddressRequest extends DataRequest
{
}
