<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for IDPlusRequest StructType
 *
 * @subpackage Structs
 */
class IDPlusRequest extends CodedDataAvailableAtLinkedAddressRequest
{
}
