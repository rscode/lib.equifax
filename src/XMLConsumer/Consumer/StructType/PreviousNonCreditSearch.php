<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\NonCreditSearchType;

/**
 * This class stands for PreviousNonCreditSearch StructType
 *
 * @subpackage Structs
 */
class PreviousNonCreditSearch extends PreviousSearch
{
    /**
     * The searchType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $searchType;


    public function __construct(string $searchType)
    {
        $this
            ->setSearchType($searchType);
    }

    /**
     * Get searchType value
     *
     * @return string
     */
    public function getSearchType(): string
    {
        return $this->searchType;
    }

    /**
     * Set searchType value
     *
     * @param string $searchType
     * @return PreviousNonCreditSearch
     * @throws InvalidArgumentException
     * @uses NonCreditSearchType::getValidValues
     * @uses NonCreditSearchType::valueIsValid
     */
    public function setSearchType(string $searchType): self
    {
        // validation for constraint: enumeration
        if (!NonCreditSearchType::valueIsValid($searchType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\NonCreditSearchType',
                is_array($searchType) ? implode(', ', $searchType) : var_export($searchType, true),
                implode(', ', NonCreditSearchType::getValidValues())
            ), __LINE__);
        }
        $this->searchType = $searchType;

        return $this;
    }
}
