<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for PersonnelVettingEnquiryResponse StructType
 * Meta information extracted from the WSDL
 * - documentation: Personnel vetting
 *
 * @subpackage Structs
 */
class PersonnelVettingEnquiryResponse extends CommonResponse
{
}
