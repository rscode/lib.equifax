<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * The attributes block is a flexible mechanism that enables Equifax to supply a selection of pre-defined, coded characteristics. Each
 * customer can have access to up to six defined attribute blocks, each containing a combination of attributes, although only one attribute
 * group can be requested for a single enquiry. Each enquiry then simply requests the appropriate attribute block number and the
 * corresponding attributes are returned. 'Same company' information can be excluded from the attributes returned via the
 * <employSameCompanyInsight> boolean element. Equifax can provide consultancy to assist in the selection of appropriate attributes. Part
 * of the consultancy process offered by Equifax is a retrospective analysis ('retro'), the process of calculating the
 * attribute/characteristic values that would have been seen for a sample of previous applications. For full details of the many hundreds
 * of characteristics that can be analysed via the Equifax Risk Navigator retro, please contact your Equifax Account Manager.
 *
 * @see https://developer.equifax.co.uk/sites/uk/files/xml_ref_guide/ref/Data%20Groups/Attribute%20Block.pdf
 */
class AttributeRequest extends CodedDataRequest
{
    /**
     * The index
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 999
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 0
     *
     * @var int|null
     */
    protected ?int $index = null;
    /**
     * The attributeProfile
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $attributeProfile = null;

    /**
     * Constructor method for AttributeRequest
     *
     * @param int    $index
     * @param string $attributeProfile
     * @uses AttributeRequest::setIndex()
     * @uses AttributeRequest::setAttributeProfile()
     */
    public function __construct(?int $index = null, ?string $attributeProfile = null)
    {
        $this
            ->setIndex($index)
            ->setAttributeProfile($attributeProfile);
    }

    /**
     * Get index value
     *
     * @return int|null
     */
    public function getIndex(): ?int
    {
        return $this->index;
    }

    /**
     * Set index value
     *
     * @param int $index
     * @return AttributeRequest
     */
    public function setIndex(?int $index = null): self
    {
        // validation for constraint: int
        if (!is_null($index) && !(is_int($index) || ctype_digit($index))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($index, true),
                gettype($index)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(999)
        if (!is_null($index) && $index > 999) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 999',
                var_export($index, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($index) && $index < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($index, true)
            ), __LINE__);
        }
        $this->index = $index;

        return $this;
    }

    /**
     * Get attributeProfile value
     *
     * @return string|null
     */
    public function getAttributeProfile(): ?string
    {
        return $this->attributeProfile;
    }

    /**
     * Set attributeProfile value
     *
     * @param string $attributeProfile
     * @return AttributeRequest
     */
    public function setAttributeProfile(?string $attributeProfile = null): self
    {
        // validation for constraint: string
        if (!is_null($attributeProfile) && !is_string($attributeProfile)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($attributeProfile, true),
                gettype($attributeProfile)
            ), __LINE__);
        }
        $this->attributeProfile = $attributeProfile;

        return $this;
    }
}
