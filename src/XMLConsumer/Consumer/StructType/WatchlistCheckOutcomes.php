<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for WatchlistCheckOutcomes StructType
 *
 * @subpackage Structs
 */
class WatchlistCheckOutcomes extends AbstractStructBase
{
    /**
     * The watchlistEntry
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 1
     *
     * @var WatchlistEntry[]
     */
    protected array $watchlistEntry;

    /**
     * Constructor method for WatchlistCheckOutcomes
     *
     * @param WatchlistEntry[] $watchlistEntry
     * @uses WatchlistCheckOutcomes::setWatchlistEntry()
     */
    public function __construct(array $watchlistEntry)
    {
        $this
            ->setWatchlistEntry($watchlistEntry);
    }

    /**
     * This method is responsible for validating the values passed to the setWatchlistEntry method
     * This method is willingly generated in order to preserve the one-line inline validation within the setWatchlistEntry method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateWatchlistEntryForArrayConstraintsFromSetWatchlistEntry(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $watchlistCheckOutcomesWatchlistEntryItem) {
            // validation for constraint: itemType
            if (!$watchlistCheckOutcomesWatchlistEntryItem instanceof WatchlistEntry) {
                $invalidValues[] = is_object($watchlistCheckOutcomesWatchlistEntryItem) ? get_class($watchlistCheckOutcomesWatchlistEntryItem) : sprintf(
                    '%s(%s)',
                    gettype($watchlistCheckOutcomesWatchlistEntryItem),
                    var_export($watchlistCheckOutcomesWatchlistEntryItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The watchlistEntry property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\WatchlistEntry, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get watchlistEntry value
     *
     * @return WatchlistEntry[]
     */
    public function getWatchlistEntry(): array
    {
        return $this->watchlistEntry;
    }

    /**
     * Set watchlistEntry value
     *
     * @param WatchlistEntry[] $watchlistEntry
     * @return WatchlistCheckOutcomes
     * @throws InvalidArgumentException
     */
    public function setWatchlistEntry(array $watchlistEntry): self
    {
        // validation for constraint: array
        if ('' !== ($watchlistEntryArrayErrorMessage = self::validateWatchlistEntryForArrayConstraintsFromSetWatchlistEntry($watchlistEntry))) {
            throw new InvalidArgumentException($watchlistEntryArrayErrorMessage, __LINE__);
        }
        $this->watchlistEntry = $watchlistEntry;

        return $this;
    }

    /**
     * Add item to watchlistEntry value
     *
     * @param WatchlistEntry $item
     * @return WatchlistCheckOutcomes
     * @throws InvalidArgumentException
     */
    public function addToWatchlistEntry(WatchlistEntry $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof WatchlistEntry) {
            throw new InvalidArgumentException(sprintf(
                'The watchlistEntry property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\WatchlistEntry, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        $this->watchlistEntry[] = $item;

        return $this;
    }
}
