<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for BankCheckRequest StructType
 *
 * @subpackage Structs
 */
class BankCheckRequest extends CodedDataRequest
{
}
