<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for LinkNamesNotificationContainer StructType
 *
 * @subpackage Structs
 */
class LinkNamesNotificationContainer extends DataItemContainer
{
    /**
     * The linkNamesNotification
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var LinkNamesNotification|null
     */
    protected ?LinkNamesNotification $linkNamesNotification = null;

    /**
     * Constructor method for LinkNamesNotificationContainer
     *
     * @param LinkNamesNotification $linkNamesNotification
     * @uses LinkNamesNotificationContainer::setLinkNamesNotification()
     */
    public function __construct(?LinkNamesNotification $linkNamesNotification = null)
    {
        $this
            ->setLinkNamesNotification($linkNamesNotification);
    }

    /**
     * Get linkNamesNotification value
     *
     * @return LinkNamesNotification|null
     */
    public function getLinkNamesNotification(): ?LinkNamesNotification
    {
        return $this->linkNamesNotification;
    }

    /**
     * Set linkNamesNotification value
     *
     * @param LinkNamesNotification $linkNamesNotification
     * @return LinkNamesNotificationContainer
     */
    public function setLinkNamesNotification(?LinkNamesNotification $linkNamesNotification = null): self
    {
        $this->linkNamesNotification = $linkNamesNotification;

        return $this;
    }
}
