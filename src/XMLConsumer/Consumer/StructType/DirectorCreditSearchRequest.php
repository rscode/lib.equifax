<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for DirectorCreditSearchRequest StructType
 * Meta information extracted from the WSDL
 * - documentation: Director credit search
 *
 * @subpackage Structs
 */
class DirectorCreditSearchRequest extends CommonRequest
{
}
