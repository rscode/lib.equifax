<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\RentalType;

/**
 * This class stands for RentalAgreement StructType
 *
 * @subpackage Structs
 */
class RentalAgreement extends FinancialAgreement
{
    /**
     * The rentalType
     * Meta information extracted from the WSDL
     * - use: optional
     *
     * @var string|null
     */
    protected ?string $rentalType = null;

    /**
     * Constructor method for RentalAgreement
     *
     * @param string $rentalType
     * @uses RentalAgreement::setRentalType()
     */
    public function __construct(?string $rentalType = null)
    {
        $this
            ->setRentalType($rentalType);
    }

    /**
     * Get rentalType value
     *
     * @return string|null
     */
    public function getRentalType(): ?string
    {
        return $this->rentalType;
    }

    /**
     * Set rentalType value
     *
     * @param string $rentalType
     * @return RentalAgreement
     * @throws InvalidArgumentException
     * @uses RentalType::getValidValues
     * @uses RentalType::valueIsValid
     */
    public function setRentalType(?string $rentalType = null): self
    {
        // validation for constraint: enumeration
        if (!RentalType::valueIsValid($rentalType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\RentalType',
                is_array($rentalType) ? implode(', ', $rentalType) : var_export($rentalType, true),
                implode(', ', RentalType::getValidValues())
            ), __LINE__);
        }
        $this->rentalType = $rentalType;

        return $this;
    }
}
