<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;

/**
 * This class stands for AMLHaloDeceasedCheck StructType
 * Meta information extracted from the WSDL
 * - documentation: A lookup of the locally head database of deceased held at Equifax.
 *
 * @subpackage Structs
 */
class AMLHaloDeceasedCheck extends DataItem
{
    /**
     * The haloCertLevelOther
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $haloCertLevelOther;
    /**
     * The haloHighestLevel
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 10
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $haloHighestLevel;
    /**
     * The haloInfoFound
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $haloInfoFound;
    /**
     * The haloLowestLevel
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 10
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $haloLowestLevel;
    /**
     * The haloCertLevel
     * Meta information extracted from the WSDL
     * - maxOccurs: 10
     * - minOccurs: 0
     *
     * @var HaloCertLevel[]
     */
    protected ?array $haloCertLevel = null;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for AMLHaloDeceasedCheck
     *
     * @param bool                    $haloCertLevelOther
     * @param int                     $haloHighestLevel
     * @param bool                    $haloInfoFound
     * @param int                     $haloLowestLevel
     * @param HaloCertLevel[]         $haloCertLevel
     * @param DOMDocument|string|null $any
     * @uses AMLHaloDeceasedCheck::setHaloCertLevelOther()
     * @uses AMLHaloDeceasedCheck::setHaloHighestLevel()
     * @uses AMLHaloDeceasedCheck::setHaloInfoFound()
     * @uses AMLHaloDeceasedCheck::setHaloLowestLevel()
     * @uses AMLHaloDeceasedCheck::setHaloCertLevel()
     * @uses AMLHaloDeceasedCheck::setAny()
     */
    public function __construct(
        bool $haloCertLevelOther,
        int $haloHighestLevel,
        bool $haloInfoFound,
        int $haloLowestLevel,
        ?array $haloCertLevel = null,
        $any = null
    ) {
        $this
            ->setHaloCertLevelOther($haloCertLevelOther)
            ->setHaloHighestLevel($haloHighestLevel)
            ->setHaloInfoFound($haloInfoFound)
            ->setHaloLowestLevel($haloLowestLevel)
            ->setHaloCertLevel($haloCertLevel)
            ->setAny($any);
    }

    /**
     * This method is responsible for validating the values passed to the setHaloCertLevel method
     * This method is willingly generated in order to preserve the one-line inline validation within the setHaloCertLevel method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateHaloCertLevelForArrayConstraintsFromSetHaloCertLevel(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $aMLHaloDeceasedCheckHaloCertLevelItem) {
            // validation for constraint: itemType
            if (!$aMLHaloDeceasedCheckHaloCertLevelItem instanceof HaloCertLevel) {
                $invalidValues[] = is_object($aMLHaloDeceasedCheckHaloCertLevelItem) ? get_class($aMLHaloDeceasedCheckHaloCertLevelItem) : sprintf(
                    '%s(%s)',
                    gettype($aMLHaloDeceasedCheckHaloCertLevelItem),
                    var_export($aMLHaloDeceasedCheckHaloCertLevelItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The haloCertLevel property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\HaloCertLevel, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get haloCertLevelOther value
     *
     * @return bool
     */
    public function getHaloCertLevelOther(): bool
    {
        return $this->haloCertLevelOther;
    }

    /**
     * Set haloCertLevelOther value
     *
     * @param bool $haloCertLevelOther
     * @return AMLHaloDeceasedCheck
     */
    public function setHaloCertLevelOther(bool $haloCertLevelOther): self
    {
        // validation for constraint: boolean
        if (!is_null($haloCertLevelOther) && !is_bool($haloCertLevelOther)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($haloCertLevelOther, true),
                gettype($haloCertLevelOther)
            ), __LINE__);
        }
        $this->haloCertLevelOther = $haloCertLevelOther;

        return $this;
    }

    /**
     * Get haloHighestLevel value
     *
     * @return int
     */
    public function getHaloHighestLevel(): int
    {
        return $this->haloHighestLevel;
    }

    /**
     * Set haloHighestLevel value
     *
     * @param int $haloHighestLevel
     * @return AMLHaloDeceasedCheck
     */
    public function setHaloHighestLevel(int $haloHighestLevel): self
    {
        // validation for constraint: int
        if (!is_null($haloHighestLevel) && !(is_int($haloHighestLevel) || ctype_digit($haloHighestLevel))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($haloHighestLevel, true),
                gettype($haloHighestLevel)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(10)
        if (!is_null($haloHighestLevel) && $haloHighestLevel > 10) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 10',
                var_export($haloHighestLevel, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($haloHighestLevel) && $haloHighestLevel < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($haloHighestLevel, true)
            ), __LINE__);
        }
        $this->haloHighestLevel = $haloHighestLevel;

        return $this;
    }

    /**
     * Get haloInfoFound value
     *
     * @return bool
     */
    public function getHaloInfoFound(): bool
    {
        return $this->haloInfoFound;
    }

    /**
     * Set haloInfoFound value
     *
     * @param bool $haloInfoFound
     * @return AMLHaloDeceasedCheck
     */
    public function setHaloInfoFound(bool $haloInfoFound): self
    {
        // validation for constraint: boolean
        if (!is_null($haloInfoFound) && !is_bool($haloInfoFound)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($haloInfoFound, true),
                gettype($haloInfoFound)
            ), __LINE__);
        }
        $this->haloInfoFound = $haloInfoFound;

        return $this;
    }

    /**
     * Get haloLowestLevel value
     *
     * @return int
     */
    public function getHaloLowestLevel(): int
    {
        return $this->haloLowestLevel;
    }

    /**
     * Set haloLowestLevel value
     *
     * @param int $haloLowestLevel
     * @return AMLHaloDeceasedCheck
     */
    public function setHaloLowestLevel(int $haloLowestLevel): self
    {
        // validation for constraint: int
        if (!is_null($haloLowestLevel) && !(is_int($haloLowestLevel) || ctype_digit($haloLowestLevel))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($haloLowestLevel, true),
                gettype($haloLowestLevel)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(10)
        if (!is_null($haloLowestLevel) && $haloLowestLevel > 10) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 10',
                var_export($haloLowestLevel, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($haloLowestLevel) && $haloLowestLevel < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($haloLowestLevel, true)
            ), __LINE__);
        }
        $this->haloLowestLevel = $haloLowestLevel;

        return $this;
    }

    /**
     * Get haloCertLevel value
     *
     * @return HaloCertLevel[]
     */
    public function getHaloCertLevel(): ?array
    {
        return $this->haloCertLevel;
    }

    /**
     * Set haloCertLevel value
     *
     * @param HaloCertLevel[] $haloCertLevel
     * @return AMLHaloDeceasedCheck
     * @throws InvalidArgumentException
     */
    public function setHaloCertLevel(?array $haloCertLevel = null): self
    {
        // validation for constraint: array
        if ('' !== ($haloCertLevelArrayErrorMessage = self::validateHaloCertLevelForArrayConstraintsFromSetHaloCertLevel($haloCertLevel))) {
            throw new InvalidArgumentException($haloCertLevelArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(10)
        if (is_array($haloCertLevel) && count($haloCertLevel) > 10) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 10',
                count($haloCertLevel)
            ), __LINE__);
        }
        $this->haloCertLevel = $haloCertLevel;

        return $this;
    }

    /**
     * Add item to haloCertLevel value
     *
     * @param HaloCertLevel $item
     * @return AMLHaloDeceasedCheck
     * @throws InvalidArgumentException
     */
    public function addToHaloCertLevel(HaloCertLevel $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof HaloCertLevel) {
            throw new InvalidArgumentException(sprintf(
                'The haloCertLevel property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\HaloCertLevel, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(10)
        if (is_array($this->haloCertLevel) && count($this->haloCertLevel) >= 10) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 10',
                count($this->haloCertLevel)
            ), __LINE__);
        }
        $this->haloCertLevel[] = $item;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return AMLHaloDeceasedCheck
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
