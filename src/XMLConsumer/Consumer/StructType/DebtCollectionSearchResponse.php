<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for DebtCollectionSearchResponse StructType
 * Meta information extracted from the WSDL
 * - documentation: Debt collection activity.
 *
 * @subpackage Structs
 */
class DebtCollectionSearchResponse extends CommonResponse
{
}
