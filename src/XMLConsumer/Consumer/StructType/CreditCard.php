<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for CreditCard StructType
 *
 * @subpackage Structs
 */
class CreditCard extends FinancialAgreement
{
}
