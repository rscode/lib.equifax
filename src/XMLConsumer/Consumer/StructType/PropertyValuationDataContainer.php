<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for PropertyValuationDataContainer StructType
 *
 * @subpackage Structs
 */
class PropertyValuationDataContainer extends DataItemContainer
{
    /**
     * The landRegistryData
     *
     * @var LandRegistryData|null
     */
    protected ?LandRegistryData $landRegistryData = null;
    /**
     * The registersOfScotlandData
     *
     * @var RegistersOfScotlandData|null
     */
    protected ?RegistersOfScotlandData $registersOfScotlandData = null;

    /**
     * Constructor method for PropertyValuationDataContainer
     *
     * @param LandRegistryData        $landRegistryData
     * @param RegistersOfScotlandData $registersOfScotlandData
     * @uses PropertyValuationDataContainer::setLandRegistryData()
     * @uses PropertyValuationDataContainer::setRegistersOfScotlandData()
     */
    public function __construct(
        ?LandRegistryData $landRegistryData = null,
        ?RegistersOfScotlandData $registersOfScotlandData = null
    ) {
        $this
            ->setLandRegistryData($landRegistryData)
            ->setRegistersOfScotlandData($registersOfScotlandData);
    }

    /**
     * Get landRegistryData value
     *
     * @return LandRegistryData|null
     */
    public function getLandRegistryData(): ?LandRegistryData
    {
        return $this->landRegistryData;
    }

    /**
     * Set landRegistryData value
     *
     * @param LandRegistryData $landRegistryData
     * @return PropertyValuationDataContainer
     */
    public function setLandRegistryData(?LandRegistryData $landRegistryData = null): self
    {
        $this->landRegistryData = $landRegistryData;

        return $this;
    }

    /**
     * Get registersOfScotlandData value
     *
     * @return RegistersOfScotlandData|null
     */
    public function getRegistersOfScotlandData(): ?RegistersOfScotlandData
    {
        return $this->registersOfScotlandData;
    }

    /**
     * Set registersOfScotlandData value
     *
     * @param RegistersOfScotlandData $registersOfScotlandData
     * @return PropertyValuationDataContainer
     */
    public function setRegistersOfScotlandData(?RegistersOfScotlandData $registersOfScotlandData = null): self
    {
        $this->registersOfScotlandData = $registersOfScotlandData;

        return $this;
    }
}
