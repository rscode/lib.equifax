<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\SalaryPaymentFrequency;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for EmploymentInfo StructType
 *
 * @subpackage Structs
 */
class EmploymentInfo extends AbstractStructBase
{
    /**
     * The annualSalary
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var int|null
     */
    protected ?int $annualSalary = null;
    /**
     * The duration
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $duration = null;
    /**
     * The employer
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var Employer|null
     */
    protected ?Employer $employer = null;
    /**
     * The occupation
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 50
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $occupation = null;
    /**
     * The paymentFrequency
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $paymentFrequency = null;

    /**
     * Constructor method for EmploymentInfo
     *
     * @param int      $annualSalary
     * @param string   $duration
     * @param Employer $employer
     * @param string   $occupation
     * @param string   $paymentFrequency
     * @uses EmploymentInfo::setAnnualSalary()
     * @uses EmploymentInfo::setDuration()
     * @uses EmploymentInfo::setEmployer()
     * @uses EmploymentInfo::setOccupation()
     * @uses EmploymentInfo::setPaymentFrequency()
     */
    public function __construct(
        ?int $annualSalary = null,
        ?string $duration = null,
        ?Employer $employer = null,
        ?string $occupation = null,
        ?string $paymentFrequency = null
    ) {
        $this
            ->setAnnualSalary($annualSalary)
            ->setDuration($duration)
            ->setEmployer($employer)
            ->setOccupation($occupation)
            ->setPaymentFrequency($paymentFrequency);
    }

    /**
     * Get annualSalary value
     *
     * @return int|null
     */
    public function getAnnualSalary(): ?int
    {
        return $this->annualSalary;
    }

    /**
     * Set annualSalary value
     *
     * @param int $annualSalary
     * @return EmploymentInfo
     */
    public function setAnnualSalary(?int $annualSalary = null): self
    {
        // validation for constraint: int
        if (!is_null($annualSalary) && !(is_int($annualSalary) || ctype_digit($annualSalary))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($annualSalary, true),
                gettype($annualSalary)
            ), __LINE__);
        }
        $this->annualSalary = $annualSalary;

        return $this;
    }

    /**
     * Get duration value
     *
     * @return string|null
     */
    public function getDuration(): ?string
    {
        return $this->duration;
    }

    /**
     * Set duration value
     *
     * @param string $duration
     * @return EmploymentInfo
     */
    public function setDuration(?string $duration = null): self
    {
        // validation for constraint: string
        if (!is_null($duration) && !is_string($duration)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($duration, true),
                gettype($duration)
            ), __LINE__);
        }
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get employer value
     *
     * @return Employer|null
     */
    public function getEmployer(): ?Employer
    {
        return $this->employer;
    }

    /**
     * Set employer value
     *
     * @param Employer $employer
     * @return EmploymentInfo
     */
    public function setEmployer(?Employer $employer = null): self
    {
        $this->employer = $employer;

        return $this;
    }

    /**
     * Get occupation value
     *
     * @return string|null
     */
    public function getOccupation(): ?string
    {
        return $this->occupation;
    }

    /**
     * Set occupation value
     *
     * @param string $occupation
     * @return EmploymentInfo
     */
    public function setOccupation(?string $occupation = null): self
    {
        // validation for constraint: string
        if (!is_null($occupation) && !is_string($occupation)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($occupation, true),
                gettype($occupation)
            ), __LINE__);
        }
        // validation for constraint: maxLength(50)
        if (!is_null($occupation) && mb_strlen((string)$occupation) > 50) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 50',
                mb_strlen((string)$occupation)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($occupation) && mb_strlen((string)$occupation) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$occupation)
            ), __LINE__);
        }
        $this->occupation = $occupation;

        return $this;
    }

    /**
     * Get paymentFrequency value
     *
     * @return string|null
     */
    public function getPaymentFrequency(): ?string
    {
        return $this->paymentFrequency;
    }

    /**
     * Set paymentFrequency value
     *
     * @param string $paymentFrequency
     * @return EmploymentInfo
     * @throws InvalidArgumentException
     * @uses SalaryPaymentFrequency::getValidValues
     * @uses SalaryPaymentFrequency::valueIsValid
     */
    public function setPaymentFrequency(?string $paymentFrequency = null): self
    {
        // validation for constraint: enumeration
        if (!SalaryPaymentFrequency::valueIsValid($paymentFrequency)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\SalaryPaymentFrequency',
                is_array($paymentFrequency) ? implode(', ', $paymentFrequency) : var_export($paymentFrequency, true),
                implode(', ', SalaryPaymentFrequency::getValidValues())
            ), __LINE__);
        }
        $this->paymentFrequency = $paymentFrequency;

        return $this;
    }
}
