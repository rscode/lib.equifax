<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for SIRANChecksRequest StructType
 *
 * @subpackage Structs
 */
class SIRANChecksRequest extends CodedDataRequest
{
}
