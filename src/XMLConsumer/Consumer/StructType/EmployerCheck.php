<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\CompanyNameCheckStatus;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\PhoneNumberCheckStatus;

/**
 * This class stands for EmployerCheck StructType
 * Meta information extracted from the WSDL
 * - documentation: The Employer Check matches the supplied employer telephone number and company name attributes to Equifax-held data and indicates the level of match found.
 *
 * @subpackage Structs
 */
class EmployerCheck extends DataItem
{
    /**
     * The companyNameCheck
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $companyNameCheck;
    /**
     * The idNumber
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 40
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $idNumber;
    /**
     * The phoneNumberCheck
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $phoneNumberCheck;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for EmployerCheck
     *
     * @param string                  $companyNameCheck
     * @param string                  $idNumber
     * @param string                  $phoneNumberCheck
     * @param DOMDocument|string|null $any
     * @uses EmployerCheck::setCompanyNameCheck()
     * @uses EmployerCheck::setIdNumber()
     * @uses EmployerCheck::setPhoneNumberCheck()
     * @uses EmployerCheck::setAny()
     */
    public function __construct(string $companyNameCheck, string $idNumber, string $phoneNumberCheck, $any = null)
    {
        $this
            ->setCompanyNameCheck($companyNameCheck)
            ->setIdNumber($idNumber)
            ->setPhoneNumberCheck($phoneNumberCheck)
            ->setAny($any);
    }

    /**
     * Get companyNameCheck value
     *
     * @return string
     */
    public function getCompanyNameCheck(): string
    {
        return $this->companyNameCheck;
    }

    /**
     * Set companyNameCheck value
     *
     * @param string $companyNameCheck
     * @return EmployerCheck
     * @throws InvalidArgumentException
     * @uses CompanyNameCheckStatus::getValidValues
     * @uses CompanyNameCheckStatus::valueIsValid
     */
    public function setCompanyNameCheck(string $companyNameCheck): self
    {
        // validation for constraint: enumeration
        if (!CompanyNameCheckStatus::valueIsValid($companyNameCheck)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\CompanyNameCheckStatus',
                is_array($companyNameCheck) ? implode(', ', $companyNameCheck) : var_export($companyNameCheck, true),
                implode(', ', CompanyNameCheckStatus::getValidValues())
            ), __LINE__);
        }
        $this->companyNameCheck = $companyNameCheck;

        return $this;
    }

    /**
     * Get idNumber value
     *
     * @return string
     */
    public function getIdNumber(): string
    {
        return $this->idNumber;
    }

    /**
     * Set idNumber value
     *
     * @param string $idNumber
     * @return EmployerCheck
     */
    public function setIdNumber(string $idNumber): self
    {
        // validation for constraint: string
        if (!is_null($idNumber) && !is_string($idNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($idNumber, true),
                gettype($idNumber)
            ), __LINE__);
        }
        // validation for constraint: maxLength(40)
        if (!is_null($idNumber) && mb_strlen((string)$idNumber) > 40) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 40',
                mb_strlen((string)$idNumber)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($idNumber) && mb_strlen((string)$idNumber) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$idNumber)
            ), __LINE__);
        }
        $this->idNumber = $idNumber;

        return $this;
    }

    /**
     * Get phoneNumberCheck value
     *
     * @return string
     */
    public function getPhoneNumberCheck(): string
    {
        return $this->phoneNumberCheck;
    }

    /**
     * Set phoneNumberCheck value
     *
     * @param string $phoneNumberCheck
     * @return EmployerCheck
     * @throws InvalidArgumentException
     * @uses PhoneNumberCheckStatus::getValidValues
     * @uses PhoneNumberCheckStatus::valueIsValid
     */
    public function setPhoneNumberCheck(string $phoneNumberCheck): self
    {
        // validation for constraint: enumeration
        if (!PhoneNumberCheckStatus::valueIsValid($phoneNumberCheck)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\PhoneNumberCheckStatus',
                is_array($phoneNumberCheck) ? implode(', ', $phoneNumberCheck) : var_export($phoneNumberCheck, true),
                implode(', ', PhoneNumberCheckStatus::getValidValues())
            ), __LINE__);
        }
        $this->phoneNumberCheck = $phoneNumberCheck;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return EmployerCheck
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
