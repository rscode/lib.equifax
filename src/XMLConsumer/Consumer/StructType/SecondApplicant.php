<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SecondApplicant StructType
 *
 * @subpackage Structs
 */
class SecondApplicant extends AbstractStructBase
{
    /**
     * The watchlistCheckOutcomes
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var WatchlistCheckOutcomes
     */
    protected WatchlistCheckOutcomes $watchlistCheckOutcomes;

    /**
     * Constructor method for SecondApplicant
     *
     * @param WatchlistCheckOutcomes $watchlistCheckOutcomes
     * @uses SecondApplicant::setWatchlistCheckOutcomes()
     */
    public function __construct(WatchlistCheckOutcomes $watchlistCheckOutcomes)
    {
        $this
            ->setWatchlistCheckOutcomes($watchlistCheckOutcomes);
    }

    /**
     * Get watchlistCheckOutcomes value
     *
     * @return WatchlistCheckOutcomes
     */
    public function getWatchlistCheckOutcomes(): WatchlistCheckOutcomes
    {
        return $this->watchlistCheckOutcomes;
    }

    /**
     * Set watchlistCheckOutcomes value
     *
     * @param WatchlistCheckOutcomes $watchlistCheckOutcomes
     * @return SecondApplicant
     */
    public function setWatchlistCheckOutcomes(WatchlistCheckOutcomes $watchlistCheckOutcomes): self
    {
        $this->watchlistCheckOutcomes = $watchlistCheckOutcomes;

        return $this;
    }
}
