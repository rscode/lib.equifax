<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for ExtendedHomeTelephoneCheckContainer StructType
 *
 * @subpackage Structs
 */
class ExtendedHomeTelephoneCheckContainer extends DataItemContainer
{
    /**
     * The extendedHomeTelephoneCheck
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var ExtendedHomeTelephoneCheck|null
     */
    protected ?ExtendedHomeTelephoneCheck $extendedHomeTelephoneCheck = null;

    /**
     * Constructor method for ExtendedHomeTelephoneCheckContainer
     *
     * @param ExtendedHomeTelephoneCheck $extendedHomeTelephoneCheck
     * @uses ExtendedHomeTelephoneCheckContainer::setExtendedHomeTelephoneCheck()
     */
    public function __construct(?ExtendedHomeTelephoneCheck $extendedHomeTelephoneCheck = null)
    {
        $this
            ->setExtendedHomeTelephoneCheck($extendedHomeTelephoneCheck);
    }

    /**
     * Get extendedHomeTelephoneCheck value
     *
     * @return ExtendedHomeTelephoneCheck|null
     */
    public function getExtendedHomeTelephoneCheck(): ?ExtendedHomeTelephoneCheck
    {
        return $this->extendedHomeTelephoneCheck;
    }

    /**
     * Set extendedHomeTelephoneCheck value
     *
     * @param ExtendedHomeTelephoneCheck $extendedHomeTelephoneCheck
     * @return ExtendedHomeTelephoneCheckContainer
     */
    public function setExtendedHomeTelephoneCheck(?ExtendedHomeTelephoneCheck $extendedHomeTelephoneCheck = null): self
    {
        $this->extendedHomeTelephoneCheck = $extendedHomeTelephoneCheck;

        return $this;
    }
}
