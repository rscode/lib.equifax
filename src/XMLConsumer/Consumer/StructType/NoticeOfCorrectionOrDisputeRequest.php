<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for NoticeOfCorrectionOrDisputeRequest StructType
 *
 * @subpackage Structs
 */
class NoticeOfCorrectionOrDisputeRequest extends RawDataAvailableAtLinkedAddressRequest
{
}
