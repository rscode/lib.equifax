<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for NamedData StructType
 *
 * @subpackage Structs
 */
class NamedData extends DataItem
{
    /**
     * The dob
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $dob = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var Name|null
     */
    protected ?Name $name = null;

    /**
     * Constructor method for NamedData
     *
     * @param string $dob
     * @param Name   $name
     * @uses NamedData::setDob()
     * @uses NamedData::setName()
     */
    public function __construct(?string $dob = null, ?Name $name = null)
    {
        $this
            ->setDob($dob)
            ->setName($name);
    }

    /**
     * Get dob value
     *
     * @return string|null
     */
    public function getDob(): ?string
    {
        return $this->dob;
    }

    /**
     * Set dob value
     *
     * @param string $dob
     * @return NamedData
     */
    public function setDob(?string $dob = null): self
    {
        // validation for constraint: string
        if (!is_null($dob) && !is_string($dob)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($dob, true),
                gettype($dob)
            ), __LINE__);
        }
        $this->dob = $dob;

        return $this;
    }

    /**
     * Get name value
     *
     * @return Name|null
     */
    public function getName(): ?Name
    {
        return $this->name;
    }

    /**
     * Set name value
     *
     * @param Name $name
     * @return NamedData
     */
    public function setName(?Name $name = null): self
    {
        $this->name = $name;

        return $this;
    }
}
