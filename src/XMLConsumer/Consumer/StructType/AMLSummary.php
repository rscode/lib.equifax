<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\CIFASPlusMigrationStatus;

/**
 * This class stands for AMLSummary StructType
 * Meta information extracted from the WSDL
 * - documentation: Summary of anti-money laundering checks relevant to fraud prevention.
 *
 * @subpackage Structs
 */
class AMLSummary extends DataItem
{
    /**
     * The cifasPlusMigrationStatus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $cifasPlusMigrationStatus;
    /**
     * The countOfCourtAndInsolvencyInformation
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 99
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $countOfCourtAndInsolvencyInformation;
    /**
     * The countOfLendersBehindOpenInsightAccounts
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 99
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $countOfLendersBehindOpenInsightAccounts;
    /**
     * The currentlyListedByBT
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $currentlyListedByBT;
    /**
     * The currentlyOnElectoralRoll
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $currentlyOnElectoralRoll;
    /**
     * The dobMatchOnElectoralRollAttainerDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $dobMatchOnElectoralRollAttainerDate;
    /**
     * The dobMatchOnInsight
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $dobMatchOnInsight;
    /**
     * The homePhoneMatch
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $homePhoneMatch;
    /**
     * The linkedForwardingAddressFound
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $linkedForwardingAddressFound;
    /**
     * The onHMTreasurySanctions
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $onHMTreasurySanctions;
    /**
     * The onCIFAS
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $onCIFAS;
    /**
     * The onCIFASPlus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $onCIFASPlus;
    /**
     * The onHalo
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $onHalo;
    /**
     * The onOFAC
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $onOFAC;
    /**
     * The onONS
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $onONS;
    /**
     * The onSeniorPoliticalFiguresSanctions
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $onSeniorPoliticalFiguresSanctions;
    /**
     * The overallOTFAIndicator
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $overallOTFAIndicator;
    /**
     * The totalAlerts
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 999
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $totalAlerts;
    /**
     * The totalProofsOfIdentity
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 999
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $totalProofsOfIdentity;
    /**
     * The totalProofsOfResidency
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 999
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $totalProofsOfResidency;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for AMLSummary
     *
     * @param string $cifasPlusMigrationStatus
     * @param int $countOfCourtAndInsolvencyInformation
     * @param int $countOfLendersBehindOpenInsightAccounts
     * @param bool $currentlyListedByBT
     * @param bool $currentlyOnElectoralRoll
     * @param bool $dobMatchOnElectoralRollAttainerDate
     * @param bool $dobMatchOnInsight
     * @param bool $homePhoneMatch
     * @param bool $linkedForwardingAddressFound
     * @param string $onHMTreasurySanctions
     * @param bool $onCIFAS
     * @param bool $onCIFASPlus
     * @param bool $onHalo
     * @param string $onOFAC
     * @param bool $onONS
     * @param string $onSeniorPoliticalFiguresSanctions
     * @param bool $overallOTFAIndicator
     * @param int $totalAlerts
     * @param int $totalProofsOfIdentity
     * @param int $totalProofsOfResidency
     * @param DOMDocument|string|null $any
     * @uses AMLSummary::setCifasPlusMigrationStatus()
     * @uses AMLSummary::setCountOfCourtAndInsolvencyInformation()
     * @uses AMLSummary::setCountOfLendersBehindOpenInsightAccounts()
     * @uses AMLSummary::setCurrentlyListedByBT()
     * @uses AMLSummary::setCurrentlyOnElectoralRoll()
     * @uses AMLSummary::setDobMatchOnElectoralRollAttainerDate()
     * @uses AMLSummary::setDobMatchOnInsight()
     * @uses AMLSummary::setHomePhoneMatch()
     * @uses AMLSummary::setLinkedForwardingAddressFound()
     * @uses AMLSummary::setOnHMTreasurySanctions()
     * @uses AMLSummary::setOnCIFAS()
     * @uses AMLSummary::setOnCIFASPlus()
     * @uses AMLSummary::setOnHalo()
     * @uses AMLSummary::setOnOFAC()
     * @uses AMLSummary::setOnONS()
     * @uses AMLSummary::setOnSeniorPoliticalFiguresSanctions()
     * @uses AMLSummary::setOverallOTFAIndicator()
     * @uses AMLSummary::setTotalAlerts()
     * @uses AMLSummary::setTotalProofsOfIdentity()
     * @uses AMLSummary::setTotalProofsOfResidency()
     * @uses AMLSummary::setAny()
     */
    public function __construct(
        string $cifasPlusMigrationStatus,
        int $countOfCourtAndInsolvencyInformation,
        int $countOfLendersBehindOpenInsightAccounts,
        bool $currentlyListedByBT,
        bool $currentlyOnElectoralRoll,
        bool $dobMatchOnElectoralRollAttainerDate,
        bool $dobMatchOnInsight,
        bool $homePhoneMatch,
        bool $linkedForwardingAddressFound,
        string $onHMTreasurySanctions,
        bool $onCIFAS,
        bool $onCIFASPlus,
        bool $onHalo,
        string $onOFAC,
        bool $onONS,
        string $onSeniorPoliticalFiguresSanctions,
        bool $overallOTFAIndicator,
        int $totalAlerts,
        int $totalProofsOfIdentity,
        int $totalProofsOfResidency,
        $any = null
    ) {
        $this
            ->setCifasPlusMigrationStatus($cifasPlusMigrationStatus)
            ->setCountOfCourtAndInsolvencyInformation($countOfCourtAndInsolvencyInformation)
            ->setCountOfLendersBehindOpenInsightAccounts($countOfLendersBehindOpenInsightAccounts)
            ->setCurrentlyListedByBT($currentlyListedByBT)
            ->setCurrentlyOnElectoralRoll($currentlyOnElectoralRoll)
            ->setDobMatchOnElectoralRollAttainerDate($dobMatchOnElectoralRollAttainerDate)
            ->setDobMatchOnInsight($dobMatchOnInsight)
            ->setHomePhoneMatch($homePhoneMatch)
            ->setLinkedForwardingAddressFound($linkedForwardingAddressFound)
            ->setOnHMTreasurySanctions($onHMTreasurySanctions)
            ->setOnCIFAS($onCIFAS)
            ->setOnCIFASPlus($onCIFASPlus)
            ->setOnHalo($onHalo)
            ->setOnOFAC($onOFAC)
            ->setOnONS($onONS)
            ->setOnSeniorPoliticalFiguresSanctions($onSeniorPoliticalFiguresSanctions)
            ->setOverallOTFAIndicator($overallOTFAIndicator)
            ->setTotalAlerts($totalAlerts)
            ->setTotalProofsOfIdentity($totalProofsOfIdentity)
            ->setTotalProofsOfResidency($totalProofsOfResidency)
            ->setAny($any);
    }

    /**
     * Get cifasPlusMigrationStatus value
     *
     * @return string
     */
    public function getCifasPlusMigrationStatus(): string
    {
        return $this->cifasPlusMigrationStatus;
    }

    /**
     * Set cifasPlusMigrationStatus value
     *
     * @param string $cifasPlusMigrationStatus
     * @return AMLSummary
     * @throws InvalidArgumentException
     * @uses CIFASPlusMigrationStatus::getValidValues
     * @uses CIFASPlusMigrationStatus::valueIsValid
     */
    public function setCifasPlusMigrationStatus(string $cifasPlusMigrationStatus): self
    {
        // validation for constraint: enumeration
        if (!CIFASPlusMigrationStatus::valueIsValid($cifasPlusMigrationStatus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\CIFASPlusMigrationStatus',
                is_array($cifasPlusMigrationStatus) ? implode(', ', $cifasPlusMigrationStatus) : var_export(
                    $cifasPlusMigrationStatus,
                    true
                ),
                implode(', ', CIFASPlusMigrationStatus::getValidValues())
            ), __LINE__);
        }
        $this->cifasPlusMigrationStatus = $cifasPlusMigrationStatus;

        return $this;
    }

    /**
     * Get countOfCourtAndInsolvencyInformation value
     *
     * @return int
     */
    public function getCountOfCourtAndInsolvencyInformation(): int
    {
        return $this->countOfCourtAndInsolvencyInformation;
    }

    /**
     * Set countOfCourtAndInsolvencyInformation value
     *
     * @param int $countOfCourtAndInsolvencyInformation
     * @return AMLSummary
     */
    public function setCountOfCourtAndInsolvencyInformation(int $countOfCourtAndInsolvencyInformation): self
    {
        // validation for constraint: int
        if (!is_null($countOfCourtAndInsolvencyInformation) && !(is_int($countOfCourtAndInsolvencyInformation) || ctype_digit($countOfCourtAndInsolvencyInformation))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($countOfCourtAndInsolvencyInformation, true),
                gettype($countOfCourtAndInsolvencyInformation)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(99)
        if (!is_null($countOfCourtAndInsolvencyInformation) && $countOfCourtAndInsolvencyInformation > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 99',
                var_export($countOfCourtAndInsolvencyInformation, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($countOfCourtAndInsolvencyInformation) && $countOfCourtAndInsolvencyInformation < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($countOfCourtAndInsolvencyInformation, true)
            ), __LINE__);
        }
        $this->countOfCourtAndInsolvencyInformation = $countOfCourtAndInsolvencyInformation;

        return $this;
    }

    /**
     * Get countOfLendersBehindOpenInsightAccounts value
     *
     * @return int
     */
    public function getCountOfLendersBehindOpenInsightAccounts(): int
    {
        return $this->countOfLendersBehindOpenInsightAccounts;
    }

    /**
     * Set countOfLendersBehindOpenInsightAccounts value
     *
     * @param int $countOfLendersBehindOpenInsightAccounts
     * @return AMLSummary
     */
    public function setCountOfLendersBehindOpenInsightAccounts(int $countOfLendersBehindOpenInsightAccounts): self
    {
        // validation for constraint: int
        if (!is_null($countOfLendersBehindOpenInsightAccounts) && !(is_int($countOfLendersBehindOpenInsightAccounts) || ctype_digit($countOfLendersBehindOpenInsightAccounts))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($countOfLendersBehindOpenInsightAccounts, true),
                gettype($countOfLendersBehindOpenInsightAccounts)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(99)
        if (!is_null($countOfLendersBehindOpenInsightAccounts) && $countOfLendersBehindOpenInsightAccounts > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 99',
                var_export($countOfLendersBehindOpenInsightAccounts, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($countOfLendersBehindOpenInsightAccounts) && $countOfLendersBehindOpenInsightAccounts < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($countOfLendersBehindOpenInsightAccounts, true)
            ), __LINE__);
        }
        $this->countOfLendersBehindOpenInsightAccounts = $countOfLendersBehindOpenInsightAccounts;

        return $this;
    }

    /**
     * Get currentlyListedByBT value
     *
     * @return bool
     */
    public function getCurrentlyListedByBT(): bool
    {
        return $this->currentlyListedByBT;
    }

    /**
     * Set currentlyListedByBT value
     *
     * @param bool $currentlyListedByBT
     * @return AMLSummary
     */
    public function setCurrentlyListedByBT(bool $currentlyListedByBT): self
    {
        // validation for constraint: boolean
        if (!is_null($currentlyListedByBT) && !is_bool($currentlyListedByBT)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($currentlyListedByBT, true),
                gettype($currentlyListedByBT)
            ), __LINE__);
        }
        $this->currentlyListedByBT = $currentlyListedByBT;

        return $this;
    }

    /**
     * Get currentlyOnElectoralRoll value
     *
     * @return bool
     */
    public function getCurrentlyOnElectoralRoll(): bool
    {
        return $this->currentlyOnElectoralRoll;
    }

    /**
     * Set currentlyOnElectoralRoll value
     *
     * @param bool $currentlyOnElectoralRoll
     * @return AMLSummary
     */
    public function setCurrentlyOnElectoralRoll(bool $currentlyOnElectoralRoll): self
    {
        // validation for constraint: boolean
        if (!is_null($currentlyOnElectoralRoll) && !is_bool($currentlyOnElectoralRoll)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($currentlyOnElectoralRoll, true),
                gettype($currentlyOnElectoralRoll)
            ), __LINE__);
        }
        $this->currentlyOnElectoralRoll = $currentlyOnElectoralRoll;

        return $this;
    }

    /**
     * Get dobMatchOnElectoralRollAttainerDate value
     *
     * @return bool
     */
    public function getDobMatchOnElectoralRollAttainerDate(): bool
    {
        return $this->dobMatchOnElectoralRollAttainerDate;
    }

    /**
     * Set dobMatchOnElectoralRollAttainerDate value
     *
     * @param bool $dobMatchOnElectoralRollAttainerDate
     * @return AMLSummary
     */
    public function setDobMatchOnElectoralRollAttainerDate(bool $dobMatchOnElectoralRollAttainerDate): self
    {
        // validation for constraint: boolean
        if (!is_null($dobMatchOnElectoralRollAttainerDate) && !is_bool($dobMatchOnElectoralRollAttainerDate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($dobMatchOnElectoralRollAttainerDate, true),
                gettype($dobMatchOnElectoralRollAttainerDate)
            ), __LINE__);
        }
        $this->dobMatchOnElectoralRollAttainerDate = $dobMatchOnElectoralRollAttainerDate;

        return $this;
    }

    /**
     * Get dobMatchOnInsight value
     *
     * @return bool
     */
    public function getDobMatchOnInsight(): bool
    {
        return $this->dobMatchOnInsight;
    }

    /**
     * Set dobMatchOnInsight value
     *
     * @param bool $dobMatchOnInsight
     * @return AMLSummary
     */
    public function setDobMatchOnInsight(bool $dobMatchOnInsight): self
    {
        // validation for constraint: boolean
        if (!is_null($dobMatchOnInsight) && !is_bool($dobMatchOnInsight)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($dobMatchOnInsight, true),
                gettype($dobMatchOnInsight)
            ), __LINE__);
        }
        $this->dobMatchOnInsight = $dobMatchOnInsight;

        return $this;
    }

    /**
     * Get homePhoneMatch value
     *
     * @return bool
     */
    public function getHomePhoneMatch(): bool
    {
        return $this->homePhoneMatch;
    }

    /**
     * Set homePhoneMatch value
     *
     * @param bool $homePhoneMatch
     * @return AMLSummary
     */
    public function setHomePhoneMatch(bool $homePhoneMatch): self
    {
        // validation for constraint: boolean
        if (!is_null($homePhoneMatch) && !is_bool($homePhoneMatch)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($homePhoneMatch, true),
                gettype($homePhoneMatch)
            ), __LINE__);
        }
        $this->homePhoneMatch = $homePhoneMatch;

        return $this;
    }

    /**
     * Get linkedForwardingAddressFound value
     *
     * @return bool
     */
    public function getLinkedForwardingAddressFound(): bool
    {
        return $this->linkedForwardingAddressFound;
    }

    /**
     * Set linkedForwardingAddressFound value
     *
     * @param bool $linkedForwardingAddressFound
     * @return AMLSummary
     */
    public function setLinkedForwardingAddressFound(bool $linkedForwardingAddressFound): self
    {
        // validation for constraint: boolean
        if (!is_null($linkedForwardingAddressFound) && !is_bool($linkedForwardingAddressFound)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($linkedForwardingAddressFound, true),
                gettype($linkedForwardingAddressFound)
            ), __LINE__);
        }
        $this->linkedForwardingAddressFound = $linkedForwardingAddressFound;

        return $this;
    }

    /**
     * Get onHMTreasurySanctions value
     *
     * @return string
     */
    public function getOnHMTreasurySanctions(): string
    {
        return $this->onHMTreasurySanctions;
    }

    /**
     * Set onHMTreasurySanctions value
     *
     * @param string $onHMTreasurySanctions
     * @return AMLSummary
     */
    public function setOnHMTreasurySanctions(string $onHMTreasurySanctions): self
    {
        // validation for constraint: string
        if (!is_null($onHMTreasurySanctions) && !is_string($onHMTreasurySanctions)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($onHMTreasurySanctions, true),
                gettype($onHMTreasurySanctions)
            ), __LINE__);
        }
        $this->onHMTreasurySanctions = $onHMTreasurySanctions;

        return $this;
    }

    /**
     * Get onCIFAS value
     *
     * @return bool
     */
    public function getOnCIFAS(): bool
    {
        return $this->onCIFAS;
    }

    /**
     * Set onCIFAS value
     *
     * @param bool $onCIFAS
     * @return AMLSummary
     */
    public function setOnCIFAS(bool $onCIFAS): self
    {
        // validation for constraint: boolean
        if (!is_null($onCIFAS) && !is_bool($onCIFAS)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($onCIFAS, true),
                gettype($onCIFAS)
            ), __LINE__);
        }
        $this->onCIFAS = $onCIFAS;

        return $this;
    }

    /**
     * Get onCIFASPlus value
     *
     * @return bool
     */
    public function getOnCIFASPlus(): bool
    {
        return $this->onCIFASPlus;
    }

    /**
     * Set onCIFASPlus value
     *
     * @param bool $onCIFASPlus
     * @return AMLSummary
     */
    public function setOnCIFASPlus(bool $onCIFASPlus): self
    {
        // validation for constraint: boolean
        if (!is_null($onCIFASPlus) && !is_bool($onCIFASPlus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($onCIFASPlus, true),
                gettype($onCIFASPlus)
            ), __LINE__);
        }
        $this->onCIFASPlus = $onCIFASPlus;

        return $this;
    }

    /**
     * Get onHalo value
     *
     * @return bool
     */
    public function getOnHalo(): bool
    {
        return $this->onHalo;
    }

    /**
     * Set onHalo value
     *
     * @param bool $onHalo
     * @return AMLSummary
     */
    public function setOnHalo(bool $onHalo): self
    {
        // validation for constraint: boolean
        if (!is_null($onHalo) && !is_bool($onHalo)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($onHalo, true),
                gettype($onHalo)
            ), __LINE__);
        }
        $this->onHalo = $onHalo;

        return $this;
    }

    /**
     * Get onOFAC value
     *
     * @return string
     */
    public function getOnOFAC(): string
    {
        return $this->onOFAC;
    }

    /**
     * Set onOFAC value
     *
     * @param string $onOFAC
     * @return AMLSummary
     */
    public function setOnOFAC(string $onOFAC): self
    {
        // validation for constraint: string
        if (!is_null($onOFAC) && !is_string($onOFAC)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($onOFAC, true),
                gettype($onOFAC)
            ), __LINE__);
        }
        $this->onOFAC = $onOFAC;

        return $this;
    }

    /**
     * Get onONS value
     *
     * @return bool
     */
    public function getOnONS(): bool
    {
        return $this->onONS;
    }

    /**
     * Set onONS value
     *
     * @param bool $onONS
     * @return AMLSummary
     */
    public function setOnONS(bool $onONS): self
    {
        // validation for constraint: boolean
        if (!is_null($onONS) && !is_bool($onONS)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($onONS, true),
                gettype($onONS)
            ), __LINE__);
        }
        $this->onONS = $onONS;

        return $this;
    }

    /**
     * Get onSeniorPoliticalFiguresSanctions value
     *
     * @return string
     */
    public function getOnSeniorPoliticalFiguresSanctions(): string
    {
        return $this->onSeniorPoliticalFiguresSanctions;
    }

    /**
     * Set onSeniorPoliticalFiguresSanctions value
     *
     * @param string $onSeniorPoliticalFiguresSanctions
     * @return AMLSummary
     */
    public function setOnSeniorPoliticalFiguresSanctions(string $onSeniorPoliticalFiguresSanctions): self
    {
        // validation for constraint: string
        if (!is_null($onSeniorPoliticalFiguresSanctions) && !is_string($onSeniorPoliticalFiguresSanctions)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($onSeniorPoliticalFiguresSanctions, true),
                gettype($onSeniorPoliticalFiguresSanctions)
            ), __LINE__);
        }
        $this->onSeniorPoliticalFiguresSanctions = $onSeniorPoliticalFiguresSanctions;

        return $this;
    }

    /**
     * Get overallOTFAIndicator value
     *
     * @return bool
     */
    public function getOverallOTFAIndicator(): bool
    {
        return $this->overallOTFAIndicator;
    }

    /**
     * Set overallOTFAIndicator value
     *
     * @param bool $overallOTFAIndicator
     * @return AMLSummary
     */
    public function setOverallOTFAIndicator(bool $overallOTFAIndicator): self
    {
        // validation for constraint: boolean
        if (!is_null($overallOTFAIndicator) && !is_bool($overallOTFAIndicator)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($overallOTFAIndicator, true),
                gettype($overallOTFAIndicator)
            ), __LINE__);
        }
        $this->overallOTFAIndicator = $overallOTFAIndicator;

        return $this;
    }

    /**
     * Get totalAlerts value
     *
     * @return int
     */
    public function getTotalAlerts(): int
    {
        return $this->totalAlerts;
    }

    /**
     * Set totalAlerts value
     *
     * @param int $totalAlerts
     * @return AMLSummary
     */
    public function setTotalAlerts(int $totalAlerts): self
    {
        // validation for constraint: int
        if (!is_null($totalAlerts) && !(is_int($totalAlerts) || ctype_digit($totalAlerts))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($totalAlerts, true),
                gettype($totalAlerts)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(999)
        if (!is_null($totalAlerts) && $totalAlerts > 999) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 999',
                var_export($totalAlerts, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($totalAlerts) && $totalAlerts < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($totalAlerts, true)
            ), __LINE__);
        }
        $this->totalAlerts = $totalAlerts;

        return $this;
    }

    /**
     * Get totalProofsOfIdentity value
     *
     * @return int
     */
    public function getTotalProofsOfIdentity(): int
    {
        return $this->totalProofsOfIdentity;
    }

    /**
     * Set totalProofsOfIdentity value
     *
     * @param int $totalProofsOfIdentity
     * @return AMLSummary
     */
    public function setTotalProofsOfIdentity(int $totalProofsOfIdentity): self
    {
        // validation for constraint: int
        if (!is_null($totalProofsOfIdentity) && !(is_int($totalProofsOfIdentity) || ctype_digit($totalProofsOfIdentity))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($totalProofsOfIdentity, true),
                gettype($totalProofsOfIdentity)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(999)
        if (!is_null($totalProofsOfIdentity) && $totalProofsOfIdentity > 999) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 999',
                var_export($totalProofsOfIdentity, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($totalProofsOfIdentity) && $totalProofsOfIdentity < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($totalProofsOfIdentity, true)
            ), __LINE__);
        }
        $this->totalProofsOfIdentity = $totalProofsOfIdentity;

        return $this;
    }

    /**
     * Get totalProofsOfResidency value
     *
     * @return int
     */
    public function getTotalProofsOfResidency(): int
    {
        return $this->totalProofsOfResidency;
    }

    /**
     * Set totalProofsOfResidency value
     *
     * @param int $totalProofsOfResidency
     * @return AMLSummary
     */
    public function setTotalProofsOfResidency(int $totalProofsOfResidency): self
    {
        // validation for constraint: int
        if (!is_null($totalProofsOfResidency) && !(is_int($totalProofsOfResidency) || ctype_digit($totalProofsOfResidency))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($totalProofsOfResidency, true),
                gettype($totalProofsOfResidency)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(999)
        if (!is_null($totalProofsOfResidency) && $totalProofsOfResidency > 999) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 999',
                var_export($totalProofsOfResidency, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($totalProofsOfResidency) && $totalProofsOfResidency < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($totalProofsOfResidency, true)
            ), __LINE__);
        }
        $this->totalProofsOfResidency = $totalProofsOfResidency;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return AMLSummary
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
