<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Introducer StructType
 *
 * @subpackage Structs
 */
class Introducer extends AbstractStructBase
{
    /**
     * The name
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 50
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $name;
    /**
     * The individualName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var Name|null
     */
    protected ?Name $individualName = null;

    /**
     * Constructor method for Introducer
     *
     * @param string $name
     * @param Name   $individualName
     * @uses Introducer::setName()
     * @uses Introducer::setIndividualName()
     */
    public function __construct(string $name, ?Name $individualName = null)
    {
        $this
            ->setName($name)
            ->setIndividualName($individualName);
    }

    /**
     * Get name value
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set name value
     *
     * @param string $name
     * @return Introducer
     */
    public function setName(string $name): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($name, true),
                gettype($name)
            ), __LINE__);
        }
        // validation for constraint: maxLength(50)
        if (!is_null($name) && mb_strlen((string)$name) > 50) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 50',
                mb_strlen((string)$name)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($name) && mb_strlen((string)$name) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$name)
            ), __LINE__);
        }
        $this->name = $name;

        return $this;
    }

    /**
     * Get individualName value
     *
     * @return Name|null
     */
    public function getIndividualName(): ?Name
    {
        return $this->individualName;
    }

    /**
     * Set individualName value
     *
     * @param Name $individualName
     * @return Introducer
     */
    public function setIndividualName(?Name $individualName = null): self
    {
        $this->individualName = $individualName;

        return $this;
    }
}
