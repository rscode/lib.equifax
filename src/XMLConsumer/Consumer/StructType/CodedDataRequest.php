<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for CodedDataRequest StructType
 *
 * @subpackage Structs
 */
abstract class CodedDataRequest extends DataRequest
{
}
