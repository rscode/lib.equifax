<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for ResponsePerson StructType
 *
 * @subpackage Structs
 */
class ResponsePerson extends PersonBase
{
    /**
     * The suppliedAddressData
     * Meta information extracted from the WSDL
     * - maxOccurs: 10
     * - minOccurs: 0
     *
     * @var SuppliedAddressData[]
     */
    protected ?array $suppliedAddressData = null;
    /**
     * The linkedAddressData
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var LinkedAddressData[]
     */
    protected ?array $linkedAddressData = null;

    /**
     * Constructor method for ResponsePerson
     *
     * @param SuppliedAddressData[] $suppliedAddressData
     * @param LinkedAddressData[]   $linkedAddressData
     * @uses ResponsePerson::setSuppliedAddressData()
     * @uses ResponsePerson::setLinkedAddressData()
     */
    public function __construct(?array $suppliedAddressData = null, ?array $linkedAddressData = null)
    {
        $this
            ->setSuppliedAddressData($suppliedAddressData)
            ->setLinkedAddressData($linkedAddressData);
    }

    /**
     * This method is responsible for validating the values passed to the setSuppliedAddressData method
     * This method is willingly generated in order to preserve the one-line inline validation within the setSuppliedAddressData method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateSuppliedAddressDataForArrayConstraintsFromSetSuppliedAddressData(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $responsePersonSuppliedAddressDataItem) {
            // validation for constraint: itemType
            if (!$responsePersonSuppliedAddressDataItem instanceof SuppliedAddressData) {
                $invalidValues[] = is_object($responsePersonSuppliedAddressDataItem) ? get_class($responsePersonSuppliedAddressDataItem) : sprintf(
                    '%s(%s)',
                    gettype($responsePersonSuppliedAddressDataItem),
                    var_export($responsePersonSuppliedAddressDataItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The suppliedAddressData property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\SuppliedAddressData, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * This method is responsible for validating the values passed to the setLinkedAddressData method
     * This method is willingly generated in order to preserve the one-line inline validation within the setLinkedAddressData method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateLinkedAddressDataForArrayConstraintsFromSetLinkedAddressData(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $responsePersonLinkedAddressDataItem) {
            // validation for constraint: itemType
            if (!$responsePersonLinkedAddressDataItem instanceof LinkedAddressData) {
                $invalidValues[] = is_object($responsePersonLinkedAddressDataItem) ? get_class($responsePersonLinkedAddressDataItem) : sprintf(
                    '%s(%s)',
                    gettype($responsePersonLinkedAddressDataItem),
                    var_export($responsePersonLinkedAddressDataItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The linkedAddressData property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\LinkedAddressData, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get suppliedAddressData value
     *
     * @return SuppliedAddressData[]
     */
    public function getSuppliedAddressData(): ?array
    {
        return $this->suppliedAddressData;
    }

    /**
     * Set suppliedAddressData value
     *
     * @param SuppliedAddressData[] $suppliedAddressData
     * @return ResponsePerson
     * @throws InvalidArgumentException
     */
    public function setSuppliedAddressData(?array $suppliedAddressData = null): self
    {
        // validation for constraint: array
        if ('' !== ($suppliedAddressDataArrayErrorMessage = self::validateSuppliedAddressDataForArrayConstraintsFromSetSuppliedAddressData($suppliedAddressData))) {
            throw new InvalidArgumentException($suppliedAddressDataArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(10)
        if (is_array($suppliedAddressData) && count($suppliedAddressData) > 10) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 10',
                count($suppliedAddressData)
            ), __LINE__);
        }
        $this->suppliedAddressData = $suppliedAddressData;

        return $this;
    }

    /**
     * Add item to suppliedAddressData value
     *
     * @param SuppliedAddressData $item
     * @return ResponsePerson
     * @throws InvalidArgumentException
     */
    public function addToSuppliedAddressData(SuppliedAddressData $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof SuppliedAddressData) {
            throw new InvalidArgumentException(sprintf(
                'The suppliedAddressData property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\SuppliedAddressData, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(10)
        if (is_array($this->suppliedAddressData) && count($this->suppliedAddressData) >= 10) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 10',
                count($this->suppliedAddressData)
            ), __LINE__);
        }
        $this->suppliedAddressData[] = $item;

        return $this;
    }

    /**
     * Get linkedAddressData value
     *
     * @return LinkedAddressData[]
     */
    public function getLinkedAddressData(): ?array
    {
        return $this->linkedAddressData;
    }

    /**
     * Set linkedAddressData value
     *
     * @param LinkedAddressData[] $linkedAddressData
     * @return ResponsePerson
     * @throws InvalidArgumentException
     */
    public function setLinkedAddressData(?array $linkedAddressData = null): self
    {
        // validation for constraint: array
        if ('' !== ($linkedAddressDataArrayErrorMessage = self::validateLinkedAddressDataForArrayConstraintsFromSetLinkedAddressData($linkedAddressData))) {
            throw new InvalidArgumentException($linkedAddressDataArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($linkedAddressData) && count($linkedAddressData) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($linkedAddressData)
            ), __LINE__);
        }
        $this->linkedAddressData = $linkedAddressData;

        return $this;
    }

    /**
     * Add item to linkedAddressData value
     *
     * @param LinkedAddressData $item
     * @return ResponsePerson
     * @throws InvalidArgumentException
     */
    public function addToLinkedAddressData(LinkedAddressData $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof LinkedAddressData) {
            throw new InvalidArgumentException(sprintf(
                'The linkedAddressData property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\LinkedAddressData, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->linkedAddressData) && count($this->linkedAddressData) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->linkedAddressData)
            ), __LINE__);
        }
        $this->linkedAddressData[] = $item;

        return $this;
    }
}
