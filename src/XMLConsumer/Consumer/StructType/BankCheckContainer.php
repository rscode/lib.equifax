<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for BankCheckContainer StructType
 *
 * @subpackage Structs
 */
class BankCheckContainer extends DataItemContainer
{
    /**
     * The bankCheck
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var BankCheck|null
     */
    protected ?BankCheck $bankCheck = null;

    /**
     * Constructor method for BankCheckContainer
     *
     * @param BankCheck $bankCheck
     * @uses BankCheckContainer::setBankCheck()
     */
    public function __construct(?BankCheck $bankCheck = null)
    {
        $this
            ->setBankCheck($bankCheck);
    }

    /**
     * Get bankCheck value
     *
     * @return BankCheck|null
     */
    public function getBankCheck(): ?BankCheck
    {
        return $this->bankCheck;
    }

    /**
     * Set bankCheck value
     *
     * @param BankCheck $bankCheck
     * @return BankCheckContainer
     */
    public function setBankCheck(?BankCheck $bankCheck = null): self
    {
        $this->bankCheck = $bankCheck;

        return $this;
    }
}
