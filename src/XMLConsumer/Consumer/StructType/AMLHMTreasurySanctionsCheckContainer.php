<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for AMLHMTreasurySanctionsCheckContainer StructType
 *
 * @subpackage Structs
 */
class AMLHMTreasurySanctionsCheckContainer extends DataItemContainer
{
    /**
     * The amlHMTreasurySanctionsCheck
     * Meta information extracted from the WSDL
     * - maxOccurs: 2
     * - minOccurs: 0
     *
     * @var AMLHMTreasurySanctionsCheck[]
     */
    protected ?array $amlHMTreasurySanctionsCheck = null;

    /**
     * Constructor method for AMLHMTreasurySanctionsCheckContainer
     *
     * @param AMLHMTreasurySanctionsCheck[] $amlHMTreasurySanctionsCheck
     * @uses AMLHMTreasurySanctionsCheckContainer::setAmlHMTreasurySanctionsCheck()
     */
    public function __construct(?array $amlHMTreasurySanctionsCheck = null)
    {
        $this
            ->setAmlHMTreasurySanctionsCheck($amlHMTreasurySanctionsCheck);
    }

    /**
     * This method is responsible for validating the values passed to the setAmlHMTreasurySanctionsCheck method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAmlHMTreasurySanctionsCheck method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAmlHMTreasurySanctionsCheckForArrayConstraintsFromSetAmlHMTreasurySanctionsCheck(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $aMLHMTreasurySanctionsCheckContainerAmlHMTreasurySanctionsCheckItem) {
            // validation for constraint: itemType
            if (!$aMLHMTreasurySanctionsCheckContainerAmlHMTreasurySanctionsCheckItem instanceof AMLHMTreasurySanctionsCheck) {
                $invalidValues[] = is_object($aMLHMTreasurySanctionsCheckContainerAmlHMTreasurySanctionsCheckItem) ? get_class($aMLHMTreasurySanctionsCheckContainerAmlHMTreasurySanctionsCheckItem) : sprintf(
                    '%s(%s)',
                    gettype($aMLHMTreasurySanctionsCheckContainerAmlHMTreasurySanctionsCheckItem),
                    var_export($aMLHMTreasurySanctionsCheckContainerAmlHMTreasurySanctionsCheckItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The amlHMTreasurySanctionsCheck property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AMLHMTreasurySanctionsCheck, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get amlHMTreasurySanctionsCheck value
     *
     * @return AMLHMTreasurySanctionsCheck[]
     */
    public function getAmlHMTreasurySanctionsCheck(): ?array
    {
        return $this->amlHMTreasurySanctionsCheck;
    }

    /**
     * Set amlHMTreasurySanctionsCheck value
     *
     * @param AMLHMTreasurySanctionsCheck[] $amlHMTreasurySanctionsCheck
     * @return AMLHMTreasurySanctionsCheckContainer
     * @throws InvalidArgumentException
     */
    public function setAmlHMTreasurySanctionsCheck(?array $amlHMTreasurySanctionsCheck = null): self
    {
        // validation for constraint: array
        if ('' !== ($amlHMTreasurySanctionsCheckArrayErrorMessage = self::validateAmlHMTreasurySanctionsCheckForArrayConstraintsFromSetAmlHMTreasurySanctionsCheck($amlHMTreasurySanctionsCheck))) {
            throw new InvalidArgumentException($amlHMTreasurySanctionsCheckArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(2)
        if (is_array($amlHMTreasurySanctionsCheck) && count($amlHMTreasurySanctionsCheck) > 2) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 2',
                count($amlHMTreasurySanctionsCheck)
            ), __LINE__);
        }
        $this->amlHMTreasurySanctionsCheck = $amlHMTreasurySanctionsCheck;

        return $this;
    }

    /**
     * Add item to amlHMTreasurySanctionsCheck value
     *
     * @param AMLHMTreasurySanctionsCheck $item
     * @return AMLHMTreasurySanctionsCheckContainer
     * @throws InvalidArgumentException
     */
    public function addToAmlHMTreasurySanctionsCheck(AMLHMTreasurySanctionsCheck $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof AMLHMTreasurySanctionsCheck) {
            throw new InvalidArgumentException(sprintf(
                'The amlHMTreasurySanctionsCheck property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AMLHMTreasurySanctionsCheck, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(2)
        if (is_array($this->amlHMTreasurySanctionsCheck) && count($this->amlHMTreasurySanctionsCheck) >= 2) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 2',
                count($this->amlHMTreasurySanctionsCheck)
            ), __LINE__);
        }
        $this->amlHMTreasurySanctionsCheck[] = $item;

        return $this;
    }
}
