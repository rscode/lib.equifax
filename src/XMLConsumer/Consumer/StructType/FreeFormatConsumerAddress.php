<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for FreeFormatConsumerAddress StructType
 *
 * @subpackage Structs
 */
class FreeFormatConsumerAddress extends ConsumerAddress
{
    /**
     * The address
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var FreeFormatAddress
     */
    protected FreeFormatAddress $address;

    /**
     * Constructor method for FreeFormatConsumerAddress
     *
     * @param FreeFormatAddress $address
     * @uses FreeFormatConsumerAddress::setAddress()
     */
    public function __construct(FreeFormatAddress $address)
    {
        $this
            ->setAddress($address);
    }

    /**
     * Get address value
     *
     * @return FreeFormatAddress
     */
    public function getAddress(): FreeFormatAddress
    {
        return $this->address;
    }

    /**
     * Set address value
     *
     * @param FreeFormatAddress $address
     * @return FreeFormatConsumerAddress
     */
    public function setAddress(FreeFormatAddress $address): self
    {
        $this->address = $address;

        return $this;
    }
}
