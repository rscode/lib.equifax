<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for PropertyRental StructType
 *
 * @subpackage Structs
 */
class PropertyRental extends FinancialAgreement
{
}
