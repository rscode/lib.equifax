<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DataItem StructType
 *
 * @subpackage Structs
 */
abstract class DataItem extends AbstractStructBase
{
    /**
     * The sourcedFrom
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 15
     * - minLength: 3
     * - use: optional
     *
     * @var string|null
     */
    protected ?string $sourcedFrom = null;

    /**
     * Constructor method for DataItem
     *
     * @param string $sourcedFrom
     * @uses DataItem::setSourcedFrom()
     */
    public function __construct(?string $sourcedFrom = null)
    {
        $this
            ->setSourcedFrom($sourcedFrom);
    }

    /**
     * Get sourcedFrom value
     *
     * @return string|null
     */
    public function getSourcedFrom(): ?string
    {
        return $this->sourcedFrom;
    }

    /**
     * Set sourcedFrom value
     *
     * @param string $sourcedFrom
     * @return DataItem
     */
    public function setSourcedFrom(?string $sourcedFrom = null): self
    {
        // validation for constraint: string
        if (!is_null($sourcedFrom) && !is_string($sourcedFrom)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($sourcedFrom, true),
                gettype($sourcedFrom)
            ), __LINE__);
        }
        // validation for constraint: maxLength(15)
        if (!is_null($sourcedFrom) && mb_strlen((string)$sourcedFrom) > 15) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 15',
                mb_strlen((string)$sourcedFrom)
            ), __LINE__);
        }
        // validation for constraint: minLength(3)
        if (!is_null($sourcedFrom) && mb_strlen((string)$sourcedFrom) < 3) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 3',
                mb_strlen((string)$sourcedFrom)
            ), __LINE__);
        }
        $this->sourcedFrom = $sourcedFrom;

        return $this;
    }
}
