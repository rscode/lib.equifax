<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for AddressSpecificDataItem StructType
 *
 * @subpackage Structs
 */
abstract class AddressSpecificDataItem extends DataItem
{
    /**
     * The electoralRoll
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var ElectoralRoll[]
     */
    protected ?array $electoralRoll = null;
    /**
     * The previousSearch
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var PreviousSearch[]
     */
    protected ?array $previousSearch = null;

    /**
     * Constructor method for AddressSpecificDataItem
     *
     * @param ElectoralRoll[]  $electoralRoll
     * @param PreviousSearch[] $previousSearch
     * @uses AddressSpecificDataItem::setElectoralRoll()
     * @uses AddressSpecificDataItem::setPreviousSearch()
     */
    public function __construct(?array $electoralRoll = null, ?array $previousSearch = null)
    {
        $this
            ->setElectoralRoll($electoralRoll)
            ->setPreviousSearch($previousSearch);
    }

    /**
     * This method is responsible for validating the values passed to the setElectoralRoll method
     * This method is willingly generated in order to preserve the one-line inline validation within the setElectoralRoll method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateElectoralRollForArrayConstraintsFromSetElectoralRoll(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $addressSpecificDataItemElectoralRollItem) {
            // validation for constraint: itemType
            if (!$addressSpecificDataItemElectoralRollItem instanceof ElectoralRoll) {
                $invalidValues[] = is_object($addressSpecificDataItemElectoralRollItem) ? get_class($addressSpecificDataItemElectoralRollItem) : sprintf(
                    '%s(%s)',
                    gettype($addressSpecificDataItemElectoralRollItem),
                    var_export($addressSpecificDataItemElectoralRollItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The electoralRoll property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\ElectoralRoll, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * This method is responsible for validating the values passed to the setPreviousSearch method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPreviousSearch method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePreviousSearchForArrayConstraintsFromSetPreviousSearch(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $addressSpecificDataItemPreviousSearchItem) {
            // validation for constraint: itemType
            if (!$addressSpecificDataItemPreviousSearchItem instanceof PreviousSearch) {
                $invalidValues[] = is_object($addressSpecificDataItemPreviousSearchItem) ? get_class($addressSpecificDataItemPreviousSearchItem) : sprintf(
                    '%s(%s)',
                    gettype($addressSpecificDataItemPreviousSearchItem),
                    var_export($addressSpecificDataItemPreviousSearchItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The previousSearch property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\PreviousSearch, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get electoralRoll value
     *
     * @return ElectoralRoll[]
     */
    public function getElectoralRoll(): ?array
    {
        return $this->electoralRoll;
    }

    /**
     * Set electoralRoll value
     *
     * @param ElectoralRoll[] $electoralRoll
     * @return AddressSpecificDataItem
     * @throws InvalidArgumentException
     */
    public function setElectoralRoll(?array $electoralRoll = null): self
    {
        // validation for constraint: array
        if ('' !== ($electoralRollArrayErrorMessage = self::validateElectoralRollForArrayConstraintsFromSetElectoralRoll($electoralRoll))) {
            throw new InvalidArgumentException($electoralRollArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($electoralRoll) && count($electoralRoll) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($electoralRoll)
            ), __LINE__);
        }
        $this->electoralRoll = $electoralRoll;

        return $this;
    }

    /**
     * Add item to electoralRoll value
     *
     * @param ElectoralRoll $item
     * @return AddressSpecificDataItem
     * @throws InvalidArgumentException
     */
    public function addToElectoralRoll(ElectoralRoll $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof ElectoralRoll) {
            throw new InvalidArgumentException(sprintf(
                'The electoralRoll property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\ElectoralRoll, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->electoralRoll) && count($this->electoralRoll) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->electoralRoll)
            ), __LINE__);
        }
        $this->electoralRoll[] = $item;

        return $this;
    }

    /**
     * Get previousSearch value
     *
     * @return PreviousSearch[]
     */
    public function getPreviousSearch(): ?array
    {
        return $this->previousSearch;
    }

    /**
     * Set previousSearch value
     *
     * @param PreviousSearch[] $previousSearch
     * @return AddressSpecificDataItem
     * @throws InvalidArgumentException
     */
    public function setPreviousSearch(?array $previousSearch = null): self
    {
        // validation for constraint: array
        if ('' !== ($previousSearchArrayErrorMessage = self::validatePreviousSearchForArrayConstraintsFromSetPreviousSearch($previousSearch))) {
            throw new InvalidArgumentException($previousSearchArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($previousSearch) && count($previousSearch) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($previousSearch)
            ), __LINE__);
        }
        $this->previousSearch = $previousSearch;

        return $this;
    }

    /**
     * Add item to previousSearch value
     *
     * @param PreviousSearch $item
     * @return AddressSpecificDataItem
     * @throws InvalidArgumentException
     */
    public function addToPreviousSearch(PreviousSearch $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof PreviousSearch) {
            throw new InvalidArgumentException(sprintf(
                'The previousSearch property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\PreviousSearch, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->previousSearch) && count($this->previousSearch) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->previousSearch)
            ), __LINE__);
        }
        $this->previousSearch[] = $item;

        return $this;
    }
}
