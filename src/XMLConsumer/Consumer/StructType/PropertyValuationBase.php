<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for PropertyValuationBase StructType
 *
 * @subpackage Structs
 */
abstract class PropertyValuationBase extends NameMatchedData
{
    /**
     * The date
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $date;
    /**
     * The period
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var DatePeriod
     */
    protected DatePeriod $period;
    /**
     * The price
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var MonetaryAmount
     */
    protected MonetaryAmount $price;
    /**
     * The updateDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $updateDate;

    /**
     * Constructor method for PropertyValuationBase
     *
     * @param string         $date
     * @param DatePeriod     $period
     * @param MonetaryAmount $price
     * @param string         $updateDate
     * @uses PropertyValuationBase::setDate()
     * @uses PropertyValuationBase::setPeriod()
     * @uses PropertyValuationBase::setPrice()
     * @uses PropertyValuationBase::setUpdateDate()
     */
    public function __construct(string $date, DatePeriod $period, MonetaryAmount $price, string $updateDate)
    {
        $this
            ->setDate($date)
            ->setPeriod($period)
            ->setPrice($price)
            ->setUpdateDate($updateDate);
    }

    /**
     * Get date value
     *
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * Set date value
     *
     * @param string $date
     * @return PropertyValuationBase
     */
    public function setDate(string $date): self
    {
        // validation for constraint: string
        if (!is_null($date) && !is_string($date)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($date, true),
                gettype($date)
            ), __LINE__);
        }
        $this->date = $date;

        return $this;
    }

    /**
     * Get period value
     *
     * @return DatePeriod
     */
    public function getPeriod(): DatePeriod
    {
        return $this->period;
    }

    /**
     * Set period value
     *
     * @param DatePeriod $period
     * @return PropertyValuationBase
     */
    public function setPeriod(DatePeriod $period): self
    {
        $this->period = $period;

        return $this;
    }

    /**
     * Get price value
     *
     * @return MonetaryAmount
     */
    public function getPrice(): MonetaryAmount
    {
        return $this->price;
    }

    /**
     * Set price value
     *
     * @param MonetaryAmount $price
     * @return PropertyValuationBase
     */
    public function setPrice(MonetaryAmount $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get updateDate value
     *
     * @return string
     */
    public function getUpdateDate(): string
    {
        return $this->updateDate;
    }

    /**
     * Set updateDate value
     *
     * @param string $updateDate
     * @return PropertyValuationBase
     */
    public function setUpdateDate(string $updateDate): self
    {
        // validation for constraint: string
        if (!is_null($updateDate) && !is_string($updateDate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($updateDate, true),
                gettype($updateDate)
            ), __LINE__);
        }
        $this->updateDate = $updateDate;

        return $this;
    }
}
