<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DowJonesWatchlist StructType
 *
 * @subpackage Structs
 */
class DowJonesWatchlist extends AbstractStructBase
{
    /**
     * The uniqueIdentifier
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $uniqueIdentifier = null;
    /**
     * The activeStatus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $activeStatus = null;
    /**
     * The createDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $createDate = null;
    /**
     * The lastUpdatedDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $lastUpdatedDate = null;
    /**
     * The listName
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var string[]
     */
    protected ?array $listName = null;
    /**
     * The deceased
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $deceased = null;

    /**
     * Constructor method for DowJonesWatchlist
     *
     * @param string   $uniqueIdentifier
     * @param string   $activeStatus
     * @param string   $createDate
     * @param string   $lastUpdatedDate
     * @param string[] $listName
     * @param string   $deceased
     * @uses DowJonesWatchlist::setUniqueIdentifier()
     * @uses DowJonesWatchlist::setActiveStatus()
     * @uses DowJonesWatchlist::setCreateDate()
     * @uses DowJonesWatchlist::setLastUpdatedDate()
     * @uses DowJonesWatchlist::setListName()
     * @uses DowJonesWatchlist::setDeceased()
     */
    public function __construct(
        ?string $uniqueIdentifier = null,
        ?string $activeStatus = null,
        ?string $createDate = null,
        ?string $lastUpdatedDate = null,
        ?array $listName = null,
        ?string $deceased = null
    ) {
        $this
            ->setUniqueIdentifier($uniqueIdentifier)
            ->setActiveStatus($activeStatus)
            ->setCreateDate($createDate)
            ->setLastUpdatedDate($lastUpdatedDate)
            ->setListName($listName)
            ->setDeceased($deceased);
    }

    /**
     * This method is responsible for validating the values passed to the setListName method
     * This method is willingly generated in order to preserve the one-line inline validation within the setListName method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateListNameForArrayConstraintsFromSetListName(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $dowJonesWatchlistListNameItem) {
            // validation for constraint: itemType
            if (!is_string($dowJonesWatchlistListNameItem)) {
                $invalidValues[] = is_object($dowJonesWatchlistListNameItem) ? get_class($dowJonesWatchlistListNameItem) : sprintf(
                    '%s(%s)',
                    gettype($dowJonesWatchlistListNameItem),
                    var_export($dowJonesWatchlistListNameItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The listName property can only contain items of type string, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get uniqueIdentifier value
     *
     * @return string|null
     */
    public function getUniqueIdentifier(): ?string
    {
        return $this->uniqueIdentifier;
    }

    /**
     * Set uniqueIdentifier value
     *
     * @param string $uniqueIdentifier
     * @return DowJonesWatchlist
     */
    public function setUniqueIdentifier(?string $uniqueIdentifier = null): self
    {
        // validation for constraint: string
        if (!is_null($uniqueIdentifier) && !is_string($uniqueIdentifier)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($uniqueIdentifier, true),
                gettype($uniqueIdentifier)
            ), __LINE__);
        }
        $this->uniqueIdentifier = $uniqueIdentifier;

        return $this;
    }

    /**
     * Get activeStatus value
     *
     * @return string|null
     */
    public function getActiveStatus(): ?string
    {
        return $this->activeStatus;
    }

    /**
     * Set activeStatus value
     *
     * @param string $activeStatus
     * @return DowJonesWatchlist
     */
    public function setActiveStatus(?string $activeStatus = null): self
    {
        // validation for constraint: string
        if (!is_null($activeStatus) && !is_string($activeStatus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($activeStatus, true),
                gettype($activeStatus)
            ), __LINE__);
        }
        $this->activeStatus = $activeStatus;

        return $this;
    }

    /**
     * Get createDate value
     *
     * @return string|null
     */
    public function getCreateDate(): ?string
    {
        return $this->createDate;
    }

    /**
     * Set createDate value
     *
     * @param string $createDate
     * @return DowJonesWatchlist
     */
    public function setCreateDate(?string $createDate = null): self
    {
        // validation for constraint: string
        if (!is_null($createDate) && !is_string($createDate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($createDate, true),
                gettype($createDate)
            ), __LINE__);
        }
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get lastUpdatedDate value
     *
     * @return string|null
     */
    public function getLastUpdatedDate(): ?string
    {
        return $this->lastUpdatedDate;
    }

    /**
     * Set lastUpdatedDate value
     *
     * @param string $lastUpdatedDate
     * @return DowJonesWatchlist
     */
    public function setLastUpdatedDate(?string $lastUpdatedDate = null): self
    {
        // validation for constraint: string
        if (!is_null($lastUpdatedDate) && !is_string($lastUpdatedDate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($lastUpdatedDate, true),
                gettype($lastUpdatedDate)
            ), __LINE__);
        }
        $this->lastUpdatedDate = $lastUpdatedDate;

        return $this;
    }

    /**
     * Get listName value
     *
     * @return string[]
     */
    public function getListName(): ?array
    {
        return $this->listName;
    }

    /**
     * Set listName value
     *
     * @param string[] $listName
     * @return DowJonesWatchlist
     * @throws InvalidArgumentException
     */
    public function setListName(?array $listName = null): self
    {
        // validation for constraint: array
        if ('' !== ($listNameArrayErrorMessage = self::validateListNameForArrayConstraintsFromSetListName($listName))) {
            throw new InvalidArgumentException($listNameArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($listName) && count($listName) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($listName)
            ), __LINE__);
        }
        $this->listName = $listName;

        return $this;
    }

    /**
     * Add item to listName value
     *
     * @param string $item
     * @return DowJonesWatchlist
     * @throws InvalidArgumentException
     */
    public function addToListName(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf(
                'The listName property can only contain items of type string, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->listName) && count($this->listName) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->listName)
            ), __LINE__);
        }
        $this->listName[] = $item;

        return $this;
    }

    /**
     * Get deceased value
     *
     * @return string|null
     */
    public function getDeceased(): ?string
    {
        return $this->deceased;
    }

    /**
     * Set deceased value
     *
     * @param string $deceased
     * @return DowJonesWatchlist
     */
    public function setDeceased(?string $deceased = null): self
    {
        // validation for constraint: string
        if (!is_null($deceased) && !is_string($deceased)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($deceased, true),
                gettype($deceased)
            ), __LINE__);
        }
        $this->deceased = $deceased;

        return $this;
    }
}
