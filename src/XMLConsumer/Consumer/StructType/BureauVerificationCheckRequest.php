<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for BureauVerificationCheckRequest StructType
 *
 * @subpackage Structs
 */
class BureauVerificationCheckRequest extends CodedDataRequest
{
}
