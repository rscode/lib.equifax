<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CountryDetails StructType
 *
 * @subpackage Structs
 */
class CountryDetails extends AbstractStructBase
{
    /**
     * The citizenship
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var string[]
     */
    protected ?array $citizenship = null;
    /**
     * The placeOfBirth
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var string[]
     */
    protected ?array $placeOfBirth = null;
    /**
     * The otherCountryDetails
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var OtherCountryDetails|null
     */
    protected ?OtherCountryDetails $otherCountryDetails = null;

    /**
     * Constructor method for CountryDetails
     *
     * @param string[]            $citizenship
     * @param string[]            $placeOfBirth
     * @param OtherCountryDetails $otherCountryDetails
     * @uses CountryDetails::setCitizenship()
     * @uses CountryDetails::setPlaceOfBirth()
     * @uses CountryDetails::setOtherCountryDetails()
     */
    public function __construct(?array $citizenship = null, ?array $placeOfBirth = null, ?OtherCountryDetails $otherCountryDetails = null)
    {
        $this
            ->setCitizenship($citizenship)
            ->setPlaceOfBirth($placeOfBirth)
            ->setOtherCountryDetails($otherCountryDetails);
    }

    /**
     * This method is responsible for validating the values passed to the setCitizenship method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCitizenship method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCitizenshipForArrayConstraintsFromSetCitizenship(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $countryDetailsCitizenshipItem) {
            // validation for constraint: itemType
            if (!is_string($countryDetailsCitizenshipItem)) {
                $invalidValues[] = is_object($countryDetailsCitizenshipItem) ? get_class($countryDetailsCitizenshipItem) : sprintf(
                    '%s(%s)',
                    gettype($countryDetailsCitizenshipItem),
                    var_export($countryDetailsCitizenshipItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The citizenship property can only contain items of type string, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * This method is responsible for validating the values passed to the setPlaceOfBirth method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPlaceOfBirth method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePlaceOfBirthForArrayConstraintsFromSetPlaceOfBirth(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $countryDetailsPlaceOfBirthItem) {
            // validation for constraint: itemType
            if (!is_string($countryDetailsPlaceOfBirthItem)) {
                $invalidValues[] = is_object($countryDetailsPlaceOfBirthItem) ? get_class($countryDetailsPlaceOfBirthItem) : sprintf(
                    '%s(%s)',
                    gettype($countryDetailsPlaceOfBirthItem),
                    var_export($countryDetailsPlaceOfBirthItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The placeOfBirth property can only contain items of type string, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get citizenship value
     *
     * @return string[]
     */
    public function getCitizenship(): ?array
    {
        return $this->citizenship;
    }

    /**
     * Set citizenship value
     *
     * @param string[] $citizenship
     * @return CountryDetails
     * @throws InvalidArgumentException
     */
    public function setCitizenship(?array $citizenship = null): self
    {
        // validation for constraint: array
        if ('' !== ($citizenshipArrayErrorMessage = self::validateCitizenshipForArrayConstraintsFromSetCitizenship($citizenship))) {
            throw new InvalidArgumentException($citizenshipArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($citizenship) && count($citizenship) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($citizenship)
            ), __LINE__);
        }
        $this->citizenship = $citizenship;

        return $this;
    }

    /**
     * Add item to citizenship value
     *
     * @param string $item
     * @return CountryDetails
     * @throws InvalidArgumentException
     */
    public function addToCitizenship(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf(
                'The citizenship property can only contain items of type string, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->citizenship) && count($this->citizenship) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->citizenship)
            ), __LINE__);
        }
        $this->citizenship[] = $item;

        return $this;
    }

    /**
     * Get placeOfBirth value
     *
     * @return string[]
     */
    public function getPlaceOfBirth(): ?array
    {
        return $this->placeOfBirth;
    }

    /**
     * Set placeOfBirth value
     *
     * @param string[] $placeOfBirth
     * @return CountryDetails
     * @throws InvalidArgumentException
     */
    public function setPlaceOfBirth(?array $placeOfBirth = null): self
    {
        // validation for constraint: array
        if ('' !== ($placeOfBirthArrayErrorMessage = self::validatePlaceOfBirthForArrayConstraintsFromSetPlaceOfBirth($placeOfBirth))) {
            throw new InvalidArgumentException($placeOfBirthArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($placeOfBirth) && count($placeOfBirth) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($placeOfBirth)
            ), __LINE__);
        }
        $this->placeOfBirth = $placeOfBirth;

        return $this;
    }

    /**
     * Add item to placeOfBirth value
     *
     * @param string $item
     * @return CountryDetails
     * @throws InvalidArgumentException
     */
    public function addToPlaceOfBirth(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf(
                'The placeOfBirth property can only contain items of type string, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->placeOfBirth) && count($this->placeOfBirth) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->placeOfBirth)
            ), __LINE__);
        }
        $this->placeOfBirth[] = $item;

        return $this;
    }

    /**
     * Get otherCountryDetails value
     *
     * @return OtherCountryDetails|null
     */
    public function getOtherCountryDetails(): ?OtherCountryDetails
    {
        return $this->otherCountryDetails;
    }

    /**
     * Set otherCountryDetails value
     *
     * @param OtherCountryDetails $otherCountryDetails
     * @return CountryDetails
     */
    public function setOtherCountryDetails(?OtherCountryDetails $otherCountryDetails = null): self
    {
        $this->otherCountryDetails = $otherCountryDetails;

        return $this;
    }
}
