<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for InsuranceQuotationSearchRequest StructType
 * Meta information extracted from the WSDL
 * - documentation: Insurance Quotation
 *
 * @subpackage Structs
 */
class InsuranceQuotationSearchRequest extends CommonRequest
{
}
