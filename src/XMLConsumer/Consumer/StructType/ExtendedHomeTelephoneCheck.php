<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;

/**
 * This class stands for ExtendedHomeTelephoneCheck StructType
 * Meta information extracted from the WSDL
 * - documentation: The (Home) Telephone Check matches the supplied home telephone number to Equifax-held data sources and indicates the level of match found as well as the telephone line type (for the first applicant's current address only).
 *
 * @subpackage Structs
 */
class ExtendedHomeTelephoneCheck extends DataItem
{
    /**
     * The idNumber
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 40
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $idNumber;
    /**
     * The telephoneNumber
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 16
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $telephoneNumber = null;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for ExtendedHomeTelephoneCheck
     *
     * @param string                  $idNumber
     * @param string                  $telephoneNumber
     * @param DOMDocument|string|null $any
     * @uses ExtendedHomeTelephoneCheck::setIdNumber()
     * @uses ExtendedHomeTelephoneCheck::setTelephoneNumber()
     * @uses ExtendedHomeTelephoneCheck::setAny()
     */
    public function __construct(string $idNumber, ?string $telephoneNumber = null, $any = null)
    {
        $this
            ->setIdNumber($idNumber)
            ->setTelephoneNumber($telephoneNumber)
            ->setAny($any);
    }

    /**
     * Get idNumber value
     *
     * @return string
     */
    public function getIdNumber(): string
    {
        return $this->idNumber;
    }

    /**
     * Set idNumber value
     *
     * @param string $idNumber
     * @return ExtendedHomeTelephoneCheck
     */
    public function setIdNumber(string $idNumber): self
    {
        // validation for constraint: string
        if (!is_null($idNumber) && !is_string($idNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($idNumber, true),
                gettype($idNumber)
            ), __LINE__);
        }
        // validation for constraint: maxLength(40)
        if (!is_null($idNumber) && mb_strlen((string)$idNumber) > 40) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 40',
                mb_strlen((string)$idNumber)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($idNumber) && mb_strlen((string)$idNumber) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$idNumber)
            ), __LINE__);
        }
        $this->idNumber = $idNumber;

        return $this;
    }

    /**
     * Get telephoneNumber value
     *
     * @return string|null
     */
    public function getTelephoneNumber(): ?string
    {
        return $this->telephoneNumber;
    }

    /**
     * Set telephoneNumber value
     *
     * @param string $telephoneNumber
     * @return ExtendedHomeTelephoneCheck
     */
    public function setTelephoneNumber(?string $telephoneNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($telephoneNumber) && !is_string($telephoneNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($telephoneNumber, true),
                gettype($telephoneNumber)
            ), __LINE__);
        }
        // validation for constraint: maxLength(16)
        if (!is_null($telephoneNumber) && mb_strlen((string)$telephoneNumber) > 16) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 16',
                mb_strlen((string)$telephoneNumber)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($telephoneNumber) && mb_strlen((string)$telephoneNumber) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$telephoneNumber)
            ), __LINE__);
        }
        $this->telephoneNumber = $telephoneNumber;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return ExtendedHomeTelephoneCheck
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
