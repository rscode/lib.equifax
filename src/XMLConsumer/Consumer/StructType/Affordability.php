<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Affordability StructType
 *
 * @subpackage Structs
 */
class Affordability extends AbstractStructBase
{
    /**
     * The ProductInformation
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var ProductInformation|null
     */
    protected ?ProductInformation $ProductInformation = null;
    /**
     * The DependentsInformation
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var DependentsInformation|null
     */
    protected ?DependentsInformation $DependentsInformation = null;
    /**
     * The AdditionalExpenditure
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AdditionalExpenditure|null
     */
    protected ?AdditionalExpenditure $AdditionalExpenditure = null;
    /**
     * The CurrentEmployment
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var CurrentEmployment|null
     */
    protected ?CurrentEmployment $CurrentEmployment = null;
    /**
     * The OtherIncomeFields
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var OtherIncomeFields|null
     */
    protected ?OtherIncomeFields $OtherIncomeFields = null;
    /**
     * The PropertyInformation
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var PropertyInformation|null
     */
    protected ?PropertyInformation $PropertyInformation = null;
    /**
     * The AdditionalSortcodeAccountInformation
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var AdditionalSortcodeAccountInformation|null
     */
    protected ?AdditionalSortcodeAccountInformation $AdditionalSortcodeAccountInformation = null;
    /**
     * The ResidencyInformation
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var ResidencyInformation|null
     */
    protected ?ResidencyInformation $ResidencyInformation = null;
    /**
     * The Automotive
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var Automotive|null
     */
    protected ?Automotive $Automotive = null;

    /**
     * Constructor method for Affordability
     *
     * @param ProductInformation                   $productInformation
     * @param DependentsInformation                $dependentsInformation
     * @param AdditionalExpenditure                $additionalExpenditure
     * @param CurrentEmployment                    $currentEmployment
     * @param OtherIncomeFields                    $otherIncomeFields
     * @param PropertyInformation                  $propertyInformation
     * @param AdditionalSortcodeAccountInformation $additionalSortcodeAccountInformation
     * @param ResidencyInformation                 $residencyInformation
     * @param Automotive                           $automotive
     * @uses Affordability::setProductInformation()
     * @uses Affordability::setDependentsInformation()
     * @uses Affordability::setAdditionalExpenditure()
     * @uses Affordability::setCurrentEmployment()
     * @uses Affordability::setOtherIncomeFields()
     * @uses Affordability::setPropertyInformation()
     * @uses Affordability::setAdditionalSortcodeAccountInformation()
     * @uses Affordability::setResidencyInformation()
     * @uses Affordability::setAutomotive()
     */
    public function __construct(
        ?ProductInformation $productInformation = null,
        ?DependentsInformation $dependentsInformation = null,
        ?AdditionalExpenditure $additionalExpenditure = null,
        ?CurrentEmployment $currentEmployment = null,
        ?OtherIncomeFields $otherIncomeFields = null,
        ?PropertyInformation $propertyInformation = null,
        ?AdditionalSortcodeAccountInformation $additionalSortcodeAccountInformation = null,
        ?ResidencyInformation $residencyInformation = null,
        ?Automotive $automotive = null
    ) {
        $this
            ->setProductInformation($productInformation)
            ->setDependentsInformation($dependentsInformation)
            ->setAdditionalExpenditure($additionalExpenditure)
            ->setCurrentEmployment($currentEmployment)
            ->setOtherIncomeFields($otherIncomeFields)
            ->setPropertyInformation($propertyInformation)
            ->setAdditionalSortcodeAccountInformation($additionalSortcodeAccountInformation)
            ->setResidencyInformation($residencyInformation)
            ->setAutomotive($automotive);
    }

    /**
     * Get ProductInformation value
     *
     * @return ProductInformation|null
     */
    public function getProductInformation(): ?ProductInformation
    {
        return $this->ProductInformation;
    }

    /**
     * Set ProductInformation value
     *
     * @param ProductInformation $productInformation
     * @return Affordability
     */
    public function setProductInformation(?ProductInformation $productInformation = null): self
    {
        $this->ProductInformation = $productInformation;

        return $this;
    }

    /**
     * Get DependentsInformation value
     *
     * @return DependentsInformation|null
     */
    public function getDependentsInformation(): ?DependentsInformation
    {
        return $this->DependentsInformation;
    }

    /**
     * Set DependentsInformation value
     *
     * @param DependentsInformation $dependentsInformation
     * @return Affordability
     */
    public function setDependentsInformation(?DependentsInformation $dependentsInformation = null): self
    {
        $this->DependentsInformation = $dependentsInformation;

        return $this;
    }

    /**
     * Get AdditionalExpenditure value
     *
     * @return AdditionalExpenditure|null
     */
    public function getAdditionalExpenditure(): ?AdditionalExpenditure
    {
        return $this->AdditionalExpenditure;
    }

    /**
     * Set AdditionalExpenditure value
     *
     * @param AdditionalExpenditure $additionalExpenditure
     * @return Affordability
     */
    public function setAdditionalExpenditure(?AdditionalExpenditure $additionalExpenditure = null): self
    {
        $this->AdditionalExpenditure = $additionalExpenditure;

        return $this;
    }

    /**
     * Get CurrentEmployment value
     *
     * @return CurrentEmployment|null
     */
    public function getCurrentEmployment(): ?CurrentEmployment
    {
        return $this->CurrentEmployment;
    }

    /**
     * Set CurrentEmployment value
     *
     * @param CurrentEmployment $currentEmployment
     * @return Affordability
     */
    public function setCurrentEmployment(?CurrentEmployment $currentEmployment = null): self
    {
        $this->CurrentEmployment = $currentEmployment;

        return $this;
    }

    /**
     * Get OtherIncomeFields value
     *
     * @return OtherIncomeFields|null
     */
    public function getOtherIncomeFields(): ?OtherIncomeFields
    {
        return $this->OtherIncomeFields;
    }

    /**
     * Set OtherIncomeFields value
     *
     * @param OtherIncomeFields $otherIncomeFields
     * @return Affordability
     */
    public function setOtherIncomeFields(?OtherIncomeFields $otherIncomeFields = null): self
    {
        $this->OtherIncomeFields = $otherIncomeFields;

        return $this;
    }

    /**
     * Get PropertyInformation value
     *
     * @return PropertyInformation|null
     */
    public function getPropertyInformation(): ?PropertyInformation
    {
        return $this->PropertyInformation;
    }

    /**
     * Set PropertyInformation value
     *
     * @param PropertyInformation $propertyInformation
     * @return Affordability
     */
    public function setPropertyInformation(?PropertyInformation $propertyInformation = null): self
    {
        $this->PropertyInformation = $propertyInformation;

        return $this;
    }

    /**
     * Get AdditionalSortcodeAccountInformation value
     *
     * @return AdditionalSortcodeAccountInformation|null
     */
    public function getAdditionalSortcodeAccountInformation(): ?AdditionalSortcodeAccountInformation
    {
        return $this->AdditionalSortcodeAccountInformation;
    }

    /**
     * Set AdditionalSortcodeAccountInformation value
     *
     * @param AdditionalSortcodeAccountInformation $additionalSortcodeAccountInformation
     * @return Affordability
     */
    public function setAdditionalSortcodeAccountInformation(
        ?AdditionalSortcodeAccountInformation $additionalSortcodeAccountInformation = null
    ): self {
        $this->AdditionalSortcodeAccountInformation = $additionalSortcodeAccountInformation;

        return $this;
    }

    /**
     * Get ResidencyInformation value
     *
     * @return ResidencyInformation|null
     */
    public function getResidencyInformation(): ?ResidencyInformation
    {
        return $this->ResidencyInformation;
    }

    /**
     * Set ResidencyInformation value
     *
     * @param ResidencyInformation $residencyInformation
     * @return Affordability
     */
    public function setResidencyInformation(?ResidencyInformation $residencyInformation = null): self
    {
        $this->ResidencyInformation = $residencyInformation;

        return $this;
    }

    /**
     * Get Automotive value
     *
     * @return Automotive|null
     */
    public function getAutomotive(): ?Automotive
    {
        return $this->Automotive;
    }

    /**
     * Set Automotive value
     *
     * @param Automotive $automotive
     * @return Affordability
     */
    public function setAutomotive(?Automotive $automotive = null): self
    {
        $this->Automotive = $automotive;

        return $this;
    }
}
