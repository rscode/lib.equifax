<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for FraudInvestigationResponse StructType
 * Meta information extracted from the WSDL
 * - documentation: This operation is used for fraud investigation of an individual
 *
 * @subpackage Structs
 */
class FraudInvestigationResponse extends CommonResponse
{
}
