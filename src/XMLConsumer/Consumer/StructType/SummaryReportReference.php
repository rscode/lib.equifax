<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SummaryReportReference StructType
 *
 * @subpackage Structs
 */
class SummaryReportReference extends AbstractStructBase
{
    /**
     * The createDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $createDate = null;
    /**
     * The createTime
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $createTime = null;
    /**
     * The enquiryReference
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $enquiryReference = null;

    /**
     * Constructor method for SummaryReportReference
     *
     * @param string $createDate
     * @param string $createTime
     * @param string $enquiryReference
     * @uses SummaryReportReference::setCreateDate()
     * @uses SummaryReportReference::setCreateTime()
     * @uses SummaryReportReference::setEnquiryReference()
     */
    public function __construct(?string $createDate = null, ?string $createTime = null, ?string $enquiryReference = null)
    {
        $this
            ->setCreateDate($createDate)
            ->setCreateTime($createTime)
            ->setEnquiryReference($enquiryReference);
    }

    /**
     * Get createDate value
     *
     * @return string|null
     */
    public function getCreateDate(): ?string
    {
        return $this->createDate;
    }

    /**
     * Set createDate value
     *
     * @param string $createDate
     * @return SummaryReportReference
     */
    public function setCreateDate(?string $createDate = null): self
    {
        // validation for constraint: string
        if (!is_null($createDate) && !is_string($createDate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($createDate, true),
                gettype($createDate)
            ), __LINE__);
        }
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createTime value
     *
     * @return string|null
     */
    public function getCreateTime(): ?string
    {
        return $this->createTime;
    }

    /**
     * Set createTime value
     *
     * @param string $createTime
     * @return SummaryReportReference
     */
    public function setCreateTime(?string $createTime = null): self
    {
        // validation for constraint: string
        if (!is_null($createTime) && !is_string($createTime)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($createTime, true),
                gettype($createTime)
            ), __LINE__);
        }
        $this->createTime = $createTime;

        return $this;
    }

    /**
     * Get enquiryReference value
     *
     * @return string|null
     */
    public function getEnquiryReference(): ?string
    {
        return $this->enquiryReference;
    }

    /**
     * Set enquiryReference value
     *
     * @param string $enquiryReference
     * @return SummaryReportReference
     */
    public function setEnquiryReference(?string $enquiryReference = null): self
    {
        // validation for constraint: string
        if (!is_null($enquiryReference) && !is_string($enquiryReference)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($enquiryReference, true),
                gettype($enquiryReference)
            ), __LINE__);
        }
        $this->enquiryReference = $enquiryReference;

        return $this;
    }
}
