<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CommonResponse StructType
 *
 * @subpackage Structs
 */
class CommonResponse extends AbstractStructBase
{
    /**
     * The clientRef
     * Meta information extracted from the WSDL
     * - documentation: Used to supply an enquiry reference identifier. | The value supplied in the request is simply echoed back in the corresponding response.
     * - base: xs:string
     * - maxLength: 40
     * - maxOccurs: 1
     * - minLength: 1
     * - minOccurs: 1
     * - pattern: .*[^\s].*
     *
     * @var string
     */
    protected string $clientRef;
    /**
     * The nonAddressSpecificData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var NonAddressSpecificData|null
     */
    protected ?NonAddressSpecificData $nonAddressSpecificData = null;
    /**
     * The jointSearch
     *
     * @var ResponseJointSearch|null
     */
    protected ?ResponseJointSearch $jointSearch = null;
    /**
     * The soleSearch
     *
     * @var ResponseSoleSearch|null
     */
    protected ?ResponseSoleSearch $soleSearch = null;

    public function __construct(
        string $clientRef,
        ?NonAddressSpecificData $nonAddressSpecificData = null,
        ?ResponseJointSearch $jointSearch = null,
        ?ResponseSoleSearch $soleSearch = null
    ) {
        $this
            ->setClientRef($clientRef)
            ->setNonAddressSpecificData($nonAddressSpecificData)
            ->setJointSearch($jointSearch)
            ->setSoleSearch($soleSearch);
    }

    /**
     * Get clientRef value
     *
     * @return string
     */
    public function getClientRef(): string
    {
        return $this->clientRef;
    }

    /**
     * Set clientRef value
     *
     * @param string $clientRef
     * @return CommonResponse
     */
    public function setClientRef(string $clientRef): self
    {
        // validation for constraint: string
        if (!is_null($clientRef) && !is_string($clientRef)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($clientRef, true),
                gettype($clientRef)
            ), __LINE__);
        }
        // validation for constraint: maxLength(40)
        if (!is_null($clientRef) && mb_strlen((string)$clientRef) > 40) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 40',
                mb_strlen((string)$clientRef)
            ), __LINE__);
        }
        // validation for constraint: minLength(1)
        if (!is_null($clientRef) && mb_strlen((string)$clientRef) < 1) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 1',
                mb_strlen((string)$clientRef)
            ), __LINE__);
        }
        // validation for constraint: pattern(.*[^\s].*)
        if (!is_null($clientRef) && !preg_match('/.*[^\\s].*/', $clientRef)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /.*[^\\s].*/',
                var_export($clientRef, true)
            ), __LINE__);
        }
        $this->clientRef = $clientRef;

        return $this;
    }

    /**
     * Get nonAddressSpecificData value
     *
     * @return NonAddressSpecificData|null
     */
    public function getNonAddressSpecificData(): ?NonAddressSpecificData
    {
        return $this->nonAddressSpecificData;
    }

    /**
     * Set nonAddressSpecificData value
     *
     * @param NonAddressSpecificData $nonAddressSpecificData
     * @return CommonResponse
     */
    public function setNonAddressSpecificData(?NonAddressSpecificData $nonAddressSpecificData = null): self
    {
        $this->nonAddressSpecificData = $nonAddressSpecificData;

        return $this;
    }

    /**
     * Get jointSearch value
     *
     * @return ResponseJointSearch|null
     */
    public function getJointSearch(): ?ResponseJointSearch
    {
        return $this->jointSearch;
    }

    /**
     * Set jointSearch value
     *
     * @param ResponseJointSearch $jointSearch
     * @return CommonResponse
     */
    public function setJointSearch(?ResponseJointSearch $jointSearch = null): self
    {
        $this->jointSearch = $jointSearch;

        return $this;
    }

    /**
     * Get soleSearch value
     *
     * @return ResponseSoleSearch|null
     */
    public function getSoleSearch(): ?ResponseSoleSearch
    {
        return $this->soleSearch;
    }

    /**
     * Set soleSearch value
     *
     * @param ResponseSoleSearch $soleSearch
     * @return CommonResponse
     */
    public function setSoleSearch(?ResponseSoleSearch $soleSearch = null): self
    {
        $this->soleSearch = $soleSearch;

        return $this;
    }
}
