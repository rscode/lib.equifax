<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for HaloCertLevel StructType
 *
 * @subpackage Structs
 */
class HaloCertLevel extends AbstractStructBase
{
    /**
     * The id
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 1
     * - minInclusive: 0
     * - use: required
     *
     * @var int
     */
    protected int $id;
    /**
     * The _
     *
     * @var bool|null
     */
    protected ?bool $_ = null;

    /**
     * Constructor method for HaloCertLevel
     *
     * @param int  $id
     * @param bool $_
     * @uses HaloCertLevel::setId()
     * @uses HaloCertLevel::set_()
     */
    public function __construct(int $id, ?bool $_ = null)
    {
        $this
            ->setId($id)
            ->set_($_);
    }

    /**
     * Get id value
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set id value
     *
     * @param int $id
     * @return HaloCertLevel
     */
    public function setId(int $id): self
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($id, true),
                gettype($id)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(1)
        if (!is_null($id) && $id > 1) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 1',
                var_export($id, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($id) && $id < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($id, true)
            ), __LINE__);
        }
        $this->id = $id;

        return $this;
    }

    /**
     * Get _ value
     *
     * @return bool|null
     */
    public function get_(): ?bool
    {
        return $this->_;
    }

    /**
     * Set _ value
     *
     * @param bool $_
     * @return HaloCertLevel
     */
    public function set_(?bool $_ = null): self
    {
        // validation for constraint: boolean
        if (!is_null($_) && !is_bool($_)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($_, true),
                gettype($_)
            ), __LINE__);
        }
        $this->_ = $_;

        return $this;
    }
}
