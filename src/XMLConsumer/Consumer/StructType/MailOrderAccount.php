<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\MailOrderType;

/**
 * This class stands for MailOrderAccount StructType
 *
 * @subpackage Structs
 */
class MailOrderAccount extends FinancialAgreement
{
    /**
     * The mailOrderType
     * Meta information extracted from the WSDL
     * - use: optional
     *
     * @var string|null
     */
    protected ?string $mailOrderType = null;

    /**
     * Constructor method for MailOrderAccount
     *
     * @param string $mailOrderType
     * @uses MailOrderAccount::setMailOrderType()
     */
    public function __construct(?string $mailOrderType = null)
    {
        $this
            ->setMailOrderType($mailOrderType);
    }

    /**
     * Get mailOrderType value
     *
     * @return string|null
     */
    public function getMailOrderType(): ?string
    {
        return $this->mailOrderType;
    }

    /**
     * Set mailOrderType value
     *
     * @param string $mailOrderType
     * @return MailOrderAccount
     * @throws InvalidArgumentException
     * @uses MailOrderType::getValidValues
     * @uses MailOrderType::valueIsValid
     */
    public function setMailOrderType(?string $mailOrderType = null): self
    {
        // validation for constraint: enumeration
        if (!MailOrderType::valueIsValid($mailOrderType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\MailOrderType',
                is_array($mailOrderType) ? implode(', ', $mailOrderType) : var_export($mailOrderType, true),
                implode(', ', MailOrderType::getValidValues())
            ), __LINE__);
        }
        $this->mailOrderType = $mailOrderType;

        return $this;
    }
}
