<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\YesNoUnknown;

/**
 * This class stands for TelephoneData StructType
 * Meta information extracted from the WSDL
 * - documentation: Home telephone number information.
 *
 * @subpackage Structs
 */
class TelephoneData extends NamedData
{
    /**
     * The telephoneNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var TelephoneNumber
     */
    protected TelephoneNumber $telephoneNumber;
    /**
     * The telephonePreferenceService
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $telephonePreferenceService;
    /**
     * The dateLoaded
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $dateLoaded = null;
    /**
     * The lineType
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 1
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $lineType = null;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for TelephoneData
     *
     * @param TelephoneNumber $telephoneNumber
     * @param string $telephonePreferenceService
     * @param string $dateLoaded
     * @param string $lineType
     * @param DOMDocument|string|null $any
     * @uses TelephoneData::setTelephoneNumber()
     * @uses TelephoneData::setTelephonePreferenceService()
     * @uses TelephoneData::setDateLoaded()
     * @uses TelephoneData::setLineType()
     * @uses TelephoneData::setAny()
     */
    public function __construct(
        TelephoneNumber $telephoneNumber,
        string $telephonePreferenceService,
        ?string $dateLoaded = null,
        ?string $lineType = null,
        $any = null
    ) {
        $this
            ->setTelephoneNumber($telephoneNumber)
            ->setTelephonePreferenceService($telephonePreferenceService)
            ->setDateLoaded($dateLoaded)
            ->setLineType($lineType)
            ->setAny($any);
    }

    /**
     * Get telephoneNumber value
     *
     * @return TelephoneNumber
     */
    public function getTelephoneNumber(): TelephoneNumber
    {
        return $this->telephoneNumber;
    }

    /**
     * Set telephoneNumber value
     *
     * @param TelephoneNumber $telephoneNumber
     * @return TelephoneData
     */
    public function setTelephoneNumber(TelephoneNumber $telephoneNumber): self
    {
        $this->telephoneNumber = $telephoneNumber;

        return $this;
    }

    /**
     * Get telephonePreferenceService value
     *
     * @return string
     */
    public function getTelephonePreferenceService(): string
    {
        return $this->telephonePreferenceService;
    }

    /**
     * Set telephonePreferenceService value
     *
     * @param string $telephonePreferenceService
     * @return TelephoneData
     * @throws InvalidArgumentException
     * @uses YesNoUnknown::getValidValues
     * @uses YesNoUnknown::valueIsValid
     */
    public function setTelephonePreferenceService(string $telephonePreferenceService): self
    {
        // validation for constraint: enumeration
        if (!YesNoUnknown::valueIsValid($telephonePreferenceService)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\YesNoUnknown',
                is_array($telephonePreferenceService) ? implode(', ', $telephonePreferenceService) : var_export(
                    $telephonePreferenceService,
                    true
                ),
                implode(', ', YesNoUnknown::getValidValues())
            ), __LINE__);
        }
        $this->telephonePreferenceService = $telephonePreferenceService;

        return $this;
    }

    /**
     * Get dateLoaded value
     *
     * @return string|null
     */
    public function getDateLoaded(): ?string
    {
        return $this->dateLoaded;
    }

    /**
     * Set dateLoaded value
     *
     * @param string $dateLoaded
     * @return TelephoneData
     */
    public function setDateLoaded(?string $dateLoaded = null): self
    {
        // validation for constraint: string
        if (!is_null($dateLoaded) && !is_string($dateLoaded)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($dateLoaded, true),
                gettype($dateLoaded)
            ), __LINE__);
        }
        $this->dateLoaded = $dateLoaded;

        return $this;
    }

    /**
     * Get lineType value
     *
     * @return string|null
     */
    public function getLineType(): ?string
    {
        return $this->lineType;
    }

    /**
     * Set lineType value
     *
     * @param string $lineType
     * @return TelephoneData
     */
    public function setLineType(?string $lineType = null): self
    {
        // validation for constraint: string
        if (!is_null($lineType) && !is_string($lineType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($lineType, true),
                gettype($lineType)
            ), __LINE__);
        }
        // validation for constraint: maxLength(1)
        if (!is_null($lineType) && mb_strlen((string)$lineType) > 1) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 1',
                mb_strlen((string)$lineType)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($lineType) && mb_strlen((string)$lineType) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$lineType)
            ), __LINE__);
        }
        $this->lineType = $lineType;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return TelephoneData
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
