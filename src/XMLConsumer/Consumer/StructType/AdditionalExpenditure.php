<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AdditionalExpenditure StructType
 *
 * @subpackage Structs
 */
class AdditionalExpenditure extends AbstractStructBase
{
    /**
     * The ExpenditureSupplyMethod1
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var ExpenditureSupplyMethod1|null
     */
    protected ?ExpenditureSupplyMethod1 $ExpenditureSupplyMethod1 = null;

    /**
     * Constructor method for AdditionalExpenditure
     *
     * @param ExpenditureSupplyMethod1 $expenditureSupplyMethod1
     * @uses AdditionalExpenditure::setExpenditureSupplyMethod1()
     */
    public function __construct(?ExpenditureSupplyMethod1 $expenditureSupplyMethod1 = null)
    {
        $this
            ->setExpenditureSupplyMethod1($expenditureSupplyMethod1);
    }

    /**
     * Get ExpenditureSupplyMethod1 value
     *
     * @return ExpenditureSupplyMethod1|null
     */
    public function getExpenditureSupplyMethod1(): ?ExpenditureSupplyMethod1
    {
        return $this->ExpenditureSupplyMethod1;
    }

    /**
     * Set ExpenditureSupplyMethod1 value
     *
     * @param ExpenditureSupplyMethod1 $expenditureSupplyMethod1
     * @return AdditionalExpenditure
     */
    public function setExpenditureSupplyMethod1(?ExpenditureSupplyMethod1 $expenditureSupplyMethod1 = null): self
    {
        $this->ExpenditureSupplyMethod1 = $expenditureSupplyMethod1;

        return $this;
    }
}
