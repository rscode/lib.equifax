<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for GAINContainer StructType
 *
 * @subpackage Structs
 */
class GAINContainer extends DataItemContainer
{
    /**
     * The goneAway
     * Meta information extracted from the WSDL
     * - maxOccurs: 99
     * - minOccurs: 0
     *
     * @var GAIN[]
     */
    protected ?array $goneAway = null;

    /**
     * Constructor method for GAINContainer
     *
     * @param GAIN[] $goneAway
     * @uses GAINContainer::setGoneAway()
     */
    public function __construct(?array $goneAway = null)
    {
        $this
            ->setGoneAway($goneAway);
    }

    /**
     * This method is responsible for validating the values passed to the setGoneAway method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGoneAway method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGoneAwayForArrayConstraintsFromSetGoneAway(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $gAINContainerGoneAwayItem) {
            // validation for constraint: itemType
            if (!$gAINContainerGoneAwayItem instanceof GAIN) {
                $invalidValues[] = is_object($gAINContainerGoneAwayItem) ? get_class($gAINContainerGoneAwayItem) : sprintf(
                    '%s(%s)',
                    gettype($gAINContainerGoneAwayItem),
                    var_export($gAINContainerGoneAwayItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The goneAway property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\GAIN, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get goneAway value
     *
     * @return GAIN[]
     */
    public function getGoneAway(): ?array
    {
        return $this->goneAway;
    }

    /**
     * Set goneAway value
     *
     * @param GAIN[] $goneAway
     * @return GAINContainer
     * @throws InvalidArgumentException
     */
    public function setGoneAway(?array $goneAway = null): self
    {
        // validation for constraint: array
        if ('' !== ($goneAwayArrayErrorMessage = self::validateGoneAwayForArrayConstraintsFromSetGoneAway($goneAway))) {
            throw new InvalidArgumentException($goneAwayArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($goneAway) && count($goneAway) > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 99',
                count($goneAway)
            ), __LINE__);
        }
        $this->goneAway = $goneAway;

        return $this;
    }

    /**
     * Add item to goneAway value
     *
     * @param GAIN $item
     * @return GAINContainer
     * @throws InvalidArgumentException
     */
    public function addToGoneAway(GAIN $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof GAIN) {
            throw new InvalidArgumentException(sprintf(
                'The goneAway property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\GAIN, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(99)
        if (is_array($this->goneAway) && count($this->goneAway) >= 99) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 99',
                count($this->goneAway)
            ), __LINE__);
        }
        $this->goneAway[] = $item;

        return $this;
    }
}
