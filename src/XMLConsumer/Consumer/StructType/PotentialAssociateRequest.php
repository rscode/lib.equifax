<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for PotentialAssociateRequest StructType
 *
 * @subpackage Structs
 */
class PotentialAssociateRequest extends RawDataRequest
{
    /**
     * The lenderName
     *
     * @var bool|null
     */
    protected ?bool $lenderName = null;

    /**
     * Constructor method for PotentialAssociateRequest
     *
     * @param bool $lenderName
     * @uses PotentialAssociateRequest::setLenderName()
     */
    public function __construct(?bool $lenderName = null)
    {
        $this
            ->setLenderName($lenderName);
    }

    /**
     * Get lenderName value
     *
     * @return bool|null
     */
    public function getLenderName(): ?bool
    {
        return $this->lenderName;
    }

    /**
     * Set lenderName value
     *
     * @param bool $lenderName
     * @return PotentialAssociateRequest
     */
    public function setLenderName(?bool $lenderName = null): self
    {
        // validation for constraint: boolean
        if (!is_null($lenderName) && !is_bool($lenderName)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($lenderName, true),
                gettype($lenderName)
            ), __LINE__);
        }
        $this->lenderName = $lenderName;

        return $this;
    }
}
