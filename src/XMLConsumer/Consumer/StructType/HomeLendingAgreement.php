<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for HomeLendingAgreement StructType
 *
 * @subpackage Structs
 */
class HomeLendingAgreement extends FinancialAgreement
{
}
