<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for SocialHousingRental StructType
 *
 * @subpackage Structs
 */
class SocialHousingRental extends FinancialAgreement
{
}
