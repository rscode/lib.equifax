<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for ApplicationFraudCheckRequest StructType
 * Meta information extracted from the WSDL
 * - documentation: Application Fraud Check against previous frauds and applications utilising the SIRAN service.
 *
 * @subpackage Structs
 */
class ApplicationFraudCheckRequest extends CommonRequest
{
}
