<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\SearchRelationship;

/**
 * This class stands for RequestJointSearch StructType
 * Meta information extracted from the WSDL
 * - documentation: Nature of the search: Joint applicant application
 *
 * @subpackage Structs
 */
class RequestJointSearch extends Search
{
    /**
     * The primary
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var RequestPerson
     */
    protected RequestPerson $primary;
    /**
     * The relationship
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $relationship;
    /**
     * The secondary
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var RequestPerson
     */
    protected RequestPerson $secondary;

    /**
     * Constructor method for RequestJointSearch
     *
     * @param RequestPerson $primary
     * @param string        $relationship
     * @param RequestPerson $secondary
     * @uses RequestJointSearch::setPrimary()
     * @uses RequestJointSearch::setRelationship()
     * @uses RequestJointSearch::setSecondary()
     */
    public function __construct(RequestPerson $primary, string $relationship, RequestPerson $secondary)
    {
        $this
            ->setPrimary($primary)
            ->setRelationship($relationship)
            ->setSecondary($secondary);
    }

    /**
     * Get primary value
     *
     * @return RequestPerson
     */
    public function getPrimary(): RequestPerson
    {
        return $this->primary;
    }

    /**
     * Set primary value
     *
     * @param RequestPerson $primary
     * @return RequestJointSearch
     */
    public function setPrimary(RequestPerson $primary): self
    {
        $this->primary = $primary;

        return $this;
    }

    /**
     * Get relationship value
     *
     * @return string
     */
    public function getRelationship(): string
    {
        return $this->relationship;
    }

    /**
     * Set relationship value
     *
     * @param string $relationship
     * @return RequestJointSearch
     * @throws InvalidArgumentException
     * @uses SearchRelationship::getValidValues
     * @uses SearchRelationship::valueIsValid
     */
    public function setRelationship(string $relationship): self
    {
        // validation for constraint: enumeration
        if (!SearchRelationship::valueIsValid($relationship)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\SearchRelationship',
                is_array($relationship) ? implode(', ', $relationship) : var_export($relationship, true),
                implode(', ', SearchRelationship::getValidValues())
            ), __LINE__);
        }
        $this->relationship = $relationship;

        return $this;
    }

    /**
     * Get secondary value
     *
     * @return RequestPerson
     */
    public function getSecondary(): RequestPerson
    {
        return $this->secondary;
    }

    /**
     * Set secondary value
     *
     * @param RequestPerson $secondary
     * @return RequestJointSearch
     */
    public function setSecondary(RequestPerson $secondary): self
    {
        $this->secondary = $secondary;

        return $this;
    }
}
