<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\CreditSearchType;

/**
 * This class stands for PreviousCreditSearch StructType
 *
 * @subpackage Structs
 */
class PreviousCreditSearch extends PreviousSearch
{
    /**
     * The searchType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $searchType;
    /**
     * The optIn
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $optIn;

    public function __construct(string $searchType, bool $optIn)
    {
        $this
            ->setSearchType($searchType)
            ->setOptIn($optIn);
    }

    /**
     * Get searchType value
     *
     * @return string
     */
    public function getSearchType(): string
    {
        return $this->searchType;
    }

    /**
     * Set searchType value
     *
     * @param string $searchType
     * @return PreviousCreditSearch
     * @throws InvalidArgumentException
     * @uses CreditSearchType::getValidValues
     * @uses CreditSearchType::valueIsValid
     */
    public function setSearchType(string $searchType): self
    {
        // validation for constraint: enumeration
        if (!CreditSearchType::valueIsValid($searchType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\CreditSearchType',
                is_array($searchType) ? implode(', ', $searchType) : var_export($searchType, true),
                implode(', ', CreditSearchType::getValidValues())
            ), __LINE__);
        }
        $this->searchType = $searchType;

        return $this;
    }

    /**
     * Get optIn value
     *
     * @return bool
     */
    public function getOptIn(): bool
    {
        return $this->optIn;
    }

    /**
     * Set optIn value
     *
     * @param bool $optIn
     * @return PreviousCreditSearch
     */
    public function setOptIn(bool $optIn): self
    {
        // validation for constraint: boolean
        if (!is_null($optIn) && !is_bool($optIn)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($optIn, true),
                gettype($optIn)
            ), __LINE__);
        }
        $this->optIn = $optIn;

        return $this;
    }
}
