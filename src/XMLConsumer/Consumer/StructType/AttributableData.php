<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for AttributableData StructType
 *
 * @subpackage Structs
 */
class AttributableData extends NamedData
{
}
