<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\SecuredLoanType;

/**
 * This class stands for SecuredLoan StructType
 *
 * @subpackage Structs
 */
class SecuredLoan extends FinancialAgreement
{
    /**
     * The flexible
     * Meta information extracted from the WSDL
     * - use: optional
     *
     * @var bool|null
     */
    protected ?bool $flexible = null;
    /**
     * The loanType
     * Meta information extracted from the WSDL
     * - use: optional
     *
     * @var string|null
     */
    protected ?string $loanType = null;

    /**
     * Constructor method for SecuredLoan
     *
     * @param bool   $flexible
     * @param string $loanType
     * @uses SecuredLoan::setFlexible()
     * @uses SecuredLoan::setLoanType()
     */
    public function __construct(?bool $flexible = null, ?string $loanType = null)
    {
        $this
            ->setFlexible($flexible)
            ->setLoanType($loanType);
    }

    /**
     * Get flexible value
     *
     * @return bool|null
     */
    public function getFlexible(): ?bool
    {
        return $this->flexible;
    }

    /**
     * Set flexible value
     *
     * @param bool $flexible
     * @return SecuredLoan
     */
    public function setFlexible(?bool $flexible = null): self
    {
        // validation for constraint: boolean
        if (!is_null($flexible) && !is_bool($flexible)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($flexible, true),
                gettype($flexible)
            ), __LINE__);
        }
        $this->flexible = $flexible;

        return $this;
    }

    /**
     * Get loanType value
     *
     * @return string|null
     */
    public function getLoanType(): ?string
    {
        return $this->loanType;
    }

    /**
     * Set loanType value
     *
     * @param string $loanType
     * @return SecuredLoan
     * @throws InvalidArgumentException
     * @uses SecuredLoanType::getValidValues
     * @uses SecuredLoanType::valueIsValid
     */
    public function setLoanType(?string $loanType = null): self
    {
        // validation for constraint: enumeration
        if (!SecuredLoanType::valueIsValid($loanType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\SecuredLoanType',
                is_array($loanType) ? implode(', ', $loanType) : var_export($loanType, true),
                implode(', ', SecuredLoanType::getValidValues())
            ), __LINE__);
        }
        $this->loanType = $loanType;

        return $this;
    }
}
