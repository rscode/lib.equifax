<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for CouncilArrears StructType
 *
 * @subpackage Structs
 */
class CouncilArrears extends FinancialAgreement
{
}
