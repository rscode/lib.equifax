<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for CustomerManagementEnquiryResponse StructType
 * Meta information extracted from the WSDL
 * - documentation: Credit customer management
 *
 * @subpackage Structs
 */
class CustomerManagementEnquiryResponse extends CommonResponse
{
}
