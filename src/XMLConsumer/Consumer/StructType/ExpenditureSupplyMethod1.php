<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ExpenditureSupplyMethod1 StructType
 *
 * @subpackage Structs
 */
class ExpenditureSupplyMethod1 extends AbstractStructBase
{
    /**
     * The ApplicantMonthlyHousingCosts
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ApplicantMonthlyHousingCosts = null;
    /**
     * The ApplicantMonthlyCreditCommitments
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ApplicantMonthlyCreditCommitments = null;
    /**
     * The ApplicantMonthlyUtilitiesAndBills
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ApplicantMonthlyUtilitiesAndBills = null;
    /**
     * The ApplicantMonthlyTransportationAndTravelCosts
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ApplicantMonthlyTransportationAndTravelCosts = null;
    /**
     * The ApplicantMonthlyFoodCosts
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ApplicantMonthlyFoodCosts = null;
    /**
     * The ApplicantChildCareAndMaintenanceCosts
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ApplicantChildCareAndMaintenanceCosts = null;
    /**
     * The ApplicantOtherRegularMonthlyExpenditure
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ApplicantOtherRegularMonthlyExpenditure = null;
    /**
     * The ApplicantTotalMonthlyExpenditure
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ApplicantTotalMonthlyExpenditure = null;

    /**
     * Constructor method for ExpenditureSupplyMethod1
     *
     * @param string $applicantMonthlyHousingCosts
     * @param string $applicantMonthlyCreditCommitments
     * @param string $applicantMonthlyUtilitiesAndBills
     * @param string $applicantMonthlyTransportationAndTravelCosts
     * @param string $applicantMonthlyFoodCosts
     * @param string $applicantChildCareAndMaintenanceCosts
     * @param string $applicantOtherRegularMonthlyExpenditure
     * @param string $applicantTotalMonthlyExpenditure
     * @uses ExpenditureSupplyMethod1::setApplicantMonthlyHousingCosts()
     * @uses ExpenditureSupplyMethod1::setApplicantMonthlyCreditCommitments()
     * @uses ExpenditureSupplyMethod1::setApplicantMonthlyUtilitiesAndBills()
     * @uses ExpenditureSupplyMethod1::setApplicantMonthlyTransportationAndTravelCosts()
     * @uses ExpenditureSupplyMethod1::setApplicantMonthlyFoodCosts()
     * @uses ExpenditureSupplyMethod1::setApplicantChildCareAndMaintenanceCosts()
     * @uses ExpenditureSupplyMethod1::setApplicantOtherRegularMonthlyExpenditure()
     * @uses ExpenditureSupplyMethod1::setApplicantTotalMonthlyExpenditure()
     */
    public function __construct(
        ?string $applicantMonthlyHousingCosts = null,
        ?string $applicantMonthlyCreditCommitments = null,
        ?string $applicantMonthlyUtilitiesAndBills = null,
        ?string $applicantMonthlyTransportationAndTravelCosts = null,
        ?string $applicantMonthlyFoodCosts = null,
        ?string $applicantChildCareAndMaintenanceCosts = null,
        ?string $applicantOtherRegularMonthlyExpenditure = null,
        ?string $applicantTotalMonthlyExpenditure = null
    ) {
        $this
            ->setApplicantMonthlyHousingCosts($applicantMonthlyHousingCosts)
            ->setApplicantMonthlyCreditCommitments($applicantMonthlyCreditCommitments)
            ->setApplicantMonthlyUtilitiesAndBills($applicantMonthlyUtilitiesAndBills)
            ->setApplicantMonthlyTransportationAndTravelCosts($applicantMonthlyTransportationAndTravelCosts)
            ->setApplicantMonthlyFoodCosts($applicantMonthlyFoodCosts)
            ->setApplicantChildCareAndMaintenanceCosts($applicantChildCareAndMaintenanceCosts)
            ->setApplicantOtherRegularMonthlyExpenditure($applicantOtherRegularMonthlyExpenditure)
            ->setApplicantTotalMonthlyExpenditure($applicantTotalMonthlyExpenditure);
    }

    /**
     * Get ApplicantMonthlyHousingCosts value
     *
     * @return string|null
     */
    public function getApplicantMonthlyHousingCosts(): ?string
    {
        return $this->ApplicantMonthlyHousingCosts;
    }

    /**
     * Set ApplicantMonthlyHousingCosts value
     *
     * @param string $applicantMonthlyHousingCosts
     * @return ExpenditureSupplyMethod1
     */
    public function setApplicantMonthlyHousingCosts(?string $applicantMonthlyHousingCosts = null): self
    {
        // validation for constraint: string
        if (!is_null($applicantMonthlyHousingCosts) && !is_string($applicantMonthlyHousingCosts)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($applicantMonthlyHousingCosts, true),
                gettype($applicantMonthlyHousingCosts)
            ), __LINE__);
        }
        $this->ApplicantMonthlyHousingCosts = $applicantMonthlyHousingCosts;

        return $this;
    }

    /**
     * Get ApplicantMonthlyCreditCommitments value
     *
     * @return string|null
     */
    public function getApplicantMonthlyCreditCommitments(): ?string
    {
        return $this->ApplicantMonthlyCreditCommitments;
    }

    /**
     * Set ApplicantMonthlyCreditCommitments value
     *
     * @param string $applicantMonthlyCreditCommitments
     * @return ExpenditureSupplyMethod1
     */
    public function setApplicantMonthlyCreditCommitments(?string $applicantMonthlyCreditCommitments = null): self
    {
        // validation for constraint: string
        if (!is_null($applicantMonthlyCreditCommitments) && !is_string($applicantMonthlyCreditCommitments)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($applicantMonthlyCreditCommitments, true),
                gettype($applicantMonthlyCreditCommitments)
            ), __LINE__);
        }
        $this->ApplicantMonthlyCreditCommitments = $applicantMonthlyCreditCommitments;

        return $this;
    }

    /**
     * Get ApplicantMonthlyUtilitiesAndBills value
     *
     * @return string|null
     */
    public function getApplicantMonthlyUtilitiesAndBills(): ?string
    {
        return $this->ApplicantMonthlyUtilitiesAndBills;
    }

    /**
     * Set ApplicantMonthlyUtilitiesAndBills value
     *
     * @param string $applicantMonthlyUtilitiesAndBills
     * @return ExpenditureSupplyMethod1
     */
    public function setApplicantMonthlyUtilitiesAndBills(?string $applicantMonthlyUtilitiesAndBills = null): self
    {
        // validation for constraint: string
        if (!is_null($applicantMonthlyUtilitiesAndBills) && !is_string($applicantMonthlyUtilitiesAndBills)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($applicantMonthlyUtilitiesAndBills, true),
                gettype($applicantMonthlyUtilitiesAndBills)
            ), __LINE__);
        }
        $this->ApplicantMonthlyUtilitiesAndBills = $applicantMonthlyUtilitiesAndBills;

        return $this;
    }

    /**
     * Get ApplicantMonthlyTransportationAndTravelCosts value
     *
     * @return string|null
     */
    public function getApplicantMonthlyTransportationAndTravelCosts(): ?string
    {
        return $this->ApplicantMonthlyTransportationAndTravelCosts;
    }

    /**
     * Set ApplicantMonthlyTransportationAndTravelCosts value
     *
     * @param string $applicantMonthlyTransportationAndTravelCosts
     * @return ExpenditureSupplyMethod1
     */
    public function setApplicantMonthlyTransportationAndTravelCosts(?string $applicantMonthlyTransportationAndTravelCosts = null): self
    {
        // validation for constraint: string
        if (!is_null($applicantMonthlyTransportationAndTravelCosts) && !is_string($applicantMonthlyTransportationAndTravelCosts)) {
            throw new InvalidArgumentException(
                sprintf(
                    'Invalid value %s, please provide a string, %s given',
                    var_export($applicantMonthlyTransportationAndTravelCosts, true),
                    gettype($applicantMonthlyTransportationAndTravelCosts)
                ),
                __LINE__
            );
        }
        $this->ApplicantMonthlyTransportationAndTravelCosts = $applicantMonthlyTransportationAndTravelCosts;

        return $this;
    }

    /**
     * Get ApplicantMonthlyFoodCosts value
     *
     * @return string|null
     */
    public function getApplicantMonthlyFoodCosts(): ?string
    {
        return $this->ApplicantMonthlyFoodCosts;
    }

    /**
     * Set ApplicantMonthlyFoodCosts value
     *
     * @param string $applicantMonthlyFoodCosts
     * @return ExpenditureSupplyMethod1
     */
    public function setApplicantMonthlyFoodCosts(?string $applicantMonthlyFoodCosts = null): self
    {
        // validation for constraint: string
        if (!is_null($applicantMonthlyFoodCosts) && !is_string($applicantMonthlyFoodCosts)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($applicantMonthlyFoodCosts, true),
                gettype($applicantMonthlyFoodCosts)
            ), __LINE__);
        }
        $this->ApplicantMonthlyFoodCosts = $applicantMonthlyFoodCosts;

        return $this;
    }

    /**
     * Get ApplicantChildCareAndMaintenanceCosts value
     *
     * @return string|null
     */
    public function getApplicantChildCareAndMaintenanceCosts(): ?string
    {
        return $this->ApplicantChildCareAndMaintenanceCosts;
    }

    /**
     * Set ApplicantChildCareAndMaintenanceCosts value
     *
     * @param string $applicantChildCareAndMaintenanceCosts
     * @return ExpenditureSupplyMethod1
     */
    public function setApplicantChildCareAndMaintenanceCosts(?string $applicantChildCareAndMaintenanceCosts = null): self
    {
        // validation for constraint: string
        if (!is_null($applicantChildCareAndMaintenanceCosts) && !is_string($applicantChildCareAndMaintenanceCosts)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($applicantChildCareAndMaintenanceCosts, true),
                gettype($applicantChildCareAndMaintenanceCosts)
            ), __LINE__);
        }
        $this->ApplicantChildCareAndMaintenanceCosts = $applicantChildCareAndMaintenanceCosts;

        return $this;
    }

    /**
     * Get ApplicantOtherRegularMonthlyExpenditure value
     *
     * @return string|null
     */
    public function getApplicantOtherRegularMonthlyExpenditure(): ?string
    {
        return $this->ApplicantOtherRegularMonthlyExpenditure;
    }

    /**
     * Set ApplicantOtherRegularMonthlyExpenditure value
     *
     * @param string $applicantOtherRegularMonthlyExpenditure
     * @return ExpenditureSupplyMethod1
     */
    public function setApplicantOtherRegularMonthlyExpenditure(?string $applicantOtherRegularMonthlyExpenditure = null): self
    {
        // validation for constraint: string
        if (!is_null($applicantOtherRegularMonthlyExpenditure) && !is_string($applicantOtherRegularMonthlyExpenditure)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($applicantOtherRegularMonthlyExpenditure, true),
                gettype($applicantOtherRegularMonthlyExpenditure)
            ), __LINE__);
        }
        $this->ApplicantOtherRegularMonthlyExpenditure = $applicantOtherRegularMonthlyExpenditure;

        return $this;
    }

    /**
     * Get ApplicantTotalMonthlyExpenditure value
     *
     * @return string|null
     */
    public function getApplicantTotalMonthlyExpenditure(): ?string
    {
        return $this->ApplicantTotalMonthlyExpenditure;
    }

    /**
     * Set ApplicantTotalMonthlyExpenditure value
     *
     * @param string $applicantTotalMonthlyExpenditure
     * @return ExpenditureSupplyMethod1
     */
    public function setApplicantTotalMonthlyExpenditure(?string $applicantTotalMonthlyExpenditure = null): self
    {
        // validation for constraint: string
        if (!is_null($applicantTotalMonthlyExpenditure) && !is_string($applicantTotalMonthlyExpenditure)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($applicantTotalMonthlyExpenditure, true),
                gettype($applicantTotalMonthlyExpenditure)
            ), __LINE__);
        }
        $this->ApplicantTotalMonthlyExpenditure = $applicantTotalMonthlyExpenditure;

        return $this;
    }
}
