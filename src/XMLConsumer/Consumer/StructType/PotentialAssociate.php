<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for PotentialAssociate StructType
 * Meta information extracted from the WSDL
 * - documentation: Potential Associate data is associate data with a name match 'level' which indicates that the associate may be associate of the applicant.
 *
 * @subpackage Structs
 */
class PotentialAssociate extends NamedData
{
}
