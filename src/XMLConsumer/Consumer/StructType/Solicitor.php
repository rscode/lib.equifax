<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Solicitor StructType
 *
 * @subpackage Structs
 */
class Solicitor extends AbstractStructBase
{
    /**
     * The name
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 50
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $name;
    /**
     * The address
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var StructuredAddress|null
     */
    protected ?StructuredAddress $address = null;

    /**
     * Constructor method for Solicitor
     *
     * @param string            $name
     * @param StructuredAddress $address
     * @uses Solicitor::setName()
     * @uses Solicitor::setAddress()
     */
    public function __construct(string $name, ?StructuredAddress $address = null)
    {
        $this
            ->setName($name)
            ->setAddress($address);
    }

    /**
     * Get name value
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set name value
     *
     * @param string $name
     * @return Solicitor
     */
    public function setName(string $name): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($name, true),
                gettype($name)
            ), __LINE__);
        }
        // validation for constraint: maxLength(50)
        if (!is_null($name) && mb_strlen((string)$name) > 50) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 50',
                mb_strlen((string)$name)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($name) && mb_strlen((string)$name) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$name)
            ), __LINE__);
        }
        $this->name = $name;

        return $this;
    }

    /**
     * Get address value
     *
     * @return StructuredAddress|null
     */
    public function getAddress(): ?StructuredAddress
    {
        return $this->address;
    }

    /**
     * Set address value
     *
     * @param StructuredAddress $address
     * @return Solicitor
     */
    public function setAddress(?StructuredAddress $address = null): self
    {
        $this->address = $address;

        return $this;
    }
}
