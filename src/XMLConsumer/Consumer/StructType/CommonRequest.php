<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CommonRequest StructType
 *
 * @subpackage Structs
 */
class CommonRequest extends AbstractStructBase
{
    /**
     * The clientRef
     * Meta information extracted from the WSDL
     * - documentation: Used to supply an enquiry reference identifier. | The value supplied in the request is simply echoed back in the corresponding response.
     * - base: xs:string
     * - maxLength: 40
     * - maxOccurs: 1
     * - minLength: 1
     * - minOccurs: 1
     * - pattern: .*[^\s].*
     *
     * @var string
     */
    protected string $clientRef;
    /**
     * The storeIdentity
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 25
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $storeIdentity = null;
    protected ?RequestJointSearch $jointSearch = null;
    protected ?RequestSoleSearch $soleSearch = null;

    public function __construct(
        string $clientRef,
        ?string $storeIdentity = null,
        ?RequestJointSearch $jointSearch = null,
        ?RequestSoleSearch $soleSearch = null
    ) {
        $this
            ->setClientRef($clientRef)
            ->setStoreIdentity($storeIdentity)
            ->setJointSearch($jointSearch)
            ->setSoleSearch($soleSearch);
    }

    public function getClientRef(): string
    {
        return $this->clientRef;
    }

    public function setClientRef(string $clientRef): self
    {
        // validation for constraint: string
        if (!is_null($clientRef) && !is_string($clientRef)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($clientRef, true),
                gettype($clientRef)
            ), __LINE__);
        }
        // validation for constraint: maxLength(40)
        if (!is_null($clientRef) && mb_strlen((string)$clientRef) > 40) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 40',
                mb_strlen((string)$clientRef)
            ), __LINE__);
        }
        // validation for constraint: minLength(1)
        if (!is_null($clientRef) && mb_strlen((string)$clientRef) < 1) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 1',
                mb_strlen((string)$clientRef)
            ), __LINE__);
        }
        // validation for constraint: pattern(.*[^\s].*)
        if (!is_null($clientRef) && !preg_match('/.*[^\\s].*/', $clientRef)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /.*[^\\s].*/',
                var_export($clientRef, true)
            ), __LINE__);
        }
        $this->clientRef = $clientRef;

        return $this;
    }

    public function getStoreIdentity(): ?string
    {
        return $this->storeIdentity;
    }

    public function setStoreIdentity(?string $storeIdentity = null): self
    {
        // validation for constraint: string
        if (!is_null($storeIdentity) && !is_string($storeIdentity)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($storeIdentity, true),
                gettype($storeIdentity)
            ), __LINE__);
        }
        // validation for constraint: maxLength(25)
        if (!is_null($storeIdentity) && mb_strlen((string)$storeIdentity) > 25) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 25',
                mb_strlen((string)$storeIdentity)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($storeIdentity) && mb_strlen((string)$storeIdentity) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$storeIdentity)
            ), __LINE__);
        }
        $this->storeIdentity = $storeIdentity;

        return $this;
    }

    public function getJointSearch(): ?RequestJointSearch
    {
        return $this->jointSearch;
    }

    public function setJointSearch(?RequestJointSearch $jointSearch = null): self
    {
        $this->jointSearch = $jointSearch;

        return $this;
    }

    public function getSoleSearch(): ?RequestSoleSearch
    {
        return $this->soleSearch;
    }

    public function setSoleSearch(?RequestSoleSearch $soleSearch = null): self
    {
        $this->soleSearch = $soleSearch;

        return $this;
    }
}
