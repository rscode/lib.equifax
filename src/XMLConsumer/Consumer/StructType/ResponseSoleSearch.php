<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ResponseSoleSearch StructType
 *
 * @subpackage Structs
 */
class ResponseSoleSearch extends AbstractStructBase
{
    /**
     * The primary
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var ResponsePerson|null
     */
    protected ?ResponsePerson $primary = null;

    /**
     * Constructor method for ResponseSoleSearch
     *
     * @param ResponsePerson $primary
     * @uses ResponseSoleSearch::setPrimary()
     */
    public function __construct(?ResponsePerson $primary = null)
    {
        $this
            ->setPrimary($primary);
    }

    /**
     * Get primary value
     *
     * @return ResponsePerson|null
     */
    public function getPrimary(): ?ResponsePerson
    {
        return $this->primary;
    }

    /**
     * Set primary value
     *
     * @param ResponsePerson $primary
     * @return ResponseSoleSearch
     */
    public function setPrimary(?ResponsePerson $primary = null): self
    {
        $this->primary = $primary;

        return $this;
    }
}
