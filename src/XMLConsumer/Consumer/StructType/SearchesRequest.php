<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for SearchesRequest StructType
 *
 * @subpackage Structs
 */
class SearchesRequest extends RawDataAvailableAtLinkedAddressRequest
{
}
