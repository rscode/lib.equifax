<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\AgeVerificationStatus;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\DOBVerificationStatus;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\TimeAtAddressVerificationStatus;

/**
 * This class stands for BureauVerification StructType
 * Meta information extracted from the WSDL
 * - documentation: The Bureau Verification check returns date of birth information relating to the first applicant from Equifax bureau sources, comparing Equifax-held data to the supplied date of birth.
 *
 * @subpackage Structs
 */
class BureauVerification extends DataItem
{
    /**
     * The ageVerificationStatus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $ageVerificationStatus;
    /**
     * The electoralRollDOBverificationStatus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $electoralRollDOBverificationStatus;
    /**
     * The idNumber
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 40
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $idNumber;
    /**
     * The insightDOBVerificationStatus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $insightDOBVerificationStatus;
    /**
     * The timeAtAddressVerificationStatus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $timeAtAddressVerificationStatus;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for BureauVerification
     *
     * @param string $ageVerificationStatus
     * @param string $electoralRollDOBverificationStatus
     * @param string $idNumber
     * @param string $insightDOBVerificationStatus
     * @param string $timeAtAddressVerificationStatus
     * @param DOMDocument|string|null $any
     * @uses BureauVerification::setAgeVerificationStatus()
     * @uses BureauVerification::setElectoralRollDOBverificationStatus()
     * @uses BureauVerification::setIdNumber()
     * @uses BureauVerification::setInsightDOBVerificationStatus()
     * @uses BureauVerification::setTimeAtAddressVerificationStatus()
     * @uses BureauVerification::setAny()
     */
    public function __construct(
        string $ageVerificationStatus,
        string $electoralRollDOBverificationStatus,
        string $idNumber,
        string $insightDOBVerificationStatus,
        string $timeAtAddressVerificationStatus,
        $any = null
    ) {
        $this
            ->setAgeVerificationStatus($ageVerificationStatus)
            ->setElectoralRollDOBverificationStatus($electoralRollDOBverificationStatus)
            ->setIdNumber($idNumber)
            ->setInsightDOBVerificationStatus($insightDOBVerificationStatus)
            ->setTimeAtAddressVerificationStatus($timeAtAddressVerificationStatus)
            ->setAny($any);
    }

    /**
     * Get ageVerificationStatus value
     *
     * @return string
     */
    public function getAgeVerificationStatus(): string
    {
        return $this->ageVerificationStatus;
    }

    /**
     * Set ageVerificationStatus value
     *
     * @param string $ageVerificationStatus
     * @return BureauVerification
     * @throws InvalidArgumentException
     * @uses AgeVerificationStatus::getValidValues
     * @uses AgeVerificationStatus::valueIsValid
     */
    public function setAgeVerificationStatus(string $ageVerificationStatus): self
    {
        // validation for constraint: enumeration
        if (!AgeVerificationStatus::valueIsValid($ageVerificationStatus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\AgeVerificationStatus',
                is_array($ageVerificationStatus) ? implode(', ', $ageVerificationStatus) : var_export($ageVerificationStatus, true),
                implode(', ', AgeVerificationStatus::getValidValues())
            ), __LINE__);
        }
        $this->ageVerificationStatus = $ageVerificationStatus;

        return $this;
    }

    /**
     * Get electoralRollDOBverificationStatus value
     *
     * @return string
     */
    public function getElectoralRollDOBverificationStatus(): string
    {
        return $this->electoralRollDOBverificationStatus;
    }

    /**
     * Set electoralRollDOBverificationStatus value
     *
     * @param string $electoralRollDOBverificationStatus
     * @return BureauVerification
     * @throws InvalidArgumentException
     * @uses DOBVerificationStatus::getValidValues
     * @uses DOBVerificationStatus::valueIsValid
     */
    public function setElectoralRollDOBverificationStatus(string $electoralRollDOBverificationStatus): self
    {
        // validation for constraint: enumeration
        if (!DOBVerificationStatus::valueIsValid($electoralRollDOBverificationStatus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\DOBVerificationStatus',
                is_array($electoralRollDOBverificationStatus) ? implode(
                    ', ',
                    $electoralRollDOBverificationStatus
                ) : var_export($electoralRollDOBverificationStatus, true),
                implode(', ', DOBVerificationStatus::getValidValues())
            ), __LINE__);
        }
        $this->electoralRollDOBverificationStatus = $electoralRollDOBverificationStatus;

        return $this;
    }

    /**
     * Get idNumber value
     *
     * @return string
     */
    public function getIdNumber(): string
    {
        return $this->idNumber;
    }

    /**
     * Set idNumber value
     *
     * @param string $idNumber
     * @return BureauVerification
     */
    public function setIdNumber(string $idNumber): self
    {
        // validation for constraint: string
        if (!is_null($idNumber) && !is_string($idNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($idNumber, true),
                gettype($idNumber)
            ), __LINE__);
        }
        // validation for constraint: maxLength(40)
        if (!is_null($idNumber) && mb_strlen((string)$idNumber) > 40) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 40',
                mb_strlen((string)$idNumber)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($idNumber) && mb_strlen((string)$idNumber) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$idNumber)
            ), __LINE__);
        }
        $this->idNumber = $idNumber;

        return $this;
    }

    /**
     * Get insightDOBVerificationStatus value
     *
     * @return string
     */
    public function getInsightDOBVerificationStatus(): string
    {
        return $this->insightDOBVerificationStatus;
    }

    /**
     * Set insightDOBVerificationStatus value
     *
     * @param string $insightDOBVerificationStatus
     * @return BureauVerification
     * @throws InvalidArgumentException
     * @uses DOBVerificationStatus::getValidValues
     * @uses DOBVerificationStatus::valueIsValid
     */
    public function setInsightDOBVerificationStatus(string $insightDOBVerificationStatus): self
    {
        // validation for constraint: enumeration
        if (!DOBVerificationStatus::valueIsValid($insightDOBVerificationStatus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\DOBVerificationStatus',
                is_array($insightDOBVerificationStatus) ? implode(
                    ', ',
                    $insightDOBVerificationStatus
                ) : var_export($insightDOBVerificationStatus, true),
                implode(', ', DOBVerificationStatus::getValidValues())
            ), __LINE__);
        }
        $this->insightDOBVerificationStatus = $insightDOBVerificationStatus;

        return $this;
    }

    /**
     * Get timeAtAddressVerificationStatus value
     *
     * @return string
     */
    public function getTimeAtAddressVerificationStatus(): string
    {
        return $this->timeAtAddressVerificationStatus;
    }

    /**
     * Set timeAtAddressVerificationStatus value
     *
     * @param string $timeAtAddressVerificationStatus
     * @return BureauVerification
     * @throws InvalidArgumentException
     * @uses TimeAtAddressVerificationStatus::getValidValues
     * @uses TimeAtAddressVerificationStatus::valueIsValid
     */
    public function setTimeAtAddressVerificationStatus(string $timeAtAddressVerificationStatus): self
    {
        // validation for constraint: enumeration
        if (!TimeAtAddressVerificationStatus::valueIsValid($timeAtAddressVerificationStatus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\TimeAtAddressVerificationStatus',
                is_array($timeAtAddressVerificationStatus) ? implode(
                    ', ',
                    $timeAtAddressVerificationStatus
                ) : var_export($timeAtAddressVerificationStatus, true),
                implode(', ', TimeAtAddressVerificationStatus::getValidValues())
            ), __LINE__);
        }
        $this->timeAtAddressVerificationStatus = $timeAtAddressVerificationStatus;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return BureauVerification
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
