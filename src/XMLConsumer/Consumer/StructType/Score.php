<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;

/**
 * This class stands for Score StructType
 *
 * @subpackage Structs
 */
class Score extends DataItem
{
    /**
     * The scoreLabel
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 7
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $scoreLabel;
    /**
     * The positive
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var bool
     */
    protected bool $positive;
    /**
     * The value
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 9999
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $value;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for Score
     *
     * @param string                  $scoreLabel
     * @param bool                    $positive
     * @param int                     $value
     * @param DOMDocument|string|null $any
     * @uses Score::setScoreLabel()
     * @uses Score::setPositive()
     * @uses Score::setValue()
     * @uses Score::setAny()
     */
    public function __construct(string $scoreLabel, bool $positive, int $value, $any = null)
    {
        $this
            ->setScoreLabel($scoreLabel)
            ->setPositive($positive)
            ->setValue($value)
            ->setAny($any);
    }

    /**
     * Get scoreLabel value
     *
     * @return string
     */
    public function getScoreLabel(): string
    {
        return $this->scoreLabel;
    }

    /**
     * Set scoreLabel value
     *
     * @param string $scoreLabel
     * @return Score
     */
    public function setScoreLabel(string $scoreLabel): self
    {
        // validation for constraint: string
        if (!is_null($scoreLabel) && !is_string($scoreLabel)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($scoreLabel, true),
                gettype($scoreLabel)
            ), __LINE__);
        }
        // validation for constraint: maxLength(7)
        if (!is_null($scoreLabel) && mb_strlen((string)$scoreLabel) > 7) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 7',
                mb_strlen((string)$scoreLabel)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($scoreLabel) && mb_strlen((string)$scoreLabel) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$scoreLabel)
            ), __LINE__);
        }
        $this->scoreLabel = $scoreLabel;

        return $this;
    }

    /**
     * Get positive value
     *
     * @return bool
     */
    public function getPositive(): bool
    {
        return $this->positive;
    }

    /**
     * Set positive value
     *
     * @param bool $positive
     * @return Score
     */
    public function setPositive(bool $positive): self
    {
        // validation for constraint: boolean
        if (!is_null($positive) && !is_bool($positive)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a bool, %s given',
                var_export($positive, true),
                gettype($positive)
            ), __LINE__);
        }
        $this->positive = $positive;

        return $this;
    }

    /**
     * Get value value
     *
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * Set value value
     *
     * @param int $value
     * @return Score
     */
    public function setValue(int $value): self
    {
        // validation for constraint: int
        if (!is_null($value) && !(is_int($value) || ctype_digit($value))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($value, true),
                gettype($value)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(9999)
        if (!is_null($value) && $value > 9999) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 9999',
                var_export($value, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($value) && $value < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($value, true)
            ), __LINE__);
        }
        $this->value = $value;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return Score
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
