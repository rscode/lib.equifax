<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Bank StructType
 * Meta information extracted from the WSDL
 * - documentation: This type is used to hold details related to a bank.
 *
 * @subpackage Structs
 */
class Bank extends AbstractStructBase
{
    /**
     * The name
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $name;
    /**
     * The address
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var FreeFormatAddress|null
     */
    protected ?FreeFormatAddress $address = null;

    /**
     * Constructor method for Bank
     *
     * @param string            $name
     * @param FreeFormatAddress $address
     * @uses Bank::setName()
     * @uses Bank::setAddress()
     */
    public function __construct(string $name, ?FreeFormatAddress $address = null)
    {
        $this
            ->setName($name)
            ->setAddress($address);
    }

    /**
     * Get name value
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set name value
     *
     * @param string $name
     * @return Bank
     */
    public function setName(string $name): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($name, true),
                gettype($name)
            ), __LINE__);
        }
        $this->name = $name;

        return $this;
    }

    /**
     * Get address value
     *
     * @return FreeFormatAddress|null
     */
    public function getAddress(): ?FreeFormatAddress
    {
        return $this->address;
    }

    /**
     * Set address value
     *
     * @param FreeFormatAddress $address
     * @return Bank
     */
    public function setAddress(?FreeFormatAddress $address = null): self
    {
        $this->address = $address;

        return $this;
    }
}
