<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for GreenDeals StructType
 *
 * @subpackage Structs
 */
class GreenDeals extends FinancialAgreement
{
}
