<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for WatchlistEntry StructType
 *
 * @subpackage Structs
 */
class WatchlistEntry extends AbstractStructBase
{
    /**
     * The watchlistCheck
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 1
     *
     * @var WatchlistCheck[]
     */
    protected array $watchlistCheck;
    /**
     * The transactionReference
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $transactionReference;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The dob
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $dob = null;
    /**
     * The gender
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $gender = null;
    /**
     * The jurisdiction
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $jurisdiction = null;
    /**
     * The citizenship
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $citizenship = null;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for WatchlistEntry
     *
     * @param WatchlistCheck[]        $watchlistCheck
     * @param string                  $transactionReference
     * @param string                  $name
     * @param string                  $dob
     * @param string                  $gender
     * @param string                  $jurisdiction
     * @param string                  $citizenship
     * @param DOMDocument|string|null $any
     * @uses WatchlistEntry::setWatchlistCheck()
     * @uses WatchlistEntry::setTransactionReference()
     * @uses WatchlistEntry::setName()
     * @uses WatchlistEntry::setDob()
     * @uses WatchlistEntry::setGender()
     * @uses WatchlistEntry::setJurisdiction()
     * @uses WatchlistEntry::setCitizenship()
     * @uses WatchlistEntry::setAny()
     */
    public function __construct(
        array $watchlistCheck,
        string $transactionReference,
        ?string $name = null,
        ?string $dob = null,
        ?string $gender = null,
        ?string $jurisdiction = null,
        ?string $citizenship = null,
        $any = null
    ) {
        $this
            ->setWatchlistCheck($watchlistCheck)
            ->setTransactionReference($transactionReference)
            ->setName($name)
            ->setDob($dob)
            ->setGender($gender)
            ->setJurisdiction($jurisdiction)
            ->setCitizenship($citizenship)
            ->setAny($any);
    }

    /**
     * This method is responsible for validating the values passed to the setWatchlistCheck method
     * This method is willingly generated in order to preserve the one-line inline validation within the setWatchlistCheck method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateWatchlistCheckForArrayConstraintsFromSetWatchlistCheck(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $watchlistEntryWatchlistCheckItem) {
            // validation for constraint: itemType
            if (!$watchlistEntryWatchlistCheckItem instanceof WatchlistCheck) {
                $invalidValues[] = is_object($watchlistEntryWatchlistCheckItem) ? get_class($watchlistEntryWatchlistCheckItem) : sprintf(
                    '%s(%s)',
                    gettype($watchlistEntryWatchlistCheckItem),
                    var_export($watchlistEntryWatchlistCheckItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The watchlistCheck property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\WatchlistCheck, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get watchlistCheck value
     *
     * @return WatchlistCheck[]
     */
    public function getWatchlistCheck(): array
    {
        return $this->watchlistCheck;
    }

    /**
     * Set watchlistCheck value
     *
     * @param WatchlistCheck[] $watchlistCheck
     * @return WatchlistEntry
     * @throws InvalidArgumentException
     */
    public function setWatchlistCheck(array $watchlistCheck): self
    {
        // validation for constraint: array
        if ('' !== ($watchlistCheckArrayErrorMessage = self::validateWatchlistCheckForArrayConstraintsFromSetWatchlistCheck($watchlistCheck))) {
            throw new InvalidArgumentException($watchlistCheckArrayErrorMessage, __LINE__);
        }
        $this->watchlistCheck = $watchlistCheck;

        return $this;
    }

    /**
     * Add item to watchlistCheck value
     *
     * @param WatchlistCheck $item
     * @return WatchlistEntry
     * @throws InvalidArgumentException
     */
    public function addToWatchlistCheck(WatchlistCheck $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof WatchlistCheck) {
            throw new InvalidArgumentException(sprintf(
                'The watchlistCheck property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\WatchlistCheck, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        $this->watchlistCheck[] = $item;

        return $this;
    }

    /**
     * Get transactionReference value
     *
     * @return string
     */
    public function getTransactionReference(): string
    {
        return $this->transactionReference;
    }

    /**
     * Set transactionReference value
     *
     * @param string $transactionReference
     * @return WatchlistEntry
     */
    public function setTransactionReference(string $transactionReference): self
    {
        // validation for constraint: string
        if (!is_null($transactionReference) && !is_string($transactionReference)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($transactionReference, true),
                gettype($transactionReference)
            ), __LINE__);
        }
        $this->transactionReference = $transactionReference;

        return $this;
    }

    /**
     * Get name value
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set name value
     *
     * @param string $name
     * @return WatchlistEntry
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($name, true),
                gettype($name)
            ), __LINE__);
        }
        $this->name = $name;

        return $this;
    }

    /**
     * Get dob value
     *
     * @return string|null
     */
    public function getDob(): ?string
    {
        return $this->dob;
    }

    /**
     * Set dob value
     *
     * @param string $dob
     * @return WatchlistEntry
     */
    public function setDob(?string $dob = null): self
    {
        // validation for constraint: string
        if (!is_null($dob) && !is_string($dob)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($dob, true),
                gettype($dob)
            ), __LINE__);
        }
        $this->dob = $dob;

        return $this;
    }

    /**
     * Get gender value
     *
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * Set gender value
     *
     * @param string $gender
     * @return WatchlistEntry
     */
    public function setGender(?string $gender = null): self
    {
        // validation for constraint: string
        if (!is_null($gender) && !is_string($gender)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($gender, true),
                gettype($gender)
            ), __LINE__);
        }
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get jurisdiction value
     *
     * @return string|null
     */
    public function getJurisdiction(): ?string
    {
        return $this->jurisdiction;
    }

    /**
     * Set jurisdiction value
     *
     * @param string $jurisdiction
     * @return WatchlistEntry
     */
    public function setJurisdiction(?string $jurisdiction = null): self
    {
        // validation for constraint: string
        if (!is_null($jurisdiction) && !is_string($jurisdiction)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($jurisdiction, true),
                gettype($jurisdiction)
            ), __LINE__);
        }
        $this->jurisdiction = $jurisdiction;

        return $this;
    }

    /**
     * Get citizenship value
     *
     * @return string|null
     */
    public function getCitizenship(): ?string
    {
        return $this->citizenship;
    }

    /**
     * Set citizenship value
     *
     * @param string $citizenship
     * @return WatchlistEntry
     */
    public function setCitizenship(?string $citizenship = null): self
    {
        // validation for constraint: string
        if (!is_null($citizenship) && !is_string($citizenship)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($citizenship, true),
                gettype($citizenship)
            ), __LINE__);
        }
        $this->citizenship = $citizenship;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return WatchlistEntry
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
