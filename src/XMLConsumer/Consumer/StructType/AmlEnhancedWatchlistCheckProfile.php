<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\AmlEnhancedWatchlistProfile;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AmlEnhancedWatchlistCheckProfile StructType
 *
 * @subpackage Structs
 */
class AmlEnhancedWatchlistCheckProfile extends AbstractStructBase
{
    /**
     * The profile
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $profile;
    /**
     * The index
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $index = null;

    /**
     * Constructor method for AmlEnhancedWatchlistCheckProfile
     *
     * @param string $profile
     * @param string $index
     * @uses AmlEnhancedWatchlistCheckProfile::setProfile()
     * @uses AmlEnhancedWatchlistCheckProfile::setIndex()
     */
    public function __construct(string $profile, ?string $index = null)
    {
        $this
            ->setProfile($profile)
            ->setIndex($index);
    }

    /**
     * Get profile value
     *
     * @return string
     */
    public function getProfile(): string
    {
        return $this->profile;
    }

    /**
     * Set profile value
     *
     * @param string $profile
     * @return AmlEnhancedWatchlistCheckProfile
     * @throws InvalidArgumentException
     * @uses AmlEnhancedWatchlistProfile::getValidValues
     * @uses AmlEnhancedWatchlistProfile::valueIsValid
     */
    public function setProfile(string $profile): self
    {
        // validation for constraint: enumeration
        if (!AmlEnhancedWatchlistProfile::valueIsValid($profile)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\AmlEnhancedWatchlistProfile',
                is_array($profile) ? implode(', ', $profile) : var_export($profile, true),
                implode(', ', AmlEnhancedWatchlistProfile::getValidValues())
            ), __LINE__);
        }
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get index value
     *
     * @return string|null
     */
    public function getIndex(): ?string
    {
        return $this->index;
    }

    /**
     * Set index value
     *
     * @param string $index
     * @return AmlEnhancedWatchlistCheckProfile
     */
    public function setIndex(?string $index = null): self
    {
        // validation for constraint: string
        if (!is_null($index) && !is_string($index)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($index, true),
                gettype($index)
            ), __LINE__);
        }
        $this->index = $index;

        return $this;
    }
}
