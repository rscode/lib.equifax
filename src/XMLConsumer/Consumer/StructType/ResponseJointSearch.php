<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ResponseJointSearch StructType
 *
 * @subpackage Structs
 */
class ResponseJointSearch extends AbstractStructBase
{
    /**
     * The primary
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var ResponsePerson
     */
    protected ResponsePerson $primary;
    /**
     * The secondary
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var ResponsePerson|null
     */
    protected ?ResponsePerson $secondary = null;

    /**
     * Constructor method for ResponseJointSearch
     *
     * @param ResponsePerson $primary
     * @param ResponsePerson $secondary
     * @uses  ResponseJointSearch::setPrimary()
     * @uses  ResponseJointSearch::setSecondary()
     */
    public function __construct(ResponsePerson $primary, ?ResponsePerson $secondary = null)
    {
        $this
            ->setPrimary($primary)
            ->setSecondary($secondary);
    }

    /**
     * Get primary value
     *
     * @return ResponsePerson
     */
    public function getPrimary(): ResponsePerson
    {
        return $this->primary;
    }

    /**
     * Set primary value
     *
     * @param ResponsePerson $primary
     * @return ResponseJointSearch
     */
    public function setPrimary(ResponsePerson $primary): self
    {
        $this->primary = $primary;

        return $this;
    }

    /**
     * Get secondary value
     *
     * @return ResponsePerson|null
     */
    public function getSecondary(): ?ResponsePerson
    {
        return $this->secondary;
    }

    /**
     * Set secondary value
     *
     * @param ResponsePerson $secondary
     * @return ResponseJointSearch
     */
    public function setSecondary(?ResponsePerson $secondary = null): self
    {
        $this->secondary = $secondary;

        return $this;
    }
}
