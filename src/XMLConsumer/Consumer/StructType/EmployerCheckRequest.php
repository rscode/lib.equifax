<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for EmployerCheckRequest StructType
 *
 * @subpackage Structs
 */
class EmployerCheckRequest extends CodedDataRequest
{
}
