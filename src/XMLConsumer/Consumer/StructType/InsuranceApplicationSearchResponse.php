<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for InsuranceApplicationSearchResponse StructType
 * Meta information extracted from the WSDL
 * - documentation: Insurance Application
 *
 * @subpackage Structs
 */
class InsuranceApplicationSearchResponse extends CommonResponse
{
}
