<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\CompanyGroupClass;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\CompanyGroupType;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CompanyGroup StructType
 *
 * @subpackage Structs
 */
class CompanyGroup extends AbstractStructBase
{
    /**
     * The companyGroupClasses
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $companyGroupClasses;
    /**
     * The companyGroupType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $companyGroupType;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for CompanyGroup
     *
     * @param string                  $companyGroupClasses
     * @param string                  $companyGroupType
     * @param DOMDocument|string|null $any
     * @uses CompanyGroup::setCompanyGroupClasses()
     * @uses CompanyGroup::setCompanyGroupType()
     * @uses CompanyGroup::setAny()
     */
    public function __construct(string $companyGroupClasses, string $companyGroupType, $any = null)
    {
        $this
            ->setCompanyGroupClasses($companyGroupClasses)
            ->setCompanyGroupType($companyGroupType)
            ->setAny($any);
    }

    /**
     * Get companyGroupClasses value
     *
     * @return string
     */
    public function getCompanyGroupClasses(): string
    {
        return $this->companyGroupClasses;
    }

    /**
     * Set companyGroupClasses value
     *
     * @param string $companyGroupClasses
     * @return CompanyGroup
     * @throws InvalidArgumentException
     * @uses CompanyGroupClass::getValidValues
     * @uses CompanyGroupClass::valueIsValid
     */
    public function setCompanyGroupClasses(string $companyGroupClasses): self
    {
        // validation for constraint: enumeration
        if (!CompanyGroupClass::valueIsValid($companyGroupClasses)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\CompanyGroupClass',
                is_array($companyGroupClasses) ? implode(', ', $companyGroupClasses) : var_export($companyGroupClasses, true),
                implode(', ', CompanyGroupClass::getValidValues())
            ), __LINE__);
        }
        $this->companyGroupClasses = $companyGroupClasses;

        return $this;
    }

    /**
     * Get companyGroupType value
     *
     * @return string
     */
    public function getCompanyGroupType(): string
    {
        return $this->companyGroupType;
    }

    /**
     * Set companyGroupType value
     *
     * @param string $companyGroupType
     * @return CompanyGroup
     * @throws InvalidArgumentException
     * @uses CompanyGroupType::getValidValues
     * @uses CompanyGroupType::valueIsValid
     */
    public function setCompanyGroupType(string $companyGroupType): self
    {
        // validation for constraint: enumeration
        if (!CompanyGroupType::valueIsValid($companyGroupType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\CompanyGroupType',
                is_array($companyGroupType) ? implode(', ', $companyGroupType) : var_export($companyGroupType, true),
                implode(', ', CompanyGroupType::getValidValues())
            ), __LINE__);
        }
        $this->companyGroupType = $companyGroupType;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return CompanyGroup
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
