<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GrossAnnualIncomeFields StructType
 *
 * @subpackage Structs
 */
class GrossAnnualIncomeFields extends AbstractStructBase
{
    /**
     * The ApplicantTotalGrossAnnualIncome
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ApplicantTotalGrossAnnualIncome = null;
    /**
     * The ApplicantGrossAnnualSalary
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ApplicantGrossAnnualSalary = null;
    /**
     * The ApplicantGrossAnnualBonus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ApplicantGrossAnnualBonus = null;
    /**
     * The ApplicantGrossAnnualSpousalIncome
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ApplicantGrossAnnualSpousalIncome = null;
    /**
     * The ApplicantGrossAnnualOtherIncome
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ApplicantGrossAnnualOtherIncome = null;
    /**
     * The HouseholdTotalGrossAnnualIncome
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $HouseholdTotalGrossAnnualIncome = null;

    /**
     * Constructor method for GrossAnnualIncomeFields
     *
     * @param string $applicantTotalGrossAnnualIncome
     * @param string $applicantGrossAnnualSalary
     * @param string $applicantGrossAnnualBonus
     * @param string $applicantGrossAnnualSpousalIncome
     * @param string $applicantGrossAnnualOtherIncome
     * @param string $householdTotalGrossAnnualIncome
     * @uses GrossAnnualIncomeFields::setApplicantTotalGrossAnnualIncome()
     * @uses GrossAnnualIncomeFields::setApplicantGrossAnnualSalary()
     * @uses GrossAnnualIncomeFields::setApplicantGrossAnnualBonus()
     * @uses GrossAnnualIncomeFields::setApplicantGrossAnnualSpousalIncome()
     * @uses GrossAnnualIncomeFields::setApplicantGrossAnnualOtherIncome()
     * @uses GrossAnnualIncomeFields::setHouseholdTotalGrossAnnualIncome()
     */
    public function __construct(
        ?string $applicantTotalGrossAnnualIncome = null,
        ?string $applicantGrossAnnualSalary = null,
        ?string $applicantGrossAnnualBonus = null,
        ?string $applicantGrossAnnualSpousalIncome = null,
        ?string $applicantGrossAnnualOtherIncome = null,
        ?string $householdTotalGrossAnnualIncome = null
    ) {
        $this
            ->setApplicantTotalGrossAnnualIncome($applicantTotalGrossAnnualIncome)
            ->setApplicantGrossAnnualSalary($applicantGrossAnnualSalary)
            ->setApplicantGrossAnnualBonus($applicantGrossAnnualBonus)
            ->setApplicantGrossAnnualSpousalIncome($applicantGrossAnnualSpousalIncome)
            ->setApplicantGrossAnnualOtherIncome($applicantGrossAnnualOtherIncome)
            ->setHouseholdTotalGrossAnnualIncome($householdTotalGrossAnnualIncome);
    }

    /**
     * Get ApplicantTotalGrossAnnualIncome value
     *
     * @return string|null
     */
    public function getApplicantTotalGrossAnnualIncome(): ?string
    {
        return $this->ApplicantTotalGrossAnnualIncome;
    }

    /**
     * Set ApplicantTotalGrossAnnualIncome value
     *
     * @param string $applicantTotalGrossAnnualIncome
     * @return GrossAnnualIncomeFields
     */
    public function setApplicantTotalGrossAnnualIncome(?string $applicantTotalGrossAnnualIncome = null): self
    {
        // validation for constraint: string
        if (!is_null($applicantTotalGrossAnnualIncome) && !is_string($applicantTotalGrossAnnualIncome)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($applicantTotalGrossAnnualIncome, true),
                gettype($applicantTotalGrossAnnualIncome)
            ), __LINE__);
        }
        $this->ApplicantTotalGrossAnnualIncome = $applicantTotalGrossAnnualIncome;

        return $this;
    }

    /**
     * Get ApplicantGrossAnnualSalary value
     *
     * @return string|null
     */
    public function getApplicantGrossAnnualSalary(): ?string
    {
        return $this->ApplicantGrossAnnualSalary;
    }

    /**
     * Set ApplicantGrossAnnualSalary value
     *
     * @param string $applicantGrossAnnualSalary
     * @return GrossAnnualIncomeFields
     */
    public function setApplicantGrossAnnualSalary(?string $applicantGrossAnnualSalary = null): self
    {
        // validation for constraint: string
        if (!is_null($applicantGrossAnnualSalary) && !is_string($applicantGrossAnnualSalary)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($applicantGrossAnnualSalary, true),
                gettype($applicantGrossAnnualSalary)
            ), __LINE__);
        }
        $this->ApplicantGrossAnnualSalary = $applicantGrossAnnualSalary;

        return $this;
    }

    /**
     * Get ApplicantGrossAnnualBonus value
     *
     * @return string|null
     */
    public function getApplicantGrossAnnualBonus(): ?string
    {
        return $this->ApplicantGrossAnnualBonus;
    }

    /**
     * Set ApplicantGrossAnnualBonus value
     *
     * @param string $applicantGrossAnnualBonus
     * @return GrossAnnualIncomeFields
     */
    public function setApplicantGrossAnnualBonus(?string $applicantGrossAnnualBonus = null): self
    {
        // validation for constraint: string
        if (!is_null($applicantGrossAnnualBonus) && !is_string($applicantGrossAnnualBonus)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($applicantGrossAnnualBonus, true),
                gettype($applicantGrossAnnualBonus)
            ), __LINE__);
        }
        $this->ApplicantGrossAnnualBonus = $applicantGrossAnnualBonus;

        return $this;
    }

    /**
     * Get ApplicantGrossAnnualSpousalIncome value
     *
     * @return string|null
     */
    public function getApplicantGrossAnnualSpousalIncome(): ?string
    {
        return $this->ApplicantGrossAnnualSpousalIncome;
    }

    /**
     * Set ApplicantGrossAnnualSpousalIncome value
     *
     * @param string $applicantGrossAnnualSpousalIncome
     * @return GrossAnnualIncomeFields
     */
    public function setApplicantGrossAnnualSpousalIncome(?string $applicantGrossAnnualSpousalIncome = null): self
    {
        // validation for constraint: string
        if (!is_null($applicantGrossAnnualSpousalIncome) && !is_string($applicantGrossAnnualSpousalIncome)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($applicantGrossAnnualSpousalIncome, true),
                gettype($applicantGrossAnnualSpousalIncome)
            ), __LINE__);
        }
        $this->ApplicantGrossAnnualSpousalIncome = $applicantGrossAnnualSpousalIncome;

        return $this;
    }

    /**
     * Get ApplicantGrossAnnualOtherIncome value
     *
     * @return string|null
     */
    public function getApplicantGrossAnnualOtherIncome(): ?string
    {
        return $this->ApplicantGrossAnnualOtherIncome;
    }

    /**
     * Set ApplicantGrossAnnualOtherIncome value
     *
     * @param string $applicantGrossAnnualOtherIncome
     * @return GrossAnnualIncomeFields
     */
    public function setApplicantGrossAnnualOtherIncome(?string $applicantGrossAnnualOtherIncome = null): self
    {
        // validation for constraint: string
        if (!is_null($applicantGrossAnnualOtherIncome) && !is_string($applicantGrossAnnualOtherIncome)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($applicantGrossAnnualOtherIncome, true),
                gettype($applicantGrossAnnualOtherIncome)
            ), __LINE__);
        }
        $this->ApplicantGrossAnnualOtherIncome = $applicantGrossAnnualOtherIncome;

        return $this;
    }

    /**
     * Get HouseholdTotalGrossAnnualIncome value
     *
     * @return string|null
     */
    public function getHouseholdTotalGrossAnnualIncome(): ?string
    {
        return $this->HouseholdTotalGrossAnnualIncome;
    }

    /**
     * Set HouseholdTotalGrossAnnualIncome value
     *
     * @param string $householdTotalGrossAnnualIncome
     * @return GrossAnnualIncomeFields
     */
    public function setHouseholdTotalGrossAnnualIncome(?string $householdTotalGrossAnnualIncome = null): self
    {
        // validation for constraint: string
        if (!is_null($householdTotalGrossAnnualIncome) && !is_string($householdTotalGrossAnnualIncome)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($householdTotalGrossAnnualIncome, true),
                gettype($householdTotalGrossAnnualIncome)
            ), __LINE__);
        }
        $this->HouseholdTotalGrossAnnualIncome = $householdTotalGrossAnnualIncome;

        return $this;
    }
}
