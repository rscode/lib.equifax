<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for AMLEnhancedWatchlistCheckSummary StructType
 *
 * @subpackage Structs
 */
class AMLEnhancedWatchlistCheckSummary extends CodedDataRequest
{
}
