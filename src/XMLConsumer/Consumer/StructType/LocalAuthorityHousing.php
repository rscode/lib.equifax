<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

/**
 * This class stands for LocalAuthorityHousing StructType
 *
 * @subpackage Structs
 */
class LocalAuthorityHousing extends FinancialAgreement
{
}
