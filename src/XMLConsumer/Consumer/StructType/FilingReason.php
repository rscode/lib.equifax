<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FilingReason StructType
 *
 * @subpackage Structs
 */
class FilingReason extends AbstractStructBase
{
    /**
     * The id
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 99
     * - minInclusive: 0
     * - use: required
     *
     * @var int
     */
    protected int $id;
    /**
     * The _
     *
     * @var string|null
     */
    protected ?string $_ = null;

    /**
     * Constructor method for FilingReason
     *
     * @param int    $id
     * @param string $_
     * @uses FilingReason::setId()
     * @uses FilingReason::set_()
     */
    public function __construct(int $id, ?string $_ = null)
    {
        $this
            ->setId($id)
            ->set_($_);
    }

    /**
     * Get id value
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set id value
     *
     * @param int $id
     * @return FilingReason
     */
    public function setId(int $id): self
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($id, true),
                gettype($id)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(99)
        if (!is_null($id) && $id > 99) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 99',
                var_export($id, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($id) && $id < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($id, true)
            ), __LINE__);
        }
        $this->id = $id;

        return $this;
    }

    /**
     * Get _ value
     *
     * @return string|null
     */
    public function get_(): ?string
    {
        return $this->_;
    }

    /**
     * Set _ value
     *
     * @param string $_
     * @return FilingReason
     */
    public function set_(?string $_ = null): self
    {
        // validation for constraint: string
        if (!is_null($_) && !is_string($_)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($_, true),
                gettype($_)
            ), __LINE__);
        }
        $this->_ = $_;

        return $this;
    }
}
