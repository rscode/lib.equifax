<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;

/**
 * This class stands for RequestPerson StructType
 *
 * @subpackage Structs
 */
class RequestPerson extends PersonBase
{
    /**
     * The currentAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var ResidenceInstance|null
     */
    protected ?ResidenceInstance $currentAddress = null;
    /**
     * The introducer
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var Introducer|null
     */
    protected ?Introducer $introducer = null;
    /**
     * The loanDetail
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var LoanDetail|null
     */
    protected ?LoanDetail $loanDetail = null;
    /**
     * The previousAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: 2
     * - minOccurs: 0
     *
     * @var RequestPreviousResidenceInstance[]
     */
    protected ?array $previousAddress = null;
    /**
     * The solicitor
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var Solicitor|null
     */
    protected ?Solicitor $solicitor = null;
    /**
     * The timeOnERCurrentAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $timeOnERCurrentAddress = null;
    /**
     * The affordability
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var Affordability|null
     */
    protected ?Affordability $affordability = null;

    /**
     * Constructor method for RequestPerson
     *
     * @param ResidenceInstance                  $currentAddress
     * @param Introducer                         $introducer
     * @param LoanDetail                         $loanDetail
     * @param RequestPreviousResidenceInstance[] $previousAddress
     * @param Solicitor                          $solicitor
     * @param string                             $timeOnERCurrentAddress
     * @param Affordability                      $affordability
     * @uses RequestPerson::setCurrentAddress()
     * @uses RequestPerson::setIntroducer()
     * @uses RequestPerson::setLoanDetail()
     * @uses RequestPerson::setPreviousAddress()
     * @uses RequestPerson::setSolicitor()
     * @uses RequestPerson::setTimeOnERCurrentAddress()
     * @uses RequestPerson::setAffordability()
     */
    public function __construct(
        ?ResidenceInstance $currentAddress = null,
        ?Introducer $introducer = null,
        ?LoanDetail $loanDetail = null,
        ?array $previousAddress = null,
        ?Solicitor $solicitor = null,
        ?string $timeOnERCurrentAddress = null,
        ?Affordability $affordability = null
    ) {
        $this
            ->setCurrentAddress($currentAddress)
            ->setIntroducer($introducer)
            ->setLoanDetail($loanDetail)
            ->setPreviousAddress($previousAddress)
            ->setSolicitor($solicitor)
            ->setTimeOnERCurrentAddress($timeOnERCurrentAddress)
            ->setAffordability($affordability);
    }

    /**
     * This method is responsible for validating the values passed to the setPreviousAddress method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPreviousAddress method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePreviousAddressForArrayConstraintsFromSetPreviousAddress(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $requestPersonPreviousAddressItem) {
            // validation for constraint: itemType
            if (!$requestPersonPreviousAddressItem instanceof RequestPreviousResidenceInstance) {
                $invalidValues[] = is_object($requestPersonPreviousAddressItem) ? get_class($requestPersonPreviousAddressItem) : sprintf(
                    '%s(%s)',
                    gettype($requestPersonPreviousAddressItem),
                    var_export($requestPersonPreviousAddressItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The previousAddress property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\RequestPreviousResidenceInstance, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get currentAddress value
     *
     * @return ResidenceInstance|null
     */
    public function getCurrentAddress(): ?ResidenceInstance
    {
        return $this->currentAddress;
    }

    /**
     * Set currentAddress value
     *
     * @param ResidenceInstance $currentAddress
     * @return RequestPerson
     */
    public function setCurrentAddress(?ResidenceInstance $currentAddress = null): self
    {
        $this->currentAddress = $currentAddress;

        return $this;
    }

    /**
     * Get introducer value
     *
     * @return Introducer|null
     */
    public function getIntroducer(): ?Introducer
    {
        return $this->introducer;
    }

    /**
     * Set introducer value
     *
     * @param Introducer $introducer
     * @return RequestPerson
     */
    public function setIntroducer(?Introducer $introducer = null): self
    {
        $this->introducer = $introducer;

        return $this;
    }

    /**
     * Get loanDetail value
     *
     * @return LoanDetail|null
     */
    public function getLoanDetail(): ?LoanDetail
    {
        return $this->loanDetail;
    }

    /**
     * Set loanDetail value
     *
     * @param LoanDetail $loanDetail
     * @return RequestPerson
     */
    public function setLoanDetail(?LoanDetail $loanDetail = null): self
    {
        $this->loanDetail = $loanDetail;

        return $this;
    }

    /**
     * Get previousAddress value
     *
     * @return RequestPreviousResidenceInstance[]
     */
    public function getPreviousAddress(): ?array
    {
        return $this->previousAddress;
    }

    /**
     * Set previousAddress value
     *
     * @param RequestPreviousResidenceInstance[] $previousAddress
     * @return RequestPerson
     * @throws InvalidArgumentException
     */
    public function setPreviousAddress(?array $previousAddress = null): self
    {
        // validation for constraint: array
        if ('' !== ($previousAddressArrayErrorMessage = self::validatePreviousAddressForArrayConstraintsFromSetPreviousAddress($previousAddress))) {
            throw new InvalidArgumentException($previousAddressArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(2)
        if (is_array($previousAddress) && count($previousAddress) > 2) {
            throw new InvalidArgumentException(sprintf(
                'Invalid count of %s, the number of elements contained by the property must be less than or equal to 2',
                count($previousAddress)
            ), __LINE__);
        }
        $this->previousAddress = $previousAddress;

        return $this;
    }

    /**
     * Add item to previousAddress value
     *
     * @param RequestPreviousResidenceInstance $item
     * @return RequestPerson
     * @throws InvalidArgumentException
     */
    public function addToPreviousAddress(RequestPreviousResidenceInstance $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof RequestPreviousResidenceInstance) {
            throw new InvalidArgumentException(sprintf(
                'The previousAddress property can only contain items of type \Ratespecial\Equifax\XMLConsumer\Consumer\StructType\RequestPreviousResidenceInstance, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        // validation for constraint: maxOccurs(2)
        if (is_array($this->previousAddress) && count($this->previousAddress) >= 2) {
            throw new InvalidArgumentException(sprintf(
                'You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 2',
                count($this->previousAddress)
            ), __LINE__);
        }
        $this->previousAddress[] = $item;

        return $this;
    }

    /**
     * Get solicitor value
     *
     * @return Solicitor|null
     */
    public function getSolicitor(): ?Solicitor
    {
        return $this->solicitor;
    }

    /**
     * Set solicitor value
     *
     * @param Solicitor $solicitor
     * @return RequestPerson
     */
    public function setSolicitor(?Solicitor $solicitor = null): self
    {
        $this->solicitor = $solicitor;

        return $this;
    }

    /**
     * Get timeOnERCurrentAddress value
     *
     * @return string|null
     */
    public function getTimeOnERCurrentAddress(): ?string
    {
        return $this->timeOnERCurrentAddress;
    }

    /**
     * Set timeOnERCurrentAddress value
     *
     * @param string $timeOnERCurrentAddress
     * @return RequestPerson
     */
    public function setTimeOnERCurrentAddress(?string $timeOnERCurrentAddress = null): self
    {
        // validation for constraint: string
        if (!is_null($timeOnERCurrentAddress) && !is_string($timeOnERCurrentAddress)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($timeOnERCurrentAddress, true),
                gettype($timeOnERCurrentAddress)
            ), __LINE__);
        }
        $this->timeOnERCurrentAddress = $timeOnERCurrentAddress;

        return $this;
    }

    /**
     * Get affordability value
     *
     * @return Affordability|null
     */
    public function getAffordability(): ?Affordability
    {
        return $this->affordability;
    }

    /**
     * Set affordability value
     *
     * @param Affordability $affordability
     * @return RequestPerson
     */
    public function setAffordability(?Affordability $affordability = null): self
    {
        $this->affordability = $affordability;

        return $this;
    }
}
