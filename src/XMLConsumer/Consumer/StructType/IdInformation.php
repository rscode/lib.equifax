<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for IdInformation StructType
 *
 * @subpackage Structs
 */
class IdInformation extends AbstractStructBase
{
    /**
     * The idType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $idType = null;
    /**
     * The id
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     *
     * @var string[]
     */
    protected ?array $id = null;

    /**
     * Constructor method for IdInformation
     *
     * @param string   $idType
     * @param string[] $id
     * @uses IdInformation::setIdType()
     * @uses IdInformation::setId()
     */
    public function __construct(?string $idType = null, ?array $id = null)
    {
        $this
            ->setIdType($idType)
            ->setId($id);
    }

    /**
     * This method is responsible for validating the values passed to the setId method
     * This method is willingly generated in order to preserve the one-line inline validation within the setId method
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateIdForArrayConstraintsFromSetId(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $idInformationIdItem) {
            // validation for constraint: itemType
            if (!is_string($idInformationIdItem)) {
                $invalidValues[] = is_object($idInformationIdItem) ? get_class($idInformationIdItem) : sprintf(
                    '%s(%s)',
                    gettype($idInformationIdItem),
                    var_export($idInformationIdItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The id property can only contain items of type string, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get idType value
     *
     * @return string|null
     */
    public function getIdType(): ?string
    {
        return $this->idType;
    }

    /**
     * Set idType value
     *
     * @param string $idType
     * @return IdInformation
     */
    public function setIdType(?string $idType = null): self
    {
        // validation for constraint: string
        if (!is_null($idType) && !is_string($idType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($idType, true),
                gettype($idType)
            ), __LINE__);
        }
        $this->idType = $idType;

        return $this;
    }

    /**
     * Get id value
     *
     * @return string[]
     */
    public function getId(): ?array
    {
        return $this->id;
    }

    /**
     * Set id value
     *
     * @param string[] $id
     * @return IdInformation
     * @throws InvalidArgumentException
     */
    public function setId(?array $id = null): self
    {
        // validation for constraint: array
        if ('' !== ($idArrayErrorMessage = self::validateIdForArrayConstraintsFromSetId($id))) {
            throw new InvalidArgumentException($idArrayErrorMessage, __LINE__);
        }
        $this->id = $id;

        return $this;
    }

    /**
     * Add item to id value
     *
     * @param string $item
     * @return IdInformation
     * @throws InvalidArgumentException
     */
    public function addToId(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf(
                'The id property can only contain items of type string, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        $this->id[] = $item;

        return $this;
    }
}
