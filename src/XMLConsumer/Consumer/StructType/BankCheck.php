<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use DOMDocument;
use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\BankAccountCheckStatus;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\SortCodeCheckStatus;

/**
 * This class stands for BankCheck StructType
 * Meta information extracted from the WSDL
 * - documentation: The Bank Check data group simply validates the supplied bank sort code and bank account details using Equifax algorithms and financial data (for the first applicant's current address only)
 *
 * @subpackage Structs
 */
class BankCheck extends DataItem
{
    /**
     * The bankAccountCheck
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $bankAccountCheck;
    /**
     * The idNumber
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxLength: 40
     * - maxOccurs: 1
     * - minLength: 0
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $idNumber;
    /**
     * The sortCodeCheck
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $sortCodeCheck;
    /**
     * The any
     *
     * @var DOMDocument|string|null
     */
    protected $any = null;

    /**
     * Constructor method for BankCheck
     *
     * @param string                  $bankAccountCheck
     * @param string                  $idNumber
     * @param string                  $sortCodeCheck
     * @param DOMDocument|string|null $any
     * @uses BankCheck::setBankAccountCheck()
     * @uses BankCheck::setIdNumber()
     * @uses BankCheck::setSortCodeCheck()
     * @uses BankCheck::setAny()
     */
    public function __construct(string $bankAccountCheck, string $idNumber, string $sortCodeCheck, $any = null)
    {
        $this
            ->setBankAccountCheck($bankAccountCheck)
            ->setIdNumber($idNumber)
            ->setSortCodeCheck($sortCodeCheck)
            ->setAny($any);
    }

    /**
     * Get bankAccountCheck value
     *
     * @return string
     */
    public function getBankAccountCheck(): string
    {
        return $this->bankAccountCheck;
    }

    /**
     * Set bankAccountCheck value
     *
     * @param string $bankAccountCheck
     * @return BankCheck
     * @throws InvalidArgumentException
     * @uses BankAccountCheckStatus::getValidValues
     * @uses BankAccountCheckStatus::valueIsValid
     */
    public function setBankAccountCheck(string $bankAccountCheck): self
    {
        // validation for constraint: enumeration
        if (!BankAccountCheckStatus::valueIsValid($bankAccountCheck)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\BankAccountCheckStatus',
                is_array($bankAccountCheck) ? implode(', ', $bankAccountCheck) : var_export($bankAccountCheck, true),
                implode(', ', BankAccountCheckStatus::getValidValues())
            ), __LINE__);
        }
        $this->bankAccountCheck = $bankAccountCheck;

        return $this;
    }

    /**
     * Get idNumber value
     *
     * @return string
     */
    public function getIdNumber(): string
    {
        return $this->idNumber;
    }

    /**
     * Set idNumber value
     *
     * @param string $idNumber
     * @return BankCheck
     */
    public function setIdNumber(string $idNumber): self
    {
        // validation for constraint: string
        if (!is_null($idNumber) && !is_string($idNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($idNumber, true),
                gettype($idNumber)
            ), __LINE__);
        }
        // validation for constraint: maxLength(40)
        if (!is_null($idNumber) && mb_strlen((string)$idNumber) > 40) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 40',
                mb_strlen((string)$idNumber)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($idNumber) && mb_strlen((string)$idNumber) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$idNumber)
            ), __LINE__);
        }
        $this->idNumber = $idNumber;

        return $this;
    }

    /**
     * Get sortCodeCheck value
     *
     * @return string
     */
    public function getSortCodeCheck(): string
    {
        return $this->sortCodeCheck;
    }

    /**
     * Set sortCodeCheck value
     *
     * @param string $sortCodeCheck
     * @return BankCheck
     * @throws InvalidArgumentException
     * @uses SortCodeCheckStatus::getValidValues
     * @uses SortCodeCheckStatus::valueIsValid
     */
    public function setSortCodeCheck(string $sortCodeCheck): self
    {
        // validation for constraint: enumeration
        if (!SortCodeCheckStatus::valueIsValid($sortCodeCheck)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\SortCodeCheckStatus',
                is_array($sortCodeCheck) ? implode(', ', $sortCodeCheck) : var_export($sortCodeCheck, true),
                implode(', ', SortCodeCheckStatus::getValidValues())
            ), __LINE__);
        }
        $this->sortCodeCheck = $sortCodeCheck;

        return $this;
    }

    /**
     * Get any value
     *
     * @param bool $asDomDocument true: returns \DOMDocument, false: returns XML string
     * @return DOMDocument|string|null
     * @uses DOMDocument::loadXML
     */
    public function getAny(bool $asDomDocument = false)
    {
        $domDocument = null;
        if (!empty($this->any) && $asDomDocument) {
            $domDocument = new DOMDocument('1.0', 'UTF-8');
            $domDocument->loadXML($this->any);
        }

        return $asDomDocument ? $domDocument : $this->any;
    }

    /**
     * Set any value
     *
     * @param DOMDocument|string|null $any
     * @return BankCheck
     * @uses \DOMNode::item()
     * @uses DOMDocument::hasChildNodes
     * @uses DOMDocument::saveXML
     */
    public function setAny($any = null): self
    {
        // validation for constraint: xml
        if (!is_null($any) && !$any instanceof DOMDocument && (!is_string($any) || (is_string($any) && (empty($any) || (($anyDoc = new DOMDocument()) && false === $anyDoc->loadXML($any)))))) {
            throw new InvalidArgumentException(
                sprintf('Invalid value %s, please provide a valid XML string', var_export($any, true)),
                __LINE__
            );
        }
        $this->any = ($any instanceof DOMDocument) ? $any->saveXML($any->hasChildNodes() ? $any->childNodes->item(0) : null) : $any;

        return $this;
    }
}
