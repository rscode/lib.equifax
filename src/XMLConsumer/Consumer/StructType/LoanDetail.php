<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\StructType;

use InvalidArgumentException;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\LoanPurpose;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for LoanDetail StructType
 *
 * @subpackage Structs
 */
class LoanDetail extends AbstractStructBase
{
    /**
     * The amountRequested
     * Meta information extracted from the WSDL
     * - base: xs:int
     * - maxInclusive: 9999999
     * - maxOccurs: 1
     * - minInclusive: 0
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $amountRequested;
    /**
     * The loanPurpose
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var string
     */
    protected string $loanPurpose;

    /**
     * Constructor method for LoanDetail
     *
     * @param int    $amountRequested
     * @param string $loanPurpose
     * @uses LoanDetail::setAmountRequested()
     * @uses LoanDetail::setLoanPurpose()
     */
    public function __construct(int $amountRequested, string $loanPurpose)
    {
        $this
            ->setAmountRequested($amountRequested)
            ->setLoanPurpose($loanPurpose);
    }

    /**
     * Get amountRequested value
     *
     * @return int
     */
    public function getAmountRequested(): int
    {
        return $this->amountRequested;
    }

    /**
     * Set amountRequested value
     *
     * @param int $amountRequested
     * @return LoanDetail
     */
    public function setAmountRequested(int $amountRequested): self
    {
        // validation for constraint: int
        if (!is_null($amountRequested) && !(is_int($amountRequested) || ctype_digit($amountRequested))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($amountRequested, true),
                gettype($amountRequested)
            ), __LINE__);
        }
        // validation for constraint: maxInclusive(9999999)
        if (!is_null($amountRequested) && $amountRequested > 9999999) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically less than or equal to 9999999',
                var_export($amountRequested, true)
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($amountRequested) && $amountRequested < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($amountRequested, true)
            ), __LINE__);
        }
        $this->amountRequested = $amountRequested;

        return $this;
    }

    /**
     * Get loanPurpose value
     *
     * @return string
     */
    public function getLoanPurpose(): string
    {
        return $this->loanPurpose;
    }

    /**
     * Set loanPurpose value
     *
     * @param string $loanPurpose
     * @return LoanDetail
     * @throws InvalidArgumentException
     * @uses LoanPurpose::getValidValues
     * @uses LoanPurpose::valueIsValid
     */
    public function setLoanPurpose(string $loanPurpose): self
    {
        // validation for constraint: enumeration
        if (!LoanPurpose::valueIsValid($loanPurpose)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\LoanPurpose',
                is_array($loanPurpose) ? implode(', ', $loanPurpose) : var_export($loanPurpose, true),
                implode(', ', LoanPurpose::getValidValues())
            ), __LINE__);
        }
        $this->loanPurpose = $loanPurpose;

        return $this;
    }
}
