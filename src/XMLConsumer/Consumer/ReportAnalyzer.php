<?php

namespace Ratespecial\Equifax\XMLConsumer\Consumer;

use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\CourtAndInsolvencyInformation;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\CreditCard;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\ElectoralRoll;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\InsightAccountContainer;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\PreviousCreditSearch;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\PreviousNonCreditSearch;
use stdClass;

class ReportAnalyzer
{
    protected readonly stdClass $report;

    /**
     * @param mixed $report Must have structure of CommonResponse
     */
    public function __construct(mixed $report)
    {
        $this->report = json_decode(json_encode($report));
    }

    /**
     * Score value of the first score in the report.
     */
    public function getScore(): int
    {
        return $this->report->nonAddressSpecificData->scores->score[0]->value;
    }

    /**
     * @return array<AnalyzerRecord>
     * @see CreditCard
     */
    public function getCreditCardAccounts(): array
    {
        return $this->getInsightAccountContainerSection('creditCard');
    }

    /**
     * @return array<AnalyzerRecord>
     */
    public function getMortgages(): array
    {
        $securedLoans = $this->getInsightAccountContainerSection('securedLoan');

        return array_filter($securedLoans, fn(AnalyzerRecord $loan) => $loan->data->loanType === 'mortgage');
    }

    /**
     * @return array<AnalyzerRecord>
     * @see PreviousCreditSearch
     */
    public function getCreditSearches(): array
    {
        return $this->getPreviousSearches('previousCreditSearch');
    }

    /**
     * @return array<AnalyzerRecord>
     * @see PreviousNonCreditSearch
     */
    public function getNonCreditSearches(): array
    {
        return $this->getPreviousSearches('previousNonCreditSearch');
    }

    /**
     * @return array<AnalyzerRecord>
     */
    protected function getPreviousSearches(string $searchType): array
    {
        $searches = $this->getDataSection('previousSearches');

        return array_reduce($searches, function ($carry, AnalyzerRecord $section) use ($searchType) {
            $sectionData = $section->data->$searchType ?? [];
            $sectionData = array_map(fn($s) => new AnalyzerRecord($s, $section->address), $sectionData);

            return array_merge($carry, $sectionData);
        }, []);
    }

    /**
     * Returns one record per court insolvency entry, along with the address it was found under
     *  [
     *     AnalyzerRecord(court_insolvency_record, MatchedConsumerAddress),
     *     AnalyzerRecord(court_insolvency_record, MatchedConsumerAddress),
     *  ]
     *
     * @return array<AnalyzerRecord>
     * @see CourtAndInsolvencyInformation
     */
    public function getCourtAndInsolvencyInfo(): array
    {
        $dataSections = $this->getDataSection('courtAndInsolvencyInformationData');
        $results      = [];

        foreach ($dataSections as $section) {
            $realData = $section->data->courtAndInsolvencyInformation ?? [];
            $sectionData = array_map(fn($s) => new AnalyzerRecord($s, $section->address), $realData);

            $results  = array_merge($results, $sectionData);
        }

        return $results;
    }

    /**
     * Returns one record per electoral roll entry, along with the address it was found under
     * [
     *    AnalyzerRecord(electoral_roll_record, MatchedConsumerAddress),
     *    AnalyzerRecord(electoral_roll_record, MatchedConsumerAddress),
     * ]
     *
     * @return array<AnalyzerRecord>
     * @see ElectoralRoll
     */
    public function getElectoralRoll(): array
    {
        $dataSections = $this->getDataSection('electoralRollData');
        $results      = [];

        foreach ($dataSections as $section) {
            $realData = $section->data->electoralRoll ?? [];
            $sectionData = array_map(fn($s) => new AnalyzerRecord($s, $section->address), $realData);

            $results  = array_merge($results, $sectionData);
        }

        return $results;
    }

    /**
     * Combines insight data from all addresses in the report.  These will be all financial accounts.  Result array will have one account
     * per record.
     *
     * If you ask pass creditCards as the section and there are 7 credit cards across 3 addresses, there will be 7 records in the result.
     *
     * [
     *   AnalyzerRecord(account, MatchedConsumerAddress),
     *   AnalyzerRecord(account, MatchedConsumerAddress),
     * ]
     *
     * @param string $insightDataSection A property of InsightAccountContainer
     * @return array<AnalyzerRecord>
     * @see InsightAccountContainer
     */
    public function getInsightAccountContainerSection(string $insightDataSection): array
    {
        $insightData = $this->getDataSection('insightData');

        return array_reduce($insightData, function ($carry, AnalyzerRecord $section) use ($insightDataSection) {
            $sectionData = $section->data->$insightDataSection ?? [];
            $sectionData = array_map(fn($s) => new AnalyzerRecord($s, $section->address), $sectionData);

            return array_merge($carry, $sectionData);
        }, []);
    }

    /**
     * Returns an array containing the values of a specific property in the AddressSpecificData for each block in the suppliedAddressData
     * and linkedAddressData sections of the report.  The elements include the MatchedConsumerAddress as well.  Will return 1 array element
     * per data section (read: address).
     *
     * If you pass insightData and there are 4 addresses with insight data, there will be 4 elements.
     *
     * @param string $section           A property of AddressSpecificData to retrieve values from
     * @return array<AnalyzerRecord>    `data` will be an address's entire section.
     * @see AddressSpecificData
     */
    public function getDataSection(string $section): array
    {
        $result = [];

        foreach ($this->report->soleSearch->primary->suppliedAddressData ?? [] as $block) {
            // @see MatchedConsumerAddress
            $address = $block->matchedAddress;

            $blockSection = $block->addressSpecificData->$section ?? null;
            if ($blockSection) {
                $result[] = new AnalyzerRecord($blockSection, $address);
            }
        }

        foreach ($this->report->soleSearch->primary->linkedAddressData ?? [] as $block) {
            // @see MatchedConsumerAddress
            $address = $block->linkedAddress;

            $blockSection = $block->addressSpecificData->$section ?? null;
            if ($blockSection) {
                $result[] = new AnalyzerRecord($blockSection, $address);
            }
        }

        return $result;
    }
}
