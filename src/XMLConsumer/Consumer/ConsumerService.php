<?php

namespace Ratespecial\Equifax\XMLConsumer\Consumer;

use Ratespecial\Equifax\XMLConsumer\Common\AbstractXmlConsumerService;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AntiMoneyLaunderingCheckRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AntiMoneyLaunderingCheckResponse;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\ApplicationFraudCheckRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\ApplicationFraudCheckResponse;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\ConsumerCreditSearchRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\ConsumerCreditSearchResponse;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\CreditQuotationSearchRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\CreditQuotationSearchResponse;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\CustomerManagementEnquiryRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\CustomerManagementEnquiryResponse;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\DebtCollectionSearchRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\DebtCollectionSearchResponse;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\DirectorCreditSearchRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\DirectorCreditSearchResponse;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\FraudInvestigationRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\FraudInvestigationResponse;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\IdCheckByOtherPublicSectorRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\IdCheckByOtherPublicSectorResponse;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\InsuranceApplicationSearchRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\InsuranceApplicationSearchResponse;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\NotifyAliasOrAssociationRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\NotifyAliasOrAssociationResponse;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\PersonnelVettingEnquiryRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\PersonnelVettingEnquiryResponse;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\TenantVerificationEnquiryRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\TenantVerificationEnquiryResponse;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\VerifyIdentityCheckRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\VerifyIdentityCheckResponse;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\WatchlistCheckFullReportRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\WatchlistCheckFullReportResponse;

class ConsumerService extends AbstractXmlConsumerService
{
    public function consumerCreditSearch(ConsumerCreditSearchRequest $request): ConsumerCreditSearchResponse
    {
        return $this->execute('consumerCreditSearch', $request);
    }

    public function antiMoneyLaunderingCheck(AntiMoneyLaunderingCheckRequest $request): AntiMoneyLaunderingCheckResponse
    {
        return $this->execute('antiMoneyLaunderingCheck', $request);
    }

    public function applicationFraudCheck(ApplicationFraudCheckRequest $request): ApplicationFraudCheckResponse
    {
        return $this->execute('applicationFraudCheck', $request);
    }

    public function creditQuotationSearch(CreditQuotationSearchRequest $request): CreditQuotationSearchResponse
    {
        return $this->execute('creditQuotationSearch', $request);
    }

    public function customerManagementEnquiry(CustomerManagementEnquiryRequest $request): CustomerManagementEnquiryResponse
    {
        return $this->execute('customerManagementEnquiry', $request);
    }

    public function debtCollectionSearch(DebtCollectionSearchRequest $request): DebtCollectionSearchResponse
    {
        return $this->execute('debtCollectionSearch', $request);
    }

    public function directorCreditSearch(DirectorCreditSearchRequest $request): DirectorCreditSearchResponse
    {
        return $this->execute('directorCreditSearch', $request);
    }

    public function fraudInvestigation(FraudInvestigationRequest $request): FraudInvestigationResponse
    {
        return $this->execute('fraudInvestigation', $request);
    }

    public function idCheckByOtherPublicSector(IdCheckByOtherPublicSectorRequest $request): IdCheckByOtherPublicSectorResponse
    {
        return $this->execute('idCheckByOtherPublicSector', $request);
    }

    public function insuranceApplicationSearch(InsuranceApplicationSearchRequest $request): InsuranceApplicationSearchResponse
    {
        return $this->execute('insuranceApplicationSearch', $request);
    }

    public function notifyAliasOrAssociation(NotifyAliasOrAssociationRequest $request): NotifyAliasOrAssociationResponse
    {
        return $this->execute('notifyAliasOrAssociation', $request);
    }

    public function personnelVettingEnquiry(PersonnelVettingEnquiryRequest $request): PersonnelVettingEnquiryResponse
    {
        return $this->execute('personnelVettingEnquiry', $request);
    }

    public function tenantVerificationEnquiry(TenantVerificationEnquiryRequest $request): TenantVerificationEnquiryResponse
    {
        return $this->execute('tenantVerificationEnquiry', $request);
    }

    public function verifyIdentityCheck(VerifyIdentityCheckRequest $request): VerifyIdentityCheckResponse
    {
        return $this->execute('verifyIdentityCheck', $request);
    }

    public function watchlistCheck(WatchlistCheckFullReportRequest $request): WatchlistCheckFullReportResponse
    {
        return $this->execute('watchlistCheck', $request);
    }
}
