<?php

namespace Ratespecial\Equifax\XMLConsumer\Consumer;

use JsonMapper;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\MatchedConsumerAddress;

/**
 * A data record returned by methods of the ReportAnalyzer.  This combines address specific data with the address associated with it.
 *
 * @see ReportAnalyzer
 */
class AnalyzerRecord
{
    private static ?JsonMapper $mapper = null;

    public readonly object $data;
    public readonly MatchedConsumerAddress $address;

    public function __construct(object $data, object $address)
    {
        $this->data = $data;

        if (!($address instanceof MatchedConsumerAddress)) {
            $address = self::getMap()->map($address, MatchedConsumerAddress::class);
        }

        $this->address = $address;
    }

    private static function getMap(): JsonMapper
    {
        if (!self::$mapper) {
            self::$mapper = new JsonMapper();
        }

        return self::$mapper;
    }
}
