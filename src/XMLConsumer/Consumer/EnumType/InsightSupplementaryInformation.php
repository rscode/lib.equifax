<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for InsightSupplementaryInformation EnumType
 *
 * @subpackage Enumerations
 */
class InsightSupplementaryInformation extends AbstractStructEnumBase
{
    /**
     * Constant for value 'deceased'
     *
     * @return string 'deceased'
     */
    public const VALUE_DECEASED = 'deceased';
    /**
     * Constant for value 'gone away'
     *
     * @return string 'gone away'
     */
    public const VALUE_GONE_AWAY = 'gone away';
    /**
     * Constant for value 'debt management'
     *
     * @return string 'debt management'
     */
    public const VALUE_DEBT_MANAGEMENT = 'debt management';
    /**
     * Constant for value 'account paid by a 3rd party'
     *
     * @return string 'account paid by a 3rd party'
     */
    public const VALUE_ACCOUNT_PAID_BY_A_3_RD_PARTY = 'account paid by a 3rd party';
    /**
     * Constant for value 'voluntary termination'
     *
     * @return string 'voluntary termination'
     */
    public const VALUE_VOLUNTARY_TERMINATION = 'voluntary termination';
    /**
     * Constant for value 'partial'
     *
     * @return string 'partial'
     */
    public const VALUE_PARTIAL = 'partial';
    /**
     * Constant for value 'debt assigned'
     *
     * @return string 'debt assigned'
     */
    public const VALUE_DEBT_ASSIGNED = 'debt assigned';
    /**
     * Constant for value 'sold to insight member'
     *
     * @return string 'sold to insight member'
     */
    public const VALUE_SOLD_TO_INSIGHT_MEMBER = 'sold to insight member';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_GONE_AWAY
     * @uses self::VALUE_DEBT_MANAGEMENT
     * @uses self::VALUE_ACCOUNT_PAID_BY_A_3_RD_PARTY
     * @uses self::VALUE_VOLUNTARY_TERMINATION
     * @uses self::VALUE_PARTIAL
     * @uses self::VALUE_DEBT_ASSIGNED
     * @uses self::VALUE_SOLD_TO_INSIGHT_MEMBER
     * @uses self::VALUE_DECEASED
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_DECEASED,
            self::VALUE_GONE_AWAY,
            self::VALUE_DEBT_MANAGEMENT,
            self::VALUE_ACCOUNT_PAID_BY_A_3_RD_PARTY,
            self::VALUE_VOLUNTARY_TERMINATION,
            self::VALUE_PARTIAL,
            self::VALUE_DEBT_ASSIGNED,
            self::VALUE_SOLD_TO_INSIGHT_MEMBER,
        ];
    }
}
