<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for CompanyClass EnumType
 *
 * @subpackage Enumerations
 */
class CompanyClass extends AbstractStructEnumBase
{
    /**
     * Constant for value 'AC'
     * Meta information extracted from the WSDL
     * - documentation: Accountancy
     *
     * @return string 'AC'
     */
    public const VALUE_AC = 'AC';
    /**
     * Constant for value 'BF'
     * Meta information extracted from the WSDL
     * - documentation: Bank (Non-CLSB and Finance Divisions)
     *
     * @return string 'BF'
     */
    public const VALUE_BF = 'BF';
    /**
     * Constant for value 'BK'
     * Meta information extracted from the WSDL
     * - documentation: Bank
     *
     * @return string 'BK'
     */
    public const VALUE_BK = 'BK';
    /**
     * Constant for value 'BS'
     * Meta information extracted from the WSDL
     * - documentation: Building Society
     *
     * @return string 'BS'
     */
    public const VALUE_BS = 'BS';
    /**
     * Constant for value 'CB'
     * Meta information extracted from the WSDL
     * - documentation: Credit Broker
     *
     * @return string 'CB'
     */
    public const VALUE_CB = 'CB';
    /**
     * Constant for value 'CC'
     * Meta information extracted from the WSDL
     * - documentation: Credit Card
     *
     * @return string 'CC'
     */
    public const VALUE_CC = 'CC';
    /**
     * Constant for value 'CE'
     * Meta information extracted from the WSDL
     * - documentation: Customs and Excise
     *
     * @return string 'CE'
     */
    public const VALUE_CE = 'CE';
    /**
     * Constant for value 'CG'
     * Meta information extracted from the WSDL
     * - documentation: Cheque Guarantor
     *
     * @return string 'CG'
     */
    public const VALUE_CG = 'CG';
    /**
     * Constant for value 'CH'
     * Meta information extracted from the WSDL
     * - documentation: Charge Card
     *
     * @return string 'CH'
     */
    public const VALUE_CH = 'CH';
    /**
     * Constant for value 'CI'
     * Meta information extracted from the WSDL
     * - documentation: Credit Insurer
     *
     * @return string 'CI'
     */
    public const VALUE_CI = 'CI';
    /**
     * Constant for value 'CL'
     * Meta information extracted from the WSDL
     * - documentation: Consumer Letter
     *
     * @return string 'CL'
     */
    public const VALUE_CL = 'CL';
    /**
     * Constant for value 'CR'
     * Meta information extracted from the WSDL
     * - documentation: Commercial Reporting
     *
     * @return string 'CR'
     */
    public const VALUE_CR = 'CR';
    /**
     * Constant for value 'CS'
     * Meta information extracted from the WSDL
     * - documentation: Communication Supplier
     *
     * @return string 'CS'
     */
    public const VALUE_CS = 'CS';
    /**
     * Constant for value 'DP'
     * Meta information extracted from the WSDL
     * - documentation: Debt Purchaser
     *
     * @return string 'DP'
     */
    public const VALUE_DP = 'DP';
    /**
     * Constant for value 'DC'
     * Meta information extracted from the WSDL
     * - documentation: Debt Collector
     *
     * @return string 'DC'
     */
    public const VALUE_DC = 'DC';
    /**
     * Constant for value 'DI'
     * Meta information extracted from the WSDL
     * - documentation: Distribution and Wholesalers
     *
     * @return string 'DI'
     */
    public const VALUE_DI = 'DI';
    /**
     * Constant for value 'EA'
     * Meta information extracted from the WSDL
     * - documentation: Employment Agency
     *
     * @return string 'EA'
     */
    public const VALUE_EA = 'EA';
    /**
     * Constant for value 'EN'
     * Meta information extracted from the WSDL
     * - documentation: Enquiry Agent
     *
     * @return string 'EN'
     */
    public const VALUE_EN = 'EN';
    /**
     * Constant for value 'ES'
     * Meta information extracted from the WSDL
     * - documentation: Energy Supplier
     *
     * @return string 'ES'
     */
    public const VALUE_ES = 'ES';
    /**
     * Constant for value 'FN'
     * Meta information extracted from the WSDL
     * - documentation: Finance House
     *
     * @return string 'FN'
     */
    public const VALUE_FN = 'FN';
    /**
     * Constant for value 'FS'
     * Meta information extracted from the WSDL
     * - documentation: Financial Services
     *
     * @return string 'FS'
     */
    public const VALUE_FS = 'FS';
    /**
     * Constant for value 'GD'
     * Meta information extracted from the WSDL
     * - documentation: Green Deal
     *
     * @return string 'GD'
     */
    public const VALUE_GD = 'GD';
    /**
     * Constant for value 'GI'
     * Meta information extracted from the WSDL
     * - documentation: General Insurance
     *
     * @return string 'GI'
     */
    public const VALUE_GI = 'GI';
    /**
     * Constant for value 'GV'
     * Meta information extracted from the WSDL
     * - documentation: Government
     *
     * @return string 'GV'
     */
    public const VALUE_GV = 'GV';
    /**
     * Constant for value 'HC'
     * Meta information extracted from the WSDL
     * - documentation: Hire Car Rental
     *
     * @return string 'HC'
     */
    public const VALUE_HC = 'HC';
    /**
     * Constant for value 'HF'
     * Meta information extracted from the WSDL
     * - documentation: Home Furnisher
     *
     * @return string 'HF'
     */
    public const VALUE_HF = 'HF';
    /**
     * Constant for value 'HI'
     * Meta information extracted from the WSDL
     * - documentation: Home Improvement
     *
     * @return string 'HI'
     */
    public const VALUE_HI = 'HI';
    /**
     * Constant for value 'HL'
     * Meta information extracted from the WSDL
     * - documentation: Home
     *
     * @return string 'HL'
     */
    public const VALUE_HL = 'HL';
    /**
     * Constant for value 'HO'
     * Meta information extracted from the WSDL
     * - documentation: House Builder
     *
     * @return string 'HO'
     */
    public const VALUE_HO = 'HO';
    /**
     * Constant for value 'HS'
     * Meta information extracted from the WSDL
     * - documentation: Health Services
     *
     * @return string 'HS'
     */
    public const VALUE_HS = 'HS';
    /**
     * Constant for value 'HT'
     * Meta information extracted from the WSDL
     * - documentation: Hotel and Travel
     *
     * @return string 'HT'
     */
    public const VALUE_HT = 'HT';
    /**
     * Constant for value 'IB'
     * Meta information extracted from the WSDL
     * - documentation: Insurance Broker
     *
     * @return string 'IB'
     */
    public const VALUE_IB = 'IB';
    /**
     * Constant for value 'IN'
     * Meta information extracted from the WSDL
     * - documentation: Insurance
     *
     * @return string 'IN'
     */
    public const VALUE_IN = 'IN';
    /**
     * Constant for value 'IR'
     * Meta information extracted from the WSDL
     * - documentation: Inland Revenue
     *
     * @return string 'IR'
     */
    public const VALUE_IR = 'IR';
    /**
     * Constant for value 'LA'
     * Meta information extracted from the WSDL
     * - documentation: Loss Adjuster
     *
     * @return string 'LA'
     */
    public const VALUE_LA = 'LA';
    /**
     * Constant for value 'LG'
     * Meta information extracted from the WSDL
     * - documentation: Leasing
     *
     * @return string 'LG'
     */
    public const VALUE_LG = 'LG';
    /**
     * Constant for value 'MD'
     * Meta information extracted from the WSDL
     * - documentation: Motor Dealer
     *
     * @return string 'MD'
     */
    public const VALUE_MD = 'MD';
    /**
     * Constant for value 'ME'
     * Meta information extracted from the WSDL
     * - documentation: Media
     *
     * @return string 'ME'
     */
    public const VALUE_ME = 'ME';
    /**
     * Constant for value 'MF'
     * Meta information extracted from the WSDL
     * - documentation: Manufacturing and Industrial
     *
     * @return string 'MF'
     */
    public const VALUE_MF = 'MF';
    /**
     * Constant for value 'MK'
     * Meta information extracted from the WSDL
     * - documentation: Marketing
     *
     * @return string 'MK'
     */
    public const VALUE_MK = 'MK';
    /**
     * Constant for value 'MN'
     * Meta information extracted from the WSDL
     * - documentation: Miscellaneous
     *
     * @return string 'MN'
     */
    public const VALUE_MN = 'MN';
    /**
     * Constant for value 'MO'
     * Meta information extracted from the WSDL
     * - documentation: Mail Order
     *
     * @return string 'MO'
     */
    public const VALUE_MO = 'MO';
    /**
     * Constant for value 'MS'
     * Meta information extracted from the WSDL
     * - documentation: Mortgage Supplier
     *
     * @return string 'MS'
     */
    public const VALUE_MS = 'MS';
    /**
     * Constant for value 'OS'
     * Meta information extracted from the WSDL
     * - documentation: Overseas
     *
     * @return string 'OS'
     */
    public const VALUE_OS = 'OS';
    /**
     * Constant for value 'PM'
     * Meta information extracted from the WSDL
     * - documentation: Property Management
     *
     * @return string 'PM'
     */
    public const VALUE_PM = 'PM';
    /**
     * Constant for value 'PO'
     * Meta information extracted from the WSDL
     * - documentation: Police
     *
     * @return string 'PO'
     */
    public const VALUE_PO = 'PO';
    /**
     * Constant for value 'PU'
     * Meta information extracted from the WSDL
     * - documentation: Public Utility
     *
     * @return string 'PU'
     */
    public const VALUE_PU = 'PU';
    /**
     * Constant for value 'RN'
     * Meta information extracted from the WSDL
     * - documentation: Rental
     *
     * @return string 'RN'
     */
    public const VALUE_RN = 'RN';
    /**
     * Constant for value 'RT'
     * Meta information extracted from the WSDL
     * - documentation: Retailer
     *
     * @return string 'RT'
     */
    public const VALUE_RT = 'RT';
    /**
     * Constant for value 'SB'
     * Meta information extracted from the WSDL
     * - documentation: Stock Broker
     *
     * @return string 'SB'
     */
    public const VALUE_SB = 'SB';
    /**
     * Constant for value 'SL'
     *
     * @return string 'SL'
     */
    public const VALUE_SL = 'SL';
    /**
     * Constant for value 'SO'
     * Meta information extracted from the WSDL
     * - documentation: Solicitor
     *
     * @return string 'SO'
     */
    public const VALUE_SO = 'SO';
    /**
     * Constant for value 'SR'
     * Meta information extracted from the WSDL
     * - documentation: Slot Rental
     *
     * @return string 'SR'
     */
    public const VALUE_SR = 'SR';
    /**
     * Constant for value 'SS'
     * Meta information extracted from the WSDL
     * - documentation: Security Services
     *
     * @return string 'SS'
     */
    public const VALUE_SS = 'SS';
    /**
     * Constant for value 'TC'
     * Meta information extracted from the WSDL
     * - documentation: TV Programme Supplier
     *
     * @return string 'TC'
     */
    public const VALUE_TC = 'TC';
    /**
     * Constant for value 'TI'
     * Meta information extracted from the WSDL
     * - documentation: Travel Insurer
     *
     * @return string 'TI'
     */
    public const VALUE_TI = 'TI';
    /**
     * Constant for value 'TP'
     * Meta information extracted from the WSDL
     * - documentation: Third party
     *
     * @return string 'TP'
     */
    public const VALUE_TP = 'TP';
    /**
     * Constant for value 'TV'
     * Meta information extracted from the WSDL
     * - documentation: TV Rental
     *
     * @return string 'TV'
     */
    public const VALUE_TV = 'TV';
    /**
     * Constant for value 'XX'
     * Meta information extracted from the WSDL
     * - documentation: Training
     *
     * @return string 'XX'
     */
    public const VALUE_XX = 'XX';

    /**
     * Return allowed values
     *
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_AC,
            self::VALUE_BF,
            self::VALUE_BK,
            self::VALUE_BS,
            self::VALUE_CB,
            self::VALUE_CC,
            self::VALUE_CE,
            self::VALUE_CG,
            self::VALUE_CH,
            self::VALUE_CI,
            self::VALUE_CL,
            self::VALUE_CR,
            self::VALUE_CS,
            self::VALUE_DP,
            self::VALUE_DC,
            self::VALUE_DI,
            self::VALUE_EA,
            self::VALUE_EN,
            self::VALUE_ES,
            self::VALUE_FN,
            self::VALUE_FS,
            self::VALUE_GD,
            self::VALUE_GI,
            self::VALUE_GV,
            self::VALUE_HC,
            self::VALUE_HF,
            self::VALUE_HI,
            self::VALUE_HL,
            self::VALUE_HO,
            self::VALUE_HS,
            self::VALUE_HT,
            self::VALUE_IB,
            self::VALUE_IN,
            self::VALUE_IR,
            self::VALUE_LA,
            self::VALUE_LG,
            self::VALUE_MD,
            self::VALUE_ME,
            self::VALUE_MF,
            self::VALUE_MK,
            self::VALUE_MN,
            self::VALUE_MO,
            self::VALUE_MS,
            self::VALUE_OS,
            self::VALUE_PM,
            self::VALUE_PO,
            self::VALUE_PU,
            self::VALUE_RN,
            self::VALUE_RT,
            self::VALUE_SB,
            self::VALUE_SL,
            self::VALUE_SO,
            self::VALUE_SR,
            self::VALUE_SS,
            self::VALUE_TC,
            self::VALUE_TI,
            self::VALUE_TP,
            self::VALUE_TV,
            self::VALUE_XX,
        ];
    }
}
