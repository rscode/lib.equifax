<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use Ratespecial\Equifax\XMLConsumer\Consumer\Dictionary\InsightPaymentStatuses;
use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for PaymentStatus EnumType
 *
 * @see InsightPaymentStatuses
 * @subpackage Enumerations
 */
class PaymentStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'B'
     * Meta information extracted from the WSDL
     * - documentation: Bad arrears
     *
     * @return string 'B'
     */
    public const VALUE_B = 'B';
    /**
     * Constant for value 'Q'
     * Meta information extracted from the WSDL
     * - documentation: Being queried
     *
     * @return string 'Q'
     */
    public const VALUE_Q = 'Q';
    /**
     * Constant for value 'FIVE'
     * Meta information extracted from the WSDL
     * - documentation: Five payments in arrears
     *
     * @return string 'FIVE'
     */
    public const VALUE_FIVE = 'FIVE';
    /**
     * Constant for value 'FOUR'
     * Meta information extracted from the WSDL
     * - documentation: Four payments in arrears
     *
     * @return string 'FOUR'
     */
    public const VALUE_FOUR = 'FOUR';
    /**
     * Constant for value 'R'
     * Meta information extracted from the WSDL
     * - documentation: Repossession
     *
     * @return string 'R'
     */
    public const VALUE_R = 'R';
    /**
     * Constant for value 'V'
     * Meta information extracted from the WSDL
     * - documentation: Voluntary repossession
     *
     * @return string 'V'
     */
    public const VALUE_V = 'V';
    /**
     * Constant for value 'N'
     * Meta information extracted from the WSDL
     * - documentation: Inactive
     *
     * @return string 'N'
     */
    public const VALUE_N = 'N';
    /**
     * Constant for value 'D'
     * Meta information extracted from the WSDL
     * - documentation: Default
     *
     * @return string 'D'
     */
    public const VALUE_D = 'D';
    /**
     * Constant for value 'A'
     * Meta information extracted from the WSDL
     * - documentation: Moderate arrears
     *
     * @return string 'A'
     */
    public const VALUE_A = 'A';
    /**
     * Constant for value 'Z'
     * Meta information extracted from the WSDL
     * - documentation: Not taken up
     *
     * @return string 'Z'
     */
    public const VALUE_Z = 'Z';
    /**
     * Constant for value 'DOT'
     * Meta information extracted from the WSDL
     * - documentation: No update received
     *
     * @return string 'DOT'
     */
    public const VALUE_DOT = 'DOT';
    /**
     * Constant for value 'ONE'
     * Meta information extracted from the WSDL
     * - documentation: One payment in arrears
     *
     * @return string 'ONE'
     */
    public const VALUE_ONE = 'ONE';
    /**
     * Constant for value 'S'
     * Meta information extracted from the WSDL
     * - documentation: Settled
     *
     * @return string 'S'
     */
    public const VALUE_S = 'S';
    /**
     * Constant for value 'SIX'
     * Meta information extracted from the WSDL
     * - documentation: Six payments in arrears
     *
     * @return string 'SIX'
     */
    public const VALUE_SIX = 'SIX';
    /**
     * Constant for value 'I'
     * Meta information extracted from the WSDL
     * - documentation: Special arrangement
     *
     * @return string 'I'
     */
    public const VALUE_I = 'I';
    /**
     * Constant for value 'THREE'
     * Meta information extracted from the WSDL
     * - documentation: Three payments in arrears
     *
     * @return string 'THREE'
     */
    public const VALUE_THREE = 'THREE';
    /**
     * Constant for value 'X'
     * Meta information extracted from the WSDL
     * - documentation: Transferred
     *
     * @return string 'X'
     */
    public const VALUE_X = 'X';
    /**
     * Constant for value 'TWO'
     * Meta information extracted from the WSDL
     * - documentation: Two payments in arrears
     *
     * @return string 'TWO'
     */
    public const VALUE_TWO = 'TWO';
    /**
     * Constant for value 'U'
     * Meta information extracted from the WSDL
     * - documentation: Unclassified
     *
     * @return string 'U'
     */
    public const VALUE_U = 'U';
    /**
     * Constant for value 'ZERO'
     * Meta information extracted from the WSDL
     * - documentation: Up to date
     *
     * @return string 'ZERO'
     */
    public const VALUE_ZERO = 'ZERO';
    /**
     * Constant for value 'W'
     * Meta information extracted from the WSDL
     * - documentation: Written off
     *
     * @return string 'W'
     */
    public const VALUE_W = 'W';
    /**
     * Constant for value 'G'
     * Meta information extracted from the WSDL
     * - documentation: Gone away
     *
     * @return string 'G'
     */
    public const VALUE_G = 'G';

    public static function getValidValues(): array
    {
        return [
            self::VALUE_B,
            self::VALUE_Q,
            self::VALUE_FIVE,
            self::VALUE_FOUR,
            self::VALUE_R,
            self::VALUE_V,
            self::VALUE_N,
            self::VALUE_D,
            self::VALUE_A,
            self::VALUE_Z,
            self::VALUE_DOT,
            self::VALUE_ONE,
            self::VALUE_S,
            self::VALUE_SIX,
            self::VALUE_I,
            self::VALUE_THREE,
            self::VALUE_X,
            self::VALUE_TWO,
            self::VALUE_U,
            self::VALUE_ZERO,
            self::VALUE_W,
            self::VALUE_G,
        ];
    }
}
