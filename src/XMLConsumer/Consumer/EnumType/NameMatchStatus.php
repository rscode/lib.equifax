<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for NameMatchStatus EnumType
 *
 * @see https://developer.equifax.co.uk/sites/uk/files/xml_ref_guide/ref/Data%20Dictionary/Name%20Match%20Levels.pdf
 */
class NameMatchStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'A'
     * Meta information extracted from the WSDL
     * - documentation: Subject 1 details including aliases
     *
     * @return string 'A'
     */
    public const VALUE_A = 'A';
    /**
     * Constant for value 'B'
     * Meta information extracted from the WSDL
     * - documentation: Subject 2 details including aliases
     *
     * @return string 'B'
     */
    public const VALUE_B = 'B';
    /**
     * Constant for value 'C'
     * Meta information extracted from the WSDL
     * - documentation: Associate 1 of Subject 1 details including aliases
     *
     * @return string 'C'
     */
    public const VALUE_C = 'C';
    /**
     * Constant for value 'D'
     * Meta information extracted from the WSDL
     * - documentation: Associate 2 of Subject 1 details
     *
     * @return string 'D'
     */
    public const VALUE_D = 'D';
    /**
     * Constant for value 'E'
     * Meta information extracted from the WSDL
     * - documentation: Associate 3 of Subject 1 details
     *
     * @return string 'E'
     */
    public const VALUE_E = 'E';
    /**
     * Constant for value 'F'
     * Meta information extracted from the WSDL
     * - documentation: Associate 1 of Subject 2 details including aliases
     *
     * @return string 'F'
     */
    public const VALUE_F = 'F';
    /**
     * Constant for value 'G'
     * Meta information extracted from the WSDL
     * - documentation: Associate 2 of Subject 2 details
     *
     * @return string 'G'
     */
    public const VALUE_G = 'G';
    /**
     * Constant for value 'H'
     * Meta information extracted from the WSDL
     * - documentation: Associate 3 of Subject 2 details
     *
     * @return string 'H'
     */
    public const VALUE_H = 'H';
    /**
     * Constant for value 'I'
     * Meta information extracted from the WSDL
     * - documentation: Potential Associate 1 of Subject 1 details and Family
     *
     * @return string 'I'
     */
    public const VALUE_I = 'I';
    /**
     * Constant for value 'J'
     * Meta information extracted from the WSDL
     * - documentation: Potential Associate 2 of Subject 1 details and Family
     *
     * @return string 'J'
     */
    public const VALUE_J = 'J';
    /**
     * Constant for value 'K'
     * Meta information extracted from the WSDL
     * - documentation: Potential Associate 3 of Subject 1 details and Family
     *
     * @return string 'K'
     */
    public const VALUE_K = 'K';
    /**
     * Constant for value 'L'
     * Meta information extracted from the WSDL
     * - documentation: Potential Associate 1 of Subject 2 details and Family
     *
     * @return string 'L'
     */
    public const VALUE_L = 'L';
    /**
     * Constant for value 'M'
     * Meta information extracted from the WSDL
     * - documentation: Potential Associate 2 of Subject 2 details and Family
     *
     * @return string 'M'
     */
    public const VALUE_M = 'M';
    /**
     * Constant for value 'N'
     * Meta information extracted from the WSDL
     * - documentation: Potential Associate 3 of Subject 2 details and Family
     *
     * @return string 'N'
     */
    public const VALUE_N = 'N';
    /**
     * Constant for value 'P'
     * Meta information extracted from the WSDL
     * - documentation: Potential Associate 1 of Subject 1 details
     *
     * @return string 'P'
     */
    public const VALUE_P = 'P';
    /**
     * Constant for value 'Q'
     * Meta information extracted from the WSDL
     * - documentation: Potential Associate 2 of Subject 1 details
     *
     * @return string 'Q'
     */
    public const VALUE_Q = 'Q';
    /**
     * Constant for value 'R'
     * Meta information extracted from the WSDL
     * - documentation: Potential Associate 3 of Subject 1 details
     *
     * @return string 'R'
     */
    public const VALUE_R = 'R';
    /**
     * Constant for value 'S'
     * Meta information extracted from the WSDL
     * - documentation: Potential Associate 1 of Subject 2 details
     *
     * @return string 'S'
     */
    public const VALUE_S = 'S';
    /**
     * Constant for value 'T'
     * Meta information extracted from the WSDL
     * - documentation: Potential Associate 2 of Subject 2 details
     *
     * @return string 'T'
     */
    public const VALUE_T = 'T';
    /**
     * Constant for value 'U'
     * Meta information extracted from the WSDL
     * - documentation: Potential Associate 3 of Subject 2 details
     *
     * @return string 'U'
     */
    public const VALUE_U = 'U';
    /**
     * Constant for value 'V'
     * Meta information extracted from the WSDL
     * - documentation: Family Member of Subject 1
     *
     * @return string 'V'
     */
    public const VALUE_V = 'V';
    /**
     * Constant for value 'W'
     * Meta information extracted from the WSDL
     * - documentation: Family Member of Subject 2
     *
     * @return string 'W'
     */
    public const VALUE_W = 'W';
    /**
     * Constant for value 'X'
     * Meta information extracted from the WSDL
     * - documentation: Attributable Data Subject 1 and Family
     *
     * @return string 'X'
     */
    public const VALUE_X = 'X';
    /**
     * Constant for value 'Y'
     * Meta information extracted from the WSDL
     * - documentation: Attributable Data Subject 2 and Family
     *
     * @return string 'Y'
     */
    public const VALUE_Y = 'Y';
    /**
     * Constant for value 'Z'
     * Meta information extracted from the WSDL
     * - documentation: Other Person
     *
     * @return string 'Z'
     */
    public const VALUE_Z = 'Z';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_A
     * @uses self::VALUE_B
     * @uses self::VALUE_C
     * @uses self::VALUE_D
     * @uses self::VALUE_E
     * @uses self::VALUE_F
     * @uses self::VALUE_G
     * @uses self::VALUE_H
     * @uses self::VALUE_I
     * @uses self::VALUE_J
     * @uses self::VALUE_K
     * @uses self::VALUE_L
     * @uses self::VALUE_M
     * @uses self::VALUE_N
     * @uses self::VALUE_P
     * @uses self::VALUE_Q
     * @uses self::VALUE_R
     * @uses self::VALUE_S
     * @uses self::VALUE_T
     * @uses self::VALUE_U
     * @uses self::VALUE_V
     * @uses self::VALUE_W
     * @uses self::VALUE_X
     * @uses self::VALUE_Y
     * @uses self::VALUE_Z
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_A,
            self::VALUE_B,
            self::VALUE_C,
            self::VALUE_D,
            self::VALUE_E,
            self::VALUE_F,
            self::VALUE_G,
            self::VALUE_H,
            self::VALUE_I,
            self::VALUE_J,
            self::VALUE_K,
            self::VALUE_L,
            self::VALUE_M,
            self::VALUE_N,
            self::VALUE_P,
            self::VALUE_Q,
            self::VALUE_R,
            self::VALUE_S,
            self::VALUE_T,
            self::VALUE_U,
            self::VALUE_V,
            self::VALUE_W,
            self::VALUE_X,
            self::VALUE_Y,
            self::VALUE_Z,
        ];
    }
}
