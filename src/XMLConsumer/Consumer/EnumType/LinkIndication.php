<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for LinkIndication EnumType
 *
 * @subpackage Enumerations
 */
class LinkIndication extends AbstractStructEnumBase
{
    /**
     * Constant for value 'intermediary'
     *
     * @return string 'intermediary'
     */
    public const VALUE_INTERMEDIARY = 'intermediary';
    /**
     * Constant for value 'lastNotConfirmed'
     *
     * @return string 'lastNotConfirmed'
     */
    public const VALUE_LAST_NOT_CONFIRMED = 'lastNotConfirmed';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_LAST_NOT_CONFIRMED
     * @uses self::VALUE_INTERMEDIARY
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_INTERMEDIARY,
            self::VALUE_LAST_NOT_CONFIRMED,
        ];
    }
}
