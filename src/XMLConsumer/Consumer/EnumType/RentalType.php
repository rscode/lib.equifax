<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for RentalType EnumType
 *
 * @subpackage Enumerations
 */
class RentalType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'financeLease'
     *
     * @return string 'financeLease'
     */
    public const VALUE_FINANCE_LEASE = 'financeLease';
    /**
     * Constant for value 'operatingLease'
     *
     * @return string 'operatingLease'
     */
    public const VALUE_OPERATING_LEASE = 'operatingLease';
    /**
     * Constant for value 'standard'
     *
     * @return string 'standard'
     */
    public const VALUE_STANDARD = 'standard';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_OPERATING_LEASE
     * @uses self::VALUE_STANDARD
     * @uses self::VALUE_FINANCE_LEASE
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_FINANCE_LEASE,
            self::VALUE_OPERATING_LEASE,
            self::VALUE_STANDARD,
        ];
    }
}
