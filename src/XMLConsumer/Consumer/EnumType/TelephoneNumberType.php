<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for TelephoneNumberType EnumType
 *
 * @subpackage Enumerations
 */
class TelephoneNumberType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'fixed'
     *
     * @return string 'fixed'
     */
    public const VALUE_FIXED = 'fixed';
    /**
     * Constant for value 'mobile'
     *
     * @return string 'mobile'
     */
    public const VALUE_MOBILE = 'mobile';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_MOBILE
     * @uses self::VALUE_FIXED
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_FIXED,
            self::VALUE_MOBILE,
        ];
    }
}
