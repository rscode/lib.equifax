<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for CurrencyType EnumType
 *
 * @subpackage Enumerations
 */
class CurrencyType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'GBP'
     *
     * @return string 'GBP'
     */
    public const VALUE_GBP = 'GBP';
    /**
     * Constant for value 'other'
     *
     * @return string 'other'
     */
    public const VALUE_OTHER = 'other';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_OTHER
     * @uses self::VALUE_GBP
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_GBP,
            self::VALUE_OTHER,
        ];
    }
}
