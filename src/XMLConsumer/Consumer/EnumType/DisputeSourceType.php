<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for DisputeSourceType EnumType
 *
 * @subpackage Enumerations
 */
class DisputeSourceType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'linkedAddress'
     *
     * @return string 'linkedAddress'
     */
    public const VALUE_LINKED_ADDRESS = 'linkedAddress';
    /**
     * Constant for value 'locateChainingNext'
     *
     * @return string 'locateChainingNext'
     */
    public const VALUE_LOCATE_CHAINING_NEXT = 'locateChainingNext';
    /**
     * Constant for value 'locateChainingPrevious'
     *
     * @return string 'locateChainingPrevious'
     */
    public const VALUE_LOCATE_CHAINING_PREVIOUS = 'locateChainingPrevious';
    /**
     * Constant for value 'locateCurrent'
     *
     * @return string 'locateCurrent'
     */
    public const VALUE_LOCATE_CURRENT = 'locateCurrent';
    /**
     * Constant for value 'suppliedAddress'
     *
     * @return string 'suppliedAddress'
     */
    public const VALUE_SUPPLIED_ADDRESS = 'suppliedAddress';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_LOCATE_CHAINING_NEXT
     * @uses self::VALUE_LOCATE_CHAINING_PREVIOUS
     * @uses self::VALUE_LOCATE_CURRENT
     * @uses self::VALUE_SUPPLIED_ADDRESS
     * @uses self::VALUE_LINKED_ADDRESS
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_LINKED_ADDRESS,
            self::VALUE_LOCATE_CHAINING_NEXT,
            self::VALUE_LOCATE_CHAINING_PREVIOUS,
            self::VALUE_LOCATE_CURRENT,
            self::VALUE_SUPPLIED_ADDRESS,
        ];
    }
}
