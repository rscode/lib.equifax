<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for SalaryPaymentFrequency EnumType
 *
 * @subpackage Enumerations
 */
class SalaryPaymentFrequency extends AbstractStructEnumBase
{
    /**
     * Constant for value 'monthly'
     *
     * @return string 'monthly'
     */
    public const VALUE_MONTHLY = 'monthly';
    /**
     * Constant for value 'weekly'
     *
     * @return string 'weekly'
     */
    public const VALUE_WEEKLY = 'weekly';
    /**
     * Constant for value 'yearly'
     *
     * @return string 'yearly'
     */
    public const VALUE_YEARLY = 'yearly';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_WEEKLY
     * @uses self::VALUE_YEARLY
     * @uses self::VALUE_MONTHLY
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_MONTHLY,
            self::VALUE_WEEKLY,
            self::VALUE_YEARLY,
        ];
    }
}
