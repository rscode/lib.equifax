<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for SortCodeCheckStatus EnumType
 *
 * @subpackage Enumerations
 */
class SortCodeCheckStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'invalid'
     *
     * @return string 'invalid'
     */
    public const VALUE_INVALID = 'invalid';
    /**
     * Constant for value 'other'
     *
     * @return string 'other'
     */
    public const VALUE_OTHER = 'other';
    /**
     * Constant for value 'valid'
     *
     * @return string 'valid'
     */
    public const VALUE_VALID = 'valid';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_OTHER
     * @uses self::VALUE_VALID
     * @uses self::VALUE_INVALID
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_INVALID,
            self::VALUE_OTHER,
            self::VALUE_VALID,
        ];
    }
}
