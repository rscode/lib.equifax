<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for AddressIndicator EnumType
 *
 * @subpackage Enumerations
 */
class AddressIndicator extends AbstractStructEnumBase
{
    /**
     * Constant for value 'current'
     *
     * @return string 'current'
     */
    public const VALUE_CURRENT = 'current';
    /**
     * Constant for value 'previous'
     *
     * @return string 'previous'
     */
    public const VALUE_PREVIOUS = 'previous';
    /**
     * Constant for value 'repossessed'
     *
     * @return string 'repossessed'
     */
    public const VALUE_REPOSSESSED = 'repossessed';
    /**
     * Constant for value 'secondPrevious'
     *
     * @return string 'secondPrevious'
     */
    public const VALUE_SECOND_PREVIOUS = 'secondPrevious';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_PREVIOUS
     * @uses self::VALUE_REPOSSESSED
     * @uses self::VALUE_SECOND_PREVIOUS
     * @uses self::VALUE_CURRENT
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_CURRENT,
            self::VALUE_PREVIOUS,
            self::VALUE_REPOSSESSED,
            self::VALUE_SECOND_PREVIOUS,
        ];
    }
}
