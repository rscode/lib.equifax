<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for LoanPurpose EnumType
 *
 * @subpackage Enumerations
 */
class LoanPurpose extends AbstractStructEnumBase
{
    /**
     * Constant for value 'car'
     *
     * @return string 'car'
     */
    public const VALUE_CAR = 'car';
    /**
     * Constant for value 'debtConsolidation'
     *
     * @return string 'debtConsolidation'
     */
    public const VALUE_DEBT_CONSOLIDATION = 'debtConsolidation';
    /**
     * Constant for value 'furniture'
     *
     * @return string 'furniture'
     */
    public const VALUE_FURNITURE = 'furniture';
    /**
     * Constant for value 'holiday'
     *
     * @return string 'holiday'
     */
    public const VALUE_HOLIDAY = 'holiday';
    /**
     * Constant for value 'homeImprovement'
     *
     * @return string 'homeImprovement'
     */
    public const VALUE_HOME_IMPROVEMENT = 'homeImprovement';
    /**
     * Constant for value 'other'
     *
     * @return string 'other'
     */
    public const VALUE_OTHER = 'other';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_DEBT_CONSOLIDATION
     * @uses self::VALUE_FURNITURE
     * @uses self::VALUE_HOLIDAY
     * @uses self::VALUE_HOME_IMPROVEMENT
     * @uses self::VALUE_OTHER
     * @uses self::VALUE_CAR
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_CAR,
            self::VALUE_DEBT_CONSOLIDATION,
            self::VALUE_FURNITURE,
            self::VALUE_HOLIDAY,
            self::VALUE_HOME_IMPROVEMENT,
            self::VALUE_OTHER,
        ];
    }
}
