<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for TelephoneLineCheckStatus EnumType
 *
 * @subpackage Enumerations
 */
class TelephoneLineCheckStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'matched'
     *
     * @return string 'matched'
     */
    public const VALUE_MATCHED = 'matched';
    /**
     * Constant for value 'notCheckedMatchedAtHomeAddress'
     *
     * @return string 'notCheckedMatchedAtHomeAddress'
     */
    public const VALUE_NOT_CHECKED_MATCHED_AT_HOME_ADDRESS = 'notCheckedMatchedAtHomeAddress';
    /**
     * Constant for value 'notMatched'
     *
     * @return string 'notMatched'
     */
    public const VALUE_NOT_MATCHED = 'notMatched';
    /**
     * Constant for value 'other'
     *
     * @return string 'other'
     */
    public const VALUE_OTHER = 'other';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_NOT_CHECKED_MATCHED_AT_HOME_ADDRESS
     * @uses self::VALUE_NOT_MATCHED
     * @uses self::VALUE_OTHER
     * @uses self::VALUE_MATCHED
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_MATCHED,
            self::VALUE_NOT_CHECKED_MATCHED_AT_HOME_ADDRESS,
            self::VALUE_NOT_MATCHED,
            self::VALUE_OTHER,
        ];
    }
}
