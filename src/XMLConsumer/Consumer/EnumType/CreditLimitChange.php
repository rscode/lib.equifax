<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for CreditLimitChange EnumType
 *
 * @subpackage Enumerations
 */
class CreditLimitChange extends AbstractStructEnumBase
{
    /**
     * Constant for value 'decrease'
     *
     * @return string 'decrease'
     */
    public const VALUE_DECREASE = 'decrease';
    /**
     * Constant for value 'increase'
     *
     * @return string 'increase'
     */
    public const VALUE_INCREASE = 'increase';
    /**
     * Constant for value 'noChange'
     *
     * @return string 'noChange'
     */
    public const VALUE_NO_CHANGE = 'noChange';
    /**
     * Constant for value 'unknown'
     *
     * @return string 'unknown'
     */
    public const VALUE_UNKNOWN = 'unknown';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_INCREASE
     * @uses self::VALUE_NO_CHANGE
     * @uses self::VALUE_UNKNOWN
     * @uses self::VALUE_DECREASE
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_DECREASE,
            self::VALUE_INCREASE,
            self::VALUE_NO_CHANGE,
            self::VALUE_UNKNOWN,
        ];
    }
}
