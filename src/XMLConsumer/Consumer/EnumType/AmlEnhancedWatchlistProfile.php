<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for AmlEnhancedWatchlistProfile EnumType
 *
 * @subpackage Enumerations
 */
class AmlEnhancedWatchlistProfile extends AbstractStructEnumBase
{
    /**
     * Constant for value 'UKSTD'
     *
     * @return string 'UKSTD'
     */
    public const VALUE_UKSTD = 'UKSTD';
    /**
     * Constant for value 'UKPLUS'
     *
     * @return string 'UKPLUS'
     */
    public const VALUE_UKPLUS = 'UKPLUS';
    /**
     * Constant for value 'GLOBAL'
     *
     * @return string 'GLOBAL'
     */
    public const VALUE_GLOBAL = 'GLOBAL';
    /**
     * Constant for value 'BESPOKE'
     *
     * @return string 'BESPOKE'
     */
    public const VALUE_BESPOKE = 'BESPOKE';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_UKPLUS
     * @uses self::VALUE_GLOBAL
     * @uses self::VALUE_BESPOKE
     * @uses self::VALUE_UKSTD
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_UKSTD,
            self::VALUE_UKPLUS,
            self::VALUE_GLOBAL,
            self::VALUE_BESPOKE,
        ];
    }
}
