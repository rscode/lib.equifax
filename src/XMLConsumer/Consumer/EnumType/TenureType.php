<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for TenureType EnumType
 *
 * @subpackage Enumerations
 */
class TenureType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'freehold'
     *
     * @return string 'freehold'
     */
    public const VALUE_FREEHOLD = 'freehold';
    /**
     * Constant for value 'leasehold'
     *
     * @return string 'leasehold'
     */
    public const VALUE_LEASEHOLD = 'leasehold';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_LEASEHOLD
     * @uses self::VALUE_FREEHOLD
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_FREEHOLD,
            self::VALUE_LEASEHOLD,
        ];
    }
}
