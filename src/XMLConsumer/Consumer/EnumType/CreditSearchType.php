<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for CreditSearchType EnumType
 *
 * @subpackage Enumerations
 */
class CreditSearchType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'creditApplication'
     *
     * @return string 'creditApplication'
     */
    public const VALUE_CREDIT_APPLICATION = 'creditApplication';
    /**
     * Constant for value 'creditCustomerManagement'
     *
     * @return string 'creditCustomerManagement'
     */
    public const VALUE_CREDIT_CUSTOMER_MANAGEMENT = 'creditCustomerManagement';
    /**
     * Constant for value 'creditQuotation'
     *
     * @return string 'creditQuotation'
     */
    public const VALUE_CREDIT_QUOTATION = 'creditQuotation';
    /**
     * Constant for value 'directorCreditSearch'
     *
     * @return string 'directorCreditSearch'
     */
    public const VALUE_DIRECTOR_CREDIT_SEARCH = 'directorCreditSearch';
    /**
     * Constant for value 'subsequentCreditSearch'
     *
     * @return string 'subsequentCreditSearch'
     */
    public const VALUE_SUBSEQUENT_CREDIT_SEARCH = 'subsequentCreditSearch';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_CREDIT_CUSTOMER_MANAGEMENT
     * @uses self::VALUE_CREDIT_QUOTATION
     * @uses self::VALUE_DIRECTOR_CREDIT_SEARCH
     * @uses self::VALUE_SUBSEQUENT_CREDIT_SEARCH
     * @uses self::VALUE_CREDIT_APPLICATION
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_CREDIT_APPLICATION,
            self::VALUE_CREDIT_CUSTOMER_MANAGEMENT,
            self::VALUE_CREDIT_QUOTATION,
            self::VALUE_DIRECTOR_CREDIT_SEARCH,
            self::VALUE_SUBSEQUENT_CREDIT_SEARCH,
        ];
    }
}
