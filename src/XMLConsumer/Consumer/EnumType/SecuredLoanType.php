<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for SecuredLoanType EnumType
 *
 * @subpackage Enumerations
 */
class SecuredLoanType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'mortgage'
     *
     * @return string 'mortgage'
     */
    public const VALUE_MORTGAGE = 'mortgage';
    /**
     * Constant for value 'securedLoan'
     *
     * @return string 'securedLoan'
     */
    public const VALUE_SECURED_LOAN = 'securedLoan';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_SECURED_LOAN
     * @uses self::VALUE_MORTGAGE
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_MORTGAGE,
            self::VALUE_SECURED_LOAN,
        ];
    }
}
