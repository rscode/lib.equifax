<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for TimeAtAddressVerificationStatus EnumType
 *
 * @subpackage Enumerations
 */
class TimeAtAddressVerificationStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'greaterThanPersonAge'
     *
     * @return string 'greaterThanPersonAge'
     */
    public const VALUE_GREATER_THAN_PERSON_AGE = 'greaterThanPersonAge';
    /**
     * Constant for value 'notSupplied'
     *
     * @return string 'notSupplied'
     */
    public const VALUE_NOT_SUPPLIED = 'notSupplied';
    /**
     * Constant for value 'other'
     *
     * @return string 'other'
     */
    public const VALUE_OTHER = 'other';
    /**
     * Constant for value 'previousAddress'
     *
     * @return string 'previousAddress'
     */
    public const VALUE_PREVIOUS_ADDRESS = 'previousAddress';
    /**
     * Constant for value 'withinTolerance0To99Years'
     *
     * @return string 'withinTolerance0To99Years'
     */
    public const VALUE_WITHIN_TOLERANCE_0_TO_99_YEARS = 'withinTolerance0To99Years';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_NOT_SUPPLIED
     * @uses self::VALUE_OTHER
     * @uses self::VALUE_PREVIOUS_ADDRESS
     * @uses self::VALUE_WITHIN_TOLERANCE_0_TO_99_YEARS
     * @uses self::VALUE_GREATER_THAN_PERSON_AGE
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_GREATER_THAN_PERSON_AGE,
            self::VALUE_NOT_SUPPLIED,
            self::VALUE_OTHER,
            self::VALUE_PREVIOUS_ADDRESS,
            self::VALUE_WITHIN_TOLERANCE_0_TO_99_YEARS,
        ];
    }
}
