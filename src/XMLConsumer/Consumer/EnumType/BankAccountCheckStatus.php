<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for BankAccountCheckStatus EnumType
 *
 * @subpackage Enumerations
 */
class BankAccountCheckStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'invalid'
     *
     * @return string 'invalid'
     */
    public const VALUE_INVALID = 'invalid';
    /**
     * Constant for value 'notCheckedSortCodeInvalid'
     *
     * @return string 'notCheckedSortCodeInvalid'
     */
    public const VALUE_NOT_CHECKED_SORT_CODE_INVALID = 'notCheckedSortCodeInvalid';
    /**
     * Constant for value 'other'
     *
     * @return string 'other'
     */
    public const VALUE_OTHER = 'other';
    /**
     * Constant for value 'valid'
     *
     * @return string 'valid'
     */
    public const VALUE_VALID = 'valid';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_NOT_CHECKED_SORT_CODE_INVALID
     * @uses self::VALUE_OTHER
     * @uses self::VALUE_VALID
     * @uses self::VALUE_INVALID
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_INVALID,
            self::VALUE_NOT_CHECKED_SORT_CODE_INVALID,
            self::VALUE_OTHER,
            self::VALUE_VALID,
        ];
    }
}
