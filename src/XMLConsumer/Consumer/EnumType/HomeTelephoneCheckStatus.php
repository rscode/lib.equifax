<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for HomeTelephoneCheckStatus EnumType
 *
 * @subpackage Enumerations
 */
class HomeTelephoneCheckStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Y'
     * Meta information extracted from the WSDL
     * - documentation: Matched on number
     *
     * @return string 'Y'
     */
    public const VALUE_Y = 'Y';
    /**
     * Constant for value 'M'
     * Meta information extracted from the WSDL
     * - documentation: Mobile or pager
     *
     * @return string 'M'
     */
    public const VALUE_M = 'M';
    /**
     * Constant for value 'S'
     * Meta information extracted from the WSDL
     * - documentation: No data held
     *
     * @return string 'S'
     */
    public const VALUE_S = 'S';
    /**
     * Constant for value 'X'
     * Meta information extracted from the WSDL
     * - documentation: Other
     *
     * @return string 'X'
     */
    public const VALUE_X = 'X';
    /**
     * Constant for value 'SEVEN'
     * Meta information extracted from the WSDL
     * - documentation: Partial match STD code matches and local number is a different length and more than 2 digits are different
     *
     * @return string 'SEVEN'
     */
    public const VALUE_SEVEN = 'SEVEN';
    /**
     * Constant for value 'TWO'
     * Meta information extracted from the WSDL
     * - documentation: Partial match STD code matches and local number is a different length and 2 digits or less is different
     *
     * @return string 'TWO'
     */
    public const VALUE_TWO = 'TWO';
    /**
     * Constant for value 'SIX'
     * Meta information extracted from the WSDL
     * - documentation: Partial match STD code matches and local number is the same length but more than 2 digits are different
     *
     * @return string 'SIX'
     */
    public const VALUE_SIX = 'SIX';
    /**
     * Constant for value 'ONE'
     * Meta information extracted from the WSDL
     * - documentation: Partial match STD code matches and local number is the same length and 2 digits or less is different
     *
     * @return string 'ONE'
     */
    public const VALUE_ONE = 'ONE';
    /**
     * Constant for value 'F'
     * Meta information extracted from the WSDL
     * - documentation: Unmatched STD code does not match and local number has more than 2 digits different
     *
     * @return string 'F'
     */
    public const VALUE_F = 'F';
    /**
     * Constant for value 'FIVE'
     * Meta information extracted from the WSDL
     * - documentation: Partial match STD code does not match and local number is a different length and 2 digits or less is different
     *
     * @return string 'FIVE'
     */
    public const VALUE_FIVE = 'FIVE';
    /**
     * Constant for value 'FOUR'
     * Meta information extracted from the WSDL
     * - documentation: Partial match STD code does not match and local number is the same length but 2 digits or less is different
     *
     * @return string 'FOUR'
     */
    public const VALUE_FOUR = 'FOUR';
    /**
     * Constant for value 'THREE'
     * Meta information extracted from the WSDL
     * - documentation: Partial match STD code does not match but local number is the same length and matches
     *
     * @return string 'THREE'
     */
    public const VALUE_THREE = 'THREE';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_Y
     * @uses self::VALUE_M
     * @uses self::VALUE_S
     * @uses self::VALUE_X
     * @uses self::VALUE_SEVEN
     * @uses self::VALUE_TWO
     * @uses self::VALUE_SIX
     * @uses self::VALUE_ONE
     * @uses self::VALUE_F
     * @uses self::VALUE_FIVE
     * @uses self::VALUE_FOUR
     * @uses self::VALUE_THREE
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_Y,
            self::VALUE_M,
            self::VALUE_S,
            self::VALUE_X,
            self::VALUE_SEVEN,
            self::VALUE_TWO,
            self::VALUE_SIX,
            self::VALUE_ONE,
            self::VALUE_F,
            self::VALUE_FIVE,
            self::VALUE_FOUR,
            self::VALUE_THREE,
        ];
    }
}
