<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for DistanceUnit EnumType
 *
 * @subpackage Enumerations
 */
class DistanceUnit extends AbstractStructEnumBase
{
    /**
     * Constant for value 'mile'
     *
     * @return string 'mile'
     */
    public const VALUE_MILE = 'mile';
    /**
     * Constant for value 'other'
     *
     * @return string 'other'
     */
    public const VALUE_OTHER = 'other';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_OTHER
     * @uses self::VALUE_MILE
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_MILE,
            self::VALUE_OTHER,
        ];
    }
}
