<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for SearchRelationship EnumType
 *
 * @subpackage Enumerations
 */
class SearchRelationship extends AbstractStructEnumBase
{
    /**
     * Constant for value 'associate'
     *
     * @return string 'associate'
     */
    public const VALUE_ASSOCIATE = 'associate';
    /**
     * Constant for value 'joint'
     *
     * @return string 'joint'
     */
    public const VALUE_JOINT = 'joint';
    /**
     * Constant for value 'transient'
     *
     * @return string 'transient'
     */
    public const VALUE_TRANSIENT = 'transient';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_JOINT
     * @uses self::VALUE_TRANSIENT
     * @uses self::VALUE_ASSOCIATE
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_ASSOCIATE,
            self::VALUE_JOINT,
            self::VALUE_TRANSIENT,
        ];
    }
}
