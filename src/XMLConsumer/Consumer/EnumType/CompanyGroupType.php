<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for CompanyGroupType EnumType
 *
 * @subpackage Enumerations
 */
class CompanyGroupType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'bankAndBuildingSociety'
     *
     * @return string 'bankAndBuildingSociety'
     */
    public const VALUE_BANK_AND_BUILDING_SOCIETY = 'bankAndBuildingSociety';
    /**
     * Constant for value 'financialService'
     *
     * @return string 'financialService'
     */
    public const VALUE_FINANCIAL_SERVICE = 'financialService';
    /**
     * Constant for value 'communicationsAndUtility'
     *
     * @return string 'communicationsAndUtility'
     */
    public const VALUE_COMMUNICATIONS_AND_UTILITY = 'communicationsAndUtility';
    /**
     * Constant for value 'retail'
     *
     * @return string 'retail'
     */
    public const VALUE_RETAIL = 'retail';
    /**
     * Constant for value 'rentalAndLeasing'
     *
     * @return string 'rentalAndLeasing'
     */
    public const VALUE_RENTAL_AND_LEASING = 'rentalAndLeasing';
    /**
     * Constant for value 'mailOrder'
     *
     * @return string 'mailOrder'
     */
    public const VALUE_MAIL_ORDER = 'mailOrder';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_FINANCIAL_SERVICE
     * @uses self::VALUE_COMMUNICATIONS_AND_UTILITY
     * @uses self::VALUE_RETAIL
     * @uses self::VALUE_RENTAL_AND_LEASING
     * @uses self::VALUE_MAIL_ORDER
     * @uses self::VALUE_BANK_AND_BUILDING_SOCIETY
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_BANK_AND_BUILDING_SOCIETY,
            self::VALUE_FINANCIAL_SERVICE,
            self::VALUE_COMMUNICATIONS_AND_UTILITY,
            self::VALUE_RETAIL,
            self::VALUE_RENTAL_AND_LEASING,
            self::VALUE_MAIL_ORDER,
        ];
    }
}
