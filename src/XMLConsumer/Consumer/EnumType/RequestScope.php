<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for RequestScope EnumType
 *
 * @subpackage Enumerations
 */
class RequestScope extends AbstractStructEnumBase
{
    /**
     * Constant for value 'supplied'
     *
     * @return string 'supplied'
     */
    public const VALUE_SUPPLIED = 'supplied';
    /**
     * Constant for value 'linked'
     *
     * @return string 'linked'
     */
    public const VALUE_LINKED = 'linked';
    /**
     * Constant for value 'suppliedAndLinked'
     *
     * @return string 'suppliedAndLinked'
     */
    public const VALUE_SUPPLIED_AND_LINKED = 'suppliedAndLinked';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_LINKED
     * @uses self::VALUE_SUPPLIED_AND_LINKED
     * @uses self::VALUE_SUPPLIED
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_SUPPLIED,
            self::VALUE_LINKED,
            self::VALUE_SUPPLIED_AND_LINKED,
        ];
    }
}
