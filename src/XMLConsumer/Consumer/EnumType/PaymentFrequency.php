<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for PaymentFrequency EnumType
 *
 * @subpackage Enumerations
 */
class PaymentFrequency extends AbstractStructEnumBase
{
    /**
     * Constant for value 'annually'
     *
     * @return string 'annually'
     */
    public const VALUE_ANNUALLY = 'annually';
    /**
     * Constant for value 'fortnightly'
     *
     * @return string 'fortnightly'
     */
    public const VALUE_FORTNIGHTLY = 'fortnightly';
    /**
     * Constant for value 'monthly'
     *
     * @return string 'monthly'
     */
    public const VALUE_MONTHLY = 'monthly';
    /**
     * Constant for value 'periodically'
     *
     * @return string 'periodically'
     */
    public const VALUE_PERIODICALLY = 'periodically';
    /**
     * Constant for value 'quarterly'
     *
     * @return string 'quarterly'
     */
    public const VALUE_QUARTERLY = 'quarterly';
    /**
     * Constant for value 'unknown'
     *
     * @return string 'unknown'
     */
    public const VALUE_UNKNOWN = 'unknown';
    /**
     * Constant for value 'weekly'
     *
     * @return string 'weekly'
     */
    public const VALUE_WEEKLY = 'weekly';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_FORTNIGHTLY
     * @uses self::VALUE_MONTHLY
     * @uses self::VALUE_PERIODICALLY
     * @uses self::VALUE_QUARTERLY
     * @uses self::VALUE_UNKNOWN
     * @uses self::VALUE_WEEKLY
     * @uses self::VALUE_ANNUALLY
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_ANNUALLY,
            self::VALUE_FORTNIGHTLY,
            self::VALUE_MONTHLY,
            self::VALUE_PERIODICALLY,
            self::VALUE_QUARTERLY,
            self::VALUE_UNKNOWN,
            self::VALUE_WEEKLY,
        ];
    }
}
