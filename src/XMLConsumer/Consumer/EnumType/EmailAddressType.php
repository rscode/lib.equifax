<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for EmailAddressType EnumType
 *
 * @subpackage Enumerations
 */
class EmailAddressType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'personal'
     *
     * @return string 'personal'
     */
    public const VALUE_PERSONAL = 'personal';
    /**
     * Constant for value 'work'
     *
     * @return string 'work'
     */
    public const VALUE_WORK = 'work';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_WORK
     * @uses self::VALUE_PERSONAL
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_PERSONAL,
            self::VALUE_WORK,
        ];
    }
}
