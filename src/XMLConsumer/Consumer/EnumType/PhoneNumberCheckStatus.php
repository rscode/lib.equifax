<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for PhoneNumberCheckStatus EnumType
 *
 * @subpackage Enumerations
 */
class PhoneNumberCheckStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'matched'
     *
     * @return string 'matched'
     */
    public const VALUE_MATCHED = 'matched';
    /**
     * Constant for value 'other'
     *
     * @return string 'other'
     */
    public const VALUE_OTHER = 'other';
    /**
     * Constant for value 'partiallyMatched'
     *
     * @return string 'partiallyMatched'
     */
    public const VALUE_PARTIALLY_MATCHED = 'partiallyMatched';
    /**
     * Constant for value 'unmatched'
     *
     * @return string 'unmatched'
     */
    public const VALUE_UNMATCHED = 'unmatched';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_OTHER
     * @uses self::VALUE_PARTIALLY_MATCHED
     * @uses self::VALUE_UNMATCHED
     * @uses self::VALUE_MATCHED
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_MATCHED,
            self::VALUE_OTHER,
            self::VALUE_PARTIALLY_MATCHED,
            self::VALUE_UNMATCHED,
        ];
    }
}
