<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for RRRecordType EnumType
 *
 * @subpackage Enumerations
 */
class RRRecordType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'amend'
     *
     * @return string 'amend'
     */
    public const VALUE_AMEND = 'amend';
    /**
     * Constant for value 'delete'
     *
     * @return string 'delete'
     */
    public const VALUE_DELETE = 'delete';
    /**
     * Constant for value 'load'
     *
     * @return string 'load'
     */
    public const VALUE_LOAD = 'load';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_DELETE
     * @uses self::VALUE_LOAD
     * @uses self::VALUE_AMEND
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_AMEND,
            self::VALUE_DELETE,
            self::VALUE_LOAD,
        ];
    }
}
