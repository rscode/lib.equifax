<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for NonCreditSearchType EnumType
 *
 * @subpackage Enumerations
 */
class NonCreditSearchType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'idCheckByOtherPublicSector'
     *
     * @return string 'idCheckByOtherPublicSector'
     */
    public const VALUE_ID_CHECK_BY_OTHER_PUBLIC_SECTOR = 'idCheckByOtherPublicSector';
    /**
     * Constant for value 'applicationFraudInvestigation'
     *
     * @return string 'applicationFraudInvestigation'
     */
    public const VALUE_APPLICATION_FRAUD_INVESTIGATION = 'applicationFraudInvestigation';
    /**
     * Constant for value 'debtCollectionActivity'
     *
     * @return string 'debtCollectionActivity'
     */
    public const VALUE_DEBT_COLLECTION_ACTIVITY = 'debtCollectionActivity';
    /**
     * Constant for value 'fraudInvestigation'
     *
     * @return string 'fraudInvestigation'
     */
    public const VALUE_FRAUD_INVESTIGATION = 'fraudInvestigation';
    /**
     * Constant for value 'idCheckToComplyWithMLRegs'
     *
     * @return string 'idCheckToComplyWithMLRegs'
     */
    public const VALUE_ID_CHECK_TO_COMPLY_WITH_MLREGS = 'idCheckToComplyWithMLRegs';
    /**
     * Constant for value 'insuranceApplication'
     *
     * @return string 'insuranceApplication'
     */
    public const VALUE_INSURANCE_APPLICATION = 'insuranceApplication';
    /**
     * Constant for value 'insuranceQuotation'
     *
     * @return string 'insuranceQuotation'
     */
    public const VALUE_INSURANCE_QUOTATION = 'insuranceQuotation';
    /**
     * Constant for value 'none'
     *
     * @return string 'none'
     */
    public const VALUE_NONE = 'none';
    /**
     * Constant for value 'noSearchType'
     *
     * @return string 'noSearchType'
     */
    public const VALUE_NO_SEARCH_TYPE = 'noSearchType';
    /**
     * Constant for value 'personnelVetting'
     *
     * @return string 'personnelVetting'
     */
    public const VALUE_PERSONNEL_VETTING = 'personnelVetting';
    /**
     * Constant for value 'siranFraud'
     *
     * @return string 'siranFraud'
     */
    public const VALUE_SIRAN_FRAUD = 'siranFraud';
    /**
     * Constant for value 'tenantVerification'
     *
     * @return string 'tenantVerification'
     */
    public const VALUE_TENANT_VERIFICATION = 'tenantVerification';
    /**
     * Constant for value 'creditfileRequest'
     *
     * @return string 'creditfileRequest'
     */
    public const VALUE_CREDITFILE_REQUEST = 'creditfileRequest';
    /**
     * Constant for value 'traceEnquiry'
     *
     * @return string 'traceEnquiry'
     */
    public const VALUE_TRACE_ENQUIRY = 'traceEnquiry';
    /**
     * Constant for value 'beneficiaryTraceEnquiry'
     *
     * @return string 'beneficiaryTraceEnquiry'
     */
    public const VALUE_BENEFICIARY_TRACE_ENQUIRY = 'beneficiaryTraceEnquiry';
    /**
     * Constant for value 'consumerSubjectAccessRequest'
     *
     * @return string 'consumerSubjectAccessRequest'
     */
    public const VALUE_CONSUMER_SUBJECT_ACCESS_REQUEST = 'consumerSubjectAccessRequest';
    /**
     * Constant for value 'consumerEnquiry'
     *
     * @return string 'consumerEnquiry'
     */
    public const VALUE_CONSUMER_ENQUIRY = 'consumerEnquiry';
    /**
     * Constant for value 'authenticationCheck'
     *
     * @return string 'authenticationCheck'
     */
    public const VALUE_AUTHENTICATION_CHECK = 'authenticationCheck';
    /**
     * Constant for value 'identityVerification'
     *
     * @return string 'identityVerification'
     */
    public const VALUE_IDENTITY_VERIFICATION = 'identityVerification';
    /**
     * Constant for value 'publicSectorWithConsent'
     *
     * @return string 'publicSectorWithConsent'
     */
    public const VALUE_PUBLIC_SECTOR_WITH_CONSENT = 'publicSectorWithConsent';
    /**
     * Constant for value 'investigationUnderSection29'
     *
     * @return string 'investigationUnderSection29'
     */
    public const VALUE_INVESTIGATION_UNDER_SECTION_29 = 'investigationUnderSection29';
    /**
     * Constant for value 'investigationUnderSocialSecurityFraudAct'
     *
     * @return string 'investigationUnderSocialSecurityFraudAct'
     */
    public const VALUE_INVESTIGATION_UNDER_SOCIAL_SECURITY_FRAUD_ACT = 'investigationUnderSocialSecurityFraudAct';
    /**
     * Constant for value 'investigationUnderSection35'
     *
     * @return string 'investigationUnderSection35'
     */
    public const VALUE_INVESTIGATION_UNDER_SECTION_35 = 'investigationUnderSection35';
    /**
     * Constant for value 'ipsABIIDCheck'
     *
     * @return string 'ipsABIIDCheck'
     */
    public const VALUE_IPS_ABIIDCHECK = 'ipsABIIDCheck';
    /**
     * Constant for value 'fraudInvestigationByPublicSector'
     *
     * @return string 'fraudInvestigationByPublicSector'
     */
    public const VALUE_FRAUD_INVESTIGATION_BY_PUBLIC_SECTOR = 'fraudInvestigationByPublicSector';
    /**
     * Constant for value 'publicSectorWithCustomerNotification'
     *
     * @return string 'publicSectorWithCustomerNotification'
     */
    public const VALUE_PUBLIC_SECTOR_WITH_CUSTOMER_NOTIFICATION = 'publicSectorWithCustomerNotification';
    /**
     * Constant for value 'creditInsurance'
     *
     * @return string 'creditInsurance'
     */
    public const VALUE_CREDIT_INSURANCE = 'creditInsurance';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_ID_CHECK_BY_OTHER_PUBLIC_SECTOR
     * @uses self::VALUE_APPLICATION_FRAUD_INVESTIGATION
     * @uses self::VALUE_DEBT_COLLECTION_ACTIVITY
     * @uses self::VALUE_FRAUD_INVESTIGATION
     * @uses self::VALUE_ID_CHECK_TO_COMPLY_WITH_MLREGS
     * @uses self::VALUE_INSURANCE_APPLICATION
     * @uses self::VALUE_INSURANCE_QUOTATION
     * @uses self::VALUE_NONE
     * @uses self::VALUE_NO_SEARCH_TYPE
     * @uses self::VALUE_PERSONNEL_VETTING
     * @uses self::VALUE_SIRAN_FRAUD
     * @uses self::VALUE_TENANT_VERIFICATION
     * @uses self::VALUE_CREDITFILE_REQUEST
     * @uses self::VALUE_TRACE_ENQUIRY
     * @uses self::VALUE_BENEFICIARY_TRACE_ENQUIRY
     * @uses self::VALUE_CONSUMER_SUBJECT_ACCESS_REQUEST
     * @uses self::VALUE_CONSUMER_ENQUIRY
     * @uses self::VALUE_AUTHENTICATION_CHECK
     * @uses self::VALUE_IDENTITY_VERIFICATION
     * @uses self::VALUE_PUBLIC_SECTOR_WITH_CONSENT
     * @uses self::VALUE_INVESTIGATION_UNDER_SECTION_29
     * @uses self::VALUE_INVESTIGATION_UNDER_SOCIAL_SECURITY_FRAUD_ACT
     * @uses self::VALUE_INVESTIGATION_UNDER_SECTION_35
     * @uses self::VALUE_IPS_ABIIDCHECK
     * @uses self::VALUE_FRAUD_INVESTIGATION_BY_PUBLIC_SECTOR
     * @uses self::VALUE_PUBLIC_SECTOR_WITH_CUSTOMER_NOTIFICATION
     * @uses self::VALUE_CREDIT_INSURANCE
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_ID_CHECK_BY_OTHER_PUBLIC_SECTOR,
            self::VALUE_APPLICATION_FRAUD_INVESTIGATION,
            self::VALUE_DEBT_COLLECTION_ACTIVITY,
            self::VALUE_FRAUD_INVESTIGATION,
            self::VALUE_ID_CHECK_TO_COMPLY_WITH_MLREGS,
            self::VALUE_INSURANCE_APPLICATION,
            self::VALUE_INSURANCE_QUOTATION,
            self::VALUE_NONE,
            self::VALUE_NO_SEARCH_TYPE,
            self::VALUE_PERSONNEL_VETTING,
            self::VALUE_SIRAN_FRAUD,
            self::VALUE_TENANT_VERIFICATION,
            self::VALUE_CREDITFILE_REQUEST,
            self::VALUE_TRACE_ENQUIRY,
            self::VALUE_BENEFICIARY_TRACE_ENQUIRY,
            self::VALUE_CONSUMER_SUBJECT_ACCESS_REQUEST,
            self::VALUE_CONSUMER_ENQUIRY,
            self::VALUE_AUTHENTICATION_CHECK,
            self::VALUE_IDENTITY_VERIFICATION,
            self::VALUE_PUBLIC_SECTOR_WITH_CONSENT,
            self::VALUE_INVESTIGATION_UNDER_SECTION_29,
            self::VALUE_INVESTIGATION_UNDER_SOCIAL_SECURITY_FRAUD_ACT,
            self::VALUE_INVESTIGATION_UNDER_SECTION_35,
            self::VALUE_IPS_ABIIDCHECK,
            self::VALUE_FRAUD_INVESTIGATION_BY_PUBLIC_SECTOR,
            self::VALUE_PUBLIC_SECTOR_WITH_CUSTOMER_NOTIFICATION,
            self::VALUE_CREDIT_INSURANCE,
        ];
    }
}
