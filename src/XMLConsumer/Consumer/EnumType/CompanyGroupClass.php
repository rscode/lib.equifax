<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for CompanyGroupClass EnumType
 *
 * @subpackage Enumerations
 */
class CompanyGroupClass extends AbstractStructEnumBase
{
    /**
     * Constant for value 'BF_BK_BS'
     *
     * @return string 'BF_BK_BS'
     */
    public const VALUE_BF_BK_BS = 'BF_BK_BS';
    /**
     * Constant for value 'CB_CC_CH_DC_FN_FS_IN_MS'
     *
     * @return string 'CB_CC_CH_DC_FN_FS_IN_MS'
     */
    public const VALUE_CB_CC_CH_DC_FN_FS_IN_MS = 'CB_CC_CH_DC_FN_FS_IN_MS';
    /**
     * Constant for value 'CS_PU'
     *
     * @return string 'CS_PU'
     */
    public const VALUE_CS_PU = 'CS_PU';
    /**
     * Constant for value 'HF_RT'
     *
     * @return string 'HF_RT'
     */
    public const VALUE_HF_RT = 'HF_RT';
    /**
     * Constant for value 'LG_RN_SR_TV'
     *
     * @return string 'LG_RN_SR_TV'
     */
    public const VALUE_LG_RN_SR_TV = 'LG_RN_SR_TV';
    /**
     * Constant for value 'MO'
     *
     * @return string 'MO'
     */
    public const VALUE_MO = 'MO';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_CB_CC_CH_DC_FN_FS_IN_MS
     * @uses self::VALUE_CS_PU
     * @uses self::VALUE_HF_RT
     * @uses self::VALUE_LG_RN_SR_TV
     * @uses self::VALUE_MO
     * @uses self::VALUE_BF_BK_BS
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_BF_BK_BS,
            self::VALUE_CB_CC_CH_DC_FN_FS_IN_MS,
            self::VALUE_CS_PU,
            self::VALUE_HF_RT,
            self::VALUE_LG_RN_SR_TV,
            self::VALUE_MO,
        ];
    }
}
