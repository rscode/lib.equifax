<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for DOBVerificationStatus EnumType
 *
 * @subpackage Enumerations
 */
class DOBVerificationStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'matchedDDMM'
     *
     * @return string 'matchedDDMM'
     */
    public const VALUE_MATCHED_DDMM = 'matchedDDMM';
    /**
     * Constant for value 'matchedDDOnly'
     *
     * @return string 'matchedDDOnly'
     */
    public const VALUE_MATCHED_DDONLY = 'matchedDDOnly';
    /**
     * Constant for value 'matchedDDYY'
     *
     * @return string 'matchedDDYY'
     */
    public const VALUE_MATCHED_DDYY = 'matchedDDYY';
    /**
     * Constant for value 'matchedMMOnly'
     *
     * @return string 'matchedMMOnly'
     */
    public const VALUE_MATCHED_MMONLY = 'matchedMMOnly';
    /**
     * Constant for value 'matchedMMYY'
     *
     * @return string 'matchedMMYY'
     */
    public const VALUE_MATCHED_MMYY = 'matchedMMYY';
    /**
     * Constant for value 'matchedToSuppliedDOB'
     *
     * @return string 'matchedToSuppliedDOB'
     */
    public const VALUE_MATCHED_TO_SUPPLIED_DOB = 'matchedToSuppliedDOB';
    /**
     * Constant for value 'matchedYYOnly'
     *
     * @return string 'matchedYYOnly'
     */
    public const VALUE_MATCHED_YYONLY = 'matchedYYOnly';
    /**
     * Constant for value 'noAccessToData'
     *
     * @return string 'noAccessToData'
     */
    public const VALUE_NO_ACCESS_TO_DATA = 'noAccessToData';
    /**
     * Constant for value 'noDataHeld'
     *
     * @return string 'noDataHeld'
     */
    public const VALUE_NO_DATA_HELD = 'noDataHeld';
    /**
     * Constant for value 'noMatchToSuppledDOBWholeDate'
     *
     * @return string 'noMatchToSuppledDOBWholeDate'
     */
    public const VALUE_NO_MATCH_TO_SUPPLED_DOBWHOLE_DATE = 'noMatchToSuppledDOBWholeDate';
    /**
     * Constant for value 'other'
     *
     * @return string 'other'
     */
    public const VALUE_OTHER = 'other';
    /**
     * Constant for value 'transposedMMDD'
     *
     * @return string 'transposedMMDD'
     */
    public const VALUE_TRANSPOSED_MMDD = 'transposedMMDD';
    /**
     * Constant for value 'under18DateNotChecked'
     *
     * @return string 'under18DateNotChecked'
     */
    public const VALUE_UNDER_18_DATE_NOT_CHECKED = 'under18DateNotChecked';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_MATCHED_DDMM
     * @uses self::VALUE_MATCHED_DDONLY
     * @uses self::VALUE_MATCHED_DDYY
     * @uses self::VALUE_MATCHED_MMONLY
     * @uses self::VALUE_MATCHED_MMYY
     * @uses self::VALUE_MATCHED_TO_SUPPLIED_DOB
     * @uses self::VALUE_MATCHED_YYONLY
     * @uses self::VALUE_NO_ACCESS_TO_DATA
     * @uses self::VALUE_NO_DATA_HELD
     * @uses self::VALUE_NO_MATCH_TO_SUPPLED_DOBWHOLE_DATE
     * @uses self::VALUE_OTHER
     * @uses self::VALUE_TRANSPOSED_MMDD
     * @uses self::VALUE_UNDER_18_DATE_NOT_CHECKED
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_MATCHED_DDMM,
            self::VALUE_MATCHED_DDONLY,
            self::VALUE_MATCHED_DDYY,
            self::VALUE_MATCHED_MMONLY,
            self::VALUE_MATCHED_MMYY,
            self::VALUE_MATCHED_TO_SUPPLIED_DOB,
            self::VALUE_MATCHED_YYONLY,
            self::VALUE_NO_ACCESS_TO_DATA,
            self::VALUE_NO_DATA_HELD,
            self::VALUE_NO_MATCH_TO_SUPPLED_DOBWHOLE_DATE,
            self::VALUE_OTHER,
            self::VALUE_TRANSPOSED_MMDD,
            self::VALUE_UNDER_18_DATE_NOT_CHECKED,
        ];
    }
}
