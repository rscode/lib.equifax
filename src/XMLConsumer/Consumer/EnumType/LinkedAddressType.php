<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for LinkedAddressType EnumType
 * Meta information extracted from the WSDL
 * - documentation: Relationship of the linked address to one of the supplied addresses.
 *
 * @subpackage Enumerations
 */
class LinkedAddressType extends AbstractStructEnumBase
{
    /**
     * Indicates that the linked address is AFTER the address that was searched on in the customer's address chain.  The searched address is
     * not the most recent address for the customer, and likely doesn't live there anymore.
     */
    public const VALUE_NEXT_FROM_SUPPLIED_CURRENT = 'nextFromSuppliedCurrent';
    public const VALUE_PREVIOUS_FROM_SUPPLIED_PREVIOUS = 'previousFromSuppliedPrevious';
    /**
     * Indicates that the linked address is BEFORE the address that was searched on in the customer's address chain.  The linked address
     * is an old address for the customer.
     */
    public const VALUE_PREVIOUS_FROM_SUPPLIED_CURRENT = 'previousFromSuppliedCurrent';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_PREVIOUS_FROM_SUPPLIED_PREVIOUS
     * @uses self::VALUE_PREVIOUS_FROM_SUPPLIED_CURRENT
     * @uses self::VALUE_NEXT_FROM_SUPPLIED_CURRENT
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_NEXT_FROM_SUPPLIED_CURRENT,
            self::VALUE_PREVIOUS_FROM_SUPPLIED_PREVIOUS,
            self::VALUE_PREVIOUS_FROM_SUPPLIED_CURRENT,
        ];
    }
}
