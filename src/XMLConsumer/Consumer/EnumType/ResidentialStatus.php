<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for ResidentialStatus EnumType
 *
 * @subpackage Enumerations
 */
class ResidentialStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'livingWithParents'
     *
     * @return string 'livingWithParents'
     */
    public const VALUE_LIVING_WITH_PARENTS = 'livingWithParents';
    /**
     * Constant for value 'mortgage'
     *
     * @return string 'mortgage'
     */
    public const VALUE_MORTGAGE = 'mortgage';
    /**
     * Constant for value 'owner'
     *
     * @return string 'owner'
     */
    public const VALUE_OWNER = 'owner';
    /**
     * Constant for value 'tenant'
     *
     * @return string 'tenant'
     */
    public const VALUE_TENANT = 'tenant';
    /**
     * Constant for value 'other'
     *
     * @return string 'other'
     */
    public const VALUE_OTHER = 'other';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_MORTGAGE
     * @uses self::VALUE_OWNER
     * @uses self::VALUE_TENANT
     * @uses self::VALUE_OTHER
     * @uses self::VALUE_LIVING_WITH_PARENTS
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_LIVING_WITH_PARENTS,
            self::VALUE_MORTGAGE,
            self::VALUE_OWNER,
            self::VALUE_TENANT,
            self::VALUE_OTHER,
        ];
    }
}
