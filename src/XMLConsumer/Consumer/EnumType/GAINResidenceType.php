<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GAINResidenceType EnumType
 *
 * @subpackage Enumerations
 */
class GAINResidenceType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'currentAddress'
     *
     * @return string 'currentAddress'
     */
    public const VALUE_CURRENT_ADDRESS = 'currentAddress';
    /**
     * Constant for value 'previousAddress'
     *
     * @return string 'previousAddress'
     */
    public const VALUE_PREVIOUS_ADDRESS = 'previousAddress';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_PREVIOUS_ADDRESS
     * @uses self::VALUE_CURRENT_ADDRESS
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_CURRENT_ADDRESS,
            self::VALUE_PREVIOUS_ADDRESS,
        ];
    }
}
