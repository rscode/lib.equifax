<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for AddressMatchingStatus EnumType
 *
 * @subpackage Enumerations
 */
class AddressMatchingStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'multipleMatch'
     *
     * @return string 'multipleMatch'
     */
    public const VALUE_MULTIPLE_MATCH = 'multipleMatch';
    /**
     * Constant for value 'noMatch'
     *
     * @return string 'noMatch'
     */
    public const VALUE_NO_MATCH = 'noMatch';
    /**
     * Constant for value 'singleMatch'
     *
     * @return string 'singleMatch'
     */
    public const VALUE_SINGLE_MATCH = 'singleMatch';
    /**
     * Constant for value 'preMatched'
     *
     * @return string 'preMatched'
     */
    public const VALUE_PRE_MATCHED = 'preMatched';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_NO_MATCH
     * @uses self::VALUE_SINGLE_MATCH
     * @uses self::VALUE_PRE_MATCHED
     * @uses self::VALUE_MULTIPLE_MATCH
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_MULTIPLE_MATCH,
            self::VALUE_NO_MATCH,
            self::VALUE_SINGLE_MATCH,
            self::VALUE_PRE_MATCHED,
        ];
    }
}
