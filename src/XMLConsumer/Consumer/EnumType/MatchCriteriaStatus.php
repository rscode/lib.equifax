<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for MatchCriteriaStatus EnumType
 *
 * @subpackage Enumerations
 */
class MatchCriteriaStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'notRequired'
     *
     * @return string 'notRequired'
     */
    public const VALUE_NOT_REQUIRED = 'notRequired';
    /**
     * Constant for value 'required'
     *
     * @return string 'required'
     */
    public const VALUE_REQUIRED = 'required';
    /**
     * Constant for value 'onlyForScores'
     *
     * @return string 'onlyForScores'
     */
    public const VALUE_ONLY_FOR_SCORES = 'onlyForScores';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_REQUIRED
     * @uses self::VALUE_ONLY_FOR_SCORES
     * @uses self::VALUE_NOT_REQUIRED
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_NOT_REQUIRED,
            self::VALUE_REQUIRED,
            self::VALUE_ONLY_FOR_SCORES,
        ];
    }
}
