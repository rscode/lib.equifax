<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for TelephoneLineType EnumType
 *
 * @subpackage Enumerations
 */
class TelephoneLineType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'fax'
     *
     * @return string 'fax'
     */
    public const VALUE_FAX = 'fax';
    /**
     * Constant for value 'mobile'
     *
     * @return string 'mobile'
     */
    public const VALUE_MOBILE = 'mobile';
    /**
     * Constant for value 'noLineTypeNotMatched'
     *
     * @return string 'noLineTypeNotMatched'
     */
    public const VALUE_NO_LINE_TYPE_NOT_MATCHED = 'noLineTypeNotMatched';
    /**
     * Constant for value 'normal'
     *
     * @return string 'normal'
     */
    public const VALUE_NORMAL = 'normal';
    /**
     * Constant for value 'other'
     *
     * @return string 'other'
     */
    public const VALUE_OTHER = 'other';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_MOBILE
     * @uses self::VALUE_NO_LINE_TYPE_NOT_MATCHED
     * @uses self::VALUE_NORMAL
     * @uses self::VALUE_OTHER
     * @uses self::VALUE_FAX
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_FAX,
            self::VALUE_MOBILE,
            self::VALUE_NO_LINE_TYPE_NOT_MATCHED,
            self::VALUE_NORMAL,
            self::VALUE_OTHER,
        ];
    }
}
