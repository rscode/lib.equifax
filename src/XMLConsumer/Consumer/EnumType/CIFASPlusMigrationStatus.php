<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for CIFASPlusMigrationStatus EnumType
 *
 * @subpackage Enumerations
 */
class CIFASPlusMigrationStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'fullyMigrated'
     *
     * @return string 'fullyMigrated'
     */
    public const VALUE_FULLY_MIGRATED = 'fullyMigrated';
    /**
     * Constant for value 'notMigrated'
     *
     * @return string 'notMigrated'
     */
    public const VALUE_NOT_MIGRATED = 'notMigrated';
    /**
     * Constant for value 'partiallyMigratedTypeA'
     *
     * @return string 'partiallyMigratedTypeA'
     */
    public const VALUE_PARTIALLY_MIGRATED_TYPE_A = 'partiallyMigratedTypeA';
    /**
     * Constant for value 'partiallyMigratedTypeS'
     *
     * @return string 'partiallyMigratedTypeS'
     */
    public const VALUE_PARTIALLY_MIGRATED_TYPE_S = 'partiallyMigratedTypeS';
    /**
     * Constant for value 'partiallyMigratedTypeT'
     *
     * @return string 'partiallyMigratedTypeT'
     */
    public const VALUE_PARTIALLY_MIGRATED_TYPE_T = 'partiallyMigratedTypeT';
    /**
     * Constant for value 'unknown'
     *
     * @return string 'unknown'
     */
    public const VALUE_UNKNOWN = 'unknown';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_NOT_MIGRATED
     * @uses self::VALUE_PARTIALLY_MIGRATED_TYPE_A
     * @uses self::VALUE_PARTIALLY_MIGRATED_TYPE_S
     * @uses self::VALUE_PARTIALLY_MIGRATED_TYPE_T
     * @uses self::VALUE_UNKNOWN
     * @uses self::VALUE_FULLY_MIGRATED
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_FULLY_MIGRATED,
            self::VALUE_NOT_MIGRATED,
            self::VALUE_PARTIALLY_MIGRATED_TYPE_A,
            self::VALUE_PARTIALLY_MIGRATED_TYPE_S,
            self::VALUE_PARTIALLY_MIGRATED_TYPE_T,
            self::VALUE_UNKNOWN,
        ];
    }
}
