<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for AssociateLevel EnumType
 *
 * @subpackage Enumerations
 */
class AssociateLevel extends AbstractStructEnumBase
{
    /**
     * Constant for value 'known'
     *
     * @return string 'known'
     */
    public const VALUE_KNOWN = 'known';
    /**
     * Constant for value 'potential'
     *
     * @return string 'potential'
     */
    public const VALUE_POTENTIAL = 'potential';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_POTENTIAL
     * @uses self::VALUE_KNOWN
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_KNOWN,
            self::VALUE_POTENTIAL,
        ];
    }
}
