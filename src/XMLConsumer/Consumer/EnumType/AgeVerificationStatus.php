<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for AgeVerificationStatus EnumType
 *
 * @subpackage Enumerations
 */
class AgeVerificationStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'other'
     *
     * @return string 'other'
     */
    public const VALUE_OTHER = 'other';
    /**
     * Constant for value 'over18'
     *
     * @return string 'over18'
     */
    public const VALUE_OVER_18 = 'over18';
    /**
     * Constant for value 'under18'
     *
     * @return string 'under18'
     */
    public const VALUE_UNDER_18 = 'under18';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_OVER_18
     * @uses self::VALUE_UNDER_18
     * @uses self::VALUE_OTHER
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_OTHER,
            self::VALUE_OVER_18,
            self::VALUE_UNDER_18,
        ];
    }
}
