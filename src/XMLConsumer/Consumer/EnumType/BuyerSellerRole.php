<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for BuyerSellerRole EnumType
 *
 * @subpackage Enumerations
 */
class BuyerSellerRole extends AbstractStructEnumBase
{
    /**
     * Constant for value 'buyerOrApplicant'
     *
     * @return string 'buyerOrApplicant'
     */
    public const VALUE_BUYER_OR_APPLICANT = 'buyerOrApplicant';
    /**
     * Constant for value 'sellerOrGrantor'
     *
     * @return string 'sellerOrGrantor'
     */
    public const VALUE_SELLER_OR_GRANTOR = 'sellerOrGrantor';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_SELLER_OR_GRANTOR
     * @uses self::VALUE_BUYER_OR_APPLICANT
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_BUYER_OR_APPLICANT,
            self::VALUE_SELLER_OR_GRANTOR,
        ];
    }
}
