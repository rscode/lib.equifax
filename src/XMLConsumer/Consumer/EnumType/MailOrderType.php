<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for MailOrderType EnumType
 *
 * @subpackage Enumerations
 */
class MailOrderType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'cash'
     *
     * @return string 'cash'
     */
    public const VALUE_CASH = 'cash';
    /**
     * Constant for value 'direct'
     *
     * @return string 'direct'
     */
    public const VALUE_DIRECT = 'direct';
    /**
     * Constant for value 'standard'
     *
     * @return string 'standard'
     */
    public const VALUE_STANDARD = 'standard';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_DIRECT
     * @uses self::VALUE_STANDARD
     * @uses self::VALUE_CASH
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_CASH,
            self::VALUE_DIRECT,
            self::VALUE_STANDARD,
        ];
    }
}
