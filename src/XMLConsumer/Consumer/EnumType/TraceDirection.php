<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for TraceDirection EnumType
 *
 * @subpackage Enumerations
 */
class TraceDirection extends AbstractStructEnumBase
{
    /**
     * Constant for value 'next'
     *
     * @return string 'next'
     */
    public const VALUE_NEXT = 'next';
    /**
     * Constant for value 'previous'
     *
     * @return string 'previous'
     */
    public const VALUE_PREVIOUS = 'previous';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_PREVIOUS
     * @uses self::VALUE_NEXT
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_NEXT,
            self::VALUE_PREVIOUS,
        ];
    }
}
