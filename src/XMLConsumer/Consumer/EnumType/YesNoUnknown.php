<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for YesNoUnknown EnumType
 *
 * @subpackage Enumerations
 */
class YesNoUnknown extends AbstractStructEnumBase
{
    /**
     * Constant for value 'no'
     *
     * @return string 'no'
     */
    public const VALUE_NO = 'no';
    /**
     * Constant for value 'yes'
     *
     * @return string 'yes'
     */
    public const VALUE_YES = 'yes';
    /**
     * Constant for value 'unknown'
     *
     * @return string 'unknown'
     */
    public const VALUE_UNKNOWN = 'unknown';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_YES
     * @uses self::VALUE_UNKNOWN
     * @uses self::VALUE_NO
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_NO,
            self::VALUE_YES,
            self::VALUE_UNKNOWN,
        ];
    }
}
