<?php

declare(strict_types=1);

namespace Ratespecial\Equifax\XMLConsumer\Consumer\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for ERSeniorityType EnumType
 *
 * @subpackage Enumerations
 */
class ERSeniorityType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'junior'
     *
     * @return string 'junior'
     */
    public const VALUE_JUNIOR = 'junior';
    /**
     * Constant for value 'senior'
     *
     * @return string 'senior'
     */
    public const VALUE_SENIOR = 'senior';
    /**
     * Constant for value 'unknown'
     *
     * @return string 'unknown'
     */
    public const VALUE_UNKNOWN = 'unknown';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_SENIOR
     * @uses self::VALUE_UNKNOWN
     * @uses self::VALUE_JUNIOR
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_JUNIOR,
            self::VALUE_SENIOR,
            self::VALUE_UNKNOWN,
        ];
    }
}
