<?php

namespace Ratespecial\Equifax\XMLConsumer\Consumer;

use stdClass;

/**
 * Takes a report and removes any elements that have a nameMatchStatus that isn't A.  If no nameMatchStatus field is present in an object,
 * it is retained.
 */
class ReportFilter
{
    public function removeNonCustomerData(mixed $data)
    {
        // Skip any object where name match is not A (the customer themselves)
        if (
            is_object($data)
            && property_exists($data, 'nameMatchStatus')
            && $data->nameMatchStatus !== 'A'
        ) {
            return null;
        }

        if (is_string($data)) {
            return $data;
        }

        $filteredObject = new stdClass();

        foreach ($data as $key => $value) {
            if (is_object($value)) {
                $filteredObject->$key = self::removeNonCustomerData($value);
            } elseif (is_array($value)) {
                foreach ($value as $element) {
                    $tempElement = self::removeNonCustomerData($element);
                    if ($tempElement) {
                        $filteredObject->$key[] = $tempElement;
                    }
                }
            } else {
                $filteredObject->$key = $value;
            }
        }

        return $filteredObject;
    }
}
