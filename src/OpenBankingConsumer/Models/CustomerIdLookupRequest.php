<?php

namespace Ratespecial\Equifax\OpenBankingConsumer\Models;

class CustomerIdLookupRequest extends AbstractOpenBankingRequest
{
    protected string $httpVerb = 'GET';
    protected string $urlPath = '/open-banking-insights/v1/general/lookup/customerId';

    protected string $externalReference = '';

    public function getExternalReference(): string
    {
        return $this->externalReference;
    }

    public function setExternalReference(string $externalReference): self
    {
        $this->externalReference = $externalReference;

        return $this;
    }

    public function getResponseClass(): string
    {
        return CustomerIdLookupResponse::class;
    }
}
