<?php

namespace Ratespecial\Equifax\OpenBankingConsumer\Models;

abstract class AbstractOpenBankingRequest
{
    protected string $urlPath = '';
    protected string $httpVerb = '';

    public function __construct(array $data = [])
    {
        if (!empty($data)) {
            $properties = get_object_vars($this);
            unset($properties['urlPath']);
            unset($properties['httpVerb']);

            $allowedData = array_intersect_key($data, $properties);
            array_walk($allowedData, fn($val, $key) => $this->$key = $val);
        }
    }

    /**
     * Return all variables to be sent in the request
     *
     * @return array
     */
    public function getParams(): array
    {
        $vars = get_object_vars($this);

        unset($vars['urlPath']);
        unset($vars['httpVerb']);

        return $vars;
    }

    /**
     * Return class name minus namespaces
     *
     * @return string
     */
    public function getCommandName(): string
    {
        $tmp = explode('\\', get_class($this));

        return end($tmp);
    }

    public function getUrlPath(): string
    {
        return $this->urlPath;
    }

    public function getHttpVerb(): string
    {
        return $this->httpVerb;
    }

    /**
     * Name of the class that will be mapped to the response of this request.
     *
     * @return string
     */
    abstract public function getResponseClass(): string;
}
