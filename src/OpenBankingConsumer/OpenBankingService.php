<?php

namespace Ratespecial\Equifax\OpenBankingConsumer;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use JsonMapper;
use JsonMapper_Exception;
use Ratespecial\Equifax\Common\AccessToken;
use Ratespecial\Equifax\Common\EquifaxApiProduct;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Common\TokenGenerationServiceInterface;
use Ratespecial\Equifax\Exceptions\MissingAccessTokenException;
use Ratespecial\Equifax\OpenBankingConsumer\Models\AbstractOpenBankingRequest;

class OpenBankingService implements TokenGenerationServiceInterface
{
    protected JsonMapper $mapper;

    public function __construct(
        protected readonly Client $client,
        public readonly EquifaxEnvironment $environment,
        protected ?AccessToken $accessToken = null,
    ) {
        $this->mapper = new JsonMapper();

        // If this function exists on the mapped object, it will be called at the end.  Good for re-organizing an object once
        // all data is present.
        $this->mapper->postMappingMethod = 'afterMapping';

        // If there are some new fields in the record that our class doesn't define, set them in the object.
        $this->mapper->undefinedPropertyHandler = function ($object, $propName, $jsonValue) {
            $object->$propName = $jsonValue;
        };
    }

    public function getServiceName(): EquifaxApiProduct
    {
        return EquifaxApiProduct::OPEN_BANKING_CONSUMER;
    }

    public function getEnvironment(): EquifaxEnvironment
    {
        return $this->environment;
    }

    /**
     * @param string $clientId
     * @param string $clientSecret
     * @return AccessToken
     * @throws GuzzleException
     * @throws JsonMapper_Exception
     */
    public function generateAccessToken(string $clientId, string $clientSecret): AccessToken
    {
//        try {
        $resp = $this->client->post('/security-service/v1/token', [
            RequestOptions::HEADERS     => [
                'Accept' => 'application/json',
            ],
            RequestOptions::FORM_PARAMS => [
                'grant_type' => 'client_credentials',
                'scope'      => 'sa.readprofile',
            ],
            RequestOptions::AUTH        => [$clientId, $clientSecret],
        ]);
//        } catch (BadResponseException $ex) {
//            throw new LuminateException($ex);
//        }

        $data = json_decode((string)$resp->getBody());

        $token = $this->mapper->map($data, new AccessToken());

        $this->accessToken = $token;

        return $token;
    }

    /**
     * Executes any call except generating an access token
     *
     * @throws MissingAccessTokenException
     * @throws JsonMapper_Exception
     * @throws GuzzleException
     */
    public function execute(AbstractOpenBankingRequest $request)
    {
        if (!$this->accessToken) {
            throw new MissingAccessTokenException('Missing access token during request');
        }

        $method = $request->getHttpVerb();

        $options = [
            RequestOptions::HEADERS => [
                'Accept'        => 'application/json',
                'Authorization' => 'Bearer ' . $this->accessToken,
            ]
        ];

        if ($request->getHttpVerb() == 'POST') {
            $options[RequestOptions::JSON] = $request->getParams();
        } elseif ($request->getHttpVerb() == 'GET' && !empty($request->getParams())) {
            $options[RequestOptions::QUERY] = $request->getParams();
        }

//        try {

        $resp = $this->client->$method($request->getUrlPath(), $options);

//        } catch (BadResponseException $ex) {
//            throw new LuminateException($ex);
//        }

        $data = json_decode((string)$resp->getBody());

        $responseClass = $request->getResponseClass();

        return $this->mapper->map($data, new $responseClass());
    }
}
