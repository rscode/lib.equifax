# Equifax SDK

## Introduction

This library was built for use in the United Kingdom.  It's highly likely that entities will differ from the United States.

Covers services:

* Luminate
* XML2 Consumer
* Open Banking Consumer

Contains Laravel Service Provider.

## Install

Via Composer

``` bash
$ composer require ratespecial/equifax
```

## Access Tokens

When talking to any API, an access token must be provided with each request.  Each service has its own authenticate API endpoint to 
generate the access token for you.  See the [TokenGenerationServiceInterface](src/Common/TokenGenerationServiceInterface.php) and it's 
implementations.  This authentication end point will require either a username & password (Luminate) or a client id & secret (XML Consumer, Open Banking)

This token expires in 24 hours, can be used on multiple requests, and therefore should be cached.
The [AccessTokenCacheInterface](src/Common/AccessTokenCacheInterface.php) helps faciliate
this.  [SimpleTokenCache](src/Common/SimpleTokenCache.php) is provided
which implements `Psr\SimpleCache\CacheInterface`, but any other cache system can be used.

A new token should be retrieved before it expires to prevent
interruption.  [RefreshAccessTokenService](src/Common/RefreshAccessTokenService.php) is designed to be run every x minutes to check if the
token expiration is within a threshold of y minutes.  If so, it will fetch and cache a new token.   

With Laravel, the [RefreshAccessTokenFactory](src/Common/RefreshAccessTokenFactory.php) can be used to generate `RefreshAccessTokenService`
instances based on config variables. There is a Laravel command
called [MaintainAccessTokensCommand](src/Laravel/Commands/MaintainAccessTokensCommand.php) that can be invoked regularly to keep a live
token ready to go.

### Client Secret Cycling

A client ID and secret are required to generate an access token for the XML Consumer service. The client secret expires after 30 days
and needs to be changed.  [ClientSecretCyclerService](src/XMLConsumer/Security/Credentials/ClientSecretCyclerService.php) assists with
updating the secret a few days before expiration, and should be invoked
daily.  [CredentialsStoreInterface](src/XMLConsumer/Security/Credentials/CredentialsStoreInterface.php) needs to be implemented using your
own environment's storage system. 

Be aware the change client secret API requires a valid access token.

With Laravel, the [ClientSecretCyclerFactory](src/XMLConsumer/Security/Credentials/ClientSecretCyclerFactory.php) can be used to generate
`ClientSecretCyclerService` instances based on config variables. There is a Laravel command
called [MaintainXmlConsumerSecretCommand](src/Laravel/Commands/MaintainXmlConsumerSecretCommand.php) that can be invoked regularly.

## Usage

### Laravel configuration

You should examine [the config file](config/equifax.php) to see what options are available. Different credentials must be supplied
depending on which Equifax services will be used.

### Luminate

Required env variables:  TODO

```dotenv
EQUIFAX_LUMINATE_USERNAME=
EQUIFAX_LUMINATE_PASSWORD=
EQUIFAX_LUMINATE_PRODUCT_ORG=
```

With Laravel, service providers will be auto discovered.

```php
use Ratespecial\Equifax\Luminate\LuminateService;
use Ratespecial\Equifax\Common\RefreshAccessTokenService;
use Illuminate\Support\Facades\App;

public function process(LuminateService $luminateService)
{
    /** @var RefreshAccessTokenService $refreshTokenService */
    $refreshTokenService = App::make('luminate-credentials-refresh');
    
    // TODO
    $refreshTokenService->refreshIfNecessary()
}
```

Without Laravel, services must be built.

```php

```

## Testing

### Adhoc testing

To play around with the APIs and see responses from Equifax directly, there are a number of test classes that can be altered with new data
from the test pack and run manually.  Since they are set to hit Equifax' servers, they are excluded from the PHPUnit test suite.

* `tests/Luminate/LuminateServiceSandboxTest`
* `tests/XMLConsumer/Address/AddressServiceUatTest`
* `tests/XMLConsumer/Consumer/ConsumerServiceUatTest`
* `tests/XMLConsumer/Security/SecurityServiceUatTest`

**To use the XMLConsumer APIs:**

1. Copy `tests/.env.example` to `/tests/.env`
2. Fill out the `EQUIFAX_XML2_*` environment variables with your test credentials
3. Execute `SecurityServiceUatTest::testLogon()`.  This will use the credentials to retrieve an access token and store it in `tests/cache/xml-access-token`
4. Other calls to `AddressServiceUatTest`, `ConsumerServiceUatTest`, and `SecurityServiceUatTest` will now use that cached token.  Try it out with `AddressServiceUatTest::testPing()`

**To use the Luminate APIs:**

1. Copy `tests/.env.example` to `/tests/.env`
2. Fill out the `EQUIFAX_LUMINATE_*` environment variables with your test credentials
3. Execute `LuminateServiceUatTest::testGetToken()`.  This will use the credentials to retrieve an access token and store it in `tests/cache/luminate-access-token`
4. Other calls to `LuminateServiceUatTest` will now use that cached token.  Try it out with `LuminateServiceUatTest::testIdentityAssuranceInitial()`

## WSDL to PHP process

### WSDL location

UAT and production WSDL packages [can be found here](https://developer.equifax.co.uk/sites/uk/files/xml_ref_guide/guide/gettingStarted.html).

### Process

**NOTE: This process is incomplete.  There have been so many tweaks and changes, it is not advised to redo this process unless there
is a major change to their service.  It might be best to start from scratch.**

1. `composer run-script wsdl-to-php`
2. Compile service type into consolidated service class, such as `SecurityService` and `ConsumerService`. Remove `ServiceType` folder
3. The following classes are duplicated between the services. They have been moved to the `Common` folder. References to these classes in
   the services / other structs should be changed to use the common version. Remove the copies in the individual services. Be sure to update
   the mapping in the `ClassMap` classes.
    1. `EWSFault`
    1. `PingRequest`
    1. `PingResponse`
    1. `ServiceDetail`

## External links

https://developer.equifax.co.uk/sites/uk/files/xml_ref_guide/guide/single.pdf

## Future upgrades

* `InvalidAccessToken` exception
* Laravel events for successful/failed token cycling, secret cycling