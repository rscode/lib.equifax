<?php
/*
 * For use with Laravel
 */

use Ratespecial\Equifax\Common\EquifaxApiProduct;
use Ratespecial\Equifax\Common\EquifaxEnvironment;

return [
    // Number of minutes before an access token expires that we should refresh it.
    'refresh-token-threshold' => env('EQUIFAX_REFRESH_TOKEN_THRESHOLD', 60),

    // When MaintainAccessTokensCommand encounters invalid credentials, this is the number of minutes before the next retry.
    'refresh-failure-delay' => env('EQUIFAX_REFRESH_FAILURE_DELAY', 1440),

    // Default Equifax environment to act on if no other is supplied.
    'default-env' => env('EQUIFAX_DEFAULT_ENV', EquifaxEnvironment::PRODUCTION->value),

    EquifaxApiProduct::LUMINATE->value => [
        EquifaxEnvironment::PRODUCTION->value => [
            'client-organization' => env('EQUIFAX_LUMINATE_PROD_CLIENT_ORG', ''),
            'username'            => env('EQUIFAX_LUMINATE_PROD_USERNAME', ''),
            // Password comes from PasswordStoreInterface

            'tenant-id'      => env('EQUIFAX_LUMINATE_PROD_TENANTID', ''),
            'application-id' => env('EQUIFAX_LUMINATE_PROD_APPID', ''),
            'config-id'      => env('EQUIFAX_LUMINATE_PROD_CONFIGID', ''),
        ],

        EquifaxEnvironment::UAT->value => [
            'client-organization' => env('EQUIFAX_LUMINATE_UAT_CLIENT_ORG', ''),
            'username'            => env('EQUIFAX_LUMINATE_UAT_USERNAME', ''),
            'password'            => env('EQUIFAX_LUMINATE_UAT_PASSWORD', ''),

            'tenant-id'      => env('EQUIFAX_LUMINATE_UAT_TENANTID', ''),
            'application-id' => env('EQUIFAX_LUMINATE_UAT_APPID', ''),
            'config-id'      => env('EQUIFAX_LUMINATE_UAT_CONFIGID', ''),
        ],

        EquifaxEnvironment::SANDBOX->value => [
            'client-organization' => env('EQUIFAX_LUMINATE_SANDBOX_CLIENT_ORG', ''),
            'username'            => env('EQUIFAX_LUMINATE_SANDBOX_USERNAME', ''),
            'password'            => env('EQUIFAX_LUMINATE_SANDBOX_PASSWORD', ''),

            'tenant-id'      => env('EQUIFAX_LUMINATE_SANDBOX_TENANTID', ''),
            'application-id' => env('EQUIFAX_LUMINATE_SANDBOX_APPID', ''),
            'config-id'      => env('EQUIFAX_LUMINATE_SANDBOX_CONFIGID', ''),
        ],
    ],

    EquifaxApiProduct::XML_CONSUMER->value => [
        EquifaxEnvironment::PRODUCTION->value => [
            'client-id'     => env('EQUIFAX_XML2_PROD_CLIENT_ID', ''),
            // Client secret comes from CredentialsStoreInterface

            // TODO
            'security-service-wsdl' => env('EQUIFAX_XML2_PROD_SECURITY_WSDL', realpath(__DIR__ . '/../wsdl/EWSSecurity UATWsdl Version4_0/securityService_UAT.wsdl')),
            'consumer-service-wsdl' => env('EQUIFAX_XML2_PROD_CONSUMER_WSDL', realpath(__DIR__ . '/../wsdl/EWSConsumer UATWsdl Version4_0/consumerService_uat.wsdl')),
            'address-service-wsdl'  => env('EQUIFAX_XML2_PROD_ADDRESS_WSDL', realpath(__DIR__ . '/../wsdl/EWSAddress UATWsdl Version4_0/addressService_uat.wsdl')),
        ],

        EquifaxEnvironment::UAT->value => [
            'client-id'     => env('EQUIFAX_XML2_UAT_CLIENT_ID', ''),
            // Client secret comes from CredentialsStoreInterface

            'security-service-wsdl' => env('EQUIFAX_XML2_UAT_SECURITY_WSDL', realpath(__DIR__ . '/../wsdl/EWSSecurity UATWsdl Version4_0/securityService_UAT.wsdl')),
            'consumer-service-wsdl' => env('EQUIFAX_XML2_UAT_CONSUMER_WSDL', realpath(__DIR__ . '/../wsdl/EWSConsumer UATWsdl Version4_0/consumerService_uat.wsdl')),
            'address-service-wsdl'  => env('EQUIFAX_XML2_UAT_ADDRESS_WSDL', realpath(__DIR__ . '/../wsdl/EWSAddress UATWsdl Version4_0/addressService_uat.wsdl')),
        ],
    ],

    EquifaxApiProduct::OPEN_BANKING_CONSUMER->value => [
        EquifaxEnvironment::SANDBOX->value => [
            'client-id'     => env('EQUIFAX_OPENBANK_SANDBOX_CLIENT_ID', ''),
            'client-secret' => env('EQUIFAX_OPENBANK_SANDBOX_CLIENT_SECRET', ''),
        ],
        EquifaxEnvironment::UAT->value => [
            'client-id'     => env('EQUIFAX_OPENBANK_UAT_CLIENT_ID', ''),
            'client-secret' => env('EQUIFAX_OPENBANK_UAT_CLIENT_SECRET', ''),
        ],
        EquifaxEnvironment::PRODUCTION->value => [
            'client-id'     => env('EQUIFAX_OPENBANK_PROD_CLIENT_ID', ''),
            'client-secret' => env('EQUIFAX_OPENBANK_PROD_CLIENT_SECRET', ''),
        ],
    ],

    'retry' => [
        // https://github.com/caseyamcl/guzzle_retry_middleware#options
        'lib-config' => [
            // Suggested library MUST BE INSTALLED if this is enabled.
            'retry_enabled'            => env('EQUIFAX_RETRY_ENABLED', false),
            'max_retry_attempts'       => env('EQUIFAX_RETRY_MAX', 3),
            'retry_on_status'          => [429, 500, 502, 503, 504],

            // Value to multiply the number of requests by if RetryAfter not supplied.
            // See https://github.com/caseyamcl/guzzle_retry_middleware#setting-default-retry-delay
            'default_retry_multiplier' => env('EQUIFAX_RETRY_MULTIPLIER', 1),

            // https://github.com/caseyamcl/guzzle_retry_middleware#adding-a-custom-retry-header-to-http-responses
            'expose_retry_header'      => true,
        ],

        // Should we log a debug message every time we retry
        'log-retries' => env('EQUIFAX_RETRY_LOG', true),
    ],
];
