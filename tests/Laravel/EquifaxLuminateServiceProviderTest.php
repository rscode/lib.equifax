<?php

namespace Tests\Ratespecial\Equifax\Laravel;

use Orchestra\Testbench\TestCase;
use Ratespecial\Equifax\Laravel\EquifaxLuminateServiceProvider;
use Ratespecial\Equifax\Laravel\EquifaxServiceProvider;
use Ratespecial\Equifax\Luminate\LuminateService;

class EquifaxLuminateServiceProviderTest extends TestCase
{
    protected function getPackageProviders($app): array
    {
        return [
            EquifaxServiceProvider::class,
            EquifaxLuminateServiceProvider::class,
        ];
    }

    public function testMakeLuminateService(): void
    {
        $i = $this->app->make(LuminateService::class);
        $this->assertInstanceOf(LuminateService::class, $i);

        $i = $this->app->make(EquifaxLuminateServiceProvider::SANDBOX_SERVICE);
        $this->assertInstanceOf(LuminateService::class, $i);

        $i = $this->app->make(EquifaxLuminateServiceProvider::UAT_SERVICE);
        $this->assertInstanceOf(LuminateService::class, $i);

        $i = $this->app->make(EquifaxLuminateServiceProvider::PRODUCTION_SERVICE);
        $this->assertInstanceOf(LuminateService::class, $i);
    }
}
