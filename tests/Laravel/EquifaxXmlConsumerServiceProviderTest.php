<?php

namespace Tests\Ratespecial\Equifax\Laravel;

use Orchestra\Testbench\TestCase;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Laravel\EquifaxServiceProvider;
use Ratespecial\Equifax\Laravel\EquifaxXmlConsumerServiceProvider;
use Ratespecial\Equifax\XMLConsumer\Address\AddressService;
use Ratespecial\Equifax\XMLConsumer\Consumer\ConsumerService;
use Ratespecial\Equifax\XMLConsumer\Security\SecurityService;

class EquifaxXmlConsumerServiceProviderTest extends TestCase
{
    protected function getPackageProviders($app): array
    {
        return [
            EquifaxServiceProvider::class,
            EquifaxXmlConsumerServiceProvider::class,
        ];
    }

    public function testMakeAddressService()
    {
        $i = $this->app->make(AddressService::class);
        $this->assertInstanceOf(AddressService::class, $i);

        $i = $this->app->make(EquifaxXmlConsumerServiceProvider::ADDRESS_UAT);
        $this->assertInstanceOf(AddressService::class, $i);
        $this->assertSame(EquifaxEnvironment::UAT, $i->getEnvironment());

        $i = $this->app->make(EquifaxXmlConsumerServiceProvider::ADDRESS_LIVE);
        $this->assertInstanceOf(AddressService::class, $i);
        $this->assertSame(EquifaxEnvironment::PRODUCTION, $i->getEnvironment());
    }

    public function testMakeSecurityService()
    {
        $i = $this->app->make(SecurityService::class);
        $this->assertInstanceOf(SecurityService::class, $i);

        $i = $this->app->make(EquifaxXmlConsumerServiceProvider::SECURITY_UAT);
        $this->assertInstanceOf(SecurityService::class, $i);
        $this->assertSame(EquifaxEnvironment::UAT, $i->getEnvironment());

        $i = $this->app->make(EquifaxXmlConsumerServiceProvider::SECURITY_LIVE);
        $this->assertInstanceOf(SecurityService::class, $i);
        $this->assertSame(EquifaxEnvironment::PRODUCTION, $i->getEnvironment());
    }

    public function testMakeXmlService()
    {
        $i = $this->app->make(ConsumerService::class);
        $this->assertInstanceOf(ConsumerService::class, $i);

        $i = $this->app->make(EquifaxXmlConsumerServiceProvider::XML_UAT);
        $this->assertInstanceOf(ConsumerService::class, $i);
        $this->assertSame(EquifaxEnvironment::UAT, $i->getEnvironment());

        $i = $this->app->make(EquifaxXmlConsumerServiceProvider::XML_LIVE);
        $this->assertInstanceOf(ConsumerService::class, $i);
        $this->assertSame(EquifaxEnvironment::PRODUCTION, $i->getEnvironment());
    }
}
