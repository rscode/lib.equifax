<?php

namespace Tests\Ratespecial\Equifax\Laravel\Commands;

use Orchestra\Testbench\TestCase;
use Ratespecial\Equifax\Laravel\EquifaxServiceProvider;
use Ratespecial\Equifax\XMLConsumer\Security\Credentials\ClientSecretCyclerFactory;
use Ratespecial\Equifax\XMLConsumer\Security\Credentials\ClientSecretCyclerService;

class MaintainSecretCommandTest extends TestCase
{
    protected function getPackageProviders($app): array
    {
        return [
            EquifaxServiceProvider::class,
        ];
    }

    public function testSuccess(): void
    {
        $mService = $this->createMock(ClientSecretCyclerService::class);
        // Constructor is disabled, so we must set this manually
        $mService->maxAgeInDays = 27;

        $mFactory = $this->createMock(ClientSecretCyclerFactory::class);
        $mFactory->expects($this->once())
            ->method('get')
            ->willreturn($mService);
        $this->app->instance(ClientSecretCyclerFactory::class, $mFactory);

        $this->artisan('equifax:maintain-secret', ['services' => 'xml-consumer', '--eq-environment' => 'uat']);
    }
}
