<?php

namespace Tests\Ratespecial\Equifax\Laravel\Commands;

use Illuminate\Contracts\Cache\Repository;
use Orchestra\Testbench\TestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Ratespecial\Equifax\Common\RefreshAccessTokenFactory;
use Ratespecial\Equifax\Common\RefreshAccessTokenService;
use Ratespecial\Equifax\Laravel\EquifaxServiceProvider;
use Ratespecial\Equifax\Luminate\Exceptions\InvalidCredentialsException;

class MaintainAccessTokensCommandTest extends TestCase
{
    private MockObject $mService;
    private MockObject $mFactory;
    private MockObject $mCache;

    protected function getPackageProviders($app): array
    {
        return [
            EquifaxServiceProvider::class,
        ];
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->mService = $this->createMock(RefreshAccessTokenService::class);
        $this->mFactory = $this->createMock(RefreshAccessTokenFactory::class);
        $this->mCache = $this->createMock(Repository::class);

        $this->app->instance(RefreshAccessTokenFactory::class, $this->mFactory);
        $this->app->instance(Repository::class, $this->mCache);
    }

    public function testSuccess(): void
    {
        $this->mService->expects($this->exactly(3))
            ->method('refreshIfNecessary');

        $this->mFactory->expects($this->exactly(3))
            ->method('get')
            ->willReturn($this->mService);

        $this->artisan('equifax:maintain-tokens', ['--eq-environment' => 'uat']);
    }

    public function testSuccessWithForce(): void
    {
        $this->mService->expects($this->exactly(3))
            ->method('refreshAndSave');

        $this->mFactory->expects($this->exactly(3))
            ->method('get')
            ->willReturn($this->mService);

        $this->artisan('equifax:maintain-tokens', ['--eq-environment' => 'uat', '--force' => true]);
    }

    public function testDefaultEnvironment(): void
    {
        $this->mService->expects($this->once())
            ->method('refreshAndSave');

        $this->mFactory->expects($this->once())
            ->method('get')
            ->willReturn($this->mService);

        $this->artisan('equifax:maintain-tokens', ['services' => 'luminate', '--force' => true]);
    }

    public function testInvalidCredentials(): void
    {
        $this->mCache->expects($this->once())
            ->method('put')
            ->with('failure-delay-LUMINATE@PRODUCTION');

        $this->mService->expects($this->once())
            ->method('refreshAndSave')
            ->willThrowException(new InvalidCredentialsException('the error message', 55));

        $this->mFactory->expects($this->once())
            ->method('get')
            ->willReturn($this->mService);

        $this->artisan('equifax:maintain-tokens', ['services' => 'luminate', '--force' => true]);
    }

    public function testSkipsOnDelayCache(): void
    {
        $this->mCache->expects($this->once())
            ->method('get')
            ->with('failure-delay-LUMINATE@PRODUCTION')
            ->willReturn('abc123');

        $this->mService->expects($this->never())
            ->method('refreshAndSave');

        $this->mFactory->expects($this->never())
            ->method('get');

        $this->artisan('equifax:maintain-tokens', ['services' => 'luminate', '--force' => true]);
    }
}
