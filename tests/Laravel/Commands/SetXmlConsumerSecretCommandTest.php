<?php

namespace Tests\Ratespecial\Equifax\Laravel\Commands;

use Orchestra\Testbench\TestCase;
use PHPUnit\Framework\Constraint\IsInstanceOf;
use Ratespecial\Equifax\Common\EquifaxApiProduct;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Laravel\EquifaxServiceProvider;
use Ratespecial\Equifax\Luminate\Credentials\PasswordStoreInterface;
use Ratespecial\Equifax\XMLConsumer\Security\Credentials\Credentials;
use Ratespecial\Equifax\XMLConsumer\Security\Credentials\CredentialsStoreInterface;

class SetXmlConsumerSecretCommandTest extends TestCase
{
    protected function getPackageProviders($app): array
    {
        return [
            EquifaxServiceProvider::class,
        ];
    }

    public function testSuccess(): void
    {
        $mLuminate = $this->createMock(PasswordStoreInterface::class);
        $this->app->instance(PasswordStoreInterface::class, $mLuminate);

        $mFactory = $this->createMock(CredentialsStoreInterface::class);
        $this->app->instance(CredentialsStoreInterface::class, $mFactory);

        $mFactory->expects($this->once())
            ->method('save')
            ->with(new IsInstanceOf(Credentials::class), EquifaxEnvironment::UAT);

        $this->artisan('equifax:set-secret', [
            'service'        => EquifaxApiProduct::XML_CONSUMER->value,
            'secret'         => 'supersecrethash',
            'eq-environment' => 'uat',
        ]);
    }
}
