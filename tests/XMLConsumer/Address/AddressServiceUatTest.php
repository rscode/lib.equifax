<?php

namespace Tests\Ratespecial\Equifax\XMLConsumer\Address;

use PHPUnit\Framework\TestCase;
use Ratespecial\Equifax\XMLConsumer\Address\AddressService;
use Ratespecial\Equifax\XMLConsumer\Address\ClassMap;
use Ratespecial\Equifax\XMLConsumer\Address\StructType\MatchAddressRequest;
use Ratespecial\Equifax\XMLConsumer\Address\StructType\Name;
use Ratespecial\Equifax\XMLConsumer\Address\StructType\NameAndAddress;
use Ratespecial\Equifax\XMLConsumer\Address\StructType\StructuredAddress;
use WsdlToPhp\PackageBase\SoapClientInterface;

/**
 * NOTE: You will need to run SecurityServiceUatTest::testLogon() to generate the token before these will work.
 */
class AddressServiceUatTest extends TestCase
{
    public function testPing()
    {
        $s = $this->buildService();

        $resp = $s->ping('generic');

        var_dump($resp);
    }

    public function testListAddressByPostcode()
    {
        $s = $this->buildService();

        $resp = $s->listAddressByPostcode('CB6 1AS', 'generic');

        var_dump($resp);
    }

    public function testMatchAddressUnique()
    {
        $s = $this->buildService();

        $req = new MatchAddressRequest('generic');

        $nameAndAddress = new NameAndAddress();

        $nameAndAddress->setName(new Name('MACIEJ', 'purse'));

        $nameAndAddress->setStructuredAddress(new StructuredAddress(
            number: 27,
            postcode: 'CB6 1AS',
        ));

        $req->addToNameAndAddress($nameAndAddress);

        $resp = $s->matchAddress($req);

//        var_dump($resp->getUniqueMatch()[0]->getMatchDetail()->getMatchedAddress()->getAddressID());

        var_dump($resp);
    }

    public function testMatchAddressNoMatch()
    {
        $s = $this->buildService();

        $req = new MatchAddressRequest('generic');

        $nameAndAddress = new NameAndAddress();

        $nameAndAddress->setName(new Name('MACIEJ', 'purse'));

        $nameAndAddress->setStructuredAddress(new StructuredAddress(
            number: 9999,
            postcode: '123 465',
        ));

        $req->addToNameAndAddress($nameAndAddress);

        $resp = $s->matchAddress($req);

        var_dump($resp);

        $this->assertNotEmpty($resp->getNoMatch());
    }

    private function buildService(): AddressService
    {
        $options = [
            SoapClientInterface::WSDL_URL      => TEST_ROOT . '/../wsdl/EWSAddress UATWsdl Version4_0/addressService_uat.wsdl',
            SoapClientInterface::WSDL_CLASSMAP => ClassMap::get(),
            SoapClientInterface::WSDL_LOCATION => 'https://api.uat.equifax.co.uk/efx-uk-xmlii-address/v1',
        ];

        return new AddressService($options, $this->readCachedToken());
    }

    private function readCachedToken(): string
    {
        $file = TEST_ROOT . '/cache/xml-access-token';

        if (file_exists($file)) {
            return file_get_contents($file);
        }

        return '';
    }
}
