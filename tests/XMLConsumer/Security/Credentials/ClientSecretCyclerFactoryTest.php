<?php

namespace Tests\Ratespecial\Equifax\XMLConsumer\Security\Credentials;

use Mockery;
use Orchestra\Testbench\TestCase;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Laravel\EquifaxServiceProvider;
use Ratespecial\Equifax\Laravel\EquifaxXmlConsumerServiceProvider;
use Ratespecial\Equifax\XMLConsumer\Security\Credentials\ClientSecretCyclerFactory;
use Ratespecial\Equifax\XMLConsumer\Security\Credentials\ClientSecretCyclerService;
use Ratespecial\Equifax\XMLConsumer\Security\Credentials\CredentialsStoreInterface;

class ClientSecretCyclerFactoryTest extends TestCase
{
    protected function getPackageProviders($app): array
    {
        return [
            EquifaxServiceProvider::class,
            EquifaxXmlConsumerServiceProvider::class,
        ];
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->instance(CredentialsStoreInterface::class, Mockery::mock(CredentialsStoreInterface::class));
    }

    public function testNullDays(): void
    {
        /** @var ClientSecretCyclerFactory $f */
        $f       = $this->app->make(ClientSecretCyclerFactory::class);
        $service = $f->get(EquifaxEnvironment::UAT);

        $this->assertInstanceOf(ClientSecretCyclerService::class, $service);
    }

    public function testWithDaysSupplied(): void
    {
        $maxDays = 5;

        /** @var ClientSecretCyclerFactory $f */
        $f       = $this->app->make(ClientSecretCyclerFactory::class);
        $service = $f->get(EquifaxEnvironment::UAT, $maxDays);

        $this->assertSame($maxDays, $service->maxAgeInDays);
    }
}
