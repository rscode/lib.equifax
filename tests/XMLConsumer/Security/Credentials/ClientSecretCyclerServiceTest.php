<?php

namespace Tests\Ratespecial\Equifax\XMLConsumer\Security\Credentials;

use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\XMLConsumer\Exceptions\MissingCredentialsException;
use Ratespecial\Equifax\XMLConsumer\Security\Credentials\ClientSecretCyclerService;
use Ratespecial\Equifax\XMLConsumer\Security\Credentials\Credentials;
use Ratespecial\Equifax\XMLConsumer\Security\Credentials\CredentialsStoreInterface;
use Ratespecial\Equifax\XMLConsumer\Security\SecurityService;
use Ratespecial\Equifax\XMLConsumer\Security\StructType\ChangeclientsecretResponse;

class ClientSecretCyclerServiceTest extends TestCase
{
    private $mSecurity;
    private $mStore;

    protected function setUp(): void
    {
        parent::setUp();

        $this->mSecurity = $this->createMock(SecurityService::class);
        $this->mStore    = $this->createMock(CredentialsStoreInterface::class);
    }

    public function testRequiresStoredCredentials()
    {
        $this->mSecurity->method('getEnvironment')
            ->willReturn(EquifaxEnvironment::UAT);
        $this->mStore->expects($this->once())
            ->method('load')
            ->willReturn(null);

        $this->expectException(MissingCredentialsException::class);

        $s = new ClientSecretCyclerService($this->mSecurity, $this->mStore);
        $s->refreshIfNecessary();
    }

    public function testDoesNotNeedRefresh(): void
    {
        $creds = new Credentials('id', 'secret', new Carbon('2023-02-15'));
        $this->mStore->expects($this->once())
            ->method('load')
            ->willReturn($creds);

        $this->mSecurity->expects($this->never())
            ->method('changeClientSecret');
        $this->mSecurity->method('getEnvironment')
            ->willReturn(EquifaxEnvironment::UAT);

        Carbon::setTestNowAndTimezone('2023-02-20', 'UTC');

        $s      = new ClientSecretCyclerService($this->mSecurity, $this->mStore);
        $result = $s->refreshIfNecessary();

        $this->assertFalse($result);
    }

    public function testNeedsRefresh(): void
    {
        $creds = new Credentials('id', 'secret', new Carbon('2023-02-15'));
        $this->mStore->expects($this->once())
            ->method('load')
            ->willReturn($creds);


        $changeResponse = new ChangeclientsecretResponse('new secret');
        $this->mSecurity->expects($this->once())
            ->method('changeClientSecret')
            ->willReturn($changeResponse);
        $this->mSecurity->method('getEnvironment')
            ->willReturn(EquifaxEnvironment::UAT);

        $this->mStore->expects($this->once())
            ->method('save')
            ->with($this->callback(function ($newCreds) use ($creds, $changeResponse) {
                return $creds->clientId == $newCreds->clientId
                    && $changeResponse->getClientSecret() == $newCreds->clientSecret
                    && Carbon::now()->toDateString() == $newCreds->generatedAt->toDateString();
            }));

        Carbon::setTestNowAndTimezone('2023-02-20', 'UTC');

        $s = new ClientSecretCyclerService($this->mSecurity, $this->mStore);

        // Date diff is 5, and only 2 is allowed.  Update should perform.
        $result = $s->refreshIfNecessary(2);

        $this->assertTrue($result);
    }
}
