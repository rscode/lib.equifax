<?php

namespace Tests\Ratespecial\Equifax\XMLConsumer\Security;

use PHPUnit\Framework\TestCase;
use Ratespecial\Equifax\XMLConsumer\Exceptions\XMLConsumerException;
use Ratespecial\Equifax\XMLConsumer\Security\SecurityService;
use SoapClient;
use SoapFault;
use stdClass;

class SecurityServiceTest extends TestCase
{
    public function testLogonFault()
    {
        $errorMessage = 'logon message';

        $this->expectException(XMLConsumerException::class);
        $this->expectExceptionMessage($errorMessage);

        $details                      = new stdClass();
        $details->logonFault          = new stdClass();
        $details->logonFault->code    = 'logon code';
        $details->logonFault->message = $errorMessage;

        $mSoap = $this->getMockBuilder(SoapClient::class)->disableOriginalConstructor()->getMock();
        $mSoap->expects($this->once())
            ->method('__soapCall')
            ->willThrowException(new SoapFault(5, 'Request processing error', details: $details));

        $s = new SecurityService([]);
        $s->setSoapClient($mSoap);

        $s->logon('abc', '123');
    }
}
