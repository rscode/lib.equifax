<?php

namespace Tests\Ratespecial\Equifax\XMLConsumer\Security\StructType;

use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use Ratespecial\Equifax\XMLConsumer\Security\StructType\LogonResponse;

class LogonResponseTest extends TestCase
{
    public function testBuildsAccessTokenObject()
    {
        Carbon::setTestNowAndTimezone('2023-03-21', 'UTC');

        $jwtString = file_get_contents(TEST_ROOT . '/fixtures/xml2/access-token');
        $r         = new LogonResponse($jwtString);

        $token = $r->getAccessTokenObject();

        $this->assertSame($jwtString, $token->access_token);
        $this->assertSame(1678399253, $token->issuedAt->timestamp);
        $this->assertSame(1678417253, $token->expiresAt()->timestamp);
        $this->assertTrue($token->isExpired());
    }
}
