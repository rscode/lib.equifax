<?php

namespace Tests\Ratespecial\Equifax\XMLConsumer\Security;

use PHPUnit\Framework\TestCase;
use Ratespecial\Equifax\XMLConsumer\Security\ClassMap;
use Ratespecial\Equifax\XMLConsumer\Security\SecurityService;
use Ratespecial\Equifax\XMLConsumer\Security\StructType\LogonResponse;
use WsdlToPhp\PackageBase\SoapClientInterface;

class SecurityServiceUatTest extends TestCase
{
    /**
     * Run this first, to cache the access token.  Be sure to set the .env vars
     */
    public function testLogon(): void
    {
        $s = $this->buildService();

        $resp = $s->logon(getenv('EQUIFAX_XML2_CLIENT_ID'), getenv('EQUIFAX_XML2_CLIENT_SECRET'));

        var_dump($resp->getAccessTokenObject());
        file_put_contents(TEST_ROOT . '/cache/xml-access-token', $resp->getAccesstoken());

        var_dump($resp);

        $this->assertInstanceOf(LogonResponse::class, $resp);
    }

    private function buildService(): SecurityService
    {
        $options = [
            SoapClientInterface::WSDL_URL      => TEST_ROOT . '/../wsdl/EWSSecurity UATWsdl Version4_0/securityService_UAT.wsdl',
            SoapClientInterface::WSDL_CLASSMAP => ClassMap::get(),
            SoapClientInterface::WSDL_LOCATION => 'https://api.uat.equifax.co.uk/efx-uk-xmlii-security/v1',
        ];

        return new SecurityService($options, $this->readCachedToken());
    }

    private function readCachedToken(): string
    {
        $file = TEST_ROOT . '/cache/xml-access-token';

        if (file_exists($file)) {
            return file_get_contents($file);
        }

        return '';
    }

    public function testPing(): void
    {
        $s = $this->buildService();

        $resp = $s->ping('generic');

        var_dump($resp);
    }

    public function testChangeClientSecret(): void
    {
        $s = $this->buildService();

        $resp = $s->changeClientSecret(getenv('EQUIFAX_XML2_CLIENT_ID'), getenv('EQUIFAX_XML2_CLIENT_SECRET'));

        var_dump($resp);
    }
}
