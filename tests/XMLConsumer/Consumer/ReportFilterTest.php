<?php

namespace Tests\Ratespecial\Equifax\XMLConsumer\Consumer;

use PHPUnit\Framework\TestCase;
use Ratespecial\Equifax\XMLConsumer\Consumer\ReportAnalyzer;
use Ratespecial\Equifax\XMLConsumer\Consumer\ReportFilter;

class ReportFilterTest extends TestCase
{
    public function testFilterNameMatchStatusA(): void
    {
        $report = json_decode(file_get_contents(TEST_ROOT . '/fixtures/xml2/SHELLI-ANN-RISBY-AND-OTHER.json'));

        $reportFilter = new ReportFilter();
        $filteredReport = $reportFilter->removeNonCustomerData($report);

        // Count specific datasets to test that elements with NameMatchStatus = "B" were removed
        $reportAnalyzer = new ReportAnalyzer($filteredReport);

        $courtAndInsolvencyData = $reportAnalyzer->getDataSection('courtAndInsolvencyInformationData');

        $this->assertCount(1, $courtAndInsolvencyData);
        $this->assertCount(2, $courtAndInsolvencyData[0]->data->courtAndInsolvencyInformation);

        $electoralRollData = $reportAnalyzer->getElectoralRoll();
        $this->assertCount(4, $electoralRollData);

        $creditCard = $reportAnalyzer->getCreditCardAccounts();
        $this->assertCount(5, $creditCard);

        $unsecuredLoan = $reportAnalyzer->getInsightAccountContainerSection('unsecuredLoan');
        $this->assertCount(2, $unsecuredLoan);

        // Test commsSupply accounts remain same
        $commsSupplyAccount = $reportAnalyzer->getInsightAccountContainerSection('commsSupplyAccount');
        $this->assertCount(5, $commsSupplyAccount);
    }
}
