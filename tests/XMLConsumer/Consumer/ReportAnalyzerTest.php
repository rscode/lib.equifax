<?php

namespace Tests\Ratespecial\Equifax\XMLConsumer\Consumer;

use PHPUnit\Framework\TestCase;
use Ratespecial\Equifax\XMLConsumer\Consumer\ReportAnalyzer;

class ReportAnalyzerTest extends TestCase
{
    public function testGetCreditCards(): void
    {
        $s = $this->buildService();

        $result = $s->getCreditCardAccounts();
        $this->assertCount(4, $result);
    }

    public function testGetCreditSearches(): void
    {
        $s = $this->buildService();

        $result = $s->getCreditSearches();
        $this->assertCount(6, $result);
    }

    public function testGetNonCreditSearches(): void
    {
        $s = $this->buildService();

        $result = $s->getNonCreditSearches();
        $this->assertCount(20, $result);
    }

    public function testGetScore(): void
    {
        $s = $this->buildService();

        $result = $s->getScore();
        $this->assertEquals(428, $result);
    }

    public function testGetMortgages(): void
    {
        $s = $this->buildService();

        $result = $s->getMortgages();
        $this->assertCount(1, $result);

        $this->assertSame('-BARCLAYS BANK(AUDIT)', $result[0]->data->companyName);

        $this->assertSame('ADV', $result[0]->address->getSourcedFrom());
        $this->assertSame('WEST FEN ROAD', $result[0]->address->getAddress()->getStreet1());
    }

    public function testGetCourtData(): void
    {
        $s = $this->buildService();

        $result = $s->getCourtAndInsolvencyInfo();

        $this->assertCount(2, $result);
    }

    public function testGetElectoralRoll(): void
    {
        $s = $this->buildService();

        $result = $s->getElectoralRoll();

        $this->assertCount(4, $result);
    }

    public function testDataSection(): void
    {
        $s = $this->buildService();

        $result = $s->getDataSection('insightData');

        $this->assertCount(4, $result);
    }

    public function testGetInsightData(): void
    {
        $s = $this->buildService();

        $result = $s->getInsightAccountContainerSection('creditCard');

        $this->assertCount(4, $result);
    }

    protected function buildService(): ReportAnalyzer
    {
        $data = json_decode(file_get_contents(TEST_ROOT . '/fixtures/xml2/SHELLI-ANN-RISBY-1979-10-11.json'));

        return new ReportAnalyzer($data);
    }
}
