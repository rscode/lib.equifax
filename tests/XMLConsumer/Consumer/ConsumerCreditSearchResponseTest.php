<?php

namespace Tests\Ratespecial\Equifax\XMLConsumer\Consumer;

use JsonMapper;
use PHPUnit\Framework\TestCase;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\ConsumerCreditSearchResponse;

class ConsumerCreditSearchResponseTest extends TestCase
{
    public function testNoMatch()
    {
        $data = file_get_contents(TEST_ROOT . '/fixtures/xml2/no-match.json');

        $m = new JsonMapper();

        /** @var ConsumerCreditSearchResponse $o */
        $o = $m->map(json_decode($data), ConsumerCreditSearchResponse::class);

        $this->assertTrue($o->isNoMatch());
    }
}
