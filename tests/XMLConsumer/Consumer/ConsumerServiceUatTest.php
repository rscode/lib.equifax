<?php

namespace Tests\Ratespecial\Equifax\XMLConsumer\Consumer;

use PHPUnit\Framework\TestCase;
use Ratespecial\Equifax\XMLConsumer\Consumer\ClassMap;
use Ratespecial\Equifax\XMLConsumer\Consumer\ConsumerService;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\MatchCriteriaStatus;
use Ratespecial\Equifax\XMLConsumer\Consumer\EnumType\RequestScope;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AliasRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AssociateRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\AttributeRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\CIFASRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\ConsumerCreditSearchRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\CourtAndInsolvencyInformationRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\ElectoralRollRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\InsightHistoryRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\InsightRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\Name;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\NoticeOfCorrectionOrDisputeRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\OutputAddressRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\RequestSoleSearch;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\ResidenceInstance;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\ResidentialStructuredAddress;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\RollingRegisterRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\ScoreAndCharacteristicRequests;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\ScoreRequest;
use Ratespecial\Equifax\XMLConsumer\Consumer\StructType\SearchesRequest;
use WsdlToPhp\PackageBase\SoapClientInterface;

/**
 * NOTE: You will need to run SecurityServiceUatTest::testLogon() to generate the token before these will work.
 */
class ConsumerServiceUatTest extends TestCase
{
    public function testConsumerCreditSearchNoMatch(): void
    {
        $this->consumerCreditSearch('1980-01-01', 'jimbo', 'jones', '123', 'main Road', 'abc 123', 'Ely');
    }

    public function testConsumerCreditSearchKatrina(): void
    {
        $this->consumerCreditSearch('1988-01-27', 'KATRINA', 'ALPTEKIN', '600', 'cromwell Road', 'CB6 1AS', 'Ely');
    }

    public function testConsumerCreditSearchShelliAnn(): void
    {
        $this->consumerCreditSearch('1979-10-11', 'SHELLI-ANN', 'RISBY', '3985', 'west fen Road', 'CB6 1AN', 'Ely');
    }

    public function testConsumerCreditSearchMasood(): void
    {
        // addy id 28030104751
        $this->consumerCreditSearch('1983-05-12', 'BONIFATE', 'MASOOD', '608', 'cromwell Road', 'CB6 1AS', 'Ely');
    }

    public function testConsumerCreditSearchOrlaLock(): void
    {
        $this->consumerCreditSearch('1965-10-11', 'LOCK', 'orla', '1509', 'downham Road', 'CB6 1Af', 'Ely');
    }

    public function testConsumerCreditSearchMajBorovyk(): void
    {
        $this->consumerCreditSearch('1980-02-01', 'allsopp', 'fulong', '1736', 'bernard st', 'CB6 1Au', 'Ely');
    }

    protected function consumerCreditSearch(
        string $dob,
        string $fname,
        string $lname,
        string $addyName,
        string $street1,
        string $postcode,
        string $postTown,
    ): void {
        $s = $this->buildService();

        // addy id 28030104751

        $residentialStructuredAddress = new ResidentialStructuredAddress(
            name: $addyName,
            postcode: $postcode,
            postTown: $postTown,
            street1: $street1,
        );

        $soleSearch = new RequestSoleSearch();
        $soleSearch->getPrimary()
            ->setDob($dob)
            ->setName(new Name($fname, $lname))
            ->setCurrentAddress(new ResidenceInstance(address: $residentialStructuredAddress));

        $soleSearch->getMatchCriteria()
            ->setAttributable(MatchCriteriaStatus::VALUE_REQUIRED)
            ->setSubject(MatchCriteriaStatus::VALUE_REQUIRED);

        $scoreAndCharacteristicRequests = new ScoreAndCharacteristicRequests(
            true,
            new ScoreRequest(['PSOLF01']),
            new AttributeRequest(1),
        );
        $soleSearch->getRequestedData()
            ->setCourtAndInsolvencyInformationRequest(new CourtAndInsolvencyInformationRequest(0, RequestScope::VALUE_SUPPLIED_AND_LINKED))
            ->setOutputAddressRequest(new OutputAddressRequest(0, RequestScope::VALUE_SUPPLIED_AND_LINKED))
            ->setElectoralRollRequest(new ElectoralRollRequest(0, RequestScope::VALUE_SUPPLIED_AND_LINKED))
            ->setSearchesRequest(new SearchesRequest(0, RequestScope::VALUE_SUPPLIED))
            ->setInsightRequest(new InsightRequest(0, RequestScope::VALUE_SUPPLIED_AND_LINKED))
            ->setInsightHistoryRequest(new InsightHistoryRequest(0, RequestScope::VALUE_SUPPLIED_AND_LINKED))
            ->setNoticeofCorrectionOrDisputeRequest(new NoticeOfCorrectionOrDisputeRequest(0, RequestScope::VALUE_SUPPLIED_AND_LINKED))
            ->setRollingRegisterRequest(new RollingRegisterRequest(0, RequestScope::VALUE_SUPPLIED_AND_LINKED))
            ->setScoreAndCharacteristicRequests($scoreAndCharacteristicRequests)
            ->setCifasRequest(new CIFASRequest(0, RequestScope::VALUE_SUPPLIED_AND_LINKED))
            ->setAliasRequest(new AliasRequest(0))
            ->setAssociateRequest(new AssociateRequest(0, false))
            ;

        $r = new ConsumerCreditSearchRequest(clientRef: 'generic');
        $r->setStoreIdentity('abc123')
            ->setSoleSearch($soleSearch);

        $resp = $s->consumerCreditSearch($r);

        file_put_contents("$fname-$lname-$dob.json", $resp->toJson());
        var_dump($resp);
    }

    private function buildService(): ConsumerService
    {
        $options = [
            SoapClientInterface::WSDL_URL      => TEST_ROOT . '/../wsdl/EWSConsumer UATWsdl Version4_0/consumerService_uat.wsdl',
            SoapClientInterface::WSDL_CLASSMAP => ClassMap::get(),
            SoapClientInterface::WSDL_LOCATION => 'https://api.uat.equifax.co.uk/efx-uk-xmlii-consumer/v1',
        ];

        return new ConsumerService($options, $this->readCachedToken());
    }

    private function readCachedToken(): string
    {
        $file = TEST_ROOT . '/cache/xml-access-token';

        if (file_exists($file)) {
            return file_get_contents($file);
        }

        return '';
    }

    public function testPing(): void
    {
        $s = $this->buildService();

        $resp = $s->ping('generic');

        var_dump($resp);
    }
}
