<?php

namespace Tests\Ratespecial\Equifax\Luminate;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use PHPUnit\Framework\TestCase;
use Ratespecial\Equifax\Common\AccessToken;
use Ratespecial\Equifax\Luminate\LuminateService;
use Ratespecial\Equifax\Luminate\Requests\DigitalTrustRequest;
use Ratespecial\Equifax\Luminate\Requests\IdentityVerificationRequest;

class LuminateServiceSandboxTest extends TestCase
{
    /**
     * Run this first.  It will pull a new access token and cache it in tests/cache.  Other functions will use this.
     */
    public function testGetToken()
    {
        $s = $this->buildService();

        try {
            $token = $s->generateAccessToken('luminate', 'sandbox');
        } catch (BadResponseException $ex) {
            echo $ex->getResponse()->getBody();
            throw $ex;
        }

        var_dump($token);

        file_put_contents(TEST_ROOT . '/cache/luminate-access-token', serialize($token));

        $this->assertTrue(true);
    }

    public function testIdentityVerification()
    {
        $s = $this->buildService();

        $req = new IdentityVerificationRequest();

        try {
            $resp = $s->execute($req);
        } catch (BadResponseException $ex) {
            echo $ex->getResponse()->getBody();
            throw $ex;
        }

        var_dump($resp);
    }

    public function testDigitalTrust()
    {
        $s = $this->buildService();

        $req = new DigitalTrustRequest();

        try {
            $resp = $s->execute($req);
        } catch (BadResponseException $ex) {
            echo $ex->getResponse()->getBody();
            throw $ex;
        }

        var_dump($resp);
    }

    private function buildService(): LuminateService
    {
        $client = new Client([
            'base_uri' => 'https://api.sandbox.equifax.co.uk',
        ]);

        return new LuminateService($client, $this->readCachedToken());
    }

    private function readCachedToken(): ?AccessToken
    {
        $file = __DIR__ . '/../cache/luminate-access-token';

        if (file_exists($file)) {
            $str = file_get_contents($file);

            $token = unserialize($str);

            if ($token instanceof AccessToken) {
                return $token;
            }
        }

        return null;
    }
}
