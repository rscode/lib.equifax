<?php

namespace Tests\Ratespecial\Equifax\Luminate;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Ratespecial\Equifax\Common\AccessToken;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Exceptions\MissingAccessTokenException;
use Ratespecial\Equifax\Luminate\Enums\IdentityDecisionAssessment;
use Ratespecial\Equifax\Luminate\Exceptions\InvalidCredentialsException;
use Ratespecial\Equifax\Luminate\Exceptions\LuminateException;
use Ratespecial\Equifax\Luminate\LuminateService;
use Ratespecial\Equifax\Luminate\Requests\IdentityAssuranceRequest;
use Ratespecial\Equifax\Luminate\Requests\IdentityVerificationRequest;

class LuminateServiceTest extends TestCase
{
    public function testGetTokenSuccess(): void
    {
        $mock         = new MockHandler([
            new Response(200, body: file_get_contents(TEST_ROOT . '/fixtures/luminate/access-token-success.json')),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client       = new Client(['handler' => $handlerStack]);
        $s            = new LuminateService($client, EquifaxEnvironment::SANDBOX);

        $token = $s->generateAccessToken('user', 'pw');

        $this->assertSame('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNzMxOTU3MDAyLCJleHAiOjE3MzE5NzUwMDJ9.9kxSztHiOk7QfKSfYFxgLQnoQXd1F6-4axz7uDBeCms', $token->access_token);
        $this->assertSame('a refresh token', $token->refresh_token);
        $this->assertFalse($token->isExpired());
    }

    public function testGetTokenError(): void
    {
        $mock         = new MockHandler([
            new Response(200, body: file_get_contents(TEST_ROOT . '/fixtures/luminate/access-token-failed.json')),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client       = new Client(['handler' => $handlerStack]);
        $s            = new LuminateService($client, EquifaxEnvironment::SANDBOX);

        $this->expectException(LuminateException::class);
        $this->expectExceptionMessage("Missing or invalid parameter 'grant_type'");

        $s->generateAccessToken('user', 'pw');
    }

    public function testIdentityVerificationRequiresAccessToken(): void
    {
        $this->expectException(MissingAccessTokenException::class);

        $mock         = new MockHandler([
            new Response(200, body: file_get_contents(TEST_ROOT . '/fixtures/luminate/identity-verification-success.json')),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client       = new Client(['handler' => $handlerStack]);
        $s            = new LuminateService($client, EquifaxEnvironment::SANDBOX);

        $req = new IdentityVerificationRequest();

        $s->execute($req);
    }

    public function testIdentityVerificationSuccess(): void
    {
        $mock         = new MockHandler([
            new Response(200, body: file_get_contents(TEST_ROOT . '/fixtures/luminate/identity-verification-success.json')),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client       = new Client(['handler' => $handlerStack]);
        $s            = new LuminateService($client, EquifaxEnvironment::SANDBOX, new AccessToken());

        $req = new IdentityVerificationRequest();

        $resp = $s->execute($req);

        $this->assertSame(IdentityDecisionAssessment::PASS, $resp->getDecision());
        $this->assertCount(1, $resp->outputAddress);
        $this->assertSame($resp->getDecision(), $resp->assessment['decision']);
        $this->assertSame('Raymond', $resp->data->identity->name->firstName);
    }

    public function testIdentityVerificationError(): void
    {
        $mock         = new MockHandler([
            new Response(200, body: file_get_contents(TEST_ROOT . '/fixtures/luminate/identity-verification-error.json')),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client       = new Client(['handler' => $handlerStack]);
        $s            = new LuminateService($client, EquifaxEnvironment::SANDBOX, new AccessToken());

        $req = new IdentityAssuranceRequest();

        $resp = $s->execute($req);

        $this->assertNull($resp->outputAddress);
        $this->assertEmpty($resp->getDecision());
    }

    public function testExecuteError(): void
    {
        $mock         = new MockHandler([
            new Response(200, body: file_get_contents(TEST_ROOT . '/fixtures/luminate/access-token-failed.json')),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client       = new Client(['handler' => $handlerStack]);
        $s            = new LuminateService($client, EquifaxEnvironment::SANDBOX, new AccessToken());

        $this->expectException(LuminateException::class);
        $this->expectExceptionMessage("Missing or invalid parameter 'grant_type'");

        $req = new IdentityAssuranceRequest();

        $s->execute($req);
    }

    public function testInvalidCredentials(): void
    {
        $mock         = new MockHandler([
            new Response(400, body: file_get_contents(TEST_ROOT . '/fixtures/luminate/invalid-credentials.json')),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client       = new Client(['handler' => $handlerStack]);
        $s            = new LuminateService($client, EquifaxEnvironment::SANDBOX, new AccessToken());

        $this->expectException(InvalidCredentialsException::class);
        $this->expectExceptionMessage('The credentials provided were invalid.');

        $s->generateAccessToken('user', 'pw');
    }

    public function testExecuteError500(): void
    {
        $response = new Response(500, body: file_get_contents(TEST_ROOT . '/fixtures/luminate/access-token-failed.json'));
        $mock         = new MockHandler([
            new ServerException('server exception', new Request('POST', ''), $response),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client       = new Client(['handler' => $handlerStack]);
        $s            = new LuminateService($client, EquifaxEnvironment::SANDBOX, new AccessToken());

        $this->expectException(LuminateException::class);
        $this->expectExceptionMessage("Missing or invalid parameter 'grant_type'");

        $req = new IdentityAssuranceRequest();

        $s->execute($req);
    }
}
