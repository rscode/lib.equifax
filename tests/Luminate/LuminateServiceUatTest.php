<?php

namespace Tests\Ratespecial\Equifax\Luminate;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Ratespecial\Equifax\Common\AccessToken;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Common\RandomPasswordGenerator;
use Ratespecial\Equifax\Luminate\Enums\IovationType;
use Ratespecial\Equifax\Luminate\Enums\Orchestration;
use Ratespecial\Equifax\Luminate\LuminateService;
use Ratespecial\Equifax\Luminate\Models\Address;
use Ratespecial\Equifax\Luminate\Models\Identity;
use Ratespecial\Equifax\Luminate\Models\KbaAnswers;
use Ratespecial\Equifax\Luminate\Requests\DigitalTrustRequest;
use Ratespecial\Equifax\Luminate\Requests\DigitalTrustResponse;
use Ratespecial\Equifax\Luminate\Requests\IdentityAssuranceRequest;
use Ratespecial\Equifax\Luminate\Requests\IdentityAssuranceResponse;
use Ratespecial\Equifax\Luminate\Requests\IdentityVerificationRequest;

class LuminateServiceUatTest extends TestCase
{
    /**
     * Run this first.  It will pull a new access token and cache it in tests/cache.  Other functions will use this.
     */
    public function testGetToken()
    {
        $s = $this->buildService();

        try {
            $token = $s->generateAccessToken(getenv('EQUIFAX_LUMINATE_USERNAME'), getenv('EQUIFAX_LUMINATE_PASSWORD'));
        } catch (BadResponseException $ex) {
            echo $ex->getResponse()->getBody();
            throw $ex;
        }

        var_dump($token);

        file_put_contents(TEST_ROOT . '/cache/luminate-access-token', serialize($token));

        $this->assertTrue(true);
    }

    public function testGetPasswordExpirationDate(): void
    {
        $s = $this->buildService();

        try {
            $date = $s->getPasswordExpirationDate(getenv('EQUIFAX_LUMINATE_USERNAME'));
        } catch (BadResponseException $ex) {
            echo $ex->getResponse()->getBody();
            throw $ex;
        }

        var_dump($date);

        $this->assertTrue(true);
    }

    public function testUpdatePassword(): void
    {
        $s = $this->buildService();

        $newPw = RandomPasswordGenerator::generate();
        var_dump($newPw);

        try {
            $s->updatePassword(
                getenv('EQUIFAX_LUMINATE_USERNAME'),
                getenv('EQUIFAX_LUMINATE_PASSWORD'),
                $newPw
            );
        } catch (BadResponseException $ex) {
            echo $ex->getResponse()->getBody();
            throw $ex;
        }

        $this->assertTrue(true);
    }

    public function testIdentityVerification()
    {
        $s = $this->buildService();

        $req = new IdentityVerificationRequest();

        try {
            $resp = $s->execute($req);
        } catch (BadResponseException $ex) {
            echo $ex->getResponse()->getBody();
            throw $ex;
        }

        var_dump($resp);
    }

    public function testIdentityAssuranceInitial()
    {
        $s = $this->buildService();

        $req = new IdentityAssuranceRequest();

        $req->setProductConfiguration('IdentityConf')
            ->setOrchestration(Orchestration::IDENTITY)
            ->setClientOrganization(getenv('EQUIFAX_LUMINATE_CLIENT_ORG'))
            ->setClientImplementation(getenv('EQUIFAX_LUMINATE_CLIENT_IMPL'));

        $req->tenantId             = getenv('EQUIFAX_LUMINATE_TENANT');
        $req->applicationId        = getenv('EQUIFAX_LUMINATE_APPLICATION');
        $req->configId             = getenv('EQUIFAX_LUMINATE_CONFIG');

        $req->referenceId = 'customer-guid-abc123';

        $identity                  = new Identity();
        $identity->name->firstName = 'anita';
        $identity->name->lastName  = 'parker';

        $addy               = new Address();
        $addy->countryCode  = 'UK';
        $addy->addressLine1 = '346 IDVERIFIER ST';
        $addy->postalCode   = 'cb62ag';
        $identity->addAddress($addy);

        $identity->dateOfBirth->readString('1960-02-15');
        $req->setIdentity($identity);

        try {
            /** @var IdentityAssuranceResponse $resp */
            $resp = $s->execute($req);

            echo "Decision: {$resp->getDecision()->name}\n";
        } catch (BadResponseException $ex) {
            echo $ex->getResponse()->getBody();
            throw $ex;
        }

        var_dump($resp);
    }

    public function testIdentityAssuranceKBAAnswer()
    {
        $s = $this->buildService();

        $req = new IdentityAssuranceRequest();

        $req->setProductConfiguration('AssuranceConf')
            ->setOrchestration(Orchestration::ASSURANCE)
            ->setClientOrganization(getenv('EQUIFAX_LUMINATE_CLIENT_ORG'))
            ->setClientImplementation(getenv('EQUIFAX_LUMINATE_CLIENT_IMPL'));

        $req->tenantId             = getenv('EQUIFAX_LUMINATE_TENANT');
        $req->applicationId        = getenv('EQUIFAX_LUMINATE_APPLICATION');
        $req->configId             = getenv('EQUIFAX_LUMINATE_CONFIG');

        // correlationId from testIdentityAssuranceInitial() response
        $req->correlationId = 'a1f21586-75f0-4011-9269-fdde6eabe03a';

        $kbaAnswers                   = new KbaAnswers();
        $kbaAnswers->authenticationId = 'e5658a89-6573-44ea-b083-f5c5be4d269f';
        $kbaAnswers->questionnaireId  = 1;
        $kbaAnswers->addAnswer(1, 4)
            ->addAnswer(2, 1)
            ->addAnswer(3, 2);
        $req->setKbaAnswers($kbaAnswers);

        try {
            /** @var IdentityAssuranceResponse $resp */
            $resp = $s->execute($req);

            echo "Decision: {$resp->getDecision()?->name}";
        } catch (BadResponseException $ex) {
            echo $ex->getResponse()->getBody();
            throw $ex;
        }

        var_dump($resp);
    }

    public function testDigitalTrust()
    {
        $s = $this->buildService();

        $req = new DigitalTrustRequest();
        $req->setClientOrganization(getenv('EQUIFAX_LUMINATE_CLIENT_ORG'))
            ->setClientImplementation(getenv('EQUIFAX_LUMINATE_CLIENT_IMPL_DIGITAL_TRUST'));

        $req->tenantId             = getenv('EQUIFAX_LUMINATE_TENANT');
        $req->applicationId        = getenv('EQUIFAX_LUMINATE_APPLICATION');
        $req->configId             = getenv('EQUIFAX_LUMINATE_CONFIG');

        $req->setIpAddress('1.2.3.4')
            ->setBlackbox('0400k7BRB106MVuVebKatfMjILVVpv8TjrJnsCkpFeqwLzYmSvqPoGczpQDhLymIorURO3j+YkPhJGyLNbFtvEdQl5RvFCMSbl5xkAQawBX7fR91szOwgLATBypq4/eQIAw4ZOY6osDdKdHC18e1J1RJhBOa3U6qhJBrxFtm61Fh9+DzIaFWA+hW9GDq2CP/ftOCIgufWrFbWkTxVajijX/olF2DiRBGUSER121491Dnt3a37RiGDPObJ+vi5DzQnOqYzk5bA5YU8bAugcndHHk6jClmq+PnlXMs/pr71oD561q8qgCu7KxEZVp3ByHIry5UVlf0ztqicuFOfdJx+sF9hMLXx7UnVEmEE5rdTqqEkGvEW2brUWH34PMhoVYD6Fb0YOrYI/9+04IiC59asVtaRO9RJsn4u3PdHf7WUtiodkQXpcNbEdJ9xVjMbBh1dWhTxheisVRn6UNv47mN+mlXb7D/pYqSmAY+0NtKrtsgSRXyVG3mWsl5VYpPiEnCm6zyr3zqcajHW/OfmYDmH9nSBC6lf1FmdBh00y6vwAYEGRvpyg4exwDTUPisCU3dizTSDQdctnwNmB+21Iy51Gm/DRJFV3ZrLq9nq8xz1YAjtLYCwYN/WX6QYH5mpgdKDEqDfM/5NRYxcdOCKkW7tG33TrJq1aUHPSfqIB97+BC/WN3PXqZGc6fh9AbbuycYL4WpFG9MVE6nWxXQbbWDwK5ImYYWmUmv6MorDq55SUazQAT2IMrF1CIovd2Ij3TdXznNbZWp/5NFsh1iTDOguRPLm+1sRoFJwLN+kuN7pHlNT+U9GC17jlnhcwCLIxeRPT/YjwZOoEVFPaYRTfzsuPzNXIK4mjKAMqULsj69LfM7VRYrI1FeHSbDspvXHhwykQoiuMSNUUyGAvKLg+9aginJmamvnOVDH3SzV6i8/tb5alAK/XRNo3H5dMIK6EAX6crild28VNRpuiAA5cNpT+Vu2lSAFu7g0I1zLD4mV3TQqnyLRgJPdhutiWSFHGwlJrrCJiDbCfVZTGBk19tyNs7g8wQxSxzPuRtycb0H5IjHkCcKSs4KVYKB4o2TkenZ9brI1B4bAJ4Hgt503KYhOcRC5SUvcM7ACXSGXOp4yYEIf+Jh5Nwu1x6PIDsDsHfN+ugZgRgN+Z8HAFhGFzd/m6snVgtcnU5pFekiB8n/NyT5AQJEWh+JVbpQPIEYDfmfBwBYRhc3f5urJ1YLXJ1OaRXpIgfJ/zck+QECRFofiVW6UDyBGA35nwcAWEYXN3+bqydWtp0PYHSzkHaNc557XJoPF7zxBTr1PcxX96FOpPevDWtH2XR0QkfOd02D73n81x6hEMCy0s3hRLn08Th9FlNHDPbKRXey06cY4a7euFqiMUgzqbwpmWa8Ix9sc424c5V5+F9+VNHlcz2VwAGEd3H4L952RAUPKwCzLQAfZaA7jSiwHHrDW3HSYxnPse2ZJCsq4e3f1u/ETzp5VpgkQXTQzZ2bCkUkx/iDDMpMV0RqqMO/4PoTXuhr9FU8NguKENqer1nNtP8OECkk/64lKoqM3wDorceNFA82L/DmuTZotRRZEBlF3CwJErGAqHKF92svMyIXqubL0y1a+8GxQj+MNg1daQYRfkIByBCQzRWZoTKAI4Nf6/22OSEZipO4O2u4JqfZfs1WgQJYAupKiIfFTQ79OZmhtMq+yzR7++NeG1q5Oo8Q5U6LRXJP973bIb2MLBc30Y1OfhAmbFzNjtK9XcVBt1aS4yGun6sJkTnH5pT4ESvR2tLhf1e0CNkmNB8SE0Myu8Ryc6vz3wafI/WYHJlN/o9aRQdZA6k0JbwW1mn6eqP7kMlJAgE40HF60O5ouwLLNTE9qpzezmUfvH4VA8MC0aE8YWUMi28qzMRUtoMQh3zDNhbf49x8JeHt9iUxGRKU9nZdXGy5CGnSyBSzZXydL0g8oojei4vE6gMfJACobb0Z4oFBKkm98fymcf5MOS6t0VJ7d+YacxYbOJydKL4e0Gu7vUyWV+1c6jxqko3yQmhUNw8wJXAJ59b7L163YZnhEICl1w4nrpJ8ZHH4GfDN1uG5Z27nOIr1FRTFbacIGLE3kbGqabKbzEOKceFcEi3vl1NcHuMPU0flysuOBR2M9by9RYgflXiIhtDfvsrzgx/uGvukwXA5b4lMrLsFcSzoyFfBg3oDY3Gltz5uXPM9+XLmqxwm7n1k/jnzHkpOyW7kISetGo90ORF3Ug7js/AOAzLbm2oJOeTXTfoxJESPmPNtziPg+j+bv48ixh64cIPD6qQoGgpjQ7CGuFxQev+4arC4Loko+ak4EwMcjNWVqalVDSyspBvK0Gfdi64D2QkBDsSCagdQdDAYl4fkXgP3TviamVaRge6Q3Ss3qbPgPahfzF10cM7yAgE6POQwa9lMahnRH7eP2rP7NzAKHNx6Jigqx2dq0h/6j6kFBgnRHnGBH/HI08t8E/3oSmAhpwCo1cQGrULgzcSXRgcwdvOz8KBCM6AgaWJm58A8MyInHLp5kYza0HZNLUmKdwfTzNhvftZGq10JW5UE+/QNfg1vrhIfjzc6P8uemDtzziWKIlG4r5bdYtBUbpa7l0GkSk7mDx9eIpUdrhrx57KLWRPdRfT4L+bKeLhTDj3dPqjBOrSBha48tzDarx3/JqyMOl3HDTtT4wDIA20+JAKO3ghM1a+uDBUWDnS80dVqUN9iPyQTRqqsjB1w4yyPAdZri73AAaNQDLH2XvzLI9l24kqW+vDxlg9287PwoEIzoA6ojTijhS29IiccunmRjNrQdk0tSYp3B6uc2Efk24xzXQlblQT79A1+DW+uEh+PN+M7mJNJFnWOzRYhpJbYXCY3wwzswJ1U92HSIaXRIXYYFyFtDbskNEC9NvSzkqpx0576p3C8A7bYN+ecKO8TwQvBWkcQ0tWfGcE8L1nCF53zkMsOBraDdlOY6My0rHbjpsuVNEzjGOR3S4hk+eKBgClJT0gs9xmmxfkODd3bQIojLjrOmEHw62wA4JszXefuuA+0qboxRtV7WojxyUuXFj1ZzTVEGoLW1sPdGGdmoKLiqC49tIM3sawfkqAyGXD87YQqa3WHGDA85ICJbv21Q8bkZAwLRDow1ZvkVkCREyMER4WaxJ1otiy90ifyPeTtLfF5qY4gB/u+Yi8qJtogg7gae0S+HPtKuHYpPgQiEVyidD6uUgxN7BWFEksYEZ4VUw==')
            ->setIovationType(IovationType::ENROLLMENT)
            ->setIovationAccountCode('UAT testing');

        try {
            /** @var DigitalTrustResponse $resp */
            $resp = $s->execute($req);

            echo "Decision: {$resp->getDecision()?->name}";
        } catch (BadResponseException $ex) {
            echo $ex->getResponse()->getBody();
            throw $ex;
        }

        var_dump($resp);
    }

    private function buildService(): LuminateService
    {
        $stack = new HandlerStack();
        $stack->setHandler(new CurlHandler());

        $middle = Middleware::tap(function (RequestInterface $req) {
            $body = (string)$req->getBody();

            if (!empty($body)) {
                $body = json_decode($body, JSON_OBJECT_AS_ARRAY);
            }
        });

        $stack->push($middle);

        $client = new Client([
            'base_uri' => 'https://api.uat.equifax.co.uk',
            'handler'  => $stack,
        ]);

        return new LuminateService($client, EquifaxEnvironment::UAT, $this->readCachedToken());
    }

    private function readCachedToken(): ?AccessToken
    {
        $file = __DIR__ . '/../cache/luminate-access-token';

        if (file_exists($file)) {
            $str = file_get_contents($file);

            $token = unserialize($str);

            if ($token instanceof AccessToken) {
                return $token;
            }
        }

        return null;
    }
}
