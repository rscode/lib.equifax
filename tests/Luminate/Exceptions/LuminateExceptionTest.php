<?php

namespace Tests\Ratespecial\Equifax\Luminate\Exceptions;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Ratespecial\Equifax\Luminate\Exceptions\LuminateException;

class LuminateExceptionTest extends TestCase
{
    public function testCanReadErrorResponse()
    {
        $clientException = new ClientException(
            '',
            new Request('POST', ''),
            new Response(400, body: file_get_contents(TEST_ROOT . '/fixtures/luminate/access-token-failed.json'))
        );

        $ourException = new LuminateException(
            (string)$clientException->getResponse()->getBody(),
            $clientException->getCode(),
            $clientException,
        );

        $this->assertSame(400, $ourException->getCode());
        $this->assertSame("Missing or invalid parameter 'grant_type'.", $ourException->getMessage());
        $this->assertSame("Missing or invalid parameter 'grant_type'.", $ourException->allErrors[0]->message);
    }
}
