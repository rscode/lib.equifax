<?php

namespace Tests\Ratespecial\Equifax\Luminate\Credentials;

use Carbon\Carbon;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Luminate\Credentials\LuminateCredentials;
use Ratespecial\Equifax\Luminate\Credentials\PasswordCyclerService;
use Ratespecial\Equifax\Luminate\Credentials\PasswordStoreInterface;
use Ratespecial\Equifax\Luminate\LuminateService;
use Ratespecial\Equifax\XMLConsumer\Exceptions\MissingCredentialsException;

class PasswordCyclerServiceTest extends TestCase
{
    private MockObject $mSecurity;
    private MockObject $mStore;

    protected function setUp(): void
    {
        parent::setUp();

        $this->mSecurity = $this->createMock(LuminateService::class);
        $this->mStore    = $this->createMock(PasswordStoreInterface::class);
    }

    public function testRequiresStoredCredentials()
    {
        $this->mSecurity->method('getEnvironment')
            ->willReturn(EquifaxEnvironment::UAT);
        $this->mStore->expects($this->once())
            ->method('load')
            ->willReturn(null);

        $this->expectException(MissingCredentialsException::class);

        $s = new PasswordCyclerService($this->mSecurity, $this->mStore);
        $s->refreshIfNecessary();
    }

    public function testDoesNotNeedRefresh(): void
    {
        $creds = new LuminateCredentials('username', 'secret', new Carbon('2023-02-15'));
        $this->mStore->expects($this->once())
            ->method('load')
            ->willReturn($creds);

        $this->mSecurity->expects($this->never())
            ->method('updatePassword');
        $this->mSecurity->method('getEnvironment')
            ->willReturn(EquifaxEnvironment::UAT);

        Carbon::setTestNowAndTimezone('2023-02-20', 'UTC');

        $s      = new PasswordCyclerService($this->mSecurity, $this->mStore);
        $result = $s->refreshIfNecessary();

        $this->assertFalse($result);
    }

    public function testNeedsRefresh(): void
    {
        $creds = new LuminateCredentials('username', 'secret', new Carbon('2023-02-15'));
        $this->mStore->expects($this->once())
            ->method('load')
            ->willReturn($creds);

        $this->mSecurity->expects($this->once())
            ->method('updatePassword');
        $this->mSecurity->method('getEnvironment')
            ->willReturn(EquifaxEnvironment::UAT);

        $this->mStore->expects($this->once())
            ->method('save')
            ->with($this->callback(function ($newCreds) use ($creds) {
                return $creds->username == $newCreds->username
                    && !empty($newCreds->password)
                    && Carbon::now()->toDateString() == $newCreds->generatedAt->toDateString();
            }));

        Carbon::setTestNowAndTimezone('2023-02-20', 'UTC');

        $s = new PasswordCyclerService($this->mSecurity, $this->mStore);

        // Date diff is 5, and only 2 is allowed.  Update should perform.
        $result = $s->refreshIfNecessary(2);

        $this->assertTrue($result);
    }
}
