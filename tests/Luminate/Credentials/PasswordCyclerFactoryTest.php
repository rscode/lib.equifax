<?php

namespace Tests\Ratespecial\Equifax\Luminate\Credentials;

use Mockery;
use Orchestra\Testbench\TestCase;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Laravel\EquifaxLuminateServiceProvider;
use Ratespecial\Equifax\Laravel\EquifaxServiceProvider;
use Ratespecial\Equifax\Luminate\Credentials\PasswordCyclerFactory;
use Ratespecial\Equifax\Luminate\Credentials\PasswordCyclerService;
use Ratespecial\Equifax\Luminate\Credentials\PasswordStoreInterface;

class PasswordCyclerFactoryTest extends TestCase
{
    protected function getPackageProviders($app): array
    {
        return [
            EquifaxServiceProvider::class,
            EquifaxLuminateServiceProvider::class,
        ];
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->instance(PasswordStoreInterface::class, Mockery::mock(PasswordStoreInterface::class));
    }

    public function testNullDays(): void
    {
        /** @var PasswordCyclerFactory $f */
        $f       = $this->app->make(PasswordCyclerFactory::class);
        $service = $f->get(EquifaxEnvironment::UAT);

        $this->assertInstanceOf(PasswordCyclerService::class, $service);
    }

    public function testWithDaysSupplied(): void
    {
        $maxDays = 5;

        /** @var PasswordCyclerFactory $f */
        $f       = $this->app->make(PasswordCyclerFactory::class);
        $service = $f->get(EquifaxEnvironment::UAT, $maxDays);

        $this->assertSame($maxDays, $service->maxAgeInDays);
    }
}
