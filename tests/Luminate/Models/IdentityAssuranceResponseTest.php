<?php

namespace Tests\Ratespecial\Equifax\Luminate\Models;

use JsonMapper;
use PHPUnit\Framework\TestCase;
use Ratespecial\Equifax\Luminate\Enums\IdentityDecisionAssessment;
use Ratespecial\Equifax\Luminate\Enums\Product;
use Ratespecial\Equifax\Luminate\Models\KbaQuestions;
use Ratespecial\Equifax\Luminate\Requests\IdentityAssuranceResponse;

class IdentityAssuranceResponseTest extends TestCase
{
    private function buildMapper(): JsonMapper
    {
        $mapper = new JsonMapper();

        // If this function exists on the mapped object, it will be called at the end.  Good for re-organizing an object once
        // all data is present.
        $mapper->postMappingMethod = 'afterMapping';

        // Some fields can be null
//        $mapper->bStrictNullTypes = false;

        // If there are some new fields in the record that our class doesn't define, set them in the object.
        $mapper->undefinedPropertyHandler = function ($object, $propName, $jsonValue) {
            $object->$propName = $jsonValue;
        };

        return $mapper;
    }

    public function testWillMap()
    {
        $data = file_get_contents(TEST_ROOT . '/fixtures/luminate/identity-assurance-1.json');
        $data = json_decode($data);

        $jsonMapper = $this->buildMapper();

        /** @var IdentityAssuranceResponse $resp */
        $resp = $jsonMapper->map($data, new IdentityAssuranceResponse());

        $this->assertSame(Product::IDENTITY_ASSURANCE, $resp->productId);
        $this->assertSame(IdentityDecisionAssessment::CHALLENGE, $resp->getDecision());
    }

    public function testBuildsKbaQuestionsInResults()
    {
        $data = file_get_contents(TEST_ROOT . '/fixtures/luminate/identity-assurance-1.json');
        $data = json_decode($data);

        $jsonMapper = $this->buildMapper();

        /** @var IdentityAssuranceResponse $resp */
        $resp = $jsonMapper->map($data, new IdentityAssuranceResponse());

        // One of the $result objects will be the Kba questions
        $resultObjs = array_filter(
            $resp->results,
            fn($result) => $result instanceof KbaQuestions
        );
        $this->assertInstanceOf(KbaQuestions::class, $resultObjs[0]);

        $this->assertTrue($resp->hasKbaQuestions());
        $this->assertInstanceOf(KbaQuestions::class, $resp->getKbaQuestions());
        $this->assertSame('7e2f2b97-2db1-4910-861b-7ab046857076', $resp->getKbaQuestions()->authenticationId);
    }

    public function testFailDecision()
    {
        $data = file_get_contents(TEST_ROOT . '/fixtures/luminate/identity-assurance-fail.json');
        $data = json_decode($data);

        $jsonMapper = $this->buildMapper();

        /** @var IdentityAssuranceResponse $resp */
        $resp = $jsonMapper->map($data, new IdentityAssuranceResponse());

        $this->assertSame(IdentityDecisionAssessment::FAIL, $resp->getDecision());
        $this->assertFalse($resp->hasKbaQuestions());
    }

    public function testUndecisioned()
    {
        $data = file_get_contents(TEST_ROOT . '/fixtures/luminate/identity-assurance-undecisioned.json');
        $data = json_decode($data);

        $jsonMapper = $this->buildMapper();

        /** @var IdentityAssuranceResponse $resp */
        $resp = $jsonMapper->map($data, new IdentityAssuranceResponse());

        $this->assertSame(IdentityDecisionAssessment::UNDECISIONED, $resp->getDecision());
        $this->assertFalse($resp->hasKbaQuestions());

        $this->assertSame('c14bd8c5-1667-4543-947e-dd5e32843bd3', $resp->getKbaDecisionBlock()->authenticationId);
        $this->assertSame(
            'QNGR: Not enough questions to meet questionnaire generation criteria.',
            $resp->getKbaDecisionBlock()->getDecision()->text
        );
        $this->assertSame('undecisioned', $resp->getKbaDecisionBlock()->getDecision()->value);
    }

    public function testCanGetErrors()
    {
        $data = file_get_contents(TEST_ROOT . '/fixtures/luminate/identity-assurance-kba-answer-errors.json');
        $data = json_decode($data);

        $jsonMapper = $this->buildMapper();

        /** @var IdentityAssuranceResponse $resp */
        $resp = $jsonMapper->map($data, new IdentityAssuranceResponse());

        $this->assertFalse($resp->hasKbaQuestions());
        $this->assertCount(1, $resp->getErrors());
    }

    public function testKbaAnswerPass()
    {
        $data = file_get_contents(TEST_ROOT . '/fixtures/luminate/identity-assurance-kba-answer-pass.json');
        $data = json_decode($data);

        $jsonMapper = $this->buildMapper();

        /** @var IdentityAssuranceResponse $resp */
        $resp = $jsonMapper->map($data, new IdentityAssuranceResponse());

        $this->assertSame(IdentityDecisionAssessment::PASS, $resp->getDecision());
        $this->assertFalse($resp->hasKbaQuestions());

        $this->assertSame('77f291e5-eecd-403a-ab09-361d7cdda57e', $resp->getKbaDecisionBlock()->authenticationId);
        $this->assertSame('pass', $resp->getKbaDecisionBlock()->getDecision()->value);
    }
}
