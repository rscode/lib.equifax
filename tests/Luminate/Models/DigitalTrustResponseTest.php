<?php

namespace Tests\Ratespecial\Equifax\Luminate\Models;

use JsonMapper;
use PHPUnit\Framework\TestCase;
use Ratespecial\Equifax\Luminate\Enums\DigitalTrustAssessment;
use Ratespecial\Equifax\Luminate\Enums\Product;
use Ratespecial\Equifax\Luminate\Requests\DigitalTrustResponse;

class DigitalTrustResponseTest extends TestCase
{
    private function buildMapper(): JsonMapper
    {
        $mapper = new JsonMapper();

        // If this function exists on the mapped object, it will be called at the end.  Good for re-organizing an object once
        // all data is present.
        $mapper->postMappingMethod = 'afterMapping';

        // Some fields can be null
//        $mapper->bStrictNullTypes = false;

        // If there are some new fields in the record that our class doesn't define, set them in the object.
        $mapper->undefinedPropertyHandler = function ($object, $propName, $jsonValue) {
            $object->$propName = $jsonValue;
        };

        return $mapper;
    }

    public function testWillMap()
    {
        $data = file_get_contents(TEST_ROOT . '/fixtures/luminate/digital-trust-success-medium.json');
        $data = json_decode($data);

        $jsonMapper = $this->buildMapper();

        /** @var DigitalTrustResponse $resp */
        $resp = $jsonMapper->map($data, new DigitalTrustResponse());

        $this->assertSame(Product::DIGITAL_TRUST, $resp->productId);
        $this->assertSame(DigitalTrustAssessment::MEDIUM, $resp->getDecision());
    }
}
