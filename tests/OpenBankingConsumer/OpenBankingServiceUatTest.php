<?php

namespace Tests\Ratespecial\Equifax\OpenBankingConsumer;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use PHPUnit\Framework\TestCase;
use Ratespecial\Equifax\Common\AccessToken;
use Ratespecial\Equifax\OpenBankingConsumer\Models\CustomerIdLookupRequest;
use Ratespecial\Equifax\OpenBankingConsumer\OpenBankingService;

class OpenBankingServiceUatTest extends TestCase
{
    /**
     * Run this first.  It will pull a new access token and cache it in tests/cache.  Other functions will use this.
     */
    public function testGetToken()
    {
        $s = $this->buildService();

        try {
            $token = $s->generateAccessToken();
        } catch (BadResponseException $ex) {
            echo $ex->getResponse()->getBody();
            throw $ex;
        }

        var_dump($token);

        file_put_contents(TEST_ROOT . '/cache/openbankingconsumer-access-token', serialize($token));
    }

    public function testIdentityVerification()
    {
        $s = $this->buildService();

        $req = new CustomerIdLookupRequest();
        $req->setExternalReference('abc123');

        try {
            $resp = $s->execute($req);
        } catch (BadResponseException $ex) {
            echo $ex->getResponse()->getBody();
            throw $ex;
        }

        var_dump($resp);
    }

    private function buildService(): OpenBankingService
    {
        $client = new Client([
            'base_uri' => 'https://api.uat.equifax.co.uk',
        ]);
        $creds  = new AccessToken();

        return new OpenBankingService($client, $creds);
    }

    private function readCachedToken(): ?AccessToken
    {
        $file = __DIR__ . '/../cache/openbankingconsumer-access-token';

        if (file_exists($file)) {
            $str = file_get_contents($file);

            $token = unserialize($str);

            if ($token instanceof AccessToken) {
                return $token;
            }
        }

        return null;
    }
}
