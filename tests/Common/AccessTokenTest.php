<?php

namespace Tests\Ratespecial\Equifax\Common;

use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use Ratespecial\Equifax\Common\AccessToken;

class AccessTokenTest extends TestCase
{
    public function testSuccess(): void
    {
        $jwt = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNzMxOTU3MDAyLCJleHAiOjE3MzE5NzUwMDJ9.9kxSztHiOk7QfKSfYFxgLQnoQXd1F6-4axz7uDBeCms';

        $t = AccessToken::createFromJWT($jwt);

        Carbon::setTestNow('2024-11-18 22:08:02');

        $this->assertSame($jwt, $t->access_token);
        $this->assertSame(18000, $t->ttl);

        $this->assertSame('2024-11-18 19:10:02', $t->issuedAt->toDateTimeString());
        $this->assertSame('2024-11-19 00:10:02', $t->expiresAt()->toDateTimeString());
        $this->assertSame(7320, (int) $t->lifeLeft()->totalSeconds);
    }
}
