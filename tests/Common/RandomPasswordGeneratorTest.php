<?php

namespace Tests\Ratespecial\Equifax\Common;

use PHPUnit\Framework\TestCase;
use Ratespecial\Equifax\Common\RandomPasswordGenerator;

class RandomPasswordGeneratorTest extends TestCase
{
    public function testSuccess(): void
    {
        $res = RandomPasswordGenerator::generate();

        $this->assertTrue(strlen($res) == 30);
    }
}
