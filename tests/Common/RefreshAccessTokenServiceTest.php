<?php

namespace Tests\Ratespecial\Equifax\Common;

use PHPUnit\Framework\TestCase;
use Ratespecial\Equifax\Common\AccessToken;
use Ratespecial\Equifax\Common\AccessTokenCacheInterface;
use Ratespecial\Equifax\Common\EquifaxApiProduct;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Common\RefreshAccessTokenService;
use Ratespecial\Equifax\Common\TokenGenerationServiceInterface;

class RefreshAccessTokenServiceTest extends TestCase
{
    public function testRefreshNotNeeded(): void
    {
        $mGenerator = $this->createMock(TokenGenerationServiceInterface::class);
        $mCache     = $this->createMock(AccessTokenCacheInterface::class);

        $mGenerator->expects($this->once())
            ->method('getServiceName')
            ->willReturn(EquifaxApiProduct::LUMINATE);
        $mGenerator->expects($this->once())
            ->method('getEnvironment')
            ->willReturn(EquifaxEnvironment::UAT);

        $token      = new AccessToken();
        $token->ttl = 18000;  // 18000 = 5 hours

        $mCache->expects($this->once())
            ->method('load')
            ->willReturn($token);

        $s = new RefreshAccessTokenService($mGenerator, $mCache, minuteThreshold: 60);

        // Won't be necessary.  Token expires in 5 hours, threshold is 1 hour.
        $result = $s->refreshIfNecessary();

        $this->assertFalse($result);
    }

    public function testRefreshToken(): void
    {
        $mGenerator = $this->getMockBuilder(TokenGenerationServiceInterface::class)->getMock();
        $mCache     = $this->getMockBuilder(AccessTokenCacheInterface::class)->getMock();

        $mGenerator->method('getServiceName')
            ->willReturn(EquifaxApiProduct::LUMINATE);
        $mGenerator->method('getEnvironment')
            ->willReturn(EquifaxEnvironment::UAT);

        $expectedNewToken               = new AccessToken();
        $expectedNewToken->access_token = 'new value';
        $mGenerator->expects($this->once())
            ->method('generateAccessToken')
            ->willReturn($expectedNewToken);

        $token = new AccessToken();
        $token->issuedAt->subMinutes(270);   // 270 = 4.5 hours
        $token->ttl = 18000;             // 18000 = 5 hours

        $mCache->expects($this->once())
            ->method('load')
            ->willReturn($token);

        $mCache->expects($this->once())
            ->method('save');

        $s = new RefreshAccessTokenService($mGenerator, $mCache, minuteThreshold: 60);

        // Won't be necessary.  Token expires in 5 hours, threshold is 1 hour.
        $result = $s->refreshIfNecessary();

        $this->assertTrue($result);
    }
}
