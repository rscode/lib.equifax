<?php

namespace Tests\Ratespecial\Equifax\Common;

use Carbon\Carbon;
use Orchestra\Testbench\TestCase;
use Ratespecial\Equifax\Common\EquifaxApiProduct;
use Ratespecial\Equifax\Common\EquifaxEnvironment;
use Ratespecial\Equifax\Common\RefreshAccessTokenFactory;
use Ratespecial\Equifax\Exceptions\MissingClientSecretException;
use Ratespecial\Equifax\Laravel\EquifaxLuminateServiceProvider;
use Ratespecial\Equifax\Laravel\EquifaxOpenBankingServiceProvider;
use Ratespecial\Equifax\Laravel\EquifaxServiceProvider;
use Ratespecial\Equifax\Laravel\EquifaxXmlConsumerServiceProvider;
use Ratespecial\Equifax\Luminate\Credentials\LuminateCredentials;
use Ratespecial\Equifax\Luminate\Credentials\PasswordStoreInterface;
use Ratespecial\Equifax\XMLConsumer\Security\Credentials\Credentials;
use Ratespecial\Equifax\XMLConsumer\Security\Credentials\CredentialsStoreInterface;

class RefreshAccessTokenFactoryTest extends TestCase
{
    protected function getPackageProviders($app): array
    {
        return [
            EquifaxServiceProvider::class,
            EquifaxLuminateServiceProvider::class,
            EquifaxOpenBankingServiceProvider::class,
            EquifaxXmlConsumerServiceProvider::class,
        ];
    }

    public function testLuminate(): void
    {
        $mStore = $this->createMock(PasswordStoreInterface::class);
        $this->app->instance(PasswordStoreInterface::class, $mStore);

        $mStore->expects($this->exactly(3)) // we call the service twice from this test.  each call will hit load once.
            ->method('load')
            ->willReturn(new LuminateCredentials('username', 'secret', Carbon::now()));

        /** @var RefreshAccessTokenFactory $f */
        $f = $this->app->make(RefreshAccessTokenFactory::class);

        $res = $f->get(EquifaxApiProduct::LUMINATE, EquifaxEnvironment::PRODUCTION);
        $this->assertSame(EquifaxEnvironment::PRODUCTION, $res->getTokenService()->getEnvironment());
        $this->assertSame(EquifaxApiProduct::LUMINATE, $res->getTokenService()->getServiceName());

        $res = $f->get(EquifaxApiProduct::LUMINATE, EquifaxEnvironment::UAT);
        $this->assertSame(EquifaxEnvironment::UAT, $res->getTokenService()->getEnvironment());
        $this->assertSame(EquifaxApiProduct::LUMINATE, $res->getTokenService()->getServiceName());

        $res = $f->get(EquifaxApiProduct::LUMINATE, EquifaxEnvironment::SANDBOX);
        $this->assertSame(EquifaxEnvironment::SANDBOX, $res->getTokenService()->getEnvironment());
        $this->assertSame(EquifaxApiProduct::LUMINATE, $res->getTokenService()->getServiceName());
    }

    public function testXmlConsumer(): void
    {
        $mStore = $this->createMock(CredentialsStoreInterface::class);
        $this->app->instance(CredentialsStoreInterface::class, $mStore);

        $mStore->expects($this->exactly(2)) // we call the service twice from this test.  each call will hit load once.
            ->method('load')
            ->willReturn(new Credentials('id', 'secret', Carbon::now()));

        /** @var RefreshAccessTokenFactory $f */
        $f = $this->app->make(RefreshAccessTokenFactory::class);

        $res = $f->get(EquifaxApiProduct::XML_CONSUMER, EquifaxEnvironment::PRODUCTION);
        $this->assertSame(EquifaxEnvironment::PRODUCTION, $res->getTokenService()->getEnvironment());
        $this->assertSame(EquifaxApiProduct::XML_CONSUMER, $res->getTokenService()->getServiceName());

        $res = $f->get(EquifaxApiProduct::XML_CONSUMER, EquifaxEnvironment::UAT);
        $this->assertSame(EquifaxEnvironment::UAT, $res->getTokenService()->getEnvironment());
        $this->assertSame(EquifaxApiProduct::XML_CONSUMER, $res->getTokenService()->getServiceName());
    }

    public function testXmlConsumerErrorOnMissingSecret(): void
    {
        $mStore = $this->getMockBuilder(CredentialsStoreInterface::class)->getMock();
        $this->app->instance(CredentialsStoreInterface::class, $mStore);

        $mStore->expects($this->once())
            ->method('load')
            ->willReturn(null);

        /** @var RefreshAccessTokenFactory $f */
        $f = $this->app->make(RefreshAccessTokenFactory::class);

        $this->expectException(MissingClientSecretException::class);

        $res = $f->get(EquifaxApiProduct::XML_CONSUMER, EquifaxEnvironment::UAT);
    }



//    public function testOpenBanking()
//    {
//        /** @var RefreshAccessTokenFactory $f */
//        $f = $this->app->make(RefreshAccessTokenFactory::class);
//
//        $res = $f->get(EquifaxApiProduct::OPEN_BANKING_CONSUMER, EquifaxEnvironment::PRODUCTION);
//        $this->assertSame(EquifaxEnvironment::PRODUCTION, $res->getTokenService()->getEnvironment());
//        $this->assertSame(EquifaxApiProduct::OPEN_BANKING_CONSUMER, $res->getTokenService()->getServiceName());
//
//        $res = $f->get(EquifaxApiProduct::OPEN_BANKING_CONSUMER, EquifaxEnvironment::UAT);
//        $this->assertSame(EquifaxEnvironment::UAT, $res->getTokenService()->getEnvironment());
//        $this->assertSame(EquifaxApiProduct::OPEN_BANKING_CONSUMER, $res->getTokenService()->getServiceName());
//    }
}
