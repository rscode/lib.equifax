<?php

use WsdlToPhp\PackageGenerator\ConfigurationReader\GeneratorOptions;
use WsdlToPhp\PackageGenerator\Generator\Generator;

require 'vendor/autoload.php';

/*
 * Address service
 */

/** @var GeneratorOptions $options */
$options = GeneratorOptions::instance();
$options->setOrigin('wsdl/EWSAddress UATWsdl Version4_0/addressService_uat.wsdl')
    ->setDestination('src/XMLConsumer/Address')
    ->setStandalone(false)
    ->setGenerateTutorialFile(false)
    ->setNamespaceDictatesDirectories(false)
    ->setSrcDirname('')
    ->setNamespace('Ratespecial\Equifax\XMLConsumer\Address');

$generator = new Generator($options);
$generator->generatePackage();

/*
 * Consumer
 */

/** @var GeneratorOptions $options */
$options = GeneratorOptions::instance();
$options->setOrigin('wsdl/EWSConsumer UATWsdl Version4_0/consumerService_uat.wsdl')
    ->setDestination('src/XMLConsumer/Consumer')
    ->setStandalone(false)
    ->setGenerateTutorialFile(false)
    ->setNamespaceDictatesDirectories(false)
    ->setSrcDirname('')
    ->setNamespace('Ratespecial\Equifax\XMLConsumer\Consumer');

$generator = new Generator($options);
$generator->generatePackage();

/*
 * Security
 */

/** @var GeneratorOptions $options */
$options = GeneratorOptions::instance();
$options->setOrigin('wsdl/EWSSecurity UATWsdl Version4_0/securityService_UAT.wsdl')
    ->setDestination('src/XMLConsumer/Security')
    ->setStandalone(false)
    ->setGenerateTutorialFile(false)
    ->setNamespaceDictatesDirectories(false)
    ->setSrcDirname('')
    ->setNamespace('Ratespecial\Equifax\XMLConsumer\Security');

$generator = new Generator($options);
$generator->generatePackage();
